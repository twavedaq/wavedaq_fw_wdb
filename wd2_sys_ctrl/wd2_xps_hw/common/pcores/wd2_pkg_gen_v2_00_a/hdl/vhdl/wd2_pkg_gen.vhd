---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  WaveDream2 Package Generator
--
--  Project :  WaveDream2
--
--  PCB  :  DRS4 WaveDream Board II (121202C)
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  09.08.2017 15:14:57
--
--  Description : Package generator for the WaveDream2 (WD2) board. Prepares the
--                packages over ethernet or serdes. The unit generates the WD2
--                specific header and stores a certain set of event data to
--                allow for momentary higher event rates and retransmission.
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

-- Add timing constraints to ignore cross clock domain paths
-- between microblaze and daq:
-- TIMESPEC TS_UC_TO_DAQ = FROM "TG_UC" TO "TG_DAQ" TIG;
-- TIMESPEC TS_DAQ_TO_UC = FROM "TG_DAQ" TO "TG_UC" TIG;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_MISC.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

library wd2_pkg_gen_v2_00_a;
use wd2_pkg_gen_v2_00_a.wd2_pkg_gen_cfg_package.all;

entity wd2_pkg_gen is
port
(
  -- ADC/DRS Data Input
  ADC_DATA_I                   : in  std_logic_vector(CPK_NR_OF_ADCS*CPK_CHNLS_PER_ADC*CPK_DRS_ADC_CH_WR_DWIDTH-1 downto 0);
  -- TDC Data Input
  TDC_DATA_I                   : in  std_logic_vector(CPK_NR_OF_TDC_CHNLS*CPK_TDC_WR_DWIDTH-1 downto 0);
  -- Advanced Trigger output Data
  TRG_DATA_I                   : in  std_logic_vector(CPK_TRG_WR_DWIDTH-1 downto 0);
  -- Scaler Data Input
  SCL_DATA_I                   : in  std_logic_vector(CPK_NR_OF_SCALERS*CPK_SCL_WR_DWIDTH-1 downto 0);

  -- Control
  CAPTURE_DATA_I               : in  std_logic;
  DATA_SERDES_DRS_WE_I         : in  std_logic;
  DRS_DATA_VALID_I             : in  std_logic;
  ADC_SAMPLING_DIV_I           : in  std_logic_vector(7 downto 0); -- only store every nth sample in ADC ringbuffer
  DRS_CH_TX_EN_I               : in  std_logic_vector(CPK_NR_OF_ADCS*CPK_CHNLS_PER_DRS-1 downto 0);
  ADC_CH_TX_EN_I               : in  std_logic_vector(CPK_NR_OF_ADCS*CPK_CHNLS_PER_ADC-1 downto 0);
  TDC_CH_TX_EN_I               : in  std_logic_vector(CPK_NR_OF_TDC_CHNLS-1 downto 0);
  TRG_TX_EN_I                  : in  std_logic;
  SCL_TX_EN_I                  : in  std_logic;
  ZERO_SUPPRESSION_EN_I        : in  std_logic;
  ZERO_SUPPRESSION_INHIBIT_I   : in  std_logic;
  ZERO_SUPPR_INHIBIT_STORE_I   : in  std_logic;
  ZERO_SUPPRESSION_MASK_I      : in  std_logic_vector(15 downto 0);
  DRS_TX_SAMPLES_I             : in  std_logic_vector(10 downto 0);
  ADC_TX_SAMPLES_I             : in  std_logic_vector(11 downto 0);
  TDC_TX_SAMPLES_I             : in  std_logic_vector(12 downto 0);
  TRG_TX_SAMPLES_I             : in  std_logic_vector( 9 downto 0);
  DRS_MAX_SAMPLES_PER_PKG_I    : in  std_logic_vector(15 downto 0);
  ADC_MAX_SAMPLES_PER_PKG_I    : in  std_logic_vector(15 downto 0);
  TDC_MAX_SAMPLES_PER_PKG_I    : in  std_logic_vector(17 downto 0);
  TRG_MAX_SAMPLES_PER_PKG_I    : in  std_logic_vector(15 downto 0);
  SCL_MAX_SAMPLES_PER_PKG_I    : in  std_logic_vector(15 downto 0);
  FIRST_PKG_DLY_I              : in  std_logic_vector(13 downto 0);
  BUSY_O                       : out std_logic;
  ETH_DST_VALID_I              : in  std_logic;
  ETH_COM_EN_I                 : in  std_logic;
  SERDES_COM_EN_I              : in  std_logic;
  DAQ_CLK_I                    : in  std_logic;
  RST_I                        : in  std_logic;

  -- HEADER DATA INPUT
  HDR_PROTOCOL_VERSION_O       : out std_logic_vector(7 downto 0);
  HDR_BOARD_TYPE_I             : in  std_logic_vector(3 downto 0);
  HDR_BOARD_REVISION_I         : in  std_logic_vector(3 downto 0);
  HDR_SERIAL_NUMBER_I          : in  std_logic_vector(15 downto 0);
  HDR_CRATE_ID_I               : in  std_logic_vector(7 downto 0);
  HDR_SLOT_ID_I                : in  std_logic_vector(7 downto 0);
  -- CH_INFO_ADC     filled by read FSM
  -- CH_INFO_CHANNEL filled by read FSM
  -- PACKAGE_TYPE    filled by read FSM
  -- FIRST_CHANNEL of type  filled by read FSM
  -- LAST_CHANNEL of type   filled by read FSM
  HDR_BOARD_VARIANT_I          : in  std_logic_vector( 1 downto 0);
  HDR_TRIG_INFO_NEW_I          : in  std_logic;
  HDR_TRIG_INFO_PERR_I         : in  std_logic;
  HDR_DAQ_PLL_LOCK_I           : in  std_logic;
  HDR_DRS_PLL_LOCK_I           : in  std_logic_vector( 1 downto 0);
  -- HDR_LMK_PLL_LOCK_I           : in   std_logic;
  -- Trigger Source
  -- Bits per sample
  -- Samples per event per channel
  -- PAYLOAD_LENGTH  filled by read FSM
  -- data_chunk_offset filled by read FSM
  HDR_TRIGGER_INFO_I           : in  std_logic_vector(47 downto 0);
  HDR_EVENT_NUMBER_I           : in  std_logic_vector(31 downto 0); -- stored on trigger
  HDR_TIMESTAMP_I              : in  std_logic_vector(63 downto 0);
  HDR_SAMPLING_FREQ_DRS_I      : in  std_logic_vector(31 downto 0); -- selected by read FSM based on data type
  HDR_SAMPLING_FREQ_ADC_I      : in  std_logic_vector(31 downto 0); -- selected by read FSM based on data type
  HDR_SAMPLING_FREQ_TDC_I      : in  std_logic_vector(31 downto 0); -- selected by read FSM based on data type
  HDR_SAMPLING_FREQ_TRG_I      : in  std_logic_vector(31 downto 0); -- selected by read FSM based on data type
  HDR_DRS_TRIGGER_CELL_I       : in  std_logic_vector(19 downto 0);  -- selected by read FSM based on channel info
  HDR_DRS_TRIGGER_CELL_VALID_I : in  std_logic;
  HDR_TEMPERATURE_I            : in  std_logic_vector(15 downto 0);
  HDR_DAC_OFS_I                : in  std_logic_vector(15 downto 0);
  HDR_DAC_ROFS_I               : in  std_logic_vector(15 downto 0);
  HDR_DAC_PZC_I                : in  std_logic_vector(15 downto 0);
  HDR_FRONTEND_SETTINGS_I      : in  std_logic_vector(CPK_NR_OF_ADCS*CPK_CHNLS_PER_ADC*9-1 downto 0);

  EVENT_TX_RATE_O              : out std_logic_vector(31 downto 0);

  -- Data output interface
  ETH_COM_EN_O                 : out std_logic;
  SERDES_COM_EN_O              : out std_logic;
  LL32_O_DATA_O                : out std_logic_vector(31 downto 0);
  LL32_O_SRC_RDY_N_O           : out std_logic;
  LL32_O_DST_RDY_N_I           : in  std_logic;
  LL32_O_SOF_N_O               : out std_logic;
  LL32_O_EOF_N_O               : out std_logic
);
end wd2_pkg_gen;

architecture struct of wd2_pkg_gen is

  signal com_enable                : std_logic := '0';
  signal tx_pkg_type               : std_logic_vector( 7 downto 0)  := (others=>'0');
  signal start_of_frame            : std_logic := '0';
  signal end_of_frame              : std_logic := '0';
  signal side_data                 : std_logic_vector( 5 downto 0) := (others=>'0');
  signal buffer_tx_data            : std_logic_vector(23 downto 0) := (others=>'0');
  signal header_tx_data            : std_logic_vector(15 downto 0) := (others=>'0');
  signal tx_data                   : std_logic_vector(25 downto 0) := (others=>'0');
  signal data_size                 : std_logic_vector( 1 downto 0) := (others=>'0');

  signal data_valid                : std_logic := '0';

  signal header_ren                : std_logic := '0';
  signal header_load               : std_logic := '0';
  signal start_of_header           : std_logic := '0';
  signal end_of_header             : std_logic := '0';
  signal type_tx_en                : std_logic_vector(CPK_NR_OF_ADCS*CPK_CHNLS_PER_DRS-1 downto 0) := (others=>'0');
  signal item_tx_en                : std_logic := '0';
  signal last_type                 : std_logic := '0';
  signal last_ch                   : std_logic := '0';
  signal rd_buffer_ready           : std_logic := '0';
  signal wr_drs_buffer_ready       : std_logic := '0';
  signal rd_drs_buffer_ready       : std_logic := '0';
  signal wr_ring_buffer_ready      : std_logic := '0';
  signal rd_ring_buffer_ready      : std_logic := '0';
  signal set_buf_sent              : std_logic := '0';
  signal wr_buffer_sent            : std_logic := '0';
  signal rd_buffer_sent            : std_logic := '0';
  signal wr_buf_sel                : std_logic_vector(log2ceil(CPK_NR_OF_BUFFERS)-1 downto 0) := (others=>'0');
  signal rd_buf_sel                : std_logic_vector(log2ceil(CPK_NR_OF_BUFFERS)-1 downto 0) := (others=>'0');
  signal ch_sel                    : std_logic_vector( 4 downto 0) := (others=>'0');
  signal rd_en                     : std_logic := '0';
  signal rd_init                   : std_logic := '0';
  signal start_of_type             : std_logic := '0';
  signal end_of_type               : std_logic := '0';
  signal start_of_event            : std_logic := '0';
  signal end_of_event              : std_logic := '0';
  signal tx_complete               : std_logic := '0';
  signal data_chunk_offset         : std_logic_vector(15 downto 0) := (others=>'0');
  signal payload_length            : std_logic_vector(15 downto 0) := (others=>'0');
  signal clear_buffer              : std_logic := '0';
  signal store_tx_enables          : std_logic := '0';
  signal ring_buffer_wsel          : std_logic := '0';
  signal drs_buffer_wsel           : std_logic := '0';
  signal drs_buffer_wen            : std_logic := '0';
  signal dataset_tx_samples        : std_logic_vector(15 downto 0) := (others=>'0');

  signal max_samples_per_pkg       : std_logic_vector(17 downto 0) := (others=>'0');
  signal first_pkg_dly             : std_logic_vector(13 downto 0) := (others=>'0');
  signal rst                       : std_logic := '0';
  signal hdr_daq_pll_lock          : std_logic := '0';
  signal hdr_temperature           : std_logic_vector(15 downto 0) := (others=>'0');
  signal drs_trigger_cell_we       : std_logic := '0';
  signal hdr_zero_suppression_mask : std_logic_vector(15 downto 0) := (others=>'0');
  signal zero_suppression_inhibit  : std_logic := '0';

begin

  sync_vectors_to_daq_clk : cdc_sync
  GENERIC MAP (
    CGN_DATA_WIDTH          => 32,
    CGN_USE_INPUT_REG_A     => 0,
    CGN_USE_OUTPUT_REG_A    => 0,
    CGN_USE_GRAY_CONVERSION => 0,
    CGN_NUM_SYNC_REGS_B     => 2,
    CGN_NUM_OUTPUT_REGS_B   => 0,
    CGN_GRAY_TO_BIN_STYLE   => 0
  )
  PORT MAP (
    CLK_A_I => DAQ_CLK_I,
    CLK_B_I => DAQ_CLK_I,
    PORT_A_I(15 downto 0)  => HDR_TEMPERATURE_I,
    PORT_A_I(16)           => RST_I,
    PORT_A_I(17)           => HDR_DAQ_PLL_LOCK_I,
    PORT_A_I(31 downto 18) => FIRST_PKG_DLY_I,
    PORT_B_O(15 downto 0)  => hdr_temperature,
    PORT_B_O(16)           => rst,
    PORT_B_O(17)           => hdr_daq_pll_lock,
    PORT_B_O(31 downto 18) => first_pkg_dly
  );

  ETH_COM_EN_O    <= ETH_COM_EN_I;
  SERDES_COM_EN_O <= SERDES_COM_EN_I;
  com_enable      <= ETH_DST_VALID_I or not(ETH_COM_EN_I);

  rd_buffer_ready <= rd_drs_buffer_ready and rd_ring_buffer_ready;

  drs_buffer_wen   <= DATA_SERDES_DRS_WE_I and drs_buffer_wsel;

  WRITE_CONTROL : entity wd2_pkg_gen_v2_00_a.wd2_pkg_gen_write_control
  port map
  (
    RST_FLAGS_O         => clear_buffer,
    BUFFER_SEL_O        => wr_buf_sel,
    RING_BUF_READY_I    => wr_ring_buffer_ready,
    DRS_BUF_READY_I     => wr_drs_buffer_ready,
    BUFFER_ALL_SENT_I   => wr_buffer_sent,
    RINGBUF_WSEL_O      => ring_buffer_wsel,
    DRS_BUF_WSEL_O      => drs_buffer_wsel,
    CAPTURE_DATA_I      => CAPTURE_DATA_I,
    BUSY_O              => BUSY_O,
    ENABLE_I            => com_enable,
    CLK_I               => DAQ_CLK_I,
    RST_I               => rst
  );

  READ_CONTROL : entity wd2_pkg_gen_v2_00_a.wd2_pkg_gen_read_control
  port map
  (
    HEADER_REN_O          => header_ren,
    HEADER_LOAD_O         => header_load,
    START_OF_HEADER_I     => start_of_header,
    END_OF_HEADER_I       => end_of_header,
    BUFFER_SEL_O          => rd_buf_sel,
    BUFFER_READY_I        => rd_buffer_ready,
    BUFFER_SENT_I         => rd_buffer_sent,
    SET_BUFFER_SENT_O     => set_buf_sent,
    BUFFER_REN_O          => rd_en,
    BUFFER_READ_INIT_O    => rd_init,
    STORE_TX_PARAMS_O     => store_tx_enables,
    DATASET_TX_SAMPLES_I  => dataset_tx_samples,
    ITEM_TX_EN_I          => item_tx_en,
    LAST_TYPE_I           => last_type,
    LAST_CH_I             => last_ch,
    DATA_VALID_I          => data_valid,
    SRC_RDY_N_O           => LL32_O_SRC_RDY_N_O,
    DST_RDY_N_I           => LL32_O_DST_RDY_N_I,
    PKG_TYPE_O            => tx_pkg_type,
    CH_SEL_O              => ch_sel,
    DATA_CHUNK_OFFSET_O   => data_chunk_offset,
    PAYLOAD_LENGTH_O      => payload_length,
    START_OF_PACKET_O     => start_of_frame,
    START_OF_TYPE_O       => start_of_type,
    START_OF_EVENT_O      => start_of_event,
    END_OF_PACKET_O       => end_of_frame,
    END_OF_TYPE_O         => end_of_type,
    END_OF_EVENT_O        => end_of_event,
    TX_COMPLETE_O         => tx_complete,
    MAX_PAYLOAD_SAMPLES_I => max_samples_per_pkg,
    FIRST_PKG_DLY_I       => first_pkg_dly,
    ENABLE_I              => com_enable,
    CLK_I                 => DAQ_CLK_I,
    RST_I                 => rst
  );

  drs_trigger_cell_we <= HDR_DRS_TRIGGER_CELL_VALID_I and drs_buffer_wsel;

  HEADER_BUFFER : entity wd2_pkg_gen_v2_00_a.wd2_pkg_gen_header_buffer
  port map
  (
    RD_EN_I                    => header_ren,
    LOAD_I                     => header_load,
    START_OF_HEADER_O          => start_of_header,
    END_OF_HEADER_O            => end_of_header,
    SET_BUFFER_SENT_I          => set_buf_sent,
    HEADER_CLR_I               => CAPTURE_DATA_I,
    DATA_TYPE_I                => tx_pkg_type,
    CH_SEL_I                   => ch_sel,
    DATA_CHUNK_OFFSET_I        => data_chunk_offset,
    PAYLOAD_LENGTH_I           => payload_length,
    START_OF_TYPE_I            => start_of_type,
    END_OF_TYPE_I              => end_of_type,
    START_OF_EVENT_I           => start_of_event,
    END_OF_EVENT_I             => end_of_event,
    PROTOCOL_VERSION_O         => HDR_PROTOCOL_VERSION_O,
    BOARD_TYPE_I               => HDR_BOARD_TYPE_I,
    BOARD_REVISION_I           => HDR_BOARD_REVISION_I,
    CRATE_ID_I                 => HDR_CRATE_ID_I,
    SLOT_ID_I                  => HDR_SLOT_ID_I,
    SERIAL_NUMBER_I            => HDR_SERIAL_NUMBER_I,
    CH_TX_EN_I                 => type_tx_en,
    ZERO_SUPPRESSION_MASK_I    => hdr_zero_suppression_mask,
    ZERO_SUPPRESSION_EN_I      => ZERO_SUPPRESSION_EN_I,
    ZERO_SUPPR_INHIBIT_STORE_I => ZERO_SUPPR_INHIBIT_STORE_I,
    ZERO_SUPPRESSION_INHIBIT_I => ZERO_SUPPRESSION_INHIBIT_I,
    ZERO_SUPPRESSION_INHIBIT_O => zero_suppression_inhibit,
    BOARD_VARIANT_I            => HDR_BOARD_VARIANT_I,
    TRIGGER_INFO_PERR_I        => HDR_TRIG_INFO_PERR_I,
    TRIGGER_INFO_NEW_I         => HDR_TRIG_INFO_NEW_I,
    DAQ_PLL_LOCK_I             => hdr_daq_pll_lock,
    DRS_PLL_LOCK_I             => HDR_DRS_PLL_LOCK_I,
    DRS_DATA_VALID_I           => DRS_DATA_VALID_I,
    TRIGGER_SOURCE_I           => "0000",
    CH_EVENT_SAMPLES_DRS_I     => DRS_TX_SAMPLES_I,
    CH_EVENT_SAMPLES_ADC_I     => ADC_TX_SAMPLES_I,
    CH_EVENT_SAMPLES_TDC_I     => TDC_TX_SAMPLES_I,
    CH_EVENT_SAMPLES_TRG_I     => TRG_TX_SAMPLES_I,
    TRIGGER_INFO_I             => HDR_TRIGGER_INFO_I,
    EVENT_NUMBER_I             => HDR_EVENT_NUMBER_I,
    TIMESTAMP_I                => HDR_TIMESTAMP_I,
    SAMPLING_FREQ_DRS_I        => HDR_SAMPLING_FREQ_DRS_I,
    SAMPLING_FREQ_ADC_I        => HDR_SAMPLING_FREQ_ADC_I,
    SAMPLING_FREQ_TDC_I        => HDR_SAMPLING_FREQ_TDC_I,
    SAMPLING_FREQ_TRG_I        => HDR_SAMPLING_FREQ_TRG_I,
    DRS_TRIGGER_CELL_WE_I      => drs_trigger_cell_we,
    DRS_TRIGGER_CELL_I         => HDR_DRS_TRIGGER_CELL_I,
    TEMPERATURE_I              => hdr_temperature,
    DAC_OFS_I                  => HDR_DAC_OFS_I,
    DAC_ROFS_I                 => HDR_DAC_ROFS_I,
    DAC_PZC_I                  => HDR_DAC_PZC_I,
    FRONTEND_SETTINGS_I        => HDR_FRONTEND_SETTINGS_I,
    DATA_O                     => header_tx_data,
    WR_BUF_SEL_I               => wr_buf_sel,
    RD_BUF_SEL_I               => rd_buf_sel,
    CAPTURE_DATA_I             => CAPTURE_DATA_I,
    CLK_I                      => DAQ_CLK_I,
    RST_I                      => rst
  );

  DATA_BUFFER : entity wd2_pkg_gen_v2_00_a.wd2_pkg_gen_event_buffer
  port map
  (
    DATA_O                     => buffer_tx_data,
    DATA_SIZE_O                => data_size,
    DATA_VALID_O               => data_valid,
    WR_DRS_BUFFER_READY_O      => wr_drs_buffer_ready,
    RD_DRS_BUFFER_READY_O      => rd_drs_buffer_ready,
    WR_RING_BUFFER_READY_O     => wr_ring_buffer_ready,
    RD_RING_BUFFER_READY_O     => rd_ring_buffer_ready,
    CLR_BUFFER_I               => clear_buffer,
    WR_BUFFER_SENT_O           => wr_buffer_sent,
    RD_BUFFER_SENT_O           => rd_buffer_sent,
    SET_BUFFER_SENT_I          => set_buf_sent,
    CLR_BUFFER_SENT_I          => clear_buffer,
    STORE_TX_ENABLE_I          => store_tx_enables,
    STORE_ZERO_SUPPRESSION_I   => CAPTURE_DATA_I,
    WR_BUF_SEL_I               => wr_buf_sel,
    RD_BUF_SEL_I               => rd_buf_sel,
    RD_DATA_TYPE_SEL_I         => tx_pkg_type,
    RD_CH_SEL_I                => ch_sel,
    RD_EN_I                    => rd_en,
    RD_INIT_I                  => rd_init,
    DRS_ADC_DATA_I             => ADC_DATA_I,
    DRS_BUF_WEN_I              => drs_buffer_wen,
    ADC_BUF_WEN_I              => ring_buffer_wsel,
    ADC_SAMPLING_DIV_I         => ADC_SAMPLING_DIV_I,
    TDC_DATA_I                 => TDC_DATA_I,
    TDC_BUF_WEN_I              => ring_buffer_wsel,
    TRG_DATA_I                 => TRG_DATA_I,
    TRG_BUF_WEN_I              => ring_buffer_wsel,
    SCL_DATA_I                 => SCL_DATA_I,
    SCL_BUF_WEN_I              => CAPTURE_DATA_I,
    DRS_CH_TX_EN_I             => DRS_CH_TX_EN_I,
    ADC_CH_TX_EN_I             => ADC_CH_TX_EN_I,
    TDC_CH_TX_EN_I             => TDC_CH_TX_EN_I,
    TRG_TX_EN_I                => TRG_TX_EN_I,
    SCL_TX_EN_I                => SCL_TX_EN_I,
    ZERO_SUPPRESSION_EN_I      => ZERO_SUPPRESSION_EN_I,
    ZERO_SUPPRESSION_INHIBIT_I => zero_suppression_inhibit,
    ZERO_SUPPRESSION_MASK_I    => ZERO_SUPPRESSION_MASK_I,
    ZERO_SUPPRESSION_MASK_O    => hdr_zero_suppression_mask,
    TYPE_TX_EN_O               => type_tx_en,
    ITEM_TX_EN_O               => item_tx_en,
    DRS_TX_SAMPLES_I           => DRS_TX_SAMPLES_I,
    ADC_TX_SAMPLES_I           => ADC_TX_SAMPLES_I,
    TDC_TX_SAMPLES_I           => TDC_TX_SAMPLES_I,
    TRG_TX_SAMPLES_I           => TRG_TX_SAMPLES_I,
    DATASET_TX_SAMPLES_O       => dataset_tx_samples,
    DRS_MAX_SAMPLES_PER_PKG_I  => DRS_MAX_SAMPLES_PER_PKG_I,
    ADC_MAX_SAMPLES_PER_PKG_I  => ADC_MAX_SAMPLES_PER_PKG_I,
    TDC_MAX_SAMPLES_PER_PKG_I  => TDC_MAX_SAMPLES_PER_PKG_I,
    TRG_MAX_SAMPLES_PER_PKG_I  => TRG_MAX_SAMPLES_PER_PKG_I,
    SCL_MAX_SAMPLES_PER_PKG_I  => SCL_MAX_SAMPLES_PER_PKG_I,
    MAX_SAMPLES_PER_PKG_O      => max_samples_per_pkg,
    LAST_TYPE_O                => last_type,
    LAST_CH_O                  => last_ch,
    CLK_I                      => DAQ_CLK_I,
    RST_I                      => rst
  );

  TX_RATE_COUNT : entity wd2_pkg_gen_v2_00_a.event_tx_rate_counter
  generic map(
    CGN_TICK_TIME_NS  =>  1.0e9,
    CGN_CLK_PERIOD_NS => 12.5
  )
  port map(
    EVENT_TX_RATE_O => EVENT_TX_RATE_O,
    EVENT_TX_DONE_I => tx_complete,
    RST_I           => rst,
    CLK_I           => DAQ_CLK_I
  );

  ---------------------------------------------------------------------
  -- Output Interface
  ---------------------------------------------------------------------
  side_data <= start_of_frame & end_of_frame & start_of_header & end_of_header & "00";
  tx_data   <= "01" & x"00" & header_tx_data when header_ren = '1' else data_size & buffer_tx_data;
  LL32_O_DATA_O   <= side_data & tx_data;
  LL32_O_SOF_N_O  <= not start_of_frame;
  LL32_O_EOF_N_O  <= not end_of_frame;

end struct;
