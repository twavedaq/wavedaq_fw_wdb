---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  WaveDream2 Package Generator Write Control
--
--  Project :  WaveDream2
--
--  PCB  :  DRS4 WaveDream Board II (121202C)
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  11.08.2017 11:08:34
--
--  Description : Buffer write controller for the package generator of the
--                WaveDream2 (WD2) board.
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
--use IEEE.STD_LOGIC_MISC.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

library wd2_pkg_gen_v2_00_a;
use wd2_pkg_gen_v2_00_a.wd2_pkg_gen_cfg_package.all;

entity wd2_pkg_gen_write_control is
port
(
  -- Buffer Interface
  RST_FLAGS_O         : out std_logic;
  BUFFER_SEL_O        : out std_logic_vector(log2ceil(CPK_NR_OF_BUFFERS)-1 downto 0);
  RING_BUF_READY_I    : in  std_logic;
  DRS_BUF_READY_I     : in  std_logic;
  BUFFER_ALL_SENT_I   : in  std_logic;
  RINGBUF_WSEL_O      : out std_logic;
  DRS_BUF_WSEL_O      : out std_logic;

  -- Write Control
  CAPTURE_DATA_I      : in  std_logic;
  BUSY_O              : out std_logic;
  ENABLE_I            : in  std_logic;
  CLK_I               : in  std_logic;
  RST_I               : in  std_logic
);
end wd2_pkg_gen_write_control;

architecture behavioral of wd2_pkg_gen_write_control is

  type type_state is (s_idle, s_wait_buf_rdy, s_ringbuf_rec, s_drs_data_write, s_wait_trigger_info, s_select_next);
  signal state         : type_state;

  attribute fsm_encoding : string;
  attribute fsm_encoding of state : signal is "one-hot";

  signal buff_sel      : std_logic_vector(log2ceil(CPK_NR_OF_BUFFERS)-1 downto 0) := (others=>'0');

begin

  BUFFER_SEL_O <= buff_sel;

  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if RST_I = '1' then
        RST_FLAGS_O    <= '0';
        RINGBUF_WSEL_O <= '0';
        DRS_BUF_WSEL_O <= '0';
        BUSY_O         <= '1';
        buff_sel       <= (others=>'0');
        state <= s_idle;
      else
        -- defaults
        RST_FLAGS_O    <= '0';
        RINGBUF_WSEL_O <= '0';
        DRS_BUF_WSEL_O <= '0';
        BUSY_O         <= '1';

        case state is
        when s_idle =>
          if BUFFER_ALL_SENT_I = '1' and ENABLE_I = '1' then
            RST_FLAGS_O <= '1';
            state <= s_wait_buf_rdy;
          end if;

          when s_wait_buf_rdy =>
          if RING_BUF_READY_I = '0' then
            state <= s_ringbuf_rec;
          end if;

          when s_ringbuf_rec =>
          RINGBUF_WSEL_O <= '1';
          if RING_BUF_READY_I = '1' then
            BUSY_O <= '0';
            if CAPTURE_DATA_I = '1' then
              state <= s_drs_data_write;
            end if;
          end if;

        when s_drs_data_write =>
          DRS_BUF_WSEL_O <= '1';
          if DRS_BUF_READY_I = '1' then
            state <= s_select_next;
          end if;

        when s_select_next =>
          buff_sel <= buff_sel + 1;
          state <= s_idle;

        when others =>
          state <= s_idle;
        end case;
      end if;
    end if;
  end process;

end behavioral;
