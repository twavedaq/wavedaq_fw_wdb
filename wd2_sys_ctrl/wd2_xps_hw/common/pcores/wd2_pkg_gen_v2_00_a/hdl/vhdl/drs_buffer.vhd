---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  WaveDream2 DRS Data Buffer
--
--  Project :  WaveDream2
--
--  PCB  :  DRS4 WaveDream Board II (121202E)
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  18.10.2017 11:09
--
--  Description : Synchronous DRS data buffer.
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
--use IEEE.STD_LOGIC_MISC.ALL;

library UNISIM;
use UNISIM.VComponents.all;

--library psi_3205_v1_00_a;
--use psi_3205_v1_00_a.cdc_package.all;

library wd2_pkg_gen_v2_00_a;
use wd2_pkg_gen_v2_00_a.wd2_pkg_gen_cfg_package.all;
use wd2_pkg_gen_v2_00_a.bram_4kx12_2kx24;

entity drs_data_buffer is
generic
(
  CGN_BUFFER_SELECT_WIDTH  : integer :=  2;
  CGN_READ_CH_SELECT_WIDTH : integer :=  5;
  CGN_NR_OF_DRS_CHANNELS   : integer := 18;
  CGN_NR_OF_ADC_CHANNELS   : integer := 16;
  CGN_WR_ADDR_WIDTH        : integer := 10;
  CGN_RD_ADDR_WIDTH        : integer :=  9;
  CGN_WR_CH_DATA_WIDTH     : integer := 12;
  CGN_RD_CH_DATA_WIDTH     : integer := 24
);
port
(
  -- Write
  WR_BUF_SEL_I     : in  std_logic_vector(CGN_BUFFER_SELECT_WIDTH-1 downto 0);
  CLR_BUF_I        : in  std_logic;
  WR_EN_I          : in  std_logic;
  DATA_I           : in  std_logic_vector(CGN_NR_OF_ADC_CHANNELS*CGN_WR_CH_DATA_WIDTH-1 downto 0);
  
  -- Read
  RD_BUF_SEL_I     : in  std_logic_vector(CGN_BUFFER_SELECT_WIDTH-1 downto 0);
  RD_CH_SEL_I      : in  std_logic_vector(CGN_READ_CH_SELECT_WIDTH-1 downto 0);
  RD_INIT_I        : in  std_logic;
  RD_EN_I          : in  std_logic;
  DATA_VALID_O     : out std_logic;
  DATA_O           : out std_logic_vector(CGN_RD_CH_DATA_WIDTH-1 downto 0);
  
  -- Flags
  BUF_0TO7_READY_O : out std_logic_vector(2**CGN_BUFFER_SELECT_WIDTH-1 downto 0);
  BUF_8_READY_O    : out std_logic_vector(2**CGN_BUFFER_SELECT_WIDTH-1 downto 0);
  
  CLK_I            : in  std_logic;
  RST_I            : in  std_logic
);
end drs_data_buffer;

architecture behavioral of drs_data_buffer is

  type data_in_type  is array (CGN_NR_OF_DRS_CHANNELS-1 downto 0) of std_logic_vector(CGN_WR_CH_DATA_WIDTH-1 downto 0);
  type data_out_type is array (CGN_NR_OF_DRS_CHANNELS-1 downto 0) of std_logic_vector(CGN_RD_CH_DATA_WIDTH-1 downto 0);
  signal buf_din        : data_in_type  := (others=>(others=>'0'));
  signal buf_dout       : data_out_type := (others=>(others=>'0'));

  signal buf_wen        : std_logic_vector(CPK_NR_OF_ADCS*CPK_CHNLS_PER_DRS-1 downto 0) := (others=>'0');

  signal wr_adr         : std_logic_vector(CGN_WR_ADDR_WIDTH-1 downto 0) := (others=>'0');
  signal rd_adr         : std_logic_vector(CGN_RD_ADDR_WIDTH-1 downto 0) := (others=>'0');
  signal buf_adr_a      : std_logic_vector(CGN_BUFFER_SELECT_WIDTH+CGN_WR_ADDR_WIDTH-1 downto 0) := (others=>'0');
  signal buf_adr_b      : std_logic_vector(CGN_BUFFER_SELECT_WIDTH+CGN_RD_ADDR_WIDTH-1 downto 0) := (others=>'0');

  signal buf_0to7_ready : std_logic_vector(2**CGN_BUFFER_SELECT_WIDTH-1 downto 0) := (others=>'0');
  signal buf_8_ready    : std_logic_vector(2**CGN_BUFFER_SELECT_WIDTH-1 downto 0) := (others=>'0');
  signal data_valid     : std_logic := '0';

  signal wr_buf_sel    : std_logic_vector(CGN_BUFFER_SELECT_WIDTH-1 downto 0) := (others=>'0');

  signal ren_s          : std_logic := '0';
  signal data_s         : std_logic_vector(CGN_RD_CH_DATA_WIDTH-1 downto 0) := (others=>'0');
  signal data_valid_s   : std_logic := '0';

begin
  
  BUF_0TO7_READY_O <= buf_0to7_ready;
  BUF_8_READY_O    <= buf_8_ready;
  
  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      wr_buf_sel <= WR_BUF_SEL_I;
    end if;
  end process;  

  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if RST_I = '1' then
        wr_adr <= (others=>'0');
        buf_0to7_ready <= (others=>'0');
        buf_8_ready    <= (others=>'0');
      else
        if CLR_BUF_I = '1' then
          wr_adr <= (others=>'0');
          buf_0to7_ready(CONV_INTEGER(WR_BUF_SEL_I)) <= '0';
          buf_8_ready(CONV_INTEGER(WR_BUF_SEL_I))    <= '0';
        elsif WR_EN_I = '1' then
          wr_adr <= wr_adr + 1;
          if wr_adr = "1111111111" then
            if buf_0to7_ready(CONV_INTEGER(WR_BUF_SEL_I)) = '0' then
              buf_0to7_ready(CONV_INTEGER(WR_BUF_SEL_I)) <= '1';
            else
              buf_8_ready(CONV_INTEGER(WR_BUF_SEL_I))    <= '1';
            end if;
          end if;
        end if;
      end if;
    end if;
  end process;

  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if RD_INIT_I = '1' or RST_I = '1' then
        data_valid <= '0';
        rd_adr     <= (others=>'0');
      elsif RD_EN_I = '1' then
        data_valid <= '1';
        rd_adr     <= rd_adr + 1;
      end if;
    end if;
  end process;

  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      ren_s        <= RD_EN_I;
      data_valid_s <= data_valid;
      if RD_INIT_I = '1' then
        DATA_VALID_O <= '0';
        DATA_O       <= (others=>'0');
      elsif RD_EN_I = '1' and ren_s = '0' then
        DATA_VALID_O <= data_valid_s;
        DATA_O       <= data_s;
      elsif RD_EN_I = '1' then
        DATA_VALID_O <= data_valid;
        DATA_O       <= buf_dout(CONV_INTEGER(RD_CH_SEL_I));
      end if;
      if ren_s = '1' then
        data_s <= buf_dout(CONV_INTEGER(RD_CH_SEL_I));
      end if;
    end if;
  end process;

  buf_adr_a <= WR_BUF_SEL_I & wr_adr;
  buf_adr_b <= RD_BUF_SEL_I & rd_adr;

  DRS_DATA_CH_BUFFERS : for ch in 0 to CGN_NR_OF_DRS_CHANNELS-1 generate

    process(DATA_I, WR_BUF_SEL_I, WR_EN_I, buf_0to7_ready)
    begin
      if (ch > 15) then
        buf_din(ch) <= DATA_I((ch-15)*8*CGN_WR_CH_DATA_WIDTH-1 downto ((ch-15)*8-1)*CGN_WR_CH_DATA_WIDTH);
        buf_wen(ch) <= WR_EN_I and not(buf_8_ready(CONV_INTEGER(WR_BUF_SEL_I))) and buf_0to7_ready(CONV_INTEGER(WR_BUF_SEL_I));
      else
        buf_din(ch) <= DATA_I((ch+1)*CGN_WR_CH_DATA_WIDTH-1 downto ch*CGN_WR_CH_DATA_WIDTH);
        buf_wen(ch) <= WR_EN_I and not(buf_0to7_ready(CONV_INTEGER(WR_BUF_SEL_I)));
      end if;
    end process;
    
    bram_4kx12_2kx24_inst : entity wd2_pkg_gen_v2_00_a.bram_4kx12_2kx24
    port map
    (
      WR_A_DATA_I => buf_din(ch),
      WR_A_ADDR_I => buf_adr_a,
      WR_A_EN_I   => buf_wen(ch),
      RD_B_DATA_O => buf_dout(ch),
      RD_B_ADDR_I => buf_adr_b,
      CLK_I       => CLK_I
    );
    
  end generate DRS_DATA_CH_BUFFERS;
  
end behavioral;
