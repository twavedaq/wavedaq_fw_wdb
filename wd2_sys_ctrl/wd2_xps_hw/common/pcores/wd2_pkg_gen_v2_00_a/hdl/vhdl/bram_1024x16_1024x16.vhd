---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  WaveDream2 BRAM instance 1kx16 1kx16
--
--  Project :  WaveDream2
--
--  PCB  :  DRS4 WaveDream Board II (121202E)
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  12.01.2018 11:01:34
--
--  Description : Block RAM instance for event buffer in WaveDream2.
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.STD_LOGIC_MISC.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

entity bram_1024x16_1024x16 is
port
(
  -- Write
  WR_A_DATA_I : in  std_logic_vector(15 downto 0);
  WR_A_ADDR_I : in  std_logic_vector( 9 downto 0);
  WR_A_EN_I   : in  std_logic;

  -- Read
  RD_B_DATA_O : out std_logic_vector(15 downto 0);
  RD_B_ADDR_I : in  std_logic_vector( 9 downto 0);

  CLK_I       : in  std_logic
);
end bram_1024x16_1024x16;

architecture behavioral of bram_1024x16_1024x16 is

  constant C_DWIDTH_IN  : integer := 16;
  constant C_DWIDTH_OUT : integer := 16;

begin

  -- buffer
  BRAM_1024x2_1024x2 : entity psi_3205_v1_00_a.ram_dp
  generic map
  (
    CGN_DATA_WIDTH_A  => C_DWIDTH_IN,
    CGN_ADDR_WIDTH_A  => 10,
    CGN_READ_PORT_A   => false,
    CGN_WRITE_PORT_A  => true,
    CGN_READ_FIRST_A  => false,
    CGN_USE_OUTREG_A  => false,
    CGN_DATA_WIDTH_B  => C_DWIDTH_OUT,
    CGN_ADDR_WIDTH_B  => 10,
    CGN_READ_PORT_B   => true,
    CGN_WRITE_PORT_B  => false,
    CGN_READ_FIRST_B  => true,
    CGN_USE_OUTREG_B  => false,
    CGN_RAM_STYLE     => "block"
  )
  port map
  (
    PA_CLK_I   => CLK_I,
    PA_EN_I    => '1',
    PA_ADDR_I  => WR_A_ADDR_I,
    PA_DATA_I  => WR_A_DATA_I,
    PA_WR_EN_I => WR_A_EN_I,
    PA_DATA_O  => open,
    PB_CLK_I   => CLK_I,
    PB_EN_I    => '1',
    PB_ADDR_I  => RD_B_ADDR_I,
    PB_DATA_I  => (others=>'0'),
    PB_WR_EN_I => '0',
    PB_DATA_O  => RD_B_DATA_O
  );

end behavioral;
