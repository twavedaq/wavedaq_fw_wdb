---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  WaveDream2 BRAM instance 2kx64 8kx16
--
--  Project :  WaveDream2
--
--  PCB  :  DRS4 WaveDream Board II (121202E)
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  07.11.2017 15:59:11
--
--  Description : Block RAM instance for event buffer in WaveDream2.
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.STD_LOGIC_MISC.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

entity bram_2kx64_8kx16 is
port
(
  -- Write
  WR_A_DATA_I : in  std_logic_vector(63 downto 0);
  WR_A_ADDR_I : in  std_logic_vector(10 downto 0);
  WR_A_EN_I   : in  std_logic;

  -- Read
  RD_B_DATA_O : out std_logic_vector(15 downto 0);
  RD_B_ADDR_I : in  std_logic_vector(12 downto 0);
  
  CLK_I       : in  std_logic
);
end bram_2kx64_8kx16;

architecture behavioral of bram_2kx64_8kx16 is
  
  constant C_DWIDTH_IN  : integer := 8;
  constant C_DWIDTH_OUT : integer := 8;

  signal rd_b_data      : std_logic_vector(63 downto 0) := (others => '0');
  signal rd_b_addr      : std_logic_vector(10 downto 0) := (others => '0');
  signal mux_addr       : std_logic_vector( 1 downto 0) := (others => '0');
  
begin

  process(mux_addr, rd_b_data)
  begin
    case mux_addr is
      when "00" => RD_B_DATA_O <= rd_b_data(63 downto 48);
      when "01" => RD_B_DATA_O <= rd_b_data(47 downto 32);
      when "10" => RD_B_DATA_O <= rd_b_data(31 downto 16);
      when "11" => RD_B_DATA_O <= rd_b_data(15 downto  0);
      when others => null;
    end case;
  end process;

  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      mux_addr <= RD_B_ADDR_I( 1 downto 0);
    end if;
  end process;
  
  rd_b_addr <= RD_B_ADDR_I(12 downto 2);
  
  BRAM_inst : for i in 7 downto 0 generate
    -- buffers
    BRAM_2kx8_2kx8 : entity psi_3205_v1_00_a.ram_dp
    generic map
    (
      CGN_DATA_WIDTH_A  => C_DWIDTH_IN,
      CGN_ADDR_WIDTH_A  => 11,
      CGN_READ_PORT_A   => false,
      CGN_WRITE_PORT_A  => true,
      CGN_READ_FIRST_A  => false,
      CGN_USE_OUTREG_A  => false,
      CGN_DATA_WIDTH_B  => C_DWIDTH_OUT,
      CGN_ADDR_WIDTH_B  => 11,
      CGN_READ_PORT_B   => true,
      CGN_WRITE_PORT_B  => false,
      CGN_READ_FIRST_B  => true,
      CGN_USE_OUTREG_B  => false,
      CGN_RAM_STYLE     => "block"
    )
    port map
    (
      PA_CLK_I   => CLK_I,
      PA_EN_I    => '1',
      PA_ADDR_I  => WR_A_ADDR_I,
      PA_DATA_I  => WR_A_DATA_I((i+1)*C_DWIDTH_IN-1 downto i*C_DWIDTH_IN),
      PA_WR_EN_I => WR_A_EN_I,
      PA_DATA_O  => open,
      PB_CLK_I   => CLK_I,
      PB_EN_I    => '1',
      PB_ADDR_I  => rd_b_addr,
      PB_DATA_I  => (others=>'0'),
      PB_WR_EN_I => '0',
      PB_DATA_O  => rd_b_data((i+1)*C_DWIDTH_OUT-1 downto i*C_DWIDTH_OUT)
    );
  end generate;
  
end behavioral;
