---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  WaveDream2 Counter Buffer
--
--  Project :  WaveDream2
--
--  PCB  :  DRS4 WaveDream Board II (121202E)
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  20.03.2018 08:29
--
--  Description : Counter data buffer for trigger rate.
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
--use IEEE.STD_LOGIC_MISC.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

library plb_scaler_v2_00_a;
use plb_scaler_v2_00_a.ipif_user_cfg.all;

entity counter_buffer is
generic
(
  CGN_MEM_DATA_WIDTH : integer := 32;
  CGN_MEM_ADDR_WIDTH : integer := 8;
  CGN_NR_OF_VALUES   : integer := 20;
  CGN_VALUE_WIDTH    : integer := 64;
  CGN_BUF_SEL_WIDTH  : integer := 2;
  CGN_CLK_PERIOD_NS  : real    := 12.5
);
port
(
  DATA_I           : in  std_logic_vector(CGN_NR_OF_VALUES*CGN_VALUE_WIDTH-1 downto 0);
  BUFFER_LOCKED_I  : in  std_logic;
  MEM_DATA_O       : out std_logic_vector(CGN_MEM_DATA_WIDTH-1 downto 0);
  MEM_ADDR_O       : out std_logic_vector(CGN_MEM_ADDR_WIDTH-1 downto 0);
  MEM_WR_EN_O      : out std_logic;
  LAST_BUFFER_O    : out std_logic_vector(CGN_BUF_SEL_WIDTH-1 downto 0);
  CLK_I            : in  std_logic
);
end counter_buffer;

architecture behavioral of counter_buffer is

  constant C_WR_WORDS_PER_VAL : integer := CGN_VALUE_WIDTH / CGN_MEM_DATA_WIDTH;
  constant C_TOTAL_WR_WORDS   : integer := CGN_NR_OF_VALUES * C_WR_WORDS_PER_VAL;
  constant C_ADDR_WIDTH       : integer := CGN_MEM_ADDR_WIDTH - CGN_BUF_SEL_WIDTH;--log2ceil(C_TOTAL_WR_WORDS);
  
--  constant C_TIMER_LOAD_VAL   : integer := integer(1000000.0/CGN_CLK_PERIOD_NS) - 1;
  constant C_TIMER_LOAD_VAL   : integer := integer(1000000000.0/CGN_CLK_PERIOD_NS) - 1;
  constant C_TIMER_WIDTH      : integer := log2ceil(C_TIMER_LOAD_VAL);

  type buffer_type is array (C_TOTAL_WR_WORDS-1 downto 0) of std_logic_vector(CGN_MEM_DATA_WIDTH-1 downto 0);
  signal buffer_reg      : buffer_type := (others=>(others=>'0'));

  signal timer_count     : std_logic_vector(C_TIMER_WIDTH-1 downto 0) := (others=>'0');
  signal tick            : std_logic := '0';

  signal wr_en           : std_logic := '0';
  signal wr_adr          : std_logic_vector(C_ADDR_WIDTH-1 downto 0)      := (others=>'0');
  signal wr_buf_sel      : std_logic_vector(CGN_BUF_SEL_WIDTH-1 downto 0) := (others=>'0');
  signal rd_buf_sel      : std_logic_vector(CGN_BUF_SEL_WIDTH-1 downto 0) := (others=>'0');
  signal last_buffer     : std_logic_vector(CGN_BUF_SEL_WIDTH-1 downto 0) := (others=>'0');
  signal buffer_locked_s : std_logic := '0';

begin

  -- Tick generator
  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if timer_count = 0 then
        timer_count <= CONV_STD_LOGIC_VECTOR(C_TIMER_LOAD_VAL, timer_count'length);
        tick <= '1';
      else
        timer_count <= timer_count - 1;
        tick <= '0';
      end if;
    end if;
  end process;

  -- writing (shift register structure)
  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if tick = '1' then
        -- load values into shift register
        for i in CGN_NR_OF_VALUES-1 downto 0 loop
          for j in C_WR_WORDS_PER_VAL-1 downto 0 loop
            buffer_reg(i*C_WR_WORDS_PER_VAL+j) <= DATA_I((i*C_WR_WORDS_PER_VAL+(j+1))*CGN_MEM_DATA_WIDTH-1 downto (i*C_WR_WORDS_PER_VAL+j)*CGN_MEM_DATA_WIDTH);
          end loop;
        end loop;
      elsif wr_en = '1' then
        -- shift
        for i in C_TOTAL_WR_WORDS-2 downto 0 loop
          buffer_reg(i) <= buffer_reg(i+1);
        end loop;
        buffer_reg(C_TOTAL_WR_WORDS-1) <= (others=>'0');
      end if;
    end if;
  end process;

  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if wr_en = '1' then
        if wr_adr = CONV_STD_LOGIC_VECTOR(C_TOTAL_WR_WORDS-1, wr_adr'length) then
          wr_en  <= '0';
          wr_adr <= (others=>'0');
          wr_buf_sel  <= wr_buf_sel + 1;
          last_buffer <= wr_buf_sel;
        else
          wr_adr <= wr_adr + 1;
        end if;
      elsif tick = '1' and (BUFFER_LOCKED_I = '0' or wr_buf_sel /= rd_buf_sel) then
        wr_en  <= '1';
        wr_adr <= (others=>'0');
      end if;
    end if;
  end process;

  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      buffer_locked_s <= BUFFER_LOCKED_I;
      if BUFFER_LOCKED_I = '1' and buffer_locked_s = '0' then
        rd_buf_sel <= last_buffer;
      end if;
    end if;
  end process;
  
  LAST_BUFFER_O <= last_buffer;

  -- memory control
  MEM_DATA_O  <= buffer_reg(0);
  MEM_ADDR_O  <= wr_buf_sel & wr_adr;
  MEM_WR_EN_O <= wr_en;

end behavioral;
