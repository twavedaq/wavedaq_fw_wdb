
----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Luca Galli, Marco Francesconi
-- 
-- Create Date:    16:54:27 05/20/2016 
-- Design Name: 
-- Module Name:    main_vhd - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: here all the wfm-based trigger algorithms comprehensive of pedestal subtraction and 
--				waveform calibration and are implemented
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library megtrigger_v1_00_a;
use megtrigger_v1_00_a.common.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity mainwfm is
  Port (
    CLK            : in STD_LOGIC;
    -- ADC data
    DATAIN         : in ADCInputData;
    -- calibration factors
    CAL            : in  CalibData; 
    -- Pedestal subtraction configuration to be configured
    PedTHR         : in  STD_LOGIC_VECTOR  (ADC_INPUT_DIM-1 downto 0); 
    PedDlyEnable   : in  STD_LOGIC_VECTOR  (1 downto 0); 
    PedDlySelect   : in  STD_LOGIC_VECTOR  (3 downto 0); 
    PedAdderSelect : in  STD_LOGIC_VECTOR  (2 downto 0); 
    -- trigger thresholds: use to be assigned...
    SumTHR0        : in  STD_LOGIC_VECTOR  (31 downto 0);
    SumTHR1        : in  STD_LOGIC_VECTOR  (31 downto 0);
    SumTHR2        : in  STD_LOGIC_VECTOR  (31 downto 0);
    -- control bits
    RUNMODE        : in  STD_LOGIC;
    FORCEPED       : in  STD_LOGIC;
    ZEROSUPPR      : in  STD_LOGIC;
    -- trigger bus
    --SYNC           : in STD_LOGIC;
    -- trigger output to serdes
    SUM            : out ADCSummedData;
    MAXWFM         : out ADCMaxValueData;
    MAX            : out STD_LOGIC_VECTOR(3 downto 0);
    ISNOTPED       : out STD_LOGIC;
    -- simple thresholds
    TRG0           : out STD_LOGIC;
    TRG1           : out STD_LOGIC;
    TRG2           : out STD_LOGIC
  );
end mainwfm;

architecture Behavioral of mainwfm is 

   --pedestal waveforms
   SIGNAL Pedestals : ADCInputData;

   -- Cascade PCOUT to PCIN, for internal use only
   TYPE PCascadeData is array (7 downto 0) of STD_LOGIC_VECTOR(47 downto 0);
   SIGNAL PCascade : PCascadeData;

   -- DSP multiplier output, (Data - Pedestal) * Calibration
   TYPE DSPCalibratedData is array (15 downto 0) of STD_LOGIC_VECTOR(35 downto 0);
   SIGNAL DSPRawWaveform : DSPCalibratedData;

   -- DSP sum output, even odd calibrated sum
   TYPE DSPSumData is array (7 downto 0) of STD_LOGIC_VECTOR(47 downto 0);
   SIGNAL FirstOut : DSPSumData;
   SIGNAL FirstSum : ADCFirstSumData;

   --DSP multiplier output corrected by size
   SIGNAL DataCalibrated : ADCCalibratedData;
   SIGNAL MaxInput : STD_LOGIC_VECTOR((DataCalibrated'length)*(DataCalibrated(0)'length)-1 downto 0);

   -- local sum
   SIGNAL Sum_local : ADCSummedData; 

   -- is pedestal from pedestal calculation
   SIGNAL IsPed : STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL FirstIsNotPed : ADCFirstFlagData;
   SIGNAL IsNotPed_local : STD_LOGIC;
begin
-- pedestal and calibration block for each input
  g_pedcal_adc : FOR i IN 0 TO 15 GENERATE
    i_pedcal_adc : entity megtrigger_v1_00_a.onepedcalc PORT MAP (
      DATAIN => DATAIN(i),
      CLK => CLK,
      PedTHR => PedTHR,
      DLY_ENABLE => PedDlyEnable,
      DLY_SELECT => PedDlySelect,
      ADDER_SELECT => PedAdderSelect,
      RUNMODE => RUNMODE,
      DATAOUT => Pedestals(i),
      ISPEDOUT => IsPed(i),
      FORCEPED => FORCEPED
    );
  END GENERATE g_pedcal_adc;

-- generate the DSPs for pedestal subtraction, calibration and first sum
  g_dsp_pair : FOR i IN 0 TO 7 GENERATE
    SIGNAL A0 : STD_LOGIC_VECTOR(17 downto 0);
    SIGNAL B0 : STD_LOGIC_VECTOR(17 downto 0);
    SIGNAL D0 : STD_LOGIC_VECTOR(17 downto 0);
    SIGNAL A1 : STD_LOGIC_VECTOR(17 downto 0);
    SIGNAL B1 : STD_LOGIC_VECTOR(17 downto 0);
    SIGNAL D1 : STD_LOGIC_VECTOR(17 downto 0);
    SIGNAL OPMODE : STD_LOGIC_VECTOR(7 downto 0);
  BEGIN
    A0 <= STD_LOGIC_VECTOR(resize(signed(CAL(2*i)),18));
    B0 <= STD_LOGIC_VECTOR(resize(unsigned(Pedestals(2*i)),18));
    D0 <= STD_LOGIC_VECTOR(resize(unsigned(DATAIN(2*i)),18));
    A1 <= STD_LOGIC_VECTOR(resize(signed(CAL(2*i+1)),18));
    B1 <= STD_LOGIC_VECTOR(resize(unsigned(Pedestals(2*i+1)),18));
    D1 <= STD_LOGIC_VECTOR(resize(unsigned(DATAIN(2*i+1)),18));
    OPMODE <= "0101" & "0" & (not IsPed(2*i+1) or not ZEROSUPPR) & "0" & (not IsPed(2*i) or not ZEROSUPPR);

    i_dps_pair0 : DSP48A1
    generic map (
       A0REG => 0,              -- First stage A input pipeline register (0/1)
       A1REG => 1,              -- Second stage A input pipeline register (0/1)
       B0REG => 0,              -- First stage B input pipeline register (0/1)
       B1REG => 1,              -- Second stage B input pipeline register (0/1)
       CARRYINREG => 0,         -- CARRYIN input pipeline register (0/1)
       CARRYINSEL => "OPMODE5", -- Specify carry-in source, "CARRYIN" or "OPMODE5"
       CARRYOUTREG => 0,        -- CARRYOUT output pipeline register (0/1)
       CREG => 0,               -- C input pipeline register (0/1)
       DREG => 0,               -- D pre-adder input pipeline register (0/1)
       MREG => 0,               -- M pipeline register (0/1)
       OPMODEREG => 0,          -- Enable=1/disable=0 OPMODE input pipeline registers
       PREG => 1,               -- P output pipeline register (0/1)
       RSTTYPE => "SYNC"        -- Specify reset type, "SYNC" or "ASYNC"
    )
    port map (
       -- Cascade Ports: 18-bit (each) output: Ports to cascade from one DSP48 to another
       BCOUT => open,           -- 18-bit output: B port cascade output
       PCOUT => open,           -- 48-bit output: P cascade output (if used, connect to PCIN of another DSP48A1)
       -- Data Ports: 1-bit (each) output: Data input and output ports
       CARRYOUT => open,     -- 1-bit output: carry output (if used, connect to CARRYIN pin of another
                                 -- DSP48A1)

       CARRYOUTF => open,   -- 1-bit output: fabric carry output
       M => open,                   -- 36-bit output: fabric multiplier data output
       P => FirstOut(i),                   -- 48-bit output: data output
       -- Cascade Ports: 48-bit (each) input: Ports to cascade from one DSP48 to another
       PCIN => PCascade(i),             -- 48-bit input: P cascade input (if used, connect to PCOUT of another DSP48A1)
       -- Control Input Ports: 1-bit (each) input: Clocking and operation mode
       CLK => CLK,               -- 1-bit input: clock input
       OPMODE => OPMODE,         -- 8-bit input: operation mode input
       -- Data Ports: 18-bit (each) input: Data input and output ports
       A => A0,                   -- 18-bit input: A data input
       B => B0,                   -- 18-bit input: B data input (connected to fabric or BCOUT of adjacent DSP48A1)
       C => X"000000000000",                   -- 48-bit input: C data input
       CARRYIN => '0',       -- 1-bit input: carry input signal (if used, connect to CARRYOUT pin of another
                                 -- DSP48A1)

       D => D0,                   -- 18-bit input: B pre-adder data input
       -- Reset/Clock Enable Input Ports: 1-bit (each) input: Reset and enable input ports
       CEA => '1',               -- 1-bit input: active high clock enable input for A registers
       CEB => '1',               -- 1-bit input: active high clock enable input for B registers
       CEC => '0',               -- 1-bit input: active high clock enable input for C registers
       CECARRYIN => '0',   -- 1-bit input: active high clock enable input for CARRYIN registers
       CED => '0',               -- 1-bit input: active high clock enable input for D registers
       CEM => '1',               -- 1-bit input: active high clock enable input for multiplier registers
       CEOPMODE => '0',     -- 1-bit input: active high clock enable input for OPMODE registers
       CEP => '1',               -- 1-bit input: active high clock enable input for P registers
       RSTA => '0',             -- 1-bit input: reset input for A pipeline registers
       RSTB => '0',             -- 1-bit input: reset input for B pipeline registers
       RSTC => '0',             -- 1-bit input: reset input for C pipeline registers
       RSTCARRYIN => '0', -- 1-bit input: reset input for CARRYIN pipeline registers
       RSTD => '0',             -- 1-bit input: reset input for D pipeline registers
       RSTM => '0',             -- 1-bit input: reset input for M pipeline registers
       RSTOPMODE => '0',   -- 1-bit input: reset input for OPMODE pipeline registers
       RSTP => '0'              -- 1-bit input: reset input for P pipeline registers
    );
    i_dps_pair1 : DSP48A1
    generic map (
       A0REG => 0,              -- First stage A input pipeline register (0/1)
       A1REG => 1,              -- Second stage A input pipeline register (0/1)
       B0REG => 0,              -- First stage B input pipeline register (0/1)
       B1REG => 1,              -- Second stage B input pipeline register (0/1)
       CARRYINREG => 0,         -- CARRYIN input pipeline register (0/1)
       CARRYINSEL => "OPMODE5", -- Specify carry-in source, "CARRYIN" or "OPMODE5"
       CARRYOUTREG => 0,        -- CARRYOUT output pipeline register (0/1)
       CREG => 0,               -- C input pipeline register (0/1)
       DREG => 0,               -- D pre-adder input pipeline register (0/1)
       MREG => 0,               -- M pipeline register (0/1)
       OPMODEREG => 0,          -- Enable=1/disable=0 OPMODE input pipeline registers
       PREG => 0,               -- P output pipeline register (0/1)
--       PREG => 1,               -- P output pipeline register (0/1)
       RSTTYPE => "SYNC"        -- Specify reset type, "SYNC" or "ASYNC"
    )
    port map (
       -- Cascade Ports: 18-bit (each) output: Ports to cascade from one DSP48 to another
       BCOUT => open,           -- 18-bit output: B port cascade output
       PCOUT => PCascade(i),           -- 48-bit output: P cascade output (if used, connect to PCIN of another DSP48A1)
       -- Data Ports: 1-bit (each) output: Data input and output ports
       CARRYOUT => open,     -- 1-bit output: carry output (if used, connect to CARRYIN pin of another
                                 -- DSP48A1)

       CARRYOUTF => open,   -- 1-bit output: fabric carry output
       M => open,                   -- 36-bit output: fabric multiplier data output
       P => open,                   -- 48-bit output: data output
       -- Cascade Ports: 48-bit (each) input: Ports to cascade from one DSP48 to another
       PCIN => X"000000000000",             -- 48-bit input: P cascade input (if used, connect to PCOUT of another DSP48A1)
       -- Control Input Ports: 1-bit (each) input: Clocking and operation mode
       CLK => CLK,               -- 1-bit input: clock input
       OPMODE => X"51",         -- 8-bit input: operation mode input
       -- Data Ports: 18-bit (each) input: Data input and output ports
       A => A1,                   -- 18-bit input: A data input
       B => B1,                   -- 18-bit input: B data input (connected to fabric or BCOUT of adjacent DSP48A1)
       C => X"000000000000",                   -- 48-bit input: C data input
       CARRYIN => '0',       -- 1-bit input: carry input signal (if used, connect to CARRYOUT pin of another
                                 -- DSP48A1)

       D => D1,                   -- 18-bit input: B pre-adder data input
       -- Reset/Clock Enable Input Ports: 1-bit (each) input: Reset and enable input ports
       CEA => '1',               -- 1-bit input: active high clock enable input for A registers
       CEB => '1',               -- 1-bit input: active high clock enable input for B registers
       CEC => '0',               -- 1-bit input: active high clock enable input for C registers
       CECARRYIN => '0',   -- 1-bit input: active high clock enable input for CARRYIN registers
       CED => '0',               -- 1-bit input: active high clock enable input for D registers
       CEM => '1',               -- 1-bit input: active high clock enable input for multiplier registers
       CEOPMODE => '0',     -- 1-bit input: active high clock enable input for OPMODE registers
       CEP => '0',               -- 1-bit input: active high clock enable input for P registers
       RSTA => '0',             -- 1-bit input: reset input for A pipeline registers
       RSTB => '0',             -- 1-bit input: reset input for B pipeline registers
       RSTC => '0',             -- 1-bit input: reset input for C pipeline registers
       RSTCARRYIN => '0', -- 1-bit input: reset input for CARRYIN pipeline registers
       RSTD => '0',             -- 1-bit input: reset input for D pipeline registers
       RSTM => '0',             -- 1-bit input: reset input for M pipeline registers
       RSTOPMODE => '0',   -- 1-bit input: reset input for OPMODE pipeline registers
       RSTP => '0'              -- 1-bit input: reset input for P pipeline registers
    );

    FirstSum(i) <= FirstOut(i)(FirstSum(i)'length-1 downto 0);

     process(CLK) begin
       if(clk'event and clk = '1') then
          FirstIsNotPed(i) <= (not IsPed(2*i)) or (not IsPed(2*i+1));
       end if;
     end process;
  END GENERATE g_dsp_pair;	 

-- same but without the sum
  g_dsp : FOR i IN 0 TO 15 GENERATE
    SIGNAL A : STD_LOGIC_VECTOR(17 downto 0);
    SIGNAL B : STD_LOGIC_VECTOR(17 downto 0);
    SIGNAL D : STD_LOGIC_VECTOR(17 downto 0);
  BEGIN
    A <= STD_LOGIC_VECTOR(resize(signed(CAL(i)),18));
    B <= STD_LOGIC_VECTOR(resize(unsigned(Pedestals(i)),18));
    D <= STD_LOGIC_VECTOR(resize(unsigned(DATAIN(i)),18));

    i_dps : DSP48A1
    generic map (
       A0REG => 0,              -- First stage A input pipeline register (0/1)
       A1REG => 1,              -- Second stage A input pipeline register (0/1)
       B0REG => 0,              -- First stage B input pipeline register (0/1)
       B1REG => 1,              -- Second stage B input pipeline register (0/1)
       CARRYINREG => 0,         -- CARRYIN input pipeline register (0/1)
       CARRYINSEL => "OPMODE5", -- Specify carry-in source, "CARRYIN" or "OPMODE5"
       CARRYOUTREG => 0,        -- CARRYOUT output pipeline register (0/1)
       CREG => 0,               -- C input pipeline register (0/1)
       DREG => 0,               -- D pre-adder input pipeline register (0/1)
--       MREG => 1,               -- M pipeline register (0/1)
       MREG => 1,               -- M pipeline register (0/1)
       OPMODEREG => 0,          -- Enable=1/disable=0 OPMODE input pipeline registers
       PREG => 0,               -- P output pipeline register (0/1)
       RSTTYPE => "SYNC"        -- Specify reset type, "SYNC" or "ASYNC"
    )
    port map (
       -- Cascade Ports: 18-bit (each) output: Ports to cascade from one DSP48 to another
       BCOUT => open,           -- 18-bit output: B port cascade output
       PCOUT => open,           -- 48-bit output: P cascade output (if used, connect to PCIN of another DSP48A1)
       -- Data Ports: 1-bit (each) output: Data input and output ports
       CARRYOUT => open,     -- 1-bit output: carry output (if used, connect to CARRYIN pin of another
                                 -- DSP48A1)

       CARRYOUTF => open,   -- 1-bit output: fabric carry output
       M => DSPRawWaveform(i),                   -- 36-bit output: fabric multiplier data output
       P => open,                   -- 48-bit output: data output
       -- Cascade Ports: 48-bit (each) input: Ports to cascade from one DSP48 to another
       PCIN => X"000000000000",             -- 48-bit input: P cascade input (if used, connect to PCOUT of another DSP48A1)
       -- Control Input Ports: 1-bit (each) input: Clocking and operation mode
       CLK => CLK,               -- 1-bit input: clock input
       OPMODE => X"51",         -- 8-bit input: operation mode input
       -- Data Ports: 18-bit (each) input: Data input and output ports
       A => A,                   -- 18-bit input: A data input
       B => B,                   -- 18-bit input: B data input (connected to fabric or BCOUT of adjacent DSP48A1)
       C => X"000000000000",                   -- 48-bit input: C data input
       CARRYIN => '0',       -- 1-bit input: carry input signal (if used, connect to CARRYOUT pin of another
                                 -- DSP48A1)

       D => D,                   -- 18-bit input: B pre-adder data input
       -- Reset/Clock Enable Input Ports: 1-bit (each) input: Reset and enable input ports
       CEA => '1',               -- 1-bit input: active high clock enable input for A registers
       CEB => '1',               -- 1-bit input: active high clock enable input for B registers
       CEC => '0',               -- 1-bit input: active high clock enable input for C registers
       CECARRYIN => '0',   -- 1-bit input: active high clock enable input for CARRYIN registers
       CED => '0',               -- 1-bit input: active high clock enable input for D registers
       CEM => '1',               -- 1-bit input: active high clock enable input for multiplier registers
       CEOPMODE => '0',     -- 1-bit input: active high clock enable input for OPMODE registers
       CEP => '1',               -- 1-bit input: active high clock enable input for P registers
       RSTA => '0',             -- 1-bit input: reset input for A pipeline registers
       RSTB => '0',             -- 1-bit input: reset input for B pipeline registers
       RSTC => '0',             -- 1-bit input: reset input for C pipeline registers
       RSTCARRYIN => '0', -- 1-bit input: reset input for CARRYIN pipeline registers
       RSTD => '0',             -- 1-bit input: reset input for D pipeline registers
       RSTM => '0',             -- 1-bit input: reset input for M pipeline registers
       RSTOPMODE => '0',   -- 1-bit input: reset input for OPMODE pipeline registers
       RSTP => '0'              -- 1-bit input: reset input for P pipeline registers
    );

--truncate output at correct length
    DataCalibrated(i) <= STD_LOGIC_VECTOR(resize(signed(DSPRawWaveform(i)), DataCalibrated(i)'length));

  END GENERATE g_dsp;

-- then sum all the samples
  allsumblock : entity megtrigger_v1_00_a.allsum port map(
    din         => FirstSum,
    isnotpedin  => FirstIsNotPed,
    clk         => CLK,
    dout        => Sum_local,
    isnotpedout => IsNotPed_local
  );

  SUM <= Sum_local;
  ISNOTPED <= IsNotPed_local;

-- search max
  max_input_preparation: FOR i in 0 to DataCalibrated'length-1 GENERATE
     MaxInput((i+1)*DataCalibrated(0)'length-1 downto i*DataCalibrated(0)'length) <= DataCalibrated(i);
  END GENERATE;
     
  max_seach : entity megtrigger_v1_00_a.allcompare
  generic map(
    CGN_DATASIZE => DataCalibrated(0)'length,
    CGN_MAXSIZE => 4,
    CGN_SIGNCOMPARE => true
  )
  port map(
    DATA_I => MaxInput,
    CLK => clk,
    DATA_O => MAXWFM,
    MAX_O => MAX
  );

 -- comparator on summed wfms for the trigger
  process(CLK) begin
    if(rising_edge(CLK)) then
      if(signed(Sum_local)>resize(signed(SumTHR0), Sum_local'length)) then
        TRG0 <= '1';
      else 
        TRG0 <= '0';
      end if;
      if(signed(Sum_local)>resize(signed(SumTHR1), Sum_local'length)) then
        TRG1 <= '1';
      else 
        TRG1 <= '0';
      end if;
--      if(signed(Sum_local)>resize(signed(SumTHR2), Sum_local'length)) then
--        TRG2 <= '1';
--      else 
--        TRG2 <= '0';
--      end if;
    end if;
  end process;
--other trigger to be used in the future
  TRG2 <= '0';

end Behavioral;


