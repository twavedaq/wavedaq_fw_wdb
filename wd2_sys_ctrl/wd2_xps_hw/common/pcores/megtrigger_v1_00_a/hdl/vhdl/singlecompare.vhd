----------------------------------------------------------------------------------
-- Company: 
-- Engineer:
-- 
-- Create Date:    16:16:50 21/05/2016 
-- Design Name: 
-- Module Name:    siglecompare - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: this moule does a comparison
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values

Library UNISIM;
use UNISIM.vcomponents.all;

entity singlecompare is
   Generic (
      CGN_MAXSIZE : integer;
      CGN_DATASIZE : integer;
      CGN_CLKPOLARITY : std_logic := '1';
      CGN_SIGNCOMPARE : boolean := false
   );
   Port (
      DATA_A_I : in  std_logic_vector (CGN_DATASIZE-1 downto 0);
      DATA_B_I : in  std_logic_vector (CGN_DATASIZE-1 downto 0);
      CLK      : in  std_logic;
      MAX_A_I  : in  std_logic_vector (CGN_MAXSIZE-1 downto 0);
      MAX_B_I  : in  std_logic_vector (CGN_MAXSIZE-1 downto 0);
      DATA_O   : out std_logic_vector (CGN_DATASIZE-1 downto 0);
      MAX_O    : out std_logic_vector (CGN_MAXSIZE-1 downto 0)
   );
end singlecompare;

architecture Behavioral of singlecompare is
   signal cmp : std_logic;
begin
   signed_compare: if(CGN_SIGNCOMPARE = true) generate
      process(DATA_A_I, DATA_B_I) begin
         if(signed(DATA_A_I) >= signed(DATA_B_I)) then
            cmp <= '1';
         else
            cmp <= '0';
         end if;
      end process;
   end generate;

   unsigned_compare: if(CGN_SIGNCOMPARE = false) generate
      process(DATA_A_I, DATA_B_I) begin
         if(DATA_A_I >= DATA_B_I) then
            cmp <= '1';
         else
            cmp <= '0';
         end if;
      end process;
   end generate;

   process(CLK, CMP) begin
      if(CLK'event and CLK = CGN_CLKPOLARITY) then
         if(cmp = '1') then
            DATA_O <= DATA_A_I;
            MAX_O <= MAX_A_I;
         else
            DATA_O <= DATA_B_I;
            MAX_O <= MAX_B_I;
         end if;
      end if;
   end process;
end Behavioral;

