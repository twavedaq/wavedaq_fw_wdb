----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Marco Francesconi
-- 
-- Create Date:    16:16:50 21/05/2016 
-- Design Name: 
-- Module Name:    allsum - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: this moule search the waveform maximum
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use IEEE.MATH_REAL.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values

Library UNISIM;
use UNISIM.vcomponents.all;

library megtrigger_v1_00_a;
use megtrigger_v1_00_a.common.ALL;

entity allcompare is
   Generic (
      CGN_DATASIZE : integer := 1;
      CGN_MAXSIZE : integer := 1;
      CGN_SIGNCOMPARE : boolean := true;
      CGN_CLKPOLARITY : std_logic := '1'
   );
   Port (
      DATA_I : in  std_logic_vector ((2**CGN_MAXSIZE)*CGN_DATASIZE-1 downto 0);
      CLK : in  std_logic;
      DATA_O : out  std_logic_vector (CGN_DATASIZE-1 downto 0);
      MAX_O  : out  std_logic_vector (CGN_MAXSIZE-1 downto 0)
   );
end allcompare;

architecture Behavioral of allcompare is
   -- 2**CGN_MAXSIZE are input values, 2**CGN_MAXSIZE-1 are internal vals
   -- example CGN_MAXSIZE=4 -> 16 Input and 8+4+2+1=15 Output
   constant NInternal : integer := (2**CGN_MAXSIZE)+(2**CGN_MAXSIZE)-1;
   type InternalDataType is array (NInternal-1 downto 0) of std_logic_vector (CGN_DATASIZE-1 downto 0);
   type InternalMaxType is array (NInternal-1 downto 0) of std_logic_vector (CGN_MAXSIZE-1 downto 0);

   signal InternalData : InternalDataType;
   signal InternalMax : InternalMaxType;

begin

   -- assign input vals to highest internal values
   input_vals: for i in 0 to (2**CGN_MAXSIZE)-1 generate
      InternalData((2**CGN_MAXSIZE)-1+i) <= DATA_I((i+1)*CGN_DATASIZE-1 downto i*CGN_DATASIZE);
      InternalMax((2**CGN_MAXSIZE)-1+i) <= std_logic_vector(to_unsigned(i, CGN_MAXSIZE));
   end generate;

   -- comparison tree
   cmp_tree: block
   begin
      cmp_layer: for iLayer in CGN_MAXSIZE-1 downto 0 generate
         constant NIN : integer := 2**(iLayer+1);
         constant NOUT : integer := 2**iLayer;

         constant inputStart : integer := (2**(iLayer+1))-1;
         constant outputStart : integer := (2**iLayer)-1;
      begin
         cmp_block: for iCmp in 0 to NOUT-1 generate
            signal DATA_A : std_logic_vector(CGN_DATASIZE-1 downto 0);
            signal DATA_B : std_logic_vector(CGN_DATASIZE-1 downto 0);
            signal MAX_A : std_logic_vector(CGN_MAXSIZE-1 downto 0);
            signal MAX_B : std_logic_vector(CGN_MAXSIZE-1 downto 0);
            signal DATA : std_logic_vector(CGN_DATASIZE-1 downto 0);
            signal MAX : std_logic_vector(CGN_MAXSIZE-1 downto 0);
         begin
            MAX_A <= InternalMax(inputStart+2*iCmp);
            MAX_B <= InternalMax(inputStart+2*iCmp+1);
            DATA_A <= InternalData(inputStart+2*iCmp);
            DATA_B <= InternalData(inputStart+2*iCmp+1);

            single_cmp : entity megtrigger_v1_00_a.singlecompare 
            generic map(
                          CGN_MAXSIZE => CGN_MAXSIZE,
                          CGN_DATASIZE => CGN_DATASIZE,
                          CGN_SIGNCOMPARE => CGN_SIGNCOMPARE,
                          CGN_CLKPOLARITY => CGN_CLKPOLARITY
                       )
            port map(
                       DATA_A_I => DATA_A,
                       DATA_B_I => DATA_B,
                       CLK => CLK,
                       MAX_A_I => MAX_A,
                       MAX_B_I => MAX_B,
                       DATA_O => DATA,
                       MAX_O => MAX
                    );
            InternalData(outputStart+iCmp) <= DATA;
            InternalMax(outputStart+iCmp) <= MAX;
         end generate;
      end generate;
   end block;

   -- extract outputs
   DATA_O <= InternalData(0);
   MAX_O <= InternalMax(0);

end Behavioral;

