library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

entity TDC_HYSTERESIS is
  generic(
    CGN_NBITS_I           : natural := 3;
    CGN_HYSTERESYS_LENGTH : natural := 2
  );
  port(
    CLK           : in std_logic;
    TDCTIME_I     : in std_logic_vector(CGN_NBITS_I-1 downto 0);
    HIT_I         : in std_logic;
    TDCSTATE_I    : in std_logic;
    ENABLE_I      : in std_logic;
    TDCTIME_O     : out std_logic_vector(CGN_NBITS_I-1 downto 0);
    HIT_O         : out std_logic;
    TDCSTATE_O    : out std_logic
   );
end;

architecture BEHAVIORAL of TDC_HYSTERESIS is
  signal OLD_TDCSTATE : std_logic_vector(CGN_HYSTERESYS_LENGTH-1 downto 0) := (others =>'0');
  signal NOT_ALLOWED : std_logic;
begin

   --
   process(CLK)
   begin
      if(rising_edge(CLK)) then
         OLD_TDCSTATE <= (OLD_TDCSTATE(CGN_HYSTERESYS_LENGTH-2 downto 0) & TDCSTATE_I);
         NOT_ALLOWED <= (or_reduce(OLD_TDCSTATE) or TDCSTATE_I) and ENABLE_I;

         if(NOT_ALLOWED = '1') then
            HIT_O <= '0';
            TDCTIME_O <= (others => '0');
            TDCSTATE_O <= '0';
         else
            HIT_O <= HIT_I;
            TDCSTATE_O <= TDCSTATE_I;
            TDCTIME_O <= TDCTIME_I;
         end if;
      end if;
   end process;

end BEHAVIORAL;
