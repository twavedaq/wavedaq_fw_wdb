library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.or_reduce;


entity TDC_TRIGGER is
  generic(
    CGN_DLY        : natural := 0
  );
  port(
    CLK_I           : in std_logic;
    HIT_I           : in std_logic_vector(15 downto 0);
    STATE_I         : in std_logic_vector(15 downto 0);
    PULSE_O         : out std_logic;
    MERGE_O         : out std_logic
   );
end;

architecture BEHAVIORAL of TDC_TRIGGER is
   signal tdc_or : std_logic;
   signal tdc_or_old : std_logic;
   signal state_or : std_logic;
   signal trigger_merge :std_logic;
begin

   --if any channel over theshold -> pulse
   process(CLK_I)
   begin
      if(rising_edge(CLK_I)) then
         state_or <= or_reduce(STATE_I);
      end if;
   end process;

   PULSE_O <= state_or;

   -- compute or of all TDC hits
   process(CLK_I)
   begin
      if(rising_edge(CLK_I)) then
         tdc_or <= or_reduce(HIT_I);
         tdc_or_old <= tdc_or;
      end if;
   end process;

   --catch hit on any channel after no hit was registered previously (avoid double fire)
   process(CLK_I)
   begin
      if(rising_edge(CLK_I)) then
         trigger_merge <= tdc_or and not tdc_or_old;
      end if;
   end process;
        
   dly_sr: if(CGN_DLY /=0) generate
      signal merge_dly: std_logic_vector(CGN_DLY-1 downto 0);
   begin
      process(CLK_I)
      begin
         if(rising_edge(CLK_I)) then
            merge_dly <= merge_dly(CGN_DLY-2 downto 0) & trigger_merge;
         end if;
      end process;
      MERGE_O <= merge_dly(CGN_DLY-1);
   end generate;
        
   no_dly_sr: if (CGN_DLY =0) generate
      MERGE_O <= trigger_merge;
   end generate;


end BEHAVIORAL;
