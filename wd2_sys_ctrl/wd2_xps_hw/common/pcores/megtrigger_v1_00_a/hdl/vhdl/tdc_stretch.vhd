library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use IEEE.math_real."ceil";
use IEEE.math_real."log2";

entity TDC_STRETCH is
  generic(
    CGN_NBITS_I        : natural := 3;
    CGN_WIDTH_SIZE     : natural := 2 --should be bigger than or equal to 2
  );
  port(
    CLK           : in std_logic;
    TDCTIME_I     : in std_logic_vector(CGN_NBITS_I-1 downto 0);
    HIT_I         : in std_logic;
    TDCSTATE_I    : in std_logic;
    TDCTIME_O     : out std_logic_vector(CGN_NBITS_I+natural(ceil(log2(real(CGN_WIDTH_SIZE))))-1 downto 0);
    HIT_O         : out std_logic;
    TDCSTATE_O    : out std_logic
   );
end;

architecture BEHAVIORAL of TDC_STRETCH is
  signal COUNTER : std_logic_vector(natural(ceil(log2(real(CGN_WIDTH_SIZE))))-1 downto 0) := (others =>'0');
  signal TDCLATCH : std_logic_vector(CGN_NBITS_I-1 downto 0);
  --signal SHAPING : std_logic;
begin

   --
   process(CLK)
   begin
      if(rising_edge(CLK)) then
         --pipeline TDC STATE
         TDCSTATE_O <= TDCSTATE_I;

         if(COUNTER = std_logic_vector(to_unsigned(CGN_WIDTH_SIZE-1, COUNTER'length))) then
           if(HIT_I = '1') then
             TDCLATCH <= TDCTIME_I;
             HIT_O <= '1';
             COUNTER <= (others=>'0');
           else
				 -- option to reset fine tdc timing to 0 when no hit
             --TDCLATCH <= (others => '0');
             HIT_O <= '0';
           end if;
         else
            COUNTER <= COUNTER + 1;
         end if;
      end if;
   end process;

   TDCTIME_O <= COUNTER & TDCLATCH;

end BEHAVIORAL;
