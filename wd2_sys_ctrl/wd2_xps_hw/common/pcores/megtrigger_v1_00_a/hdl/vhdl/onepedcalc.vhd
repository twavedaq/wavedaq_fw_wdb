----------------------------------------------------------------------------------
-- Company: 
-- Engineer:  Luca Galli Marco Francesconi
-- 
-- Create Date:    10:29:42 02/20/2015 
-- Design Name: 
-- Module Name:    onepedcal.vhdl - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.MATH_REAL.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

library megtrigger_v1_00_a;
use megtrigger_v1_00_a.common.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

--library megtrigger_v1_00_a;
--use megtrigger_v1_00_a.common.ALL;

entity onepedcalc is
  generic (
    SR_SIZE   : integer   :=   16;
    DLY0_SIZE  : integer   :=  16;
    DLY1_SIZE  : integer   := 31
  );
  Port (	
    DATAIN : in  STD_LOGIC_VECTOR (ADC_INPUT_DIM-1 downto 0);
    CLK : in  STD_LOGIC;
    PedTHR : in  STD_LOGIC_VECTOR (ADC_INPUT_DIM-1 downto 0);
    RUNMODE : in  STD_LOGIC;
    DATAOUT : out STD_LOGIC_VECTOR (ADC_INPUT_DIM-1 downto 0);
    ISPEDOUT : out STD_LOGIC;
    DLY_ENABLE : in STD_LOGIC_VECTOR(1 downto 0);
    DLY_SELECT : in  STD_LOGIC_VECTOR (integer(ceil(log2(real(SR_SIZE))))-1 downto 0);
    ADDER_SELECT : in  STD_LOGIC_VECTOR (2 downto 0);
    --SYNC : in STD_LOGIC;
    FORCEPED : in STD_LOGIC
  );
end onepedcalc;

architecture Behavioral of onepedcalc is

  signal datainreg : STD_LOGIC_VECTOR (ADC_INPUT_DIM-1 downto 0);
  signal pedval : STD_LOGIC_VECTOR (ADC_INPUT_DIM-1 downto 0);
  type dly0_type is array (DLY0_SIZE-1 downto 0) of STD_LOGIC_VECTOR (ADC_INPUT_DIM-1 downto 0); 
  type dly1_type is array (DLY1_SIZE-1 downto 0) of STD_LOGIC_VECTOR (ADC_INPUT_DIM-1 downto 0); 
  type sr_type is array (SR_SIZE-1 downto 0) of STD_LOGIC_VECTOR (ADC_INPUT_DIM-1 downto 0); 
  type sr_adder_type is array (15 downto 0) of STD_LOGIC_VECTOR (ADC_INPUT_DIM-1 downto 0); 
  signal fixdlydata0 : dly0_type;
  signal fixdlydata1 : dly1_type;
  signal dlydata : sr_type;
  signal fixdlyout0 : STD_LOGIC_VECTOR (ADC_INPUT_DIM-1 downto 0);
  signal dlyout : STD_LOGIC_VECTOR (ADC_INPUT_DIM-1 downto 0);
  signal adderdata : sr_adder_type;

  type adder_0_type is array (7 downto 0) of signed (ADC_INPUT_DIM downto 0);
  type adder_1_type is array (3 downto 0) of signed (ADC_INPUT_DIM+1 downto 0);
  type adder_2_type is array (1 downto 0) of signed (ADC_INPUT_DIM+2 downto 0);
  signal adder_0_data : adder_0_type;
  signal adder_1_data : adder_1_type;
  signal adder_2_data : adder_2_type;
  signal adder_3_data : signed (ADC_INPUT_DIM+3 downto 0);
  signal adderout : signed (ADC_INPUT_DIM-1 downto 0);
  signal adderout_reg : signed (ADC_INPUT_DIM-1 downto 0);
  signal adderout_reg_reg : signed (ADC_INPUT_DIM-1 downto 0);

  signal tomux_0 : signed (ADC_INPUT_DIM downto 0);
  signal tomux_1 : signed (ADC_INPUT_DIM+1 downto 0);
  signal tomux_2 : signed (ADC_INPUT_DIM+2 downto 0);
  signal tomux_3 : signed (ADC_INPUT_DIM+3 downto 0);

  signal checkvalueup, checkvaluedown : signed(ADC_INPUT_DIM downto 0);
  signal isped : STD_LOGIC;
  signal isped_reg : STD_LOGIC;

begin 

  -- input data register
  process (CLK)
  begin
    if(CLK'event and CLK = '1') then
	   if(RUNMODE = '1' or FORCEPED = '1') then
	     datainreg <= DATAIN;
		end if;
    end if;
  end process;

  --register ISPED and compute pedestal value
  process (CLK)
  begin
    if(CLK'event and CLK = '1') then
        isped_reg <= isped;
        --pedval <= std_logic_vector(resize(shift_right(resize(adderout_reg, ADC_INPUT_DIM+1) + signed('0' & datainreg), 1), ADC_INPUT_DIM)); -- new pedestal is average of current measurement and old pedestal
        pedval <= datainreg;
    end if;
  end process;
  
  --dly shift register
  process (CLK)
  begin
    if(CLK'event and CLK = '1') then
      if(FORCEPED = '1') then
        dlydata <= dlydata(SR_SIZE-2 downto 0) & datainreg; -- if pedestal is garbage flush in the data
	   elsif(isped_reg = '1') then
        dlydata <= dlydata(SR_SIZE-2 downto 0) & pedval;
		else
		  dlydata <= dlydata(SR_SIZE-2 downto 0) & std_logic_vector(adderout_reg_reg);
		end if;
    end if;
  end process;

  --fix dly shift register 0
  process (CLK)
  begin
    if(CLK'event and CLK = '1') then
        fixdlydata0 <= fixdlydata0(DLY0_SIZE-2 downto 0) & dlydata(to_integer(unsigned(DLY_SELECT)));
    end if;
  end process;

   -- mux output 0
  process (CLK)
  begin
    if(CLK'event and CLK = '1') then
        if(DLY_ENABLE(0) = '1') then
           fixdlyout0 <= fixdlydata0(DLY0_SIZE-1);
        else
           fixdlyout0 <= dlydata(to_integer(unsigned(DLY_SELECT)));
        end if;
    end if;
  end process;


  --fix dly shift register 1
  process (CLK)
  begin
    if(CLK'event and CLK = '1') then
        fixdlydata1 <= fixdlydata1(DLY1_SIZE-2 downto 0) & fixdlyout0;
    end if;
  end process;

   -- mux output 1
  process (CLK)
  begin
    if(CLK'event and CLK = '1') then
        if(DLY_ENABLE(1) = '1') then
           dlyout <= fixdlydata1(DLY1_SIZE-1);
        else
           dlyout <= fixdlyout0;
        end if;
    end if;
  end process;
  
  -- adder shift register
  process (CLK)
  begin
    if(CLK'event and CLK = '1') then
         adderdata <= adderdata(14 downto 0) & dlyout;
    end if;
  end process;
  
  --adder tree
  tree_level_0 : for i in 0 to 7 generate
    process (CLK) 
    begin
      if(CLK'event and CLK = '1') then
          adder_0_data(i) <= signed('0' & adderdata(2*i)) + signed('0' & adderdata(2*i+1));
      end if;
    end process;
  end generate tree_level_0;
  
  tree_level_1 : for i in 0 to 3 generate
    process (CLK) 
    begin
      if(CLK'event and CLK = '1') then
          adder_1_data(i) <= ('0' & adder_0_data(2*i)) + ('0' & adder_0_data(2*i+1));
      end if;
    end process;
  end generate tree_level_1;

  tree_level_2 : for i in 0 to 1 generate
    process (CLK) 
    begin
      if(CLK'event and CLK = '1') then
          adder_2_data(i) <= ('0' & adder_1_data(2*i)) + ('0' & adder_1_data(2*i+1));
      end if;
    end process;
  end generate tree_level_2;
  
  process (CLK) 
  begin
    if(CLK'event and CLK = '1') then
        adder_3_data <= ('0' & adder_2_data(0)) + ('0' & adder_2_data(1));
    end if;
  end process;
  
  -- add values to compensate for truncation
  tomux_0 <= adder_0_data(0) + 1;
  tomux_1 <= adder_1_data(0) + 2;
  tomux_2 <= adder_2_data(0) + 4;
  tomux_3 <= adder_3_data + 8;

  --adder out mux
  process (CLK)
  begin
    if(CLK'event and CLK = '1') then
      --if(isped = '1') then
        case ADDER_SELECT is
          when "000" => adderout <= signed(adderdata(0));
          when "001" => adderout <= tomux_0(ADC_INPUT_DIM downto 1);
          when "010" => adderout <= tomux_1(ADC_INPUT_DIM+1 downto 2);
          when "011" => adderout <= tomux_2(ADC_INPUT_DIM+2 downto 3);
          when "100" => adderout <= tomux_3(ADC_INPUT_DIM+3 downto 4);
          when others => adderout <= (others=>'0');
        end case;
      --end if;
    end if;
  end process;
  
  DATAOUT <= STD_LOGIC_VECTOR(adderout);
  
  --calculate current threshold
  process(CLK)
  begin
    if(CLK'event and CLK = '1') then
	   --if(isped = '1') then
        checkvaluedown <= ('0' & adderout) - signed('0' & PedTHR);
	     checkvalueup <= ('0' & adderout) + signed('0' & PedTHR);
		--end if;
		  adderout_reg <= adderout;
		  adderout_reg_reg <= adderout_reg;
	 end if;
  end process;
  
  --check pedestal
  process (datainreg, checkvalueup, checkvaluedown, RUNMODE, FORCEPED) 
  begin
    if(FORCEPED = '1') then
      isped <= '1';
    else
      if((signed('0' & datainreg) > checkvalueup) or (signed('0' & datainreg) < checkvaluedown)) then
        isped <= '0';
      else
        isped <= '1';
      end if;
    end if;
  end process;

--OUTPUT of ISPED
  ISPEDOUT <= isped;

end Behavioral;

