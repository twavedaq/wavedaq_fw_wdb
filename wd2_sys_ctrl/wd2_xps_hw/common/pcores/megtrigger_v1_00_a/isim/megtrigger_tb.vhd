-- TestBench Template 
use std.env.stop;
use STD.textio.all;

library ieee;
   use ieee.std_logic_1164.ALL;
   use IEEE.std_logic_textio.all;

library megtrigger_v1_00_a;
   use megtrigger_v1_00_a.ALL;
   use megtrigger_v1_00_a.common.all;

ENTITY megtrigger_tb IS
END megtrigger_tb;

ARCHITECTURE behavior OF megtrigger_tb IS 

-- Component Declaration
component megtrigger
   Port (
      DATA_CLK_I        : in  STD_LOGIC;
      UC_CLK_I          : in  STD_LOGIC;
      SYSTEM_CLK_I      : in  STD_LOGIC;
      ADCDATA_I         : in  STD_LOGIC_VECTOR(ADC_INPUT_DIM*16-1 downto 0) ;
      TDCDATA_I         : in STD_LOGIC_VECTOR(TDC_INPUT_DIM*16-1 downto 0) ;
      SHAPER_TRG_I      : in STD_LOGIC_VECTOR(15 downto 0) ;
      CALDATA_I         : in STD_LOGIC_VECTOR(CAL_DIM*16-1 downto 0) ;
      OFFSETDATA_I      : in STD_LOGIC_VECTOR(OFFSET_DIM*16-1 downto 0) ;
      PEDCONF_I         : in STD_LOGIC_VECTOR(31 downto 0) ;
      THR0_I            : in STD_LOGIC_VECTOR(31 downto 0) ;
      THR1_I            : in STD_LOGIC_VECTOR(31 downto 0) ;
      THR2_I            : in STD_LOGIC_VECTOR(31 downto 0) ;
      DBGSERDES_I       : in STD_LOGIC_VECTOR(63 downto 0) ;
      TDCCHMASK_I       : in STD_LOGIC_VECTOR(15 downto 0) ;
      CTRLBUS_I         : in STD_LOGIC_VECTOR(31 downto 0) ;
      LOCAL_TRIG_O      : out STD_LOGIC;
      LOCAL_VETO_TRIG_O : out STD_LOGIC;
      SYNC_I            : in STD_LOGIC;
      DRS_BUSY_I        : in STD_LOGIC;
      BUSY_O            : out STD_LOGIC;
      TCBData_O         : out STD_LOGIC_VECTOR(63 downto 0)
   );
end component;

   CONSTANT clk_period : time := 12500 ps;

   SIGNAL clk : std_logic;
   SIGNAL uc_clk : std_logic;
   SIGNAL system_clk : std_logic;

   --input signals
   SIGNAL adc : ADCInputData;
   SIGNAL ADCDATA : STD_LOGIC_VECTOR (ADC_INPUT_DIM*16-1 downto 0);
   SIGNAL tdc : TDCInputData;
   SIGNAL TDCDATA : STD_LOGIC_VECTOR (TDC_INPUT_DIM*16-1 downto 0);
   SIGNAL COMP : STD_LOGIC_VECTOR (15 downto 0);

   --control bus
   SIGNAL CONTROLBUS : std_logic_vector(31 downto 0);

   SIGNAL BUSYMASK : std_logic;
   SIGNAL ALGSEL : std_logic_vector(1 downto 0);
   SIGNAL FORCERUN : std_logic;
   SIGNAL CLEANPED : std_logic;
   SIGNAL TDC_POLARITY : std_logic;
   SIGNAL DEBUG_CTRL : std_logic;
   SIGNAL SW_HYSTERESIS : std_logic;

   --pedestal configuration
   SIGNAL PEDCONF : std_logic_vector(31 downto 0);

   SIGNAL PED_THR : STD_LOGIC_VECTOR(ADC_INPUT_DIM-1 downto 0);
   SIGNAL PED_DLY_ENABLE : STD_LOGIC_VECTOR(1 downto 0);
   SIGNAL PED_DLY_SELECT : STD_LOGIC_VECTOR(3 downto 0);
   SIGNAL PED_ADDER_SELECT : STD_LOGIC_VECTOR(2 downto 0);

   -- trgbus
   SIGNAL SYNC : std_logic;
   SIGNAL DRS_BUSY : std_logic;

   --calib
   SIGNAL calib : CalibData;
   SIGNAL CALDATA : STD_LOGIC_VECTOR (CAL_DIM*16-1 downto 0);

   --offset
   SIGNAL offset : OffsetData;
   SIGNAL OFFDATA : STD_LOGIC_VECTOR (OFFSET_DIM*16-1 downto 0);

   -- others
   SIGNAL TDCCHMASK : std_logic_vector(15 downto 0);
   SIGNAL DEBUGWORD : std_logic_vector(63 downto 0);
   SIGNAL TCBDATA : std_logic_vector(63 downto 0);
BEGIN

-- Component Instantiation
   uut: megtrigger 
      PORT MAP(
         DATA_CLK_I => clk, 
         UC_CLK_I => uc_clk,
         SYSTEM_CLK_I => system_clk,
         ADCDATA_I => ADCDATA,
         TDCDATA_I => TDCDATA,
         SHAPER_TRG_I => COMP,
         CALDATA_I => CALDATA,
         OFFSETDATA_I => OFFDATA,
         PEDCONF_I => PEDCONF,
         THR0_I => X"00000000",
         THR1_I => X"00000000",
         THR2_I => X"00000000",
         DBGSERDES_I => DEBUGWORD,
         TDCCHMASK_I => TDCCHMASK,
         CTRLBUS_I => CONTROLBUS,
         LOCAL_TRIG_O => open,
         LOCAL_VETO_TRIG_O => open,
         SYNC_I => SYNC,
         DRS_BUSY_I => DRS_BUSY,
         BUSY_O => open,
         TCBData_O => TCBDATA
      );

   --clock generating process
   clock : PROCESS
   BEGIN
         clk <= '1';
         wait for clk_period/4;
         uc_clk <= '1';
         system_clk <= '1';
         wait for clk_period/4;
         clk <= '0';
         wait for clk_period/4;
         uc_clk <= '0';
         system_clk <= '0';
         wait for clk_period/4;
   END PROCESS clock;

   -- encode bus
   CONTROLBUS <= ( 0 => BUSYMASK,
                   4 => ALGSEL(0),
                   5 => ALGSEL(1),
                   7 => FORCERUN,
                   8 => CLEANPED,
                   9 => TDC_POLARITY,
                  10 => DEBUG_CTRL,
                  11 => '0',
                  12 => SW_HYSTERESIS,
              others => '0');

   PEDCONF(ADC_INPUT_DIM-1 downto 0) <= PED_THR;
   PEDCONF(15 downto ADC_INPUT_DIM) <= (others => '0');
   PEDCONF(19 downto 16) <= PED_DLY_SELECT;
   PEDCONF(21 downto 20) <= PED_DLY_ENABLE;
   PEDCONF(23 downto 22) <= (others => '0');
   PEDCONF(26 downto 24) <= PED_ADDER_SELECT;
   PEDCONF(31 downto 27) <= (others => '0');

   g_calib : FOR i IN 0 TO 15 GENERATE
     ADCDATA(ADC_INPUT_DIM*(i+1)-1 downto ADC_INPUT_DIM*i) <= adc(i);
     TDCDATA(TDC_INPUT_DIM*(i+1)-1 downto TDC_INPUT_DIM*i) <= tdc(i);
     CALDATA(CAL_DIM*(i+1)-1 downto CAL_DIM*i) <= calib(i);
     OFFDATA(OFFSET_DIM*(i+1)-1 downto OFFSET_DIM*i) <= offset(i);
   END GENERATE g_calib;

   --  Test Bench Statements

   -- Fixed data
   BUSYMASK <= '1'; 
   FORCERUN <= '0';
   CLEANPED <= '0';
   DEBUG_CTRL <= '1'; -- 0 = DEBUGWORD, 1 = 0xA(0..F)
   DEBUGWORD <= X"DEADBEEFDEADBEEF";

   COMP <= X"0000"; 

   -- variable stimuli
   tb : PROCESS
      file input_file : TEXT open READ_MODE is "file_io.in";
      file output_file : TEXT open WRITE_MODE is "file_io.out";
      variable my_line : LINE;
      variable input_line :LINE;
      variable output_line : LINE;
      variable my_adc : std_logic_vector(ADC_INPUT_DIM-1 downto 0);
      variable my_tdc : std_logic_vector(TDC_INPUT_DIM-1 downto 0);
      variable my_cal : std_logic_vector(CAL_DIM-1 downto 0);
      variable my_offset : std_logic_vector(OFFSET_DIM-1 downto 0);
      variable my_algsel : std_logic_vector(1 downto 0);
      variable my_bit : std_logic;
      variable my_16bit : std_logic_vector(15 downto 0);
      variable my_adderselect : std_logic_vector(2 downto 0);
      variable my_dlyenable : std_logic_vector(1 downto 0);
      variable my_dlyselect : std_logic_vector(3 downto 0);
      variable my_pedthr : STD_LOGIC_VECTOR(ADC_INPUT_DIM-1 downto 0);
   BEGIN

      --conf values
      readline(input_file, input_line);
      read(input_line, my_algsel);
      write(my_line, string'("*************ALGSEL: "));
      write(my_line, my_algsel);
      writeline(output, my_line);
      ALGSEL <= my_algsel;

      read(input_line, my_bit);
      write(my_line, string'("*************TDC POLARITY: "));
      write(my_line, my_bit);
      writeline(output, my_line);
      TDC_POLARITY <= my_bit;

      read(input_line, my_bit);
      write(my_line, string'("*************TDC SW HYSTERESIS: "));
      write(my_line, my_bit);
      writeline(output, my_line);
      SW_HYSTERESIS <= my_bit;

      read(input_line, my_16bit);
      write(my_line, string'("*************TDC CHANNEL MASK: "));
      write(my_line, my_16bit);
      writeline(output, my_line);
      TDCCHMASK <= my_16bit;

      --pedestal
      readline(input_file, input_line);
      read(input_line, my_pedthr);
      read(input_line, my_dlyenable);
      read(input_line, my_dlyselect);
      read(input_line, my_adderselect);
      PED_THR <= my_pedthr;
      PED_DLY_SELECT <= my_dlyselect;
      PED_DLY_ENABLE <= my_dlyenable;
      PED_ADDER_SELECT <= my_adderselect;
      write(my_line, string'("*************PEDESTAL THRESHOLD: "));
      write(my_line, my_pedthr);
      writeline(output, my_line);
      write(my_line, string'("*************PEDESTAL DELAY: "));
      write(my_line, my_dlyenable);
      write(my_line, my_dlyselect);
      writeline(output, my_line);
      write(my_line, string'("*************PEDESTAL ADDER SELECT: "));
      write(my_line, my_adderselect);
      writeline(output, my_line);

      SYNC <= '0';
      DRS_BUSY <= '0';

      --weights
      for iCh in 0 to 15 loop
        readline(input_file, input_line);
        read(input_line, my_cal);
        calib(iCh) <= my_cal;
        write(my_line, string'("*************Ch"));
        write(my_line, iCh);
        write(my_line, string'(": "));
        write(my_line, my_cal);
        writeline(output, my_line);
      end loop;

      --offsets
      for iCh in 0 to 15 loop
        readline(input_file, input_line);
        read(input_line, my_offset);
        offset(iCh) <= my_offset;
        write(my_line, string'("*************Ch"));
        write(my_line, iCh);
        write(my_line, string'(": "));
        write(my_line, my_offset);
        writeline(output, my_line);
      end loop;

      wait for 125 ns; -- wait until global set/reset completes

      write(my_line, string'("Generating SYNC"));
      writeline(output, my_line);

      -- generate SYNC
      SYNC <= '1';
      wait for 4*clk_period;
      SYNC <= '0';
      wait for clk_period;

      write(my_line, string'("Initializing pedestal"));
      writeline(output, my_line);

      -- load first row and force pedestal
      DRS_BUSY <= '1';
      readline(input_file, input_line);
      for iCh in 0 to 15 loop
         read(input_line, my_adc);
         adc(iCh) <= my_adc;
         read(input_line, my_tdc);
         tdc(iCh) <= my_tdc;
      end loop;
      wait for 4*clk_period;
      DRS_BUSY <= '0';
      wait for 200*clk_period;

      write(my_line, string'("Loop on file"));
      writeline(output, my_line);

      --loop until EOF
      loop
        exit when endfile(input_file);
        readline(input_file, input_line);
        for iCh in 0 to 15 loop
           read(input_line, my_adc);
           adc(iCh) <= my_adc;
           read(input_line, my_tdc);
           tdc(iCh) <= my_tdc;
        end loop;
        write(output_line, tcbdata);
        writeline(output_file, output_line);
        wait for clk_period;
      end loop;

      write(my_line, string'("waiting some more cycles"));
      writeline(output, my_line);

      --loop for few more cycles
      for iWait in 0 to 30 loop
        write(output_line, tcbdata);
        writeline(output_file, output_line);
        wait for clk_period;
      end loop;

      report "*** Test SUCCESSFULL ****"  severity failure ;

   END PROCESS tb;

--  End Test Bench 
END;
