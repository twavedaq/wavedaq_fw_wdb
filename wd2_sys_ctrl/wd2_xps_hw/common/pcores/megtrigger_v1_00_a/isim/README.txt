--- CMDs ---

make
   build executable and executes it using cmd.tcl as input script

make clean
   removes all generates files

--- File Format ---
all files are text file with strings conining ONLY 0 and 1
file_io.in:
   header:
      ALGSEL(2 bits) TDC_POLARITY(1 bit) TDC_SW_HYSTERESIS(1 bit) TDCCHMASK(16 bits)
      PEDTHR(ADC_INPUT_DIM bits) DLY_ENABLE(2 bits) DLY_SELECT(4 bits) ADDERSELECT(3 bits)
      16 rows of:
         CALIB(CAL_DIM bits)
      16 rows of:
         OFFSET(OFFSET_DIM bits)
   each line (first row is also used for init value): 
      ADC_DATA0(ADC_INPUT_DIM bits) TDC_DATA0(TDC_INPUT_DIM bits) ADC_DATA1 TDC_DATA1 .... ADC_DATA15 TDC_DATA15

file_io.out:
   each line is a 64bit word to TCB

-- Relevant files --
To add a new file to the simulation add a line to megtrigger.prj
