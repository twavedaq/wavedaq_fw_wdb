---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Synchronization Register
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid (Author of generation script)
--  Created :  21.08.2014 11:10:00
--
--  Description :  Generic synchronization register to be insertet in xps designs.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

entity util_sync_reg is
  generic (
    CGN_REGISTER_WIDTH : integer := 32;
    CGN_USE_INPUT_REG  : integer := 0;
    CGN_NO_REGISTERS   : integer := 2
  );
  port (
    SYNC_REGS_D_I : in  std_logic_vector(CGN_REGISTER_WIDTH-1 downto 0);
    SYNC_REGS_Q_O : out std_logic_vector(CGN_REGISTER_WIDTH-1 downto 0);
    INPUT_CLK_I   : in  std_logic;
    SYNC_CLK_I    : in  std_logic
  );
end util_sync_reg;

architecture behavioral of util_sync_reg is
begin

  sync_register_inst : entity psi_3205_v1_00_a.cdc_sync
  generic map
  (
    CGN_DATA_WIDTH          => CGN_REGISTER_WIDTH,
    CGN_USE_INPUT_REG_A     => CGN_USE_INPUT_REG,
    CGN_USE_OUTPUT_REG_A    => 0,
    CGN_USE_GRAY_CONVERSION => 0,
    CGN_NUM_SYNC_REGS_B     => CGN_NO_REGISTERS,
    CGN_NUM_OUTPUT_REGS_B   => 0,
    CGN_GRAY_TO_BIN_STYLE   => 0
  )
  port map
  (
    CLK_A_I => INPUT_CLK_I,
    CLK_B_I => SYNC_CLK_I,
    PORT_A_I => SYNC_REGS_D_I,
    PORT_B_O => SYNC_REGS_Q_O
  );

end architecture behavioral;
