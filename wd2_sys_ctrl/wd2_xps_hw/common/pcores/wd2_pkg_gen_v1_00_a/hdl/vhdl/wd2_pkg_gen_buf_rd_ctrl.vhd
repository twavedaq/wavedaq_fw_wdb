---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  WaveDream2 Package Generator Buffer Read Controller
--
--  Project :  WaveDream2
--
--  PCB  :  DRS4 WaveDream Board II (121202C)
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  12.03.2015 09:14:57
--
--  Description : Controls the read process to the data buffers of the
--                WaveDream2 (WD2) package generator. Data read will be
--                transmitted via a local link interface.
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_MISC.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.ram_dp;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

entity wd2_pkg_gen_rd_ctrl is
generic
(
  CGN_BUFFERS               : integer :=    4; -- To be adapted to depth of one RAM
  CGN_MAX_SAMPLES_PER_EVENT : integer := 1024;
  CGN_SEGMENTS              : integer :=    2;
  CGN_NR_OF_ADCS            : integer :=    2;
  CGN_CHNLS_PER_ADC         : integer :=    8;
  CGN_ADC_CH_DWIDTH         : integer :=   12
);  
port
(
  BUF_RD_ADDR_O    : out std_logic_vector(log2ceil(CGN_BUFFERS*CGN_MAX_SAMPLES_PER_EVENT)-1 downto 0);
  BUF_READ_DONE_O  : out std_logic_vector(CGN_BUFFERS-1 downto 0);
  BUF_EMPTY_I      : in  std_logic_vector(CGN_BUFFERS-1 downto 0);
  RD_BUFFER_NR_O   : out std_logic_vector(log2ceil(CGN_BUFFERS)-1 downto 0);
  RD_CHIP_NR_O     : out std_logic_vector(log2ceil(CGN_NR_OF_ADCS)-1 downto 0);
  RD_CHANNEL_NR_O  : out std_logic_vector(log2ceil(CGN_CHNLS_PER_ADC)-1 downto 0);
  RD_SEGMENT_NR_O  : out std_logic_vector(log2ceil(CGN_SEGMENTS)-1 downto 0);

  SHUFFLER_EOP_N_O : out std_logic;
  SHUFFLER_EN_O    : out std_logic;
  SHUFFLER_WEN_O   : out std_logic;
  SHUFFLER_FULL_I  : in  std_logic;
  SHUFFLER_AFULL_I : in  std_logic;

  EVENT_NR_I       : in  std_logic_vector(31 downto 0);
  ADC_0_CH_TX_EN_I : in  std_logic_vector(CGN_CHNLS_PER_ADC-1 downto 0);
  ADC_1_CH_TX_EN_I : in  std_logic_vector(CGN_CHNLS_PER_ADC-1 downto 0);

  PKG_NR_O         : out std_logic_vector(15 downto 0);

  -- General Signals
  ENABLE_I : IN std_logic;
  CLK_I    : IN std_logic;
  RST_I    : IN std_logic
);
end wd2_pkg_gen_rd_ctrl;

architecture Behavioral of wd2_pkg_gen_rd_ctrl is

  -- UDP Package LUT RAM address:
  -- Entry:   | Buffer | Chip | ADC-Ch. | Ch. Seq. Nr |
  -- Index:   | 6 .. 5 |  4   | 4 ... 1 |      0      |

  constant C_LUT_ENTRIES_PER_BUFFER : integer := CGN_SEGMENTS*CGN_NR_OF_ADCS*CGN_CHNLS_PER_ADC;
  constant C_LUT_ADDRS              : integer := CGN_BUFFERS*C_LUT_ENTRIES_PER_BUFFER;
  constant C_BUF_ADDRS              : integer := CGN_BUFFERS*CGN_MAX_SAMPLES_PER_EVENT;

  -- states of state machine
  type type_rd_state is (init, wait_data, read_data);
  signal rd_state           : type_rd_state := init;
  attribute fsm_encoding : string;
  attribute fsm_encoding of rd_state : signal is "one-hot";

  signal pkgnr_lut_addr    : std_logic_vector(log2ceil(C_LUT_ADDRS)-1 downto 0)  := (others=>'0');
  signal pkgnr_lut_we      : std_logic := '0';
  signal pkgnr_lut_din     : std_logic_vector(15 downto 0) := (others=>'0');

  signal rd_entry_nr       : std_logic_vector(log2ceil(CGN_MAX_SAMPLES_PER_EVENT)-log2ceil(CGN_SEGMENTS)-1 downto 0) := (others=>'0');
  signal rd_segment_nr     : std_logic_vector(log2ceil(CGN_SEGMENTS)-1 downto 0) := (others=>'0');
  signal rd_chip_nr        : std_logic_vector(log2ceil(CGN_NR_OF_ADCS)-1 downto 0) := (others=>'0');
  signal rd_channel_nr     : std_logic_vector(log2ceil(CGN_CHNLS_PER_ADC)-1 downto 0) := (others=>'0');
  signal rd_buffer_nr      : std_logic_vector(log2ceil(CGN_BUFFERS)-1 downto 0) := (others=>'0');

  signal adc_0_ch_tx_en    : std_logic_vector(CGN_CHNLS_PER_ADC-1 downto 0) := (others=>'0');
  signal adc_1_ch_tx_en    : std_logic_vector(CGN_CHNLS_PER_ADC-1 downto 0) := (others=>'0');

  signal udp_pkg_nr         : std_logic_vector(15 downto 0) := (others=>'0');

begin

  parameter_lookup_table : entity psi_3205_v1_00_a.ram_dp
    generic map
    (
      CGN_DATA_WIDTH_A => 16,
      CGN_ADDR_WIDTH_A => log2ceil(C_LUT_ADDRS),
      CGN_READ_PORT_A  => true,
      CGN_WRITE_PORT_A => true,
      CGN_READ_FIRST_A => false,
      CGN_USE_OUTREG_A => true,
      CGN_DATA_WIDTH_B => 16,
      CGN_ADDR_WIDTH_B => log2ceil(C_LUT_ADDRS),
      CGN_READ_PORT_B  => true,
      CGN_WRITE_PORT_B => false,
      CGN_READ_FIRST_B => true,
      CGN_USE_OUTREG_B => false,
      CGN_RAM_STYLE    => "block"  -- {auto|block|distributed|pipe_distributed|block_power1|block_power2}
    )
    port map
    (
      PA_CLK_I   => CLK_I,
      PA_EN_I    => '1',
      PA_ADDR_I  => pkgnr_lut_addr,
      PA_DATA_I  => pkgnr_lut_din,
      PA_WR_EN_I => pkgnr_lut_we,
      PA_DATA_O  => PKG_NR_O,
      PB_CLK_I   => '0',
      PB_EN_I    => '0',
      PB_ADDR_I  => (others=>'0'),
      PB_DATA_I  => (others=>'0'),
      PB_WR_EN_I => '0',
      PB_DATA_O  => open
    );

  SHUFFLER_EN_O  <= '1';

  -- UDP Package LUT RAM address:
  -- Entry:   | Buffer | Chip | ADC-Ch. | Ch. Seq. Nr |
  -- Index:   | 6 .. 5 |  4   | 3 ... 1 |      0      |
  fsm_read_buffer : process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if RST_I = '1' then
        pkgnr_lut_we     <= '0';
        rd_buffer_nr     <= (others=>'0');
        rd_segment_nr    <= (others=>'0');
        rd_chip_nr       <= (others=>'0');
        rd_channel_nr    <= (others=>'0');
        rd_entry_nr      <= (others=>'0');
        udp_pkg_nr       <= (others=>'0');
        SHUFFLER_WEN_O   <= '0';
        SHUFFLER_EOP_N_O <= '1';
        rd_state <= init;
      else
        -- defaults -----------------------
        SHUFFLER_WEN_O   <= '0';
        SHUFFLER_EOP_N_O <= '1';
        pkgnr_lut_we     <= '0';
        pkgnr_lut_din    <= (others=>'0');
        BUF_READ_DONE_O  <= (others=>'0');
        -----------------------------------
        case rd_state is
          when init =>
            -- reset udp package numbers in lut
            pkgnr_lut_we <= '1';
            rd_segment_nr <= rd_segment_nr + 1;
            if rd_segment_nr = CONV_STD_LOGIC_VECTOR(CGN_SEGMENTS-1, rd_segment_nr'length) then
              rd_segment_nr <= (others=>'0');
              rd_channel_nr <= rd_channel_nr + 1;
              if rd_channel_nr = CONV_STD_LOGIC_VECTOR(CGN_CHNLS_PER_ADC-1, rd_channel_nr'length) then
                rd_channel_nr <= (others=>'0');
                rd_chip_nr <= rd_chip_nr + 1;
                if rd_chip_nr = CONV_STD_LOGIC_VECTOR(CGN_NR_OF_ADCS-1, rd_chip_nr'length) then
                  rd_chip_nr    <= (others=>'0');
                  rd_buffer_nr <= rd_buffer_nr + 1;
--                  if pkgnr_lut_addr = CONV_STD_LOGIC_VECTOR(C_LUT_ADDRS-1, pkgnr_lut_addr'length) then
                  if rd_buffer_nr = CONV_STD_LOGIC_VECTOR(CGN_BUFFERS-1, rd_buffer_nr'length) then
                    rd_buffer_nr  <= (others=>'0');
                    rd_state <= wait_data;
                  end if;
                end if;
              end if;
            end if;

          when wait_data =>
            if BUF_EMPTY_I(CONV_INTEGER(rd_buffer_nr)) = '0' and ENABLE_I = '1' then
              adc_0_ch_tx_en <= ADC_0_CH_TX_EN_I;
              adc_1_ch_tx_en <= ADC_1_CH_TX_EN_I;
              rd_state <= read_data;
            end if; 

          when read_data =>
            if SHUFFLER_AFULL_I = '0' then
              if ( rd_chip_nr = "0" and adc_0_ch_tx_en(CONV_INTEGER(rd_channel_nr)) = '1' ) or
                 ( rd_chip_nr = "1" and adc_1_ch_tx_en(CONV_INTEGER(rd_channel_nr)) = '1' ) then
                SHUFFLER_WEN_O <= '1';
                rd_entry_nr <= rd_entry_nr + 1;
              end if;
              if ( rd_chip_nr = "0" and adc_0_ch_tx_en(CONV_INTEGER(rd_channel_nr)) = '0' ) or
                 ( rd_chip_nr = "1" and adc_1_ch_tx_en(CONV_INTEGER(rd_channel_nr)) = '0' ) then
                if rd_channel_nr = CONV_STD_LOGIC_VECTOR(CGN_CHNLS_PER_ADC-1, rd_channel_nr'length) then
                  rd_channel_nr <= (others=>'0');
                  if rd_chip_nr = CONV_STD_LOGIC_VECTOR(CGN_NR_OF_ADCS-1, rd_chip_nr'length) then
                    rd_chip_nr <= (others=>'0');
                    BUF_READ_DONE_O(CONV_INTEGER(rd_buffer_nr)) <= '1'; 
                    rd_buffer_nr  <= rd_buffer_nr + 1;
                    rd_state <= wait_data;
                  else
                    rd_chip_nr <= rd_chip_nr + 1;
                  end if;
                else
                  rd_channel_nr <= rd_channel_nr + 1;
                end if;
              elsif rd_entry_nr = 0 then
                pkgnr_lut_din <= udp_pkg_nr;
                pkgnr_lut_we  <= '1';
              elsif rd_entry_nr = CONV_STD_LOGIC_VECTOR((C_BUF_ADDRS/CGN_SEGMENTS)-1, rd_entry_nr'length) then
                SHUFFLER_EOP_N_O <= '0';
                udp_pkg_nr    <= udp_pkg_nr + 1;
                if rd_segment_nr = CONV_STD_LOGIC_VECTOR(CGN_SEGMENTS-1, rd_segment_nr'length) then
                  rd_segment_nr <= (others=>'0');
                  if rd_channel_nr = CONV_STD_LOGIC_VECTOR(CGN_CHNLS_PER_ADC-1, rd_channel_nr'length) then
                    rd_channel_nr <= (others=>'0');
                    if rd_chip_nr = CONV_STD_LOGIC_VECTOR(CGN_NR_OF_ADCS-1, rd_chip_nr'length) then
                      rd_chip_nr <= (others=>'0');
                      BUF_READ_DONE_O(CONV_INTEGER(rd_buffer_nr)) <= '1'; 
                      rd_buffer_nr <= rd_buffer_nr + 1;
                      rd_state <= wait_data;
                    else
                      rd_chip_nr <= rd_chip_nr + 1;
                    end if;
                  else
                    rd_channel_nr <= rd_channel_nr + 1;
                  end if;
                else
                  rd_segment_nr <= rd_segment_nr + 1;
                end if;
              end if;
            end if;

          when others =>
            rd_state <= wait_data;

        end case;
      end if;
    end if;
  end process fsm_read_buffer;

  pkgnr_lut_addr  <= rd_buffer_nr & rd_chip_nr & rd_channel_nr & rd_segment_nr;

  BUF_RD_ADDR_O   <= rd_buffer_nr & rd_segment_nr & rd_entry_nr;
  RD_BUFFER_NR_O  <= rd_buffer_nr;
  RD_CHIP_NR_O    <= rd_chip_nr;
  RD_CHANNEL_NR_O <= rd_channel_nr;
  RD_SEGMENT_NR_O <= rd_segment_nr;

end Behavioral;
