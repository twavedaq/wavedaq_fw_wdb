---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  WaveDream2 Package Generator RAM Arbiter
--
--  Project :  WaveDream2
--
--  PCB  :  DRS4 WaveDream Board II (121202C)
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  16.03.2015 11:19:31
--
--  Description : RAM arbiter  for the Wavedream 2Package.
--                Makes sure there is no access collision on the dual port RAM.
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.STD_LOGIC_ARITH.ALL;
--use IEEE.STD_LOGIC_UNSIGNED.ALL;
--use IEEE.STD_LOGIC_MISC.ALL;

--library UNISIM;
--use UNISIM.VComponents.all;

entity wd2_pkg_gen_2x12to3x8 is
port
(
  -- input interface
  DATA_I   : in  std_logic_vector(11 downto 0);
  WR_EN_I  : in  std_logic;
  FULL_O   : out std_logic;
  SOP_N_I  : in  std_logic;
  EOP_N_I  : in  std_logic;
  -- output interface
  DATA_O   : out std_logic_vector( 7 downto 0);
  RD_EN_I  : in  std_logic;
  EMPTY_O  : out std_logic;
  SOP_N_O  : out std_logic;
  EOP_N_O  : out std_logic;
  -- clock, reset ...
  ENABLE_I : in  std_logic;
  CLK_I    : in  std_logic;
  RST_I    : in  std_logic
);
end wd2_pkg_gen_2x12to3x8;

architecture Behavioral of wd2_pkg_gen_2x12to3x8 is

  signal a_lsb_full  : std_logic := '0';
  signal a_msb_full  : std_logic := '0';
  signal b_lsb_full  : std_logic := '0';
  signal b_msb_full  : std_logic := '0';
  signal a_full      : std_logic := '0';
  signal b_full      : std_logic := '0';
  signal full        : std_logic := '0';
  signal empty       : std_logic := '0';
  
  signal wr_sel_sreg : std_logic_vector(1 downto 0) := "01";
  signal rd_sel_sreg : std_logic_vector(2 downto 0) := "001";

  signal a_sop_n     : std_logic := '0';
  signal a_eop_n     : std_logic := '0';
  signal b_sop_n     : std_logic := '0';
  signal b_eop_n     : std_logic := '0';
  
  signal a_data_reg  : std_logic_vector(11 downto 0) := (others=>'0');
  signal b_data_reg  : std_logic_vector(11 downto 0) := (others=>'0');

begin

  a_full <= a_lsb_full or a_msb_full;
  b_full <= b_lsb_full or b_msb_full;

  full    <= a_full and b_full;
  FULL_O  <= full;
  empty   <= not(a_lsb_full) and not(a_msb_full and b_lsb_full) and not(b_msb_full);
  EMPTY_O <= empty;

  output_mux : process(rd_sel_sreg, a_data_reg, b_data_reg, a_sop_n, a_eop_n, b_sop_n, b_eop_n)
  begin
    case rd_sel_sreg is
      when "001" =>
        DATA_O <= a_data_reg(7 downto 0);
        SOP_N_O <= '1';
        EOP_N_O <= '1';
      when "010" =>
        DATA_O  <= b_data_reg(3 downto 0) & a_data_reg(11 downto 8);
        SOP_N_O <= a_sop_n;
        EOP_N_O <= a_eop_n;
      when "100" =>
        DATA_O  <= b_data_reg(11 downto 4);
        SOP_N_O <= b_sop_n;
        EOP_N_O <= b_eop_n;
      when others =>
        DATA_O  <= a_data_reg(7 downto 0);
        SOP_N_O <= '1';
        EOP_N_O <= '1';
    end case;
  end process output_mux;

  update_registers : process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if RST_I = '1' then
        a_lsb_full  <= '0';
        a_msb_full  <= '0';
        b_lsb_full  <= '0';
        b_msb_full  <= '0';
        rd_sel_sreg <= "001";
        wr_sel_sreg <= "01";
      elsif ENABLE_I = '1' then
        -- read -----------------------------
        if RD_EN_I = '1' and empty = '0' then
          case rd_sel_sreg is
            when "001"  => a_lsb_full <= '0';
            when "010"  => a_msb_full <= '0';
                           b_lsb_full <= '0';
            when "100"  => b_msb_full <= '0';
            when others => null;
          end case;
          rd_sel_sreg <= rd_sel_sreg(1 downto 0) & rd_sel_sreg(2);
        end if;
        -------------------------------------
        -- write ----------------------------
        if WR_EN_I = '1' then
          case wr_sel_sreg is
            when "01" =>
              if (a_msb_full = '0') or (rd_sel_sreg = "010" and RD_EN_I = '1') then --  and empty = '0' omitted: cannot be 1 if a_msb_full is 1
                a_data_reg  <= DATA_I;
                a_lsb_full  <= '1';
                a_msb_full  <= '1';
                a_sop_n     <= SOP_N_I;
                a_eop_n     <= EOP_N_I;
                wr_sel_sreg <= wr_sel_sreg(0) & wr_sel_sreg(1);
              end if;
            when "10" =>
              if  (b_msb_full = '0') or (rd_sel_sreg = "100" and RD_EN_I = '1')  then  --  and empty = '0' omitted: cannot be 1 if b_msb_full is 1
                b_data_reg  <= DATA_I;
                b_lsb_full  <= '1';
                b_msb_full  <= '1';
                b_sop_n     <= SOP_N_I;
                b_eop_n     <= EOP_N_I;
                wr_sel_sreg <= wr_sel_sreg(0) & wr_sel_sreg(1);
              end if;
            when others =>
              null;
          end case;
        end if;
        -------------------------------------
      end if;
    end if;
  end process update_registers;

end Behavioral;
