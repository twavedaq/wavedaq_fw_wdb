---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  LED Controler
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  16.12.2015 14:23:00
--
--  Description :  Controls the behavior of the RGB LED on the front panel of the
--                 WaveDream2 board.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

entity util_led_ctrl is
  generic (
    CGN_NR_OF_PLL_LOCKS : integer := 9;
    CGN_CLK_PERIOD      : real    := 20.0; -- ns
    CGN_MIN_TIME        : real    := 50000000.0 -- ns
  );
  port (
    LED_R_N_O    : out std_logic;
    LED_G_N_O    : out std_logic;
    LED_B_N_O    : out std_logic;
    LED_R_CTRL_I : in  std_logic;
    LED_G_CTRL_I : in  std_logic;
    LED_B_CTRL_I : in  std_logic;
    PLL_LOCK_I   : in  std_logic_vector(CGN_NR_OF_PLL_LOCKS-1 downto 0);
    RESET_I      : in  std_logic;
    CLK_I        : in  std_logic
  );
end util_led_ctrl;

architecture behavioral of util_led_ctrl is

  constant C_COUNTER_INIT    : integer := INTEGER(CGN_MIN_TIME/CGN_CLK_PERIOD);
  constant C_COUNTER_WIDTH   : integer := log2ceil(C_COUNTER_INIT);
  constant C_COUNTER_INIT_LV : std_logic_vector := CONV_STD_LOGIC_VECTOR(C_COUNTER_INIT, C_COUNTER_WIDTH);
  constant C_ALL_PLLS_LOCKED : std_logic_vector(CGN_NR_OF_PLL_LOCKS-1 downto 0) := (others=>'1');
  
  type type_drs_led_state is (s_idle, s_hold);
  signal drs_led_state   : type_drs_led_state;

  signal led_b           : std_logic := '0';
  signal drs_led_counter : std_logic_vector(C_COUNTER_WIDTH-1 downto 0) := (others=>'0');

begin

  -- led pulse stretched
  proc_drs_red_led: process(CLK_I)
  begin

    if rising_edge(CLK_I) then
      if RESET_I = '1' then
        led_b           <= '0';
        drs_led_counter <= (others=>'0');
        drs_led_state   <= s_idle;
      else
        -- led timer counter
        if drs_led_counter > 0 then
          drs_led_counter <= drs_led_counter - 1;
        end if;
        -- fsm
        case drs_led_state is
        when s_idle =>
          led_b <= LED_B_CTRL_I;
          if led_b /= LED_B_CTRL_I then
            drs_led_counter <= C_COUNTER_INIT_LV;
            drs_led_state <= s_hold;
          end if;
        when s_hold =>
          if drs_led_counter = 0 then
            drs_led_state <= s_idle;
          end if;
        end case;
      end if;
    end if;
    
  end process;

  simple_led_logic : process(LED_R_CTRL_I, LED_G_CTRL_I, PLL_LOCK_I, led_b, drs_led_state)
  begin
    if (LED_R_CTRL_I = '0') or (PLL_LOCK_I /= C_ALL_PLLS_LOCKED) then
      LED_R_N_O <= '0';
      LED_G_N_O <= '1';
      LED_B_N_O <= '1';
    elsif led_b = '0' or drs_led_state = s_hold then
      LED_R_N_O <= '1';
      LED_G_N_O <= '1';
      LED_B_N_O <= led_b;
    else
      LED_R_N_O <= '1';
      LED_G_N_O <= LED_G_CTRL_I;
      LED_B_N_O <= '1';
    end if;
  end process simple_led_logic;

end architecture behavioral;