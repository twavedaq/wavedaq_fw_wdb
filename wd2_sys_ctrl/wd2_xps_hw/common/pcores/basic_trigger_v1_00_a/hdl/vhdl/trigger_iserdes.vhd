---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  ISERDES for higher trigger time resolution
--
--  Project :  WaveDream2
--
--  PCB  :  WaveDream2
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  28.11.2016 15:25:23
--
--  Description :  ISERDES unit sampling the trigger (comparator) input of each channel
--                 of the WaveDream2 Board.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_MISC.AND_REDUCE;

library basic_trigger_v1_00_a;
use basic_trigger_v1_00_a.trigger_iserdes_channel;
use basic_trigger_v1_00_a.active_ch_single;

library UNISIM;
use UNISIM.VComponents.all;

entity trigger_iserdes is
  Generic
  (
    CGN_NR_OF_CHANNELS : integer := 16;
    CGN_RATIO          : integer :=  8
  );
  Port
  (
    SERIAL_TRIGGER_I    : in  std_logic_vector(CGN_NR_OF_CHANNELS-1 downto 0);
    PARALLEL_TRIGGER_O  : out std_logic_vector(CGN_NR_OF_CHANNELS*CGN_RATIO-1 downto 0);
    BIT_CLK_I           : in  std_logic; -- Directly from PLL
    DIV_CLK_I           : in  std_logic; -- Global Clock
    PLL_LOCKED_I        : in  std_logic; -- From PLL
    LOCKED_O            : out std_logic;
    RESET_I             : in  std_logic;
    -- Triggered Channels within window
    ACTIVE_CH_O             : out std_logic_vector(CGN_NR_OF_CHANNELS-1 downto 0);
    ACTIVE_CH_WINDOW_I      : in  std_logic_vector(7 downto 0);
    ACTIVE_CH_POLARITY_I    : in  std_logic -- 1 = positive, 0 = inverted
 );
end trigger_iserdes;

architecture structural of trigger_iserdes is

  signal strobe           : std_logic_vector(1 downto 0) := (others=>'0');
  signal bit_clk          : std_logic_vector(1 downto 0) := (others=>'0');
  signal locked           : std_logic_vector(1 downto 0) := (others=>'0');
  signal parallel_trigger : std_logic_vector(CGN_NR_OF_CHANNELS*CGN_RATIO-1 downto 0) := (others=>'0');

begin

  bufpll_gen : for i in 0 to 1 generate
  begin

  BUFPLL_inst : BUFPLL
  generic map (
    DIVIDE => CGN_RATIO, -- DIVCLK divider (1-8)
    ENABLE_SYNC => TRUE  -- Enable synchrnonization between PLL and GCLK (TRUE/FALSE)
  )
  port map (
    IOCLK        => bit_clk(i),       -- 1-bit output: Output I/O clock
    LOCK         => locked(i),      -- 1-bit output: Synchronized LOCK output
    SERDESSTROBE => strobe(i),        -- 1-bit output: Output SERDES strobe (connect to ISERDES2/OSERDES2)
    GCLK         => DIV_CLK_I,     -- 1-bit input: BUFG clock input
    LOCKED       => PLL_LOCKED_I,  -- 1-bit input: LOCKED input from PLL
    PLLIN        => BIT_CLK_I      -- 1-bit input: Clock input from PLL
  );

  end generate;

  serdes_channels : for i in 0 to CGN_NR_OF_CHANNELS-1 generate
  begin

    trigger_iserdes_channel_inst : entity basic_trigger_v1_00_a.trigger_iserdes_channel
    Generic map
    (
      CGN_RATIO => CGN_RATIO
    )
    Port map
    (
      SERIAL_TRIGGER_I   => SERIAL_TRIGGER_I(i),
      PARALLEL_TRIGGER_O => parallel_trigger((i+1)*CGN_RATIO-1 downto i*CGN_RATIO),
      BIT_CLK_I          => bit_clk(i / 8),
      DIV_CLK_I          => DIV_CLK_I,
      STROBE_I           => strobe(i / 8),
      RESET_I            => RESET_I
    );
    
    active_channels : entity basic_trigger_v1_00_a.active_ch_single
    port map(
      COMP_SERDES_I         => parallel_trigger((i+1)*8-1 downto i*8),
      ACTIVE_CH_O           => ACTIVE_CH_O(i),
      TICKS_IN_DRS_WINDOW_I => ACTIVE_CH_WINDOW_I,
      POLARITY_I            => ACTIVE_CH_POLARITY_I,
      CLK_I                 => DIV_CLK_I
    );
    
  end generate;

  LOCKED_O           <= AND_REDUCE(locked);
  PARALLEL_TRIGGER_O <= parallel_trigger;

end structural;

