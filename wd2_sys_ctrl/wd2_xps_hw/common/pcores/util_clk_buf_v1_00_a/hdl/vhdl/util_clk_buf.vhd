---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Clock Buffer
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was tested with)
--
--  Author  :  Elmar Schmid (Author of generation script)
--  Created :  30.06.2015 09:26:59
--
--  Description :  Clock buffer to be inserted in xps designs.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
Library UNISIM;
use UNISIM.vcomponents.all;

entity util_clk_buf is
  generic (
    CGN_BUFFER_TYPE   : string   := "BUFG";   -- "BUFGMUX", "BUFGMUX_1", "BUFG", "BUFGCE", "BUFGCE_1", "BUFH", "BUFIO2", "BUFIO2_2CLK", "BUFPLL", "BUFIO2FB"
    CGN_CLK_SEL_TYPE  : string   := "SYNC";   -- "SYNC", "ASYNC"
    CGN_DIVIDE        : integer  := 1;        -- BUFIO2,BUFPLL=1:8; BUFIO2_2CLK=3:8
    CGN_DIVIDE_BYPASS : boolean  := TRUE;
    CGN_I_INVERT      : boolean  := FALSE;
    CGN_USE_DOUBLER   : boolean  := FALSE
  );
  port (
    I0_I           : in  std_logic;
    I1_I           : in  std_logic;
    S_I            : in  std_logic;
    CE_I           : in  std_logic;
    O_O            : out std_logic;
    GCLK_I         : in  std_logic;
    PLLIN0_I       : in  std_logic;
    PLLIN1_I       : in  std_logic;
    LOCKED_I       : in  std_logic;
    LOCK_O         : out std_logic;
    DIVCLK_O       : out std_logic;
    IOCLK_O        : out std_logic;
    SERDESSTROBE_O : out std_logic
  );
end util_clk_buf;

architecture behavioral of util_clk_buf is

begin

  bufgmux_gen : if CGN_BUFFER_TYPE = "BUFGMUX" generate
    bufgmux_inst : BUFGMUX
    generic map (
      CLK_SEL_TYPE => "SYNC"  -- Glitchles ("SYNC") or fast ("ASYNC") clock switch-over
    )
    port map (
      O  => O_O,    -- 1-bit output: Clock buffer output
      I0 => I0_I,   -- 1-bit input:  Clock buffer input (S=0)
      I1 => I1_I,   -- 1-bit input:  Clock buffer input (S=1)
      S  => S_I     -- 1-bit input:  Clock buffer select
    );
  end generate bufgmux_gen;

  bufgmux_1_gen : if CGN_BUFFER_TYPE = "BUFGMUX_1" generate
    bufgmux_1_inst : BUFGMUX_1
    generic map (
      CLK_SEL_TYPE => "SYNC"  -- Glitchles ("SYNC") or fast ("ASYNC") clock switch-over
    )
    port map (
      O  => O_O,    -- 1-bit output: Clock buffer output
      I0 => I0_I,   -- 1-bit input:  Clock buffer input
      I1 => I1_I,   -- 1-bit input:  Clock buffer input
      S  => S_I     -- 1-bit input:  Clock buffer select
    );
  end generate bufgmux_1_gen;

  bufg_gen : if CGN_BUFFER_TYPE = "BUFG" generate
    bufg_inst : BUFG
    port map (
      O => O_O,   -- 1-bit output: Clock buffer output
      I => I0_I   -- 1-bit input:  Clock buffer input
    );
  end generate bufg_gen;

  bufgce_gen : if CGN_BUFFER_TYPE = "BUFGCE" generate
    bufgce_inst : BUFGCE
    port map (
      O  => O_O,    -- 1-bit output: Clock buffer output
      CE => CE_I,   -- 1-bit input:  Clock buffer select
      I  => I0_I    -- 1-bit input:  Clock buffer input
    );
  end generate bufgce_gen;

  bufgce_1_gen : if CGN_BUFFER_TYPE = "BUFGCE_1" generate
    bufgce_1_inst : BUFGCE_1
    port map (
      O  => O_O,    -- 1-bit output: Clock buffer output
      CE => CE_I,   -- 1-bit input:  Clock buffer select
      I  => I0_I    -- 1-bit input:  Clock buffer input
    );
  end generate bufgce_1_gen;

  bufh_gen : if CGN_BUFFER_TYPE = "BUFH" generate
    bufh_inst : BUFH
    port map (
      O => O_O, -- 1-bit Clock Output
      I => I0_I -- 1-bit Clock Input
    );
  end generate bufh_gen;

  bufio2_gen : if CGN_BUFFER_TYPE = "BUFIO2" generate
    bufio2_inst : BUFIO2
    generic map (
      DIVIDE        => CGN_DIVIDE,        -- Set the DIVCLK divider divide-by value.
      DIVIDE_BYPASS => CGN_DIVIDE_BYPASS, -- DIVCLK output sourced from Divider (FALSE) or from I input, bypassing Divider (TRUE).
      I_INVERT      => CGN_I_INVERT,
      USE_DOUBLER   => CGN_USE_DOUBLER
    )
    port map (
      DIVCLK        => DIVCLK_O,       -- 1-bit Output divided clock
      IOCLK         => IOCLK_O,        -- 1-bit Output clock
      SERDESSTROBE  => SERDESSTROBE_O, -- 1-bit Output SERDES strobe (Clock Enable)
      I             => I0_I            -- 1-bit Clock input
    );
  end generate bufio2_gen;

  bufio2_2clk_gen : if CGN_BUFFER_TYPE = "BUFIO2_2CLK" generate
    bufio2_2clk_inst : BUFIO2_2CLK
    generic map (
      DIVIDE        => CGN_DIVIDE      -- Set the DIVCLK divider divide-by value.
    )
    port map (
      DIVCLK        => DIVCLK_O,       -- 1-bit Output divided clock
      IOCLK         => IOCLK_O,        -- 1-bit Output clock
      SERDESSTROBE  => SERDESSTROBE_O, -- 1-bit Output SERDES strobe (Clock Enable)
      I             => I0_I,           -- 1-bit Clock input (BUFG)
      IB            => I1_I            -- 1-bit Secondary clock input
    );
   end generate bufio2_2clk_gen;

  bufpll_gen : if CGN_BUFFER_TYPE = "BUFPLL" generate
    bufpll_inst : BUFPLL
    generic map (
      DIVIDE       => CGN_DIVIDE      -- Set the PLLIN divider divide-by value for SERDESSTROBE.
    )
    port map (
      IOCLK        => IOCLK_O,        -- 1-bit Output PLL clock
      LOCK         => LOCK_O,         -- 1-bit Synchronized LOCK output
      SERDESSTROBE => SERDESSTROBE_O, -- 1-bit Output SERDES strobe (clock enable)
      GCLK         => GCLK_I,         -- 1-bit GCLK clock input
      LOCKED       => LOCKED_I,       -- 1-bit LOCKED sign from PLL input
      PLLIN        => PLLIN0_I        -- 1-bit PLL clock input
    );
  end generate bufpll_gen;

  bufio2fb_gen : if CGN_BUFFER_TYPE = "BUFIO2FB" generate
    bufio2fb_inst : BUFIO2FB
    generic map (
      DIVIDE_BYPASS => CGN_DIVIDE_BYPASS -- DIVCLK output sourced from Divider (FALSE) or from I input, bypassing Divider (TRUE) Note: if FALSE, also need to set CLKDIV DIVIDE value to 1.
    )
    port map (
      O => O_O,   -- 1-bit Output feedback clock
      I => I0_I   -- 1-bit Feedback clock input
    );
  end generate bufio2fb_gen;

end architecture behavioral;