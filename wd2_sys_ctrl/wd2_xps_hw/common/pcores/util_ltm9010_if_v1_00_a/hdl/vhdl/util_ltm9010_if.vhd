----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:    15:32:53 05/11/2011
-- Design Name:
-- Module Name:    adc_interface - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library util_ltm9010_if_v1_00_a;
use util_ltm9010_if_v1_00_a.adc_clock;
use util_ltm9010_if_v1_00_a.adc_data;
use util_ltm9010_if_v1_00_a.adc_frame;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

entity util_ltm9010_if is
generic
(
  CGN_ON_CHIP_TERM_CLOCK : boolean := TRUE;
  CGN_ON_CHIP_TERM_FRAME : boolean := TRUE;
  CGN_ON_CHIP_TERM_DATA  : boolean := TRUE;
  CGN_NUM_SERIAL_BITS    : integer := 12
);
port
(
  -- ADC Fast bit clock (ADCLK x 6)
  ADC_LCLK_P_I       : IN std_logic;
  ADC_LCLK_N_I       : IN std_logic;

  -- ADCLK (Frame Signal) same alignment as data
  ADC_ADCLK_P_I      : IN std_logic;
  ADC_ADCLK_N_I      : IN std_logic;

  -- ADC Data
  ADC_DATA_P_I       : IN std_logic_vector(1 to 8);
  ADC_DATA_N_I       : IN std_logic_vector(1 to 8);

  -- user interface
  DATA1_O            : OUT std_logic_vector(CGN_NUM_SERIAL_BITS-1 downto 0);
  DATA2_O            : OUT std_logic_vector(CGN_NUM_SERIAL_BITS-1 downto 0);
  DATA3_O            : OUT std_logic_vector(CGN_NUM_SERIAL_BITS-1 downto 0);
  DATA4_O            : OUT std_logic_vector(CGN_NUM_SERIAL_BITS-1 downto 0);
  DATA5_O            : OUT std_logic_vector(CGN_NUM_SERIAL_BITS-1 downto 0);
  DATA6_O            : OUT std_logic_vector(CGN_NUM_SERIAL_BITS-1 downto 0);
  DATA7_O            : OUT std_logic_vector(CGN_NUM_SERIAL_BITS-1 downto 0);
  DATA8_O            : OUT std_logic_vector(CGN_NUM_SERIAL_BITS-1 downto 0);
  FRAME_O            : OUT std_logic_vector(CGN_NUM_SERIAL_BITS-1 downto 0);
  ALIGNED_O          : OUT std_logic;
  BIT_CLK_O          : OUT std_logic;
  STROBE_O           : OUT std_logic;

  ADC_DIV_CLK_EXT_I  : IN  std_logic;

  RESET_I            : IN  std_logic;
  BITSLIP_EN_I       : IN  std_logic;
  EN_I               : IN  std_logic;
  TEST_PATTERN_EN_I  : IN  std_logic;
  PLL_LOCKED_O       : OUT std_logic;
  SYS_CLK_I          : IN  std_logic
);
end util_ltm9010_if;

architecture Behavioral of util_ltm9010_if is

  type type_adc_data_word is array (1 to 8) of std_logic_vector(CGN_NUM_SERIAL_BITS-1 downto 0);
  signal adc_data_word        : type_adc_data_word;
  signal frame                : std_logic_vector(CGN_NUM_SERIAL_BITS-1 downto 0);
  signal bitslip              : std_logic;

  signal store_msbs           : std_logic;
  signal msb_reg_ce           : std_logic;
  signal bitslip_en           : std_logic;
  signal adc_halfword_div_clk : std_logic;
  signal adc_bit_clk          : std_logic;
  signal adc_bit_clk_n        : std_logic;
  signal adc_serdes_strobe    : std_logic;
  signal enable               : std_logic;
  signal serdes_reset         : std_logic;
  signal serdes_rst_sreg      : std_logic_vector(5 downto 0);
  signal pll_locked           : std_logic;
  signal test_pattern_en      : std_logic;

begin

  cdc_sync_inst_0: cdc_sync
  generic map
  (
    CGN_DATA_WIDTH           =>  2,
    --CGN_DATA_WIDTH           =>  4,

    CGN_USE_INPUT_REG_A      =>  1,
    CGN_USE_OUTPUT_REG_A     =>  0,
    CGN_USE_GRAY_CONVERSION  =>  0,
    CGN_NUM_SYNC_REGS_B      =>  1,
    CGN_NUM_OUTPUT_REGS_B    =>  1,

    CGN_GRAY_TO_BIN_STYLE    =>  0   --  0: serial structure
                                     --  1: Kogge-Stone parallel prefix
                                     --  2: Sklansky parallel-prefix propagate-lookahead structure
  )
  PORT MAP
  (
    CLK_A_I => SYS_CLK_I,
    CLK_B_I => adc_halfword_div_clk,
    PORT_A_I(0) => BITSLIP_EN_I,
    PORT_A_I(1) => TEST_PATTERN_EN_I,
    PORT_B_O(0) => bitslip_en,
    PORT_B_O(1) => test_pattern_en
  );

  enable <= '1';

  process(adc_halfword_div_clk, RESET_I, pll_locked)
  begin
    if RESET_I = '1' or pll_locked = '0' then
      serdes_rst_sreg <= (others=>'1');
    elsif rising_edge(adc_halfword_div_clk) then
      serdes_rst_sreg <= '0' & serdes_rst_sreg(5 downto 1);
    end if;
  end process;
  serdes_reset <= serdes_rst_sreg(0);

  inst_adc_clock: entity util_ltm9010_if_v1_00_a.adc_clock
  generic map
  (
    CGN_ON_CHIP_TERM       => CGN_ON_CHIP_TERM_CLOCK
  )
  PORT MAP
  (
    ADC_LCLK_P_I           => ADC_LCLK_P_I,
    ADC_LCLK_N_I           => ADC_LCLK_N_I,
    RESET_I                => RESET_I,
    PLL_LOCKED_O           => pll_locked,
    ADC_BIT_CLK_O          => adc_bit_clk,
    ADC_BIT_CLK_N_O        => adc_bit_clk_n,
    ADC_HALFWORD_DIV_CLK_O => adc_halfword_div_clk,
    ADC_SERDES_STROBE_O    => adc_serdes_strobe
  );
  BIT_CLK_O <= adc_bit_clk;
  STROBE_O  <= adc_serdes_strobe;

  Inst_adc_frame: entity util_ltm9010_if_v1_00_a.adc_frame
  generic map
  (
    CGN_ON_CHIP_TERM       => CGN_ON_CHIP_TERM_FRAME,
    CGN_NUM_SERIAL_BITS    => CGN_NUM_SERIAL_BITS
  )
  PORT MAP
  (
    ADC_HALFWORD_DIV_CLK_I => adc_halfword_div_clk,
    ADC_BIT_CLK_I          => adc_bit_clk,
    ADC_BIT_CLK_N_I        => adc_bit_clk_n,
    ADC_SERDES_STROBE_I    => adc_serdes_strobe,
    ADC_ADCLK_P_I          => ADC_ADCLK_P_I,
    ADC_ADCLK_N_I          => ADC_ADCLK_N_I,
    ADC_DIV_CLK_EXT_I      => ADC_DIV_CLK_EXT_I,
    RESET_I                => serdes_reset,
    EN_I                   => enable,
    BITSLIP_EN_I           => bitslip_en,
    FRAME_O                => FRAME_O,
    MSB_REG_CE_O           => msb_reg_ce,
    BITSLIP_O              => bitslip,
    ALIGNED_O              => ALIGNED_O
  );

  adc_data_gen : for i in 1 to 8 generate
    -- defining HU_SET for use of relative constraints in data component
    constant hu_set_str : string := "adc_data_hu_set_" & integer'image(i);
    attribute HU_SET: string;
    attribute HU_SET of Inst_adc_data : label is hu_set_str;
  begin
    Inst_adc_data : entity util_ltm9010_if_v1_00_a.adc_data
    generic map
    (
      CGN_ON_CHIP_TERM       => CGN_ON_CHIP_TERM_DATA,
      CGN_NUM_SERIAL_BITS    => CGN_NUM_SERIAL_BITS
    )
    PORT MAP
    (
      ADC_HALFWORD_DIV_CLK_I => adc_halfword_div_clk,
      ADC_BIT_CLK_I          => adc_bit_clk,
      ADC_BIT_CLK_N_I        => adc_bit_clk_n,
      ADC_SERDES_STROBE_I    => adc_serdes_strobe,
      ADC_DATA_P_I           => ADC_DATA_P_I(i),
      ADC_DATA_N_I           => ADC_DATA_N_I(i),
      ADC_DATA_O             => adc_data_word(i),
      ADC_DIV_CLK_EXT_I      => ADC_DIV_CLK_EXT_I,
      RESET_I                => serdes_reset,
      EN_I                   => enable,
      TEST_PATTERN_EN_I      => test_pattern_en,
      MSB_REG_CE_I           => msb_reg_ce,
      BITSLIP_I              => bitslip
    );
  end generate;

  DATA1_O <= adc_data_word(1);
  DATA2_O <= adc_data_word(2);
  DATA3_O <= adc_data_word(3);
  DATA4_O <= adc_data_word(4);
  DATA5_O <= adc_data_word(5);
  DATA6_O <= adc_data_word(6);
  DATA7_O <= adc_data_word(7);
  DATA8_O <= adc_data_word(8);

  PLL_LOCKED_O <= pll_locked;

end Behavioral;
