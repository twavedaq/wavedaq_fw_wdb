library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity adc_data is
generic
(
  CGN_ON_CHIP_TERM       : boolean := TRUE;
  CGN_NUM_SERIAL_BITS    : integer := 12
);
Port
(
  -- ADC Bitclock divided by 3
  ADC_HALFWORD_DIV_CLK_I : IN  std_logic;
  ADC_BIT_CLK_I          : IN  std_logic;
  ADC_BIT_CLK_N_I        : IN  std_logic;
  ADC_SERDES_STROBE_I    : IN  std_logic;

  -- ADC Data
  ADC_DATA_P_I           : IN  std_logic;
  ADC_DATA_N_I           : IN  std_logic;

  ADC_DATA_O             : OUT std_logic_vector(CGN_NUM_SERIAL_BITS-1 downto 0);

  ADC_DIV_CLK_EXT_I      : IN  std_logic;

  RESET_I                : IN  std_logic;
  EN_I                   : IN  std_logic;

  TEST_PATTERN_EN_I      : IN  std_logic;
  MSB_REG_CE_I           : IN  std_logic;
  BITSLIP_I              : IN  std_logic
);
end adc_data;


architecture Behavioral of adc_data is

  constant C_TEST_PATTERN_32_0  : std_logic_vector(31 downto 0) := x"55555555";
  constant C_TEST_PATTERN_32_1  : std_logic_vector(31 downto 0) := x"AAAAAAAA";
  constant C_ADC_TEST_PATTERN_0 : std_logic_vector(CGN_NUM_SERIAL_BITS-1 downto 0) := C_TEST_PATTERN_32_0(CGN_NUM_SERIAL_BITS-1 downto 0);
  constant C_ADC_TEST_PATTERN_1 : std_logic_vector(CGN_NUM_SERIAL_BITS-1 downto 0) := C_TEST_PATTERN_32_1(CGN_NUM_SERIAL_BITS-1 downto 0);

  signal adc_data_ser       : std_logic;

  signal serdes_data_out    : std_logic_vector(CGN_NUM_SERIAL_BITS/2-1 downto 0);
  signal frame_msbs         : std_logic_vector(CGN_NUM_SERIAL_BITS/2-1 downto 0);
  signal frame_lsbs         : std_logic_vector(CGN_NUM_SERIAL_BITS/2-1 downto 0);
  signal frame_vec          : std_logic_vector(CGN_NUM_SERIAL_BITS-1 downto 0);
  signal adc_data_word      : std_logic_vector(CGN_NUM_SERIAL_BITS-1 downto 0);

  signal clk_in_int         : std_logic;
  signal clk_in_int_p       : std_logic;
  signal clk_in_int_n       : std_logic;
  signal clk_div            : std_logic;
  signal clk_div_int        : std_logic;
  signal clk_in_int_buf     : std_logic;
  signal clk_in_int_p_delay : std_logic;
  signal clk_in_int_delay   : std_logic;
  signal clk_in_int_inv     : std_logic;
  signal clk_in_int_n_delay : std_logic;

  signal test_pattern       : std_logic := '0';

  signal iserdes_q          : std_logic_vector(7 downto 0) := (others => '0');
  signal icascade           : std_logic;
  signal slave_shiftout     : std_logic;

  attribute RLOC : string;

begin

---------------------------------------------------------------------------------------------------
-- data input differential buffer
---------------------------------------------------------------------------------------------------

  adc_lclk_ibufds : IBUFDS
  generic map
  (
    DIFF_TERM   => CGN_ON_CHIP_TERM,
    IOSTANDARD  => "LVDS_25"
  )
  port map
  (
    I  => ADC_DATA_P_I,
    IB => ADC_DATA_N_I,
    O  => adc_data_ser
  );

  -- Instantiate the serdes primitive
  ----------------------------------
  -- declare the iserdes
  iserdes2_master : ISERDES2
   generic map (
     BITSLIP_ENABLE => TRUE,
     DATA_RATE      => "SDR",
     DATA_WIDTH     => CGN_NUM_SERIAL_BITS/2,
     INTERFACE_TYPE => "RETIMED", --"NETWORKING_PIPELINED", --"NETWORKING",
     SERDES_MODE    => "MASTER")
   port map (
     Q1         => iserdes_q(3), -- 4. bit in time
     Q2         => iserdes_q(2), -- 5. bit in time
     Q3         => iserdes_q(1), -- 6. bit in time
     Q4         => iserdes_q(0), -- 7. bit in time (LSB @ AD9637)
     SHIFTOUT   => icascade,
     INCDEC     => open,
     VALID      => open,
     BITSLIP    => BITSLIP_I,       -- 1-bit Invoke Bitslip. This can be used with any DATA_WIDTH, cascaded or not.
                                  -- The amount of bitslip is fixed by the DATA_WIDTH selection.
     CE0        => EN_I,   -- 1-bit Clock enable input
     CLK0       => ADC_BIT_CLK_I, -- 1-bit IO Clock network input. Optionally Invertible. This is the primary clock
                                   -- input used when the clock doubler circuit is not engaged (see DATA_RATE
                                   -- attribute).
     CLK1       => '0',--ADC_BIT_CLK_N_I, -- 1-bit Optionally invertible IO Clock network input. Timing note: CLK1 should be
                                   -- 180 degrees out of phase with CLK0.
     CLKDIV     => ADC_HALFWORD_DIV_CLK_I,                        -- 1-bit Global clock network input. This is the clock for the fabric domain.
     D          => adc_data_ser, -- 1-bit Input signal from IOB.
     IOCE       => ADC_SERDES_STROBE_I,                       -- 1-bit Data strobe signal derived from BUFIO CE. Strobes data capture for
                                                      -- NETWORKING and NETWORKING_PIPELINES alignment modes.

     RST        => RESET_I,        -- 1-bit Asynchronous reset only.
     SHIFTIN    => slave_shiftout,


    -- unused connections
     FABRICOUT  => open,
     CFB0       => open,
     CFB1       => open,
     DFB        => open);

  iserdes2_slave : ISERDES2
   generic map (
     BITSLIP_ENABLE => TRUE,
     DATA_RATE      => "SDR",
     DATA_WIDTH     => CGN_NUM_SERIAL_BITS/2,
     INTERFACE_TYPE => "RETIMED", --"NETWORKING_PIPELINED", --"NETWORKING",
     SERDES_MODE    => "SLAVE")
   port map (
    Q1         => iserdes_q(7), -- ignored
    Q2         => iserdes_q(6), -- 1. bit in time (MSB @ AD9637)
    Q3         => iserdes_q(5), -- 2. bit in time
    Q4         => iserdes_q(4), -- 3. bit in time
    SHIFTOUT   => slave_shiftout,
    BITSLIP    => BITSLIP_I,      -- 1-bit Invoke Bitslip. This can be used with any DATA_WIDTH, cascaded or not.
                                -- The amount of bitslip is fixed by the DATA_WIDTH selection.
    CE0        => EN_I,   -- 1-bit Clock enable input
    CLK0       => ADC_BIT_CLK_I, -- 1-bit IO Clock network input. Optionally Invertible. This is the primary clock
                                  -- input used when the clock doubler circuit is not engaged (see DATA_RATE
                                  -- attribute).
    CLK1       => '0',--ADC_BIT_CLK_N_I, -- 1-bit Optionally invertible IO Clock network input. Timing note: CLK1 should be
                                  -- 180 degrees out of phase with CLK0.
    CLKDIV     => ADC_HALFWORD_DIV_CLK_I,                        -- 1-bit Global clock network input. This is the clock for the fabric domain.
    D          => '0',            -- 1-bit Input signal from IOB.
    IOCE       => ADC_SERDES_STROBE_I,   -- 1-bit Data strobe signal derived from BUFIO CE. Strobes data capture for
                                  -- NETWORKING and NETWORKING_PIPELINES alignment modes.

    RST        => RESET_I,       -- 1-bit Asynchronous reset only.
    SHIFTIN    => icascade,
    -- unused connections
    FABRICOUT  => open,
    CFB0       => open,
    CFB1       => open,
    DFB        => open);

  serdes_data_out <= iserdes_q(CGN_NUM_SERIAL_BITS/2-1 downto 0);

  -- msb/lsb store
  process(ADC_HALFWORD_DIV_CLK_I)
  begin
    if rising_edge(ADC_HALFWORD_DIV_CLK_I) then
      if MSB_REG_CE_I = '1' then
        frame_msbs <= serdes_data_out;
      else
        frame_lsbs <= serdes_data_out;
      end if;
    end if;
  end process;

  -- toggle test pattern
  process(ADC_HALFWORD_DIV_CLK_I)
  begin
    if rising_edge(ADC_HALFWORD_DIV_CLK_I) then
      if TEST_PATTERN_EN_I = '1' then
        if MSB_REG_CE_I = '1' then
          test_pattern <= not test_pattern;
        end if;
      else
        test_pattern <= '0';
      end if;
    end if;
  end process;

  clock_domain_crossing : for i in CGN_NUM_SERIAL_BITS-1 downto 0 generate
    constant C_Y_OFFSET          : integer := i/4;
    constant rloc_1st_str : string := "X0" & "Y" & integer'image(C_Y_OFFSET);
    constant rloc_2nd_str : string := "X1" & "Y" & integer'image(C_Y_OFFSET);
    signal adc_data_word_int : std_logic;
    signal adc_data_o_int    : std_logic;
    attribute RLOC of adc_data_word_int : signal is rloc_1st_str;
    attribute RLOC of adc_data_o_int    : signal is rloc_2nd_str;
  begin

    process(ADC_HALFWORD_DIV_CLK_I)
    begin
      if rising_edge(ADC_HALFWORD_DIV_CLK_I) then
        if MSB_REG_CE_I = '1' then
          if TEST_PATTERN_EN_I = '0' then
            adc_data_word_int <= frame_vec(i);
          else
            if test_pattern = '0' then
              adc_data_word_int <= C_ADC_TEST_PATTERN_0(i);
            else
              adc_data_word_int <= C_ADC_TEST_PATTERN_1(i);
            end if;
          end if;
        end if;
      end if;
    end process;

    process(ADC_DIV_CLK_EXT_I)
    begin
      if rising_edge(ADC_DIV_CLK_EXT_I) then
        adc_data_o_int <= adc_data_word_int;
      end if;
    end process;

    ADC_DATA_O(i) <= adc_data_o_int;

  end generate clock_domain_crossing;

  frame_vec <= frame_msbs & frame_lsbs;

end Behavioral;
