
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library Unisim;
use Unisim.vcomponents.all;

entity util_pll_adv is
  generic (
    -- pll_adv parameters
    CGN_COMPENSATION          : string  := "SYSTEM_SYNCHRONOUS";  -- "SYSTEM_SYNCHRNOUS", "SOURCE_SYNCHRNOUS", "
                                        -- INTERNAL", "EXTERNAL", "DCM2PLL", "PLL2DCM"
    CGN_BANDWIDTH             : string  := "OPTIMIZED";  -- "HIGH", "LOW" or "OPTIMIZED"
    CGN_CLKOUT0_DIVIDE        : integer := 1;  -- Division factor for CLKOUT0 (1 to 128)
    CGN_CLKOUT0_DUTY_CYCLE    : real    := 0.5;  -- Duty cycle for CLKOUT0 (0.01 to 0.99)
    CGN_CLKOUT0_PHASE         : real    := 0.0;  -- Phase shift (degrees) for CLKOUT0 (0.0 to 360.0)
    CGN_CLKOUT1_DIVIDE        : integer := 1;  -- Division factor for CLKOUT1 (1 to 128)
    CGN_CLKOUT1_DUTY_CYCLE    : real    := 0.5;  -- Duty cycle for CLKOUT1 (0.01 to 0.99)
    CGN_CLKOUT1_PHASE         : real    := 0.0;  -- Phase shift (degrees) for CLKOUT1 (0.0 to 360.0)
    CGN_CLKOUT2_DIVIDE        : integer := 1;  -- Division factor for CLKOUT2 (1 to 128)
    CGN_CLKOUT2_DUTY_CYCLE    : real    := 0.5;  -- Duty cycle for CLKOUT2 (0.01 to 0.99)
    CGN_CLKOUT2_PHASE         : real    := 0.0;  -- Phase shift (degrees) for CLKOUT2 (0.0 to 360.0)
    CGN_CLKOUT3_DIVIDE        : integer := 1;  -- Division factor for CLKOUT3 (1 to 128)
    CGN_CLKOUT3_DUTY_CYCLE    : real    := 0.5;  -- Duty cycle for CLKOUT3 (0.01 to 0.99)
    CGN_CLKOUT3_PHASE         : real    := 0.0;  -- Phase shift (degrees) for CLKOUT3 (0.0 to 360.0)
    CGN_CLKOUT4_DIVIDE        : integer := 1;  -- Division factor for CLKOUT4 (1 to 128)
    CGN_CLKOUT4_DUTY_CYCLE    : real    := 0.5;  -- Duty cycle for CLKOUT4 (0.01 to 0.99)
    CGN_CLKOUT4_PHASE         : real    := 0.0;  -- Phase shift (degrees) for CLKOUT4 (0.0 to 360.0)
    CGN_CLKOUT5_DIVIDE        : integer := 1;  -- Division factor for CLKOUT5 (1 to 128)
    CGN_CLKOUT5_DUTY_CYCLE    : real    := 0.5;  -- Duty cycle for CLKOUT5 (0.01 to 0.99)
    CGN_CLKOUT5_PHASE         : real    := 0.0;  -- Phase shift (degrees) for CLKOUT5 (0.0 to 360.0)
    CGN_CLKFBOUT_MULT         : integer := 1;  -- Multiplication factor for all output clocks
    CGN_DIVCLK_DIVIDE         : integer := 1;  -- Division factor for all clocks (1 to 52)
    CGN_CLKFBOUT_PHASE        : real    := 0.0;  -- Phase shift (degrees) of all output clocks
    CGN_REF_JITTER            : real    := 0.100;  -- Input reference jitter (0.000 to 0.999 UI%)
    CGN_CLKIN1_PERIOD         : real    := 4.000;  -- Clock period (ns) of input clock on CLKIN1
    -- CGN_CLKIN2_PERIOD         : real    := 0.000;  -- Clock period (ns) of input clock on CLKIN2
    CGN_CLK_FEEDBACK          : string  := "CLKFBOUT"; -- "CLKFBOUT", "CLKOUT0"
    CGN_RESET_ON_LOSS_OF_LOCK : boolean := false;  -- Auto reset when LOCK is lost, TRUE/FALSE
    -- parameters for pcore
    CGN_CLKIN1_BUF            : boolean := false;
    -- CGN_CLKIN2_BUF          : boolean := false;
    CGN_CLKFBOUT_BUF          : boolean := false;
    CGN_CLKOUT0_BUF           : boolean := false;
    CGN_CLKOUT1_BUF           : boolean := false;
    CGN_CLKOUT2_BUF           : boolean := false;
    CGN_CLKOUT3_BUF           : boolean := false;
    CGN_CLKOUT4_BUF           : boolean := false;
    CGN_CLKOUT5_BUF           : boolean := false;
    CGN_EXT_RESET_HIGH        : boolean := true
    );
  port (
    CLKIN1_I           : in  std_logic;
    -- CLKIN2_I           : in  std_logic;
    -- CLKINSEL_I         : in  std_logic;
    CLKFBIN_I          : in  std_logic;
    CLKFBDCM_O         : out std_logic;
    CLKFBOUT_O         : out std_logic;
    CLKOUT0_O          : out std_logic;
    CLKOUT1_O          : out std_logic;
    CLKOUT2_O          : out std_logic;
    CLKOUT3_O          : out std_logic;
    CLKOUT4_O          : out std_logic;
    CLKOUT5_O          : out std_logic;
    CLKOUT0_NOBUF_O    : out std_logic;
    CLKOUT1_NOBUF_O    : out std_logic;
    CLKOUT2_NOBUF_O    : out std_logic;
    CLKOUT3_NOBUF_O    : out std_logic;
    CLKOUT4_NOBUF_O    : out std_logic;
    CLKOUT5_NOBUF_O    : out std_logic;
    CLKOUTDCM0_O       : out std_logic;
    CLKOUTDCM1_O       : out std_logic;
    CLKOUTDCM2_O       : out std_logic;
    CLKOUTDCM3_O       : out std_logic;
    CLKOUTDCM4_O       : out std_logic;
    CLKOUTDCM5_O       : out std_logic;
    DRP_DADDR_I        : in  std_logic_vector (6 downto 0);
    DRP_DI_I           : in  std_logic_vector (15 downto 0);
    DRP_DWE_I          : in  std_logic;
    DRP_DEN_I          : in  std_logic;
    DRP_DCLK_I         : in  std_logic;
    --DRP_REL_I          : in  std_logic;
    DRP_DO_O           : out std_logic_vector (15 downto 0);
    DRP_DRDY_O         : out std_logic;
    LOCKED_O           : out std_logic;
    RESET_I            : in  std_logic
    );
end util_pll_adv;

architecture STRUCT of util_pll_adv is

  signal rsti : std_logic;

  signal clkin1_buf   : std_logic;
  signal clkin2_buf   : std_logic;
  signal clkfbout_buf : std_logic;
  signal clkout0_buf  : std_logic;
  signal clkout1_buf  : std_logic;
  signal clkout2_buf  : std_logic;
  signal clkout3_buf  : std_logic;
  signal clkout4_buf  : std_logic;
  signal clkout5_buf  : std_logic;

  function UpperCase_Char(char : character) return character is
  begin
    -- If char is not an upper case letter then return char
    if char < 'a' or char > 'z' then
      return char;
    end if;
    -- Otherwise map char to its corresponding lower case character and
    -- return that
    case char is
      when 'a'    => return 'A'; when 'b' => return 'B'; when 'c' => return 'C'; when 'd' => return 'D';
      when 'e'    => return 'E'; when 'f' => return 'F'; when 'g' => return 'G'; when 'h' => return 'H';
      when 'i'    => return 'I'; when 'j' => return 'J'; when 'k' => return 'K'; when 'l' => return 'L';
      when 'm'    => return 'M'; when 'n' => return 'N'; when 'o' => return 'O'; when 'p' => return 'P';
      when 'q'    => return 'Q'; when 'r' => return 'R'; when 's' => return 'S'; when 't' => return 'T';
      when 'u'    => return 'U'; when 'v' => return 'V'; when 'w' => return 'W'; when 'x' => return 'X';
      when 'y'    => return 'Y'; when 'z' => return 'Z';
      when others => return char;
    end case;
  end UpperCase_Char;

  function UpperCase_String (s : string) return string is
    variable res               : string(s'range);
  begin  -- function LoweerCase_String
    for I in s'range loop
      res(I) := UpperCase_Char(s(I));
    end loop;  -- I
    return res;
  end function UpperCase_String;

begin

  -----------------------------------------------------------------------------
  -- handle the reset
  -----------------------------------------------------------------------------
  Rst_is_Active_High : if (CGN_EXT_RESET_HIGH = true) generate
    rsti <= RESET_I;
  end generate Rst_is_Active_High;

  Rst_is_Active_Low : if (CGN_EXT_RESET_HIGH = false) generate
    rsti <= not RESET_I;
  end generate Rst_is_Active_Low;

  PLL_ADV_inst : PLL_ADV
    generic map (
      COMPENSATION           => UpperCase_String(CGN_COMPENSATION),
      BANDWIDTH              => UpperCase_String(CGN_BANDWIDTH),
      CLKOUT0_DIVIDE         => CGN_CLKOUT0_DIVIDE,
      CLKOUT0_DUTY_CYCLE     => CGN_CLKOUT0_DUTY_CYCLE,
      CLKOUT0_PHASE          => CGN_CLKOUT0_PHASE,
      CLKOUT1_DIVIDE         => CGN_CLKOUT1_DIVIDE,
      CLKOUT1_DUTY_CYCLE     => CGN_CLKOUT1_DUTY_CYCLE,
      CLKOUT1_PHASE          => CGN_CLKOUT1_PHASE,
      CLKOUT2_DIVIDE         => CGN_CLKOUT2_DIVIDE,
      CLKOUT2_DUTY_CYCLE     => CGN_CLKOUT2_DUTY_CYCLE,
      CLKOUT2_PHASE          => CGN_CLKOUT2_PHASE,
      CLKOUT3_DIVIDE         => CGN_CLKOUT3_DIVIDE,
      CLKOUT3_DUTY_CYCLE     => CGN_CLKOUT3_DUTY_CYCLE,
      CLKOUT3_PHASE          => CGN_CLKOUT3_PHASE,
      CLKOUT4_DIVIDE         => CGN_CLKOUT4_DIVIDE,
      CLKOUT4_DUTY_CYCLE     => CGN_CLKOUT4_DUTY_CYCLE,
      CLKOUT4_PHASE          => CGN_CLKOUT4_PHASE,
      CLKOUT5_DIVIDE         => CGN_CLKOUT5_DIVIDE,
      CLKOUT5_DUTY_CYCLE     => CGN_CLKOUT5_DUTY_CYCLE,
      CLKOUT5_PHASE          => CGN_CLKOUT5_PHASE,
      CLKFBOUT_MULT          => CGN_CLKFBOUT_MULT,
      DIVCLK_DIVIDE          => CGN_DIVCLK_DIVIDE,
      CLKFBOUT_PHASE         => CGN_CLKFBOUT_PHASE,
      REF_JITTER             => CGN_REF_JITTER,
      CLKIN1_PERIOD          => CGN_CLKIN1_PERIOD,
      CLKIN2_PERIOD          => CGN_CLKIN1_PERIOD,
      CLK_FEEDBACK           => CGN_CLK_FEEDBACK,
      RESET_ON_LOSS_OF_LOCK  => CGN_RESET_ON_LOSS_OF_LOCK
      )
    port map (
      CLKIN1                => clkin1_buf,
      CLKIN2                => '0', 
      CLKFBIN               => CLKFBIN_I,
      CLKINSEL              => '1', -- 1 selects CLKIN1, and 0 selects CLKIN2
      CLKFBDCM              => CLKFBDCM_O,
      CLKFBOUT              => clkfbout_buf,
      CLKOUT0               => clkout0_buf,
      CLKOUT1               => clkout1_buf,
      CLKOUT2               => clkout2_buf,
      CLKOUT3               => clkout3_buf,
      CLKOUT4               => clkout4_buf,
      CLKOUT5               => clkout5_buf,
      CLKOUTDCM0            => CLKOUTDCM0_O,
      CLKOUTDCM1            => CLKOUTDCM1_O,
      CLKOUTDCM2            => CLKOUTDCM2_O,
      CLKOUTDCM3            => CLKOUTDCM3_O,
      CLKOUTDCM4            => CLKOUTDCM4_O,
      CLKOUTDCM5            => CLKOUTDCM5_O,
      DADDR                 => DRP_DADDR_I(4 downto 0),
      DI                    => DRP_DI_I,
      DWE                   => DRP_DWE_I,
      DEN                   => DRP_DEN_I,
      DCLK                  => DRP_DCLK_I,
      REL                   => '0',
      DO                    => DRP_DO_O,
      DRDY                  => DRP_DRDY_O,
      LOCKED                => LOCKED_O,
      RST                   => rsti    -- Asynchronous PLL reset
    );

  -----------------------------------------------------------------------------
  -- Clkin1 and Clkin2
  -----------------------------------------------------------------------------
  Using_BUFG_for_CLKIN1 : if (CGN_CLKIN1_BUF) generate
    CLKIN1_BUFG_INST    : BUFG
      port map (
        I => CLKIN1_I,
        O => clkin1_buf);
  end generate Using_BUFG_for_CLKIN1;

  No_BUFG_for_CLKIN1 : if (not CGN_CLKIN1_BUF) generate
    clkin1_buf <= CLKIN1_I;
  end generate No_BUFG_for_CLKIN1;

-- Using_BUFG_for_CLKIN2 : if (CGN_CLKIN2_BUF) generate
-- CLKIN2_BUFG_INST : BUFG
-- port map (
-- I => CLKIN2,
-- O => clkin2_buf);
-- end generate Using_BUFG_for_CLKIN2;
--
-- No_BUFG_for_CLKIN2 : if (not CGN_CLKIN2_BUF) generate
-- clkin2_buf <= CLKIN2;
-- end generate No_BUFG_for_CLKIN2;

  -----------------------------------------------------------------------------
  -- Clkfb
  -----------------------------------------------------------------------------
  Using_BUFG_for_CLKFBOUT : if (CGN_CLKFBOUT_BUF) generate
    CLKFB_BUFG_INST       : BUFG
      port map (
        I => clkfbout_buf,
        O => CLKFBOUT_O);
  end generate Using_BUFG_for_CLKFBOUT;

  No_BUFG_for_CLKFBOUT : if (not CGN_CLKFBOUT_BUF) generate
    CLKFBOUT_O <= clkfbout_buf;
  end generate No_BUFG_for_CLKFBOUT;

  -----------------------------------------------------------------------------
  -- ClkOut0
  -----------------------------------------------------------------------------
  Using_BUFG_for_CLKOUT0 : if (CGN_CLKOUT0_BUF) generate

    CLKOUT0_BUFG_INST : BUFG
      port map (
        I => clkout0_buf,
        O => CLKOUT0_O);
  end generate Using_BUFG_for_CLKOUT0;

  No_BUFG_for_CLKOUT0 : if (not CGN_CLKOUT0_BUF) generate
    CLKOUT0_O <= clkout0_buf;
  end generate No_BUFG_for_CLKOUT0;

  CLKOUT0_NOBUF_O <= clkout0_buf;

  -----------------------------------------------------------------------------
  -- ClkOut1
  -----------------------------------------------------------------------------
  Using_BUFG_for_CLKOUT1 : if (CGN_CLKOUT1_BUF) generate

    CLKOUT1_BUFG_INST : BUFG
      port map (
        I => clkout1_buf,
        O => CLKOUT1_O);
  end generate Using_BUFG_for_CLKOUT1;

  No_BUFG_for_CLKOUT1 : if (not CGN_CLKOUT1_BUF) generate
    CLKOUT1_O <= clkout1_buf;
  end generate No_BUFG_for_CLKOUT1;

  CLKOUT1_NOBUF_O <= clkout1_buf;

  -----------------------------------------------------------------------------
  -- ClkOut2
  -----------------------------------------------------------------------------
  Using_BUFG_for_CLKOUT2 : if (CGN_CLKOUT2_BUF) generate

    CLKOUT2_BUFG_INST : BUFG
      port map (
        I => clkout2_buf,
        O => CLKOUT2_O);
  end generate Using_BUFG_for_CLKOUT2;

  No_BUFG_for_CLKOUT2 : if (not CGN_CLKOUT2_BUF) generate
    CLKOUT2_O <= clkout2_buf;
  end generate No_BUFG_for_CLKOUT2;

  CLKOUT2_NOBUF_O <= clkout2_buf;

  -----------------------------------------------------------------------------
  -- ClkOut3
  -----------------------------------------------------------------------------
  Using_BUFG_for_CLKOUT3 : if (CGN_CLKOUT3_BUF) generate

    CLKOUT3_BUFG_INST : BUFG
      port map (
        I => clkout3_buf,
        O => CLKOUT3_O);
  end generate Using_BUFG_for_CLKOUT3;

  No_BUFG_for_CLKOUT3    : if (not CGN_CLKOUT3_BUF) generate
    CLKOUT3_O <= clkout3_buf;
  end generate No_BUFG_for_CLKOUT3;

  CLKOUT3_NOBUF_O <= clkout3_buf;

  -----------------------------------------------------------------------------
  -- ClkOut4
  -----------------------------------------------------------------------------
  Using_BUFG_for_CLKOUT4 : if (CGN_CLKOUT4_BUF) generate

    CLKOUT4_BUFG_INST : BUFG
      port map (
        I => clkout4_buf,
        O => CLKOUT4_O);
  end generate Using_BUFG_for_CLKOUT4;

  No_BUFG_for_CLKOUT4 : if (not CGN_CLKOUT4_BUF) generate
    CLKOUT4_O <= clkout4_buf;
  end generate No_BUFG_for_CLKOUT4;

  CLKOUT4_NOBUF_O <= clkout4_buf;

  -----------------------------------------------------------------------------
  -- ClkOut5
  -----------------------------------------------------------------------------
  Using_BUFG_for_CLKOUT5 : if (CGN_CLKOUT5_BUF) generate

    CLKOUT5_BUFG_INST : BUFG
      port map (
        I => clkout5_buf,
        O => CLKOUT5_O);
  end generate Using_BUFG_for_CLKOUT5;

  No_BUFG_for_CLKOUT5 : if (not CGN_CLKOUT5_BUF) generate
    CLKOUT5_O <= clkout5_buf;
  end generate No_BUFG_for_CLKOUT5;

  CLKOUT5_NOBUF_O <= clkout5_buf;

end STRUCT;

-------------------------------------------------------------------------------
-- dcm module wrapper for clock generator
-------------------------------------------------------------------------------

