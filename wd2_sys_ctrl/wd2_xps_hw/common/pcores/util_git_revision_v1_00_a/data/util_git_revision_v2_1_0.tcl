###################################################################
##
## Copyright(C) 2003-2006 by Xilinx, Inc. All rights reserved.
##
## chipscope_vio
##
###################################################################

#
# ELABORATE_PROC
#


proc git_rev_generate { mhsinst } {


   if {[catch {set git_root [exec git rev-parse --show-toplevel]} errmsg]} {
      puts "Project is NOT in a git work folder..."
      set c_git_hash "0xdeadbeef"
   } else {
      puts "Project is in a git work folder..."
      # Get the git hashtag for this project
      set c_git_hash [exec git log -1 --pretty=%h]
   }
   # Convert the hex number string to an integer
   scan $c_git_hash "%x" c_git_hash
   # Set the generic
   set parm_handle [xget_hw_parameter_handle $mhsinst "CGN_GIT_HASHTAG"]
   xset_hw_parameter_value $parm_handle [expr $c_git_hash]
   # Show this in the log
   puts "GIT hashtag is set to: "
   puts [format "CGN_GIT_HASHTAG : 0x%08X" $c_git_hash ]

   return
}
