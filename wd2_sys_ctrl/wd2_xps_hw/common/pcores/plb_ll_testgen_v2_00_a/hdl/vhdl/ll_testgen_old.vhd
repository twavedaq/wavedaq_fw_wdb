--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : ll_testgen.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  tg32
--  Created :  2014.08.07
--
--  Description :
--    add a predefined udp header to data stream
--    ip checksum has to be precalculated at offset 24 and 25
--    in header table
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

-- DO NOT EDIT BELOW THIS LINE --------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;


library psi_3205_v1_00_a;
use psi_3205_v1_00_a.all;


--library cdc_package_v1_00_a;
--use cdc_package_v1_00_a.cdc_package.all;


--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------

entity ll_testgen is
  generic
  (

    CGN_LL_DATA_WIDTH       : integer := 32;
    CGN_LL_REM_WIDTH        : integer := 2;
    CGN_FRAME_LENGTH_WIDTH  : integer := 6;
    CGN_IP_HEADER_SEL_WIDTH    : integer := 6;
  );
  port
  (
    -- input register

    -- local link
    LL_CLK_I             : in   std_logic;
    LL_TX_D_O            : out  std_logic_vector(CGN_LL_DATA_WIDTH-1 downto 0);
    LL_TX_SOF_N_O        : out  std_logic;
    LL_TX_EOF_N_O        : out  std_logic;
    LL_TX_REM_O          : out  std_logic_vector(CGN_LL_REM_WIDTH-1 downto 0);
    LL_TX_SRC_RDY_N_O    : out  std_logic;
    LL_TX_DST_RDY_N_I    : in   std_logic;
    FRAME_LENGTH_O       : out  std_logic_vector(CGN_FRAME_LENGTH_WIDTH-1 downto 0);
    IP_HEADER_SEL_O         : out  std_logic_vector(CGN_IP_HEADER_SEL_WIDTH-1 downto 0)

  );

  attribute SIGIS : string;
  attribute SIGIS of LL_CLK_I    : signal is "CLK";

end entity ll_testgen;

------------------------------------------------------------------------------
-- Architecture section
------------------------------------------------------------------------------

architecture IMP of ll_testgen is

  --USER signal declarations added here, as needed for user logic

  ------------------------------------------------
  -- bit length of counters
  ------------------------------------------------
  constant C_BIT_COUNT_IFG      : integer := 16;
  constant C_BIT_COUNT_TOTAL    : integer := 32;
  constant C_BIT_LOOP_SERV_NUM  : integer := 16;
  constant C_BIT_LOOP_DATA      : integer := 16;


  signal ll_tx_d               : std_logic_vector(CGN_LL_DATA_WIDTH-1 downto 0);
  signal ll_tx_rem             : std_logic_vector(0 to 2);
  signal ll_tx_sof_n           : std_logic;
  signal ll_tx_eof_n           : std_logic;
  signal ll_tx_src_rdy_n       : std_logic;
--  signal ll_tx_dst_rdy_n       : std_logic;

-- test pattern
  signal data                  : std_logic_vector(CGN_LL_DATA_WIDTH-1 downto 0);
  signal data_rd_en            : std_logic;
  signal test_pattern_rst      : std_logic;
  signal test_pattern_sel_12   : std_logic;


  signal reg_ctrl            : std_logic_vector(31 downto 0);
  signal reg_status          : std_logic_vector(31 downto 0);

  signal reg_header          : std_logic_vector(31 downto 0);
  signal frame_num           : std_logic_vector(31 downto 0);

  signal count_ifg           : std_logic_vector(C_BIT_COUNT_IFG-1 downto 0);
  signal reg_count_ifg       : std_logic_vector(C_BIT_COUNT_IFG-1 downto 0);

  signal count_total         : std_logic_vector(C_BIT_COUNT_TOTAL-1 downto 0);
  signal reg_count_total     : std_logic_vector(C_BIT_COUNT_TOTAL-1 downto 0);

  signal loop_serv_num       : std_logic_vector(C_BIT_LOOP_SERV_NUM-1 downto 0);
  signal reg_loop_serv_num   : std_logic_vector(C_BIT_LOOP_SERV_NUM-1 downto 0);

  signal data_loop           : std_logic_vector(C_BIT_LOOP_DATA-4 downto 0);
  signal reg_data_loop       : std_logic_vector(C_BIT_LOOP_DATA-1 downto 0);

  signal server_num          : std_logic_vector(5 downto 0);
  signal reg_server_num      : std_logic_vector(5 downto 0);

  type states is (S_IDLE, S_SOF, S_HDR, S_DATA, S_EOF, S_SERV_NUM, S_TOTAL_CNT, S_IFG);
  signal state : states;
  attribute fsm_encoding : string;
  attribute fsm_encoding of state : signal is "one-hot";

  signal running                   : std_logic;
  signal ctrl_reset                : std_logic;
  signal ctrl_start                : std_logic;
  signal ctrl_start_r              : std_logic;
  signal ctrl_stop                 : std_logic;

  component test_pattern is
  port
  (
    CLK_I    : in   std_logic;
    RD_EN_I  : in   std_logic;
    RST_I    : in   std_logic;
    SEL_12_I : in   std_logic;
    DATA_O   : out  std_logic_vector (63 downto 0);
    VALID_O  : out  std_logic
  );
  end component test_pattern;





begin

  -----------------------------------------------------------------------------
  -- register usage:
  -----------------------------------------------------------------------------
  --
  -- slv_reg0 : reg_ctrl
  -- slv_reg1 : reg_header
  -- slv_reg2 : reg_count_ifg
  -- slv_reg3 : reg_count_total
  -- slv_reg4 : reg_loop_serv_num -- packets per server num (port)
  -- slv_reg5 : reg_data_loop
  -- slv_reg6 : reg_server_num
  -- slv_reg7 : reg_status
  -----------------------------------------------------------------------------

reg_ctrl              REG_CTRL_I
reg_header            REG_HEADER_ID_I
reg_count_ifg         REG_IFG_COUNT_I
reg_count_total       REG_TOTAL_FRAMES_I
reg_loop_serv_num     REG_FRAMES_PER_IP_HEADER_I
reg_data_loop         REG_DATA_LENGTH_BYTE_I
reg_server_num        REG_NUM_OF_IP_HEADERS_I
reg_status            REG_STATUS_O


reg_status(31) <= running;


reg_status(30 downto 0) <= frame_num(30 downto 0);




  ctrl_reset          <= reg_ctrl(0);
  ctrl_stop           <= reg_ctrl(1);
  ctrl_start          <= reg_ctrl(2);
  test_pattern_sel_12 <= reg_ctrl(3);


----------------------------------------
-- Test_pattern
----------------------------------------

  test_pattern_inst : test_pattern
  port map
  (
    CLK_I    => LL_CLK_I,
    RD_EN_I  => data_rd_en,
    RST_I    => test_pattern_rst,
    SEL_12_I => test_pattern_sel_12,
    DATA_O   => data,
    VALID_O  => open
  );

----------------------------------------

----------------------------------------
-- Header
----------------------------------------

  function log2 (val: INTEGER) return natural is
    variable res : natural;
  begin
    for i in 0 to 31 loop
      if (val <= (2**i)) then
        res := i;
        exit;
      end if;
    end loop;
    return res;
  end function Log2;

  constant C_HEADER_LEN_BIT       : integer := 128;

  constant C_HEADER_ENTRIES       : integer := C_HEADER_LEN_BIT / CGN_LL_DATA_WIDTH;
  constant C_HEADER_ENTRIES_WIDTH : log2(C_HEADER_ENTRIES);

C_BIT_REM

  type header_type is array (0 to C_HEADER_ENTRIES-1) of std_logic_vector(CGN_LL_DATA_WIDTH-1 downto 0);

  signal header  : header_type := (others => (others => '1'));

  signal header_loop          : std_logic_vector(C_HEADER_ENTRIES_WIDTH-1 downto 0):= (others => '0');



  header() <= reg_header & frame_num;


  process (LL_CLK_I)
  begin
    if rising_edge(LL_CLK_I) then
      ctrl_start_r <= ctrl_start;
    end if;
  end process;


----------------------------------------
-- State Machine
----------------------------------------

  fsm: process (LL_CLK_I) is
  begin
    if rising_edge(LL_CLK_I) then
      -- default assignment
      test_pattern_rst <= '0';

      if ctrl_reset = '1' then
        state <= S_IDLE;
        ll_tx_rem        <= "111";
        ll_tx_sof_n      <= '1';
        ll_tx_eof_n      <= '1';
        ll_tx_src_rdy_n  <= '1';
        test_pattern_rst <= '1';
        running <= '0';
      else
        case state is
--------------------------------------------------------------------------------
          when S_IDLE =>      ll_tx_rem       <= "111";
                              ll_tx_sof_n     <= '1';
                              ll_tx_eof_n     <= '1';
                              ll_tx_src_rdy_n <= '1';
                              running         <= '0';
                              server_num      <= (others => '0');
                              count_total     <= reg_count_total;
                              data_loop       <= reg_data_loop(C_BIT_LOOP_DATA-1 downto C_BIT_REM);
                              count_ifg       <= reg_count_ifg;
                              loop_serv_num   <= reg_loop_serv_num;
                              frame_num       <= (others => '0');
                              header_loop     <= (others => '0');
                              if (ctrl_start = '1') and (ctrl_start_r = '0') then  -- rising edge of ctrl_start
                                running          <= '1';
                                state <= S_SOF;
                              end if;
--------------------------------------------------------------------------------

          when S_SOF =>       ll_tx_sof_n      <= '0';
                              ll_tx_src_rdy_n  <= '0';
                              ll_tx_d          <=  header(header_loop);
                              state            <= S_HDR;

--------------------------------------------------------------------------------

          when S_HDR =>       if LL_TX_DST_RDY_N_I = '0' then
                                ll_tx_sof_n  <= '1';

                                if (header_loop < (C_HEADER_ENTRIES-1)) then
                                  header_loop <= header_loop + 1;
                                  ll_tx_d     <= header(header_loop);
                                  state       <= S_HDR;
                                else

                                  state <= S_DATA;
                                end if;

                              end if;

--------------------------------------------------------------------------------

          when S_DATA =>      if LL_TX_DST_RDY_N_I = '0' then
                                ll_tx_d   <= data;
                                if (data_loop > 0) then
                                  data_loop <= data_loop - 1 ;
                                  state <= S_DATA;
                                else
                                  ll_tx_rem   <= reg_data_loop(CGN_LL_REM_WIDTH-1 downto 0);
                                  ll_tx_eof_n <= '0';
                                  state <= S_EOF;
                                end if;

                              end if;

--------------------------------------------------------------------------------
          when S_EOF =>       if LL_TX_DST_RDY_N_I = '0' then
                                ll_tx_eof_n      <= '1';
                                ll_tx_src_rdy_n  <= '1';
                                state <= S_SERV_NUM;
                              end if;
                              test_pattern_rst <= '1';

--------------------------------------------------------------------------------
          when S_SERV_NUM =>  if loop_serv_num > 0 then
                                loop_serv_num <= loop_serv_num -1;
                              else
                                loop_serv_num <= reg_loop_serv_num;
                                if server_num < reg_server_num then
                                  server_num <= server_num + 1;
                                else
                                  server_num <= (others => '0');
                                end if;
                              end if;
                              state <= S_TOTAL_CNT;

--------------------------------------------------------------------------------
          when S_TOTAL_CNT => if count_total > 0 then
                                count_total <= count_total -1;
                                frame_num  <= frame_num + 1;
                                state <= S_IFG;
                              else
                                state <= S_IDLE;
                              end if;

--------------------------------------------------------------------------------
          when S_IFG =>       data_loop       <= reg_data_loop(C_BIT_LOOP_DATA-1 downto C_BIT_REM);
                              if ctrl_stop = '1' then
                                state <= S_IDLE;
                              elsif count_ifg > 0 then
                                count_ifg <= count_ifg - 1;
                                state <= S_IFG;
                              else
                                count_ifg <= reg_count_ifg;
                                ll_tx_sof_n      <= '0';
                                ll_tx_src_rdy_n  <= '0';
                                ll_tx_d          <= reg_header & frame_num;
                                state <= S_SOF;
                              end if;

--------------------------------------------------------------------------------
        end case;

      end if;
    end if;
  end process fsm;
--------------------------------------------------------------------------------

  data_rd_en <= '1' when (    ((state = S_HDR)  and (header_loop < (C_HEADER_ENTRIES-1)) and (LL_TX_DST_RDY_N_I = '0'))
                           or ((state = S_DATA) and (LL_TX_DST_RDY_N_I = '0'))) else '0';

  LL_TX_D_O            <= ll_tx_d;
  LL_TX_REM_O          <= ll_tx_rem;
  LL_TX_SOF_N_O        <= ll_tx_sof_n;
  LL_TX_EOF_N_O        <= ll_tx_eof_n;
  LL_TX_SRC_RDY_N_O    <= ll_tx_src_rdy_n;
  IP_HEADER_SEL_O         <= server_num;

end IMP;

