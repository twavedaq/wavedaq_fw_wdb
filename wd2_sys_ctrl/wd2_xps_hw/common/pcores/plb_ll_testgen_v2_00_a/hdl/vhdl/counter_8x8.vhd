----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:    12:29:11 09/16/2011
-- Design Name:
-- Module Name:    conv_72_64 - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity counter_8x8 is
port
(
  CLK_I   : in   std_logic;
  RD_EN_I : in   std_logic;
  RST_I   : in   std_logic;
  DATA_O  : out  std_logic_vector (63 downto 0)
);
end counter_8x8;


architecture behavioral of counter_8x8 is

  signal data_ctr                       : std_logic_vector(4 downto 0);
  signal data_byte_0                    : std_logic_vector(7 downto 0);
  signal data_byte_1                    : std_logic_vector(7 downto 0);
  signal data_byte_2                    : std_logic_vector(7 downto 0);
  signal data_byte_3                    : std_logic_vector(7 downto 0);
  signal data_byte_4                    : std_logic_vector(7 downto 0);
  signal data_byte_5                    : std_logic_vector(7 downto 0);
  signal data_byte_6                    : std_logic_vector(7 downto 0);
  signal data_byte_7                    : std_logic_vector(7 downto 0);

begin

  process(CLK_I) is
  begin
    if rising_edge(CLK_I) then
      if (RST_I = '1') then
        data_ctr  <=  (others => '0');
      elsif (RD_EN_I = '1') then
        data_ctr  <=  data_ctr + 1;
      end if;
    end if;
  end process;

  data_byte_0 <= data_ctr & "000";
  data_byte_1 <= data_ctr & "001";
  data_byte_2 <= data_ctr & "010";
  data_byte_3 <= data_ctr & "011";
  data_byte_4 <= data_ctr & "100";
  data_byte_5 <= data_ctr & "101";
  data_byte_6 <= data_ctr & "110";
  data_byte_7 <= data_ctr & "111";

  DATA_O <= data_byte_0 & data_byte_1 & data_byte_2 & data_byte_3 & data_byte_4 & data_byte_5 & data_byte_6 & data_byte_7;

end Behavioral;

