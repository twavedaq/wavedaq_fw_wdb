-------------------------------------------------------------------------------
-- $Id:$
-------------------------------------------------------------------------------
-- based on proc_common_v3_00_a/hdl/vhdl/async_fifo_fg.vhd
-------------------------------------------------------------------------------
--
-- *************************************************************************
-- **                                                                     **
-- ** DISCLAIMER OF LIABILITY                                             **
-- **                                                                     **
-- ** This text/file contains proprietary, confidential                   **
-- ** information of Xilinx, Inc., is distributed under                   **
-- ** license from Xilinx, Inc., and may be used, copied                  **
-- ** and/or disclosed only pursuant to the terms of a valid              **
-- ** license agreement with Xilinx, Inc. Xilinx hereby                   **
-- ** grants you a license to use this text/file solely for               **
-- ** design, simulation, implementation and creation of                  **
-- ** design files limited to Xilinx devices or technologies.             **
-- ** Use with non-Xilinx devices or technologies is expressly            **
-- ** prohibited and immediately terminates your license unless           **
-- ** covered by a separate agreement.                                    **
-- **                                                                     **
-- ** Xilinx is providing this design, code, or information               **
-- ** "as-is" solely for use in developing programs and                   **
-- ** solutions for Xilinx devices, with no obligation on the             **
-- ** part of Xilinx to provide support. By providing this design,        **
-- ** code, or information as one possible implementation of              **
-- ** this feature, application or standard, Xilinx is making no          **
-- ** representation that this implementation is free from any            **
-- ** claims of infringement. You are responsible for obtaining           **
-- ** any rights you may require for your implementation.                 **
-- ** Xilinx expressly disclaims any warranty whatsoever with             **
-- ** respect to the adequacy of the implementation, including            **
-- ** but not limited to any warranties or representations that this      **
-- ** implementation is free from claims of infringement, implied         **
-- ** warranties of merchantability or fitness for a particular           **
-- ** purpose.                                                            **
-- **                                                                     **
-- ** Xilinx products are not intended for use in life support            **
-- ** appliances, devices, or systems. Use in such applications is        **
-- ** expressly prohibited.                                               **
-- **                                                                     **
-- ** Any modifications that are made to the Source Code are              **
-- ** done at the users sole risk and will be unsupported.                **
-- ** The Xilinx Support Hotline does not have access to source           **
-- ** code and therefore cannot answer specific questions related         **
-- ** to source HDL. The Xilinx Hotline support of original source        **
-- ** code IP shall only address issues and questions related             **
-- ** to the standard Netlist version of the core (and thus               **
-- ** indirectly, the original core source).                              **
-- **                                                                     **
-- ** Copyright (c) 2008, 2009, 2010 Xilinx, Inc. All rights reserved.    **
-- **                                                                     **
-- ** This copyright and support notice must be retained as part          **
-- ** of this text at all times.                                          **
-- **                                                                     **
-- *************************************************************************
--
-------------------------------------------------------------------------------
-- Filename:        fifo_gen_async.vhd
--
-- Description:
-- This HDL file adapts the legacy CoreGen Async FIFO interface to the new
-- FIFO Generator Async FIFO interface. This wrapper facilitates the "on
-- the fly" call of FIFO Generator during design implementation.
--
--
-- VHDL-Standard:   VHDL'93
-------------------------------------------------------------------------------
-- Structure:
--              fifo_gen_async.vhd
--                 |
--                 |-- fifo_generator_v4_3
--                 |
--                 |-- fifo_generator_v9_3
--
-------------------------------------------------------------------------------
-- Revision History:
--
--
-- Author:          DET
-- Revision:        $Revision: 1.5.2.68 $
-- Date:            $1/15/2008$
--
-- History:
--   DET   1/15/2008       Initial Version
--
--     DET     7/30/2008     for EDK 11.1
-- ~~~~~~
--     - Added parameter C_ALLOW_2N_DEPTH to enable use of FIFO Generator
--       feature of specifing 2**N depth of FIFO, Legacy CoreGen Async FIFOs
--       only allowed (2**N)-1 depth specification. Parameter is defalted to
--       the legacy CoreGen method so current users are not impacted.
--     - Incorporated calculation and assignment corrections for the Read and
--       Write Pointer Widths.
--     - Upgraded to FIFO Generator Version 4.3.
--     - Corrected a swap of the Rd_Err and the Wr_Err connections on the FIFO
--       Generator instance.
-- ^^^^^^
--
--      MSH and DET     3/2/2009     For Lava SP2
-- ~~~~~~
--     - Added FIFO Generator version 5.1 for use with Virtex6 and Spartan6
--       devices.
--     - IfGen used so that legacy FPGA families still use Fifo Generator
--       version 4.3.
-- ^^^^^^
--
--     DET     2/9/2010     for EDK 12.1
-- ~~~~~~
--     - Updated the S6/V6 FIFO Generator version from V5.2 to V5.3.
-- ^^^^^^
--
--     DET     3/10/2010     For EDK 12.x
-- ~~~~~~
--   -- Per CR553307
--     - Updated the S6/V6 FIFO Generator version from V5.3 to 6_1.
-- ^^^^^^
--
--     DET     6/18/2010     EDK_MS2
-- ~~~~~~
--    -- Per IR565916
--     - Added derivative part type checks for S6 or V6.
-- ^^^^^^
--
--     DET     8/30/2010     EDK_MS4
-- ~~~~~~
--    -- Per CR573867
--     - Updated the S6/V6 FIFO Generator version from V6.1 to 7.2.
--     - Added all of the AXI parameters and ports. They are not used
--       in this application.
--     - Updated method for derivative part support using new family
--       aliasing function in family_support.vhd.
--     - Incorporated an implementation to deal with unsupported FPGA
--       parts passed in on the C_FAMILY parameter.
-- ^^^^^^
--
--     DET     10/4/2010     EDK 13.1
-- ~~~~~~
--     - Updated the FIFO Generator version from V7.2 to 7.3.
-- ^^^^^^
--
--     DET     12/8/2010     EDK 13.1
-- ~~~~~~
--    -- Per CR586109
--     - Updated the FIFO Generator version from V7.3 to 8.1.
-- ^^^^^^
--
--     DET     3/2/2011     EDK 13.2
-- ~~~~~~
--    -- Per CR595473
--     - Update to use fifo_generator_v8_2
-- ^^^^^^
--
--
--     RBODDU  08/18/2011     EDK 13.3
-- ~~~~~~
--     - Update to use fifo_generator_v8_3
-- ^^^^^^
--
--     RBODDU  06/07/2012     EDK 14.2
-- ~~~~~~
--     - Update to use fifo_generator_v9_1
-- ^^^^^^
--     RBODDU  06/11/2012     EDK 14.4
-- ~~~~~~
--     - Update to use fifo_generator_v9_2
-- ^^^^^^
--     RBODDU  07/12/2012     EDK 14.5
-- ~~~~~~
--     - Update to use fifo_generator_v9_3
-- ^^^^^^
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- synopsys translate_off
library XilinxCoreLib;
--use XilinxCoreLib.all;
-- synopsys translate_on


--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------

entity fifo_gen_async is
  generic
  (
    -- fpga family and tool version
    C_FAMILY            : string := "spartan6"; -- "spartan6" -- "virtex5";
    C_ISE_12_4          : boolean := true;  -- true
    C_ISE_14_7          : boolean := false; -- false

    -- fifo size and type
    C_DATA_WIDTH        : integer := 8;
    C_FIFO_DEPTH        : integer := 2**11;
    C_MEMORY_TYPE       : integer := 1;     -- 0 = distributed RAM, 1 = BRAM

    C_USE_EMBEDDED_REG  : integer := 0;     -- Valid only for BRAM based FIFO, otherwise needs to be set to 0
    C_PRELOAD_REGS      : integer := 1;     -- 1 = first word fall through
    C_PRELOAD_LATENCY   : integer := 0;     -- 0 = first word fall through,  needs to be set 2 when C_USE_EMBEDDED_REG = 1

    -- write port
    C_HAS_WR_COUNT      : integer := 0;
    C_WR_COUNT_WIDTH    : integer := 4;
    C_HAS_ALMOST_FULL   : integer := 0;
    C_HAS_PROG_FULL     : integer := 1;     -- 0 or 1; 1 for constant prog full
    C_PROG_FULL_OFFSET  : integer := 3;     -- Setting determines the difference between FULL and PROG_FULL conditions
    C_HAS_WR_ACK        : integer := 0;
    C_WR_ACK_LOW        : integer := 0;
    C_HAS_WR_ERR        : integer := 0;
    C_WR_ERR_LOW        : integer := 0;

    -- read port
    C_HAS_RD_COUNT      : integer := 0;
    C_RD_COUNT_WIDTH    : integer := 4;
    C_HAS_ALMOST_EMPTY  : integer := 1;
    C_HAS_PROG_EMPTY    : integer := 1;     -- 0 or 1; 1 for constant prog empty
    C_PROG_EMPTY_OFFSET : integer := 2;     -- Setting determines the difference between EMPTY and PROG_EMPTY conditions
    C_HAS_RD_ACK        : integer := 0;
    C_RD_ACK_LOW        : integer := 0;
    C_HAS_RD_ERR        : integer := 0;
    C_RD_ERR_LOW        : integer := 0
  );
  port
  (
    -- Common
    RESET_ASYNC_I     : in  std_logic := '1';

    -- Write Port
    WR_CLK_I          : in  std_logic;
    WR_EN_I           : in  std_logic;
    WR_DATA_I         : in  std_logic_vector(C_DATA_WIDTH - 1 downto 0);
    WR_FULL_O         : out std_logic;
    WR_ALMOST_FULL_O  : out std_logic;
    WR_PROG_FULL_O    : out std_logic;
    WR_COUNT_O        : out std_logic_vector(C_WR_COUNT_WIDTH - 1 downto 0);
    WR_ACK_O          : out std_logic;
    WR_ERR_O          : out std_logic;

    -- Read Port
    RD_CLK_I          : in  std_logic;
    RD_EN_I           : in  std_logic;
    RD_DATA_O         : out std_logic_vector(C_DATA_WIDTH - 1 downto 0);
    RD_EMPTY_O        : out std_logic;     -- only 1 entry left in fifo
    RD_ALMOST_EMPTY_O : out std_logic;
    RD_PROG_EMPTY_O   : out std_logic;
    RD_COUNT_O        : out std_logic_vector(C_RD_COUNT_WIDTH - 1 downto 0);
    RD_ACK_O          : out std_logic;
    RD_ERR_O          : out std_logic
  );

end entity fifo_gen_async;


--------------------------------------------------------------------------------
-- Architecture section
--------------------------------------------------------------------------------

architecture implementation of fifo_gen_async is

  -- Function delarations

  -------------------------------------------------------------------
  -- Function
  --
  -- Function Name: GetMemType
  --
  -- Function Description:
  -- Generates the required integer value for the FG instance assignment
  -- of the C_MEMORY_TYPE parameter. Derived from
  -- the input memory type parameter C_MEMORY_TYPE.
  --
  -- FIFO Generator values
  --   0 = Any
  --   1 = BRAM
  --   2 = Distributed Memory
  --   3 = Shift Registers
  --
  -------------------------------------------------------------------
  function GetMemType(inputmemtype : integer) return integer is
    variable memtype : Integer := 0;
  begin
    if (inputmemtype = 0) then          -- distributed Memory
      memtype := 2;
    else
      memtype := 1;                     -- BRAM
    end if;

    return (memtype);

  end function GetMemType;

  -------------------------------------------------------------------------------
  -- Function log2 -- returns number of bits needed to encode x choices
  --   x = 0  returns 0
  --   x = 1  returns 0
  --   x = 2  returns 1
  --   x = 4  returns 2, etc.
  -- from file proc_common_pkg.vhd
  -- Copyright (c) 2001-2010 Xilinx, Inc. All rights reserved.
  -------------------------------------------------------------------------------
  function log2(x : natural) return integer is
    variable i   : integer := 0;
    variable val : integer := 1;
  begin
    if x = 0 then
      return 0;
    else
      for j in 0 to 29 loop             -- for loop for XST
        if val >= x then
          null;
        else
          i := i + 1;
          val := val * 2;
        end if;
      end loop;
      -- Fix per CR520627  XST was ignoring this anyway and printing a
      -- Warning in SRP file. This will get rid of the warning and not
      -- impact simulation.
      -- synthesis translate_off
      assert val >= x report "Function log2 received argument larger" & " than its capability of 2^30. " severity failure;
      -- synthesis translate_on
      return i;
    end if;
  end function log2;

  -------------------------------------------------------------------
  -- Function
  --
  -- Function Name: toLowerCaseChar
  -------------------------------------------------------------------
  function toLowerCaseChar(char : character) return character is
  begin
    -- If char is not an upper case letter then return char
    if char < 'A' OR char > 'Z' then
      return char;
    end if;
    -- Otherwise map char to its corresponding lower case character and
    -- return that
    case char is
      when 'A'    => return 'a';
      when 'B'    => return 'b';
      when 'C'    => return 'c';
      when 'D'    => return 'd';
      when 'E'    => return 'e';
      when 'F'    => return 'f';
      when 'G'    => return 'g';
      when 'H'    => return 'h';
      when 'I'    => return 'i';
      when 'J'    => return 'j';
      when 'K'    => return 'k';
      when 'L'    => return 'l';
      when 'M'    => return 'm';
      when 'N'    => return 'n';
      when 'O'    => return 'o';
      when 'P'    => return 'p';
      when 'Q'    => return 'q';
      when 'R'    => return 'r';
      when 'S'    => return 's';
      when 'T'    => return 't';
      when 'U'    => return 'u';
      when 'V'    => return 'v';
      when 'W'    => return 'w';
      when 'X'    => return 'x';
      when 'Y'    => return 'y';
      when 'Z'    => return 'z';
      when others => return char;
    end case;
  end toLowerCaseChar;

  ----------------------------------------------------------------------------
  -- Function: equalIgnoringCase
  --
  -- Compare one string against another for equality with case insensitivity.
  -- Can be used to test see if a family, C_FAMILY, is equal to some
  -- family. However such usage is discouraged. Use instead availability
  -- primitive guards based on the function, 'supported', wherever possible.
  ----------------------------------------------------------------------------
  function equalIgnoringCase(str1, str2 : string) return boolean is
    constant LEN1  : integer := str1'length;
    constant LEN2  : integer := str2'length;
    variable equal : boolean := TRUE;
  begin
    if not (LEN1 = LEN2) then
      equal := FALSE;
    else
      for i in str1'range loop
        if not (toLowerCaseChar(str1(i)) = toLowerCaseChar(str2(i))) then
          equal := FALSE;
        end if;
      end loop;
    end if;
    return equal;
  end equalIgnoringCase;

  ----------------------------------------------------------------------------
  -- Function: get_root_family
  --
  -- This function takes in the string for the desired FPGA family type and
  -- returns the root FPGA family type string. This is used for derivative part
  -- aliasing to the root family. This is primarily for fifo_generator and
  -- blk_mem_gen calls that need the root family passed to the call.
  ----------------------------------------------------------------------------
  function get_root_family(family_in : string) return string is
  begin

    -- spartan3 Root family
    if (equalIgnoringCase(family_in, "spartan3")) Then
      return "spartan3";
    elsif (equalIgnoringCase(family_in, "spartan3a")) Then
      return "spartan3";
    elsif (equalIgnoringCase(family_in, "spartan3an")) Then
      return "spartan3";
    elsif (equalIgnoringCase(family_in, "spartan3adsp")) Then
      return "spartan3";
    elsif (equalIgnoringCase(family_in, "aspartan3")) Then
      return "spartan3";
    elsif (equalIgnoringCase(family_in, "aspartan3a")) Then
      return "spartan3";
    elsif (equalIgnoringCase(family_in, "aspartan3adsp")) Then
      return "spartan3";
    elsif (equalIgnoringCase(family_in, "spartan3e")) Then
      return "spartan3";
    elsif (equalIgnoringCase(family_in, "aspartan3e")) Then
      return "spartan3";

    -- virtex4 Root family
    elsif (equalIgnoringCase(family_in, "virtex4")) Then
      return "virtex4";
    elsif (equalIgnoringCase(family_in, "virtex4lx")) Then
      return "virtex4";
    elsif (equalIgnoringCase(family_in, "virtex4fx")) Then
      return "virtex4";
    elsif (equalIgnoringCase(family_in, "virtex4sx")) Then
      return "virtex4";
    elsif (equalIgnoringCase(family_in, "qvirtex4")) Then
      return "virtex4";
    elsif (equalIgnoringCase(family_in, "qrvirtex4")) Then
      return "virtex4";

    -- virtex5 Root family
    elsif (equalIgnoringCase(family_in, "virtex5")) Then
      return "virtex5";
    elsif (equalIgnoringCase(family_in, "qvirtex5")) Then
      return "virtex5";
    elsif (equalIgnoringCase(family_in, "qrvirtex5")) Then
      return "virtex5";
    elsif (equalIgnoringCase(family_in, "virtex5tx")) Then
      return "virtex5";
    elsif (equalIgnoringCase(family_in, "virtex5fx")) Then
      return "virtex5";

    -- virtex6 Root family
    elsif (equalIgnoringCase(family_in, "virtex6")) Then
      return "virtex6";
    elsif (equalIgnoringCase(family_in, "virtex6l")) Then
      return "virtex6";
    elsif (equalIgnoringCase(family_in, "qvirtex6")) Then
      return "virtex6";
    elsif (equalIgnoringCase(family_in, "virtex6cx")) Then
      return "virtex6";

    -- spartan6 Root family
    elsif (equalIgnoringCase(family_in, "spartan6")) Then
      return "spartan6";
    elsif (equalIgnoringCase(family_in, "spartan6l")) Then
      return "spartan6";
    elsif (equalIgnoringCase(family_in, "qspartan6")) Then
      return "spartan6";
    elsif (equalIgnoringCase(family_in, "aspartan6")) Then
      return "spartan6";
    elsif (equalIgnoringCase(family_in, "qspartan6l")) Then
      return "spartan6";

    -- Virtex7 Root family
    elsif (equalIgnoringCase(family_in, "virtex7")) Then
      return "virtex7";
    elsif (equalIgnoringCase(family_in, "virtex7l")) Then
      return "virtex7";
    elsif (equalIgnoringCase(family_in, "qvirtex7")) Then
      return "virtex7";
    elsif (equalIgnoringCase(family_in, "qvirtex7l")) Then
      return "virtex7";

    -- Kintex7 Root family
    elsif (equalIgnoringCase(family_in, "kintex7")) Then
      return "kintex7";
    elsif (equalIgnoringCase(family_in, "kintex7l")) Then
      return "kintex7";
    elsif (equalIgnoringCase(family_in, "qkintex7")) Then
      return "kintex7";
    elsif (equalIgnoringCase(family_in, "qkintex7l")) Then
      return "kintex7";

    -- artix7 Root family
    elsif (equalIgnoringCase(family_in, "artix7")) Then
      return "artix7";
    elsif (equalIgnoringCase(family_in, "aartix7")) Then
      return "artix7";
    elsif (equalIgnoringCase(family_in, "artix7l")) Then
      return "artix7";
    elsif (equalIgnoringCase(family_in, "qartix7")) Then
      return "artix7";
    elsif (equalIgnoringCase(family_in, "qartix7l")) Then
      return "artix7";

    -- zynq Root family
    elsif (equalIgnoringCase(family_in, "zynq")) Then
      return "zynq";
    elsif (equalIgnoringCase(family_in, "azynq")) Then
      return "zynq";
    elsif (equalIgnoringCase(family_in, "qzynq")) Then
      return "zynq";

    -- No Match to supported families and derivatives
    else
      return "nofamily";

    End if;

  end get_root_family;

  -- Constant Declarations  ----------------------------------------------

  constant FAMILY_TO_USE        : string := get_root_family(C_FAMILY); -- function from family_support.vhd
  constant FAMILY_NOT_SUPPORTED : boolean := (equalIgnoringCase(FAMILY_TO_USE, "nofamily"));
  constant FAMILY_IS_SUPPORTED  : boolean := not (FAMILY_NOT_SUPPORTED);

  constant FAM_IS_S3_V4_V5 : boolean := (equalIgnoringCase(FAMILY_TO_USE,
                                         "spartan3") or equalIgnoringCase(FAMILY_TO_USE,
                                         "virtex4") or equalIgnoringCase(FAMILY_TO_USE,
                                         "virtex5")) and FAMILY_IS_SUPPORTED;

  constant FAM_IS_NOT_S3_V4_V5 : boolean := not (FAM_IS_S3_V4_V5) and FAMILY_IS_SUPPORTED;

  -- Get the integer value for a Block memory type fifo generator call
  constant FG_MEM_TYPE : integer := GetMemType(C_MEMORY_TYPE);

  -- Set the required integer value for the FG instance assignment
  -- of the C_IMPLEMENTATION_TYPE parameter. Derived from
  -- the input memory type parameter C_MEMORY_TYPE.
  --
  --  0 = Common Clock BRAM / Distributed RAM (Synchronous FIFO)
  --  1 = Common Clock Shift Register (Synchronous FIFO)
  --  2 = Independent Clock BRAM/Distributed RAM (Asynchronous FIFO)
  --  3 = Independent/Common Clock V4 Built In Memory -- not used in legacy fifo calls
  --  5 = Independent/Common Clock V5 Built in Memory  -- not used in legacy fifo calls
  --
  constant FG_IMP_TYPE : integer := 2;

  -- programable thresholds
  constant PROG_FULL_THRESH_ASSERT_VAL : integer := C_FIFO_DEPTH - C_PROG_FULL_OFFSET;
  constant PROG_FULL_THRESH_NEGATE_VAL : integer := C_FIFO_DEPTH - C_PROG_FULL_OFFSET - 1;

  constant PROG_EMPTY_THRESH_ASSERT_VAL : integer := C_PROG_EMPTY_OFFSET;
  constant PROG_EMPTY_THRESH_NEGATE_VAL : integer := C_PROG_EMPTY_OFFSET + 1;


  constant RD_PNTR_WIDTH : integer range 4 to 22 := log2(C_FIFO_DEPTH);
  constant WR_PNTR_WIDTH : integer range 4 to 22 := log2(C_FIFO_DEPTH);

  -- constant zeros for programmable threshold inputs
  constant PROG_RDTHRESH_ZEROS : std_logic_vector(RD_PNTR_WIDTH - 1 downto 0) := (others => '0');
  Constant PROG_WRTHRESH_ZEROS : std_logic_vector(WR_PNTR_WIDTH - 1 downto 0) := (others => '0');

  -- Signals Declarations
  Signal sig_full_fifo_rdcnt : std_logic_vector(C_RD_COUNT_WIDTH - 1 DOWNTO 0);
  Signal sig_full_fifo_wrcnt : std_logic_vector(C_WR_COUNT_WIDTH - 1 DOWNTO 0);

  -- Signals

  signal sig_full : std_logic;

begin --(architecture implementation)


  -- Rip the LS bits of the write data count and assign to Write Count
  -- output port
  WR_COUNT_O <= sig_full_fifo_wrcnt(C_WR_COUNT_WIDTH - 1 downto 0);

  -- Rip the LS bits of the read data count and assign to Read Count
  -- output port
  RD_COUNT_O <= sig_full_fifo_rdcnt(C_RD_COUNT_WIDTH - 1 downto 0);

  ------------------------------------------------------------
  -- If Generate
  --
  -- Label: GEN_NO_FAMILY
  --
  -- If Generate Description:
  --   This IfGen is implemented if an unsupported FPGA family
  -- is passed in on the C_FAMILY parameter,
  --
  ------------------------------------------------------------
  GEN_NO_FAMILY : if (FAMILY_NOT_SUPPORTED) generate
  begin

    -- synthesis translate_off
    -------------------------------------------------------------
    -- Combinational Process
    --
    -- Label: DO_ASSERTION
    --
    -- Process Description:
    -- Generate a simulation error assertion for an unsupported
    -- FPGA family string passed in on the C_FAMILY parameter.
    --
    -------------------------------------------------------------
    DO_ASSERTION : process
    begin

      -- Wait until second rising wr clock edge to issue assertion
      wait until WR_CLK_I = '1';
      wait until WR_CLK_I = '0';
      wait until WR_CLK_I = '1';

      -- Report an error in simulation environment
      assert FALSE report "********* UNSUPPORTED FPGA DEVICE! Check C_FAMILY parameter assignment!" severity ERROR;

      wait;                             -- halt this process

    end process DO_ASSERTION;

    -- synthesis translate_on

    -- Tie outputs to logic low or logic high as required
    RD_DATA_O         <= (others => '0');  -- : out std_logic_vector(C_DATA_WIDTH-1 downto 0);
    WR_FULL_O         <= '0';              -- : out std_logic;
    RD_EMPTY_O        <= '1';              -- : out std_logic;
    WR_ALMOST_FULL_O  <= '0';              -- : out std_logic;
    RD_ACK_O          <= '0';              -- : out std_logic;
    RD_ERR_O          <= '1';              -- : out std_logic;
    WR_ACK_O          <= '0';              -- : out std_logic;
    WR_ERR_O          <= '1';              -- : out std_logic
    RD_ALMOST_EMPTY_O <= '0';              -- : out std_logic;
    WR_COUNT_O        <= (others => '0');  -- : out std_logic_vector(C_WR_COUNT_WIDTH-1 downto 0);
    RD_COUNT_O        <= (others => '0');  -- : out std_logic_vector(C_RD_COUNT_WIDTH-1 downto 0);

  end generate GEN_NO_FAMILY;

  ------------------------------------------------------------
  -- If Generate
  --
  -- Label: V5_AND_EARLIER
  --
  -- If Generate Description:
  --  This IFGen Implements the FIFO using FIFO Generator 4.3
  --  for FPGA Families earlier than Virtex-6 and Spartan-6.
  --
  ------------------------------------------------------------
  V5_AND_EARLIER : if (FAM_IS_S3_V4_V5) generate

    -------------------------------------------------------------------------------------
    -- Start FIFO Generator Component for V4_3
    -- Component declaration for V4_3 pulled from the L.16\rtf\vhdl\src\XilinxCoreLib
    -- file: fifo_generator_v4_3_comp.vhd
    -- This component is used for both dual clock (async) and synchronous fifos
    -- implemented with BRAM or distributed RAM. Hard FIFO simulation support is not
    -- provided in FIFO Generator V4.3 so don't use this for Hard Block FIFOs.
    -------------------------------------------------------------------------------------
    COMPONENT fifo_generator_v4_3
      GENERIC(
        --------------------------------------------------------------------------------
        -- Generic Declarations (alphabetical)
        --------------------------------------------------------------------------------
        C_COMMON_CLOCK                 : integer := 0;
        C_COUNT_TYPE                   : integer := 0;
        C_DATA_COUNT_WIDTH             : integer := 2;
        C_DEFAULT_VALUE                : string := "";
        C_DIN_WIDTH                    : integer := 8;
        C_DOUT_RST_VAL                 : string := "";
        C_DOUT_WIDTH                   : integer := 8;
        C_ENABLE_RLOCS                 : integer := 0;
        C_FAMILY                       : string := "";
        C_FULL_FLAGS_RST_VAL           : integer := 1;
        C_HAS_ALMOST_EMPTY             : integer := 0;
        C_HAS_ALMOST_FULL              : integer := 0;
        C_HAS_BACKUP                   : integer := 0;
        C_HAS_DATA_COUNT               : integer := 0;
        C_HAS_INT_CLK                  : integer := 0;
        C_HAS_MEMINIT_FILE             : integer := 0;
        C_HAS_OVERFLOW                 : integer := 0;
        C_HAS_RD_DATA_COUNT            : integer := 0;
        C_HAS_RD_RST                   : integer := 0;
        C_HAS_RST                      : integer := 1;
        C_HAS_SRST                     : integer := 0;
        C_HAS_UNDERFLOW                : integer := 0;
        C_HAS_VALID                    : integer := 0;
        C_HAS_WR_ACK                   : integer := 0;
        C_HAS_WR_DATA_COUNT            : integer := 0;
        C_HAS_WR_RST                   : integer := 0;
        C_IMPLEMENTATION_TYPE          : integer := 0;
        C_INIT_WR_PNTR_VAL             : integer := 0;
        C_MEMORY_TYPE                  : integer := 1;
        C_MIF_FILE_NAME                : string := "";
        C_OPTIMIZATION_MODE            : integer := 0;
        C_OVERFLOW_LOW                 : integer := 0;
        C_PRELOAD_LATENCY              : integer := 1;
        C_PRELOAD_REGS                 : integer := 0;
        C_PRIM_FIFO_TYPE               : string := "4kx4";
        C_PROG_EMPTY_THRESH_ASSERT_VAL : integer := 0;
        C_PROG_EMPTY_THRESH_NEGATE_VAL : integer := 0;
        C_PROG_EMPTY_TYPE              : integer := 0;
        C_PROG_FULL_THRESH_ASSERT_VAL  : integer := 0;
        C_PROG_FULL_THRESH_NEGATE_VAL  : integer := 0;
        C_PROG_FULL_TYPE               : integer := 0;
        C_RD_DATA_COUNT_WIDTH          : integer := 2;
        C_RD_DEPTH                     : integer := 256;
        C_RD_FREQ                      : integer := 1;
        C_RD_PNTR_WIDTH                : integer := 8;
        C_UNDERFLOW_LOW                : integer := 0;
        C_USE_DOUT_RST                 : integer := 0;
        C_USE_ECC                      : integer := 0;
        C_USE_EMBEDDED_REG             : integer := 0;
        C_USE_FIFO16_FLAGS             : integer := 0;
        C_USE_FWFT_DATA_COUNT          : integer := 0;
        C_VALID_LOW                    : integer := 0;
        C_WR_ACK_LOW                   : integer := 0;
        C_WR_DATA_COUNT_WIDTH          : integer := 2;
        C_WR_DEPTH                     : integer := 256;
        C_WR_FREQ                      : integer := 1;
        C_WR_PNTR_WIDTH                : integer := 8;
        C_WR_RESPONSE_LATENCY          : integer := 1;
        C_MSGON_VAL                    : integer := 1
      );
      PORT(
        --------------------------------------------------------------------------------
        -- Input and Output Declarations
        --------------------------------------------------------------------------------
        CLK                      : IN  std_logic := '0';
        BACKUP                   : IN  std_logic := '0';
        BACKUP_MARKER            : IN  std_logic := '0';
        DIN                      : IN  std_logic_vector(C_DIN_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
        PROG_EMPTY_THRESH        : IN  std_logic_vector(C_RD_PNTR_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
        PROG_EMPTY_THRESH_ASSERT : IN  std_logic_vector(C_RD_PNTR_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
        PROG_EMPTY_THRESH_NEGATE : IN  std_logic_vector(C_RD_PNTR_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
        PROG_FULL_THRESH         : IN  std_logic_vector(C_WR_PNTR_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
        PROG_FULL_THRESH_ASSERT  : IN  std_logic_vector(C_WR_PNTR_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
        PROG_FULL_THRESH_NEGATE  : IN  std_logic_vector(C_WR_PNTR_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
        RD_CLK                   : IN  std_logic := '0';
        RD_EN                    : IN  std_logic := '0';
        RD_RST                   : IN  std_logic := '0';
        RST                      : IN  std_logic := '0';
        SRST                     : IN  std_logic := '0';
        WR_CLK                   : IN  std_logic := '0';
        WR_EN                    : IN  std_logic := '0';
        WR_RST                   : IN  std_logic := '0';
        INT_CLK                  : IN  std_logic := '0';

        ALMOST_EMPTY             : OUT std_logic;
        ALMOST_FULL              : OUT std_logic;
        DATA_COUNT               : OUT std_logic_vector(C_DATA_COUNT_WIDTH - 1 DOWNTO 0);
        DOUT                     : OUT std_logic_vector(C_DOUT_WIDTH - 1 DOWNTO 0);
        EMPTY                    : OUT std_logic;
        FULL                     : OUT std_logic;
        OVERFLOW                 : OUT std_logic;
        PROG_EMPTY               : OUT std_logic;
        PROG_FULL                : OUT std_logic;
        VALID                    : OUT std_logic;
        RD_DATA_COUNT            : OUT std_logic_vector(C_RD_DATA_COUNT_WIDTH - 1 DOWNTO 0);
        UNDERFLOW                : OUT std_logic;
        WR_ACK                   : OUT std_logic;
        WR_DATA_COUNT            : OUT std_logic_vector(C_WR_DATA_COUNT_WIDTH - 1 DOWNTO 0);
        SBITERR                  : OUT std_logic;
        DBITERR                  : OUT std_logic
      );
    END COMPONENT;

    -- The following attributes tells XST that the fifo_generator_v4_3 is a black box
    -- which should be generated using the command given by the value
    -- of this attribute
    attribute box_type : string;
    attribute box_type of fifo_generator_v4_3 : component is
    "black_box";
    attribute GENERATOR_DEFAULT : string;
    attribute GENERATOR_DEFAULT of fifo_generator_v4_3 : component is
    "generatecore com.xilinx.ip.fifo_generator_v4_3.fifo_generator_v4_3 -a map_qvirtex_to=virtex map_qrvirtex_to=virtex map_virtexe_to=virtex map_qvirtex2_to=virtex2 map_qrvirtex2_to=virtex2 map_spartan2_to=virtex map_spartan2e_to=virtex map_virtex5_to=virtex4 map_spartan3a_to=spartan3e spartan3an_to=spartan3e spartan3adsp_to=spartan3e ";

  -- End FIFO Generator 4.3 Component ---------------------------------------


  begin
    WR_FULL_O <= sig_full;

    -------------------------------------------------------------------------------
    -- Instantiate the generalized FIFO Generator instance
    --
    -- NOTE:
    -- DO NOT CHANGE TO DIRECT ENTITY INSTANTIATION!!!
    -- This is a Coregen FIFO Generator Call module for
    -- BRAM implementations of a legacy FIFO
    --
    -------------------------------------------------------------------------------
    I_ASYNC_FIFO_BRAM : fifo_generator_v4_3
      generic map(
        C_COMMON_CLOCK                 => 0,
        C_COUNT_TYPE                   => 0,
        C_DATA_COUNT_WIDTH             => C_WR_COUNT_WIDTH,
        C_DEFAULT_VALUE                => "BlankString",
        C_DIN_WIDTH                    => C_DATA_WIDTH,
        C_DOUT_RST_VAL                 => "0",
        C_DOUT_WIDTH                   => C_DATA_WIDTH,
        C_ENABLE_RLOCS                 => 0,
        C_FAMILY                       => FAMILY_TO_USE,
        C_HAS_ALMOST_EMPTY             => C_HAS_ALMOST_EMPTY,
        C_HAS_ALMOST_FULL              => C_HAS_ALMOST_FULL,
        C_HAS_BACKUP                   => 0,
        C_HAS_DATA_COUNT               => 0,
        C_HAS_MEMINIT_FILE             => 0,
        C_HAS_OVERFLOW                 => C_HAS_WR_ERR,
        C_HAS_RD_DATA_COUNT            => C_HAS_RD_COUNT,
        C_HAS_RD_RST                   => 0,
        C_HAS_RST                      => 1,
        C_HAS_SRST                     => 0,
        C_HAS_UNDERFLOW                => C_HAS_RD_ERR,
        C_HAS_VALID                    => C_HAS_RD_ACK,
        C_HAS_WR_ACK                   => C_HAS_WR_ACK,
        C_HAS_WR_DATA_COUNT            => C_HAS_WR_COUNT,
        C_HAS_WR_RST                   => 0,
        C_IMPLEMENTATION_TYPE          => FG_IMP_TYPE,
        C_INIT_WR_PNTR_VAL             => 0,
        C_MEMORY_TYPE                  => FG_MEM_TYPE,
        C_MIF_FILE_NAME                => "BlankString",
        C_OPTIMIZATION_MODE            => 0,
        C_OVERFLOW_LOW                 => C_WR_ERR_LOW,
        C_PRELOAD_REGS                 => C_PRELOAD_REGS,     ----0, Fixed CR#658129
        C_PRELOAD_LATENCY              => C_PRELOAD_LATENCY,  ----1, Fixed CR#658129
        C_PRIM_FIFO_TYPE               => "512x36",           -- only used for V5 Hard FIFO
        C_PROG_EMPTY_THRESH_ASSERT_VAL => PROG_EMPTY_THRESH_ASSERT_VAL,
        C_PROG_EMPTY_THRESH_NEGATE_VAL => PROG_EMPTY_THRESH_NEGATE_VAL,
        C_PROG_EMPTY_TYPE              => C_HAS_PROG_EMPTY,
        C_PROG_FULL_THRESH_ASSERT_VAL  => PROG_FULL_THRESH_ASSERT_VAL,
        C_PROG_FULL_THRESH_NEGATE_VAL  => PROG_FULL_THRESH_NEGATE_VAL,
        C_PROG_FULL_TYPE               => C_HAS_PROG_FULL,
        C_RD_DATA_COUNT_WIDTH          => C_RD_COUNT_WIDTH,
        C_RD_DEPTH                     => C_FIFO_DEPTH,
        C_RD_FREQ                      => 1,
        C_RD_PNTR_WIDTH                => RD_PNTR_WIDTH,
        C_UNDERFLOW_LOW                => C_RD_ERR_LOW,
        C_USE_DOUT_RST                 => 1,
        C_USE_EMBEDDED_REG             => C_USE_EMBEDDED_REG, ----0, Fixed CR#658129
        C_USE_FIFO16_FLAGS             => 0,
        C_USE_FWFT_DATA_COUNT          => 0,
        C_VALID_LOW                    => 0,
        C_WR_ACK_LOW                   => C_WR_ACK_LOW,
        C_WR_DATA_COUNT_WIDTH          => C_WR_COUNT_WIDTH,
        C_WR_DEPTH                     => C_FIFO_DEPTH,
        C_WR_FREQ                      => 1,
        C_WR_PNTR_WIDTH                => WR_PNTR_WIDTH,
        C_WR_RESPONSE_LATENCY          => 1,
        C_USE_ECC                      => 0,
        C_FULL_FLAGS_RST_VAL           => 0,
        C_HAS_INT_CLK                  => 0,
        C_MSGON_VAL                    => 1
      )
      port map(
        CLK                      => '0',                 -- : IN  std_logic := '0';
        BACKUP                   => '0',                 -- : IN  std_logic := '0';
        BACKUP_MARKER            => '0',                 -- : IN  std_logic := '0';
        DIN                      => WR_DATA_I,           -- : IN  std_logic_vector(C_DIN_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
        PROG_EMPTY_THRESH        => PROG_RDTHRESH_ZEROS, -- : IN  std_logic_vector(C_RD_PNTR_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
        PROG_EMPTY_THRESH_ASSERT => PROG_RDTHRESH_ZEROS, -- : IN  std_logic_vector(C_RD_PNTR_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
        PROG_EMPTY_THRESH_NEGATE => PROG_RDTHRESH_ZEROS, -- : IN  std_logic_vector(C_RD_PNTR_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
        PROG_FULL_THRESH         => PROG_WRTHRESH_ZEROS, -- : IN  std_logic_vector(C_WR_PNTR_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
        PROG_FULL_THRESH_ASSERT  => PROG_WRTHRESH_ZEROS, -- : IN  std_logic_vector(C_WR_PNTR_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
        PROG_FULL_THRESH_NEGATE  => PROG_WRTHRESH_ZEROS, -- : IN  std_logic_vector(C_WR_PNTR_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
        RD_CLK                   => RD_CLK_I,            -- : IN  std_logic := '0';
        RD_EN                    => RD_EN_I,             -- : IN  std_logic := '0';
        RD_RST                   => RESET_ASYNC_I,       -- : IN  std_logic := '0';
        RST                      => RESET_ASYNC_I,       -- : IN  std_logic := '0';
        SRST                     => '0',                 -- : IN  std_logic := '0';
        WR_CLK                   => WR_CLK_I,            -- : IN  std_logic := '0';
        WR_EN                    => WR_EN_I,             -- : IN  std_logic := '0';
        WR_RST                   => RESET_ASYNC_I,       -- : IN  std_logic := '0';
        INT_CLK                  => '0',                 -- : IN  std_logic := '0';

        ALMOST_EMPTY             => RD_ALMOST_EMPTY_O,   -- : OUT std_logic;
        ALMOST_FULL              => WR_ALMOST_FULL_O,    -- : OUT std_logic;
        DATA_COUNT               => open,                -- : OUT std_logic_vector(C_DATA_COUNT_WIDTH-1 DOWNTO 0);
        DOUT                     => RD_DATA_O,           -- : OUT std_logic_vector(C_DOUT_WIDTH-1 DOWNTO 0);
        EMPTY                    => RD_EMPTY_O,          -- : OUT std_logic;
        FULL                     => sig_full,            -- : OUT std_logic;
        OVERFLOW                 => WR_ERR_O,            -- : OUT std_logic;
        PROG_EMPTY               => RD_PROG_EMPTY_O,     -- : OUT std_logic;
        PROG_FULL                => WR_PROG_FULL_O,      -- : OUT std_logic;
        VALID                    => RD_ACK_O,            -- : OUT std_logic;
        RD_DATA_COUNT            => sig_full_fifo_rdcnt, -- : OUT std_logic_vector(C_RD_DATA_COUNT_WIDTH-1 DOWNTO 0);
        UNDERFLOW                => RD_ERR_O,            -- : OUT std_logic;
        WR_ACK                   => WR_ACK_O,            -- : OUT std_logic;
        WR_DATA_COUNT            => sig_full_fifo_wrcnt, -- : OUT std_logic_vector(C_WR_DATA_COUNT_WIDTH-1 DOWNTO 0);
        SBITERR                  => open,                -- : OUT std_logic;
        DBITERR                  => open                 -- : OUT std_logic
      );
  end generate V5_AND_EARLIER;

  ------------------------------------------------------------
  -- If Generate
  --
  -- Label: V6_S6_AND_LATER
  --
  -- If Generate Description:
  -- This IfGen implements the fifo using fifo_generator_v9_3
  -- when the designated FPGA Family is Spartan-6, Virtex-6 or
  -- later.
  --
  ------------------------------------------------------------
  V6_S6_AND_LATER : if (FAM_IS_NOT_S3_V4_V5) generate
  begin
    WR_FULL_O <= sig_full;

    fifogen_ise_12_4 : if (C_ISE_12_4) generate

      -------------------------------------------------------------------------------------
      -- Start FIFO Generator Component for V7_2
      -- The Component declaration for V5_2 pulled from the M.70c\rtf\vhdl\src\XilinxCoreLib
      -- file: fifo_generator_v6_1_comp.vhd.
      -- However, to support NCSIM, Params and Ports are ordered to match the verilog
      -- definition in M.70c\rtf\verilog\src\XilinxCoreLib\fifo_generator_v7_2.v .
      --
      -- This component is used for both dual clock (async) and synchronous fifos
      -- implemented with BRAM or distributed RAM. Hard FIFO simulation support may not
      -- be provided in FIFO Generator V7.2 so not supported here.
      --
      -- Note: AXI ports and parameters added for this version of FIFO Generator.
      --
      -------------------------------------------------------------------------------------
      COMPONENT fifo_generator_v7_2
        GENERIC(
          --------------------------------------------------------------------------------
          -- Generic Declarations (verilog model ordering)
          --------------------------------------------------------------------------------
          C_COMMON_CLOCK                      : integer := 0;
          C_COUNT_TYPE                        : integer := 0;
          C_DATA_COUNT_WIDTH                  : integer := 2;
          C_DEFAULT_VALUE                     : string := "";
          C_DIN_WIDTH                         : integer := 8;
          C_DOUT_RST_VAL                      : string := "";
          C_DOUT_WIDTH                        : integer := 8;
          C_ENABLE_RLOCS                      : integer := 0;
          C_FAMILY                            : string := "";
          C_FULL_FLAGS_RST_VAL                : integer := 1;
          C_HAS_ALMOST_EMPTY                  : integer := 0;
          C_HAS_ALMOST_FULL                   : integer := 0;
          C_HAS_BACKUP                        : integer := 0;
          C_HAS_DATA_COUNT                    : integer := 0;
          C_HAS_INT_CLK                       : integer := 0;
          C_HAS_MEMINIT_FILE                  : integer := 0;
          C_HAS_OVERFLOW                      : integer := 0;
          C_HAS_RD_DATA_COUNT                 : integer := 0;
          C_HAS_RD_RST                        : integer := 0;
          C_HAS_RST                           : integer := 1;
          C_HAS_SRST                          : integer := 0;
          C_HAS_UNDERFLOW                     : integer := 0;
          C_HAS_VALID                         : integer := 0;
          C_HAS_WR_ACK                        : integer := 0;
          C_HAS_WR_DATA_COUNT                 : integer := 0;
          C_HAS_WR_RST                        : integer := 0;
          C_IMPLEMENTATION_TYPE               : integer := 0;
          C_INIT_WR_PNTR_VAL                  : integer := 0;
          C_MEMORY_TYPE                       : integer := 1;
          C_MIF_FILE_NAME                     : string := "";
          C_OPTIMIZATION_MODE                 : integer := 0;
          C_OVERFLOW_LOW                      : integer := 0;
          C_PRELOAD_LATENCY                   : integer := 1;
          C_PRELOAD_REGS                      : integer := 0;
          C_PRIM_FIFO_TYPE                    : string := "4kx4";
          C_PROG_EMPTY_THRESH_ASSERT_VAL      : integer := 0;
          C_PROG_EMPTY_THRESH_NEGATE_VAL      : integer := 0;
          C_PROG_EMPTY_TYPE                   : integer := 0;
          C_PROG_FULL_THRESH_ASSERT_VAL       : integer := 0;
          C_PROG_FULL_THRESH_NEGATE_VAL       : integer := 0;
          C_PROG_FULL_TYPE                    : integer := 0;
          C_RD_DATA_COUNT_WIDTH               : integer := 2;
          C_RD_DEPTH                          : integer := 256;
          C_RD_FREQ                           : integer := 1;
          C_RD_PNTR_WIDTH                     : integer := 8;
          C_UNDERFLOW_LOW                     : integer := 0;
          C_USE_DOUT_RST                      : integer := 0;
          C_USE_ECC                           : integer := 0;
          C_USE_EMBEDDED_REG                  : integer := 0;
          C_USE_FIFO16_FLAGS                  : integer := 0;
          C_USE_FWFT_DATA_COUNT               : integer := 0;
          C_VALID_LOW                         : integer := 0;
          C_WR_ACK_LOW                        : integer := 0;
          C_WR_DATA_COUNT_WIDTH               : integer := 2;
          C_WR_DEPTH                          : integer := 256;
          C_WR_FREQ                           : integer := 1;
          C_WR_PNTR_WIDTH                     : integer := 8;
          C_WR_RESPONSE_LATENCY               : integer := 1;
          C_MSGON_VAL                         : integer := 1;
          C_ENABLE_RST_SYNC                   : integer := 1;
          C_ERROR_INJECTION_TYPE              : integer := 0;

          -- AXI Interface related parameters start here
          C_INTERFACE_TYPE                    : integer := 0; -- 0: Native Interface; 1: AXI Interface
          C_AXI_TYPE                          : integer := 0; -- 0: AXI Stream; 1: AXI Full; 2: AXI Lite
          C_HAS_AXI_WR_CHANNEL                : integer := 0;
          C_HAS_AXI_RD_CHANNEL                : integer := 0;
          C_HAS_SLAVE_CE                      : integer := 0;
          C_HAS_MASTER_CE                     : integer := 0;
          C_ADD_NGC_CONSTRAINT                : integer := 0;
          C_USE_COMMON_OVERFLOW               : integer := 0;
          C_USE_COMMON_UNDERFLOW              : integer := 0;
          C_USE_DEFAULT_SETTINGS              : integer := 0;

          -- AXI Full/Lite
          C_AXI_ID_WIDTH                      : integer := 4;
          C_AXI_ADDR_WIDTH                    : integer := 32;
          C_AXI_DATA_WIDTH                    : integer := 64;
          C_HAS_AXI_AWUSER                    : integer := 0;
          C_HAS_AXI_WUSER                     : integer := 0;
          C_HAS_AXI_BUSER                     : integer := 0;
          C_HAS_AXI_ARUSER                    : integer := 0;
          C_HAS_AXI_RUSER                     : integer := 0;
          C_AXI_ARUSER_WIDTH                  : integer := 1;
          C_AXI_AWUSER_WIDTH                  : integer := 1;
          C_AXI_WUSER_WIDTH                   : integer := 1;
          C_AXI_BUSER_WIDTH                   : integer := 1;
          C_AXI_RUSER_WIDTH                   : integer := 1;

          -- AXI Streaming
          C_HAS_AXIS_TDATA                    : integer := 0;
          C_HAS_AXIS_TID                      : integer := 0;
          C_HAS_AXIS_TDEST                    : integer := 0;
          C_HAS_AXIS_TUSER                    : integer := 0;
          C_HAS_AXIS_TREADY                   : integer := 1;
          C_HAS_AXIS_TLAST                    : integer := 0;
          C_HAS_AXIS_TSTRB                    : integer := 0;
          C_HAS_AXIS_TKEEP                    : integer := 0;
          C_AXIS_TDATA_WIDTH                  : integer := 64;
          C_AXIS_TID_WIDTH                    : integer := 8;
          C_AXIS_TDEST_WIDTH                  : integer := 4;
          C_AXIS_TUSER_WIDTH                  : integer := 4;
          C_AXIS_TSTRB_WIDTH                  : integer := 4;
          C_AXIS_TKEEP_WIDTH                  : integer := 4;

          -- AXI Channel Type
          -- WACH --> Write Address Channel
          -- WDCH --> Write Data Channel
          -- WRCH --> Write Response Channel
          -- RACH --> Read Address Channel
          -- RDCH --> Read Data Channel
          -- AXIS --> AXI Streaming
          C_WACH_TYPE                         : integer := 0; -- 0 = FIFO; 1 = Register Slice; 2 = Pass Through Logic
          C_WDCH_TYPE                         : integer := 0; -- 0 = FIFO; 1 = Register Slice; 2 = Pass Through Logie
          C_WRCH_TYPE                         : integer := 0; -- 0 = FIFO; 1 = Register Slice; 2 = Pass Through Logie
          C_RACH_TYPE                         : integer := 0; -- 0 = FIFO; 1 = Register Slice; 2 = Pass Through Logie
          C_RDCH_TYPE                         : integer := 0; -- 0 = FIFO; 1 = Register Slice; 2 = Pass Through Logie
          C_AXIS_TYPE                         : integer := 0; -- 0 = FIFO; 1 = Register Slice; 2 = Pass Through Logie

          -- AXI Implementation Type
          -- 1 = Common Clock Block RAM FIFO
          -- 2 = Common Clock Distributed RAM FIFO
          -- 11 = Independent Clock Block RAM FIFO
          -- 12 = Independent Clock Distributed RAM FIFO
          C_IMPLEMENTATION_TYPE_WACH          : integer := 1;
          C_IMPLEMENTATION_TYPE_WDCH          : integer := 1;
          C_IMPLEMENTATION_TYPE_WRCH          : integer := 1;
          C_IMPLEMENTATION_TYPE_RACH          : integer := 1;
          C_IMPLEMENTATION_TYPE_RDCH          : integer := 1;
          C_IMPLEMENTATION_TYPE_AXIS          : integer := 1;

          -- AXI FIFO Type
          -- 0 = Data FIFO
          -- 1 = Packet FIFO
          -- 2 = Low Latency Data FIFO
          C_APPLICATION_TYPE_WACH             : integer := 0;
          C_APPLICATION_TYPE_WDCH             : integer := 0;
          C_APPLICATION_TYPE_WRCH             : integer := 0;
          C_APPLICATION_TYPE_RACH             : integer := 0;
          C_APPLICATION_TYPE_RDCH             : integer := 0;
          C_APPLICATION_TYPE_AXIS             : integer := 0;

          -- Enable ECC
          -- 0 = ECC disabled
          -- 1 = ECC enabled
          C_USE_ECC_WACH                      : integer := 0;
          C_USE_ECC_WDCH                      : integer := 0;
          C_USE_ECC_WRCH                      : integer := 0;
          C_USE_ECC_RACH                      : integer := 0;
          C_USE_ECC_RDCH                      : integer := 0;
          C_USE_ECC_AXIS                      : integer := 0;

          -- ECC Error Injection Type
          -- 0 = No Error Injection
          -- 1 = Single Bit Error Injection
          -- 2 = Double Bit Error Injection
          -- 3 = Single Bit and Double Bit Error Injection
          C_ERROR_INJECTION_TYPE_WACH         : integer := 0;
          C_ERROR_INJECTION_TYPE_WDCH         : integer := 0;
          C_ERROR_INJECTION_TYPE_WRCH         : integer := 0;
          C_ERROR_INJECTION_TYPE_RACH         : integer := 0;
          C_ERROR_INJECTION_TYPE_RDCH         : integer := 0;
          C_ERROR_INJECTION_TYPE_AXIS         : integer := 0;

          -- Input Data Width
          -- Accumulation of all AXI input signal's width
          C_DIN_WIDTH_WACH                    : integer := 32;
          C_DIN_WIDTH_WDCH                    : integer := 64;
          C_DIN_WIDTH_WRCH                    : integer := 2;
          C_DIN_WIDTH_RACH                    : integer := 32;
          C_DIN_WIDTH_RDCH                    : integer := 64;
          C_DIN_WIDTH_AXIS                    : integer := 1;

          C_WR_DEPTH_WACH                     : integer := 16;
          C_WR_DEPTH_WDCH                     : integer := 1024;
          C_WR_DEPTH_WRCH                     : integer := 16;
          C_WR_DEPTH_RACH                     : integer := 16;
          C_WR_DEPTH_RDCH                     : integer := 1024;
          C_WR_DEPTH_AXIS                     : integer := 1024;

          C_WR_PNTR_WIDTH_WACH                : integer := 4;
          C_WR_PNTR_WIDTH_WDCH                : integer := 10;
          C_WR_PNTR_WIDTH_WRCH                : integer := 4;
          C_WR_PNTR_WIDTH_RACH                : integer := 4;
          C_WR_PNTR_WIDTH_RDCH                : integer := 10;
          C_WR_PNTR_WIDTH_AXIS                : integer := 10;

          C_HAS_DATA_COUNTS_WACH              : integer := 0;
          C_HAS_DATA_COUNTS_WDCH              : integer := 0;
          C_HAS_DATA_COUNTS_WRCH              : integer := 0;
          C_HAS_DATA_COUNTS_RACH              : integer := 0;
          C_HAS_DATA_COUNTS_RDCH              : integer := 0;
          C_HAS_DATA_COUNTS_AXIS              : integer := 0;

          C_HAS_PROG_FLAGS_WACH               : integer := 0;
          C_HAS_PROG_FLAGS_WDCH               : integer := 0;
          C_HAS_PROG_FLAGS_WRCH               : integer := 0;
          C_HAS_PROG_FLAGS_RACH               : integer := 0;
          C_HAS_PROG_FLAGS_RDCH               : integer := 0;
          C_HAS_PROG_FLAGS_AXIS               : integer := 0;

          C_PROG_FULL_TYPE_WACH               : integer := 5;
          C_PROG_FULL_TYPE_WDCH               : integer := 5;
          C_PROG_FULL_TYPE_WRCH               : integer := 5;
          C_PROG_FULL_TYPE_RACH               : integer := 5;
          C_PROG_FULL_TYPE_RDCH               : integer := 5;
          C_PROG_FULL_TYPE_AXIS               : integer := 5;
          C_PROG_FULL_THRESH_ASSERT_VAL_WACH  : integer := 1023;
          C_PROG_FULL_THRESH_ASSERT_VAL_WDCH  : integer := 1023;
          C_PROG_FULL_THRESH_ASSERT_VAL_WRCH  : integer := 1023;
          C_PROG_FULL_THRESH_ASSERT_VAL_RACH  : integer := 1023;
          C_PROG_FULL_THRESH_ASSERT_VAL_RDCH  : integer := 1023;
          C_PROG_FULL_THRESH_ASSERT_VAL_AXIS  : integer := 1023;

          C_PROG_EMPTY_TYPE_WACH              : integer := 5;
          C_PROG_EMPTY_TYPE_WDCH              : integer := 5;
          C_PROG_EMPTY_TYPE_WRCH              : integer := 5;
          C_PROG_EMPTY_TYPE_RACH              : integer := 5;
          C_PROG_EMPTY_TYPE_RDCH              : integer := 5;
          C_PROG_EMPTY_TYPE_AXIS              : integer := 5;
          C_PROG_EMPTY_THRESH_ASSERT_VAL_WACH : integer := 1022;
          C_PROG_EMPTY_THRESH_ASSERT_VAL_WDCH : integer := 1022;
          C_PROG_EMPTY_THRESH_ASSERT_VAL_WRCH : integer := 1022;
          C_PROG_EMPTY_THRESH_ASSERT_VAL_RACH : integer := 1022;
          C_PROG_EMPTY_THRESH_ASSERT_VAL_RDCH : integer := 1022;
          C_PROG_EMPTY_THRESH_ASSERT_VAL_AXIS : integer := 1022;

          C_REG_SLICE_MODE_WACH               : integer := 0;
          C_REG_SLICE_MODE_WDCH               : integer := 0;
          C_REG_SLICE_MODE_WRCH               : integer := 0;
          C_REG_SLICE_MODE_RACH               : integer := 0;
          C_REG_SLICE_MODE_RDCH               : integer := 0;
          C_REG_SLICE_MODE_AXIS               : integer := 0
        );
        PORT(
          --------------------------------------------------------------------------------
          -- Input and Output Declarations
          --------------------------------------------------------------------------------
          BACKUP                   : IN  std_logic := '0';
          BACKUP_MARKER            : IN  std_logic := '0';
          CLK                      : IN  std_logic := '0';
          RST                      : IN  std_logic := '0';
          SRST                     : IN  std_logic := '0';
          WR_CLK                   : IN  std_logic := '0';
          WR_RST                   : IN  std_logic := '0';
          RD_CLK                   : IN  std_logic := '0';
          RD_RST                   : IN  std_logic := '0';
          DIN                      : IN  std_logic_vector(C_DIN_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          WR_EN                    : IN  std_logic := '0'; --Mandatory input
          RD_EN                    : IN  std_logic := '0'; --Mandatory input
          PROG_EMPTY_THRESH        : IN  std_logic_vector(C_RD_PNTR_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          PROG_EMPTY_THRESH_ASSERT : IN  std_logic_vector(C_RD_PNTR_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          PROG_EMPTY_THRESH_NEGATE : IN  std_logic_vector(C_RD_PNTR_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          PROG_FULL_THRESH         : IN  std_logic_vector(C_WR_PNTR_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          PROG_FULL_THRESH_ASSERT  : IN  std_logic_vector(C_WR_PNTR_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          PROG_FULL_THRESH_NEGATE  : IN  std_logic_vector(C_WR_PNTR_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          INT_CLK                  : IN  std_logic := '0';
          INJECTDBITERR            : IN  std_logic := '0'; -- New FG 5.1/5.2
          INJECTSBITERR            : IN  std_logic := '0'; -- New FG 5.1/5.2

          DOUT                     : OUT std_logic_vector(C_DOUT_WIDTH - 1 DOWNTO 0);
          FULL                     : OUT std_logic;
          ALMOST_FULL              : OUT std_logic;
          WR_ACK                   : OUT std_logic;
          OVERFLOW                 : OUT std_logic;
          EMPTY                    : OUT std_logic;
          ALMOST_EMPTY             : OUT std_logic;
          VALID                    : OUT std_logic;
          UNDERFLOW                : OUT std_logic;
          DATA_COUNT               : OUT std_logic_vector(C_DATA_COUNT_WIDTH - 1 DOWNTO 0);
          RD_DATA_COUNT            : OUT std_logic_vector(C_RD_DATA_COUNT_WIDTH - 1 DOWNTO 0);
          WR_DATA_COUNT            : OUT std_logic_vector(C_WR_DATA_COUNT_WIDTH - 1 DOWNTO 0);
          PROG_FULL                : OUT std_logic;
          PROG_EMPTY               : OUT std_logic;
          SBITERR                  : OUT std_logic;
          DBITERR                  : OUT std_logic;

          -- AXI Global Signal
          M_ACLK                   : IN  std_logic := '0';
          S_ACLK                   : IN  std_logic := '0';
          S_ARESETN                : IN  std_logic := '0';
          M_ACLK_EN                : IN  std_logic := '0';
          S_ACLK_EN                : IN  std_logic := '0';

          -- AXI Full/Lite Slave Write Channel (write side)
          S_AXI_AWID               : IN  std_logic_vector(C_AXI_ID_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWADDR             : IN  std_logic_vector(C_AXI_ADDR_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWLEN              : IN  std_logic_vector(8 - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWSIZE             : IN  std_logic_vector(3 - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWBURST            : IN  std_logic_vector(2 - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWLOCK             : IN  std_logic_vector(2 - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWCACHE            : IN  std_logic_vector(4 - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWPROT             : IN  std_logic_vector(3 - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWQOS              : IN  std_logic_vector(4 - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWREGION           : IN  std_logic_vector(4 - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWUSER             : IN  std_logic_vector(C_AXI_AWUSER_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWVALID            : IN  std_logic := '0';
          S_AXI_AWREADY            : OUT std_logic;
          S_AXI_WID                : IN  std_logic_vector(C_AXI_ID_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_WDATA              : IN  std_logic_vector(C_AXI_DATA_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_WSTRB              : IN  std_logic_vector(C_AXI_DATA_WIDTH / 8 - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_WLAST              : IN  std_logic := '0';
          S_AXI_WUSER              : IN  std_logic_vector(C_AXI_WUSER_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_WVALID             : IN  std_logic := '0';
          S_AXI_WREADY             : OUT std_logic;
          S_AXI_BID                : OUT std_logic_vector(C_AXI_ID_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_BRESP              : OUT std_logic_vector(2 - 1 DOWNTO 0);
          S_AXI_BUSER              : OUT std_logic_vector(C_AXI_BUSER_WIDTH - 1 DOWNTO 0);
          S_AXI_BVALID             : OUT std_logic;
          S_AXI_BREADY             : IN  std_logic := '0';

          -- AXI Full/Lite Master Write Channel (Read side)
          M_AXI_AWID               : OUT std_logic_vector(C_AXI_ID_WIDTH - 1 DOWNTO 0);
          M_AXI_AWADDR             : OUT std_logic_vector(C_AXI_ADDR_WIDTH - 1 DOWNTO 0);
          M_AXI_AWLEN              : OUT std_logic_vector(8 - 1 DOWNTO 0);
          M_AXI_AWSIZE             : OUT std_logic_vector(3 - 1 DOWNTO 0);
          M_AXI_AWBURST            : OUT std_logic_vector(2 - 1 DOWNTO 0);
          M_AXI_AWLOCK             : OUT std_logic_vector(2 - 1 DOWNTO 0);
          M_AXI_AWCACHE            : OUT std_logic_vector(4 - 1 DOWNTO 0);
          M_AXI_AWPROT             : OUT std_logic_vector(3 - 1 DOWNTO 0);
          M_AXI_AWQOS              : OUT std_logic_vector(4 - 1 DOWNTO 0);
          M_AXI_AWREGION           : OUT std_logic_vector(4 - 1 DOWNTO 0);
          M_AXI_AWUSER             : OUT std_logic_vector(C_AXI_AWUSER_WIDTH - 1 DOWNTO 0);
          M_AXI_AWVALID            : OUT std_logic;
          M_AXI_AWREADY            : IN  std_logic := '0';
          M_AXI_WID                : OUT std_logic_vector(C_AXI_ID_WIDTH - 1 DOWNTO 0);
          M_AXI_WDATA              : OUT std_logic_vector(C_AXI_DATA_WIDTH - 1 DOWNTO 0);
          M_AXI_WSTRB              : OUT std_logic_vector(C_AXI_DATA_WIDTH / 8 - 1 DOWNTO 0);
          M_AXI_WLAST              : OUT std_logic;
          M_AXI_WUSER              : OUT std_logic_vector(C_AXI_WUSER_WIDTH - 1 DOWNTO 0);
          M_AXI_WVALID             : OUT std_logic;
          M_AXI_WREADY             : IN  std_logic := '0';
          M_AXI_BID                : IN  std_logic_vector(C_AXI_ID_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_BRESP              : IN  std_logic_vector(2 - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_BUSER              : IN  std_logic_vector(C_AXI_BUSER_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_BVALID             : IN  std_logic := '0';
          M_AXI_BREADY             : OUT std_logic;

          -- AXI Full/Lite Slave Read Channel (Write side)
          S_AXI_ARID               : IN  std_logic_vector(C_AXI_ID_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARADDR             : IN  std_logic_vector(C_AXI_ADDR_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARLEN              : IN  std_logic_vector(8 - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARSIZE             : IN  std_logic_vector(3 - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARBURST            : IN  std_logic_vector(2 - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARLOCK             : IN  std_logic_vector(2 - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARCACHE            : IN  std_logic_vector(4 - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARPROT             : IN  std_logic_vector(3 - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARQOS              : IN  std_logic_vector(4 - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARREGION           : IN  std_logic_vector(4 - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARUSER             : IN  std_logic_vector(C_AXI_ARUSER_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARVALID            : IN  std_logic := '0';
          S_AXI_ARREADY            : OUT std_logic;
          S_AXI_RID                : OUT std_logic_vector(C_AXI_ID_WIDTH - 1 DOWNTO 0);
          S_AXI_RDATA              : OUT std_logic_vector(C_AXI_DATA_WIDTH - 1 DOWNTO 0);
          S_AXI_RRESP              : OUT std_logic_vector(2 - 1 DOWNTO 0);
          S_AXI_RLAST              : OUT std_logic;
          S_AXI_RUSER              : OUT std_logic_vector(C_AXI_RUSER_WIDTH - 1 DOWNTO 0);
          S_AXI_RVALID             : OUT std_logic;
          S_AXI_RREADY             : IN  std_logic := '0';

          -- AXI Full/Lite Master Read Channel (Read side)
          M_AXI_ARID               : OUT std_logic_vector(C_AXI_ID_WIDTH - 1 DOWNTO 0);
          M_AXI_ARADDR             : OUT std_logic_vector(C_AXI_ADDR_WIDTH - 1 DOWNTO 0);
          M_AXI_ARLEN              : OUT std_logic_vector(8 - 1 DOWNTO 0);
          M_AXI_ARSIZE             : OUT std_logic_vector(3 - 1 DOWNTO 0);
          M_AXI_ARBURST            : OUT std_logic_vector(2 - 1 DOWNTO 0);
          M_AXI_ARLOCK             : OUT std_logic_vector(2 - 1 DOWNTO 0);
          M_AXI_ARCACHE            : OUT std_logic_vector(4 - 1 DOWNTO 0);
          M_AXI_ARPROT             : OUT std_logic_vector(3 - 1 DOWNTO 0);
          M_AXI_ARQOS              : OUT std_logic_vector(4 - 1 DOWNTO 0);
          M_AXI_ARREGION           : OUT std_logic_vector(4 - 1 DOWNTO 0);
          M_AXI_ARUSER             : OUT std_logic_vector(C_AXI_ARUSER_WIDTH - 1 DOWNTO 0);
          M_AXI_ARVALID            : OUT std_logic;
          M_AXI_ARREADY            : IN  std_logic := '0';
          M_AXI_RID                : IN  std_logic_vector(C_AXI_ID_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_RDATA              : IN  std_logic_vector(C_AXI_DATA_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_RRESP              : IN  std_logic_vector(2 - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_RLAST              : IN  std_logic := '0';
          M_AXI_RUSER              : IN  std_logic_vector(C_AXI_RUSER_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_RVALID             : IN  std_logic := '0';
          M_AXI_RREADY             : OUT std_logic;

          -- AXI Streaming Slave Signals (Write side)
          S_AXIS_TVALID            : IN  std_logic := '0';
          S_AXIS_TREADY            : OUT std_logic;
          S_AXIS_TDATA             : IN  std_logic_vector(C_AXIS_TDATA_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXIS_TSTRB             : IN  std_logic_vector(C_AXIS_TSTRB_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXIS_TKEEP             : IN  std_logic_vector(C_AXIS_TKEEP_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXIS_TLAST             : IN  std_logic := '0';
          S_AXIS_TID               : IN  std_logic_vector(C_AXIS_TID_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXIS_TDEST             : IN  std_logic_vector(C_AXIS_TDEST_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXIS_TUSER             : IN  std_logic_vector(C_AXIS_TUSER_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');

          -- AXI Streaming Master Signals (Read side)
          M_AXIS_TVALID            : OUT std_logic;
          M_AXIS_TREADY            : IN  std_logic := '0';
          M_AXIS_TDATA             : OUT std_logic_vector(C_AXIS_TDATA_WIDTH - 1 DOWNTO 0);
          M_AXIS_TSTRB             : OUT std_logic_vector(C_AXIS_TSTRB_WIDTH - 1 DOWNTO 0);
          M_AXIS_TKEEP             : OUT std_logic_vector(C_AXIS_TKEEP_WIDTH - 1 DOWNTO 0);
          M_AXIS_TLAST             : OUT std_logic;
          M_AXIS_TID               : OUT std_logic_vector(C_AXIS_TID_WIDTH - 1 DOWNTO 0);
          M_AXIS_TDEST             : OUT std_logic_vector(C_AXIS_TDEST_WIDTH - 1 DOWNTO 0);
          M_AXIS_TUSER             : OUT std_logic_vector(C_AXIS_TUSER_WIDTH - 1 DOWNTO 0);

          -- AXI Full/Lite Write Address Channel Signals
          AXI_AW_INJECTSBITERR     : IN  std_logic := '0';
          AXI_AW_INJECTDBITERR     : IN  std_logic := '0';
          AXI_AW_PROG_FULL_THRESH  : IN  std_logic_vector(C_WR_PNTR_WIDTH_WACH - 1 DOWNTO 0) := (OTHERS => '0');
          AXI_AW_PROG_EMPTY_THRESH : IN  std_logic_vector(C_WR_PNTR_WIDTH_WACH - 1 DOWNTO 0) := (OTHERS => '0');
          AXI_AW_DATA_COUNT        : OUT std_logic_vector(C_WR_PNTR_WIDTH_WACH DOWNTO 0);
          AXI_AW_WR_DATA_COUNT     : OUT std_logic_vector(C_WR_PNTR_WIDTH_WACH DOWNTO 0);
          AXI_AW_RD_DATA_COUNT     : OUT std_logic_vector(C_WR_PNTR_WIDTH_WACH DOWNTO 0);
          AXI_AW_SBITERR           : OUT std_logic;
          AXI_AW_DBITERR           : OUT std_logic;
          AXI_AW_OVERFLOW          : OUT std_logic;
          AXI_AW_UNDERFLOW         : OUT std_logic;

          -- AXI Full/Lite Write Data Channel Signals
          AXI_W_INJECTSBITERR      : IN  std_logic := '0';
          AXI_W_INJECTDBITERR      : IN  std_logic := '0';
          AXI_W_PROG_FULL_THRESH   : IN  std_logic_vector(C_WR_PNTR_WIDTH_WDCH - 1 DOWNTO 0) := (OTHERS => '0');
          AXI_W_PROG_EMPTY_THRESH  : IN  std_logic_vector(C_WR_PNTR_WIDTH_WDCH - 1 DOWNTO 0) := (OTHERS => '0');
          AXI_W_DATA_COUNT         : OUT std_logic_vector(C_WR_PNTR_WIDTH_WDCH DOWNTO 0);
          AXI_W_WR_DATA_COUNT      : OUT std_logic_vector(C_WR_PNTR_WIDTH_WDCH DOWNTO 0);
          AXI_W_RD_DATA_COUNT      : OUT std_logic_vector(C_WR_PNTR_WIDTH_WDCH DOWNTO 0);
          AXI_W_SBITERR            : OUT std_logic;
          AXI_W_DBITERR            : OUT std_logic;
          AXI_W_OVERFLOW           : OUT std_logic;
          AXI_W_UNDERFLOW          : OUT std_logic;

          -- AXI Full/Lite Write Response Channel Signals
          AXI_B_INJECTSBITERR      : IN  std_logic := '0';
          AXI_B_INJECTDBITERR      : IN  std_logic := '0';
          AXI_B_PROG_FULL_THRESH   : IN  std_logic_vector(C_WR_PNTR_WIDTH_WRCH - 1 DOWNTO 0) := (OTHERS => '0');
          AXI_B_PROG_EMPTY_THRESH  : IN  std_logic_vector(C_WR_PNTR_WIDTH_WRCH - 1 DOWNTO 0) := (OTHERS => '0');
          AXI_B_DATA_COUNT         : OUT std_logic_vector(C_WR_PNTR_WIDTH_WRCH DOWNTO 0);
          AXI_B_WR_DATA_COUNT      : OUT std_logic_vector(C_WR_PNTR_WIDTH_WRCH DOWNTO 0);
          AXI_B_RD_DATA_COUNT      : OUT std_logic_vector(C_WR_PNTR_WIDTH_WRCH DOWNTO 0);
          AXI_B_SBITERR            : OUT std_logic;
          AXI_B_DBITERR            : OUT std_logic;
          AXI_B_OVERFLOW           : OUT std_logic;
          AXI_B_UNDERFLOW          : OUT std_logic;

          -- AXI Full/Lite Read Address Channel Signals
          AXI_AR_INJECTSBITERR     : IN  std_logic := '0';
          AXI_AR_INJECTDBITERR     : IN  std_logic := '0';
          AXI_AR_PROG_FULL_THRESH  : IN  std_logic_vector(C_WR_PNTR_WIDTH_RACH - 1 DOWNTO 0) := (OTHERS => '0');
          AXI_AR_PROG_EMPTY_THRESH : IN  std_logic_vector(C_WR_PNTR_WIDTH_RACH - 1 DOWNTO 0) := (OTHERS => '0');
          AXI_AR_DATA_COUNT        : OUT std_logic_vector(C_WR_PNTR_WIDTH_RACH DOWNTO 0);
          AXI_AR_WR_DATA_COUNT     : OUT std_logic_vector(C_WR_PNTR_WIDTH_RACH DOWNTO 0);
          AXI_AR_RD_DATA_COUNT     : OUT std_logic_vector(C_WR_PNTR_WIDTH_RACH DOWNTO 0);
          AXI_AR_SBITERR           : OUT std_logic;
          AXI_AR_DBITERR           : OUT std_logic;
          AXI_AR_OVERFLOW          : OUT std_logic;
          AXI_AR_UNDERFLOW         : OUT std_logic;

          -- AXI Full/Lite Read Data Channel Signals
          AXI_R_INJECTSBITERR      : IN  std_logic := '0';
          AXI_R_INJECTDBITERR      : IN  std_logic := '0';
          AXI_R_PROG_FULL_THRESH   : IN  std_logic_vector(C_WR_PNTR_WIDTH_RDCH - 1 DOWNTO 0) := (OTHERS => '0');
          AXI_R_PROG_EMPTY_THRESH  : IN  std_logic_vector(C_WR_PNTR_WIDTH_RDCH - 1 DOWNTO 0) := (OTHERS => '0');
          AXI_R_DATA_COUNT         : OUT std_logic_vector(C_WR_PNTR_WIDTH_RDCH DOWNTO 0);
          AXI_R_WR_DATA_COUNT      : OUT std_logic_vector(C_WR_PNTR_WIDTH_RDCH DOWNTO 0);
          AXI_R_RD_DATA_COUNT      : OUT std_logic_vector(C_WR_PNTR_WIDTH_RDCH DOWNTO 0);
          AXI_R_SBITERR            : OUT std_logic;
          AXI_R_DBITERR            : OUT std_logic;
          AXI_R_OVERFLOW           : OUT std_logic;
          AXI_R_UNDERFLOW          : OUT std_logic;

          -- AXI Streaming FIFO Related Signals
          AXIS_INJECTSBITERR       : IN  std_logic := '0';
          AXIS_INJECTDBITERR       : IN  std_logic := '0';
          AXIS_PROG_FULL_THRESH    : IN  std_logic_vector(C_WR_PNTR_WIDTH_AXIS - 1 DOWNTO 0) := (OTHERS => '0');
          AXIS_PROG_EMPTY_THRESH   : IN  std_logic_vector(C_WR_PNTR_WIDTH_AXIS - 1 DOWNTO 0) := (OTHERS => '0');
          AXIS_DATA_COUNT          : OUT std_logic_vector(C_WR_PNTR_WIDTH_AXIS DOWNTO 0);
          AXIS_WR_DATA_COUNT       : OUT std_logic_vector(C_WR_PNTR_WIDTH_AXIS DOWNTO 0);
          AXIS_RD_DATA_COUNT       : OUT std_logic_vector(C_WR_PNTR_WIDTH_AXIS DOWNTO 0);
          AXIS_SBITERR             : OUT std_logic;
          AXIS_DBITERR             : OUT std_logic;
          AXIS_OVERFLOW            : OUT std_logic;
          AXIS_UNDERFLOW           : OUT std_logic
        );
      END COMPONENT;

      -- The following attributes tells XST that the fifo_generator_v7_2 is a black box
      -- which should be generated using the command given by the value
      -- of this attribute
      attribute box_type : string;

      attribute box_type of fifo_generator_v7_2 : component is
      "black_box";

      attribute GENERATOR_DEFAULT : string;
      attribute GENERATOR_DEFAULT of fifo_generator_v7_2 : component is
      "generatecore com.xilinx.ip.fifo_generator_v7_2.fifo_generator_v7_2 -a map_qvirtex_to=virtex map_qrvirtex_to=virtex map_virtexe_to=virtex map_qvirtex2_to=virtex2 map_qrvirtex2_to=virtex2 map_spartan2_to=virtex map_spartan2e_to=virtex map_virtex5_to=virtex4 map_spartan3a_to=spartan3e spartan3an_to=spartan3e spartan3adsp_to=spartan3e ";

    -- End FIFO Generator Component ---------------------------------------


    begin
      -------------------------------------------------------------------------------
      -- Instantiate the generalized FIFO Generator instance
      --
      -- NOTE:
      -- DO NOT CHANGE TO DIRECT ENTITY INSTANTIATION!!!
      -- This is a Coregen FIFO Generator Call module for
      -- legacy BRAM implementations of an Async FIFo.
      --
      -------------------------------------------------------------------------------
      I_ASYNC_FIFO_BRAM : fifo_generator_v7_2
        generic map(
          C_COMMON_CLOCK                      => 0,
          C_COUNT_TYPE                        => 0,
          C_DATA_COUNT_WIDTH                  => C_WR_COUNT_WIDTH,
          C_DEFAULT_VALUE                     => "BlankString",
          C_DIN_WIDTH                         => C_DATA_WIDTH,
          C_DOUT_RST_VAL                      => "0",
          C_DOUT_WIDTH                        => C_DATA_WIDTH,
          C_ENABLE_RLOCS                      => 0,
          C_FAMILY                            => FAMILY_TO_USE,
          C_FULL_FLAGS_RST_VAL                => 0,
          C_HAS_ALMOST_EMPTY                  => C_HAS_ALMOST_EMPTY,
          C_HAS_ALMOST_FULL                   => C_HAS_ALMOST_FULL,
          C_HAS_BACKUP                        => 0,
          C_HAS_DATA_COUNT                    => 0,
          C_HAS_INT_CLK                       => 0,
          C_HAS_MEMINIT_FILE                  => 0,
          C_HAS_OVERFLOW                      => C_HAS_WR_ERR,
          C_HAS_RD_DATA_COUNT                 => C_HAS_RD_COUNT,
          C_HAS_RD_RST                        => 0,
          C_HAS_RST                           => 1,
          C_HAS_SRST                          => 0,
          C_HAS_UNDERFLOW                     => C_HAS_RD_ERR,
          C_HAS_VALID                         => C_HAS_RD_ACK,
          C_HAS_WR_ACK                        => C_HAS_WR_ACK,
          C_HAS_WR_DATA_COUNT                 => C_HAS_WR_COUNT,
          C_HAS_WR_RST                        => 0,
          C_IMPLEMENTATION_TYPE               => FG_IMP_TYPE,
          C_INIT_WR_PNTR_VAL                  => 0,
          C_MEMORY_TYPE                       => FG_MEM_TYPE,
          C_MIF_FILE_NAME                     => "BlankString",
          C_OPTIMIZATION_MODE                 => 0,
          C_OVERFLOW_LOW                      => C_WR_ERR_LOW,
          C_PRELOAD_LATENCY                   => C_PRELOAD_LATENCY,  ----1, Fixed CR#658129
          C_PRELOAD_REGS                      => C_PRELOAD_REGS,     ----0, Fixed CR#658129
          C_PRIM_FIFO_TYPE                    => "512x36",           -- only used for V5 Hard FIFO
          C_PROG_EMPTY_THRESH_ASSERT_VAL      => PROG_EMPTY_THRESH_ASSERT_VAL,
          C_PROG_EMPTY_THRESH_NEGATE_VAL      => PROG_EMPTY_THRESH_NEGATE_VAL,
          C_PROG_EMPTY_TYPE                   => C_HAS_PROG_EMPTY,
          C_PROG_FULL_THRESH_ASSERT_VAL       => PROG_FULL_THRESH_ASSERT_VAL,
          C_PROG_FULL_THRESH_NEGATE_VAL       => PROG_FULL_THRESH_NEGATE_VAL,
          C_PROG_FULL_TYPE                    => C_HAS_PROG_FULL,
          C_RD_DATA_COUNT_WIDTH               => C_RD_COUNT_WIDTH,
          C_RD_DEPTH                          => C_FIFO_DEPTH,
          C_RD_FREQ                           => 1,
          C_RD_PNTR_WIDTH                     => RD_PNTR_WIDTH,
          C_UNDERFLOW_LOW                     => C_RD_ERR_LOW,
          C_USE_DOUT_RST                      => 1,
          C_USE_ECC                           => 0,
          C_USE_EMBEDDED_REG                  => C_USE_EMBEDDED_REG, ----0, Fixed CR#658129
          C_USE_FIFO16_FLAGS                  => 0,
          C_USE_FWFT_DATA_COUNT               => 0,
          C_VALID_LOW                         => 0,
          C_WR_ACK_LOW                        => C_WR_ACK_LOW,
          C_WR_DATA_COUNT_WIDTH               => C_WR_COUNT_WIDTH,
          C_WR_DEPTH                          => C_FIFO_DEPTH,
          C_WR_FREQ                           => 1,
          C_WR_PNTR_WIDTH                     => WR_PNTR_WIDTH,
          C_WR_RESPONSE_LATENCY               => 1,
          C_MSGON_VAL                         => 1,
          C_ENABLE_RST_SYNC                   => 1,
          C_ERROR_INJECTION_TYPE              => 0,

          -- AXI Interface related parameters start here
          C_INTERFACE_TYPE                    => 0,     -- : integer := 0; -- 0: Native Interface; 1: AXI Interface
          C_AXI_TYPE                          => 0,     -- : integer := 0; -- 0: AXI Stream; 1: AXI Full; 2: AXI Lite
          C_HAS_AXI_WR_CHANNEL                => 0,     -- : integer := 0;
          C_HAS_AXI_RD_CHANNEL                => 0,     -- : integer := 0;
          C_HAS_SLAVE_CE                      => 0,     -- : integer := 0;
          C_HAS_MASTER_CE                     => 0,     -- : integer := 0;
          C_ADD_NGC_CONSTRAINT                => 0,     -- : integer := 0;
          C_USE_COMMON_OVERFLOW               => 0,     -- : integer := 0;
          C_USE_COMMON_UNDERFLOW              => 0,     -- : integer := 0;
          C_USE_DEFAULT_SETTINGS              => 0,     -- : integer := 0;

          -- AXI Full/Lite
          C_AXI_ID_WIDTH                      => 4,     -- : integer := 0;
          C_AXI_ADDR_WIDTH                    => 32,    -- : integer := 0;
          C_AXI_DATA_WIDTH                    => 64,    -- : integer := 0;
          C_HAS_AXI_AWUSER                    => 0,     -- : integer := 0;
          C_HAS_AXI_WUSER                     => 0,     -- : integer := 0;
          C_HAS_AXI_BUSER                     => 0,     -- : integer := 0;
          C_HAS_AXI_ARUSER                    => 0,     -- : integer := 0;
          C_HAS_AXI_RUSER                     => 0,     -- : integer := 0;
          C_AXI_ARUSER_WIDTH                  => 1,     -- : integer := 0;
          C_AXI_AWUSER_WIDTH                  => 1,     -- : integer := 0;
          C_AXI_WUSER_WIDTH                   => 1,     -- : integer := 0;
          C_AXI_BUSER_WIDTH                   => 1,     -- : integer := 0;
          C_AXI_RUSER_WIDTH                   => 1,     -- : integer := 0;

          -- AXI Streaming
          C_HAS_AXIS_TDATA                    => 0,     -- : integer := 0;
          C_HAS_AXIS_TID                      => 0,     -- : integer := 0;
          C_HAS_AXIS_TDEST                    => 0,     -- : integer := 0;
          C_HAS_AXIS_TUSER                    => 0,     -- : integer := 0;
          C_HAS_AXIS_TREADY                   => 1,     -- : integer := 0;
          C_HAS_AXIS_TLAST                    => 0,     -- : integer := 0;
          C_HAS_AXIS_TSTRB                    => 0,     -- : integer := 0;
          C_HAS_AXIS_TKEEP                    => 0,     -- : integer := 0;
          C_AXIS_TDATA_WIDTH                  => 64,    -- : integer := 1;
          C_AXIS_TID_WIDTH                    => 8,     -- : integer := 1;
          C_AXIS_TDEST_WIDTH                  => 4,     -- : integer := 1;
          C_AXIS_TUSER_WIDTH                  => 4,     -- : integer := 1;
          C_AXIS_TSTRB_WIDTH                  => 4,     -- : integer := 1;
          C_AXIS_TKEEP_WIDTH                  => 4,     -- : integer := 1;

          -- AXI Channel Type
          -- WACH --> Write Address Channel
          -- WDCH --> Write Data Channel
          -- WRCH --> Write Response Channel
          -- RACH --> Read Address Channel
          -- RDCH --> Read Data Channel
          -- AXIS --> AXI Streaming
          C_WACH_TYPE                         => 0,     -- : integer := 0; -- 0 = FIFO; 1 = Register Slice; 2 = Pass Through Logic
          C_WDCH_TYPE                         => 0,     -- : integer := 0; -- 0 = FIFO; 1 = Register Slice; 2 = Pass Through Logie
          C_WRCH_TYPE                         => 0,     -- : integer := 0; -- 0 = FIFO; 1 = Register Slice; 2 = Pass Through Logie
          C_RACH_TYPE                         => 0,     -- : integer := 0; -- 0 = FIFO; 1 = Register Slice; 2 = Pass Through Logie
          C_RDCH_TYPE                         => 0,     -- : integer := 0; -- 0 = FIFO; 1 = Register Slice; 2 = Pass Through Logie
          C_AXIS_TYPE                         => 0,     -- : integer := 0; -- 0 = FIFO; 1 = Register Slice; 2 = Pass Through Logie

          -- AXI Implementation Type
          -- 1 = Common Clock Block RAM FIFO
          -- 2 = Common Clock Distributed RAM FIFO
          -- 11 = Independent Clock Block RAM FIFO
          -- 12 = Independent Clock Distributed RAM FIFO
          C_IMPLEMENTATION_TYPE_WACH          => 1,     -- : integer := 0;
          C_IMPLEMENTATION_TYPE_WDCH          => 1,     -- : integer := 0;
          C_IMPLEMENTATION_TYPE_WRCH          => 1,     -- : integer := 0;
          C_IMPLEMENTATION_TYPE_RACH          => 1,     -- : integer := 0;
          C_IMPLEMENTATION_TYPE_RDCH          => 1,     -- : integer := 0;
          C_IMPLEMENTATION_TYPE_AXIS          => 1,     -- : integer := 0;

          -- AXI FIFO Type
          -- 0 = Data FIFO
          -- 1 = Packet FIFO
          -- 2 = Low Latency Data FIFO
          C_APPLICATION_TYPE_WACH             => 0,     -- : integer := 0;
          C_APPLICATION_TYPE_WDCH             => 0,     -- : integer := 0;
          C_APPLICATION_TYPE_WRCH             => 0,     -- : integer := 0;
          C_APPLICATION_TYPE_RACH             => 0,     -- : integer := 0;
          C_APPLICATION_TYPE_RDCH             => 0,     -- : integer := 0;
          C_APPLICATION_TYPE_AXIS             => 0,     -- : integer := 0;

          -- Enable ECC
          -- 0 = ECC disabled
          -- 1 = ECC enabled
          C_USE_ECC_WACH                      => 0,     -- : integer := 0;
          C_USE_ECC_WDCH                      => 0,     -- : integer := 0;
          C_USE_ECC_WRCH                      => 0,     -- : integer := 0;
          C_USE_ECC_RACH                      => 0,     -- : integer := 0;
          C_USE_ECC_RDCH                      => 0,     -- : integer := 0;
          C_USE_ECC_AXIS                      => 0,     -- : integer := 0;

          -- ECC Error Injection Type
          -- 0 = No Error Injection
          -- 1 = Single Bit Error Injection
          -- 2 = Double Bit Error Injection
          -- 3 = Single Bit and Double Bit Error Injection
          C_ERROR_INJECTION_TYPE_WACH         => 0,     -- : integer := 0;
          C_ERROR_INJECTION_TYPE_WDCH         => 0,     -- : integer := 0;
          C_ERROR_INJECTION_TYPE_WRCH         => 0,     -- : integer := 0;
          C_ERROR_INJECTION_TYPE_RACH         => 0,     -- : integer := 0;
          C_ERROR_INJECTION_TYPE_RDCH         => 0,     -- : integer := 0;
          C_ERROR_INJECTION_TYPE_AXIS         => 0,     -- : integer := 0;

          -- Input Data Width
          -- Accumulation of all AXI input signal's width
          C_DIN_WIDTH_WACH                    => 32,    -- : integer := 1;
          C_DIN_WIDTH_WDCH                    => 64,    -- : integer := 1;
          C_DIN_WIDTH_WRCH                    => 2,     -- : integer := 1;
          C_DIN_WIDTH_RACH                    => 32,    -- : integer := 1;
          C_DIN_WIDTH_RDCH                    => 64,    -- : integer := 1;
          C_DIN_WIDTH_AXIS                    => 1,     -- : integer := 1;

          C_WR_DEPTH_WACH                     => 16,    -- : integer := 16;
          C_WR_DEPTH_WDCH                     => 1024,  -- : integer := 16;
          C_WR_DEPTH_WRCH                     => 16,    -- : integer := 16;
          C_WR_DEPTH_RACH                     => 16,    -- : integer := 16;
          C_WR_DEPTH_RDCH                     => 1024,  -- : integer := 16;
          C_WR_DEPTH_AXIS                     => 1024,  -- : integer := 16;

          C_WR_PNTR_WIDTH_WACH                => 4,     -- : integer := 4;
          C_WR_PNTR_WIDTH_WDCH                => 10,    -- : integer := 4;
          C_WR_PNTR_WIDTH_WRCH                => 4,     -- : integer := 4;
          C_WR_PNTR_WIDTH_RACH                => 4,     -- : integer := 4;
          C_WR_PNTR_WIDTH_RDCH                => 10,    -- : integer := 4;
          C_WR_PNTR_WIDTH_AXIS                => 10,    -- : integer := 4;

          C_HAS_DATA_COUNTS_WACH              => 0,     -- : integer := 0;
          C_HAS_DATA_COUNTS_WDCH              => 0,     -- : integer := 0;
          C_HAS_DATA_COUNTS_WRCH              => 0,     -- : integer := 0;
          C_HAS_DATA_COUNTS_RACH              => 0,     -- : integer := 0;
          C_HAS_DATA_COUNTS_RDCH              => 0,     -- : integer := 0;
          C_HAS_DATA_COUNTS_AXIS              => 0,     -- : integer := 0;

          C_HAS_PROG_FLAGS_WACH               => 0,     -- : integer := 0;
          C_HAS_PROG_FLAGS_WDCH               => 0,     -- : integer := 0;
          C_HAS_PROG_FLAGS_WRCH               => 0,     -- : integer := 0;
          C_HAS_PROG_FLAGS_RACH               => 0,     -- : integer := 0;
          C_HAS_PROG_FLAGS_RDCH               => 0,     -- : integer := 0;
          C_HAS_PROG_FLAGS_AXIS               => 0,     -- : integer := 0;

          C_PROG_FULL_TYPE_WACH               => 5,     -- : integer := 0;
          C_PROG_FULL_TYPE_WDCH               => 5,     -- : integer := 0;
          C_PROG_FULL_TYPE_WRCH               => 5,     -- : integer := 0;
          C_PROG_FULL_TYPE_RACH               => 5,     -- : integer := 0;
          C_PROG_FULL_TYPE_RDCH               => 5,     -- : integer := 0;
          C_PROG_FULL_TYPE_AXIS               => 5,     -- : integer := 0;
          C_PROG_FULL_THRESH_ASSERT_VAL_WACH  => 1023,  -- : integer := 0;
          C_PROG_FULL_THRESH_ASSERT_VAL_WDCH  => 1023,  -- : integer := 0;
          C_PROG_FULL_THRESH_ASSERT_VAL_WRCH  => 1023,  -- : integer := 0;
          C_PROG_FULL_THRESH_ASSERT_VAL_RACH  => 1023,  -- : integer := 0;
          C_PROG_FULL_THRESH_ASSERT_VAL_RDCH  => 1023,  -- : integer := 0;
          C_PROG_FULL_THRESH_ASSERT_VAL_AXIS  => 1023,  -- : integer := 0;

          C_PROG_EMPTY_TYPE_WACH              => 5,     -- : integer := 0;
          C_PROG_EMPTY_TYPE_WDCH              => 5,     -- : integer := 0;
          C_PROG_EMPTY_TYPE_WRCH              => 5,     -- : integer := 0;
          C_PROG_EMPTY_TYPE_RACH              => 5,     -- : integer := 0;
          C_PROG_EMPTY_TYPE_RDCH              => 5,     -- : integer := 0;
          C_PROG_EMPTY_TYPE_AXIS              => 5,     -- : integer := 0;
          C_PROG_EMPTY_THRESH_ASSERT_VAL_WACH => 1022,  -- : integer := 0;
          C_PROG_EMPTY_THRESH_ASSERT_VAL_WDCH => 1022,  -- : integer := 0;
          C_PROG_EMPTY_THRESH_ASSERT_VAL_WRCH => 1022,  -- : integer := 0;
          C_PROG_EMPTY_THRESH_ASSERT_VAL_RACH => 1022,  -- : integer := 0;
          C_PROG_EMPTY_THRESH_ASSERT_VAL_RDCH => 1022,  -- : integer := 0;
          C_PROG_EMPTY_THRESH_ASSERT_VAL_AXIS => 1022,  -- : integer := 0;

          C_REG_SLICE_MODE_WACH               => 0,     -- : integer := 0;
          C_REG_SLICE_MODE_WDCH               => 0,     -- : integer := 0;
          C_REG_SLICE_MODE_WRCH               => 0,     -- : integer := 0;
          C_REG_SLICE_MODE_RACH               => 0,     -- : integer := 0;
          C_REG_SLICE_MODE_RDCH               => 0,     -- : integer := 0;
          C_REG_SLICE_MODE_AXIS               => 0      -- : integer := 0

        )
        port map(
          BACKUP                   => '0',                  -- : IN  std_logic := '0';
          BACKUP_MARKER            => '0',                  -- : IN  std_logic := '0';
          CLK                      => '0',                  -- : IN  std_logic := '0';
          RST                      => RESET_ASYNC_I,        -- : IN  std_logic := '0';
          SRST                     => '0',                  -- : IN  std_logic := '0';
          WR_CLK                   => WR_CLK_I,             -- : IN  std_logic := '0';
          WR_RST                   => RESET_ASYNC_I,        -- : IN  std_logic := '0';
          RD_CLK                   => RD_CLK_I,             -- : IN  std_logic := '0';
          RD_RST                   => RESET_ASYNC_I,        -- : IN  std_logic := '0';
          DIN                      => WR_DATA_I,            -- : IN  std_logic_vector(C_DIN_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          WR_EN                    => WR_EN_I,              -- : IN  std_logic := '0';
          RD_EN                    => RD_EN_I,              -- : IN  std_logic := '0';
          PROG_EMPTY_THRESH        => PROG_RDTHRESH_ZEROS,  -- : IN  std_logic_vector(C_RD_PNTR_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          PROG_EMPTY_THRESH_ASSERT => PROG_RDTHRESH_ZEROS,  -- : IN  std_logic_vector(C_RD_PNTR_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          PROG_EMPTY_THRESH_NEGATE => PROG_RDTHRESH_ZEROS,  -- : IN  std_logic_vector(C_RD_PNTR_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          PROG_FULL_THRESH         => PROG_WRTHRESH_ZEROS,  -- : IN  std_logic_vector(C_WR_PNTR_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          PROG_FULL_THRESH_ASSERT  => PROG_WRTHRESH_ZEROS,  -- : IN  std_logic_vector(C_WR_PNTR_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          PROG_FULL_THRESH_NEGATE  => PROG_WRTHRESH_ZEROS,  -- : IN  std_logic_vector(C_WR_PNTR_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          INT_CLK                  => '0',                  -- : IN  std_logic := '0';
          INJECTDBITERR            => '0', -- new FG 5.1/5.2-- : IN  std_logic := '0';
          INJECTSBITERR            => '0', -- new FG 5.1/5.2-- : IN  std_logic := '0';

          DOUT                     => RD_DATA_O,            -- : OUT std_logic_vector(C_DOUT_WIDTH-1 DOWNTO 0);
          FULL                     => sig_full,             -- : OUT std_logic;
          ALMOST_FULL              => WR_ALMOST_FULL_O,     -- : OUT std_logic;
          WR_ACK                   => WR_ACK_O,             -- : OUT std_logic;
          OVERFLOW                 => WR_ERR_O,             -- : OUT std_logic;
          EMPTY                    => RD_EMPTY_O,           -- : OUT std_logic;
          ALMOST_EMPTY             => RD_ALMOST_EMPTY_O,    -- : OUT std_logic;
          VALID                    => RD_ACK_O,             -- : OUT std_logic;
          UNDERFLOW                => RD_ERR_O,             -- : OUT std_logic;
          DATA_COUNT               => open,                 -- : OUT std_logic_vector(C_DATA_COUNT_WIDTH-1 DOWNTO 0);
          RD_DATA_COUNT            => sig_full_fifo_rdcnt,  -- : OUT std_logic_vector(C_RD_DATA_COUNT_WIDTH-1 DOWNTO 0);
          WR_DATA_COUNT            => sig_full_fifo_wrcnt,  -- : OUT std_logic_vector(C_WR_DATA_COUNT_WIDTH-1 DOWNTO 0);
          PROG_FULL                => WR_PROG_FULL_O,       -- : OUT std_logic;
          PROG_EMPTY               => RD_PROG_EMPTY_O,      -- : OUT std_logic;
          SBITERR                  => open,                 -- : OUT std_logic;
          DBITERR                  => open,                 -- : OUT std_logic

          -- AXI Global Signal
          M_ACLK                   => '0',             -- : IN  std_logic := '0';
          S_ACLK                   => '0',             -- : IN  std_logic := '0';
          S_ARESETN                => '0',             -- : IN  std_logic := '0';
          M_ACLK_EN                => '0',             -- : IN  std_logic := '0';
          S_ACLK_EN                => '0',             -- : IN  std_logic := '0';

          -- AXI Full/Lite Slave Write Channel (write side)
          S_AXI_AWID               => (others => '0'), -- : IN  std_logic_vector(C_AXI_ID_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWADDR             => (others => '0'), -- : IN  std_logic_vector(C_AXI_ADDR_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWLEN              => (others => '0'), -- : IN  std_logic_vector(8-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWSIZE             => (others => '0'), -- : IN  std_logic_vector(3-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWBURST            => (others => '0'), -- : IN  std_logic_vector(2-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWLOCK             => (others => '0'), -- : IN  std_logic_vector(2-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWCACHE            => (others => '0'), -- : IN  std_logic_vector(4-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWPROT             => (others => '0'), -- : IN  std_logic_vector(3-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWQOS              => (others => '0'), -- : IN  std_logic_vector(4-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWREGION           => (others => '0'), -- : IN  std_logic_vector(4-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWUSER             => (others => '0'), -- : IN  std_logic_vector(C_AXI_AWUSER_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWVALID            => '0',             -- : IN  std_logic := '0';
          S_AXI_AWREADY            => open,            -- : OUT std_logic;
          S_AXI_WID                => (others => '0'), -- : IN  std_logic_vector(C_AXI_ID_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_WDATA              => (others => '0'), -- : IN  std_logic_vector(C_AXI_DATA_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_WSTRB              => (others => '0'), -- : IN  std_logic_vector(C_AXI_DATA_WIDTH/8-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_WLAST              => '0',             -- : IN  std_logic := '0';
          S_AXI_WUSER              => (others => '0'), -- : IN  std_logic_vector(C_AXI_WUSER_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_WVALID             => '0',             -- : IN  std_logic := '0';
          S_AXI_WREADY             => open,            -- : OUT std_logic;
          S_AXI_BID                => open,            -- : OUT std_logic_vector(C_AXI_ID_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_BRESP              => open,            -- : OUT std_logic_vector(2-1 DOWNTO 0);
          S_AXI_BUSER              => open,            -- : OUT std_logic_vector(C_AXI_BUSER_WIDTH-1 DOWNTO 0);
          S_AXI_BVALID             => open,            -- : OUT std_logic;
          S_AXI_BREADY             => '0',             -- : IN  std_logic := '0';

          -- AXI Full/Lite Master Write Channel (Read side)
          M_AXI_AWID               => open,            -- : OUT std_logic_vector(C_AXI_ID_WIDTH-1 DOWNTO 0);
          M_AXI_AWADDR             => open,            -- : OUT std_logic_vector(C_AXI_ADDR_WIDTH-1 DOWNTO 0);
          M_AXI_AWLEN              => open,            -- : OUT std_logic_vector(8-1 DOWNTO 0);
          M_AXI_AWSIZE             => open,            -- : OUT std_logic_vector(3-1 DOWNTO 0);
          M_AXI_AWBURST            => open,            -- : OUT std_logic_vector(2-1 DOWNTO 0);
          M_AXI_AWLOCK             => open,            -- : OUT std_logic_vector(2-1 DOWNTO 0);
          M_AXI_AWCACHE            => open,            -- : OUT std_logic_vector(4-1 DOWNTO 0);
          M_AXI_AWPROT             => open,            -- : OUT std_logic_vector(3-1 DOWNTO 0);
          M_AXI_AWQOS              => open,            -- : OUT std_logic_vector(4-1 DOWNTO 0);
          M_AXI_AWREGION           => open,            -- : OUT std_logic_vector(4-1 DOWNTO 0);
          M_AXI_AWUSER             => open,            -- : OUT std_logic_vector(C_AXI_AWUSER_WIDTH-1 DOWNTO 0);
          M_AXI_AWVALID            => open,            -- : OUT std_logic;
          M_AXI_AWREADY            => '0',             -- : IN  std_logic := '0';
          M_AXI_WID                => open,            -- : OUT std_logic_vector(C_AXI_ID_WIDTH-1 DOWNTO 0);
          M_AXI_WDATA              => open,            -- : OUT std_logic_vector(C_AXI_DATA_WIDTH-1 DOWNTO 0);
          M_AXI_WSTRB              => open,            -- : OUT std_logic_vector(C_AXI_DATA_WIDTH/8-1 DOWNTO 0);
          M_AXI_WLAST              => open,            -- : OUT std_logic;
          M_AXI_WUSER              => open,            -- : OUT std_logic_vector(C_AXI_WUSER_WIDTH-1 DOWNTO 0);
          M_AXI_WVALID             => open,            -- : OUT std_logic;
          M_AXI_WREADY             => '0',             -- : IN  std_logic := '0';
          M_AXI_BID                => (others => '0'), -- : IN  std_logic_vector(C_AXI_ID_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_BRESP              => (others => '0'), -- : IN  std_logic_vector(2-1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_BUSER              => (others => '0'), -- : IN  std_logic_vector(C_AXI_BUSER_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_BVALID             => '0',             -- : IN  std_logic := '0';
          M_AXI_BREADY             => open,            -- : OUT std_logic;

          -- AXI Full/Lite Slave Read Channel (Write side)
          S_AXI_ARID               => (others => '0'), -- : IN  std_logic_vector(C_AXI_ID_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARADDR             => (others => '0'), -- : IN  std_logic_vector(C_AXI_ADDR_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARLEN              => (others => '0'), -- : IN  std_logic_vector(8-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARSIZE             => (others => '0'), -- : IN  std_logic_vector(3-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARBURST            => (others => '0'), -- : IN  std_logic_vector(2-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARLOCK             => (others => '0'), -- : IN  std_logic_vector(2-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARCACHE            => (others => '0'), -- : IN  std_logic_vector(4-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARPROT             => (others => '0'), -- : IN  std_logic_vector(3-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARQOS              => (others => '0'), -- : IN  std_logic_vector(4-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARREGION           => (others => '0'), -- : IN  std_logic_vector(4-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARUSER             => (others => '0'), -- : IN  std_logic_vector(C_AXI_ARUSER_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARVALID            => '0',             -- : IN  std_logic := '0';
          S_AXI_ARREADY            => open,            -- : OUT std_logic;
          S_AXI_RID                => open,            -- : OUT std_logic_vector(C_AXI_ID_WIDTH-1 DOWNTO 0);
          S_AXI_RDATA              => open,            -- : OUT std_logic_vector(C_AXI_DATA_WIDTH-1 DOWNTO 0);
          S_AXI_RRESP              => open,            -- : OUT std_logic_vector(2-1 DOWNTO 0);
          S_AXI_RLAST              => open,            -- : OUT std_logic;
          S_AXI_RUSER              => open,            -- : OUT std_logic_vector(C_AXI_RUSER_WIDTH-1 DOWNTO 0);
          S_AXI_RVALID             => open,            -- : OUT std_logic;
          S_AXI_RREADY             => '0',             -- : IN  std_logic := '0';

          -- AXI Full/Lite Master Read Channel (Read side)
          M_AXI_ARID               => open,            -- : OUT std_logic_vector(C_AXI_ID_WIDTH-1 DOWNTO 0);
          M_AXI_ARADDR             => open,            -- : OUT std_logic_vector(C_AXI_ADDR_WIDTH-1 DOWNTO 0);
          M_AXI_ARLEN              => open,            -- : OUT std_logic_vector(8-1 DOWNTO 0);
          M_AXI_ARSIZE             => open,            -- : OUT std_logic_vector(3-1 DOWNTO 0);
          M_AXI_ARBURST            => open,            -- : OUT std_logic_vector(2-1 DOWNTO 0);
          M_AXI_ARLOCK             => open,            -- : OUT std_logic_vector(2-1 DOWNTO 0);
          M_AXI_ARCACHE            => open,            -- : OUT std_logic_vector(4-1 DOWNTO 0);
          M_AXI_ARPROT             => open,            -- : OUT std_logic_vector(3-1 DOWNTO 0);
          M_AXI_ARQOS              => open,            -- : OUT std_logic_vector(4-1 DOWNTO 0);
          M_AXI_ARREGION           => open,            -- : OUT std_logic_vector(4-1 DOWNTO 0);
          M_AXI_ARUSER             => open,            -- : OUT std_logic_vector(C_AXI_ARUSER_WIDTH-1 DOWNTO 0);
          M_AXI_ARVALID            => open,            -- : OUT std_logic;
          M_AXI_ARREADY            => '0',             -- : IN  std_logic := '0';
          M_AXI_RID                => (others => '0'), -- : IN  std_logic_vector(C_AXI_ID_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_RDATA              => (others => '0'), -- : IN  std_logic_vector(C_AXI_DATA_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_RRESP              => (others => '0'), -- : IN  std_logic_vector(2-1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_RLAST              => '0',             -- : IN  std_logic := '0';
          M_AXI_RUSER              => (others => '0'), -- : IN  std_logic_vector(C_AXI_RUSER_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_RVALID             => '0',             -- : IN  std_logic := '0';
          M_AXI_RREADY             => open,            -- : OUT std_logic;

          -- AXI Streaming Slave Signals (Write side)
          S_AXIS_TVALID            => '0',             -- : IN  std_logic := '0';
          S_AXIS_TREADY            => open,            -- : OUT std_logic;
          S_AXIS_TDATA             => (others => '0'), -- : IN  std_logic_vector(C_AXIS_TDATA_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          S_AXIS_TSTRB             => (others => '0'), -- : IN  std_logic_vector(C_AXIS_TSTRB_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          S_AXIS_TKEEP             => (others => '0'), -- : IN  std_logic_vector(C_AXIS_TKEEP_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          S_AXIS_TLAST             => '0',             -- : IN  std_logic := '0';
          S_AXIS_TID               => (others => '0'), -- : IN  std_logic_vector(C_AXIS_TID_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          S_AXIS_TDEST             => (others => '0'), -- : IN  std_logic_vector(C_AXIS_TDEST_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          S_AXIS_TUSER             => (others => '0'), -- : IN  std_logic_vector(C_AXIS_TUSER_WIDTH-1 DOWNTO 0) := (OTHERS => '0');

          -- AXI Streaming Master Signals (Read side)
          M_AXIS_TVALID            => open,            -- : OUT std_logic;
          M_AXIS_TREADY            => '0',             -- : IN  std_logic := '0';
          M_AXIS_TDATA             => open,            -- : OUT std_logic_vector(C_AXIS_TDATA_WIDTH-1 DOWNTO 0);
          M_AXIS_TSTRB             => open,            -- : OUT std_logic_vector(C_AXIS_TSTRB_WIDTH-1 DOWNTO 0);
          M_AXIS_TKEEP             => open,            -- : OUT std_logic_vector(C_AXIS_TKEEP_WIDTH-1 DOWNTO 0);
          M_AXIS_TLAST             => open,            -- : OUT std_logic;
          M_AXIS_TID               => open,            -- : OUT std_logic_vector(C_AXIS_TID_WIDTH-1 DOWNTO 0);
          M_AXIS_TDEST             => open,            -- : OUT std_logic_vector(C_AXIS_TDEST_WIDTH-1 DOWNTO 0);
          M_AXIS_TUSER             => open,            -- : OUT std_logic_vector(C_AXIS_TUSER_WIDTH-1 DOWNTO 0);

          -- AXI Full/Lite Write Address Channel Signals
          AXI_AW_INJECTSBITERR     => '0',             -- : IN  std_logic := '0';
          AXI_AW_INJECTDBITERR     => '0',             -- : IN  std_logic := '0';
          AXI_AW_PROG_FULL_THRESH  => (others => '0'), -- : IN  std_logic_vector(C_WR_PNTR_WIDTH_WACH-1 DOWNTO 0) := (OTHERS => '0');
          AXI_AW_PROG_EMPTY_THRESH => (others => '0'), -- : IN  std_logic_vector(C_WR_PNTR_WIDTH_WACH-1 DOWNTO 0) := (OTHERS => '0');
          AXI_AW_DATA_COUNT        => open,            -- : OUT std_logic_vector(C_WR_PNTR_WIDTH_WACH DOWNTO 0);
          AXI_AW_WR_DATA_COUNT     => open,            -- : OUT std_logic_vector(C_WR_PNTR_WIDTH_WACH DOWNTO 0);
          AXI_AW_RD_DATA_COUNT     => open,            -- : OUT std_logic_vector(C_WR_PNTR_WIDTH_WACH DOWNTO 0);
          AXI_AW_SBITERR           => open,            -- : OUT std_logic;
          AXI_AW_DBITERR           => open,            -- : OUT std_logic;
          AXI_AW_OVERFLOW          => open,            -- : OUT std_logic;
          AXI_AW_UNDERFLOW         => open,            -- : OUT std_logic;

          -- AXI Full/Lite Write Data Channel Signals
          AXI_W_INJECTSBITERR      => '0',             -- : IN  std_logic := '0';
          AXI_W_INJECTDBITERR      => '0',             -- : IN  std_logic := '0';
          AXI_W_PROG_FULL_THRESH   => (others => '0'), -- : IN  std_logic_vector(C_WR_PNTR_WIDTH_WDCH-1 DOWNTO 0) := (OTHERS => '0');
          AXI_W_PROG_EMPTY_THRESH  => (others => '0'), -- : IN  std_logic_vector(C_WR_PNTR_WIDTH_WDCH-1 DOWNTO 0) := (OTHERS => '0');
          AXI_W_DATA_COUNT         => open,            -- : OUT std_logic_vector(C_WR_PNTR_WIDTH_WDCH DOWNTO 0);
          AXI_W_WR_DATA_COUNT      => open,            -- : OUT std_logic_vector(C_WR_PNTR_WIDTH_WDCH DOWNTO 0);
          AXI_W_RD_DATA_COUNT      => open,            -- : OUT std_logic_vector(C_WR_PNTR_WIDTH_WDCH DOWNTO 0);
          AXI_W_SBITERR            => open,            -- : OUT std_logic;
          AXI_W_DBITERR            => open,            -- : OUT std_logic;
          AXI_W_OVERFLOW           => open,            -- : OUT std_logic;
          AXI_W_UNDERFLOW          => open,            -- : OUT std_logic;

          -- AXI Full/Lite Write Response Channel Signals
          AXI_B_INJECTSBITERR      => '0',             -- : IN  std_logic := '0';
          AXI_B_INJECTDBITERR      => '0',             -- : IN  std_logic := '0';
          AXI_B_PROG_FULL_THRESH   => (others => '0'), -- : IN  std_logic_vector(C_WR_PNTR_WIDTH_WRCH-1 DOWNTO 0) := (OTHERS => '0');
          AXI_B_PROG_EMPTY_THRESH  => (others => '0'), -- : IN  std_logic_vector(C_WR_PNTR_WIDTH_WRCH-1 DOWNTO 0) := (OTHERS => '0');
          AXI_B_DATA_COUNT         => open,            -- : OUT std_logic_vector(C_WR_PNTR_WIDTH_WRCH DOWNTO 0);
          AXI_B_WR_DATA_COUNT      => open,            -- : OUT std_logic_vector(C_WR_PNTR_WIDTH_WRCH DOWNTO 0);
          AXI_B_RD_DATA_COUNT      => open,            -- : OUT std_logic_vector(C_WR_PNTR_WIDTH_WRCH DOWNTO 0);
          AXI_B_SBITERR            => open,            -- : OUT std_logic;
          AXI_B_DBITERR            => open,            -- : OUT std_logic;
          AXI_B_OVERFLOW           => open,            -- : OUT std_logic;
          AXI_B_UNDERFLOW          => open,            -- : OUT std_logic;

          -- AXI Full/Lite Read Address Channel Signals
          AXI_AR_INJECTSBITERR     => '0',             -- : IN  std_logic := '0';
          AXI_AR_INJECTDBITERR     => '0',             -- : IN  std_logic := '0';
          AXI_AR_PROG_FULL_THRESH  => (others => '0'), -- : IN  std_logic_vector(C_WR_PNTR_WIDTH_RACH-1 DOWNTO 0) := (OTHERS => '0');
          AXI_AR_PROG_EMPTY_THRESH => (others => '0'), -- : IN  std_logic_vector(C_WR_PNTR_WIDTH_RACH-1 DOWNTO 0) := (OTHERS => '0');
          AXI_AR_DATA_COUNT        => open,            -- : OUT std_logic_vector(C_WR_PNTR_WIDTH_RACH DOWNTO 0);
          AXI_AR_WR_DATA_COUNT     => open,            -- : OUT std_logic_vector(C_WR_PNTR_WIDTH_RACH DOWNTO 0);
          AXI_AR_RD_DATA_COUNT     => open,            -- : OUT std_logic_vector(C_WR_PNTR_WIDTH_RACH DOWNTO 0);
          AXI_AR_SBITERR           => open,            -- : OUT std_logic;
          AXI_AR_DBITERR           => open,            -- : OUT std_logic;
          AXI_AR_OVERFLOW          => open,            -- : OUT std_logic;
          AXI_AR_UNDERFLOW         => open,            -- : OUT std_logic;

          -- AXI Full/Lite Read Data Channel Signals
          AXI_R_INJECTSBITERR      => '0',             -- : IN  std_logic := '0';
          AXI_R_INJECTDBITERR      => '0',             -- : IN  std_logic := '0';
          AXI_R_PROG_FULL_THRESH   => (others => '0'), -- : IN  std_logic_vector(C_WR_PNTR_WIDTH_RDCH-1 DOWNTO 0) := (OTHERS => '0');
          AXI_R_PROG_EMPTY_THRESH  => (others => '0'), -- : IN  std_logic_vector(C_WR_PNTR_WIDTH_RDCH-1 DOWNTO 0) := (OTHERS => '0');
          AXI_R_DATA_COUNT         => open,            -- : OUT std_logic_vector(C_WR_PNTR_WIDTH_RDCH DOWNTO 0);
          AXI_R_WR_DATA_COUNT      => open,            -- : OUT std_logic_vector(C_WR_PNTR_WIDTH_RDCH DOWNTO 0);
          AXI_R_RD_DATA_COUNT      => open,            -- : OUT std_logic_vector(C_WR_PNTR_WIDTH_RDCH DOWNTO 0);
          AXI_R_SBITERR            => open,            -- : OUT std_logic;
          AXI_R_DBITERR            => open,            -- : OUT std_logic;
          AXI_R_OVERFLOW           => open,            -- : OUT std_logic;
          AXI_R_UNDERFLOW          => open,            -- : OUT std_logic;

          -- AXI Streaming FIFO Related Signals
          AXIS_INJECTSBITERR       => '0',             -- : IN  std_logic := '0';
          AXIS_INJECTDBITERR       => '0',             -- : IN  std_logic := '0';
          AXIS_PROG_FULL_THRESH    => (others => '0'), -- : IN  std_logic_vector(C_WR_PNTR_WIDTH_AXIS-1 DOWNTO 0) := (OTHERS => '0');
          AXIS_PROG_EMPTY_THRESH   => (others => '0'), -- : IN  std_logic_vector(C_WR_PNTR_WIDTH_AXIS-1 DOWNTO 0) := (OTHERS => '0');
          AXIS_DATA_COUNT          => open,            -- : OUT std_logic_vector(C_WR_PNTR_WIDTH_AXIS DOWNTO 0);
          AXIS_WR_DATA_COUNT       => open,            -- : OUT std_logic_vector(C_WR_PNTR_WIDTH_AXIS DOWNTO 0);
          AXIS_RD_DATA_COUNT       => open,            -- : OUT std_logic_vector(C_WR_PNTR_WIDTH_AXIS DOWNTO 0);
          AXIS_SBITERR             => open,            -- : OUT std_logic;
          AXIS_DBITERR             => open,            -- : OUT std_logic;
          AXIS_OVERFLOW            => open,            -- : OUT std_logic;
          AXIS_UNDERFLOW           => open             -- : OUT std_logic
        );

    end generate fifogen_ise_12_4;

    fifogen_ise_14_7 : if (C_ISE_14_7) generate

      -------------------------------------------------------------------------------------
      -- Start FIFO Generator Component for fifo_generator_v9_3
      -- The Component declaration for fifo_generator_v9_3 pulled from the
      -- Coregen version of
      -- file: fifo_generator_v9_3_comp.vhd.
      --
      -- This component is used for both dual clock (async) and synchronous fifos
      -- implemented with BRAM or distributed RAM. Hard FIFO simulation support may not
      -- be provided in FIFO Generator V8.2 so not supported here.
      --
      -- Note: AXI ports and parameters added for this version of FIFO Generator.
      --
      -------------------------------------------------------------------------------------
      COMPONENT fifo_generator_v9_3
        GENERIC(
          --------------------------------------------------------------------------------
          -- Generic Declarations (verilog model ordering)
          --------------------------------------------------------------------------------
          C_COMMON_CLOCK                      : integer := 0;
          C_COUNT_TYPE                        : integer := 0;
          C_DATA_COUNT_WIDTH                  : integer := 2;
          C_DEFAULT_VALUE                     : string := "";
          C_DIN_WIDTH                         : integer := 8;
          C_DOUT_RST_VAL                      : string := "";
          C_DOUT_WIDTH                        : integer := 8;
          C_ENABLE_RLOCS                      : integer := 0;
          C_FAMILY                            : string := "";
          C_FULL_FLAGS_RST_VAL                : integer := 1;
          C_HAS_ALMOST_EMPTY                  : integer := 0;
          C_HAS_ALMOST_FULL                   : integer := 0;
          C_HAS_BACKUP                        : integer := 0;
          C_HAS_DATA_COUNT                    : integer := 0;
          C_HAS_INT_CLK                       : integer := 0;
          C_HAS_MEMINIT_FILE                  : integer := 0;
          C_HAS_OVERFLOW                      : integer := 0;
          C_HAS_RD_DATA_COUNT                 : integer := 0;
          C_HAS_RD_RST                        : integer := 0;
          C_HAS_RST                           : integer := 1;
          C_HAS_SRST                          : integer := 0;
          C_HAS_UNDERFLOW                     : integer := 0;
          C_HAS_VALID                         : integer := 0;
          C_HAS_WR_ACK                        : integer := 0;
          C_HAS_WR_DATA_COUNT                 : integer := 0;
          C_HAS_WR_RST                        : integer := 0;
          C_IMPLEMENTATION_TYPE               : integer := 0;
          C_INIT_WR_PNTR_VAL                  : integer := 0;
          C_MEMORY_TYPE                       : integer := 1;
          C_MIF_FILE_NAME                     : string := "";
          C_OPTIMIZATION_MODE                 : integer := 0;
          C_OVERFLOW_LOW                      : integer := 0;
          C_PRELOAD_LATENCY                   : integer := 1;
          C_PRELOAD_REGS                      : integer := 0;
          C_PRIM_FIFO_TYPE                    : string := "4kx4";
          C_PROG_EMPTY_THRESH_ASSERT_VAL      : integer := 0;
          C_PROG_EMPTY_THRESH_NEGATE_VAL      : integer := 0;
          C_PROG_EMPTY_TYPE                   : integer := 0;
          C_PROG_FULL_THRESH_ASSERT_VAL       : integer := 0;
          C_PROG_FULL_THRESH_NEGATE_VAL       : integer := 0;
          C_PROG_FULL_TYPE                    : integer := 0;
          C_RD_DATA_COUNT_WIDTH               : integer := 2;
          C_RD_DEPTH                          : integer := 256;
          C_RD_FREQ                           : integer := 1;
          C_RD_PNTR_WIDTH                     : integer := 8;
          C_UNDERFLOW_LOW                     : integer := 0;
          C_USE_DOUT_RST                      : integer := 0;
          C_USE_ECC                           : integer := 0;
          C_USE_EMBEDDED_REG                  : integer := 0;
          C_USE_FIFO16_FLAGS                  : integer := 0;
          C_USE_FWFT_DATA_COUNT               : integer := 0;
          C_VALID_LOW                         : integer := 0;
          C_WR_ACK_LOW                        : integer := 0;
          C_WR_DATA_COUNT_WIDTH               : integer := 2;
          C_WR_DEPTH                          : integer := 256;
          C_WR_FREQ                           : integer := 1;
          C_WR_PNTR_WIDTH                     : integer := 8;
          C_WR_RESPONSE_LATENCY               : integer := 1;
          C_MSGON_VAL                         : integer := 1;
          C_ENABLE_RST_SYNC                   : integer := 1;
          C_ERROR_INJECTION_TYPE              : integer := 0;
          C_SYNCHRONIZER_STAGE                : integer := 2;

          -- AXI Interface related parameters start here
          C_INTERFACE_TYPE                    : integer := 0; -- 0: Native Interface; 1: AXI Interface
          C_AXI_TYPE                          : integer := 0; -- 0: AXI Stream; 1: AXI Full; 2: AXI Lite
          C_HAS_AXI_WR_CHANNEL                : integer := 0;
          C_HAS_AXI_RD_CHANNEL                : integer := 0;
          C_HAS_SLAVE_CE                      : integer := 0;
          C_HAS_MASTER_CE                     : integer := 0;
          C_ADD_NGC_CONSTRAINT                : integer := 0;
          C_USE_COMMON_OVERFLOW               : integer := 0;
          C_USE_COMMON_UNDERFLOW              : integer := 0;
          C_USE_DEFAULT_SETTINGS              : integer := 0;

          -- AXI Full/Lite
          C_AXI_ID_WIDTH                      : integer := 0;
          C_AXI_ADDR_WIDTH                    : integer := 0;
          C_AXI_DATA_WIDTH                    : integer := 0;
          C_HAS_AXI_AWUSER                    : integer := 0;
          C_HAS_AXI_WUSER                     : integer := 0;
          C_HAS_AXI_BUSER                     : integer := 0;
          C_HAS_AXI_ARUSER                    : integer := 0;
          C_HAS_AXI_RUSER                     : integer := 0;
          C_AXI_ARUSER_WIDTH                  : integer := 0;
          C_AXI_AWUSER_WIDTH                  : integer := 0;
          C_AXI_WUSER_WIDTH                   : integer := 0;
          C_AXI_BUSER_WIDTH                   : integer := 0;
          C_AXI_RUSER_WIDTH                   : integer := 0;

          -- AXI Streaming
          C_HAS_AXIS_TDATA                    : integer := 0;
          C_HAS_AXIS_TID                      : integer := 0;
          C_HAS_AXIS_TDEST                    : integer := 0;
          C_HAS_AXIS_TUSER                    : integer := 0;
          C_HAS_AXIS_TREADY                   : integer := 0;
          C_HAS_AXIS_TLAST                    : integer := 0;
          C_HAS_AXIS_TSTRB                    : integer := 0;
          C_HAS_AXIS_TKEEP                    : integer := 0;
          C_AXIS_TDATA_WIDTH                  : integer := 1;
          C_AXIS_TID_WIDTH                    : integer := 1;
          C_AXIS_TDEST_WIDTH                  : integer := 1;
          C_AXIS_TUSER_WIDTH                  : integer := 1;
          C_AXIS_TSTRB_WIDTH                  : integer := 1;
          C_AXIS_TKEEP_WIDTH                  : integer := 1;

          -- AXI Channel Type
          -- WACH --> Write Address Channel
          -- WDCH --> Write Data Channel
          -- WRCH --> Write Response Channel
          -- RACH --> Read Address Channel
          -- RDCH --> Read Data Channel
          -- AXIS --> AXI Streaming
          C_WACH_TYPE                         : integer := 0; -- 0 = FIFO; 1 = Register Slice; 2 = Pass Through Logic
          C_WDCH_TYPE                         : integer := 0; -- 0 = FIFO; 1 = Register Slice; 2 = Pass Through Logie
          C_WRCH_TYPE                         : integer := 0; -- 0 = FIFO; 1 = Register Slice; 2 = Pass Through Logie
          C_RACH_TYPE                         : integer := 0; -- 0 = FIFO; 1 = Register Slice; 2 = Pass Through Logie
          C_RDCH_TYPE                         : integer := 0; -- 0 = FIFO; 1 = Register Slice; 2 = Pass Through Logie
          C_AXIS_TYPE                         : integer := 0; -- 0 = FIFO; 1 = Register Slice; 2 = Pass Through Logie

          -- AXI Implementation Type
          -- 1 = Common Clock Block RAM FIFO
          -- 2 = Common Clock Distributed RAM FIFO
          -- 11 = Independent Clock Block RAM FIFO
          -- 12 = Independent Clock Distributed RAM FIFO
          C_IMPLEMENTATION_TYPE_WACH          : integer := 0;
          C_IMPLEMENTATION_TYPE_WDCH          : integer := 0;
          C_IMPLEMENTATION_TYPE_WRCH          : integer := 0;
          C_IMPLEMENTATION_TYPE_RACH          : integer := 0;
          C_IMPLEMENTATION_TYPE_RDCH          : integer := 0;
          C_IMPLEMENTATION_TYPE_AXIS          : integer := 0;

          -- AXI FIFO Type
          -- 0 = Data FIFO
          -- 1 = Packet FIFO
          -- 2 = Low Latency Data FIFO
          C_APPLICATION_TYPE_WACH             : integer := 0;
          C_APPLICATION_TYPE_WDCH             : integer := 0;
          C_APPLICATION_TYPE_WRCH             : integer := 0;
          C_APPLICATION_TYPE_RACH             : integer := 0;
          C_APPLICATION_TYPE_RDCH             : integer := 0;
          C_APPLICATION_TYPE_AXIS             : integer := 0;

          -- Enable ECC
          -- 0 = ECC disabled
          -- 1 = ECC enabled
          C_USE_ECC_WACH                      : integer := 0;
          C_USE_ECC_WDCH                      : integer := 0;
          C_USE_ECC_WRCH                      : integer := 0;
          C_USE_ECC_RACH                      : integer := 0;
          C_USE_ECC_RDCH                      : integer := 0;
          C_USE_ECC_AXIS                      : integer := 0;

          -- ECC Error Injection Type
          -- 0 = No Error Injection
          -- 1 = Single Bit Error Injection
          -- 2 = Double Bit Error Injection
          -- 3 = Single Bit and Double Bit Error Injection
          C_ERROR_INJECTION_TYPE_WACH         : integer := 0;
          C_ERROR_INJECTION_TYPE_WDCH         : integer := 0;
          C_ERROR_INJECTION_TYPE_WRCH         : integer := 0;
          C_ERROR_INJECTION_TYPE_RACH         : integer := 0;
          C_ERROR_INJECTION_TYPE_RDCH         : integer := 0;
          C_ERROR_INJECTION_TYPE_AXIS         : integer := 0;

          -- Input Data Width
          -- Accumulation of all AXI input signal's width
          C_DIN_WIDTH_WACH                    : integer := 1;
          C_DIN_WIDTH_WDCH                    : integer := 1;
          C_DIN_WIDTH_WRCH                    : integer := 1;
          C_DIN_WIDTH_RACH                    : integer := 1;
          C_DIN_WIDTH_RDCH                    : integer := 1;
          C_DIN_WIDTH_AXIS                    : integer := 1;

          C_WR_DEPTH_WACH                     : integer := 16;
          C_WR_DEPTH_WDCH                     : integer := 16;
          C_WR_DEPTH_WRCH                     : integer := 16;
          C_WR_DEPTH_RACH                     : integer := 16;
          C_WR_DEPTH_RDCH                     : integer := 16;
          C_WR_DEPTH_AXIS                     : integer := 16;

          C_WR_PNTR_WIDTH_WACH                : integer := 4;
          C_WR_PNTR_WIDTH_WDCH                : integer := 4;
          C_WR_PNTR_WIDTH_WRCH                : integer := 4;
          C_WR_PNTR_WIDTH_RACH                : integer := 4;
          C_WR_PNTR_WIDTH_RDCH                : integer := 4;
          C_WR_PNTR_WIDTH_AXIS                : integer := 4;

          C_HAS_DATA_COUNTS_WACH              : integer := 0;
          C_HAS_DATA_COUNTS_WDCH              : integer := 0;
          C_HAS_DATA_COUNTS_WRCH              : integer := 0;
          C_HAS_DATA_COUNTS_RACH              : integer := 0;
          C_HAS_DATA_COUNTS_RDCH              : integer := 0;
          C_HAS_DATA_COUNTS_AXIS              : integer := 0;

          C_HAS_PROG_FLAGS_WACH               : integer := 0;
          C_HAS_PROG_FLAGS_WDCH               : integer := 0;
          C_HAS_PROG_FLAGS_WRCH               : integer := 0;
          C_HAS_PROG_FLAGS_RACH               : integer := 0;
          C_HAS_PROG_FLAGS_RDCH               : integer := 0;
          C_HAS_PROG_FLAGS_AXIS               : integer := 0;

          C_PROG_FULL_TYPE_WACH               : integer := 0;
          C_PROG_FULL_TYPE_WDCH               : integer := 0;
          C_PROG_FULL_TYPE_WRCH               : integer := 0;
          C_PROG_FULL_TYPE_RACH               : integer := 0;
          C_PROG_FULL_TYPE_RDCH               : integer := 0;
          C_PROG_FULL_TYPE_AXIS               : integer := 0;
          C_PROG_FULL_THRESH_ASSERT_VAL_WACH  : integer := 0;
          C_PROG_FULL_THRESH_ASSERT_VAL_WDCH  : integer := 0;
          C_PROG_FULL_THRESH_ASSERT_VAL_WRCH  : integer := 0;
          C_PROG_FULL_THRESH_ASSERT_VAL_RACH  : integer := 0;
          C_PROG_FULL_THRESH_ASSERT_VAL_RDCH  : integer := 0;
          C_PROG_FULL_THRESH_ASSERT_VAL_AXIS  : integer := 0;

          C_PROG_EMPTY_TYPE_WACH              : integer := 0;
          C_PROG_EMPTY_TYPE_WDCH              : integer := 0;
          C_PROG_EMPTY_TYPE_WRCH              : integer := 0;
          C_PROG_EMPTY_TYPE_RACH              : integer := 0;
          C_PROG_EMPTY_TYPE_RDCH              : integer := 0;
          C_PROG_EMPTY_TYPE_AXIS              : integer := 0;
          C_PROG_EMPTY_THRESH_ASSERT_VAL_WACH : integer := 0;
          C_PROG_EMPTY_THRESH_ASSERT_VAL_WDCH : integer := 0;
          C_PROG_EMPTY_THRESH_ASSERT_VAL_WRCH : integer := 0;
          C_PROG_EMPTY_THRESH_ASSERT_VAL_RACH : integer := 0;
          C_PROG_EMPTY_THRESH_ASSERT_VAL_RDCH : integer := 0;
          C_PROG_EMPTY_THRESH_ASSERT_VAL_AXIS : integer := 0;

          C_REG_SLICE_MODE_WACH               : integer := 0;
          C_REG_SLICE_MODE_WDCH               : integer := 0;
          C_REG_SLICE_MODE_WRCH               : integer := 0;
          C_REG_SLICE_MODE_RACH               : integer := 0;
          C_REG_SLICE_MODE_RDCH               : integer := 0;
          C_REG_SLICE_MODE_AXIS               : integer := 0
        );

        PORT(
          ------------------------------------------------------------------------------
          -- Input and Output Declarations
          ------------------------------------------------------------------------------

          -- Conventional FIFO Interface Signals
          BACKUP                   : IN  std_logic := '0';
          BACKUP_MARKER            : IN  std_logic := '0';
          CLK                      : IN  std_logic := '0';
          RST                      : IN  std_logic := '0';
          SRST                     : IN  std_logic := '0';
          WR_CLK                   : IN  std_logic := '0';
          WR_RST                   : IN  std_logic := '0';
          RD_CLK                   : IN  std_logic := '0';
          RD_RST                   : IN  std_logic := '0';
          DIN                      : IN  std_logic_vector(C_DIN_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          WR_EN                    : IN  std_logic := '0';
          RD_EN                    : IN  std_logic := '0';

          -- Optional inputs
          PROG_EMPTY_THRESH        : IN  std_logic_vector(C_RD_PNTR_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          PROG_EMPTY_THRESH_ASSERT : IN  std_logic_vector(C_RD_PNTR_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          PROG_EMPTY_THRESH_NEGATE : IN  std_logic_vector(C_RD_PNTR_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          PROG_FULL_THRESH         : IN  std_logic_vector(C_WR_PNTR_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          PROG_FULL_THRESH_ASSERT  : IN  std_logic_vector(C_WR_PNTR_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          PROG_FULL_THRESH_NEGATE  : IN  std_logic_vector(C_WR_PNTR_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          INT_CLK                  : IN  std_logic := '0';
          INJECTDBITERR            : IN  std_logic := '0';
          INJECTSBITERR            : IN  std_logic := '0';

          DOUT                     : OUT std_logic_vector(C_DOUT_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          FULL                     : OUT std_logic := '0';
          ALMOST_FULL              : OUT std_logic := '0';
          WR_ACK                   : OUT std_logic := '0';
          OVERFLOW                 : OUT std_logic := '0';
          EMPTY                    : OUT std_logic := '1';
          ALMOST_EMPTY             : OUT std_logic := '1';
          VALID                    : OUT std_logic := '0';
          UNDERFLOW                : OUT std_logic := '0';
          DATA_COUNT               : OUT std_logic_vector(C_DATA_COUNT_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          RD_DATA_COUNT            : OUT std_logic_vector(C_RD_DATA_COUNT_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          WR_DATA_COUNT            : OUT std_logic_vector(C_WR_DATA_COUNT_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          PROG_FULL                : OUT std_logic := '0';
          PROG_EMPTY               : OUT std_logic := '1';
          SBITERR                  : OUT std_logic := '0';
          DBITERR                  : OUT std_logic := '0';

          -- AXI Global Signal
          M_ACLK                   : IN  std_logic := '0';
          S_ACLK                   : IN  std_logic := '0';
          S_ARESETN                : IN  std_logic := '1'; -- Active low reset, default value set to 1
          M_ACLK_EN                : IN  std_logic := '0';
          S_ACLK_EN                : IN  std_logic := '0';

          -- AXI Full/Lite Slave Write Channel (write side)
          S_AXI_AWID               : IN  std_logic_vector(C_AXI_ID_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWADDR             : IN  std_logic_vector(C_AXI_ADDR_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWLEN              : IN  std_logic_vector(8 - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWSIZE             : IN  std_logic_vector(3 - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWBURST            : IN  std_logic_vector(2 - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWLOCK             : IN  std_logic_vector(2 - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWCACHE            : IN  std_logic_vector(4 - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWPROT             : IN  std_logic_vector(3 - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWQOS              : IN  std_logic_vector(4 - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWREGION           : IN  std_logic_vector(4 - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWUSER             : IN  std_logic_vector(C_AXI_AWUSER_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWVALID            : IN  std_logic := '0';
          S_AXI_AWREADY            : OUT std_logic := '0';
          S_AXI_WID                : IN  std_logic_vector(C_AXI_ID_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_WDATA              : IN  std_logic_vector(C_AXI_DATA_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_WSTRB              : IN  std_logic_vector(C_AXI_DATA_WIDTH / 8 - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_WLAST              : IN  std_logic := '0';
          S_AXI_WUSER              : IN  std_logic_vector(C_AXI_WUSER_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_WVALID             : IN  std_logic := '0';
          S_AXI_WREADY             : OUT std_logic := '0';
          S_AXI_BID                : OUT std_logic_vector(C_AXI_ID_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_BRESP              : OUT std_logic_vector(2 - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_BUSER              : OUT std_logic_vector(C_AXI_BUSER_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_BVALID             : OUT std_logic := '0';
          S_AXI_BREADY             : IN  std_logic := '0';

          -- AXI Full/Lite Master Write Channel (Read side)
          M_AXI_AWID               : OUT std_logic_vector(C_AXI_ID_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_AWADDR             : OUT std_logic_vector(C_AXI_ADDR_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_AWLEN              : OUT std_logic_vector(8 - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_AWSIZE             : OUT std_logic_vector(3 - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_AWBURST            : OUT std_logic_vector(2 - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_AWLOCK             : OUT std_logic_vector(2 - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_AWCACHE            : OUT std_logic_vector(4 - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_AWPROT             : OUT std_logic_vector(3 - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_AWQOS              : OUT std_logic_vector(4 - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_AWREGION           : OUT std_logic_vector(4 - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_AWUSER             : OUT std_logic_vector(C_AXI_AWUSER_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_AWVALID            : OUT std_logic := '0';
          M_AXI_AWREADY            : IN  std_logic := '0';
          M_AXI_WID                : OUT std_logic_vector(C_AXI_ID_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_WDATA              : OUT std_logic_vector(C_AXI_DATA_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_WSTRB              : OUT std_logic_vector(C_AXI_DATA_WIDTH / 8 - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_WLAST              : OUT std_logic := '0';
          M_AXI_WUSER              : OUT std_logic_vector(C_AXI_WUSER_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_WVALID             : OUT std_logic := '0';
          M_AXI_WREADY             : IN  std_logic := '0';
          M_AXI_BID                : IN  std_logic_vector(C_AXI_ID_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_BRESP              : IN  std_logic_vector(2 - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_BUSER              : IN  std_logic_vector(C_AXI_BUSER_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_BVALID             : IN  std_logic := '0';
          M_AXI_BREADY             : OUT std_logic := '0';

          -- AXI Full/Lite Slave Read Channel (Write side)
          S_AXI_ARID               : IN  std_logic_vector(C_AXI_ID_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARADDR             : IN  std_logic_vector(C_AXI_ADDR_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARLEN              : IN  std_logic_vector(8 - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARSIZE             : IN  std_logic_vector(3 - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARBURST            : IN  std_logic_vector(2 - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARLOCK             : IN  std_logic_vector(2 - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARCACHE            : IN  std_logic_vector(4 - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARPROT             : IN  std_logic_vector(3 - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARQOS              : IN  std_logic_vector(4 - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARREGION           : IN  std_logic_vector(4 - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARUSER             : IN  std_logic_vector(C_AXI_ARUSER_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARVALID            : IN  std_logic := '0';
          S_AXI_ARREADY            : OUT std_logic := '0';
          S_AXI_RID                : OUT std_logic_vector(C_AXI_ID_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_RDATA              : OUT std_logic_vector(C_AXI_DATA_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_RRESP              : OUT std_logic_vector(2 - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_RLAST              : OUT std_logic := '0';
          S_AXI_RUSER              : OUT std_logic_vector(C_AXI_RUSER_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_RVALID             : OUT std_logic := '0';
          S_AXI_RREADY             : IN  std_logic := '0';

          -- AXI Full/Lite Master Read Channel (Read side)
          M_AXI_ARID               : OUT std_logic_vector(C_AXI_ID_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_ARADDR             : OUT std_logic_vector(C_AXI_ADDR_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_ARLEN              : OUT std_logic_vector(8 - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_ARSIZE             : OUT std_logic_vector(3 - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_ARBURST            : OUT std_logic_vector(2 - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_ARLOCK             : OUT std_logic_vector(2 - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_ARCACHE            : OUT std_logic_vector(4 - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_ARPROT             : OUT std_logic_vector(3 - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_ARQOS              : OUT std_logic_vector(4 - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_ARREGION           : OUT std_logic_vector(4 - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_ARUSER             : OUT std_logic_vector(C_AXI_ARUSER_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_ARVALID            : OUT std_logic := '0';
          M_AXI_ARREADY            : IN  std_logic := '0';
          M_AXI_RID                : IN  std_logic_vector(C_AXI_ID_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_RDATA              : IN  std_logic_vector(C_AXI_DATA_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_RRESP              : IN  std_logic_vector(2 - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_RLAST              : IN  std_logic := '0';
          M_AXI_RUSER              : IN  std_logic_vector(C_AXI_RUSER_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_RVALID             : IN  std_logic := '0';
          M_AXI_RREADY             : OUT std_logic := '0';

          -- AXI Streaming Slave Signals (Write side)
          S_AXIS_TVALID            : IN  std_logic := '0';
          S_AXIS_TREADY            : OUT std_logic := '0';
          S_AXIS_TDATA             : IN  std_logic_vector(C_AXIS_TDATA_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXIS_TSTRB             : IN  std_logic_vector(C_AXIS_TSTRB_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXIS_TKEEP             : IN  std_logic_vector(C_AXIS_TKEEP_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXIS_TLAST             : IN  std_logic := '0';
          S_AXIS_TID               : IN  std_logic_vector(C_AXIS_TID_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXIS_TDEST             : IN  std_logic_vector(C_AXIS_TDEST_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          S_AXIS_TUSER             : IN  std_logic_vector(C_AXIS_TUSER_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');

          -- AXI Streaming Master Signals (Read side)
          M_AXIS_TVALID            : OUT std_logic := '0';
          M_AXIS_TREADY            : IN  std_logic := '0';
          M_AXIS_TDATA             : OUT std_logic_vector(C_AXIS_TDATA_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXIS_TSTRB             : OUT std_logic_vector(C_AXIS_TSTRB_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXIS_TKEEP             : OUT std_logic_vector(C_AXIS_TKEEP_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXIS_TLAST             : OUT std_logic := '0';
          M_AXIS_TID               : OUT std_logic_vector(C_AXIS_TID_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXIS_TDEST             : OUT std_logic_vector(C_AXIS_TDEST_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');
          M_AXIS_TUSER             : OUT std_logic_vector(C_AXIS_TUSER_WIDTH - 1 DOWNTO 0) := (OTHERS => '0');

          -- AXI Full/Lite Write Address Channel Signals
          AXI_AW_INJECTSBITERR     : IN  std_logic := '0';
          AXI_AW_INJECTDBITERR     : IN  std_logic := '0';
          AXI_AW_PROG_FULL_THRESH  : IN  std_logic_vector(C_WR_PNTR_WIDTH_WACH - 1 DOWNTO 0) := (OTHERS => '0');
          AXI_AW_PROG_EMPTY_THRESH : IN  std_logic_vector(C_WR_PNTR_WIDTH_WACH - 1 DOWNTO 0) := (OTHERS => '0');
          AXI_AW_DATA_COUNT        : OUT std_logic_vector(C_WR_PNTR_WIDTH_WACH DOWNTO 0) := (OTHERS => '0');
          AXI_AW_WR_DATA_COUNT     : OUT std_logic_vector(C_WR_PNTR_WIDTH_WACH DOWNTO 0) := (OTHERS => '0');
          AXI_AW_RD_DATA_COUNT     : OUT std_logic_vector(C_WR_PNTR_WIDTH_WACH DOWNTO 0) := (OTHERS => '0');
          AXI_AW_SBITERR           : OUT std_logic := '0';
          AXI_AW_DBITERR           : OUT std_logic := '0';
          AXI_AW_OVERFLOW          : OUT std_logic := '0';
          AXI_AW_UNDERFLOW         : OUT std_logic := '0';
          AXI_AW_PROG_FULL         : OUT STD_LOGIC := '0';
          AXI_AW_PROG_EMPTY        : OUT STD_LOGIC := '1';

          -- AXI Full/Lite Write Data Channel Signals
          AXI_W_INJECTSBITERR      : IN  std_logic := '0';
          AXI_W_INJECTDBITERR      : IN  std_logic := '0';
          AXI_W_PROG_FULL_THRESH   : IN  std_logic_vector(C_WR_PNTR_WIDTH_WDCH - 1 DOWNTO 0) := (OTHERS => '0');
          AXI_W_PROG_EMPTY_THRESH  : IN  std_logic_vector(C_WR_PNTR_WIDTH_WDCH - 1 DOWNTO 0) := (OTHERS => '0');
          AXI_W_DATA_COUNT         : OUT std_logic_vector(C_WR_PNTR_WIDTH_WDCH DOWNTO 0) := (OTHERS => '0');
          AXI_W_WR_DATA_COUNT      : OUT std_logic_vector(C_WR_PNTR_WIDTH_WDCH DOWNTO 0) := (OTHERS => '0');
          AXI_W_RD_DATA_COUNT      : OUT std_logic_vector(C_WR_PNTR_WIDTH_WDCH DOWNTO 0) := (OTHERS => '0');
          AXI_W_SBITERR            : OUT std_logic := '0';
          AXI_W_DBITERR            : OUT std_logic := '0';
          AXI_W_OVERFLOW           : OUT std_logic := '0';
          AXI_W_UNDERFLOW          : OUT std_logic := '0';
          AXI_W_PROG_FULL          : OUT STD_LOGIC := '0';
          AXI_W_PROG_EMPTY         : OUT STD_LOGIC := '1';

          -- AXI Full/Lite Write Response Channel Signals
          AXI_B_INJECTSBITERR      : IN  std_logic := '0';
          AXI_B_INJECTDBITERR      : IN  std_logic := '0';
          AXI_B_PROG_FULL_THRESH   : IN  std_logic_vector(C_WR_PNTR_WIDTH_WRCH - 1 DOWNTO 0) := (OTHERS => '0');
          AXI_B_PROG_EMPTY_THRESH  : IN  std_logic_vector(C_WR_PNTR_WIDTH_WRCH - 1 DOWNTO 0) := (OTHERS => '0');
          AXI_B_DATA_COUNT         : OUT std_logic_vector(C_WR_PNTR_WIDTH_WRCH DOWNTO 0) := (OTHERS => '0');
          AXI_B_WR_DATA_COUNT      : OUT std_logic_vector(C_WR_PNTR_WIDTH_WRCH DOWNTO 0) := (OTHERS => '0');
          AXI_B_RD_DATA_COUNT      : OUT std_logic_vector(C_WR_PNTR_WIDTH_WRCH DOWNTO 0) := (OTHERS => '0');
          AXI_B_SBITERR            : OUT std_logic := '0';
          AXI_B_DBITERR            : OUT std_logic := '0';
          AXI_B_OVERFLOW           : OUT std_logic := '0';
          AXI_B_UNDERFLOW          : OUT std_logic := '0';
          AXI_B_PROG_FULL          : OUT STD_LOGIC := '0';
          AXI_B_PROG_EMPTY         : OUT STD_LOGIC := '1';

          -- AXI Full/Lite Read Address Channel Signals
          AXI_AR_INJECTSBITERR     : IN  std_logic := '0';
          AXI_AR_INJECTDBITERR     : IN  std_logic := '0';
          AXI_AR_PROG_FULL_THRESH  : IN  std_logic_vector(C_WR_PNTR_WIDTH_RACH - 1 DOWNTO 0) := (OTHERS => '0');
          AXI_AR_PROG_EMPTY_THRESH : IN  std_logic_vector(C_WR_PNTR_WIDTH_RACH - 1 DOWNTO 0) := (OTHERS => '0');
          AXI_AR_DATA_COUNT        : OUT std_logic_vector(C_WR_PNTR_WIDTH_RACH DOWNTO 0) := (OTHERS => '0');
          AXI_AR_WR_DATA_COUNT     : OUT std_logic_vector(C_WR_PNTR_WIDTH_RACH DOWNTO 0) := (OTHERS => '0');
          AXI_AR_RD_DATA_COUNT     : OUT std_logic_vector(C_WR_PNTR_WIDTH_RACH DOWNTO 0) := (OTHERS => '0');
          AXI_AR_SBITERR           : OUT std_logic := '0';
          AXI_AR_DBITERR           : OUT std_logic := '0';
          AXI_AR_OVERFLOW          : OUT std_logic := '0';
          AXI_AR_UNDERFLOW         : OUT std_logic := '0';
          AXI_AR_PROG_FULL         : OUT STD_LOGIC := '0';
          AXI_AR_PROG_EMPTY        : OUT STD_LOGIC := '1';

          -- AXI Full/Lite Read Data Channel Signals
          AXI_R_INJECTSBITERR      : IN  std_logic := '0';
          AXI_R_INJECTDBITERR      : IN  std_logic := '0';
          AXI_R_PROG_FULL_THRESH   : IN  std_logic_vector(C_WR_PNTR_WIDTH_RDCH - 1 DOWNTO 0) := (OTHERS => '0');
          AXI_R_PROG_EMPTY_THRESH  : IN  std_logic_vector(C_WR_PNTR_WIDTH_RDCH - 1 DOWNTO 0) := (OTHERS => '0');
          AXI_R_DATA_COUNT         : OUT std_logic_vector(C_WR_PNTR_WIDTH_RDCH DOWNTO 0) := (OTHERS => '0');
          AXI_R_WR_DATA_COUNT      : OUT std_logic_vector(C_WR_PNTR_WIDTH_RDCH DOWNTO 0) := (OTHERS => '0');
          AXI_R_RD_DATA_COUNT      : OUT std_logic_vector(C_WR_PNTR_WIDTH_RDCH DOWNTO 0) := (OTHERS => '0');
          AXI_R_SBITERR            : OUT std_logic := '0';
          AXI_R_DBITERR            : OUT std_logic := '0';
          AXI_R_OVERFLOW           : OUT std_logic := '0';
          AXI_R_UNDERFLOW          : OUT std_logic := '0';
          AXI_R_PROG_FULL          : OUT STD_LOGIC := '0';
          AXI_R_PROG_EMPTY         : OUT STD_LOGIC := '1';

          -- AXI Streaming FIFO Related Signals
          AXIS_INJECTSBITERR       : IN  std_logic := '0';
          AXIS_INJECTDBITERR       : IN  std_logic := '0';
          AXIS_PROG_FULL_THRESH    : IN  std_logic_vector(C_WR_PNTR_WIDTH_AXIS - 1 DOWNTO 0) := (OTHERS => '0');
          AXIS_PROG_EMPTY_THRESH   : IN  std_logic_vector(C_WR_PNTR_WIDTH_AXIS - 1 DOWNTO 0) := (OTHERS => '0');
          AXIS_DATA_COUNT          : OUT std_logic_vector(C_WR_PNTR_WIDTH_AXIS DOWNTO 0) := (OTHERS => '0');
          AXIS_WR_DATA_COUNT       : OUT std_logic_vector(C_WR_PNTR_WIDTH_AXIS DOWNTO 0) := (OTHERS => '0');
          AXIS_RD_DATA_COUNT       : OUT std_logic_vector(C_WR_PNTR_WIDTH_AXIS DOWNTO 0) := (OTHERS => '0');
          AXIS_SBITERR             : OUT std_logic := '0';
          AXIS_DBITERR             : OUT std_logic := '0';
          AXIS_OVERFLOW            : OUT std_logic := '0';
          AXIS_UNDERFLOW           : OUT std_logic := '0';
          AXIS_PROG_FULL           : OUT STD_LOGIC := '0';
          AXIS_PROG_EMPTY          : OUT STD_LOGIC := '1'
        );
      END COMPONENT;

      -- The following attributes tells XST that the fifo_generator_v8_3 is a black box
      -- which should be generated using the command given by the value
      -- of this attribute

      attribute box_type : string;
      attribute box_type of fifo_generator_v9_3 : component is
      "black_box";
      attribute GENERATOR_DEFAULT : string;
      attribute GENERATOR_DEFAULT of fifo_generator_v9_3 : component is
      "generatecore com.xilinx.ip.fifo_generator_v9_3.fifo_generator_v9_3 -a map_qvirtex_to=virtex map_qrvirtex_to=virtex map_virtexe_to=virtex map_qvirtex2_to=virtex2 map_qrvirtex2_to=virtex2 map_spartan2_to=virtex map_spartan2e_to=virtex map_virtex5_to=virtex4 map_spartan3a_to=spartan3e spartan3an_to=spartan3e spartan3adsp_to=spartan3e ";

    -- End FIFO Generator Component ---------------------------------------

    begin

      -------------------------------------------------------------------------------
      -- Instantiate the generalized FIFO Generator instance
      --
      -- NOTE:
      -- DO NOT CHANGE TO DIRECT ENTITY INSTANTIATION!!!
      -- This is a Coregen FIFO Generator Call module for
      -- legacy BRAM implementations of an Async FIFo.
      --
      -------------------------------------------------------------------------------
      I_ASYNC_FIFO_BRAM : fifo_generator_v9_3
        generic map
        (
          C_COMMON_CLOCK                      => 0,
          C_COUNT_TYPE                        => 0,
          C_DATA_COUNT_WIDTH                  => C_WR_COUNT_WIDTH,
          C_DEFAULT_VALUE                     => "BlankString",
          C_DIN_WIDTH                         => C_DATA_WIDTH,
          C_DOUT_RST_VAL                      => "0",
          C_DOUT_WIDTH                        => C_DATA_WIDTH,
          C_ENABLE_RLOCS                      => 0,
          C_FAMILY                            => FAMILY_TO_USE,
          C_FULL_FLAGS_RST_VAL                => 0,
          C_HAS_ALMOST_EMPTY                  => C_HAS_ALMOST_EMPTY,
          C_HAS_ALMOST_FULL                   => C_HAS_ALMOST_FULL,
          C_HAS_BACKUP                        => 0,
          C_HAS_DATA_COUNT                    => 0,
          C_HAS_INT_CLK                       => 0,
          C_HAS_MEMINIT_FILE                  => 0,
          C_HAS_OVERFLOW                      => C_HAS_WR_ERR,
          C_HAS_RD_DATA_COUNT                 => C_HAS_RD_COUNT,
          C_HAS_RD_RST                        => 0,
          C_HAS_RST                           => 1,
          C_HAS_SRST                          => 0,
          C_HAS_UNDERFLOW                     => C_HAS_RD_ERR,
          C_HAS_VALID                         => C_HAS_RD_ACK,
          C_HAS_WR_ACK                        => C_HAS_WR_ACK,
          C_HAS_WR_DATA_COUNT                 => C_HAS_WR_COUNT,
          C_HAS_WR_RST                        => 0,
          C_IMPLEMENTATION_TYPE               => FG_IMP_TYPE,
          C_INIT_WR_PNTR_VAL                  => 0,
          C_MEMORY_TYPE                       => FG_MEM_TYPE,
          C_MIF_FILE_NAME                     => "BlankString",
          C_OPTIMIZATION_MODE                 => 0,
          C_OVERFLOW_LOW                      => C_WR_ERR_LOW,
          C_PRELOAD_LATENCY                   => C_PRELOAD_LATENCY,  ----1, Fixed CR#658129
          C_PRELOAD_REGS                      => C_PRELOAD_REGS,     ----0, Fixed CR#658129
          C_PRIM_FIFO_TYPE                    => "512x36",           -- only used for V5 Hard FIFO
          C_PROG_EMPTY_THRESH_ASSERT_VAL      => PROG_EMPTY_THRESH_ASSERT_VAL,
          C_PROG_EMPTY_THRESH_NEGATE_VAL      => PROG_EMPTY_THRESH_NEGATE_VAL,
          C_PROG_EMPTY_TYPE                   => C_HAS_PROG_EMPTY,
          C_PROG_FULL_THRESH_ASSERT_VAL       => PROG_FULL_THRESH_ASSERT_VAL,
          C_PROG_FULL_THRESH_NEGATE_VAL       => PROG_FULL_THRESH_NEGATE_VAL,
          C_PROG_FULL_TYPE                    => C_HAS_PROG_FULL,
          C_RD_DATA_COUNT_WIDTH               => C_RD_COUNT_WIDTH,
          C_RD_DEPTH                          => C_FIFO_DEPTH,
          C_RD_FREQ                           => 1,
          C_RD_PNTR_WIDTH                     => RD_PNTR_WIDTH,
          C_UNDERFLOW_LOW                     => C_RD_ERR_LOW,
          C_USE_DOUT_RST                      => 1,
          C_USE_ECC                           => 0,
          C_USE_EMBEDDED_REG                  => C_USE_EMBEDDED_REG, ----0, Fixed CR#658129
          C_USE_FIFO16_FLAGS                  => 0,
          C_USE_FWFT_DATA_COUNT               => 0,
          C_VALID_LOW                         => 0,
          C_WR_ACK_LOW                        => C_WR_ACK_LOW,
          C_WR_DATA_COUNT_WIDTH               => C_WR_COUNT_WIDTH,
          C_WR_DEPTH                          => C_FIFO_DEPTH,
          C_WR_FREQ                           => 1,
          C_WR_PNTR_WIDTH                     => WR_PNTR_WIDTH,
          C_WR_RESPONSE_LATENCY               => 1,
          C_MSGON_VAL                         => 1,
          C_ENABLE_RST_SYNC                   => 1,
          C_ERROR_INJECTION_TYPE              => 0,

          -- AXI Interface related parameters start here
          C_INTERFACE_TYPE                    => 0,  -- : integer := 0; -- 0: Native Interface; 1: AXI Interface
          C_AXI_TYPE                          => 0,  -- : integer := 0; -- 0: AXI Stream; 1: AXI Full; 2: AXI Lite
          C_HAS_AXI_WR_CHANNEL                => 0,  -- : integer := 0;
          C_HAS_AXI_RD_CHANNEL                => 0,  -- : integer := 0;
          C_HAS_SLAVE_CE                      => 0,  -- : integer := 0;
          C_HAS_MASTER_CE                     => 0,  -- : integer := 0;
          C_ADD_NGC_CONSTRAINT                => 0,  -- : integer := 0;
          C_USE_COMMON_OVERFLOW               => 0,  -- : integer := 0;
          C_USE_COMMON_UNDERFLOW              => 0,  -- : integer := 0;
          C_USE_DEFAULT_SETTINGS              => 0,  -- : integer := 0;

          -- AXI Full/Lite
          C_AXI_ID_WIDTH                      => 4,  -- : integer := 0;
          C_AXI_ADDR_WIDTH                    => 32, -- : integer := 0;
          C_AXI_DATA_WIDTH                    => 64, -- : integer := 0;
          C_HAS_AXI_AWUSER                    => 0,  -- : integer := 0;
          C_HAS_AXI_WUSER                     => 0,  -- : integer := 0;
          C_HAS_AXI_BUSER                     => 0,  -- : integer := 0;
          C_HAS_AXI_ARUSER                    => 0,  -- : integer := 0;
          C_HAS_AXI_RUSER                     => 0,  -- : integer := 0;
          C_AXI_ARUSER_WIDTH                  => 1,  -- : integer := 0;
          C_AXI_AWUSER_WIDTH                  => 1,  -- : integer := 0;
          C_AXI_WUSER_WIDTH                   => 1,  -- : integer := 0;
          C_AXI_BUSER_WIDTH                   => 1,  -- : integer := 0;
          C_AXI_RUSER_WIDTH                   => 1,  -- : integer := 0;

          -- AXI Streaming
          C_HAS_AXIS_TDATA                    => 0,  -- : integer := 0;
          C_HAS_AXIS_TID                      => 0,  -- : integer := 0;
          C_HAS_AXIS_TDEST                    => 0,  -- : integer := 0;
          C_HAS_AXIS_TUSER                    => 0,  -- : integer := 0;
          C_HAS_AXIS_TREADY                   => 1,  -- : integer := 0;
          C_HAS_AXIS_TLAST                    => 0,  -- : integer := 0;
          C_HAS_AXIS_TSTRB                    => 0,  -- : integer := 0;
          C_HAS_AXIS_TKEEP                    => 0,  -- : integer := 0;
          C_AXIS_TDATA_WIDTH                  => 64, -- : integer := 1;
          C_AXIS_TID_WIDTH                    => 8,  -- : integer := 1;
          C_AXIS_TDEST_WIDTH                  => 4,  -- : integer := 1;
          C_AXIS_TUSER_WIDTH                  => 4,  -- : integer := 1;
          C_AXIS_TSTRB_WIDTH                  => 4,  -- : integer := 1;
          C_AXIS_TKEEP_WIDTH                  => 4,  -- : integer := 1;

          -- AXI Channel Type
          -- WACH --> Write Address Channel
          -- WDCH --> Write Data Channel
          -- WRCH --> Write Response Channel
          -- RACH --> Read Address Channel
          -- RDCH --> Read Data Channel
          -- AXIS --> AXI Streaming
          C_WACH_TYPE                         => 0, -- : integer := 0; -- 0 = FIFO; 1 = Register Slice; 2 = Pass Through Logic
          C_WDCH_TYPE                         => 0, -- : integer := 0; -- 0 = FIFO; 1 = Register Slice; 2 = Pass Through Logie
          C_WRCH_TYPE                         => 0, -- : integer := 0; -- 0 = FIFO; 1 = Register Slice; 2 = Pass Through Logie
          C_RACH_TYPE                         => 0, -- : integer := 0; -- 0 = FIFO; 1 = Register Slice; 2 = Pass Through Logie
          C_RDCH_TYPE                         => 0, -- : integer := 0; -- 0 = FIFO; 1 = Register Slice; 2 = Pass Through Logie
          C_AXIS_TYPE                         => 0, -- : integer := 0; -- 0 = FIFO; 1 = Register Slice; 2 = Pass Through Logie

          -- AXI Implementation Type
          -- 1 = Common Clock Block RAM FIFO
          -- 2 = Common Clock Distributed RAM FIFO
          -- 11 = Independent Clock Block RAM FIFO
          -- 12 = Independent Clock Distributed RAM FIFO
          C_IMPLEMENTATION_TYPE_WACH          => 1,    -- : integer := 0;
          C_IMPLEMENTATION_TYPE_WDCH          => 1,    -- : integer := 0;
          C_IMPLEMENTATION_TYPE_WRCH          => 1,    -- : integer := 0;
          C_IMPLEMENTATION_TYPE_RACH          => 1,    -- : integer := 0;
          C_IMPLEMENTATION_TYPE_RDCH          => 1,    -- : integer := 0;
          C_IMPLEMENTATION_TYPE_AXIS          => 1,    -- : integer := 0;

          -- AXI FIFO Type
          -- 0 = Data FIFO
          -- 1 = Packet FIFO
          -- 2 = Low Latency Data FIFO
          C_APPLICATION_TYPE_WACH             => 0,    -- : integer := 0;
          C_APPLICATION_TYPE_WDCH             => 0,    -- : integer := 0;
          C_APPLICATION_TYPE_WRCH             => 0,    -- : integer := 0;
          C_APPLICATION_TYPE_RACH             => 0,    -- : integer := 0;
          C_APPLICATION_TYPE_RDCH             => 0,    -- : integer := 0;
          C_APPLICATION_TYPE_AXIS             => 0,    -- : integer := 0;

          -- Enable ECC
          -- 0 = ECC disabled
          -- 1 = ECC enabled
          C_USE_ECC_WACH                      => 0,    -- : integer := 0;
          C_USE_ECC_WDCH                      => 0,    -- : integer := 0;
          C_USE_ECC_WRCH                      => 0,    -- : integer := 0;
          C_USE_ECC_RACH                      => 0,    -- : integer := 0;
          C_USE_ECC_RDCH                      => 0,    -- : integer := 0;
          C_USE_ECC_AXIS                      => 0,    -- : integer := 0;

          -- ECC Error Injection Type
          -- 0 = No Error Injection
          -- 1 = Single Bit Error Injection
          -- 2 = Double Bit Error Injection
          -- 3 = Single Bit and Double Bit Error Injection
          C_ERROR_INJECTION_TYPE_WACH         => 0,    -- : integer := 0;
          C_ERROR_INJECTION_TYPE_WDCH         => 0,    -- : integer := 0;
          C_ERROR_INJECTION_TYPE_WRCH         => 0,    -- : integer := 0;
          C_ERROR_INJECTION_TYPE_RACH         => 0,    -- : integer := 0;
          C_ERROR_INJECTION_TYPE_RDCH         => 0,    -- : integer := 0;
          C_ERROR_INJECTION_TYPE_AXIS         => 0,    -- : integer := 0;

          -- Input Data Width
          -- Accumulation of all AXI input signal's width
          C_DIN_WIDTH_WACH                    => 32,   -- : integer := 1;
          C_DIN_WIDTH_WDCH                    => 64,   -- : integer := 1;
          C_DIN_WIDTH_WRCH                    => 2,    -- : integer := 1;
          C_DIN_WIDTH_RACH                    => 32,   -- : integer := 1;
          C_DIN_WIDTH_RDCH                    => 64,   -- : integer := 1;
          C_DIN_WIDTH_AXIS                    => 1,    -- : integer := 1;

          C_WR_DEPTH_WACH                     => 16,   -- : integer := 16;
          C_WR_DEPTH_WDCH                     => 1024, -- : integer := 16;
          C_WR_DEPTH_WRCH                     => 16,   -- : integer := 16;
          C_WR_DEPTH_RACH                     => 16,   -- : integer := 16;
          C_WR_DEPTH_RDCH                     => 1024, -- : integer := 16;
          C_WR_DEPTH_AXIS                     => 1024, -- : integer := 16;

          C_WR_PNTR_WIDTH_WACH                => 4,    -- : integer := 4;
          C_WR_PNTR_WIDTH_WDCH                => 10,   -- : integer := 4;
          C_WR_PNTR_WIDTH_WRCH                => 4,    -- : integer := 4;
          C_WR_PNTR_WIDTH_RACH                => 4,    -- : integer := 4;
          C_WR_PNTR_WIDTH_RDCH                => 10,   -- : integer := 4;
          C_WR_PNTR_WIDTH_AXIS                => 10,   -- : integer := 4;

          C_HAS_DATA_COUNTS_WACH              => 0,    -- : integer := 0;
          C_HAS_DATA_COUNTS_WDCH              => 0,    -- : integer := 0;
          C_HAS_DATA_COUNTS_WRCH              => 0,    -- : integer := 0;
          C_HAS_DATA_COUNTS_RACH              => 0,    -- : integer := 0;
          C_HAS_DATA_COUNTS_RDCH              => 0,    -- : integer := 0;
          C_HAS_DATA_COUNTS_AXIS              => 0,    -- : integer := 0;

          C_HAS_PROG_FLAGS_WACH               => 0,    -- : integer := 0;
          C_HAS_PROG_FLAGS_WDCH               => 0,    -- : integer := 0;
          C_HAS_PROG_FLAGS_WRCH               => 0,    -- : integer := 0;
          C_HAS_PROG_FLAGS_RACH               => 0,    -- : integer := 0;
          C_HAS_PROG_FLAGS_RDCH               => 0,    -- : integer := 0;
          C_HAS_PROG_FLAGS_AXIS               => 0,    -- : integer := 0;

          C_PROG_FULL_TYPE_WACH               => 5,    -- : integer := 0;
          C_PROG_FULL_TYPE_WDCH               => 5,    -- : integer := 0;
          C_PROG_FULL_TYPE_WRCH               => 5,    -- : integer := 0;
          C_PROG_FULL_TYPE_RACH               => 5,    -- : integer := 0;
          C_PROG_FULL_TYPE_RDCH               => 5,    -- : integer := 0;
          C_PROG_FULL_TYPE_AXIS               => 5,    -- : integer := 0;
          C_PROG_FULL_THRESH_ASSERT_VAL_WACH  => 1023, -- : integer := 0;
          C_PROG_FULL_THRESH_ASSERT_VAL_WDCH  => 1023, -- : integer := 0;
          C_PROG_FULL_THRESH_ASSERT_VAL_WRCH  => 1023, -- : integer := 0;
          C_PROG_FULL_THRESH_ASSERT_VAL_RACH  => 1023, -- : integer := 0;
          C_PROG_FULL_THRESH_ASSERT_VAL_RDCH  => 1023, -- : integer := 0;
          C_PROG_FULL_THRESH_ASSERT_VAL_AXIS  => 1023, -- : integer := 0;

          C_PROG_EMPTY_TYPE_WACH              => 5,    -- : integer := 0;
          C_PROG_EMPTY_TYPE_WDCH              => 5,    -- : integer := 0;
          C_PROG_EMPTY_TYPE_WRCH              => 5,    -- : integer := 0;
          C_PROG_EMPTY_TYPE_RACH              => 5,    -- : integer := 0;
          C_PROG_EMPTY_TYPE_RDCH              => 5,    -- : integer := 0;
          C_PROG_EMPTY_TYPE_AXIS              => 5,    -- : integer := 0;
          C_PROG_EMPTY_THRESH_ASSERT_VAL_WACH => 1022, -- : integer := 0;
          C_PROG_EMPTY_THRESH_ASSERT_VAL_WDCH => 1022, -- : integer := 0;
          C_PROG_EMPTY_THRESH_ASSERT_VAL_WRCH => 1022, -- : integer := 0;
          C_PROG_EMPTY_THRESH_ASSERT_VAL_RACH => 1022, -- : integer := 0;
          C_PROG_EMPTY_THRESH_ASSERT_VAL_RDCH => 1022, -- : integer := 0;
          C_PROG_EMPTY_THRESH_ASSERT_VAL_AXIS => 1022, -- : integer := 0;

          C_REG_SLICE_MODE_WACH               => 0,    -- : integer := 0;
          C_REG_SLICE_MODE_WDCH               => 0,    -- : integer := 0;
          C_REG_SLICE_MODE_WRCH               => 0,    -- : integer := 0;
          C_REG_SLICE_MODE_RACH               => 0,    -- : integer := 0;
          C_REG_SLICE_MODE_RDCH               => 0,    -- : integer := 0;
          C_REG_SLICE_MODE_AXIS               => 0     -- : integer := 0
        )
        port map
        (
          BACKUP                   => '0',                    -- : IN  std_logic := '0';
          BACKUP_MARKER            => '0',                    -- : IN  std_logic := '0';
          CLK                      => '0',                    -- : IN  std_logic := '0';
          RST                      => RESET_ASYNC_I,          -- : IN  std_logic := '0';
          SRST                     => '0',                    -- : IN  std_logic := '0';
          WR_CLK                   => WR_CLK_I,               -- : IN  std_logic := '0';
          WR_RST                   => RESET_ASYNC_I,          -- : IN  std_logic := '0';
          RD_CLK                   => RD_CLK_I,               -- : IN  std_logic := '0';
          RD_RST                   => RESET_ASYNC_I,          -- : IN  std_logic := '0';
          DIN                      => WR_DATA_I,              -- : IN  std_logic_vector(C_DIN_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          WR_EN                    => WR_EN_I,                -- : IN  std_logic := '0';
          RD_EN                    => RD_EN_I,                -- : IN  std_logic := '0';
          PROG_EMPTY_THRESH        => PROG_RDTHRESH_ZEROS,    -- : IN  std_logic_vector(C_RD_PNTR_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          PROG_EMPTY_THRESH_ASSERT => PROG_RDTHRESH_ZEROS,    -- : IN  std_logic_vector(C_RD_PNTR_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          PROG_EMPTY_THRESH_NEGATE => PROG_RDTHRESH_ZEROS,    -- : IN  std_logic_vector(C_RD_PNTR_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          PROG_FULL_THRESH         => PROG_WRTHRESH_ZEROS,    -- : IN  std_logic_vector(C_WR_PNTR_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          PROG_FULL_THRESH_ASSERT  => PROG_WRTHRESH_ZEROS,    -- : IN  std_logic_vector(C_WR_PNTR_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          PROG_FULL_THRESH_NEGATE  => PROG_WRTHRESH_ZEROS,    -- : IN  std_logic_vector(C_WR_PNTR_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          INT_CLK                  => '0',                    -- : IN  std_logic := '0';
          INJECTDBITERR            => '0',  -- new FG 5.1/5.2 -- : IN  std_logic := '0';
          INJECTSBITERR            => '0',  -- new FG 5.1/5.2 -- : IN  std_logic := '0';

          DOUT                     => RD_DATA_O,              -- : OUT std_logic_vector(C_DOUT_WIDTH-1 DOWNTO 0);
          FULL                     => sig_full,               -- : OUT std_logic;
          ALMOST_FULL              => WR_ALMOST_FULL_O,       -- : OUT std_logic;
          WR_ACK                   => WR_ACK_O,               -- : OUT std_logic;
          OVERFLOW                 => WR_ERR_O,               -- : OUT std_logic;
          EMPTY                    => RD_EMPTY_O,             -- : OUT std_logic;
          ALMOST_EMPTY             => RD_ALMOST_EMPTY_O,      -- : OUT std_logic;
          VALID                    => RD_ACK_O,               -- : OUT std_logic;
          UNDERFLOW                => RD_ERR_O,               -- : OUT std_logic;
          DATA_COUNT               => open,                   -- : OUT std_logic_vector(C_DATA_COUNT_WIDTH-1 DOWNTO 0);
          RD_DATA_COUNT            => sig_full_fifo_rdcnt,    -- : OUT std_logic_vector(C_RD_DATA_COUNT_WIDTH-1 DOWNTO 0);
          WR_DATA_COUNT            => sig_full_fifo_wrcnt,    -- : OUT std_logic_vector(C_WR_DATA_COUNT_WIDTH-1 DOWNTO 0);
          PROG_FULL                => WR_PROG_FULL_O,         -- : OUT std_logic;
          PROG_EMPTY               => RD_PROG_EMPTY_O,        -- : OUT std_logic;
          SBITERR                  => open,                   -- : OUT std_logic;
          DBITERR                  => open,                   -- : OUT std_logic


          -- AXI Global Signal
          M_ACLK                   => '0',                -- : IN  std_logic := '0';
          S_ACLK                   => '0',                -- : IN  std_logic := '0';
          S_ARESETN                => '0',                -- : IN  std_logic := '0';
          M_ACLK_EN                => '0',                -- : IN  std_logic := '0';
          S_ACLK_EN                => '0',                -- : IN  std_logic := '0';

          -- AXI Full/Lite Slave Write Channel (write side)
          S_AXI_AWID               => (others => '0'),    -- : IN  std_logic_vector(C_AXI_ID_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWADDR             => (others => '0'),    -- : IN  std_logic_vector(C_AXI_ADDR_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWLEN              => (others => '0'),    -- : IN  std_logic_vector(8-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWSIZE             => (others => '0'),    -- : IN  std_logic_vector(3-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWBURST            => (others => '0'),    -- : IN  std_logic_vector(2-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWLOCK             => (others => '0'),    -- : IN  std_logic_vector(2-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWCACHE            => (others => '0'),    -- : IN  std_logic_vector(4-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWPROT             => (others => '0'),    -- : IN  std_logic_vector(3-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWQOS              => (others => '0'),    -- : IN  std_logic_vector(4-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWREGION           => (others => '0'),    -- : IN  std_logic_vector(4-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWUSER             => (others => '0'),    -- : IN  std_logic_vector(C_AXI_AWUSER_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_AWVALID            => '0',                -- : IN  std_logic := '0';
          S_AXI_AWREADY            => open,               -- : OUT std_logic;
          S_AXI_WID                => (others => '0'),    -- : IN  std_logic_vector(C_AXI_ID_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_WDATA              => (others => '0'),    -- : IN  std_logic_vector(C_AXI_DATA_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_WSTRB              => (others => '0'),    -- : IN  std_logic_vector(C_AXI_DATA_WIDTH/8-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_WLAST              => '0',                -- : IN  std_logic := '0';
          S_AXI_WUSER              => (others => '0'),    -- : IN  std_logic_vector(C_AXI_WUSER_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_WVALID             => '0',                -- : IN  std_logic := '0';
          S_AXI_WREADY             => open,               -- : OUT std_logic;
          S_AXI_BID                => open,               -- : OUT std_logic_vector(C_AXI_ID_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_BRESP              => open,               -- : OUT std_logic_vector(2-1 DOWNTO 0);
          S_AXI_BUSER              => open,               -- : OUT std_logic_vector(C_AXI_BUSER_WIDTH-1 DOWNTO 0);
          S_AXI_BVALID             => open,               -- : OUT std_logic;
          S_AXI_BREADY             => '0',                -- : IN  std_logic := '0';

          -- AXI Full/Lite Master Write Channel (Read side)
          M_AXI_AWID               => open,               -- : OUT std_logic_vector(C_AXI_ID_WIDTH-1 DOWNTO 0);
          M_AXI_AWADDR             => open,               -- : OUT std_logic_vector(C_AXI_ADDR_WIDTH-1 DOWNTO 0);
          M_AXI_AWLEN              => open,               -- : OUT std_logic_vector(8-1 DOWNTO 0);
          M_AXI_AWSIZE             => open,               -- : OUT std_logic_vector(3-1 DOWNTO 0);
          M_AXI_AWBURST            => open,               -- : OUT std_logic_vector(2-1 DOWNTO 0);
          M_AXI_AWLOCK             => open,               -- : OUT std_logic_vector(2-1 DOWNTO 0);
          M_AXI_AWCACHE            => open,               -- : OUT std_logic_vector(4-1 DOWNTO 0);
          M_AXI_AWPROT             => open,               -- : OUT std_logic_vector(3-1 DOWNTO 0);
          M_AXI_AWQOS              => open,               -- : OUT std_logic_vector(4-1 DOWNTO 0);
          M_AXI_AWREGION           => open,               -- : OUT std_logic_vector(4-1 DOWNTO 0);
          M_AXI_AWUSER             => open,               -- : OUT std_logic_vector(C_AXI_AWUSER_WIDTH-1 DOWNTO 0);
          M_AXI_AWVALID            => open,               -- : OUT std_logic;
          M_AXI_AWREADY            => '0',                -- : IN  std_logic := '0';
          M_AXI_WID                => open,               -- : OUT std_logic_vector(C_AXI_ID_WIDTH-1 DOWNTO 0);
          M_AXI_WDATA              => open,               -- : OUT std_logic_vector(C_AXI_DATA_WIDTH-1 DOWNTO 0);
          M_AXI_WSTRB              => open,               -- : OUT std_logic_vector(C_AXI_DATA_WIDTH/8-1 DOWNTO 0);
          M_AXI_WLAST              => open,               -- : OUT std_logic;
          M_AXI_WUSER              => open,               -- : OUT std_logic_vector(C_AXI_WUSER_WIDTH-1 DOWNTO 0);
          M_AXI_WVALID             => open,               -- : OUT std_logic;
          M_AXI_WREADY             => '0',                -- : IN  std_logic := '0';
          M_AXI_BID                => (others => '0'),    -- : IN  std_logic_vector(C_AXI_ID_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_BRESP              => (others => '0'),    -- : IN  std_logic_vector(2-1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_BUSER              => (others => '0'),    -- : IN  std_logic_vector(C_AXI_BUSER_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_BVALID             => '0',                -- : IN  std_logic := '0';
          M_AXI_BREADY             => open,               -- : OUT std_logic;

          -- AXI Full/Lite Slave Read Channel (Write side)
          S_AXI_ARID               => (others => '0'),    -- : IN  std_logic_vector(C_AXI_ID_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARADDR             => (others => '0'),    -- : IN  std_logic_vector(C_AXI_ADDR_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARLEN              => (others => '0'),    -- : IN  std_logic_vector(8-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARSIZE             => (others => '0'),    -- : IN  std_logic_vector(3-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARBURST            => (others => '0'),    -- : IN  std_logic_vector(2-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARLOCK             => (others => '0'),    -- : IN  std_logic_vector(2-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARCACHE            => (others => '0'),    -- : IN  std_logic_vector(4-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARPROT             => (others => '0'),    -- : IN  std_logic_vector(3-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARQOS              => (others => '0'),    -- : IN  std_logic_vector(4-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARREGION           => (others => '0'),    -- : IN  std_logic_vector(4-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARUSER             => (others => '0'),    -- : IN  std_logic_vector(C_AXI_ARUSER_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          S_AXI_ARVALID            => '0',                -- : IN  std_logic := '0';
          S_AXI_ARREADY            => open,               -- : OUT std_logic;
          S_AXI_RID                => open,               -- : OUT std_logic_vector(C_AXI_ID_WIDTH-1 DOWNTO 0);
          S_AXI_RDATA              => open,               -- : OUT std_logic_vector(C_AXI_DATA_WIDTH-1 DOWNTO 0);
          S_AXI_RRESP              => open,               -- : OUT std_logic_vector(2-1 DOWNTO 0);
          S_AXI_RLAST              => open,               -- : OUT std_logic;
          S_AXI_RUSER              => open,               -- : OUT std_logic_vector(C_AXI_RUSER_WIDTH-1 DOWNTO 0);
          S_AXI_RVALID             => open,               -- : OUT std_logic;
          S_AXI_RREADY             => '0',                -- : IN  std_logic := '0';

          -- AXI Full/Lite Master Read Channel (Read side)
          M_AXI_ARID               => open,               -- : OUT std_logic_vector(C_AXI_ID_WIDTH-1 DOWNTO 0);
          M_AXI_ARADDR             => open,               -- : OUT std_logic_vector(C_AXI_ADDR_WIDTH-1 DOWNTO 0);
          M_AXI_ARLEN              => open,               -- : OUT std_logic_vector(8-1 DOWNTO 0);
          M_AXI_ARSIZE             => open,               -- : OUT std_logic_vector(3-1 DOWNTO 0);
          M_AXI_ARBURST            => open,               -- : OUT std_logic_vector(2-1 DOWNTO 0);
          M_AXI_ARLOCK             => open,               -- : OUT std_logic_vector(2-1 DOWNTO 0);
          M_AXI_ARCACHE            => open,               -- : OUT std_logic_vector(4-1 DOWNTO 0);
          M_AXI_ARPROT             => open,               -- : OUT std_logic_vector(3-1 DOWNTO 0);
          M_AXI_ARQOS              => open,               -- : OUT std_logic_vector(4-1 DOWNTO 0);
          M_AXI_ARREGION           => open,               -- : OUT std_logic_vector(4-1 DOWNTO 0);
          M_AXI_ARUSER             => open,               -- : OUT std_logic_vector(C_AXI_ARUSER_WIDTH-1 DOWNTO 0);
          M_AXI_ARVALID            => open,               -- : OUT std_logic;
          M_AXI_ARREADY            => '0',                -- : IN  std_logic := '0';
          M_AXI_RID                => (others => '0'),    -- : IN  std_logic_vector(C_AXI_ID_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_RDATA              => (others => '0'),    -- : IN  std_logic_vector(C_AXI_DATA_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_RRESP              => (others => '0'),    -- : IN  std_logic_vector(2-1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_RLAST              => '0',                -- : IN  std_logic := '0';
          M_AXI_RUSER              => (others => '0'),    -- : IN  std_logic_vector(C_AXI_RUSER_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          M_AXI_RVALID             => '0',                -- : IN  std_logic := '0';
          M_AXI_RREADY             => open,               -- : OUT std_logic;

          -- AXI Streaming Slave Signals (Write side)
          S_AXIS_TVALID            => '0',                -- : IN  std_logic := '0';
          S_AXIS_TREADY            => open,               -- : OUT std_logic;
          S_AXIS_TDATA             => (others => '0'),    -- : IN  std_logic_vector(C_AXIS_TDATA_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          S_AXIS_TSTRB             => (others => '0'),    -- : IN  std_logic_vector(C_AXIS_TSTRB_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          S_AXIS_TKEEP             => (others => '0'),    -- : IN  std_logic_vector(C_AXIS_TKEEP_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          S_AXIS_TLAST             => '0',                -- : IN  std_logic := '0';
          S_AXIS_TID               => (others => '0'),    -- : IN  std_logic_vector(C_AXIS_TID_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          S_AXIS_TDEST             => (others => '0'),    -- : IN  std_logic_vector(C_AXIS_TDEST_WIDTH-1 DOWNTO 0) := (OTHERS => '0');
          S_AXIS_TUSER             => (others => '0'),    -- : IN  std_logic_vector(C_AXIS_TUSER_WIDTH-1 DOWNTO 0) := (OTHERS => '0');

          -- AXI Streaming Master Signals (Read side)
          M_AXIS_TVALID            => open,               -- : OUT std_logic;
          M_AXIS_TREADY            => '0',                -- : IN  std_logic := '0';
          M_AXIS_TDATA             => open,               -- : OUT std_logic_vector(C_AXIS_TDATA_WIDTH-1 DOWNTO 0);
          M_AXIS_TSTRB             => open,               -- : OUT std_logic_vector(C_AXIS_TSTRB_WIDTH-1 DOWNTO 0);
          M_AXIS_TKEEP             => open,               -- : OUT std_logic_vector(C_AXIS_TKEEP_WIDTH-1 DOWNTO 0);
          M_AXIS_TLAST             => open,               -- : OUT std_logic;
          M_AXIS_TID               => open,               -- : OUT std_logic_vector(C_AXIS_TID_WIDTH-1 DOWNTO 0);
          M_AXIS_TDEST             => open,               -- : OUT std_logic_vector(C_AXIS_TDEST_WIDTH-1 DOWNTO 0);
          M_AXIS_TUSER             => open,               -- : OUT std_logic_vector(C_AXIS_TUSER_WIDTH-1 DOWNTO 0);

          -- AXI Full/Lite Write Address Channel Signals
          AXI_AW_INJECTSBITERR     => '0',                -- : IN  std_logic := '0';
          AXI_AW_INJECTDBITERR     => '0',                -- : IN  std_logic := '0';
          AXI_AW_PROG_FULL_THRESH  => (others => '0'),    -- : IN  std_logic_vector(C_WR_PNTR_WIDTH_WACH-1 DOWNTO 0) := (OTHERS => '0');
          AXI_AW_PROG_EMPTY_THRESH => (others => '0'),    -- : IN  std_logic_vector(C_WR_PNTR_WIDTH_WACH-1 DOWNTO 0) := (OTHERS => '0');
          AXI_AW_DATA_COUNT        => open,               -- : OUT std_logic_vector(C_WR_PNTR_WIDTH_WACH DOWNTO 0);
          AXI_AW_WR_DATA_COUNT     => open,               -- : OUT std_logic_vector(C_WR_PNTR_WIDTH_WACH DOWNTO 0);
          AXI_AW_RD_DATA_COUNT     => open,               -- : OUT std_logic_vector(C_WR_PNTR_WIDTH_WACH DOWNTO 0);
          AXI_AW_SBITERR           => open,               -- : OUT std_logic;
          AXI_AW_DBITERR           => open,               -- : OUT std_logic;
          AXI_AW_OVERFLOW          => open,               -- : OUT std_logic;
          AXI_AW_UNDERFLOW         => open,               -- : OUT std_logic;
          AXI_AW_PROG_FULL         => open,               -- : OUT STD_LOGIC := '0';
          AXI_AW_PROG_EMPTY        => open,               -- : OUT STD_LOGIC := '1';

          -- AXI Full/Lite Write Data Channel Signals
          AXI_W_INJECTSBITERR      => '0',                -- : IN  std_logic := '0';
          AXI_W_INJECTDBITERR      => '0',                -- : IN  std_logic := '0';
          AXI_W_PROG_FULL_THRESH   => (others => '0'),    -- : IN  std_logic_vector(C_WR_PNTR_WIDTH_WDCH-1 DOWNTO 0) := (OTHERS => '0');
          AXI_W_PROG_EMPTY_THRESH  => (others => '0'),    -- : IN  std_logic_vector(C_WR_PNTR_WIDTH_WDCH-1 DOWNTO 0) := (OTHERS => '0');
          AXI_W_DATA_COUNT         => open,               -- : OUT std_logic_vector(C_WR_PNTR_WIDTH_WDCH DOWNTO 0);
          AXI_W_WR_DATA_COUNT      => open,               -- : OUT std_logic_vector(C_WR_PNTR_WIDTH_WDCH DOWNTO 0);
          AXI_W_RD_DATA_COUNT      => open,               -- : OUT std_logic_vector(C_WR_PNTR_WIDTH_WDCH DOWNTO 0);
          AXI_W_SBITERR            => open,               -- : OUT std_logic;
          AXI_W_DBITERR            => open,               -- : OUT std_logic;
          AXI_W_OVERFLOW           => open,               -- : OUT std_logic;
          AXI_W_UNDERFLOW          => open,               -- : OUT std_logic;
          AXI_W_PROG_FULL          => open,               -- : OUT STD_LOGIC := '0';
          AXI_W_PROG_EMPTY         => open,               -- : OUT STD_LOGIC := '1';

          -- AXI Full/Lite Write Response Channel Signals
          AXI_B_INJECTSBITERR      => '0',                -- : IN  std_logic := '0';
          AXI_B_INJECTDBITERR      => '0',                -- : IN  std_logic := '0';
          AXI_B_PROG_FULL_THRESH   => (others => '0'),    -- : IN  std_logic_vector(C_WR_PNTR_WIDTH_WRCH-1 DOWNTO 0) := (OTHERS => '0');
          AXI_B_PROG_EMPTY_THRESH  => (others => '0'),    -- : IN  std_logic_vector(C_WR_PNTR_WIDTH_WRCH-1 DOWNTO 0) := (OTHERS => '0');
          AXI_B_DATA_COUNT         => open,               -- : OUT std_logic_vector(C_WR_PNTR_WIDTH_WRCH DOWNTO 0);
          AXI_B_WR_DATA_COUNT      => open,               -- : OUT std_logic_vector(C_WR_PNTR_WIDTH_WRCH DOWNTO 0);
          AXI_B_RD_DATA_COUNT      => open,               -- : OUT std_logic_vector(C_WR_PNTR_WIDTH_WRCH DOWNTO 0);
          AXI_B_SBITERR            => open,               -- : OUT std_logic;
          AXI_B_DBITERR            => open,               -- : OUT std_logic;
          AXI_B_OVERFLOW           => open,               -- : OUT std_logic;
          AXI_B_UNDERFLOW          => open,               -- : OUT std_logic;
          AXI_B_PROG_FULL          => open,               -- : OUT STD_LOGIC := '0';
          AXI_B_PROG_EMPTY         => open,               -- : OUT STD_LOGIC := '1';

          -- AXI Full/Lite Read Address Channel Signals
          AXI_AR_INJECTSBITERR     => '0',                -- : IN  std_logic := '0';
          AXI_AR_INJECTDBITERR     => '0',                -- : IN  std_logic := '0';
          AXI_AR_PROG_FULL_THRESH  => (others => '0'),    -- : IN  std_logic_vector(C_WR_PNTR_WIDTH_RACH-1 DOWNTO 0) := (OTHERS => '0');
          AXI_AR_PROG_EMPTY_THRESH => (others => '0'),    -- : IN  std_logic_vector(C_WR_PNTR_WIDTH_RACH-1 DOWNTO 0) := (OTHERS => '0');
          AXI_AR_DATA_COUNT        => open,               -- : OUT std_logic_vector(C_WR_PNTR_WIDTH_RACH DOWNTO 0);
          AXI_AR_WR_DATA_COUNT     => open,               -- : OUT std_logic_vector(C_WR_PNTR_WIDTH_RACH DOWNTO 0);
          AXI_AR_RD_DATA_COUNT     => open,               -- : OUT std_logic_vector(C_WR_PNTR_WIDTH_RACH DOWNTO 0);
          AXI_AR_SBITERR           => open,               -- : OUT std_logic;
          AXI_AR_DBITERR           => open,               -- : OUT std_logic;
          AXI_AR_OVERFLOW          => open,               -- : OUT std_logic;
          AXI_AR_UNDERFLOW         => open,               -- : OUT std_logic;
          AXI_AR_PROG_FULL         => open,               -- : OUT STD_LOGIC := '0';
          AXI_AR_PROG_EMPTY        => open,               -- : OUT STD_LOGIC := '1';

          -- AXI Full/Lite Read Data Channel Signals
          AXI_R_INJECTSBITERR      => '0',                -- : IN  std_logic := '0';
          AXI_R_INJECTDBITERR      => '0',                -- : IN  std_logic := '0';
          AXI_R_PROG_FULL_THRESH   => (others => '0'),    -- : IN  std_logic_vector(C_WR_PNTR_WIDTH_RDCH-1 DOWNTO 0) := (OTHERS => '0');
          AXI_R_PROG_EMPTY_THRESH  => (others => '0'),    -- : IN  std_logic_vector(C_WR_PNTR_WIDTH_RDCH-1 DOWNTO 0) := (OTHERS => '0');
          AXI_R_DATA_COUNT         => open,               -- : OUT std_logic_vector(C_WR_PNTR_WIDTH_RDCH DOWNTO 0);
          AXI_R_WR_DATA_COUNT      => open,               -- : OUT std_logic_vector(C_WR_PNTR_WIDTH_RDCH DOWNTO 0);
          AXI_R_RD_DATA_COUNT      => open,               -- : OUT std_logic_vector(C_WR_PNTR_WIDTH_RDCH DOWNTO 0);
          AXI_R_SBITERR            => open,               -- : OUT std_logic;
          AXI_R_DBITERR            => open,               -- : OUT std_logic;
          AXI_R_OVERFLOW           => open,               -- : OUT std_logic;
          AXI_R_UNDERFLOW          => open,               -- : OUT std_logic;
          AXI_R_PROG_FULL          => open,               -- : OUT STD_LOGIC := '0';
          AXI_R_PROG_EMPTY         => open,               -- : OUT STD_LOGIC := '1';

          -- AXI Streaming FIFO Related Signals
          AXIS_INJECTSBITERR       => '0',                -- : IN  std_logic := '0';
          AXIS_INJECTDBITERR       => '0',                -- : IN  std_logic := '0';
          AXIS_PROG_FULL_THRESH    => (others => '0'),    -- : IN  std_logic_vector(C_WR_PNTR_WIDTH_AXIS-1 DOWNTO 0) := (OTHERS => '0');
          AXIS_PROG_EMPTY_THRESH   => (others => '0'),    -- : IN  std_logic_vector(C_WR_PNTR_WIDTH_AXIS-1 DOWNTO 0) := (OTHERS => '0');
          AXIS_DATA_COUNT          => open,               -- : OUT std_logic_vector(C_WR_PNTR_WIDTH_AXIS DOWNTO 0);
          AXIS_WR_DATA_COUNT       => open,               -- : OUT std_logic_vector(C_WR_PNTR_WIDTH_AXIS DOWNTO 0);
          AXIS_RD_DATA_COUNT       => open,               -- : OUT std_logic_vector(C_WR_PNTR_WIDTH_AXIS DOWNTO 0);
          AXIS_SBITERR             => open,               -- : OUT std_logic;
          AXIS_DBITERR             => open,               -- : OUT std_logic;
          AXIS_OVERFLOW            => open,               -- : OUT std_logic;
          AXIS_UNDERFLOW           => open,               -- : OUT std_logic
          AXIS_PROG_FULL           => open,               -- : OUT STD_LOGIC := '0';
          AXIS_PROG_EMPTY          => open                -- : OUT STD_LOGIC := '1';
        );

    end generate fifogen_ise_14_7;

  end generate V6_S6_AND_LATER;

end implementation;
