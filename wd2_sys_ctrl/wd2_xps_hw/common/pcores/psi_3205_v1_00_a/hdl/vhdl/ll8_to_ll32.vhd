----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:    10:22:11 02/12/2008
-- Design Name:
-- Module Name:    ll32to8 - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ll8_to_ll32 is
  port
  (
    LL_CLK_I           : IN  STD_LOGIC;
    RESET_I            : IN  STD_LOGIC;

    LL8_I_DATA_I         : in  STD_LOGIC_VECTOR (7 downto 0);
    LL8_I_SOF_N_I        : in  STD_LOGIC;
    LL8_I_EOF_N_I        : in  STD_LOGIC;
    LL8_I_SRC_RDY_N_I    : IN  STD_LOGIC;
    LL8_I_DST_RDY_N_O    : OUT STD_LOGIC;

    LL32_O_DATA_O        : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    LL32_O_SOF_N_O       : OUT STD_LOGIC;
    LL32_O_EOF_N_O       : OUT STD_LOGIC;
    LL32_O_REM_O         : out STD_LOGIC_VECTOR (1 downto 0);
    LL32_O_SRC_RDY_N_O   : OUT STD_LOGIC;
    LL32_O_DST_RDY_N_I   : IN  STD_LOGIC
  );
end ll8_to_ll32;


architecture Behavioral of ll8_to_ll32 is
  signal byte_sel      : std_logic_vector (1 downto 0);
  signal ll32_rem      : std_logic_vector (1 downto 0);
  signal ll32_sof      : std_logic;
  signal ll32_eof      : std_logic;
  signal reg_valid     : std_logic;

  signal byte0         : std_logic_vector (7 downto 0);
  signal byte1         : std_logic_vector (7 downto 0);
  signal byte2         : std_logic_vector (7 downto 0);
  signal byte3         : std_logic_vector (7 downto 0);

  signal byte_en0      : std_logic;
  signal byte_en1      : std_logic;
  signal byte_en2      : std_logic;
  signal byte_en3      : std_logic;

  signal enable_gate   : std_logic;

begin


  process (LL_CLK_I)
  begin
    if rising_edge(LL_CLK_I) then
      if (RESET_I = '1') then
        byte0 <= x"00";
      elsif (byte_en0 = '1') then
        byte0 <= LL8_I_DATA_I;
      end if;
    end if;
  end process;

  process (LL_CLK_I)
  begin
    if rising_edge(LL_CLK_I) then
      if (RESET_I = '1') or (byte_en0 = '1') then
        byte1 <= x"00";
      elsif (byte_en1 = '1') then
        byte1 <= LL8_I_DATA_I;
      end if;
    end if;
  end process;

  process (LL_CLK_I)
  begin
    if rising_edge(LL_CLK_I) then
      if (RESET_I = '1') or (byte_en0 = '1') then
        byte2 <= x"00";
      elsif (byte_en2 = '1') then
        byte2 <= LL8_I_DATA_I;
      end if;
    end if;
  end process;

  process (LL_CLK_I)
  begin
    if rising_edge(LL_CLK_I) then
      if (RESET_I = '1') or (byte_en0 = '1') then
        byte3 <= x"00";
      elsif (byte_en3 = '1') then
        byte3 <= LL8_I_DATA_I;
      end if;
    end if;
  end process;

  process (LL_CLK_I)
  begin
    if rising_edge(LL_CLK_I) then
      if (RESET_I = '1') then
        ll32_rem <= "11";
      elsif (enable_gate = '1') then
        ll32_rem <= byte_sel;
      end if;
    end if;
  end process;

  process (LL_CLK_I)
  begin
    if rising_edge(LL_CLK_I) then
      if (RESET_I = '1') then
        ll32_sof <= '1';
      elsif ((enable_gate = '1') and (LL8_I_SOF_N_I = '0')) then
        ll32_sof <= '0';
      elsif (byte_en0 = '1') then
        ll32_sof <= '1';
      end if;
    end if;
  end process;

  process (LL_CLK_I)
  begin
    if rising_edge(LL_CLK_I) then
      if (RESET_I = '1') then
        ll32_eof <= '1';
      elsif ((enable_gate = '1') and (LL8_I_EOF_N_I = '0')) then
        ll32_eof <= '0';
      elsif (byte_en0 = '1') then
        ll32_eof <= '1';
      end if;
    end if;
  end process;


  process (LL_CLK_I)
  begin
    if rising_edge(LL_CLK_I) then
      if (RESET_I = '1') then
        reg_valid <= '0';
      elsif (byte_en3 = '1') or ((enable_gate = '1') and (LL8_I_EOF_N_I = '0')) then
        reg_valid <= '1';
      elsif (LL32_O_DST_RDY_N_I = '0') then
        reg_valid <= '0';
      end if;
    end if;
  end process;


  byte_sel <= "00" when (LL8_I_SOF_N_I = '0') else ll32_rem + 1;

  enable_gate <= (not LL8_I_SRC_RDY_N_I) and (not reg_valid or not LL32_O_DST_RDY_N_I);

  byte_en0 <= '1' when (enable_gate = '1') and ( byte_sel = "00") else '0';
  byte_en1 <= '1' when (enable_gate = '1') and ( byte_sel = "01") else '0';
  byte_en2 <= '1' when (enable_gate = '1') and ( byte_sel = "10") else '0';
  byte_en3 <= '1' when (enable_gate = '1') and ( byte_sel = "11") else '0';


  LL8_I_DST_RDY_N_O  <= reg_valid and LL32_O_DST_RDY_N_I;  -- == (not (not reg_valid or not LL32_O_DST_RDY_N_I))

  LL32_O_SRC_RDY_N_O <= not reg_valid;

--  LL32_O_DATA_O      <= byte3 & byte2 & byte1 & byte0;
  LL32_O_DATA_O      <= byte0 & byte1 & byte2 & byte3;

  LL32_O_SOF_N_O     <= ll32_sof;
  LL32_O_EOF_N_O     <= ll32_eof;
  LL32_O_REM_O       <= ll32_rem;

end Behavioral;




