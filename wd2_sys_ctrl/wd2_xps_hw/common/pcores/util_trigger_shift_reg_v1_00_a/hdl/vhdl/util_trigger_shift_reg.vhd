---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Simple Shift Register
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid (Author of generation script)
--  Created :  16.09.2014 10:24:30
--
--  Description :  Shift register for trigger information.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

Library UNISIM;
use UNISIM.vcomponents.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

library util_trigger_shift_reg_v1_00_a;
use util_trigger_shift_reg_v1_00_a.shift_register;

entity util_trigger_shift_reg is
  generic (
    CGN_NR_OF_BITS         : integer := 8;
    CGN_OVERSAMPLNG_FACTOR : integer := 1;
    CGN_PARITY             : integer := 1; -- 0=No Parity, 1=Even Parity, 2=Odd Parity
    CGN_PERR_COUNT_WIDTH   : integer := 8; --Parity error counter width [bits]"
    CGN_DIRECTION          : integer := 0 -- Specifies data direction 0="LSB first" or 1="MSB first"
  );
  port (
    SDATA_I          : in  std_logic;
    PDATA_O          : out std_logic_vector(CGN_NR_OF_BITS-1 downto 0);
    VALID_O          : out std_logic;
    VALID_RST_I      : in  std_logic;
    PERR_O           : out std_logic;
    PERR_COUNT_O     : out std_logic_vector(CGN_PERR_COUNT_WIDTH-1 downto 0);
    PERR_COUNT_RST_I : in  std_logic;
    RST_I            : in  std_logic;
    SHIFT_CLK_I      : in  std_logic;
    PCLK_I           : in  std_logic
  );
end util_trigger_shift_reg;

architecture behavioral of util_trigger_shift_reg is

  function get_parity_bits(parity_type : integer) return integer is
  begin
    if parity_type = 0 then
      return 0;
    else
      return 1;
    end if;
  end function get_parity_bits;

  function get_os_count_init(os_factor : integer) return integer is
  begin
    if os_factor < 2 then
      return 0;
    else
      return os_factor-2;
    end if;
  end function get_os_count_init;

  function get_os_counter_width(os_factor : integer) return integer is
  begin
    if os_factor = 1 then
      return 1;
    else
      return log2ceil(os_factor);
    end if;
  end function get_os_counter_width;

  constant C_PARITY_BITS       : integer := get_parity_bits(CGN_PARITY);
  constant C_MAX_PERR_COUNT    : std_logic_vector(CGN_PERR_COUNT_WIDTH-1 downto 0) := (others=>'1');
  constant C_BIT_COUNTER_WIDTH : integer := log2ceil(CGN_NR_OF_BITS+C_PARITY_BITS-1);
  constant C_OS_COUNTER_WIDTH  : integer := get_os_counter_width(CGN_OVERSAMPLNG_FACTOR);
  constant C_OS_COUNTER_INIT   : integer := get_os_count_init(CGN_OVERSAMPLNG_FACTOR);
  constant C_SAMPLING_POINT    : integer := CGN_OVERSAMPLNG_FACTOR/2;

  type type_state is (idle_s, start_s, shift_s);
  signal state          : type_state;
  signal bit_count      : std_logic_vector(C_BIT_COUNTER_WIDTH-1 downto 0) := (others=>'0');
  signal os_count       : std_logic_vector(C_OS_COUNTER_WIDTH-1 downto 0) := (others=>'0');
  signal pdata          : std_logic_vector(CGN_NR_OF_BITS+C_PARITY_BITS-1 downto 0) := (others=>'0');
  signal pdata_out      : std_logic_vector(CGN_NR_OF_BITS-1 downto 0) := (others=>'0');
  signal parity_chk     : std_logic := '0';
  signal perr_out       : std_logic := '0';
  signal perr_count     : std_logic_vector(CGN_PERR_COUNT_WIDTH-1 downto 0) := (others=>'0');
  signal perr_count_rst : std_logic := '0';
  signal rst_parity     : std_logic := '0';
  signal store          : std_logic := '0';
  signal valid_out      : std_logic := '0';
  signal sdata_s1       : std_logic := '0';
  signal sdata_s2       : std_logic := '0';
  signal receive_en     : std_logic := '0';
  signal shift_en       : std_logic := '0';
  signal rst            : std_logic := '0';

begin

  cdc_sync_in_inst: cdc_sync  
  generic map
  (
    CGN_DATA_WIDTH           =>  1,
    CGN_USE_INPUT_REG_A      =>  0,
    CGN_USE_OUTPUT_REG_A     =>  0,
    CGN_USE_GRAY_CONVERSION  =>  0, 
    CGN_NUM_SYNC_REGS_B      =>  1,
    CGN_NUM_OUTPUT_REGS_B    =>  1,
    CGN_GRAY_TO_BIN_STYLE    =>  0
  )
  PORT MAP
  (
    CLK_A_I => PCLK_I,
    CLK_B_I => SHIFT_CLK_I,
    PORT_A_I(0) => RST_I,
    PORT_B_O(0) => rst
  );

  cdc_sync_out_vec_inst: cdc_sync  
  generic map
  (
    CGN_DATA_WIDTH           =>  CGN_NR_OF_BITS,
    CGN_USE_INPUT_REG_A      =>  0,
    CGN_USE_OUTPUT_REG_A     =>  0,
    CGN_USE_GRAY_CONVERSION  =>  0, 
    CGN_NUM_SYNC_REGS_B      =>  1,
    CGN_NUM_OUTPUT_REGS_B    =>  1,
    CGN_GRAY_TO_BIN_STYLE    =>  0
  )
  PORT MAP
  (
    CLK_A_I  => SHIFT_CLK_I,
    CLK_B_I  => PCLK_I,
    PORT_A_I => pdata_out,
    PORT_B_O => PDATA_O
  );



  cdc_sync_out_inst: cdc_sync  
  generic map
  (
    CGN_DATA_WIDTH           =>  1,
    CGN_USE_INPUT_REG_A      =>  0,
    CGN_USE_OUTPUT_REG_A     =>  0,
    CGN_USE_GRAY_CONVERSION  =>  0, 
    CGN_NUM_SYNC_REGS_B      =>  1,
    CGN_NUM_OUTPUT_REGS_B    =>  1,
    CGN_GRAY_TO_BIN_STYLE    =>  0
  )
  PORT MAP
  (
    CLK_A_I     => SHIFT_CLK_I,
    CLK_B_I     => PCLK_I,
    PORT_A_I(0) => valid_out,
    PORT_B_O(0) => VALID_O
  );






  i_ff : process(SHIFT_CLK_I)
  begin
    if rising_edge(SHIFT_CLK_I) then
      sdata_s1 <= SDATA_I;
    end if;
  end process i_ff;
  sync_ff : process(SHIFT_CLK_I)
  begin
    if rising_edge(SHIFT_CLK_I) then
      sdata_s2 <= sdata_s1;
    end if;
  end process sync_ff;

  shift_register : entity util_trigger_shift_reg_v1_00_a.shift_register
  generic map(
    CGN_NR_OF_BITS => CGN_NR_OF_BITS+C_PARITY_BITS,
    CGN_DIRECTION  => CGN_DIRECTION
  )
  port map(
    PDATA_I => (others=>'0'),
    SDATA_I => sdata_s2,
    PDATA_O => pdata,
    SDATA_O => open,
    LOAD_I  => store,
    EN_I    => shift_en,
    CLK_I   => SHIFT_CLK_I
  );

  shift_en <= '1' when  os_count = CONV_STD_LOGIC_VECTOR(C_SAMPLING_POINT, C_OS_COUNTER_WIDTH)
                        and receive_en = '1'
                  else '0';

  process(SHIFT_CLK_I)
  begin
    if rising_edge(SHIFT_CLK_I) then
      if rst = '1' then
        bit_count  <= (others=>'0');
        os_count   <= (others=>'0');
        store      <= '0';
        receive_en <= '0';
        rst_parity <= '0';
      else
        store      <= '0';
        rst_parity <= '0';
        case state is
          when idle_s =>
            rst_parity <= '1';
            if sdata_s2 = '1' then
              os_count   <= CONV_STD_LOGIC_VECTOR(C_OS_COUNTER_INIT, C_OS_COUNTER_WIDTH);
              state <= start_s;
            end if;
          when start_s =>
            bit_count  <= CONV_STD_LOGIC_VECTOR(CGN_NR_OF_BITS+C_PARITY_BITS-1, C_BIT_COUNTER_WIDTH);
            if sdata_s2 = '0' then
              if CGN_OVERSAMPLNG_FACTOR = 1 then
                receive_en <= '1';
              end if;
              state <= shift_s;
            end if;
          when shift_s =>
            if os_count = 0 then
              os_count   <= CONV_STD_LOGIC_VECTOR(CGN_OVERSAMPLNG_FACTOR-1, C_OS_COUNTER_WIDTH);
              receive_en <= '1';
              if bit_count > 0 then
                if receive_en = '1' then
                  bit_count <= bit_count - 1;
                end if;
              else
                receive_en <= '0';
                store      <= '1';
                state <= idle_s;
              end if;
            else
              os_count <= os_count - 1;
            end if;
          when others =>
            state <= idle_s;
        end case;
      end if;
    end if;
  end process;

  process(SHIFT_CLK_I)
  begin
    if RST_I = '1' or VALID_RST_I = '1' then
      valid_out <= '0';
    elsif rising_edge(SHIFT_CLK_I) then
      if store = '1' then
        valid_out <= '1';
      end if;
    end if;
  end process;


  no_parity_otuputs : if CGN_PARITY = 0 generate

    parity_chk <= '0';
    perr_count <= (others=>'0');
    process(SHIFT_CLK_I)
    begin
      if rising_edge(SHIFT_CLK_I) then
        if rst = '1' then
          pdata_out <= (others=>'0');
        elsif store = '1' then
          pdata_out <= pdata(CGN_NR_OF_BITS-1 downto 0); -- no parity bits
        end if;
      end if;
    end process;

  end generate;

  parity_otuputs : if CGN_PARITY /= 0 generate
    signal parity : std_logic := '0';
  begin

    process(SHIFT_CLK_I)
    begin
      if rising_edge(SHIFT_CLK_I) then
        if rst = '1' then
          pdata_out <= (others=>'0');
          perr_out  <= '0';
        elsif store = '1' then
          pdata_out <= pdata(CGN_NR_OF_BITS downto 1); -- pdata(0) is the parity bit
          perr_out  <= parity_chk;
        end if;
        if rst = '1' or perr_count_rst = '1' then
          perr_count <= (others=>'0');
        elsif store = '1' and perr_count < C_MAX_PERR_COUNT then
          perr_count <= perr_count + parity_chk;
        end if;
      end if;
    end process;

    process(SHIFT_CLK_I)
    begin
      if rising_edge(SHIFT_CLK_I) then
        if rst_parity = '1' then
          if CGN_PARITY = 1 then -- even parity
            parity <= '0';
          else -- odd parity
            parity <= '1';
          end if;
        elsif shift_en = '1' then
          parity <= parity_chk;
        end if;
      end if;
    end process;

    parity_chk <= parity XOR pdata(0); 


    cdc_sync_parity_vec_out_inst: cdc_sync  
    generic map
    (
      CGN_DATA_WIDTH           =>  CGN_PERR_COUNT_WIDTH,
      CGN_USE_INPUT_REG_A      =>  0,
      CGN_USE_OUTPUT_REG_A     =>  0,
      CGN_USE_GRAY_CONVERSION  =>  0, 
      CGN_NUM_SYNC_REGS_B      =>  1,
      CGN_NUM_OUTPUT_REGS_B    =>  1,
      CGN_GRAY_TO_BIN_STYLE    =>  0
    )
    PORT MAP
    (
      CLK_A_I  => SHIFT_CLK_I,
      CLK_B_I  => PCLK_I,
      PORT_A_I => perr_count,
      PORT_B_O => PERR_COUNT_O
    );


    cdc_sync_parity_out_inst: cdc_sync  
    generic map
    (
      CGN_DATA_WIDTH           =>  1,
      CGN_USE_INPUT_REG_A      =>  0,
      CGN_USE_OUTPUT_REG_A     =>  0,
      CGN_USE_GRAY_CONVERSION  =>  0, 
      CGN_NUM_SYNC_REGS_B      =>  1,
      CGN_NUM_OUTPUT_REGS_B    =>  1,
      CGN_GRAY_TO_BIN_STYLE    =>  0
    )
    PORT MAP
    (
      CLK_A_I     => SHIFT_CLK_I,
      CLK_B_I     => PCLK_I,    
      PORT_A_I(0) => perr_out,
      PORT_B_O(0) => PERR_O  
    );


    cdc_sync_parity_in_inst: cdc_sync  
    generic map
    (
      CGN_DATA_WIDTH           =>  1,
      CGN_USE_INPUT_REG_A      =>  0,
      CGN_USE_OUTPUT_REG_A     =>  0,
      CGN_USE_GRAY_CONVERSION  =>  0, 
      CGN_NUM_SYNC_REGS_B      =>  1,
      CGN_NUM_OUTPUT_REGS_B    =>  1,
      CGN_GRAY_TO_BIN_STYLE    =>  0
    )
    PORT MAP
    (
      CLK_A_I     => PCLK_I,
      CLK_B_I     => SHIFT_CLK_I,
      PORT_A_I(0) => PERR_COUNT_RST_I,
      PORT_B_O(0) => perr_count_rst
 
    );

  end generate;

end architecture behavioral;
