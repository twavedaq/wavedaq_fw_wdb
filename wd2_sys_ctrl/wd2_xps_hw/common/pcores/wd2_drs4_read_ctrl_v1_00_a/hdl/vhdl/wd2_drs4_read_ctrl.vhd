---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  DRS4 Readout Control
--
--  Project :  WaveDream2 (\\FS03\Mentor_Prj\48\121202\C\)
--
--  PCB  :  121202C DRS4 Wave DREAM Board
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  06.07.2015 11:29:55
--
--  Description :  Entity to generate all control signals for the DRS4 chip and the
--                 ADC on the WD2 board. Configuration tasks over slow (SPI, Shiftregister)
--                 interfaces are not included here.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
--use ieee.numeric_std.all;
library UNISIM;
use UNISIM.Vcomponents.ALL;
--use work.drs4_pack.all;
library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

entity wd2_drs4_read_ctrl is
  generic (
    CGN_ADC_CLOCK_PERIOD_NS     : integer := 12;
    CGN_SR_CLK_DIVIDER          : integer :=  5;
    CGN_SR_CLK_DIV_COUNTER_SIZE : integer :=  3;
    CGN_MAX_VALID_DELAY_STEPS   : integer := 60;
    CGN_ODELAY_VALUE            : integer :=  0;
    CGN_SR_CLOCK_PERIOD_NS      : integer := 60  -- min CGN_ADC_CLOCK_PERIOD_NS*2
  );
  port (
--    ADC_WORD_VALID_0_I   : in  std_logic; -- from ISERDES Interface
--    ADC_WORD_VALID_1_I   : in  std_logic; -- from ISERDES Interface
    EVENT_DATA_VALID_O   : out std_logic; -- to ISERDES Interface
    VALID_DELAY_I        : in  std_logic_vector(7 downto 0); -- from control register
    ADC_0_CH_EN_I        : in  std_logic_vector(8 downto 0);
    ADC_1_CH_EN_I        : in  std_logic_vector(8 downto 0);
    ADC_0_CH_TX_EN_O     : out std_logic_vector(8 downto 0);
    ADC_1_CH_TX_EN_O     : out std_logic_vector(8 downto 0);
    EVENT_NUMBER_O       : out std_logic_vector(31 downto 0);
    EVENT_COUNTER_RST_I  : in  std_logic;
    DRS_SAMPLING_RATE_I  : in  std_logic_vector(15 downto 0);
    ADC_SAMPLING_RATE_I  : in  std_logic_vector(15 downto 0);
    SAMPLING_RATE_O      : out std_logic_vector(15 downto 0);

    DA_ACTIVE_I          : in  std_logic; -- from control register
    READOUT_MODE_I       : in  std_logic; -- from control register
    READOUT_SOURCE_I     : in  std_logic; -- from control register
    READOUT_SOURCE_O     : out std_logic;
    START_DAQ_I          : in  std_logic; -- from control register
    CONTINUOUS_DAQ_I     : in  std_logic; -- from control register

    DRS_CONFIGURE_I      : in  std_logic; -- from control register
    DRS_CONFIG_DONE_O    : out std_logic;
    DRS_SOFT_TRIGGER_I   : in  std_logic; -- from control register
    DRS_TRIGGER_I        : in  std_logic; -- from trigger logic

    -- DRS Chip Registers
    -- Configuration Register (CFR) Bits
    DRS_CFR_DMODE_I      : in  std_logic; -- from control register
    DRS_CFR_PLLEN_I      : in  std_logic; -- from control register
    DRS_CFR_WSRLOOP_I    : in  std_logic; -- from control register
    -- Write Shift Register (WSR)
    DRS_WSR_I            : in  std_logic_vector(7 downto 0); -- from control register
    -- Write Config Register (WCR)
    DRS_WCR_I            : in  std_logic_vector(7 downto 0); -- from control register

    -- DRS Chip Control Signals
    DRS_DENABLE_O        : out std_logic; -- to drs chip
    DRS_DWRITE_I         : in  std_logic; -- to drs chip
    DRS_DWRITE_O         : out std_logic; -- to drs chip
    DRS_DWRITE_T         : out std_logic; -- to drs chip
    DRS_RSRLOAD_O        : out std_logic; -- to drs chip
    DRS_SRCLK_0_O        : out std_logic; -- to drs chip
    DRS_SRCLK_1_O        : out std_logic; -- to drs chip
    DRS_ADDR_O           : out std_logic_vector(3 downto 0); -- to drs chip
    DRS_SRIN_O           : out std_logic; -- to drs chip
    DRS_SROUT_0_I        : in  std_logic; -- from drs chip
    DRS_SROUT_1_I        : in  std_logic; -- from drs chip

    -- 
    READING_CH9_O        : out std_logic;
    BUSY_O               : out std_logic;
    SYS_BUSY_I           : in  std_logic;

    STOP_CELL_0_O        : out std_logic_vector(9 downto 0); -- to status register
    STOP_WSR_0_O         : out std_logic_vector(7 downto 0); -- to status register
    STOP_CELL_1_O        : out std_logic_vector(9 downto 0); -- to status register
    STOP_WSR_1_O         : out std_logic_vector(7 downto 0); -- to status register

    LMK_DRS_REF_CLK_EN_I : in  std_logic;
    LMK_LOCK_I           : in  std_logic;
    LMK_SYNC_N_I         : in  std_logic;
    RESET_I              : in  std_logic;
    CLK_DRS_CTRL_I       : in  std_logic; -- DRS control (ADC sampling) clock
    CLK_DAQ_I            : in  std_logic; -- DAQ (divided data) clock
    CLK_SYS_I            : in  std_logic  -- Microblaze clock
  );
end wd2_drs4_read_ctrl;

architecture behavioral of wd2_drs4_read_ctrl is

  constant C_SAMPLES_PER_EVENT         : integer := 1024;
  constant C_FSM_TIMER_SIZE            : integer := 15;
  constant C_CLK_EDGE_COUNTER_SIZE     : integer := log2ceil(C_SAMPLES_PER_EVENT+1); --11; -- count 1024
  constant C_SAMPLE_COUNTER_SIZE       : integer := log2ceil(C_SAMPLES_PER_EVENT+1); --11; -- count 1024
  constant C_FSM_CLK_PERIOD            : integer := CGN_ADC_CLOCK_PERIOD_NS;
  constant C_START_DELAY               : integer := 120000/C_FSM_CLK_PERIOD; -- 120.00us/6ns = 20000
  --constant C_START_DELAY               : integer :=   3150/C_FSM_CLK_PERIOD; --   3.15us/6ns =   500
  constant C_POST_RST_DELAY            : integer := 120000/C_FSM_CLK_PERIOD; -- 120.00us/6ns = 20000
  constant C_WAIT_VDD_DELAY            : integer := 120000/C_FSM_CLK_PERIOD; -- 120.00us/6ns = 20000
  constant C_DWRITE_DENABLE_DELAY      : integer :=     12/C_FSM_CLK_PERIOD; --     12ns/6ns =     2
  constant C_VALID_DELAY_BITS          : integer := 8;
  constant C_MASK_COUNT_INIT           : integer := CGN_SR_CLK_DIVIDER-1;
  constant C_MASK_COUNT_BITS           : integer := log2ceil(CGN_SR_CLK_DIVIDER);

  -- DRS addresses
  constant C_DRS_ADDR_SELECT_CH0       : std_logic_vector(3 downto 0) := "0000";
  constant C_DRS_ADDR_EN_ALL_OUT       : std_logic_vector(3 downto 0) := "1001";
  constant C_DRS_ADDR_EN_TRANSP_MODE   : std_logic_vector(3 downto 0) := "1010";
  constant C_DRS_ADDR_READ_SHIFT_REG   : std_logic_vector(3 downto 0) := "1011";
  constant C_DRS_ADDR_CONFIG_REG       : std_logic_vector(3 downto 0) := "1100";
  constant C_DRS_ADDR_WRITE_SHIFT_REG  : std_logic_vector(3 downto 0) := "1101";
  constant C_DRS_ADDR_WRITE_CONFIG_REG : std_logic_vector(3 downto 0) := "1110";
  constant C_DRS_ADDR_STANDBY          : std_logic_vector(3 downto 0) := "1111";

  attribute fsm_encoding : string;
  attribute keep         : string;

  -- state of DRS readout state machine
  type type_state is (init, idle,
                      done, trailer, start_running, running, set_denable, start_readout,
                      readout, setup_load_rsr, load_rsr,
                      conf_setup, conf_strobe, conf_stop,
                      init_rsr_done, init_rsr); -- readout_ssr
  signal state        : type_state;
  attribute fsm_encoding of state : signal is "one-hot";

  type type_sample_state is (init_sampling, delay_sampling, sampling, done_sampling);
  signal state_sample       : type_sample_state;
  attribute fsm_encoding of state_sample : signal is "one-hot";

  signal fsm_timer_count           : std_logic_vector(C_FSM_TIMER_SIZE-1 downto 0)            := (others=>'0');
  signal clk_div_count             : std_logic_vector(CGN_SR_CLK_DIV_COUNTER_SIZE-1 downto 0) := (others=>'0');
  signal drs_srclk_edge_count      : std_logic_vector(C_CLK_EDGE_COUNTER_SIZE-1 downto 0)     := (others=>'0');
  signal rst_clk_div_counter       : std_logic := '0';

  signal sampling_start            : std_logic := '0';
  signal sampling_start_s          : std_logic := '0';
  signal sampling_idle             : std_logic := '0';
  signal rst_sample_fsm            : std_logic := '0';
  signal sample_count              : std_logic_vector(C_SAMPLE_COUNTER_SIZE-1 downto 0) := (others=>'0');
  signal mask_count                : std_logic_vector(C_MASK_COUNT_BITS-1 downto 0) := (others=>'0');
  signal mask_ratio_m              : std_logic_vector(C_MASK_COUNT_BITS-1 downto 0) := (others=>'0');
  signal mask_ratio                : std_logic_vector(C_MASK_COUNT_BITS-1 downto 0) := (others=>'0');
  signal mask_ratio_s              : std_logic_vector(C_MASK_COUNT_BITS-1 downto 0) := (others=>'0');
  signal event_data_valid          : std_logic := '0';
  signal event_count               : std_logic_vector(31 downto 0) := (others=>'0');
  signal adc_0_ch_en               : std_logic_vector( 8 downto 0) := (others=>'0');
  signal adc_1_ch_en               : std_logic_vector( 8 downto 0) := (others=>'0');
  signal adc_0_ch_tx_en            : std_logic_vector( 8 downto 0) := (others=>'0');
  signal adc_1_ch_tx_en            : std_logic_vector( 8 downto 0) := (others=>'0');
  signal reading_ch9               : std_logic := '0';

  signal drs_cfr                   : std_logic_vector( 7 downto 0) := (others=>'0');
  signal drs_wsr                   : std_logic_vector( 7 downto 0) := (others=>'0');
  signal drs_wcr                   : std_logic_vector( 7 downto 0) := (others=>'0');
  signal drs_cfg_sel               : std_logic_vector( 1 downto 0) := (others=>'0');
  signal drs_cfg_addr              : std_logic_vector( 3 downto 0) := (others=>'0');
  signal drs_cfg_data              : std_logic_vector( 7 downto 0) := (others=>'0');
  signal drs_addr_readout          : std_logic_vector( 3 downto 0) := (others=>'0');
  signal drs_sampling_rate         : std_logic_vector(15 downto 0) := (others=>'0');

  signal event_counter_rst         : std_logic := '0';
  signal soft_trigger              : std_logic := '0';
  signal soft_trigger_s0           : std_logic := '0';
  signal soft_trigger_s1           : std_logic := '0';
  signal reset                     : std_logic := '0';

  signal drs_addr                  : std_logic_vector(3 downto 0) := (others=>'0');
  signal drs_addr_s                : std_logic_vector(3 downto 0) := (others=>'0');
  signal drs_rsrload               : std_logic := '0';
  signal drs_rsrload_s             : std_logic := '0';
  signal fsm_denable               : std_logic := '0';
  signal drs_denable               : std_logic := '0';
  signal drs_denable_s             : std_logic := '0';
  signal drs_srin                  : std_logic := '0';
  signal drs_srin_s                : std_logic := '0';
  signal drs_srclk                 : std_logic := '0';
  signal drs_srclk_s               : std_logic := '0';
  signal drs_srclk_0_out_s         : std_logic := '0';
  signal drs_srclk_1_out_s         : std_logic := '0';
  signal drs_srclk_out             : std_logic := '0';
  signal drs_srclk_out_s           : std_logic := '0';
  signal drs_srclk_out_s2          : std_logic := '0';
  signal drs_srout_0               : std_logic := '0';
  signal drs_srout_1               : std_logic := '0';
  attribute keep of drs_srclk      : signal is "true";
  attribute keep of drs_srclk_0_out_s : signal is "true";
  attribute keep of drs_srclk_1_out_s : signal is "true";

--  signal drs_stop_wsr_0            : std_logic_vector(7 downto 0) := (others=>'0');
--  signal drs_stop_wsr_1            : std_logic_vector(7 downto 0) := (others=>'0');
  signal stop_wsr_sreg_en          : std_logic := '0';
  signal drs_stop_cell_0           : std_logic_vector(9 downto 0) := (others=>'0');
  signal drs_stop_cell_1           : std_logic_vector(9 downto 0) := (others=>'0');
  signal stop_cell_0_out           : std_logic_vector(9 downto 0) := (others=>'0');
  signal stop_cell_1_out           : std_logic_vector(9 downto 0) := (others=>'0');
  signal store_drs_stop_cells      : std_logic := '0';
  signal store_drs_stop_cells_s    : std_logic := '0';
  signal drs_sr_reg                : std_logic_vector(7 downto 0) := (others=>'0');

  signal old_readout_mode          : std_logic := '0';
  signal readout_mode              : std_logic := '0';
  signal readout_source            : std_logic := '0';
  signal readout_source_trg        : std_logic := '0';
  signal da_active                 : std_logic := '0';
  signal start_daq                 : std_logic := '0';
  signal continuous_daq            : std_logic := '0';
  signal drs_configure             : std_logic := '0';
  signal drs_config_done           : std_logic := '0';
  signal valid_delay               : std_logic_vector(C_VALID_DELAY_BITS-1 downto 0) := (others=>'0');

  signal lmk_drs_ref_clk_en        : std_logic := '0';
  signal lmk_lock                  : std_logic := '0';
  signal lmk_sync_n                : std_logic := '0';
  signal daq_inhibit               : std_logic := '0';

  signal drs_soft_trig             : std_logic := '0';
  signal set_drs_write             : std_logic := '0';
  signal rst_drs_write             : std_logic := '0';
  signal rst_dwrite                : std_logic := '0';
  signal trigger                   : std_logic := '0';
  signal trigger_inhibit           : std_logic := '0';

  signal drs_write_s1              : std_logic := '0';
  signal drs_write_s2              : std_logic := '0';
  signal drs_write_s3              : std_logic := '0';

begin

  drs_addr_readout <= C_DRS_ADDR_EN_TRANSP_MODE when readout_source_trg = '1' else
                      C_DRS_ADDR_SELECT_CH0     when reading_ch9 = '1' else
                      C_DRS_ADDR_EN_ALL_OUT;
  --   read data from ADC in transparent mode or perform readout of sampled drs data

  -- Control signal synchronization
  cdc_sync_sys_to_drs_ctrl: cdc_sync  
  generic map
  (
    CGN_DATA_WIDTH           =>  49,
    CGN_USE_INPUT_REG_A      =>  0,
    CGN_USE_OUTPUT_REG_A     =>  0,
    CGN_USE_GRAY_CONVERSION  =>  0,
    CGN_NUM_SYNC_REGS_B      =>  2,
    CGN_NUM_OUTPUT_REGS_B    =>  0,
    CGN_GRAY_TO_BIN_STYLE    =>  0
  )
  PORT MAP
  (
    CLK_A_I               => CLK_SYS_I,
    CLK_B_I               => CLK_DAQ_I,
    PORT_A_I( 7 downto  0) => DRS_WSR_I,
    PORT_A_I(15 downto  8) => DRS_WCR_I,
    PORT_A_I(24 downto 16) => ADC_0_CH_EN_I,
    PORT_A_I(33 downto 25) => ADC_1_CH_EN_I,
    PORT_A_I(34)           => DRS_CFR_DMODE_I,
    PORT_A_I(35)           => DRS_CFR_PLLEN_I,
    PORT_A_I(36)           => DRS_CFR_WSRLOOP_I,
    PORT_A_I(37)           => EVENT_COUNTER_RST_I,
    PORT_A_I(38)           => DRS_SOFT_TRIGGER_I,
    PORT_A_I(39)           => RESET_I,
    PORT_A_I(40)           => DA_ACTIVE_I,
    PORT_A_I(41)           => READOUT_MODE_I,
    PORT_A_I(42)           => READOUT_SOURCE_I,
    PORT_A_I(43)           => START_DAQ_I,
    PORT_A_I(44)           => CONTINUOUS_DAQ_I,
    PORT_A_I(45)           => DRS_CONFIGURE_I,
    PORT_A_I(46)           => LMK_DRS_REF_CLK_EN_I,
    PORT_A_I(47)           => LMK_LOCK_I,
    PORT_A_I(48)           => LMK_SYNC_N_I,
    PORT_B_O( 7 downto  0) => drs_wsr,
    PORT_B_O(15 downto  8) => drs_wcr,
    PORT_B_O(24 downto 16) => adc_0_ch_en,
    PORT_B_O(33 downto 25) => adc_1_ch_en,
    PORT_B_O(36 downto 34) => drs_cfr(2 downto 0),
    PORT_B_O(37)           => event_counter_rst,
    PORT_B_O(38)           => soft_trigger_s0,
    PORT_B_O(39)           => reset,
    PORT_B_O(40)           => da_active,
    PORT_B_O(41)           => readout_mode,
    PORT_B_O(42)           => readout_source,
    PORT_B_O(43)           => start_daq,
    PORT_B_O(44)           => continuous_daq,
    PORT_B_O(45)           => drs_configure,
    PORT_B_O(46)           => lmk_drs_ref_clk_en,
    PORT_B_O(47)           => lmk_lock,
    PORT_B_O(48)           => lmk_sync_n
  );
  drs_cfr(7 downto 3) <= (others=>'1'); -- drs datasheet rev 0.9, page 11

  cdc_sync_sys_to_drs_ctrl_valid: cdc_sync  
  generic map
  (
    CGN_DATA_WIDTH           =>  C_VALID_DELAY_BITS+16,
    CGN_USE_INPUT_REG_A      =>  0,
    CGN_USE_OUTPUT_REG_A     =>  0,
    CGN_USE_GRAY_CONVERSION  =>  0,
    CGN_NUM_SYNC_REGS_B      =>  2,
    CGN_NUM_OUTPUT_REGS_B    =>  0,
    CGN_GRAY_TO_BIN_STYLE    =>  0
  )
  PORT MAP
  (
    CLK_A_I => CLK_SYS_I,
    CLK_B_I => CLK_DAQ_I,
    PORT_A_I(15 downto 0)                     => DRS_SAMPLING_RATE_I,
    PORT_A_I(15+C_VALID_DELAY_BITS downto 16) => VALID_DELAY_I,
    PORT_B_O(15 downto 0)                     => drs_sampling_rate,
    PORT_B_O(15+C_VALID_DELAY_BITS downto 16) => valid_delay
  );

  cdc_sync_daq_to_sys : cdc_sync
  generic map
  (
    CGN_DATA_WIDTH           =>  1,
    CGN_USE_INPUT_REG_A      =>  0,
    CGN_USE_OUTPUT_REG_A     =>  0,
    CGN_USE_GRAY_CONVERSION  =>  0,
    CGN_NUM_SYNC_REGS_B      =>  2,
    CGN_NUM_OUTPUT_REGS_B    =>  0,
    CGN_GRAY_TO_BIN_STYLE    =>  0
  )
  PORT MAP
  (
    CLK_A_I => CLK_DAQ_I,
    CLK_B_I => CLK_SYS_I,
    PORT_A_I(0) => drs_config_done,
    PORT_B_O(0) => DRS_CONFIG_DONE_O
  );

  cdc_sync_stop_cell : cdc_sync
  generic map
  (
    CGN_DATA_WIDTH           =>  20,
    CGN_USE_INPUT_REG_A      =>  0,
    CGN_USE_OUTPUT_REG_A     =>  0,
    CGN_USE_GRAY_CONVERSION  =>  0,
    CGN_NUM_SYNC_REGS_B      =>  2,
    CGN_NUM_OUTPUT_REGS_B    =>  0,
    CGN_GRAY_TO_BIN_STYLE    =>  0
  )
  PORT MAP
  (
    CLK_A_I => CLK_DRS_CTRL_I,
    CLK_B_I => CLK_SYS_I,
    PORT_A_I( 9 downto  0) => stop_cell_0_out,
    PORT_A_I(19 downto 10) => stop_cell_1_out,
    PORT_B_O( 9 downto  0) => STOP_CELL_0_O,
    PORT_B_O(19 downto 10) => STOP_CELL_1_O
  );

  trigger_sync_ff : process(CLK_DAQ_I)
  begin
    if rising_edge(CLK_DAQ_I) then
      drs_write_s1 <= DRS_DWRITE_I;
      drs_write_s2 <= drs_write_s1;
      drs_write_s3 <= drs_write_s2;
    end if;
  end process trigger_sync_ff;

  DRS_DWRITE_T <= '0';

  trigger_valid_sync_ff : process(CLK_DAQ_I)
  begin
    if rising_edge(CLK_DAQ_I) then
      soft_trigger_s1 <= soft_trigger_s0;
      soft_trigger <= soft_trigger_s0 or soft_trigger_s1;
    end if;
  end process trigger_valid_sync_ff;

  trigger <= DRS_TRIGGER_I or soft_trigger or rst_drs_write;
  rst_dwrite <= '0' when trigger_inhibit = '1' else trigger;

  trg_inhibit_latch : process(SYS_BUSY_I, trigger)
  begin
    if SYS_BUSY_I = '1' then
      trigger_inhibit <= '1';
    elsif trigger = '0' then
      trigger_inhibit <= '0';
    end if;
  end process trg_inhibit_latch;

  -- flip-flop for o_drs_write signal: raise with set_drs_write and lower with trigger or rst
  proc_drs_write_ff: process(rst_dwrite, set_drs_write)
  begin
    if set_drs_write = '1' then
      DRS_DWRITE_O <= '1';
    elsif rising_edge(rst_dwrite) then
      DRS_DWRITE_O <= '0';
    end if;
  end process;

  clk_div_counter : process(CLK_DAQ_I)
  begin
    if rising_edge(CLK_DAQ_I) then
      if rst_clk_div_counter = '1' then
        clk_div_count <= (others=>'0');
      else
        if clk_div_count > 0 then
          clk_div_count <= clk_div_count - 1;
        elsif drs_srclk_edge_count > 0 then
          clk_div_count <= CONV_STD_LOGIC_VECTOR(CGN_SR_CLK_DIVIDER-1, CGN_SR_CLK_DIV_COUNTER_SIZE);
        end if;
      end if;
    end if;
  end process clk_div_counter;

  srclk_gen : process(CLK_DAQ_I)
  begin
    if rising_edge(CLK_DAQ_I) then
      drs_srclk_s <= drs_srclk;
      if clk_div_count = CONV_STD_LOGIC_VECTOR(CGN_SR_CLK_DIVIDER/2, CGN_SR_CLK_DIV_COUNTER_SIZE) then
        drs_srclk       <= '1';
        drs_srclk_out   <= '1';
      elsif clk_div_count = 0 then
        drs_srclk       <= '0';
        drs_srclk_out   <= '0';
      end if;
    end if;
  end process srclk_gen;

  sample_fsm : process(CLK_DAQ_I)
  begin
    if rising_edge(CLK_DAQ_I) then
      sampling_idle    <= '0';
      event_data_valid <= '0';

      if rst_sample_fsm = '1' then
        state_sample <= done_sampling;
        sample_count <= (others=>'0');
        mask_count   <= (others=>'0');
      else

        case state_sample is

          when init_sampling =>
            sampling_idle <= '1';
            sample_count  <= (others=>'0');
            sample_count(C_VALID_DELAY_BITS-1 downto 0) <= valid_delay;
            if sampling_start = '1' then
              mask_ratio_m <= mask_ratio;
              state_sample <= delay_sampling;
            end if;

          when delay_sampling =>
            if sample_count = 0 then
              sample_count <= CONV_STD_LOGIC_VECTOR(C_SAMPLES_PER_EVENT, C_SAMPLE_COUNTER_SIZE);
              mask_count   <= (others=>'0');
              state_sample <= sampling;
            else
              sample_count <= sample_count - 1;
            end if;

          when sampling =>
            if sample_count = 0 then
              state_sample <= done_sampling;
            else
              if mask_count = 0 then
                mask_count <= mask_ratio_m;
                event_data_valid <= '1';
                sample_count <= sample_count - 1;
              else
                mask_count <= mask_count - 1;
              end if;
            end if;

          when done_sampling =>
            sampling_idle <= '1';
            if sampling_start = '0' then
              state_sample <= init_sampling;
            end if;

          when others =>
            state_sample <= done_sampling;

        end case;
      end if;
      EVENT_DATA_VALID_O <= event_data_valid;
    end if;
  end process sample_fsm;

  readout_fsm : process(CLK_DAQ_I)
  begin
    if rising_edge(CLK_DAQ_I) then

      -- default states
      drs_rsrload          <= '0';
      BUSY_O               <= '1';
      set_drs_write        <= '0';
      rst_drs_write        <= '0';
      rst_sample_fsm       <= '0';
      sampling_start       <= '0';
      store_drs_stop_cells <= '0';
      mask_ratio           <= (others=>'0');

      if drs_configure = '0' then
        drs_config_done <= '0';
      end if;

      -- fsm timer
      if fsm_timer_count > 0 then
        fsm_timer_count <= fsm_timer_count - 1;
      end if;

      if drs_srclk_edge_count > 0 and (drs_srclk_s = '0' and drs_srclk = '1') then -- edge count and rising edge
        drs_srclk_edge_count <= drs_srclk_edge_count - 1;
      end if;

      if reset = '1' then
        fsm_denable          <= '0';
        drs_addr             <= C_DRS_ADDR_EN_TRANSP_MODE;
        drs_cfg_sel          <= "11";
        SAMPLING_RATE_O      <= ADC_SAMPLING_RATE_I;
        readout_source_trg   <= '0';
        reading_ch9          <= '0';
        drs_srin             <= '0';
        rst_drs_write        <= '1';
        event_count          <= (others=>'0');
        event_count(0)       <= '1';
        drs_srclk_edge_count <= (others=>'0');
        rst_sample_fsm       <= '1';
        sampling_start       <= '0';
        store_drs_stop_cells <= '0';
        mask_ratio           <= (others=>'0');
        fsm_timer_count      <= CONV_STD_LOGIC_VECTOR(C_POST_RST_DELAY, C_FSM_TIMER_SIZE);
        state <= init;
      else
        case state is
          when init =>
            fsm_denable <= '0';
            if drs_configure = '1' and drs_config_done = '0' then
              fsm_timer_count <= CONV_STD_LOGIC_VECTOR(CGN_SR_CLK_DIVIDER, C_FSM_TIMER_SIZE);
              drs_cfg_sel <= "11";
              state <= conf_setup;
            elsif fsm_timer_count = 0 then
              state <= idle;
            end if;

          when idle =>
            drs_addr    <= C_DRS_ADDR_EN_TRANSP_MODE;
            drs_srin    <= '0';
            fsm_denable <= da_active;   -- start domino wave only if dactive=1

            -- detect 1 to 0 transition of readout mode
            old_readout_mode <= readout_mode;
            if old_readout_mode = '1' and readout_mode = '0' then
              drs_srclk_edge_count <= CONV_STD_LOGIC_VECTOR(C_SAMPLES_PER_EVENT, C_CLK_EDGE_COUNTER_SIZE);
              state <= init_rsr;
            elsif drs_configure = '1' and drs_config_done = '0' then
              state <= init;
            elsif daq_inhibit = '0' and (start_daq = '1' or continuous_daq = '1') then
              fsm_timer_count <= CONV_STD_LOGIC_VECTOR(C_START_DELAY, C_FSM_TIMER_SIZE);
              state <= start_running;
            end if;

          -- -- -- Standard acquisition -- -- --
          when start_running =>
            --drs_addr       <= C_DRS_ADDR_EN_TRANSP_MODE;
            fsm_denable    <= '1';
            set_drs_write  <= '1';  -- set DRS_DWRITE_IO in proc_drs_write
            if daq_inhibit = '1' then
              state <= idle;
            elsif fsm_timer_count = 0 then
              state <= running;
            end if;

          when running =>
            --drs_addr       <= C_DRS_ADDR_EN_TRANSP_MODE;
            BUSY_O <= '0'; -- ready for trigger
            if daq_inhibit = '1' then
              state <= idle;
            elsif (drs_write_s2 = '0' and drs_write_s3 = '1') or   -- dwrite falling edge or
                   --soft_trigger   = '1' or   -- software trigger or
                   drs_cfr(0)     = '0' then -- DMODE = 0
              -- store channels to transmit
              adc_0_ch_tx_en <= adc_0_ch_en;
              adc_1_ch_tx_en <= adc_1_ch_en;
              -- stop domino wave & start readout sequence
              if readout_source = '0' then -- read drs samples
                SAMPLING_RATE_O    <= drs_sampling_rate;
                readout_source_trg <= '0';
                rst_drs_write      <= '1';
                fsm_timer_count    <= CONV_STD_LOGIC_VECTOR(C_DWRITE_DENABLE_DELAY, C_FSM_TIMER_SIZE);
                state <= set_denable;
              else
                -- ADC synchronization???
                SAMPLING_RATE_O    <= ADC_SAMPLING_RATE_I;
                readout_source_trg <= '1';
                sampling_start     <= '1';
                mask_ratio         <= (others=>'0');
                state <= readout;
              end if;
            end if;

          when set_denable =>
            --drs_addr       <= C_DRS_ADDR_EN_TRANSP_MODE;
            if fsm_timer_count = 0 then
              fsm_denable <= da_active;   -- stop domino wave only if dactive=0
              state <= start_readout;
            end if;

          when start_readout =>
            --drs_addr       <= C_DRS_ADDR_EN_TRANSP_MODE;
            --drs_stop_wsr_0  <= (others => '0');
            --drs_stop_wsr_1  <= (others => '0');

--            if fsm_timer_count = 0 then
--              if da_active = '0' then 
--                fsm_denable <= '0';   -- stop domino wave only if dactive=0
--              end if;   

            if readout_mode = '1' then --and DRS_ADC_CLK_O = '1' then
              --drs_srclk_edge_count <= CONV_STD_LOGIC_VECTOR(8, C_CLK_EDGE_COUNTER_SIZE);
              --state <= store_wsr;
              fsm_timer_count <= CONV_STD_LOGIC_VECTOR(CGN_SR_CLK_DIVIDER, C_FSM_TIMER_SIZE);
              state <= setup_load_rsr;
            else
              sampling_start       <= '1';
              mask_ratio           <= CONV_STD_LOGIC_VECTOR(C_MASK_COUNT_INIT, C_MASK_COUNT_BITS);
              drs_srclk_edge_count <= CONV_STD_LOGIC_VECTOR(C_SAMPLES_PER_EVENT, C_CLK_EDGE_COUNTER_SIZE);
              state <= readout;
            end if;

-- -- !!! WSR Store only needed for channel cascading which is not supported on WD2 !!!
-- -- !!! due to the way DRS channels are connected to the ADC                      !!!
--
--          when store_wsr =>
--            drs_addr   <= C_DRS_ADDR_WRITE_SHIFT_REG;
--
--            if drs_srclk_s = '0' and drs_srclk = '1' then -- rising edge
--              drs_stop_wsr_0 <= drs_stop_wsr_0(6 downto 0) & drs_srout_0;
--              drs_stop_wsr_1 <= drs_stop_wsr_1(6 downto 0) & drs_srout_1;
--            end if;
--
--            if drs_srclk_edge_count = 0 then
--              if fsm_timer_count = 0 then
--                STOP_WSR_0_O    <= drs_stop_wsr_0;
--                STOP_WSR_1_O    <= drs_stop_wsr_1;
--                fsm_timer_count <= CONV_STD_LOGIC_VECTOR(CGN_SR_CLK_DIVIDER, C_FSM_TIMER_SIZE);
--                state <= setup_load_rsr;
--              end if;
--            else
--              fsm_timer_count <= CONV_STD_LOGIC_VECTOR(CGN_SR_CLK_DIVIDER, C_FSM_TIMER_SIZE);
--            end if;

          when setup_load_rsr =>
          -- state added for timing i.e. to make sure ts > 5 ns for RSLOAD after address change
            drs_addr   <= drs_addr_readout;
            if drs_srclk_edge_count = 0 then
              if fsm_timer_count = 0 then
                fsm_timer_count <= CONV_STD_LOGIC_VECTOR(CGN_SR_CLK_DIVIDER, C_FSM_TIMER_SIZE);
                state <= load_rsr;
              end if;
            end if;

          when load_rsr =>
            drs_rsrload <= '1'; -- load read shift register with stop position
            --drs_addr   <= drs_addr_readout;
            if fsm_timer_count = 0 then
              sampling_start       <= '1';
              mask_ratio           <= CONV_STD_LOGIC_VECTOR(C_MASK_COUNT_INIT, C_MASK_COUNT_BITS);
              drs_srclk_edge_count <= CONV_STD_LOGIC_VECTOR(C_SAMPLES_PER_EVENT, C_CLK_EDGE_COUNTER_SIZE);
              state <= readout;
            end if;

--          when readout_ssr =>
--            --drs_addr   <= drs_addr_readout;
--
--            if drs_srclk_s = '0' and drs_srclk = '1' then -- rising edge
--              drs_stop_cell_0 <= drs_stop_cell_0(8 downto 0) & drs_srout_0;
--              drs_stop_cell_1 <= drs_stop_cell_1(8 downto 0) & drs_srout_1;
--            end if;
--
--            if drs_srclk_edge_count = C_SAMPLES_PER_EVENT - 10 then
--              stop_cell_0_out <= drs_stop_cell_0;
--              stop_cell_1_out <= drs_stop_cell_1;
--              state <= readout;
--            end if;

          when readout =>
            drs_addr   <= drs_addr_readout;

            if (readout_source_trg = '0' and drs_srclk_edge_count = 0) or -- drs sampling done
               (readout_source_trg = '1' and sampling_idle = '0') then    -- adc read started
              state <= trailer;
            end if;

            if drs_srclk_edge_count = C_SAMPLES_PER_EVENT - 9 then
              store_drs_stop_cells <= '1';
            end if;

          when trailer =>
            if sampling_idle = '1' then
              fsm_timer_count <= CONV_STD_LOGIC_VECTOR(CGN_SR_CLK_DIVIDER, C_FSM_TIMER_SIZE);
              state <= done; -- default
              if readout_source_trg = '0' and 
                 (adc_0_ch_tx_en(8) = '1' or adc_1_ch_tx_en(8) = '1') and 
                 drs_addr = C_DRS_ADDR_EN_ALL_OUT then
                reading_ch9          <= '1';
                state <= start_readout;
              end if;
            end if;

          when done =>
            reading_ch9 <= '0';
            if fsm_timer_count = 0 then
              event_count <= event_count + 1;
              state <= idle;
            end if;

          -- -- -- INIT RSR States -- -- --
          when init_rsr =>
            drs_addr   <= C_DRS_ADDR_READ_SHIFT_REG;

            if drs_srclk_edge_count = 1 and drs_srclk = '0' then
              drs_srin   <= '1';
            end if;

            if drs_srclk_edge_count = 0 then
              fsm_timer_count <= CONV_STD_LOGIC_VECTOR(CGN_SR_CLK_DIVIDER, C_FSM_TIMER_SIZE);
              state <= init_rsr_done;
            end if;

          when init_rsr_done =>
            --drs_addr   <= C_DRS_ADDR_READ_SHIFT_REG;
            drs_srin   <= '1';
            if fsm_timer_count = 0 then
              state <= idle;
            end if;

          -- -- -- CONFIG States -- -- --
          when conf_setup =>
            fsm_denable    <= '0';
            drs_addr       <= drs_cfg_addr;
            drs_sr_reg     <= drs_cfg_data;
            if fsm_timer_count = 0 then
              drs_srclk_edge_count <= CONV_STD_LOGIC_VECTOR(8, C_CLK_EDGE_COUNTER_SIZE);
              state <= conf_strobe;
            end if;

          when conf_strobe =>  
            --drs_addr   <= drs_cfg_addr;
            fsm_denable    <= '0';

            if drs_srclk_s = '0' and drs_srclk = '1' then -- rising edge
              drs_sr_reg <= drs_sr_reg(6 downto 0) & '0';
              drs_srin   <= drs_sr_reg(7);
            end if;

            if drs_srclk_edge_count = 0 then
              fsm_timer_count <= CONV_STD_LOGIC_VECTOR(CGN_SR_CLK_DIVIDER, C_FSM_TIMER_SIZE);
              drs_cfg_sel <= drs_cfg_sel - 1;
              state <= conf_stop;
            end if;
          
          when conf_stop =>
            --drs_addr   <= drs_cfg_addr;
            fsm_denable    <= '0';
            if fsm_timer_count = 0 then
              if drs_cfg_sel = 0 then
                fsm_timer_count <= CONV_STD_LOGIC_VECTOR(C_POST_RST_DELAY, C_FSM_TIMER_SIZE);
                drs_config_done <= '1'; 
                state <= idle;
              else
                drs_srclk_edge_count <= CONV_STD_LOGIC_VECTOR(8, C_CLK_EDGE_COUNTER_SIZE);
                state <= conf_setup;
              end if;
            end if;

          when others =>
            state <= idle;
        end case;
        -- reset event counter
        if event_counter_rst = '1' then
          event_count    <= (others=>'0');
          event_count(0) <= '1';
        end if;
      end if;
    end if;
  end process readout_fsm;

  process(drs_cfg_sel)
  begin
    case drs_cfg_sel is
      when "00" =>
        drs_cfg_addr <= C_DRS_ADDR_CONFIG_REG;
        drs_cfg_data <= drs_cfr;
      when "01" =>
        drs_cfg_addr <= C_DRS_ADDR_WRITE_CONFIG_REG;
        drs_cfg_data <= drs_wcr;
      when "10" =>
        drs_cfg_addr <= C_DRS_ADDR_WRITE_SHIFT_REG;
        drs_cfg_data <= drs_wsr;
      when "11" =>
        drs_cfg_addr <= C_DRS_ADDR_CONFIG_REG;
        drs_cfg_data <= drs_cfr;
      when others =>
        drs_cfg_addr <= C_DRS_ADDR_CONFIG_REG;
        drs_cfg_data <= drs_cfr;
    end case;
  end process;

  daq_inhibit <= not(lmk_drs_ref_clk_en) or not(lmk_lock) or not(lmk_sync_n);
  drs_denable <= fsm_denable and not(daq_inhibit);

  READOUT_SOURCE_O <= readout_source_trg;
  READING_CH9_O    <= reading_ch9;
  EVENT_NUMBER_O   <= event_count;
  ADC_0_CH_TX_EN_O <= adc_0_ch_tx_en;
  ADC_1_CH_TX_EN_O <= adc_1_ch_tx_en;

  cdc_sync_drs_if : cdc_sync
  generic map
  (
    CGN_DATA_WIDTH           =>  9,
    CGN_USE_INPUT_REG_A      =>  0,
    CGN_USE_OUTPUT_REG_A     =>  0,
    CGN_USE_GRAY_CONVERSION  =>  0,
    CGN_NUM_SYNC_REGS_B      =>  2,
    CGN_NUM_OUTPUT_REGS_B    =>  0,
    CGN_GRAY_TO_BIN_STYLE    =>  0
  )
  PORT MAP
  (
    CLK_A_I => CLK_DAQ_I,
    CLK_B_I => CLK_DRS_CTRL_I,
    PORT_A_I(3 downto 0) => drs_addr,
    PORT_A_I(4) => drs_denable,
    PORT_A_I(5) => drs_rsrload,
    PORT_A_I(6) => drs_srclk_out,
    PORT_A_I(7) => drs_srin,
    PORT_A_I(8) => store_drs_stop_cells,
    PORT_B_O(3 downto 0) => drs_addr_s,
    PORT_B_O(4) => drs_denable_s,
    PORT_B_O(5) => drs_rsrload_s,
    PORT_B_O(6) => drs_srclk_out_s,
    PORT_B_O(7) => drs_srin_s,
    PORT_B_O(8) => store_drs_stop_cells_s
  );

  drs_srclk_0_out_s <= drs_srclk_out_s;
  drs_srclk_1_out_s <= drs_srclk_out_s;

  io_ffs : process(CLK_DRS_CTRL_I)
  begin
    if rising_edge(CLK_DRS_CTRL_I) then
      drs_srclk_out_s2 <= drs_srclk_out_s;
      DRS_DENABLE_O    <= drs_denable_s;
      DRS_RSRLOAD_O    <= drs_rsrload_s;
      DRS_SRCLK_0_O    <= drs_srclk_0_out_s;
      DRS_SRCLK_1_O    <= drs_srclk_1_out_s;
      DRS_ADDR_O       <= drs_addr_s;
      DRS_SRIN_O       <= drs_srin_s;
      drs_srout_0      <= DRS_SROUT_0_I;
      drs_srout_1      <= DRS_SROUT_1_I;
    end if;
  end process io_ffs;

  stop_cell_sr : process(CLK_DRS_CTRL_I)
  begin
    if rising_edge(CLK_DRS_CTRL_I) then
      if drs_srclk_out_s2 = '0' and drs_srclk_out_s = '1' then -- rising edge
        drs_stop_cell_0 <= drs_stop_cell_0(8 downto 0) & drs_srout_0;
        drs_stop_cell_1 <= drs_stop_cell_1(8 downto 0) & drs_srout_1;
      end if;
      if store_drs_stop_cells_s = '1' then
        stop_cell_0_out <= drs_stop_cell_0;
        stop_cell_1_out <= drs_stop_cell_1;
      end if;
    end if;
  end process stop_cell_sr;

end behavioral;
