------------------------------------------------------------------------------
-- user_logic.vhd - entity/architecture pair
------------------------------------------------------------------------------
--
-- ***************************************************************************
-- ** Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.            **
-- **                                                                       **
-- ** Xilinx, Inc.                                                          **
-- ** XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"         **
-- ** AS A COURTESY TO YOU, SOLELY FOR USE IN DEVELOPING PROGRAMS AND       **
-- ** SOLUTIONS FOR XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE,        **
-- ** OR INFORMATION AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE,        **
-- ** APPLICATION OR STANDARD, XILINX IS MAKING NO REPRESENTATION           **
-- ** THAT THIS IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,     **
-- ** AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE      **
-- ** FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY              **
-- ** WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE               **
-- ** IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR        **
-- ** REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF       **
-- ** INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS       **
-- ** FOR A PARTICULAR PURPOSE.                                             **
-- **                                                                       **
-- ***************************************************************************
--
------------------------------------------------------------------------------
-- Filename:          user_logic.vhd
-- Version:           1.00.a
-- Description:       User logic.
-- Date:              Thu Aug 14 12:00:34 2014 (by Create and Import Peripheral Wizard)
-- VHDL Standard:     VHDL'93
------------------------------------------------------------------------------
-- Naming Conventions:
--   active low signals:                    "*_n"
--   clock signals:                         "clk", "clk_div#", "clk_#x"
--   reset signals:                         "rst", "rst_n"
--   generics:                              "C_*"
--   user defined types:                    "*_TYPE"
--   state machine next state:              "*_ns"
--   state machine current state:           "*_cs"
--   combinatorial signals:                 "*_com"
--   pipelined or register delay signals:   "*_d#"
--   counter signals:                       "*cnt*"
--   clock enable signals:                  "*_ce"
--   internal version of output port:       "*_i"
--   device pins:                           "*_pin"
--   ports:                                 "- Names begin with Uppercase"
--   processes:                             "*_PROCESS"
--   component instantiations:              "<ENTITY_>I_<#|FUNC>"
------------------------------------------------------------------------------

-- DO NOT EDIT BELOW THIS LINE --------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library proc_common_v3_00_a;
use proc_common_v3_00_a.proc_common_pkg.all;

-- DO NOT EDIT ABOVE THIS LINE --------------------

--USER libraries added here

------------------------------------------------------------------------------
-- Entity section
------------------------------------------------------------------------------
-- Definition of Generics:
--   C_SLV_AWIDTH                 -- Slave interface address bus width
--   C_SLV_DWIDTH                 -- Slave interface data bus width
--   C_NUM_MEM                    -- Number of memory spaces
--
-- Definition of Ports:
--   Bus2IP_Clk                   -- Bus to IP clock
--   Bus2IP_Reset                 -- Bus to IP reset
--   Bus2IP_Addr                  -- Bus to IP address bus
--   Bus2IP_CS                    -- Bus to IP chip select for user logic memory selection
--   Bus2IP_RNW                   -- Bus to IP read/not write
--   Bus2IP_Data                  -- Bus to IP data bus
--   Bus2IP_BE                    -- Bus to IP byte enables
--   IP2Bus_Data                  -- IP to Bus data bus
--   IP2Bus_RdAck                 -- IP to Bus read transfer acknowledgement
--   IP2Bus_WrAck                 -- IP to Bus write transfer acknowledgement
--   IP2Bus_Error                 -- IP to Bus error response
------------------------------------------------------------------------------

entity user_logic is
  generic
  (
    -- ADD USER GENERICS BELOW THIS LINE ---------------
    --USER generics added here
    CGN_NO_CTRL_REG                : integer              := 8;
    CGN_NO_STAT_REG                : integer              := 8;
    CGN_REG_ADDR_WIDTH             : integer              := 8;
    -- ADD USER GENERICS ABOVE THIS LINE ---------------

    -- DO NOT EDIT BELOW THIS LINE ---------------------
    -- Bus protocol parameters, do not add to or delete
    C_SLV_AWIDTH                   : integer              := 32;
    C_SLV_DWIDTH                   : integer              := 32;
    C_NUM_MEM                      : integer              := 2
    -- DO NOT EDIT ABOVE THIS LINE ---------------------
  );
  port
  (
    -- ADD USER PORTS BELOW THIS LINE ------------------
    --USER ports added here
    STAT_REG_D_I                   : in  std_logic_vector( (CGN_NO_STAT_REG*C_SLV_DWIDTH)-1 downto 0 );
    STAT_REG_D_O                   : out std_logic_vector( (CGN_NO_STAT_REG*C_SLV_DWIDTH)-1 downto 0 );
    STAT_REG_READ_O                : out std_logic_vector( CGN_NO_STAT_REG-1 downto 0 );
    CTRL_REG_Q_O                   : out std_logic_vector( (CGN_NO_CTRL_REG*C_SLV_DWIDTH)-1 downto 0 );
    CTRL_REG_READ_O                : out std_logic_vector( CGN_NO_CTRL_REG-1 downto 0 );
    CTRL_BIT_TRIG_O                : out std_logic_vector( (CGN_NO_CTRL_REG*C_SLV_DWIDTH)-1 downto 0 );
    CTRL_BYTE_TRIG_O               : out std_logic_vector( (CGN_NO_CTRL_REG*C_SLV_DWIDTH/8)-1 downto 0 );
    -- ADD USER PORTS ABOVE THIS LINE ------------------

    -- DO NOT EDIT BELOW THIS LINE ---------------------
    -- Bus protocol ports, do not add to or delete
    Bus2IP_Clk                     : in  std_logic;
    Bus2IP_Reset                   : in  std_logic;
    Bus2IP_Addr                    : in  std_logic_vector(0 to C_SLV_AWIDTH-1);
    Bus2IP_CS                      : in  std_logic_vector(0 to C_NUM_MEM-1);
    Bus2IP_RNW                     : in  std_logic;
    Bus2IP_Data                    : in  std_logic_vector(0 to C_SLV_DWIDTH-1);
    Bus2IP_BE                      : in  std_logic_vector(0 to C_SLV_DWIDTH/8-1);
    IP2Bus_Data                    : out std_logic_vector(0 to C_SLV_DWIDTH-1);
    IP2Bus_RdAck                   : out std_logic;
    IP2Bus_WrAck                   : out std_logic;
    IP2Bus_Error                   : out std_logic
    -- DO NOT EDIT ABOVE THIS LINE ---------------------
  );

  attribute MAX_FANOUT : string;
  attribute SIGIS : string;

  attribute SIGIS of Bus2IP_Clk    : signal is "CLK";
  attribute SIGIS of Bus2IP_Reset  : signal is "RST";

end entity user_logic;

------------------------------------------------------------------------------
-- Architecture section
------------------------------------------------------------------------------

architecture IMP of user_logic is

  --USER signal declarations added here, as needed for user logic
  component generic_register_bank is
    generic (
      CGN_NO_CTRL_REG    : integer := 8;
      CGN_NO_STAT_REG    : integer := 8;
      CGN_REG_ADDR_WIDTH : integer := 8;
      CGN_REG_DATA_WIDTH : integer := 32  -- 64,32,16,8
    );
    port (
      UC_ADDR_I        : in  std_logic_vector( CGN_REG_ADDR_WIDTH-1 downto 0 );
      UC_BE_I          : in  std_logic_vector( (CGN_REG_DATA_WIDTH/8)-1 downto 0 );
      UC_RNW_I         : in  std_logic;
      UC_REG_EN_I      : in  std_logic_vector( 1 downto 0 ); -- "01" enables the control regsiter, "10" enables the status register
      UC_DATA_I        : in  std_logic_vector( CGN_REG_DATA_WIDTH-1 downto 0 );
      UC_DATA_O        : out std_logic_vector( CGN_REG_DATA_WIDTH-1 downto 0 );
      STAT_REG_D_I     : in  std_logic_vector( (CGN_NO_STAT_REG*CGN_REG_DATA_WIDTH)-1 downto 0 );
      STAT_REG_D_O     : out std_logic_vector( (CGN_NO_STAT_REG*CGN_REG_DATA_WIDTH)-1 downto 0 );
      STAT_REG_READ_O  : out std_logic_vector( CGN_NO_STAT_REG-1 downto 0 );
      CTRL_REG_Q_O     : out std_logic_vector( (CGN_NO_CTRL_REG*CGN_REG_DATA_WIDTH)-1 downto 0 );
      CTRL_REG_READ_O  : out std_logic_vector( CGN_NO_CTRL_REG-1 downto 0 );
      CTRL_BIT_TRIG_O  : out std_logic_vector( (CGN_NO_CTRL_REG*CGN_REG_DATA_WIDTH)-1 downto 0 );
      CTRL_BYTE_TRIG_O : out std_logic_vector( (CGN_NO_CTRL_REG*CGN_REG_DATA_WIDTH/8)-1 downto 0 );
      RESET_I          : in  std_logic;
      CLK_I            : in  std_logic
    );
  end component;

  ------------------------------------------
  -- Signals for user logic memory space example
  ------------------------------------------
  type BYTE_RAM_TYPE is array (0 to 255) of std_logic_vector(0 to 7);
  type DO_TYPE is array (0 to C_NUM_MEM-1) of std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal mem_data_out                   : DO_TYPE;
  signal mem_address                    : std_logic_vector(0 to 7);
  signal mem_select                     : std_logic_vector(0 to 1);
  signal mem_read_enable                : std_logic;
  signal mem_read_enable_dly1           : std_logic;
  signal mem_read_req                   : std_logic;
  signal mem_ip2bus_data                : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal mem_read_ack_dly1              : std_logic;
  signal mem_read_ack                   : std_logic;
  signal mem_write_ack                  : std_logic;

begin

  --USER logic implementation added here

  ------------------------------------------
  -- Example code to access user logic memory region
  -- 
  -- Note:
  -- The example code presented here is to show you one way of using
  -- the user logic memory space features. The Bus2IP_Addr, Bus2IP_CS,
  -- and Bus2IP_RNW IPIC signals are dedicated to these user logic
  -- memory spaces. Each user logic memory space has its own address
  -- range and is allocated one bit on the Bus2IP_CS signal to indicated
  -- selection of that memory space. Typically these user logic memory
  -- spaces are used to implement memory controller type cores, but it
  -- can also be used in cores that need to access additional address space
  -- (non C_BASEADDR based), s.t. bridges. This code snippet infers
  -- 2 256x32-bit (byte accessible) single-port Block RAM by XST.
  ------------------------------------------
--  mem_select      <= Bus2IP_CS;
  mem_read_enable <= ( Bus2IP_CS(0) or Bus2IP_CS(1) ) and Bus2IP_RNW;
  mem_read_ack    <= mem_read_ack_dly1;
  mem_write_ack   <= ( Bus2IP_CS(0) or Bus2IP_CS(1) ) and not(Bus2IP_RNW);
--  mem_address     <= Bus2IP_Addr(C_SLV_AWIDTH-10 to C_SLV_AWIDTH-3);

  -- implement single clock wide read request
  mem_read_req    <= mem_read_enable and not(mem_read_enable_dly1);
  BRAM_RD_REQ_PROC : process( Bus2IP_Clk ) is
  begin

    if ( Bus2IP_Clk'event and Bus2IP_Clk = '1' ) then
      if ( Bus2IP_Reset = '1' ) then
        mem_read_enable_dly1 <= '0';
      else
        mem_read_enable_dly1 <= mem_read_enable;
      end if;
    end if;

  end process BRAM_RD_REQ_PROC;

  -- this process generates the read acknowledge 1 clock after read enable
  -- is presented to the BRAM block. The BRAM block has a 1 clock delay
  -- from read enable to data out.
  BRAM_RD_ACK_PROC : process( Bus2IP_Clk ) is
  begin

    if ( Bus2IP_Clk'event and Bus2IP_Clk = '1' ) then
      if ( Bus2IP_Reset = '1' ) then
        mem_read_ack_dly1 <= '0';
      else
        mem_read_ack_dly1 <= mem_read_req;
      end if;
    end if;

  end process BRAM_RD_ACK_PROC;


  wd2_register_bank : generic_register_bank
    generic map(
      CGN_NO_CTRL_REG    => CGN_NO_CTRL_REG,
      CGN_NO_STAT_REG    => CGN_NO_STAT_REG,
      CGN_REG_ADDR_WIDTH => CGN_REG_ADDR_WIDTH,
      CGN_REG_DATA_WIDTH => C_SLV_DWIDTH
    )
    port map(
      UC_ADDR_I        => Bus2IP_Addr(C_SLV_AWIDTH-10 to C_SLV_AWIDTH-3),
      UC_BE_I          => Bus2IP_BE,
      UC_RNW_I         => Bus2IP_RNW,
      UC_REG_EN_I      => Bus2IP_CS,
      UC_DATA_I        => Bus2IP_Data,
      UC_DATA_O        => mem_ip2bus_data,
      STAT_REG_D_I     => STAT_REG_D_I,
      STAT_REG_D_O     => STAT_REG_D_O,
      STAT_REG_READ_O  => STAT_REG_READ_O,
      CTRL_REG_Q_O     => CTRL_REG_Q_O,
      CTRL_REG_READ_O  => CTRL_REG_READ_O,
      CTRL_BIT_TRIG_O  => CTRL_BIT_TRIG_O,
      CTRL_BYTE_TRIG_O => CTRL_BYTE_TRIG_O,
      RESET_I          => Bus2IP_Reset,
      CLK_I            => Bus2IP_Clk
    );

  -- implement Block RAM(s)
--  BRAM_GEN : for i in 0 to C_NUM_MEM-1 generate
--    constant NUM_BYTE_LANES : integer := (C_SLV_DWIDTH+7)/8;
--  begin
--
--    BYTE_BRAM_GEN : for byte_index in 0 to NUM_BYTE_LANES-1 generate
--      signal ram           : BYTE_RAM_TYPE;
--      signal write_enable  : std_logic;
--      signal data_in       : std_logic_vector(0 to 7);
--      signal data_out      : std_logic_vector(0 to 7);
--      signal read_address  : std_logic_vector(0 to 7);
--    begin
--
--      write_enable <= not(Bus2IP_RNW) and
--                      Bus2IP_CS(i) and
--                      Bus2IP_BE(byte_index);
--
--      data_in <= Bus2IP_Data(byte_index*8 to byte_index*8+7);
--      mem_data_out(i)(byte_index*8 to byte_index*8+7) <= data_out;
--
--      BYTE_RAM_PROC : process( Bus2IP_Clk ) is
--      begin
--
--        if ( Bus2IP_Clk'event and Bus2IP_Clk = '1' ) then
--          if ( write_enable = '1' ) then
--            ram(CONV_INTEGER(mem_address)) <= data_in;
--          end if;
--          read_address <= mem_address;
--        end if;
--
--      end process BYTE_RAM_PROC;
--
--      data_out <= ram(CONV_INTEGER(read_address));
--
--    end generate BYTE_BRAM_GEN;
--
--  end generate BRAM_GEN;

  -- implement Block RAM read mux
--  MEM_IP2BUS_DATA_PROC : process( mem_data_out, mem_select ) is
--  begin
--
--    case mem_select is
--      when "10" => mem_ip2bus_data <= mem_data_out(0);
--      when "01" => mem_ip2bus_data <= mem_data_out(1);
--      when others => mem_ip2bus_data <= (others => '0');
--    end case;
--
--  end process MEM_IP2BUS_DATA_PROC;

  ------------------------------------------
  -- Example code to drive IP to Bus signals
  ------------------------------------------
  IP2Bus_Data  <= mem_ip2bus_data when mem_read_ack = '1' else
                  (others => '0');

  IP2Bus_WrAck <= mem_write_ack;
  IP2Bus_RdAck <= mem_read_ack;
  IP2Bus_Error <= '0';

end IMP;
