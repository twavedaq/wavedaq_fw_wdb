--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : oserdes_if.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  se32
--  Created :  04.04.2016 14:28:38
--
--  Description :
--    OSERDES interface to transmit data from WDB to DCB.
--
--------------------------------------------------------------------------------

-- UCF Hints:
-- Add the following lines to the .ucf file in order to make build work without
-- timing errors (example):
-- NET "SERDES_CLK_DCB_P_I"   TNM_NET = TNM_NET_SERDES_DCB_CLK;
-- NET "SERDES_CLK_DCB_N_I"   TNM_NET = TNM_NET_SERDES_DCB_CLK;
-- TIMESPEC TS_SERDES_DCB_CLK = PERIOD "TNM_NET_SERDES_DCB_CLK" 100 MHz HIGH 50%;


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.all;

library UNISIM;
use UNISIM.Vcomponents.ALL;

--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------

entity oserdes_if is
  generic
  (
    CGN_SERIAL_DWIDTH   : integer := 1;
    CGN_PARALLEL_DWIDTH : integer := 8;
    CGN_TRAIN_PATTERN   : std_logic_vector(7 downto 0) := X"5A";
    CGN_BIT_ORDER       : string := "MSB_first"; -- "LSB_first"
    CGN_CLKIN_PERIOD    : real := 10.0;
    CGN_GENERATE_CLOCKS : integer := 1;
    CGN_DIFF_IN_CLOCK   : integer := 0;
    CGN_USE_TX_FIFO     : integer := 1;
    CGN_USE_ODELAY      : integer := 0;
    CGN_ODELAY_VALUE    : integer := 0
  );
  port
  (
    -- parallel data input (to FIFO)
    PDATA_I          : in  std_logic_vector(CGN_PARALLEL_DWIDTH-1 downto 0);
    FIFO_WEN_I       : in  std_logic;
    FIFO_FULL_O      : out std_logic;
    FIFO_AFULL_O     : out std_logic;
    TRAIN_EN_I       : in  std_logic;

    -- serial data output
    SDATA_P_O        : out std_logic_vector(CGN_SERIAL_DWIDTH-1 downto 0);
    SDATA_N_O        : out std_logic_vector(CGN_SERIAL_DWIDTH-1 downto 0);

    -- clocking and resets
    PKG_CLK_I        : in  std_logic;
    SERDES_CLK_P_I   : in  std_logic;
    SERDES_CLK_N_I   : in  std_logic;
    BIT_CLK_I        : in  std_logic;
    DIV_CLK_I        : in  std_logic;
    EXT_PLL_LOCKED_I : in  std_logic;
    BIT_CLK_FB_O     : out std_logic;
    LOCKED_O         : out std_logic;
    PLL_RESET_I      : in  std_logic;
    RESET_I          : in  std_logic
  );
end oserdes_if;


--------------------------------------------------------------------------------
-- Architecture section
--------------------------------------------------------------------------------

architecture behavior of oserdes_if is

  constant C_NUM_SERIAL_BITS : integer   := CGN_PARALLEL_DWIDTH/CGN_SERIAL_DWIDTH;
  constant clk_en            : std_logic := '1';

  signal clkin               : std_logic;
  signal fb_clk              : std_logic;
  signal bit_clk             : std_logic;
  signal serial_clk          : std_logic;
  signal parallel_clk        : std_logic;
  signal parallel_clk_buf    : std_logic;
  signal pll_locked          : std_logic;
  signal bufpll_locked       : std_logic;
  signal reset               : std_logic;

  signal fifo_ren            : std_logic;
  signal fifo_dout           : std_logic_vector(CGN_PARALLEL_DWIDTH-1 downto 0);
  signal fifo_empty          : std_logic;
  signal fifo_empty_s        : std_logic;
  signal fifo_empty_s_dly    : std_logic;

  -- Before the buffer
  signal pdata               : std_logic_vector(CGN_PARALLEL_DWIDTH-1 downto 0);
  signal sdata               : std_logic_vector(CGN_SERIAL_DWIDTH-1 downto 0);
  signal sdata_dly           : std_logic_vector(CGN_SERIAL_DWIDTH-1 downto 0);

  type serdarr is array (CGN_SERIAL_DWIDTH-1 downto 0) of std_logic_vector(C_NUM_SERIAL_BITS-1 downto 0);
  signal oserdes_d           : serdarr := ((others=>(others=>'0')));
  signal serdesstrobe        : std_logic;
  signal ocascade_ms_d       : std_logic_vector(CGN_PARALLEL_DWIDTH-1 downto 0);
  signal ocascade_ms_t       : std_logic_vector(CGN_PARALLEL_DWIDTH-1 downto 0);
  signal ocascade_sm_d       : std_logic_vector(CGN_PARALLEL_DWIDTH-1 downto 0);
  signal ocascade_sm_t       : std_logic_vector(CGN_PARALLEL_DWIDTH-1 downto 0);

begin

  cdc_sync_sys_to_oserdes : entity psi_3205_v1_00_a.cdc_sync
  generic map
  (
    CGN_DATA_WIDTH           =>  1,
    CGN_USE_INPUT_REG_A      =>  0,
    CGN_USE_OUTPUT_REG_A     =>  0,
    CGN_USE_GRAY_CONVERSION  =>  0,
    CGN_NUM_SYNC_REGS_B      =>  2,
    CGN_NUM_OUTPUT_REGS_B    =>  0,
    CGN_GRAY_TO_BIN_STYLE    =>  0
  )
  PORT MAP
  (
    CLK_A_I    => PKG_CLK_I, --SYS_CLK_I,
    CLK_B_I    => parallel_clk_buf,
    PORT_A_I(0) => RESET_I,
    PORT_B_O(0) => reset
  );

  TX_FIFO : if CGN_USE_TX_FIFO = 1 generate
    tx_buffer_fifo : entity psi_3205_v1_00_a.fifo_gen_async
    generic map
    (
      -- fpga family and tool version
      C_FAMILY            => "spartan6",
      C_ISE_12_4          => false,
      C_ISE_14_7          => true,
      -- fifo size and type
      C_DATA_WIDTH        => CGN_PARALLEL_DWIDTH,
      C_FIFO_DEPTH        => 2**4,
      C_MEMORY_TYPE       => 0,     -- 0 = distributed RAM, 1 = BRAM
      C_USE_EMBEDDED_REG  => 0,     -- Valid only for BRAM based FIFO, otherwise needs to be set to 0
      C_PRELOAD_REGS      => 1,     -- 1 = first word fall through
      C_PRELOAD_LATENCY   => 0,     -- 0 = first word fall through,  needs to be set 2 when C_USE_EMBEDDED_REG = 1
      -- write port
      C_HAS_WR_COUNT      => 0,
      C_WR_COUNT_WIDTH    => 4,
      C_HAS_ALMOST_FULL   => 1,
      C_HAS_PROG_FULL     => 0,     -- 0 or 1, 1 for constant prog full
      C_PROG_FULL_OFFSET  => 3,     -- Setting determines the difference between FULL and PROG_FULL conditions
      C_HAS_WR_ACK        => 0,
      C_WR_ACK_LOW        => 0,
      C_HAS_WR_ERR        => 0,
      C_WR_ERR_LOW        => 0,
      -- read port
      C_HAS_RD_COUNT      => 0,
      C_RD_COUNT_WIDTH    => 4,
      C_HAS_ALMOST_EMPTY  => 0,
      C_HAS_PROG_EMPTY    => 0,     -- 0 or 1, 1 for constant prog empty
      C_PROG_EMPTY_OFFSET => 2,     -- Setting determines the difference between EMPTY and PROG_EMPTY conditions
      C_HAS_RD_ACK        => 0,
      C_RD_ACK_LOW        => 0,
      C_HAS_RD_ERR        => 0,
      C_RD_ERR_LOW        => 0
    )
    port map
    (
      -- Common
      RESET_ASYNC_I     => RESET_I,
      -- Write Port
      WR_CLK_I          => PKG_CLK_I,
      WR_EN_I           => FIFO_WEN_I,
      WR_DATA_I         => PDATA_I,
      WR_FULL_O         => FIFO_FULL_O,
      WR_ALMOST_FULL_O  => FIFO_AFULL_O,
      WR_PROG_FULL_O    => open,
      WR_COUNT_O        => open,
      WR_ACK_O          => open,
      WR_ERR_O          => open,
      -- Read Port
      RD_CLK_I          => parallel_clk_buf,
      RD_EN_I           => fifo_ren,
      RD_DATA_O         => fifo_dout,
      RD_EMPTY_O        => fifo_empty,
      RD_ALMOST_EMPTY_O => open,
      RD_PROG_EMPTY_O   => open,
      RD_COUNT_O        => open,
      RD_ACK_O          => open,
      RD_ERR_O          => open
    );
  end generate TX_FIFO;

  NO_TX_FIFO : if CGN_USE_TX_FIFO = 0 generate
    FIFO_FULL_O  <= '0';
    FIFO_AFULL_O <= '0';
    fifo_dout    <= PDATA_I;
    fifo_empty   <= TRAIN_EN_I;
  end generate NO_TX_FIFO;

  CLOCK_GENERATOR : if CGN_GENERATE_CLOCKS = 1 generate

    DIFF_CLK_IN_BUF : if CGN_DIFF_IN_CLOCK = 1 generate
      clkin_in_buf : IBUFGDS
      port map (
        O  => clkin,
        I  => SERDES_CLK_P_I,
        IB => SERDES_CLK_N_I
      );
    end generate DIFF_CLK_IN_BUF;

    SE_CLK_IN : if CGN_DIFF_IN_CLOCK = 0 generate
      clkin <= SERDES_CLK_P_I;
    end generate SE_CLK_IN;

   -- set up the fabric PLL_BASE to drive the BUFPLL
   pll_base_inst : PLL_BASE
    generic map (
      BANDWIDTH             => "OPTIMIZED",
      CLK_FEEDBACK          => "CLKFBOUT", -- "CLKOUT0", -- "CLKFBOUT",
      COMPENSATION          => "SYSTEM_SYNCHRONOUS",
      DIVCLK_DIVIDE         => 1,
      CLKFBOUT_MULT         => C_NUM_SERIAL_BITS,
      CLKFBOUT_PHASE        => 0.000,
      CLKOUT0_DIVIDE        => 1,
      CLKOUT0_PHASE         => 0.000,
      CLKOUT0_DUTY_CYCLE    => 0.500,
      CLKOUT1_DIVIDE        => C_NUM_SERIAL_BITS,
      CLKOUT1_PHASE         => 0.000,
      CLKOUT1_DUTY_CYCLE    => 0.500,
      CLKIN_PERIOD          => CGN_CLKIN_PERIOD,
      REF_JITTER            => 0.010
    )
   port map (
     -- Output clocks
      CLKFBOUT              => open,
      CLKOUT0               => serial_clk,
      CLKOUT1               => parallel_clk,
      CLKOUT2               => open,
      CLKOUT3               => open,
      CLKOUT4               => open,
      CLKOUT5               => open,
      -- Status and control signals
      LOCKED                => pll_locked,
      RST                   => PLL_RESET_I,
      -- Input clock control
      CLKFBIN               => parallel_clk_buf,
      CLKIN                 => clkin
    );
--    port map (
--      -- Output clocks
--       CLKFBOUT              => fb_clk,
--       CLKOUT0               => serial_clk,
--       CLKOUT1               => parallel_clk,
--       CLKOUT2               => open,
--       CLKOUT3               => open,
--       CLKOUT4               => open,
--       CLKOUT5               => open,
--       -- Status and control signals
--       LOCKED                => pll_locked,
--       RST                   => PLL_RESET_I,
--       -- Input clock control
--       CLKFBIN               => fb_clk,
--       CLKIN                 => clkin
--     );

   pclk_buf : BUFG
    port map (
      O => parallel_clk_buf,
      I => parallel_clk
    );
  end generate CLOCK_GENERATOR;

  EXTERNAL_CLOCKS : if CGN_GENERATE_CLOCKS = 0 generate
    parallel_clk_buf <= DIV_CLK_I;
    serial_clk       <= BIT_CLK_I;
    pll_locked       <= EXT_PLL_LOCKED_I;
  end generate EXTERNAL_CLOCKS;

  -- Create the clock logic
   bufpll_inst : BUFPLL
    generic map (
    DIVIDE       => 8
  )
    port map (
      IOCLK        => bit_clk,
      LOCK         => bufpll_locked,
      SERDESSTROBE => serdesstrobe,
      GCLK         => parallel_clk_buf,  -- GCLK pin must be driven by BUFG
      LOCKED       => pll_locked,
      PLLIN        => serial_clk
  );
  LOCKED_O     <= bufpll_locked;
  BIT_CLK_FB_O <= bit_clk;

  process(parallel_clk_buf)
  begin
    if rising_edge(parallel_clk_buf) then
      fifo_empty_s      <= fifo_empty;
    end if;
  end process;

  fifo_ren          <= not fifo_empty;

  -- We have multiple bits- step over every bit, instantiating the required elements
  pins: for pin_count in 0 to CGN_SERIAL_DWIDTH-1 generate
  begin

    -- Instantiate the buffers
    ----------------------------------
    -- Instantiate a buffer for every bit of the data bus
    obufds_inst : OBUFDS
    generic map (
      IOSTANDARD => "LVDS_25")
    port map (
      O          => SDATA_P_O(pin_count),
      OB         => SDATA_N_O(pin_count),
      I          => sdata_dly(pin_count));

    ODELAY_SERDES : if CGN_USE_ODELAY = 1 generate
      -- Instantiate the delay primitive
      -----------------------------------
      iodelay2_oserdes : IODELAY2
      generic map (
        DATA_RATE              => "SDR",
        COUNTER_WRAPAROUND     => "STAY_AT_LIMIT",
        DELAY_SRC              => "ODATAIN",
        SERDES_MODE            => "NONE",
        ODELAY_VALUE           => CGN_ODELAY_VALUE,
        SIM_TAPDELAY_VALUE     => 0)
      port map (
        IDATAIN                => '0',
        DATAOUT                => open,
        T                      => '0',
        DATAOUT2               => open,
        DOUT                   => sdata_dly(pin_count),
        ODATAIN                => sdata(pin_count),
        TOUT                   => open,
        IOCLK0                => '0',
        IOCLK1                => '0',
        CLK                   => '0',
        CAL                   => '0',
        INC                   => '0',
        CE                    => '0',
        BUSY                  => open,
        RST                   => '0'
      );
    end generate ODELAY_SERDES;

    NO_ODELAY_SERDES : if CGN_USE_ODELAY = 0 generate
      sdata_dly(pin_count) <= sdata(pin_count);
    end generate NO_ODELAY_SERDES;

    -- Instantiate the serdes primitive
    ----------------------------------
    -- declare the oserdes
    -- for MSB first transmission, MSB-LSB
    -- have to be swapped for the data and
    -- for the training patterns.

    MSB_first_gen : if CGN_BIT_ORDER = "MSB_first" generate

      oserdes2_master : OSERDES2
        generic map (
          DATA_RATE_OQ   => "SDR",
          DATA_RATE_OT   => "SDR",
          TRAIN_PATTERN  => 5, -- MSB first
          DATA_WIDTH     => 8,
          SERDES_MODE    => "MASTER",
          OUTPUT_MODE    => "SINGLE_ENDED")
        port map (
          D1         => oserdes_d(pin_count)(3), -- MSB first
          D2         => oserdes_d(pin_count)(2), -- MSB first
          D3         => oserdes_d(pin_count)(1), -- MSB first
          D4         => oserdes_d(pin_count)(0), -- MSB first
          T1         => '0',
          T2         => '0',
          T3         => '0',
          T4         => '0',
          SHIFTIN1   => '1',
          SHIFTIN2   => '1',
          SHIFTIN3   => ocascade_sm_d(pin_count),
          SHIFTIN4   => ocascade_sm_t(pin_count),
          SHIFTOUT1  => ocascade_ms_d(pin_count),
          SHIFTOUT2  => ocascade_ms_t(pin_count),
          SHIFTOUT3  => open,
          SHIFTOUT4  => open,
          TRAIN      => '0',
          OCE        => clk_en,
          CLK0       => bit_clk,
          CLK1       => '0',
          CLKDIV     => parallel_clk_buf,
          OQ         => sdata(pin_count),
          TQ         => open,
          IOCE       => serdesstrobe,
          TCE        => clk_en,
          RST        => reset);

      oserdes2_slave : OSERDES2
        generic map (
          DATA_RATE_OQ   => "SDR",
          DATA_RATE_OT   => "SDR",
          DATA_WIDTH     => 8,
          SERDES_MODE    => "SLAVE",
          TRAIN_PATTERN  => 7, -- MSB first | 14, -- LSB first
          OUTPUT_MODE    => "SINGLE_ENDED")
        port map (
          D1         => oserdes_d(pin_count)(7), -- MSB first
          D2         => oserdes_d(pin_count)(6), -- MSB first
          D3         => oserdes_d(pin_count)(5), -- MSB first
          D4         => oserdes_d(pin_count)(4), -- MSB first
          T1         => '0',
          T2         => '0',
          T3         => '0',
          T4         => '0',
          SHIFTIN1   => ocascade_ms_d(pin_count),
          SHIFTIN2   => ocascade_ms_t(pin_count),
          SHIFTIN3   => '1',
          SHIFTIN4   => '1',
          SHIFTOUT1  => open,
          SHIFTOUT2  => open,
          SHIFTOUT3  => ocascade_sm_d(pin_count),
          SHIFTOUT4  => ocascade_sm_t(pin_count),
          TRAIN      => '0',
          OCE        => clk_en,
          CLK0       => bit_clk,
          CLK1       => '0',
          CLKDIV     => parallel_clk_buf,
          OQ         => open,
          TQ         => open,
          IOCE       => serdesstrobe,
          TCE        => clk_en,
          RST        => reset);
    end generate;

    LSB_first_gen : if CGN_BIT_ORDER = "LSB_first" generate

      oserdes2_master : OSERDES2
        generic map (
          DATA_RATE_OQ   => "SDR",
          DATA_RATE_OT   => "SDR",
          TRAIN_PATTERN  => 10, -- LSB first
          DATA_WIDTH     => 8,
          SERDES_MODE    => "MASTER",
          OUTPUT_MODE    => "SINGLE_ENDED")
        port map (
          D1         => oserdes_d(pin_count)(4), -- LSB first
          D2         => oserdes_d(pin_count)(5), -- LSB first
          D3         => oserdes_d(pin_count)(6), -- LSB first
          D4         => oserdes_d(pin_count)(7), -- LSB first
          T1         => '0',
          T2         => '0',
          T3         => '0',
          T4         => '0',
          SHIFTIN1   => '1',
          SHIFTIN2   => '1',
          SHIFTIN3   => ocascade_sm_d(pin_count),
          SHIFTIN4   => ocascade_sm_t(pin_count),
          SHIFTOUT1  => ocascade_ms_d(pin_count),
          SHIFTOUT2  => ocascade_ms_t(pin_count),
          SHIFTOUT3  => open,
          SHIFTOUT4  => open,
          TRAIN      => '0',
          OCE        => clk_en,
          CLK0       => bit_clk,
          CLK1       => '0',
          CLKDIV     => parallel_clk_buf,
          OQ         => sdata(pin_count),
          TQ         => open,
          IOCE       => serdesstrobe,
          TCE        => clk_en,
          RST        => reset);

      oserdes2_slave : OSERDES2
        generic map (
          DATA_RATE_OQ   => "SDR",
          DATA_RATE_OT   => "SDR",
          DATA_WIDTH     => 8,
          SERDES_MODE    => "SLAVE",
          TRAIN_PATTERN  => 14, -- LSB first
          OUTPUT_MODE    => "SINGLE_ENDED")
        port map (
          D1         => oserdes_d(pin_count)(0), -- LSB first
          D2         => oserdes_d(pin_count)(1), -- LSB first
          D3         => oserdes_d(pin_count)(2), -- LSB first
          D4         => oserdes_d(pin_count)(3), -- LSB first
          T1         => '0',
          T2         => '0',
          T3         => '0',
          T4         => '0',
          SHIFTIN1   => ocascade_ms_d(pin_count),
          SHIFTIN2   => ocascade_ms_t(pin_count),
          SHIFTIN3   => '1',
          SHIFTIN4   => '1',
          SHIFTOUT1  => open,
          SHIFTOUT2  => open,
          SHIFTOUT3  => ocascade_sm_d(pin_count),
          SHIFTOUT4  => ocascade_sm_t(pin_count),
          TRAIN      => '0',
          OCE        => clk_en,
          CLK0       => bit_clk,
          CLK1       => '0',
          CLKDIV     => parallel_clk_buf,
          OQ         => open,
          TQ         => open,
          IOCE       => serdesstrobe,
          TCE        => clk_en,
          RST        => reset);
    end generate;

     -- Concatenate the serdes outputs together. Keep the timesliced
     --   bits together, and placing the earliest bits on the right
     --   ie, if data comes in 0, 1, 2, 3, 4, 5, 6, 7, ...
     --       the output will be 3210, 7654, ...
     -------------------------------------------------------------


    out_slices: for slice_count in 0 to CGN_SERIAL_DWIDTH-1 generate begin
        oserdes_d(slice_count) <= fifo_dout(C_NUM_SERIAL_BITS*(slice_count+1)-1 downto C_NUM_SERIAL_BITS*slice_count)
                                  when fifo_empty = '0' else
                                  CGN_TRAIN_PATTERN(C_NUM_SERIAL_BITS-1 downto 0);
     end generate out_slices;

  end generate pins;
END behavior;
