--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : ll_mux_2_to_1.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  tg32
--  Created :  2014.07.30
--
--  Description :  
--    local link multiplexer and arbiter
--    optional timeout  after 2**CGN_TIMEOUT_CNTR_BIT clk cycles if CGN_TIMEOUT_CNTR_BIT > 0. 
--    If timeout occurs 1 cycle with eof_n = '0' and src_rdy_n = '0' is send without checking
--    dst_rdy_n. If necessary add timeout signal to destination.
--
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;


--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------

entity ll_mux_2_to_1 is
  generic
  (
    CGN_LL_DATA_WIDTH     : integer := 32;
    CGN_LL_REM_WIDTH      : integer := 2;
    CGN_TIMEOUT_CNTR_BIT  : integer := 14;
    CGN_SIDE_DATA_WIDTH   : integer := 1
  );
  port
  (
    -- Ports
    CLK_I               : in  std_logic;
    RESET_I             : in  std_logic;

    -- Local Link A
    LL_A_I_DATA_I       : in  std_logic_vector(CGN_LL_DATA_WIDTH-1 downto 0);
    LL_A_I_SOF_N_I      : in  std_logic;
    LL_A_I_EOF_N_I      : in  std_logic;
    LL_A_I_REM_I        : in  std_logic_vector(CGN_LL_REM_WIDTH-1 downto 0) := (others => '0');
    LL_A_I_SRC_RDY_N_I  : in  std_logic;
    LL_A_I_DST_RDY_N_O  : out std_logic;
    LL_A_I_SIDE_DATA_I  : in  std_logic_vector(CGN_SIDE_DATA_WIDTH-1 downto 0) := (others => '0');

    -- Local Link B
    LL_B_I_DATA_I       : in  std_logic_vector(CGN_LL_DATA_WIDTH-1 downto 0);
    LL_B_I_SOF_N_I      : in  std_logic;
    LL_B_I_EOF_N_I      : in  std_logic;
    LL_B_I_REM_I        : in  std_logic_vector(CGN_LL_REM_WIDTH-1 downto 0) := (others => '0');
    LL_B_I_SRC_RDY_N_I  : in  std_logic;
    LL_B_I_DST_RDY_N_O  : out std_logic;
    LL_B_I_SIDE_DATA_I  : in  std_logic_vector(CGN_SIDE_DATA_WIDTH-1 downto 0) := (others => '0');

    -- Local Link Q
    LL_Q_O_Data_O       : out std_logic_vector(CGN_LL_DATA_WIDTH-1 downto 0);
    LL_Q_O_SOF_N_O      : out std_logic;
    LL_Q_O_EOF_N_O      : out std_logic;
    LL_Q_O_REM_O        : out std_logic_vector(CGN_LL_REM_WIDTH-1 downto 0);
    LL_Q_O_SRC_RDY_N_O  : out std_logic;
    LL_Q_O_DST_RDY_N_I  : in  std_logic;
    LL_Q_O_SIDE_DATA_O  : out std_logic_vector(CGN_SIDE_DATA_WIDTH-1 downto 0)
  );
end ll_mux_2_to_1;


--------------------------------------------------------------------------------
-- Architecture section
--------------------------------------------------------------------------------

architecture behavior of ll_mux_2_to_1 is

  signal  input_rdy_a    :   std_logic;
  signal  input_rdy_b    :   std_logic;

  signal  frame_end_a    :   std_logic;
  signal  frame_end_b    :   std_logic;

  signal  start_a        :   std_logic;
  signal  start_b        :   std_logic;

  signal  in_frame_a     :   std_logic;
  signal  in_frame_b     :   std_logic;
  signal  not_in_frame   :   std_logic;

  signal  last_a         :   std_logic;
  signal  last_b         :   std_logic;

  signal  timeout        :   std_logic;


begin

-- Mux
  LL_Q_O_Data_O       <=  LL_A_I_DATA_I        when in_frame_a = '1' else
                          LL_B_I_DATA_I;

  LL_Q_O_SOF_N_O      <=  LL_A_I_SOF_N_I       when in_frame_a = '1' else
                          LL_B_I_SOF_N_I;

  LL_Q_O_EOF_N_O      <=  '0'                  when timeout = '1'    else
                          LL_A_I_EOF_N_I       when in_frame_a = '1' else
                          LL_B_I_EOF_N_I;

  LL_Q_O_REM_O        <=  LL_A_I_REM_I         when in_frame_a = '1' else
                          LL_B_I_REM_I;

  LL_Q_O_SRC_RDY_N_O  <=  '0'                  when timeout = '1'    else
                          LL_A_I_SRC_RDY_N_I   when in_frame_a = '1' else
                          LL_B_I_SRC_RDY_N_I   when in_frame_b = '1' else
                          '1';

  LL_Q_O_SIDE_DATA_O  <=  LL_A_I_SIDE_DATA_I   when in_frame_a = '1' else
                          LL_B_I_SIDE_DATA_I;

  LL_A_I_DST_RDY_N_O  <=  LL_Q_O_DST_RDY_N_I   when in_frame_a = '1' else input_rdy_a; -- read til sof a ...
  LL_B_I_DST_RDY_N_O  <=  LL_Q_O_DST_RDY_N_I   when in_frame_b = '1' else input_rdy_b; -- read til sof b ...

--------------------------------------------------------------------------------

  input_rdy_a <= '1' when (LL_A_I_SRC_RDY_N_I = '0') and (LL_A_I_SOF_N_I = '0') else '0';
  input_rdy_b <= '1' when (LL_B_I_SRC_RDY_N_I = '0') and (LL_B_I_SOF_N_I = '0') else '0';

  frame_end_a <= '1' when (LL_A_I_SRC_RDY_N_I = '0') and (LL_A_I_EOF_N_I = '0') else '0';
  frame_end_b <= '1' when (LL_B_I_SRC_RDY_N_I = '0') and (LL_B_I_EOF_N_I = '0') else '0';

  start_a <= not_in_frame and input_rdy_a and (last_b or (not input_rdy_b));
  start_b <= not_in_frame and input_rdy_b and (last_a or (not input_rdy_a));

  not_in_frame <= (not in_frame_a) and (not in_frame_b);
  last_b       <= (not last_a);


  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if (RESET_I = '1') then
        last_a       <= '0';
        in_frame_a   <= '0';
        in_frame_b   <= '0';
      else
        if (start_a = '1') then
          last_a       <= '1';
          in_frame_a   <= '1';
        elsif (start_b = '1') then
          last_a       <= '0';
          in_frame_b   <= '1';
        end if;

        if ((in_frame_a = '1') and (frame_end_a = '1') and (LL_Q_O_DST_RDY_N_I = '0')) or (timeout = '1') then
          in_frame_a   <= '0';
        end if;

        if ((in_frame_b = '1') and (frame_end_b = '1') and (LL_Q_O_DST_RDY_N_I = '0')) or (timeout = '1') then
          in_frame_b   <= '0';
        end if;

      end if;
    end if;
  end process;


  gen_no_timeout : if (CGN_TIMEOUT_CNTR_BIT = 0) generate
  begin
    timeout <= '0';
  end generate;


  gen_timeout : if (CGN_TIMEOUT_CNTR_BIT > 0) generate
    signal  timeout_cntr   :   std_logic_vector(CGN_TIMEOUT_CNTR_BIT downto 0);
  begin
    process(CLK_I)
    begin
      if rising_edge(CLK_I) then
        if (RESET_I = '1') or (not_in_frame = '1') or (timeout = '1') then
          timeout_cntr <= (others => '0');
        elsif (LL_Q_O_DST_RDY_N_I = '0') then
          timeout_cntr <= timeout_cntr + 1;
        end if;
      end if;
    end process;

    timeout <= timeout_cntr(CGN_TIMEOUT_CNTR_BIT);

  end generate;



-------------------------------------------------------------------------------
END behavior;

