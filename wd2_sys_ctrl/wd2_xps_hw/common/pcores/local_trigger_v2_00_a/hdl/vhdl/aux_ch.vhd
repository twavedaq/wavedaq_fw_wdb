---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Local Trigger Unit
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid (Author of generation script)
--  Created :  12.03.2018 09:06:08
--
--  Description :  Local WD2 trigger unit. Reduces and forwards the comparator signals
--                 from the front end to the OSERDES lines going to the backplane.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.STD_LOGIC_UNSIGNED.ALL;
--use IEEE.STD_LOGIC_MISC.ALL;

library local_trigger_v2_00_a;
use local_trigger_v2_00_a.edge_detector;
use local_trigger_v2_00_a.trigger_iserdes_channel;
use local_trigger_v2_00_a.dual_input_pulse_shaper;
use local_trigger_v2_00_a.counter;

Library UNISIM;
use UNISIM.vcomponents.all;

entity aux_ch is
  port (
    -- Trigger Signals
    MCX_TRIGGER_I             : in  std_logic;
    BACKPLANE_TRIGGER_I       : in  std_logic;
    EXT_CLK_I                 : in  std_logic;
    -- External Trigger Signals Select
    BACKPLANE_PLUGGED_I       : in  std_logic;
    -- Detected Edges (Shaped) Output
    EXT_SHAPER_OUT_O          : out std_logic;
    -- Trigger Configuration
    CFG_POLARITY_EXT_I        : in  std_logic; -- 0 = rising, 1 = falling
    CFG_TRIG_PULSE_LENGTH_I   : in  std_logic_vector(4 downto 0);
    -- SERDES clocking
    SERDES_DIV_CLK_I          : in  std_logic;
    SERDES_BIT_CLK_MCX_I      : in  std_logic;
    SERDES_BIT_CLK_BPL_I      : in  std_logic;
    SERDES_BIT_CLK_ECK_I      : in  std_logic;
    SERDES_STROBE_MCX_I       : in  std_logic;
    SERDES_STROBE_BPL_I       : in  std_logic;
    SERDES_STROBE_ECK_I       : in  std_logic;
    -- Trigger Counter
    RST_COUNTER_I             : in  std_logic;
    EXT_COUNT_O               : out std_logic_vector(63 downto 0);
    EXT_CLK_COUNT_O           : out std_logic_vector(63 downto 0);
    -- Clock and Reset
    RESET_I                   : in  std_logic;
    CLK_I                     : in  std_logic
  );
end aux_ch;

architecture behavioral of aux_ch is

  constant C_SERDES_PWIDTH    : integer := 8;
  constant C_EDGE_COUNT_WIDTH : integer := 3;

  signal serdes_data_mcx      : std_logic_vector(C_SERDES_PWIDTH-1 downto 0)  := (others=>'0');
  signal serdes_data_bpl      : std_logic_vector(C_SERDES_PWIDTH-1 downto 0)  := (others=>'0');
  signal serdes_data_ext_clk  : std_logic_vector(C_SERDES_PWIDTH-1 downto 0)  := (others=>'0');
  signal edge_detect_in_ext   : std_logic_vector(C_SERDES_PWIDTH-1 downto 0)  := (others=>'0');
  signal edge_detected        : std_logic := '0';
  signal edge_count_ext       : std_logic_vector(C_EDGE_COUNT_WIDTH-1 downto 0) := (others=>'0');
  signal edge_count_ext_clk   : std_logic_vector(C_EDGE_COUNT_WIDTH-1 downto 0) := (others=>'0');

begin

  mcx_iserdes_inst : entity local_trigger_v2_00_a.trigger_iserdes_channel
  generic map (
    CGN_RATIO           => C_SERDES_PWIDTH
  )
  port map (
    SERIAL_TRIGGER_I    => MCX_TRIGGER_I,
    PARALLEL_TRIGGER_O  => serdes_data_mcx,
    BIT_CLK_I           => SERDES_BIT_CLK_MCX_I,
    DIV_CLK_I           => SERDES_DIV_CLK_I,
    STROBE_I            => SERDES_STROBE_MCX_I,
    RESET_I             => RESET_I
  );

  bpl_iserdes_inst : entity local_trigger_v2_00_a.trigger_iserdes_channel
  generic map (
    CGN_RATIO           => C_SERDES_PWIDTH
  )
  port map (
    SERIAL_TRIGGER_I    => BACKPLANE_TRIGGER_I,
    PARALLEL_TRIGGER_O  => serdes_data_bpl,
    BIT_CLK_I           => SERDES_BIT_CLK_BPL_I,
    DIV_CLK_I           => SERDES_DIV_CLK_I,
    STROBE_I            => SERDES_STROBE_BPL_I,
    RESET_I             => RESET_I
  );

  ext_clk_iserdes_inst : entity local_trigger_v2_00_a.trigger_iserdes_channel
  generic map (
    CGN_RATIO           => C_SERDES_PWIDTH
  )
  port map (
    SERIAL_TRIGGER_I    => EXT_CLK_I,
    PARALLEL_TRIGGER_O  => serdes_data_ext_clk,
    BIT_CLK_I           => SERDES_BIT_CLK_ECK_I,
    DIV_CLK_I           => SERDES_DIV_CLK_I,
    STROBE_I            => SERDES_STROBE_ECK_I,
    RESET_I             => RESET_I
  );

  edge_detect_in_ext <= serdes_data_bpl when BACKPLANE_PLUGGED_I = '1' else serdes_data_mcx;

  ext_edge_detector_inst : entity local_trigger_v2_00_a.edge_detector
  generic map (
    CGN_DATA_WIDTH    => C_SERDES_PWIDTH,
    CGN_COUNT_WIDTH   => C_EDGE_COUNT_WIDTH
  )
  port map (
    DATA_I            => edge_detect_in_ext,
    POLARITY_I        => CFG_POLARITY_EXT_I,
    EDGE_DETECT_O     => edge_detected,
    EDGE_COUNT_O      => edge_count_ext,
    CLK_I             => CLK_I
  );

  ext_clk_edge_detector_inst : entity local_trigger_v2_00_a.edge_detector
  generic map (
    CGN_DATA_WIDTH  => C_SERDES_PWIDTH,
    CGN_COUNT_WIDTH => C_EDGE_COUNT_WIDTH
  )
  port map (
    DATA_I        => serdes_data_ext_clk,
    POLARITY_I    => '0',
    EDGE_DETECT_O => open,
    EDGE_COUNT_O  => edge_count_ext_clk,
    CLK_I         => CLK_I
  );

  ext_pulse_shaper_inst : entity local_trigger_v2_00_a.dual_input_pulse_shaper
  generic map (
    CGN_LENGTH_BITS => 5
  )
  port map (
    PULSE_I         => edge_detected,
    POLARITY_I      => CFG_POLARITY_EXT_I,
    SERDES_LSB_I    => edge_detect_in_ext(0),
    PULSE_LENGTH_I  => CFG_TRIG_PULSE_LENGTH_I,
    EDGE_O          => EXT_SHAPER_OUT_O,
    CLK_I           => CLK_I
  );

  ------------------------------
  -- Trigger Counter (Scaler) --
  ------------------------------

--  ext_trig_counter_inst : entity local_trigger_v2_00_a.counter
  ext_trig_counter_inst : entity local_trigger_v2_00_a.dsp_counter
  port map (
    CLK_I       => CLK_I,
    RST_I       => RST_COUNTER_I,
    INCREMENT_I => edge_count_ext,
    COUNT_O     => EXT_COUNT_O
  );

--  ext_clk_counter_inst : entity local_trigger_v2_00_a.counter
  ext_clk_counter_inst : entity local_trigger_v2_00_a.dsp_counter
  port map (
    CLK_I       => CLK_I,
    RST_I       => RST_COUNTER_I,
    INCREMENT_I => edge_count_ext_clk,
    COUNT_O     => EXT_CLK_COUNT_O
  );

end architecture behavioral;
