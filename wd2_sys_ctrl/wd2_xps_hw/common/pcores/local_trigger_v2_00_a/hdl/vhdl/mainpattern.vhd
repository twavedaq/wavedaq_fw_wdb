
----------------------------------------------------------------------------------
-- Company:
-- Engineer: Marco Francesconi
--
-- Create Date:    16:54:27 05/26/2016
-- Design Name:
-- Module Name:    mainpattern_vhd - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description: here there is a pattern-based trigger,
--				mainly intended for BGO Cosmic RUN
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_MISC.OR_REDUCE;
use IEEE.STD_LOGIC_MISC.AND_REDUCE;

library local_trigger_v2_00_a;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mainpattern is
  Generic
  (
    CGN_NR_OF_CH       : integer := 16;
    CGN_NR_OF_PATTERNS : integer := 32
  );
  Port
  (
    SRC_EN_PATTERNS_I     : in  std_logic_vector(CGN_NR_OF_PATTERNS*CGN_NR_OF_CH-1 downto 0); -- patterns_i (array of 32 vectors of 32 bits)
    STATE_PATTERNS_I      : in  std_logic_vector(CGN_NR_OF_PATTERNS*CGN_NR_OF_CH-1 downto 0); -- patterns_i (array of 32 vectors of 32 bits)
    PATTERN_EN_I          : in  std_logic_vector(CGN_NR_OF_PATTERNS-1 downto 0); -- pattern bit enable (equivalent to set pattern(i)=X"00000000")
    LEAD_TRAIL_EDGE_SEL_I : in  std_logic;
    EDGE_I                : in  std_logic_vector(CGN_NR_OF_CH-1 downto 0); -- comparator input
    TRIGGER_O             : out std_logic; --trigger output
    PATTERNS_COUNT_O      : out std_logic_vector(CGN_NR_OF_PATTERNS*64-1 downto 0); -- patterns_count_o (array of 19 vectors of 64 bits)
    COUNTER_RST_I         : in  std_logic;
    CLK_I                 : in  std_logic
  );
end mainpattern;

architecture Behavioral of mainpattern is

  type pattern_type is array (CGN_NR_OF_PATTERNS-1 downto 0 ) of std_logic_vector(CGN_NR_OF_CH-1 downto 0);
  signal src_en_pattern     : pattern_type := (others=>(others=>'0'));
  signal state_pattern      : pattern_type := (others=>(others=>'0'));

  signal pattern_match_ch   : pattern_type := (others=>(others=>'0'));
  signal pattern_match      : std_logic_vector (CGN_NR_OF_PATTERNS-1 downto 0) := (others=>'0');  -- pattern matched register
  signal pattern_match_s    : std_logic_vector (CGN_NR_OF_PATTERNS-1 downto 0) := (others=>'0');  -- pattern matched register
  signal pattern_match_re   : std_logic_vector (CGN_NR_OF_PATTERNS-1 downto 0) := (others=>'0');  -- pattern matched register

  signal match              : std_logic := '0';
  signal match_s            : std_logic := '0';

begin

  -- Truth table:
  --
  -- ch   = channel
  -- ptrn = pattern number
  -- c  = EDGE_I(ch)
  -- ph = High Pattern(ch)
  -- pl = Low Pattern(ch)
  -- o  = patternmatch(ch)
  --
  -- c  ph pl   o
  -- 0  0  0    1
  -- 0  0  1    1
  -- 0  1  0    0
  -- 0  1  1    0
  -- 1  0  0    1
  -- 1  0  1    0
  -- 1  1  0    1
  -- 1  1  1    0

  -- check pattern matching for enabled ones, then store the result in PATTERNMATCH array
  pattern_p_ch : for p in 0 to CGN_NR_OF_PATTERNS-1 generate

    src_en_pattern(p) <= SRC_EN_PATTERNS_I((p+1)*CGN_NR_OF_CH-1 downto p*CGN_NR_OF_CH);
    state_pattern(p)  <=  STATE_PATTERNS_I((p+1)*CGN_NR_OF_CH-1 downto p*CGN_NR_OF_CH);

    pattern_ch : for ch in 0 to CGN_NR_OF_CH-1 generate

      pattern_match_ch(p)(ch) <=       ( src_en_pattern(p)(ch) and ( EDGE_I(ch) xnor state_pattern(p)(ch) ) )
                                 or not( src_en_pattern(p)(ch) );

    end generate pattern_ch;

    process(PATTERN_EN_I, pattern_match_ch)
    begin
      if PATTERN_EN_I(p) = '0' then
        pattern_match(p) <= '0';
      else
        pattern_match(p) <= and_reduce(pattern_match_ch(p));
      end if;
    end process;

    process(CLK_I)
    begin
      if rising_edge(CLK_I) then
        pattern_match_s(p)  <= pattern_match(p);
      end if;
    end process;

    pattern_match_re(p) <= pattern_match(p) and not pattern_match_s(p);

    coincidence_counter_inst : entity local_trigger_v2_00_a.dsp_counter
    port map (
      CLK_I       => CLK_I,
      RST_I       => COUNTER_RST_I,
      INCREMENT_I => "00" & pattern_match_re(p),
      COUNT_O     => PATTERNS_COUNT_O((p+1)*64-1 downto p*64)
    );

  end generate pattern_p_ch;

  match <= or_reduce(pattern_match);

  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      match_s <= match;
    end if;
  end process;

  TRIGGER_O <= ( match and not(match_s) )  -- leading edge
               when LEAD_TRAIL_EDGE_SEL_I = '0' else
               ( not(match) and match_s ); -- trailing edge



end Behavioral;
