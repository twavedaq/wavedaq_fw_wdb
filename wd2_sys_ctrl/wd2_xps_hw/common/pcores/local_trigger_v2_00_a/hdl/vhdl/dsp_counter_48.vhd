--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : edge_count.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  se32
--  Created :  2022.05.12
--
--  Description :
--    count rising edges in input data vector using DSP slices
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;


library unisim;
use unisim.VComponents.all;


--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------

entity dsp_counter is
  port
  (
    CLK_I       : in  std_logic;
    RST_I       : in  std_logic;
    INCREMENT_I : in  std_logic;
    COUNT_O     : out std_logic_vector(47 downto 0)
  );
end dsp_counter;


--------------------------------------------------------------------------------
-- Architecture section
--------------------------------------------------------------------------------

architecture behave of dsp_counter is

   signal increment : std_logic_vector(47 downto 0) := (others => '0');

begin

  increment(0) <= INCREMENT_I;

  -- counter
  DSP48A1_LSBs_inst : DSP48A1
  generic map (
    A0REG       => 0, -- Enable=1/disable=0 first stage A input pipeline register
    A1REG       => 0, -- Enable=1/disable=0 second stage A input pipeline register
    B0REG       => 0, -- Enable=1/disable=0 first stage B input pipeline register
    B1REG       => 0, -- Enable=1/disable=0 second stage B input pipeline register
    CARRYINREG  => 0, -- Enable=1/disable=0 CARRYIN input pipeline register
    CARRYINSEL  => "OPMODE5", -- Specify carry-in source, "CARRYIN" or "OPMODE5"
    CARRYOUTREG => 0, -- Enable=1/disable=0 CARRYOUT output pipeline register
    CREG        => 1, -- Enable=1/disable=0 C input pipeline register
    DREG        => 0, -- Enable=1/disable=0 D pre-adder input pipeline register
    MREG        => 0, -- Enable=1/disable=0 M pipeline register
    OPMODEREG   => 0, -- Enable=1/disable=0 OPMODE input pipeline registers
    PREG        => 1, -- Enable=1/disable=0 P output pipeline register
    RSTTYPE     => "SYNC" -- Specify reset type, "SYNC" or "ASYNC"
  )
  port map (
    -- Cascade Ports: 18-bit (each) Cascade Ports
    BCOUT      => open,                 -- 18-bit B port cascade output
    PCOUT      => open,                 -- 48-bit cascade output
    -- Data Ports: 1-bit (each) Data Ports
    CARRYOUT   => open,                 -- 1-bit carry output
    CARRYOUTF  => open,                 -- 1-bit fabric carry output
    M          => open,                 -- 36-bit fabric multiplier data output
    P          => COUNT_O,              -- 48-bit output
    -- Cascade Ports: 48-bit (each) Cascade Ports
    PCIN       => (others => '0'),      -- 48-bit P cascade input
    -- Control Inputs: 1-bit (each) Control Inputs
    CLK        => CLK_I,                -- 1-bit Clock input
    OPMODE     => "00001110",           -- 8-bit operation mode input
    -- Data Ports: 18-bit (each) Data Ports
    A          => (others => '0'),      -- 18-bit A data input
    B          => (others => '0'),      -- 18-bit B data input (can be connected to fabric or BCOUT of adjacent DSP48A1)
    C          => increment,            -- 48-bit C data input
    CARRYIN    => '0',                  -- 1-bit carry input signal
    D          => (others => '0'),      -- 18-bit B pre-adder data input
    -- Reset/Clock Enable Inputs: 1-bit (each) Reset/Clock Enable Inputs
    CEA        => '0',                  -- 1-bit active high clock enable input for A input registers
    CEB        => '0',                  -- 1-bit active high clock enable input for B input registers
    CEC        => '1',                  -- 1-bit active high clock enable input for C input registers
    CECARRYIN  => '0',                  -- 1-bit active high clock enable input for CARRYIN registers
    CED        => '0',                  -- 1-bit active high clock enable input for D input registers
    CEM        => '0',                  -- 1-bit active high clock enable input for multiplier registers
    CEOPMODE   => '0',                  -- 1-bit active high clock enable input for OPMODE registers
    CEP        => '1',                  -- 1-bit active high clock enable input for P output registers
    RSTA       => '0',                  -- 1-bit reset input for A input pipeline registers
    RSTB       => '0',                  -- 1-bit reset input for B input pipeline registers
    RSTC       => RST_I,                -- 1-bit reset input for C input pipeline registers
    RSTCARRYIN => '0',                  -- 1-bit reset input for CARRYIN input pipeline registers
    RSTD       => '0',                  -- 1-bit reset input for D input pipeline registers
    RSTM       => '0',                  -- 1-bit reset input for M pipeline registers
    RSTOPMODE  => '0',                  -- 1-bit reset input for OPMODE input pipeline registers
    RSTP       => RST_I                 -- 1-bit reset input for P pipeline registers
  );

end architecture behave;
