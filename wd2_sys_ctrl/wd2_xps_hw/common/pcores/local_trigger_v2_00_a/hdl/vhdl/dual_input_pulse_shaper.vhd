---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Synchronous Pulse Shaper
--
--  Project :  WaveDream2
--
--  PCB  :  WD2
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  09.03.2018 16:13:21
--
--  Description :  Synchronous pulse shaper.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_MISC.ALL;

Library UNISIM;
use UNISIM.vcomponents.all;

entity dual_input_pulse_shaper is
  generic (
    CGN_LENGTH_BITS : integer := 8
  );
  port (
    -- Trigger Signals
    PULSE_I        : in  std_logic;
    POLARITY_I     : in  std_logic; -- 0 = rising, 1 = falling
    SERDES_LSB_I   : in  std_logic;
    PULSE_LENGTH_I : in  std_logic_vector(CGN_LENGTH_BITS-1 downto 0);
    EDGE_O         : out std_logic;
    CLK_I          : in  std_logic
  );
end dual_input_pulse_shaper;

architecture behavioral of dual_input_pulse_shaper is

  signal edge_shaped : std_logic := '0';
  signal serdes_lsb  : std_logic := '0';
  signal pcount      : std_logic_vector(CGN_LENGTH_BITS-1 downto 0) := (others=>'0');
  
begin

  serdes_lsb <= SERDES_LSB_I when POLARITY_I = '0' else not SERDES_LSB_I;

  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if PULSE_I = '1' and edge_shaped = '0' then
        pcount <= PULSE_LENGTH_I;
      elsif pcount > 0 then
        pcount <= pcount - 1;
      end if;
    end if;
  end process;

  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if PULSE_I = '1' then
        edge_shaped <= '1';
      elsif pcount = 0 and serdes_lsb = '0' then
        edge_shaped <= '0';
      end if;
    end if;
  end process;

  EDGE_O <= edge_shaped;

end architecture behavioral;
