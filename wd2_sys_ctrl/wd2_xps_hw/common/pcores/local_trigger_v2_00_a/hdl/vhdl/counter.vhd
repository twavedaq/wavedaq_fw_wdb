--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : edge_count.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  tg32, se32
--  Created :  2018.01.31
--
--  Description :
--    count rising edges in input data vector
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;


library unisim;
use unisim.VComponents.all;


--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------

entity counter is
  port
  (
    CLK_I       : in  std_logic;
    RST_I       : in  std_logic;
    INCREMENT_I : in  std_logic_vector( 2 downto 0);
    COUNT_O     : out std_logic_vector(63 downto 0)
  );
end counter;


--------------------------------------------------------------------------------
-- Architecture section
--------------------------------------------------------------------------------

architecture behave of counter is

   signal counter  :  std_logic_vector(63 downto 0);

begin

  count_proc: process (CLK_I) is
  begin
    if rising_edge(CLK_I) then
      if RST_I = '1' then
        counter <= (others => '0');
      else
       counter <= counter + INCREMENT_I;
     end if;
   end if;
  end process;

  COUNT_O <= counter;

end architecture behave;
