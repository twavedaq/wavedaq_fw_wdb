#
# ELABORATE_PROC
#

proc reg_bank_generics_generate { mhsinst } {

  puts " "
  puts "*************************************"
  puts "User Script to set Register Bank Size"
  puts " "

  puts "Searching for register map version..."
  # TBD: replace text search by specific hw instance and parameter search functions
  set reg_map_vers ""
  set mhs_fp [open system.mhs r]
  while { [gets $mhs_fp current_line] != -1 } {
    if { [string match "*BEGIN*util_register_map*" $current_line] } {
      while { [gets $mhs_fp current_line] != -1 } {
        if { [string match "*PARAMETER HW_VER*" $current_line] } {
          regexp {\d+.\d+..+} $current_line reg_map_vers
          break
        } elseif { [string match "END" $current_line] } {
          puts "ERROR: Register map version not found in instance"
          exit
        }
      }
      if { $reg_map_vers != "" } {
        break
      }
    }
  }

  if { $reg_map_vers == "" } {
    puts "ERROR: Register map version not found in .mhs"
    exit
  }
  puts "Register map version = $reg_map_vers"

  set reg_map_name "util_register_map_v"
  set reg_map_name $reg_map_name[string map {. _} $reg_map_vers]

  set reg_map_dir [ lindex [ lsort [glob -directory ../common/pcores $reg_map_name] ] end ]
  puts "Register map = $reg_map_dir"
  set reg_map_dir "$reg_map_dir/hdl/vhdl/"
  set reg_map_vhd [ glob -directory $reg_map_dir util_register_map*.vhd ]

  set nr_of_ctrl_regs 0
  set nr_of_stat_regs 0
  set hw_ctrl_regs ""
  set hw_stat_regs ""

  puts "Searching $reg_map_vhd for register information..."
  set vhd_fp [open $reg_map_vhd r]
  while { [gets $vhd_fp current_line] != -1 && ( $nr_of_ctrl_regs == 0 || $nr_of_stat_regs == 0 || $hw_ctrl_regs == "" || $hw_stat_regs == "" ) } {
    # find nr of ctrl regs
    if { [string match "*CGN_NUM_CTRL_REG*" $current_line] } {
      set nr_of_ctrl_regs [ regsub -all {[^0-9]} $current_line "" ]
      puts "=> Number of Control Registers = $nr_of_ctrl_regs"
    }
    # find nr of stat regs
    if { [string match "*CGN_NUM_STAT_REG*" $current_line] } {
      set nr_of_stat_regs [ regsub -all {[^0-9]} $current_line "" ]
      puts "=> Number of Status Registers = $nr_of_stat_regs"
    }
    # find hw/sw ctrl reg flags
    if { [string match "*CGN_HW_CTRL_REG*" $current_line] } {
      regexp {\"[0-1]+\"} $current_line hw_ctrl_regs
      puts "=> HW Control Registers = $hw_ctrl_regs"
    }
    # find hw/sw stat reg flags
    if { [string match "*CGN_HW_STAT_REG*" $current_line] } {
      regexp {\"[0-1]+\"} $current_line hw_stat_regs
      puts "=> HW Status Registers = $hw_stat_regs"
    }
  }
  puts " "

  # exit if h-file does not contain values
  if { $nr_of_ctrl_regs == 0 || $nr_of_stat_regs == 0 } {
    puts "ERROR: Values not found in $reg_map_vhd"
    exit
  }

  puts "Register Bank size information found."
  puts " "
  puts "Setting generics..."
  # set generic for nr of ctrl regs
  set parm_handle [xget_hw_parameter_handle $mhsinst "CGN_NO_CTRL_REG"]
  xset_hw_parameter_value $parm_handle [expr $nr_of_ctrl_regs]
  # Show this in the log
  puts "Register Bank CGN_NO_CTRL_REG is set to: $nr_of_ctrl_regs"

  # set generic for nr of stat regs
  set parm_handle [xget_hw_parameter_handle $mhsinst "CGN_NO_STAT_REG"]
  xset_hw_parameter_value $parm_handle [expr $nr_of_stat_regs]
  # Show this in the log
  puts "Register Bank CGN_NO_STAT_REG is set to: $nr_of_stat_regs"

  # set generic for hw/sw ctrl reg flags
  set parm_handle [xget_hw_parameter_handle $mhsinst "CGN_HW_CTRL_REG"]
  set hw_ctrl_regs [expr $hw_ctrl_regs]
  set hw_ctrl_regs "0b$hw_ctrl_regs"
  xset_hw_parameter_value $parm_handle $hw_ctrl_regs
  # Show this in the log
  puts "Register Bank CGN_HW_CTRL_REG is set to: $hw_ctrl_regs"

  # set generic for hw/sw stat reg flags
  set parm_handle [xget_hw_parameter_handle $mhsinst "CGN_HW_STAT_REG"]
  set hw_stat_regs [expr $hw_stat_regs]
  set hw_stat_regs "0b$hw_stat_regs"
  xset_hw_parameter_value $parm_handle $hw_stat_regs
  # Show this in the log
  puts "Register Bank CGN_HW_STAT_REG is set to: $hw_stat_regs"

  puts " "
  puts "End of User Script for Register Bank Size"
  puts "*****************************************"
  puts " "

  return
}
