---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Generic SPI Controler
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  09.07.2014 14:58:31
--
--  Description :  Generic implementation of an SPI Slave Interface.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
-- synopsys translate_off
library UNISIM;
use UNISIM.Vcomponents.ALL;
-- synopsys translate_on

entity spi_slave is
  generic (
    CGN_NUM_D_BITS     : integer := 8; -- allowable values: 1,2,3,...,32
    CGN_CPOL           : integer := 0; -- 0=positive _/"\_ , 1=negative "\_/"
    CGN_CPHA           : integer := 0; -- 0=first edge centered on data, 1=second edge centered on data
    CGN_LSB_FIRST      : integer := 0;
    CGN_HAS_RX_FIFO    : integer := 1;
    CGN_HAS_TX_FIFO    : integer := 1
  );
  port (
    -- SPI interface
    SOMI_O             : out std_logic;
    SIMO_I             : in  std_logic;
    SCK_I              : in  std_logic;
    SS_N_I             : in  std_logic;

    -- Status
    SLAVE_MODE_SEL_O   : out std_logic;
    -- FIFO control and status
    RX_FIFO_RESET_I    : in  std_logic;
    RX_FIFO_AEMPTY_O   : out std_logic;
    RX_FIFO_EMPTY_O    : out std_logic;
    RX_FIFO_RE_I       : in  std_logic;
    RX_FIFO_DATA_O     : out std_logic_vector((((CGN_NUM_D_BITS+7)/8)*8)-1 downto 0);
    TX_FIFO_RESET_I    : in  std_logic;
    TX_FIFO_AFULL_O    : out std_logic;
    TX_FIFO_FULL_O     : out std_logic;
    TX_FIFO_WE_I       : in  std_logic;
    TX_FIFO_DATA_I     : in  std_logic_vector((((CGN_NUM_D_BITS+7)/8)*8)-1 downto 0);

    RESET_I            : in  std_logic;
    CLK_I              : in  std_logic
  );
end spi_slave;



architecture behavioral_async of spi_slave is

  constant C_NUM_BYTES      : integer := (CGN_NUM_D_BITS+7)/8;

  signal edge_count         : std_logic_vector(5 downto 0);
  signal edge_count_zero    : std_logic := '0';
  signal edge_count_zero_s1 : std_logic := '0';
  signal edge_count_zero_s2 : std_logic := '0';
  signal edge_count_zero_s3 : std_logic := '0';
  
  signal input_shift_reg    : std_logic_vector(CGN_NUM_D_BITS-1 downto 0) := (others => '0');
  signal output_shift_reg   : std_logic_vector(CGN_NUM_D_BITS-1 downto 0) := (others => '0');

  -- internal rx fifo signals
  signal rx_fifo_full       : std_logic := '0';
  signal rx_fifo_afull      : std_logic := '0';
  signal rx_fifo_we         : std_logic := '0';
  signal rx_fifo_wdata      : std_logic_vector((C_NUM_BYTES*8)-1 downto 0) := (others => '0');

  -- internal tx fifo signals
  signal tx_fifo_aempty     : std_logic := '0';
  signal tx_fifo_empty      : std_logic := '0';
  signal tx_fifo_re         : std_logic := '0';
  signal tx_fifo_rdata      : std_logic_vector((C_NUM_BYTES*8)-1 downto 0) := (others => '0');

  signal somi               : std_logic := '0';
  signal sck                : std_logic := '0';

  component spi_fifo
    generic (
      CGN_NUM_BYTES : integer := 1; -- allowable values: 1,2,3,4
      CGN_HAS_FIFO  : integer := 1
    );
    port (
      WRITE_DATA_I   : in  std_logic_vector((C_NUM_BYTES*8)-1 downto 0);
      WRITE_EN_I     : in  std_logic;
      READ_DATA_O    : out std_logic_vector((C_NUM_BYTES*8)-1 downto 0);
      READ_EN_I      : in  std_logic;
      FULL_O         : out std_logic;
      ALMOST_FULL_O  : out std_logic;
      ALMOST_EMPTY_O : out std_logic;
      EMPTY_O        : out std_logic;
      RESET_I        : in  std_logic;
      CLK_I          : in  std_logic
    );
  end component;

begin

  rx_fifo_or_reg : spi_fifo
  generic map(
    CGN_NUM_BYTES => C_NUM_BYTES,
    CGN_HAS_FIFO  => CGN_HAS_RX_FIFO
  )
  port map(
    WRITE_DATA_I   => rx_fifo_wdata,
    WRITE_EN_I     => rx_fifo_we,
    READ_DATA_O    => RX_FIFO_DATA_O,
    READ_EN_I      => RX_FIFO_RE_I,
    FULL_O         => rx_fifo_full,
    ALMOST_FULL_O  => rx_fifo_afull,
    ALMOST_EMPTY_O => RX_FIFO_AEMPTY_O,
    EMPTY_O        => RX_FIFO_EMPTY_O,
    RESET_I        => RX_FIFO_RESET_I,
    CLK_I          => CLK_I
  );

  rx_fifo_wdata((C_NUM_BYTES*8)-1 downto CGN_NUM_D_BITS) <= (others=>'0');
  rx_fifo_wdata(CGN_NUM_D_BITS-1 downto 0)               <= input_shift_reg;

  tx_fifo_or_reg : spi_fifo
  generic map(
    CGN_NUM_BYTES => C_NUM_BYTES,
    CGN_HAS_FIFO  => CGN_HAS_TX_FIFO
  )
  port map(
    WRITE_DATA_I   => TX_FIFO_DATA_I,
    WRITE_EN_I     => TX_FIFO_WE_I,
    READ_DATA_O    => tx_fifo_rdata,
    READ_EN_I      => tx_fifo_re,
    FULL_O         => TX_FIFO_FULL_O,
    ALMOST_FULL_O  => TX_FIFO_AFULL_O,
    ALMOST_EMPTY_O => tx_fifo_aempty,
    EMPTY_O        => tx_fifo_empty,
    RESET_I        => TX_FIFO_RESET_I,
    CLK_I          => CLK_I
  );

  SLAVE_MODE_SEL_O <= SS_N_I;

  somi   <= output_shift_reg(0) when CGN_LSB_FIRST =1 else output_shift_reg(CGN_NUM_D_BITS-1);
  SOMI_O <= somi when SS_N_I = '0' else 'Z';

  fifo_enables : process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      edge_count_zero_s1 <= edge_count_zero;
      edge_count_zero_s2 <= edge_count_zero_s1;
      edge_count_zero_s3 <= edge_count_zero_s2;
      if edge_count_zero_s3 = '0' and edge_count_zero_s2 = '1' then
        rx_fifo_we <= '1';
        if tx_fifo_empty = '0' then
          tx_fifo_re <= '1';
        end if;
      else
        rx_fifo_we <= '0';
        tx_fifo_re <= '0';
      end if;
    end if;
  end process fifo_enables;

  sck <= SCK_I when CGN_CPOL = CGN_CPHA else not(SCK_I);

  edge_count_zero <= '1' when edge_count = 0 else '0';

  rx_control : process(sck, RESET_I, SS_N_I)
  begin
    if RESET_I = '1' or SS_N_I = '1' then
      edge_count <= CONV_STD_LOGIC_VECTOR(CGN_NUM_D_BITS, 6);
    elsif rising_edge(sck) then
      if edge_count > 0 then
        -- shift input shift register
        if CGN_LSB_FIRST =1 then
          -- LSB first shifting
          input_shift_reg <= SIMO_I & input_shift_reg(CGN_NUM_D_BITS-1 downto 1);
        else
          -- MSB first shifting
          input_shift_reg <= input_shift_reg(CGN_NUM_D_BITS-2 downto 0) & SIMO_I;
        end if;
        -- count clock edge
        edge_count <= edge_count-1;
      end if;
    end if;
  end process rx_control;

  tx_control : process(sck, RESET_I, SS_N_I, tx_fifo_empty, tx_fifo_rdata)
  begin
    if RESET_I = '1' or SS_N_I = '1' then
      if tx_fifo_empty = '1' then
        output_shift_reg <= (others=>'0');
      else
        output_shift_reg <= tx_fifo_rdata(CGN_NUM_D_BITS-1 downto 0);
      end if;
    elsif falling_edge(sck) then
      if edge_count > 0 then
        if CGN_CPHA = 1 and edge_count = CGN_NUM_D_BITS then
          output_shift_reg <= output_shift_reg;
        else
          -- shift output shift register
          if CGN_LSB_FIRST =1 then
            -- LSB first shifting
            output_shift_reg <= '0' & output_shift_reg(CGN_NUM_D_BITS-1 downto 1);
          else
            -- MSB first shifting
            output_shift_reg <= output_shift_reg(CGN_NUM_D_BITS-2 downto 0) & '0';
          end if;
        end if;
      end if;
    end if;
  end process tx_control;

end behavioral_async;

--architecture behavioral_sync of spi_slave is
--
--  -- states of state machine
--  type type_state is (idle, wait_tx_edge, wait_rx_edge, store);
--  signal state            : type_state := idle;
--
--  signal edge_count       : std_logic_vector(6 downto 0);
--  
--  signal input_shift_reg  : std_logic_vector((C_NUM_BYTES*8)-1 downto 0) := (others => '0');
--  signal output_shift_reg : std_logic_vector((C_NUM_BYTES*8)-1 downto 0) := (others => '0');
--
--  -- internal rx fifo signals
--  signal rx_fifo_full   : std_logic := '0';
--  signal rx_fifo_afull  : std_logic := '0';
--  signal rx_fifo_we     : std_logic := '0';
--  signal rx_fifo_wdata  : std_logic_vector((C_NUM_BYTES*8)-1 downto 0) := (others => '0');
--
--  -- internal tx fifo signals
--  signal tx_fifo_aempty : std_logic := '0';
--  signal tx_fifo_empty  : std_logic := '0';
--  signal tx_fifo_re     : std_logic := '0';
--  signal tx_fifo_rdata  : std_logic_vector((C_NUM_BYTES*8)-1 downto 0) := (others => '0');
--
--
--  signal simo_s1   : std_logic := '0';
--  signal simo_s2   : std_logic := '0';
--  signal simo_s3   : std_logic := '0';
--  signal simo      : std_logic := '0';
--  signal somi      : std_logic := '0';
--  signal sck_s1    : std_logic := '0';
--  signal sck_s2    : std_logic := '0';
--  signal sck_s3    : std_logic := '0';
--  signal ss_s1_n   : std_logic := '0';
--  signal ss_s2_n   : std_logic := '0';
--  signal ss_n      : std_logic := '0';
--  signal sck_edge : std_logic := '0';
--
--
--
--  component spi_fifo
--    generic (
--      C_NUM_BYTES : integer := 1; -- allowable values: 1,2,3,4
--      CGN_HAS_FIFO      : integer := 1
--    );
--    port (
--      WRITE_DATA_I   : in  std_logic_vector((C_NUM_BYTES*8)-1 downto 0);
--      WRITE_EN_I     : in  std_logic;
--      READ_DATA_O    : out std_logic_vector((C_NUM_BYTES*8)-1 downto 0);
--      READ_EN_I      : in  std_logic;
--      FULL_O         : out std_logic;
--      ALMOST_FULL_O  : out std_logic;
--      ALMOST_EMPTY_O : out std_logic;
--      EMPTY_O        : out std_logic;
--      RESET_I        : in  std_logic;
--      CLK_I          : in  std_logic
--    );
--  end component;
--
--begin
--
--  rx_fifo_or_reg : spi_fifo
--    generic map(
--      C_NUM_BYTES => C_NUM_BYTES,
--      CGN_HAS_FIFO      => CGN_HAS_RX_FIFO
--    )
--    port map(
--      WRITE_DATA_I   => rx_fifo_wdata,
--      WRITE_EN_I     => rx_fifo_we,
--      READ_DATA_O    => RX_FIFO_DATA_O,
--      READ_EN_I      => RX_FIFO_RE_I,
--      FULL_O         => rx_fifo_full,
--      ALMOST_FULL_O  => rx_fifo_afull,
--      ALMOST_EMPTY_O => RX_FIFO_AEMPTY_O,
--      EMPTY_O        => RX_FIFO_EMPTY_O,
--      RESET_I        => RX_FIFO_RESET_I,
--      CLK_I          => CLK_I
--    );
--
--  rx_fifo_wdata <= input_shift_reg;
--
--  tx_fifo_or_reg : spi_fifo
--  generic map(
--    C_NUM_BYTES => C_NUM_BYTES,
--    CGN_HAS_FIFO      => CGN_HAS_TX_FIFO
--  )
--  port map(
--    WRITE_DATA_I   => TX_FIFO_DATA_I,
--    WRITE_EN_I     => TX_FIFO_WE_I,
--    READ_DATA_O    => tx_fifo_rdata,
--    READ_EN_I      => tx_fifo_re,
--    FULL_O         => TX_FIFO_FULL_O,
--    ALMOST_FULL_O  => TX_FIFO_AFULL_O,
--    ALMOST_EMPTY_O => tx_fifo_aempty,
--    EMPTY_O        => tx_fifo_empty,
--    RESET_I        => TX_FIFO_RESET_I,
--    CLK_I          => CLK_I
--  );
--
--  -- synchronize to local clock
--  sync_ffs : process(CLK_I)
--  begin
--    if rising_edge(CLK_I) then
--      simo_s1 <= SIMO_I;
--      simo_s2 <= simo_s1;
--      simo    <= simo_s2;
--      sck_s1  <= SCK_I;
--      if CPOL_I = '1' then
--        sck_s2 <= not sck_s1;
--      else
--        sck_s2 <= sck_s1;
--      end if;
--      sck_s3  <= sck_s2;
--      ss_s1_n <= SS_N_I;
--      ss_s2_n <= ss_s1_n;
--      ss_n    <= ss_s2_n;
--    end if;
--  end process sync_ffs;
--
--  sck_edge <= sck_s3 xor sck_s2;
--
--  somi <= output_shift_reg(CONV_INTEGER(CGN_NUM_D_BITS)-1);
--
--  rx_fifo_wdata <= input_shift_reg;
--
--  control_fsm : process(CLK_I)
--  begin
--    if rising_edge(CLK_I) then
--      if RESET_I = '1' then
--        SOMI_O           <= 'Z';
--        tx_fifo_re       <= '0';
--        rx_fifo_we       <= '0';
--        edge_count       <= (others=>'0');
--        input_shift_reg  <= (others=>'0');
--        output_shift_reg <= (others=>'0');
--        state            <= idle;
--      else
--
--        SOMI_O       <= 'Z';
--        tx_fifo_re   <= '0';
--        rx_fifo_we   <= '0';
--
--        -- clock edge counter
--        if edge_count > 0 and sck_edge = '1' then
--          edge_count <= edge_count-1;
--        end if;
--
--        case state is
--        when idle =>
--          edge_count <= CGN_NUM_D_BITS & '0';
--          if ss_n = '0' then
--            SOMI_O <= somi;
--            output_shift_reg <= tx_fifo_rdata;
--            if tx_fifo_empty = '0' then
--              tx_fifo_re <= '1';
--            end if;
--            if CPHA_I = '0' then
--              state <= wait_rx_edge;
--            else
--              state <= wait_tx_edge;
--            end if;
--          end if;
--        when wait_rx_edge =>
--          SOMI_O <= somi;
--          if ss_n = '0' then
--            if edge_count > 0 then
--              if sck_edge = '1' then
--                input_shift_reg <= input_shift_reg((C_NUM_BYTES*8)-2 downto 0) & simo;
--                state <= wait_tx_edge;
--              end if;
--            else
--              state <= store;
--            end if;
--          else
--            state <= idle;
--          end if;
--        when wait_tx_edge =>
--          SOMI_O <= somi;
--          if ss_n = '0' then
--            if edge_count > 0 then
--              if sck_edge = '1' then
--                output_shift_reg <= output_shift_reg((C_NUM_BYTES*8)-2 downto 0) & '0';
--                state <= wait_rx_edge;
--              end if;
--            else
--              state <= store;
--            end if;
--          else
--            state <= idle;
--          end if;
--        when store =>
--          SOMI_O     <= somi;
--          rx_fifo_we <= '1';
--          if ss_n = '1' then
--            state <= idle;
--          end if;
--        when others =>
--          state <= idle;
--        end case;
--      end if;
--    end if;
--  end process control_fsm;
--
--end behavioral_sync;
