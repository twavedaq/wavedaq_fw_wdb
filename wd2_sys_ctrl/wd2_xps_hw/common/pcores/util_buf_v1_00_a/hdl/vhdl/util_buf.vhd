---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Buffer
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was tested with)
--
--  Author  :  Elmar Schmid (Author of generation script)
--  Created :  30.11.2018 15:57:59
--
--  Description :  Buffer to be inserted in xps designs.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
Library UNISIM;
use UNISIM.vcomponents.all;

entity util_buf is
  generic (
    CGN_BUFFER_TYPE   : string   := "IBUF"   -- "IBUF", "OBUF"
  );
  port (
    IN_I  : in  std_logic;
    OUT_O : out std_logic
  );
end util_buf;

architecture behavioral of util_buf is

begin

  IBUF_gen : if CGN_BUFFER_TYPE = "IBUF" generate
    IBUF_inst : IBUF
    generic map
    (
      IBUF_LOW_PWR => TRUE,
      IOSTANDARD   => "DEFAULT"
    )
    port map
    (
      O => OUT_O,
      I => IN_I
    );
  end generate IBUF_gen;

  OBUF_gen : if CGN_BUFFER_TYPE = "OBUF" generate
    OBUF_inst : OBUF
    generic map
    (
      DRIVE      => 12,
      IOSTANDARD => "DEFAULT",
      SLEW       => "SLOW"
    )
    port map
    (
      O => OUT_O,
      I => IN_I
    );
  end generate OBUF_gen;

end architecture behavioral;