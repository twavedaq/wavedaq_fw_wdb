onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider TB
add wave -noupdate /spi_slave_tb/TbRunning
add wave -noupdate /spi_slave_tb/ProcessDone
add wave -noupdate /spi_slave_tb/NextCase
add wave -noupdate /spi_slave_tb/RST_I
add wave -noupdate /spi_slave_tb/CLK_I
add wave -noupdate -expand -group {Register Bank} /spi_slave_tb/RB_ADDR_O
add wave -noupdate -expand -group {Register Bank} /spi_slave_tb/RB_BE_O
add wave -noupdate -expand -group {Register Bank} /spi_slave_tb/RB_DATA_I
add wave -noupdate -expand -group {Register Bank} /spi_slave_tb/RB_DATA_O
add wave -noupdate -expand -group {Register Bank} /spi_slave_tb/RB_RD_ACK_I
add wave -noupdate -expand -group {Register Bank} /spi_slave_tb/RB_REG_SEL_O
add wave -noupdate -expand -group {Register Bank} /spi_slave_tb/RB_REQ_O
add wave -noupdate -expand -group {Register Bank} /spi_slave_tb/RB_RNW_O
add wave -noupdate -expand -group {Register Bank} /spi_slave_tb/RB_WR_ACK_I
add wave -noupdate -expand -group {Register Bank} /spi_slave_tb/RB_ADDR_O
add wave -noupdate -expand -group {Register Bank} /spi_slave_tb/RB_BE_O
add wave -noupdate -expand -group {Register Bank} /spi_slave_tb/RB_DATA_I
add wave -noupdate -expand -group {Register Bank} /spi_slave_tb/RB_DATA_O
add wave -noupdate -expand -group {Register Bank} /spi_slave_tb/RB_RD_ACK_I
add wave -noupdate -expand -group {Register Bank} /spi_slave_tb/RB_REG_SEL_O
add wave -noupdate -expand -group {Register Bank} /spi_slave_tb/RB_REQ_O
add wave -noupdate -expand -group {Register Bank} /spi_slave_tb/RB_RNW_O
add wave -noupdate -expand -group {Register Bank} /spi_slave_tb/RB_WR_ACK_I
add wave -noupdate -expand -group SPI /spi_slave_tb/SCK_I
add wave -noupdate -expand -group SPI /spi_slave_tb/SIMO_I
add wave -noupdate -expand -group SPI /spi_slave_tb/SOMI_O
add wave -noupdate -expand -group SPI /spi_slave_tb/SS_N_I
add wave -noupdate -group {FIFO interface} /spi_slave_tb/RX_FIFO_AEMPTY_O
add wave -noupdate -group {FIFO interface} /spi_slave_tb/RX_FIFO_DATA_O
add wave -noupdate -group {FIFO interface} /spi_slave_tb/RX_FIFO_EMPTY_O
add wave -noupdate -group {FIFO interface} /spi_slave_tb/RX_FIFO_RE_I
add wave -noupdate -group {FIFO interface} /spi_slave_tb/RX_FIFO_RESET_I
add wave -noupdate -group {FIFO interface} /spi_slave_tb/TX_FIFO_AFULL_O
add wave -noupdate -group {FIFO interface} /spi_slave_tb/TX_FIFO_DATA_I
add wave -noupdate -group {FIFO interface} /spi_slave_tb/TX_FIFO_FULL_O
add wave -noupdate -group {FIFO interface} /spi_slave_tb/TX_FIFO_RESET_I
add wave -noupdate -group {FIFO interface} /spi_slave_tb/TX_FIFO_WE_I
add wave -noupdate -group {FIFO interface} /spi_slave_tb/RX_FIFO_AEMPTY_O
add wave -noupdate -group {FIFO interface} /spi_slave_tb/RX_FIFO_DATA_O
add wave -noupdate -group {FIFO interface} /spi_slave_tb/RX_FIFO_EMPTY_O
add wave -noupdate -group {FIFO interface} /spi_slave_tb/RX_FIFO_RE_I
add wave -noupdate -group {FIFO interface} /spi_slave_tb/RX_FIFO_RESET_I
add wave -noupdate -group {FIFO interface} /spi_slave_tb/TX_FIFO_AFULL_O
add wave -noupdate -group {FIFO interface} /spi_slave_tb/TX_FIFO_DATA_I
add wave -noupdate -group {FIFO interface} /spi_slave_tb/TX_FIFO_FULL_O
add wave -noupdate -group {FIFO interface} /spi_slave_tb/TX_FIFO_RESET_I
add wave -noupdate -group {FIFO interface} /spi_slave_tb/TX_FIFO_WE_I
add wave -noupdate /spi_slave_tb/RECFG_O
add wave -noupdate /spi_slave_tb/SYNC_O
add wave -noupdate /spi_slave_tb/CLEAR_O
add wave -noupdate /spi_slave_tb/SLAVE_SEL_O
add wave -noupdate /spi_slave_tb/MisoData
add wave -noupdate /spi_slave_tb/MosiData
add wave -noupdate -divider {Slave IF (DUT)}
add wave -noupdate /spi_slave_tb/i_dut/addr
add wave -noupdate /spi_slave_tb/i_dut/addr_count
add wave -noupdate /spi_slave_tb/i_dut/ascii_tx_en
add wave -noupdate /spi_slave_tb/i_dut/bin_tx_en
add wave -noupdate /spi_slave_tb/i_dut/cmd
add wave -noupdate /spi_slave_tb/i_dut/deselect
add wave -noupdate /spi_slave_tb/i_dut/process_data
add wave -noupdate /spi_slave_tb/i_dut/rb_data
add wave -noupdate /spi_slave_tb/i_dut/run_cmd
add wave -noupdate /spi_slave_tb/i_dut/rx_data
add wave -noupdate /spi_slave_tb/i_dut/rx_data_valid
add wave -noupdate /spi_slave_tb/i_dut/rx_wr_en
add wave -noupdate /spi_slave_tb/i_dut/slave_sel
add wave -noupdate /spi_slave_tb/i_dut/slave_sel_s1
add wave -noupdate /spi_slave_tb/i_dut/slave_sel_s2
add wave -noupdate /spi_slave_tb/i_dut/slave_sel_s3
add wave -noupdate /spi_slave_tb/i_dut/state
add wave -noupdate /spi_slave_tb/i_dut/tx_byte_sel
add wave -noupdate /spi_slave_tb/i_dut/tx_data
add wave -noupdate /spi_slave_tb/i_dut/tx_data_ack
add wave -noupdate /spi_slave_tb/i_dut/tx_fifo_data
add wave -noupdate /spi_slave_tb/i_dut/tx_fifo_empty
add wave -noupdate /spi_slave_tb/i_dut/tx_fifo_empty_n
add wave -noupdate /spi_slave_tb/i_dut/tx_rd_en
add wave -noupdate /spi_slave_tb/i_dut/tx_valid
add wave -noupdate /spi_slave_tb/i_dut/uc_data_en
add wave -noupdate -divider {SPI IF}
add wave -noupdate /spi_slave_tb/i_dut/spi_if_inst/edge_count
add wave -noupdate /spi_slave_tb/i_dut/spi_if_inst/input_shift_reg
add wave -noupdate /spi_slave_tb/i_dut/spi_if_inst/output_shift_reg
add wave -noupdate /spi_slave_tb/i_dut/spi_if_inst/RX_DATA_O
add wave -noupdate /spi_slave_tb/i_dut/spi_if_inst/RX_OVERFLOW_O
add wave -noupdate /spi_slave_tb/i_dut/spi_if_inst/rx_valid
add wave -noupdate /spi_slave_tb/i_dut/spi_if_inst/RX_VALID_O
add wave -noupdate /spi_slave_tb/i_dut/spi_if_inst/sck
add wave -noupdate /spi_slave_tb/i_dut/spi_if_inst/sck_lead_edge
add wave -noupdate /spi_slave_tb/i_dut/spi_if_inst/sck_lead_edge_s
add wave -noupdate /spi_slave_tb/i_dut/spi_if_inst/sck_meta
add wave -noupdate /spi_slave_tb/i_dut/spi_if_inst/sck_previous
add wave -noupdate /spi_slave_tb/i_dut/spi_if_inst/sck_trail_edge
add wave -noupdate /spi_slave_tb/i_dut/spi_if_inst/simo
add wave -noupdate /spi_slave_tb/i_dut/spi_if_inst/simo_meta
add wave -noupdate /spi_slave_tb/i_dut/spi_if_inst/SLAVE_SEL_O
add wave -noupdate /spi_slave_tb/i_dut/spi_if_inst/ss_meta_n
add wave -noupdate /spi_slave_tb/i_dut/spi_if_inst/ss_n
add wave -noupdate /spi_slave_tb/i_dut/spi_if_inst/TX_DATA_I
add wave -noupdate /spi_slave_tb/i_dut/spi_if_inst/TX_READY_O
add wave -noupdate -divider {Register Bank IF}
add wave -noupdate /spi_slave_tb/i_reg_bank/SPI_ADDR_I
add wave -noupdate /spi_slave_tb/i_reg_bank/SPI_BE_I
add wave -noupdate /spi_slave_tb/i_reg_bank/SPI_DATA_I
add wave -noupdate /spi_slave_tb/i_reg_bank/SPI_DATA_O
add wave -noupdate /spi_slave_tb/i_reg_bank/SPI_RD_ACK_O
add wave -noupdate /spi_slave_tb/i_reg_bank/SPI_REG_SEL_I
add wave -noupdate /spi_slave_tb/i_reg_bank/SPI_REQ_I
add wave -noupdate /spi_slave_tb/i_reg_bank/SPI_RNW_I
add wave -noupdate /spi_slave_tb/i_reg_bank/SPI_WR_ACK_O
TreeUpdate [SetDefaultTree]
quietly WaveActivateNextPane
add wave -noupdate /spi_slave_tb/CLK_I
add wave -noupdate /spi_slave_tb/i_dut/spi_if_inst/input_shift_reg
add wave -noupdate /spi_slave_tb/RB_REQ_O
add wave -noupdate /spi_slave_tb/RB_RNW_O
add wave -noupdate /spi_slave_tb/RB_RD_ACK_I
add wave -noupdate /spi_slave_tb/RB_WR_ACK_I
add wave -noupdate /spi_slave_tb/RB_DATA_I
add wave -noupdate /spi_slave_tb/i_dut/rb_data
add wave -noupdate /spi_slave_tb/i_dut/rb_data_bytes
add wave -noupdate /spi_slave_tb/i_dut/tx_data
add wave -noupdate /spi_slave_tb/i_dut/spi_if_inst/output_shift_reg
add wave -noupdate /spi_slave_tb/i_dut/ascii_tx_en
add wave -noupdate /spi_slave_tb/i_dut/tx_fifo_empty
add wave -noupdate /spi_slave_tb/i_dut/bin_tx_en
add wave -noupdate -expand -group SPI /spi_slave_tb/SCK_I
add wave -noupdate -expand -group SPI /spi_slave_tb/SIMO_I
add wave -noupdate -expand -group SPI /spi_slave_tb/SOMI_O
add wave -noupdate -expand -group SPI /spi_slave_tb/SS_N_I
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {5999807 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 179
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {63 us}
