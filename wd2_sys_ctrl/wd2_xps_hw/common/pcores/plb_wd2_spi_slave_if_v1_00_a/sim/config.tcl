##############################################################################
#  Copyright (c) 2019 by Paul Scherrer Institute, Switzerland
#  All rights reserved.
#  Authors: Oliver Bruendler
##############################################################################

#Constants
set LibPath "~/EE_Projects/psi_fpga_all/VHDL"

#Import psi::sim
namespace import psi::sim::*

#Set library
add_library spi_slave_lib
add_library spi_slave_tb_lib

#suppress messages
compile_suppress 135,1236,1073,1246
run_suppress 8684,3479,3813,8009,3812

# Library
add_sources $LibPath {
	psi_tb/hdl/psi_tb_txt_util.vhd \
	psi_tb/hdl/psi_tb_compare_pkg.vhd \
	psi_tb/hdl/psi_tb_activity_pkg.vhd \
	psi_common/hdl/psi_common_array_pkg.vhd \
} -lib spi_slave_tb_lib -tag lib

# project sources
add_sources "../hdl/vhdl" {
	spi_spec_cmd_decoder.vhd \
	spi_if.vhd \
	spi_slave.vhd \
} -lib spi_slave_lib -ver 2002 -tag src
#	fifo_synch_8x512.vhd \
#	spi_fifo.vhd \

# testbenches
add_sources "../tb" {
	spi_slave_tb/generic_list.vhd \
	spi_slave_tb/slv8_list.vhd \
	spi_slave_tb/spi_slave_tb_pkg.vhd \
	spi_slave_tb/rb_generic_register_bank.vhd \
	spi_slave_tb/rb_user_logic.vhd \
	spi_slave_tb/spi_m_bfm.vhd \
	spi_slave_tb/spi_slave_tb_case_binary.vhd \
	spi_slave_tb/spi_slave_tb_case_ascii.vhd \
	spi_slave_tb/spi_slave_tb.vhd \
} -lib spi_slave_tb_lib -tag tb

#TB Runs
create_tb_run "spi_slave_tb" spi_slave_tb_lib
add_tb_run
