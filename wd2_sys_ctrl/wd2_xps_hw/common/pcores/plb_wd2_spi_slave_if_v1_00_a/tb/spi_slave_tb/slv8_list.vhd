library ieee;
use ieee.std_logic_1164.all;

library spi_slave_tb_lib;

package slv8_list is new spi_slave_tb_lib.generic_list
  generic map(std_logic_vector(7 downto 0));
