------------------------------------------------------------
-- Copyright (c) 2021 by Paul Scherrer Institute, Switzerland
-- All rights reserved.
------------------------------------------------------------

------------------------------------------------------------
-- Libraries
------------------------------------------------------------
library UNISIM;
  use UNISIM.Vcomponents.ALL;

library ieee;
  use ieee.std_logic_1164.all;
  use ieee.std_logic_arith.all;
  use ieee.std_logic_unsigned.all;

library spi_slave_lib;

library spi_slave_tb_lib;
  use spi_slave_tb_lib.psi_tb_txt_util.all;
  use spi_slave_tb_lib.psi_tb_compare_pkg.all;
  use spi_slave_tb_lib.psi_tb_activity_pkg.all;
	use spi_slave_tb_lib.psi_common_array_pkg.all;

library spi_slave_tb_lib;
  use spi_slave_tb_lib.slv8_list.all;

------------------------------------------------------------
-- Package Header
------------------------------------------------------------
package spi_slave_tb_pkg is

  ------------------------------------------------------------
  -- Test Case Control
  ------------------------------------------------------------
  shared variable TestSubCase  : integer  := -1;
  shared variable CaseProcDone : std_logic_vector(0 to 2) := "000";
  constant AllCaseProcDone     : std_logic_vector(CaseProcDone'range)  := (others => '1');
  constant CaseProcNr_spi_io_c   : integer := 0;
  constant CaseProcNr_fifo_if_c  : integer := 1;
  constant CaseProcNr_spec_cmd_c : integer := 2;

  procedure InitTestCase;

  procedure FinishTestCase;

  procedure ControlWaitCompl;

  procedure WaitForCase(SubCase : in  integer);

  ------------------------------------------------------------

  function TO_STD_LOGIC_VECTOR(AslvData : t_aslv32) return std_logic_vector;

  function GetReg(RegType : string; Addr : integer; NrOfRegs : integer) return t_aslv8;

  procedure FeedExpectQueue(
              ExpectQueueData : t_aslv8
  );

  procedure SpiSendBinary(
              TxData : t_aslv8;
              signal MosiData_I : inout std_logic_vector(7 downto 0)
  );

  procedure SpiBinWrite8(
              Addr   : std_logic_vector(7 downto 0);
              WrData : t_aslv8;
              signal MosiData_I : inout std_logic_vector(7 downto 0)
  );

  procedure SpiBinWrite16(
              Addr   : std_logic_vector(15 downto 0);
              WrData : t_aslv8;
              signal MosiData_I : inout std_logic_vector(7 downto 0)
  );

  procedure SpiBinWrite32(
              Addr   : std_logic_vector(31 downto 0);
              WrData : t_aslv8;
              signal MosiData_I : inout std_logic_vector(7 downto 0)
  );

  procedure SpiBinRead8(
              Addr                : std_logic_vector(7 downto 0);
              constant Size       : integer;
              signal   MosiData_I : inout std_logic_vector(7 downto 0)
  );

  procedure SpiBinRead16(
              Addr                : std_logic_vector(15 downto 0);
              constant Size       : integer;
              signal   MosiData_I : inout std_logic_vector(7 downto 0)
  );

  procedure SpiBinRead32(
              Addr                : std_logic_vector(31 downto 0);
              constant Size       : integer;
              signal   MosiData_I : inout std_logic_vector(7 downto 0)
  );

  procedure SpiRxCheck(
              NrOfBytes : in integer;
              Msg       : in string := ""
  );

  procedure FlushQueues;

  procedure CheckCtrlRegContent(
              Addr       : in integer;
              TargetData : in t_aslv8;
       signal MemData    : in std_logic_vector;
              Msg        : in string := ""
       );

  -- *** Generics Record ***
  type Generics_t is record
    Dummy : boolean; -- required since empty records are not allowed
  end record;

  ------------------------------------------------------------
  -- Not exported Generics
  ------------------------------------------------------------

  shared variable TestCase : integer := -1;

  constant CGN_NUM_D_BITS : integer := 8;
  constant CGN_CPOL : integer := 0;
  constant CGN_CPHA : integer := 0;
  constant CGN_HAS_RX_FIFO : integer := 1;
  constant CGN_HAS_TX_FIFO : integer := 1;
  constant CGN_LSB_FIRST : integer := 0;

  constant CPK_CMD_WR8       : std_logic_vector(7 downto 0) := x"11";
  constant CPK_CMD_WR16      : std_logic_vector(7 downto 0) := x"12";
  constant CPK_CMD_WR32      : std_logic_vector(7 downto 0) := x"14";
  constant CPK_CMD_RD8       : std_logic_vector(7 downto 0) := x"21";
  constant CPK_CMD_RD16      : std_logic_vector(7 downto 0) := x"22";
  constant CPK_CMD_RD32      : std_logic_vector(7 downto 0) := x"24";
  constant CPK_CMD_ASCII_STX : std_logic_vector(7 downto 0) := x"02";
  constant CPK_ACK_ASCII_ETX : std_logic_vector(7 downto 0) := x"03";

  constant CPK_STATUS_REG_CONTENT : t_aslv32(7 downto 0) := (x"8F8E8D8C", x"8B8A8988", x"87868584", x"83828180", x"9F9E9D9C", x"9B9A9998", x"97969594", x"93929190");
  constant CPK_CTRL_REG_INIT      : t_aslv32(7 downto 0) := (x"FFFFFFFF", x"FFFFFFFF", x"FFFFFFFF", x"FFFFFFFF", x"FFFFFFFF", x"FFFFFFFF", x"FFFFFFFF", x"FFFFFFFF");
  signal CTRL_REG_Q_O              : std_logic_vector(8*32-1 downto 0);
  constant CPK_READ8_MISO_HEAD     : t_aslv8(2 downto 0) := (x"00", x"00", x"00");
  constant CPK_READ16_MISO_HEAD    : t_aslv8(3 downto 0) := (x"00", x"00", x"00", x"00");
  constant CPK_READ32_MISO_HEAD    : t_aslv8(5 downto 0) := (x"00", x"00", x"00", x"00", x"00", x"00");
  constant CPK_WRITE8_MISO_HEAD     : t_aslv8(2 downto 0) := (x"00", x"00", x"00");
  constant CPK_WRITE16_MISO_HEAD    : t_aslv8(3 downto 0) := (x"00", x"00", x"00", x"00");
  constant CPK_WRITE32_MISO_HEAD    : t_aslv8(5 downto 0) := (x"00", x"00", x"00", x"00", x"00", x"00");
  shared variable miso_queue    : generic_list;
  shared variable expect_queue  : generic_list;

  signal MisoDataQueue_updated  : boolean := false;

end package;

------------------------------------------------------------
-- Package Body
------------------------------------------------------------
package body spi_slave_tb_pkg is

  procedure InitTestCase is
  begin
    CaseProcDone := (others => '0');
    TestSubCase  := -1;
    FlushQueues;
    wait for 10 ns;
  end procedure;

  procedure FinishTestCase is
  begin
    TestSubCase := -1;
    wait for 1 us;
  end procedure;

  procedure ControlWaitCompl is
  begin
    while CaseProcDone /= AllCaseProcDone loop
      --wait until rising_edge(Clk);
      wait for 10 ns;
    end loop;
    CaseProcDone := (others => '0');
  end procedure;

  procedure WaitForCase(SubCase : in  integer) is
  begin
    while TestSubCase /= SubCase loop
      --wait until rising_edge(Clk);
      wait for 10 ns;
    end loop;
  end procedure;

  function TO_STD_LOGIC_VECTOR(AslvData : t_aslv32) return std_logic_vector is
    variable SlvLen  : integer;
    variable SlvDataTo     : std_logic_vector(0 to AslvData'length*AslvData(AslvData'low)'length-1);
    variable SlvDataDownto : std_logic_vector(AslvData'length*AslvData(AslvData'low)'length-1 downto 0);
  begin
    SlvLen := AslvData(AslvData'low)'length;
    if AslvData'ascending then
      for i_aslv in AslvData'range loop
        SlvDataTo(i_aslv*SlvLen to (i_aslv+1)*SlvLen-1) := AslvData(i_aslv);
      end loop;
      return SlvDataTo;
    else
      for i_aslv in AslvData'range loop
        SlvDataDownto((i_aslv+1)*SlvLen-1 downto i_aslv*SlvLen) := AslvData(i_aslv);
      end loop;
      return SlvDataDownto;
    end if;
  end function;

  function GetReg(RegType : string; Addr : integer; NrOfRegs : integer) return t_aslv8 is
    variable i_word : integer;
    variable i_byte : integer;
    variable data   : t_aslv8(0 to NrOfRegs-1);
  begin

    for n in 0 to NrOfRegs-1 loop

      i_word := (Addr + n) / 4;
      i_byte := 4 - ((Addr + n) mod 4);

      assert i_word <= CPK_STATUS_REG_CONTENT'high
        report "###Error: Register address out of range"
        severity error;

      if RegType = "CONTROL" then
        data(n) := CPK_CTRL_REG_INIT(i_word)((i_byte)*8-1 downto (i_byte-1)*8);
      elsif RegType = "STATUS" then
        data(n) := CPK_STATUS_REG_CONTENT(i_word)((i_byte)*8-1 downto (i_byte-1)*8);
      end if;

    end loop;

    return data;

  end function;

  procedure FeedExpectQueue(
              ExpectQueueData : t_aslv8
  ) is
  begin
    for i in ExpectQueueData'range loop
      expect_queue.append(ExpectQueueData(i));
    end loop;
  end procedure;

  procedure SpiSendBinary(
              TxData : t_aslv8;
              signal MosiData_I : inout std_logic_vector(7 downto 0)
  ) is
  begin
    for i in TxData'range loop
      MosiData_I <= TxData(i);
      wait for 1 ns;
    end loop;
  end procedure;

  procedure SpiBinWrite8(
              Addr   : std_logic_vector(7 downto 0);
              WrData : t_aslv8;
              signal MosiData_I : inout std_logic_vector(7 downto 0)
  ) is
  begin
    SpiSendBinary( (CPK_CMD_WR8, Addr, WrData), MosiData_I);
  end procedure;

  procedure SpiBinWrite16(
              Addr   : std_logic_vector(15 downto 0);
              WrData : t_aslv8;
              signal MosiData_I : inout std_logic_vector(7 downto 0)
  ) is
  begin
    SpiSendBinary( (CPK_CMD_WR16, Addr(15 downto 8), Addr(7 downto 0), WrData), MosiData_I);
  end procedure;

  procedure SpiBinWrite32(
              Addr   : std_logic_vector(31 downto 0);
              WrData : t_aslv8;
              signal MosiData_I : inout std_logic_vector(7 downto 0)
  ) is
  begin
    SpiSendBinary( (CPK_CMD_WR32, Addr(31 downto 24), Addr(23 downto 16), Addr(15 downto 8), Addr(7 downto 0), WrData), MosiData_I);
  end procedure;

  procedure SpiBinRead8(
              Addr                : std_logic_vector(7 downto 0);
              constant Size       : integer;
              signal   MosiData_I : inout std_logic_vector(7 downto 0)
  ) is
    variable TxDummyData : t_aslv8(Size-1 downto 0) := (others => (others => '0'));
  begin
    SpiSendBinary( (CPK_CMD_RD8, Addr, x"00", TxDummyData), MosiData_I);
  end procedure;

  procedure SpiBinRead16(
              Addr                : std_logic_vector(15 downto 0);
              constant Size       : integer;
              signal   MosiData_I : inout std_logic_vector(7 downto 0)
  ) is
    variable TxDummyData : t_aslv8(Size-1 downto 0) := (others => (others => '0'));
  begin
    SpiSendBinary( (CPK_CMD_RD16, Addr(15 downto 8), Addr(7 downto 0), x"00", TxDummyData), MosiData_I);
  end procedure;

  procedure SpiBinRead32(
              Addr                : std_logic_vector(31 downto 0);
              constant Size       : integer;
              signal   MosiData_I : inout std_logic_vector(7 downto 0)
  ) is
    variable TxDummyData : t_aslv8(Size-1 downto 0) := (others => (others => '0'));
  begin
    SpiSendBinary( (CPK_CMD_RD32, Addr(31 downto 24), Addr(23 downto 16), Addr(15 downto 8), Addr(7 downto 0), x"00", TxDummyData), MosiData_I);
  end procedure;

  procedure SpiRxCheck(
              NrOfBytes : in integer;
              Msg       : in string := ""
  ) is
    variable BytesReceived : integer := 0;
    variable RxByte        : std_logic_vector(7 downto 0);
    variable ExpByte       : std_logic_vector(7 downto 0);
  begin
    for i in 1 to NrOfBytes loop
      if miso_queue.length = 0 then
        wait on MisoDataQueue_updated for 1 ms;
        assert miso_queue.length > 0
        report "###Error: Timeout waiting for MISO data" & Msg
        severity error;
      end if;

      assert expect_queue.length > 0
        report "###Error: No MISO data expected" & Msg
        severity error;

      RxByte  := miso_queue.get(0);
      miso_queue.delete(0);
      ExpByte := expect_queue.get(0);
      expect_queue.delete(0);
      StdlvCompareStdlv(ExpByte, RxByte, "SPI MISO Byte Value Wrong at byte " & integer'image(i) & " - " & Msg);
    end loop;
  end procedure;

  procedure FlushQueues is
  begin
    miso_queue.clear;
    expect_queue.clear;
  end procedure;

  procedure CheckCtrlRegContent(
              Addr       : in integer;
              TargetData : in t_aslv8;
       signal MemData    : in std_logic_vector;
              Msg        : in string := ""
  ) is
    variable Offset  : integer;
    variable ByteSel : integer;
    variable MemByte : std_logic_vector(7 downto 0);
  begin
    Offset  := Addr/4;
    ByteSel := 3 - (Addr mod 4);
    for i in TargetData'range loop
      MemByte := MemData((Offset*32+(ByteSel+1)*8)-1 downto Offset*32+ByteSel*8);
      StdlvCompareStdlv(TargetData(i), MemByte, "Written CTRL register Value Wrong at Offset " & integer'image(Offset) & "   Byte " & integer'image(ByteSel) & " - " & Msg);
      ByteSel := ByteSel - 1;
      if ByteSel < 0 then
        Offset  := Offset + 1;
        ByteSel := 3;
      end if;
    end loop;
  end procedure;

end;
