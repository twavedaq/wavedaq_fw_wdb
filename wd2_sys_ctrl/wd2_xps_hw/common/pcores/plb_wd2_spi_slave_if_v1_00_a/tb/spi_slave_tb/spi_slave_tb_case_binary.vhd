------------------------------------------------------------
-- Copyright (c) 2021 by Paul Scherrer Institute, Switzerland
-- All rights reserved.
------------------------------------------------------------

------------------------------------------------------------
-- Libraries
------------------------------------------------------------
library UNISIM;
  use UNISIM.Vcomponents.ALL;

library ieee;
  use ieee.std_logic_1164.all;
  use ieee.std_logic_arith.all;
  use ieee.std_logic_unsigned.all;

library spi_slave_lib;

library spi_slave_tb_lib;
  use spi_slave_tb_lib.spi_slave_tb_pkg.all;

library spi_slave_tb_lib;
  use spi_slave_tb_lib.psi_tb_txt_util.all;
  use spi_slave_tb_lib.psi_tb_compare_pkg.all;
  use spi_slave_tb_lib.psi_tb_activity_pkg.all;
  use spi_slave_tb_lib.psi_common_array_pkg.all;

------------------------------------------------------------
-- Package Header
------------------------------------------------------------
package spi_slave_tb_case_binary is

  procedure spi_io (
    signal MosiData : inout std_logic_vector(7 downto 0);
    signal MisoData : in    std_logic_vector(7 downto 0);
    signal SS_N     : in    std_logic;
    constant Generics_c : Generics_t);

  procedure fifo_if (
    signal RX_FIFO_RESET_I : inout std_logic;
    signal RX_FIFO_AEMPTY_O : in std_logic;
    signal RX_FIFO_EMPTY_O : in std_logic;
    signal RX_FIFO_RE_I : inout std_logic;
    signal RX_FIFO_DATA_O : in std_logic_vector;
    signal TX_FIFO_RESET_I : inout std_logic;
    signal TX_FIFO_AFULL_O : in std_logic;
    signal TX_FIFO_FULL_O : in std_logic;
    signal TX_FIFO_WE_I : inout std_logic;
    signal TX_FIFO_DATA_I : inout std_logic_vector;
    constant Generics_c : Generics_t);

  procedure spec_cmd (
    signal CLEAR_O : in std_logic;
    signal SYNC_O : in std_logic;
    signal RECFG_O : in std_logic;
    constant Generics_c : Generics_t);

end package;

------------------------------------------------------------
-- Package Body
------------------------------------------------------------
package body spi_slave_tb_case_binary is
  procedure spi_io (
    signal MosiData : inout std_logic_vector(7 downto 0);
    signal MisoData : in    std_logic_vector(7 downto 0);
    signal SS_N     : in    std_logic;
    constant Generics_c : Generics_t) is
  begin
    print(">> -- Binary --");

    print(">> Read 8");
    InitTestCase;
    TestSubCase := 0;
    SpiBinRead8(x"00", 1, MosiData);
    FeedExpectQueue( (CPK_READ8_MISO_HEAD, GetReg("STATUS", 0, 1) ) );
    SpiRxCheck(4, "Binary Read 8");
    wait until SS_N = '1';
    CaseProcDone(CaseProcNr_spi_io_c) := '1';
    ControlWaitCompl;

    print(">> Read 16");
    InitTestCase;
    TestSubCase := 1;
    SpiBinRead16(x"0004", 2, MosiData);
    FeedExpectQueue( (CPK_READ16_MISO_HEAD, GetReg("STATUS", 4, 2) ) );
    SpiRxCheck(6, "Binary Read 16");
    wait until SS_N = '1';
    CaseProcDone(CaseProcNr_spi_io_c) := '1';
    ControlWaitCompl;

    print(">> Read 32");
    InitTestCase;
    TestSubCase := 2;
    SpiBinRead32(x"00000008", 4, MosiData);
    FeedExpectQueue( (CPK_READ32_MISO_HEAD, GetReg("STATUS", 8, 4) ) );
    SpiRxCheck(10, "Binary Read 32");
    wait until SS_N = '1';
    CaseProcDone(CaseProcNr_spi_io_c) := '1';
    ControlWaitCompl;

--    print(">> Write 8");
--    InitTestCase;
--    TestSubCase := 3;
--    SpiBinWrite8(x"00", (x"A5", x"A5"), MosiData);
--    FeedExpectQueue( (CPK_WRITE8_MISO_HEAD, x"00" ) );
--    SpiRxCheck(4, "Binary Write 8");
--    wait until SS_N = '1';
--    CaseProcDone(CaseProcNr_spi_io_c) := '1';
--    ControlWaitCompl;

    print(">> Write 16");
    InitTestCase;
    TestSubCase := 4;
    SpiBinWrite16(x"1004", (x"B4", x"C3"), MosiData);
    --FeedExpectQueue( (CPK_WRITE16_MISO_HEAD, x"00", x"00" ) );
    --SpiRxCheck(6, "Binary Write 16");
    wait until SS_N = '1';
    CheckCtrlRegContent(4, (x"B4", x"C3"), CTRL_REG_Q_O);
    CaseProcDone(CaseProcNr_spi_io_c) := '1';
    ControlWaitCompl;

    print(">> Write 32");
    InitTestCase;
    TestSubCase := 5;
    SpiBinWrite32(x"00001008", (x"D2", x"E1", x"F0", x"71"), MosiData);
    --FeedExpectQueue( (CPK_WRITE32_MISO_HEAD, x"00", x"00", x"00", x"00" ) );
    --SpiRxCheck(10, "Binary Write 32");
    wait until SS_N = '1';
    CheckCtrlRegContent(8, (x"D2", x"E1", x"F0", x"71"), CTRL_REG_Q_O);
    CaseProcDone(CaseProcNr_spi_io_c) := '1';
    ControlWaitCompl;

    -- ToDo: Add read or write of multiple words

    FinishTestCase;
  end procedure;

  procedure fifo_if (
    signal RX_FIFO_RESET_I : inout std_logic;
    signal RX_FIFO_AEMPTY_O : in std_logic;
    signal RX_FIFO_EMPTY_O : in std_logic;
    signal RX_FIFO_RE_I : inout std_logic;
    signal RX_FIFO_DATA_O : in std_logic_vector;
    signal TX_FIFO_RESET_I : inout std_logic;
    signal TX_FIFO_AFULL_O : in std_logic;
    signal TX_FIFO_FULL_O : in std_logic;
    signal TX_FIFO_WE_I : inout std_logic;
    signal TX_FIFO_DATA_I : inout std_logic_vector;
    constant Generics_c : Generics_t) is
  begin
    WaitForCase(0);
    CaseProcDone(CaseProcNr_fifo_if_c) := '1';

    WaitForCase(1);
    CaseProcDone(CaseProcNr_fifo_if_c) := '1';

    WaitForCase(2);
    CaseProcDone(CaseProcNr_fifo_if_c) := '1';

--    WaitForCase(3);
--    CaseProcDone(CaseProcNr_fifo_if_c) := '1';

    WaitForCase(4);
    CaseProcDone(CaseProcNr_fifo_if_c) := '1';

    WaitForCase(5);
    CaseProcDone(CaseProcNr_fifo_if_c) := '1';
  end procedure;

  procedure spec_cmd (
    signal CLEAR_O : in std_logic;
    signal SYNC_O : in std_logic;
    signal RECFG_O : in std_logic;
    constant Generics_c : Generics_t) is
  begin
    WaitForCase(0);
    CaseProcDone(CaseProcNr_spec_cmd_c) := '1';

    WaitForCase(1);
    CaseProcDone(CaseProcNr_spec_cmd_c) := '1';

    WaitForCase(2);
    CaseProcDone(CaseProcNr_spec_cmd_c) := '1';

--    WaitForCase(3);
--    CaseProcDone(CaseProcNr_spec_cmd_c) := '1';

    WaitForCase(4);
    CaseProcDone(CaseProcNr_spec_cmd_c) := '1';

    WaitForCase(5);
    CaseProcDone(CaseProcNr_spec_cmd_c) := '1';
  end procedure;

end;
