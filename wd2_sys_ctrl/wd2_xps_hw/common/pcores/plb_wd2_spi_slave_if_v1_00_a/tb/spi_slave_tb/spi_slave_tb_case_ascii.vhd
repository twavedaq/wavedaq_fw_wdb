------------------------------------------------------------
-- Copyright (c) 2021 by Paul Scherrer Institute, Switzerland
-- All rights reserved.
------------------------------------------------------------

------------------------------------------------------------
-- Libraries
------------------------------------------------------------
library UNISIM;
  use UNISIM.Vcomponents.ALL;

library ieee;
  use ieee.std_logic_1164.all;
  use ieee.std_logic_arith.all;
  use ieee.std_logic_unsigned.all;

library spi_slave_lib;

library spi_slave_tb_lib;
  use spi_slave_tb_lib.spi_slave_tb_pkg.all;

library spi_slave_tb_lib;
  use spi_slave_tb_lib.psi_tb_txt_util.all;
  use spi_slave_tb_lib.psi_tb_compare_pkg.all;
  use spi_slave_tb_lib.psi_tb_activity_pkg.all;
  use spi_slave_tb_lib.psi_common_array_pkg.all;

------------------------------------------------------------
-- Package Header
------------------------------------------------------------
package spi_slave_tb_case_ascii is

  procedure spi_io (
    signal MosiData : inout std_logic_vector(7 downto 0);
    signal MisoData : in    std_logic_vector(7 downto 0);
    signal SS_N     : in    std_logic;
    constant Generics_c : Generics_t);

  procedure fifo_if (
    signal RX_FIFO_RESET_I : inout std_logic;
    signal RX_FIFO_AEMPTY_O : in std_logic;
    signal RX_FIFO_EMPTY_O : in std_logic;
    signal RX_FIFO_RE_I : inout std_logic;
    signal RX_FIFO_DATA_O : in std_logic_vector;
    signal TX_FIFO_RESET_I : inout std_logic;
    signal TX_FIFO_AFULL_O : in std_logic;
    signal TX_FIFO_FULL_O : in std_logic;
    signal TX_FIFO_WE_I : inout std_logic;
    signal TX_FIFO_DATA_I : inout std_logic_vector;
    constant Generics_c : Generics_t);

  procedure spec_cmd (
    signal CLEAR_O : in std_logic;
    signal SYNC_O : in std_logic;
    signal RECFG_O : in std_logic;
    constant Generics_c : Generics_t);

end package;

------------------------------------------------------------
-- Package Body
------------------------------------------------------------
package body spi_slave_tb_case_ascii is
  procedure spi_io (
    signal MosiData : inout std_logic_vector(7 downto 0);
    signal MisoData : in    std_logic_vector(7 downto 0);
    signal SS_N     : in    std_logic;
    constant Generics_c : Generics_t) is
  begin
    assert false report "Case ASCII Procedure SPI_IO: No Content added yet!" severity warning;
  end procedure;

  procedure fifo_if (
    signal RX_FIFO_RESET_I : inout std_logic;
    signal RX_FIFO_AEMPTY_O : in std_logic;
    signal RX_FIFO_EMPTY_O : in std_logic;
    signal RX_FIFO_RE_I : inout std_logic;
    signal RX_FIFO_DATA_O : in std_logic_vector;
    signal TX_FIFO_RESET_I : inout std_logic;
    signal TX_FIFO_AFULL_O : in std_logic;
    signal TX_FIFO_FULL_O : in std_logic;
    signal TX_FIFO_WE_I : inout std_logic;
    signal TX_FIFO_DATA_I : inout std_logic_vector;
    constant Generics_c : Generics_t) is
  begin
    assert false report "Case ASCII Procedure FIFO_IF: No Content added yet!" severity warning;
  end procedure;

  procedure spec_cmd (
    signal CLEAR_O : in std_logic;
    signal SYNC_O : in std_logic;
    signal RECFG_O : in std_logic;
    constant Generics_c : Generics_t) is
  begin
    assert false report "Case ASCII Procedure SPEC_CMD: No Content added yet!" severity warning;
  end procedure;

end;
