library ieee;
  use ieee.std_logic_1164.all;
  use ieee.std_logic_arith.all;
  use ieee.std_logic_unsigned.all;

library spi_slave_tb_lib;
  use spi_slave_tb_lib.spi_slave_tb_pkg.all;

library spi_slave_tb_lib;
  use spi_slave_tb_lib.slv8_list.all;

library spi_slave_tb_lib;
  use spi_slave_tb_lib.psi_tb_txt_util.all;

entity spi_m_bfm  is
  port (
    -- Data to send to slave
    MosiData_I : in  std_logic_vector(7 downto 0);
    MisoData_O : out std_logic_vector(7 downto 0);
    -- SPI interface signals
    SS_N_O     : out std_logic;
    MOSI_O     : out std_logic;
    MISO_I     : in  std_logic;
    SCK_O      : out std_logic
  );
end spi_m_bfm;

architecture behavioral of spi_m_bfm is

  constant C_T_SCK_PERIOD : time := 200 ns;
  constant C_T_SS_TO_SCK  : time := 300 ns; -- > C_T_SCK_PERIOD/2 !!!
  constant C_T_SCK_TO_SS  : time := 300 ns;
  constant C_T_SCK_TO_TX  : time :=  50 ns;
  constant C_T_SCK_TO_RX  : time :=  50 ns;

  shared variable send_queue : generic_list;
  signal send_queue_updated  : boolean;

  signal ss_n : std_logic := '1';
  signal sck  : std_logic := '0';

begin

  SS_N_O <= ss_n;
  SCK_O  <= sck when CGN_CPOL = 0 else not sck;

  LIST_APPEND_PROC : process
  begin
    wait on MosiData_I'transaction;
    send_queue.append(MosiData_I);
    send_queue_updated <= not send_queue_updated;
  end process;

  TX_PROC : process
    variable TxByte : std_logic_vector(7 downto 0) := (others => 'U');
  begin

    -- Idle output signals
    ss_n <= '1';
    sck  <= '0';
    MOSI_O <= 'Z';

    if send_queue.length = 0 then
      wait on send_queue_updated;
    end if;

    -- Select
    ss_n <= '0';
    MOSI_O <= 'U';
    wait for C_T_SS_TO_SCK - C_T_SCK_PERIOD/2;

    -- Generate clock burst
    while send_queue.length > 0 loop
      -- Read FIFO queue and clear item
      TxByte := send_queue.get(0);
      send_queue.delete(0);
      -- Generate byte burst
      for i in 7 downto 0 loop
        MOSI_O <= TxByte(i);
        wait for C_T_SCK_PERIOD/2;
        sck <= '1';
        wait for C_T_SCK_PERIOD/2;
        sck <= '0';
      end loop;
    end loop;

    MOSI_O <= 'U';
    wait for C_T_SCK_TO_SS;

  end process;

  RX_PROC : process
    variable RxByte : std_logic_vector(7 downto 0) := (others => 'U');
  begin

    wait until ss_n = '0';

    while ss_n = '0' loop
      -- Receive bits
      for i in 7 downto 0 loop
        wait until rising_edge(sck);
        if ss_n = '0' then
          RxByte(i) := MISO_I;
        else
          RxByte(i) := 'U';
        end if;
      end loop;
      MisoData_O <= RxByte;
    end loop;

  end process;

end architecture;
