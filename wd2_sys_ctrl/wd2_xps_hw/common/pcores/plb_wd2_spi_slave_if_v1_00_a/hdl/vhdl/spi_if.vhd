---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Generic SPI Controler
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  09.07.2014 14:58:31
--
--  Description :  Generic implementation of an SPI Slave Interface.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
-- synopsys translate_off
library UNISIM;
use UNISIM.Vcomponents.ALL;
-- synopsys translate_on

entity spi_if is
  generic (
    CGN_NUM_D_BITS     : integer := 8; -- allowable values: 1,2,3,...,32
    CGN_CPOL           : integer := 0; -- 0=positive _/"\_ , 1=negative "\_/"
    CGN_CPHA           : integer := 0; -- 0=first edge centered on data, 1=second edge centered on data
    CGN_LSB_FIRST      : integer := 0
  );
  port (
    -- SPI interface
    SOMI_O             : out std_logic;
    SIMO_I             : in  std_logic;
    SCK_I              : in  std_logic;
    SS_N_I             : in  std_logic;

    -- Data
    RX_DATA_O          : out std_logic_vector((((CGN_NUM_D_BITS+7)/8)*8)-1 downto 0);
    TX_DATA_I          : in  std_logic_vector((((CGN_NUM_D_BITS+7)/8)*8)-1 downto 0);

    -- Handshake
    RX_VALID_O         : out std_logic;
    RX_READY_I         : in  std_logic;
    TX_VALID_I         : in  std_logic;
    TX_READY_O         : out std_logic;

    -- Status
    SLAVE_SEL_O        : out std_logic;
    RX_OVERFLOW_O      : out std_logic;
    CLR_RX_OVFL_I      : in  std_logic;

    RST_I              : in  std_logic;
    CLK_I              : in  std_logic
  );
end spi_if;

architecture behavioral of spi_if is

  constant C_NUM_BYTES      : integer := (CGN_NUM_D_BITS+7)/8;

  signal edge_count         : std_logic_vector(5 downto 0) := (others=>'0');

  signal input_shift_reg    : std_logic_vector(CGN_NUM_D_BITS-1 downto 0) := (others => '0');
  signal output_shift_reg   : std_logic_vector(CGN_NUM_D_BITS-1 downto 0) := (others => '0');

  signal somi               : std_logic := '0';
  signal simo_meta           : std_logic := '0';
  signal simo                : std_logic := '0';
  signal sck_meta            : std_logic := '0';
  signal sck                : std_logic := '0';
  signal sck_previous        : std_logic := '0';
  signal sck_lead_edge       : std_logic := '0';
  signal sck_lead_edge_s     : std_logic := '0';
  signal sck_trail_edge      : std_logic := '0';
  signal ss_meta_n           : std_logic := '1';
  signal ss_n                : std_logic := '1';
  signal rx_valid            : std_logic := '0';
  signal tx_ready            : std_logic := '0';

begin


  sync : process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      simo_meta    <= SIMO_I;
      simo         <= simo_meta;
      sck_meta     <= SCK_I;
      sck          <= sck_meta;
      sck_previous <= sck;
      ss_meta_n    <= SS_N_I;
      ss_n         <= ss_meta_n;
    end if;
  end process;
  SOMI_O      <= output_shift_reg(0) when CGN_LSB_FIRST = 1 else output_shift_reg(CGN_NUM_D_BITS-1);

  SLAVE_SEL_O <= ss_n;
  RX_VALID_O  <= rx_valid;

  sck_mode_sel : process(sck, sck_previous)
  begin
    if CGN_CPOL = CGN_CPHA then
      sck_lead_edge  <=     sck and not sck_previous;
      sck_trail_edge <= not sck and     sck_previous;
      else
      sck_lead_edge  <= not sck and     sck_previous;
      sck_trail_edge <=     sck and not sck_previous;
      end if;
  end process;

  edge_counter : process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if RST_I = '1' or ss_n = '1' then
        edge_count <= CONV_STD_LOGIC_VECTOR(CGN_NUM_D_BITS, 6);
      elsif sck_lead_edge = '1' then
          -- count clock edge
        if edge_count = 0 then
          edge_count <= CONV_STD_LOGIC_VECTOR(CGN_NUM_D_BITS-1, 6);
      else
          edge_count <= edge_count-1;
      end if;
    end if;
    end if;
  end process;

  rx_dout_reg : process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      sck_lead_edge_s <= sck_lead_edge;
      if RST_I = '1' then
        rx_valid <= '0';
        RX_OVERFLOW_O <= '0';
      else
        if CLR_RX_OVFL_I = '1' then
          RX_OVERFLOW_O <= '0';
        end if;
        if sck_lead_edge_s = '1' and edge_count = 0 then
          RX_DATA_O((C_NUM_BYTES*8)-1 downto CGN_NUM_D_BITS) <= (others=>'0');
          RX_DATA_O(CGN_NUM_D_BITS-1 downto 0)               <= input_shift_reg;
          rx_valid <= '1';
          if rx_valid = '1' and RX_READY_I = '0' then
            RX_OVERFLOW_O <= '1';
          end if;
        elsif RX_READY_I = '1' then
          rx_valid <= '0';
        end if;
      end if;
    end if;
  end process;

  rx_reg : process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if RST_I = '1' or ss_n = '1' then
        input_shift_reg <= (others => '0');
      elsif sck_lead_edge = '1' then
      -- shift input shift register
      if CGN_LSB_FIRST =1 then
        -- LSB first shifting
          input_shift_reg <= simo & input_shift_reg(CGN_NUM_D_BITS-1 downto 1);
      else
        -- MSB first shifting
          input_shift_reg <= input_shift_reg(CGN_NUM_D_BITS-2 downto 0) & simo;
      end if;
        end if;
      end if;
  end process rx_reg;

  tx_reg : process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      -- defaults
      TX_READY_O <= '0';
      -- -- --
      if RST_I = '1' or ss_n = '1' then
      output_shift_reg <= (others=>'0');
        tx_ready <= '0';
      elsif sck_trail_edge = '1' then
      if edge_count = 0 then
          if TX_VALID_I = '1' then
            tx_ready <= '1';
        output_shift_reg <= TX_DATA_I(CGN_NUM_D_BITS-1 downto 0);
          else
            output_shift_reg <= (others => '0');
          end if;
      else
        if CGN_CPHA = 1 and edge_count = CGN_NUM_D_BITS then
          output_shift_reg <= output_shift_reg;
        else
          -- shift output shift register
          if CGN_LSB_FIRST = 1 then
            -- LSB first shifting
            output_shift_reg <= '0' & output_shift_reg(CGN_NUM_D_BITS-1 downto 1);
          else
            -- MSB first shifting
            output_shift_reg <= output_shift_reg(CGN_NUM_D_BITS-2 downto 0) & '0';
          end if;
        end if;
      end if;
      elsif sck_lead_edge = '1' then
        TX_READY_O <= tx_ready;
        tx_ready   <= '0';
    end if;
    end if;
  end process tx_reg;

end behavioral;
