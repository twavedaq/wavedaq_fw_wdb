# Information on PCore Simulation

## Maintainers
Elmar Schmid [elmar.schmid@psi.ch]

## Authors
Elmar Schmid [elmar.schmid@psi.ch]

# Simulation setup

A simulation of the core can be run by using the scripts in the sim/ folder.
Note that the pcore was only partially simulated, i.e. the spi_if and the spi_slave entities.

Simulation was originally run with modelsim 10.7c. However since the design was made on the
ISE 14.7 it was not possible to compile the simulation libraries to use the FIFOs that were
generated with coregen.

# Prerequisites

The simulation testbench uses packages provided in the PSI VHDL library that can be found here:
https://github.com/paulscherrerinstitute

The repositories psi_common as well as psi_tb.
They have to be cloned and mapped by providing the proper path setting in config.tcl.
