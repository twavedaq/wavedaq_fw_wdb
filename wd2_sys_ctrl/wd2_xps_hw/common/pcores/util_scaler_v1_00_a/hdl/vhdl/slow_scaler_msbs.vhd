---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Slow Scaler LSBs
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  18.05.2016 08:33:58
--
--  Description :  Logic to count the pulses discriminated by the comparator on each
--                 channel of the WaveDream board. This part represents the slow
--                 counter for the msbs of the scaler, clocked with the microblaze
--                 clock.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity slow_scaler_lsbs is
  generic (
    CGN_COUNTER_WIDTH : integer := 56
  );
  port (
    COUNTER_VALUE_O     : out std_logic_vector(CGN_COUNTER_WIDTH-1 downto 0);
    EN_I                : in  std_logic;
    RESET_I             : in  std_logic;
    CLK_I               : in  std_logic
  );
end slow_scaler_lsbs;

architecture behavioral of slow_scaler_lsbs is

  signal counter : std_logic_vector(CGN_COUNTER_WIDTH-1 downto 0) := (others=>'0');

begin

  COUNTER_VALUE_O <= counter;

  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if RESET_I = '1' then
        counter <= (others=>'0');
      elsif EN_I = '1' then
        counter <= counter + 1;
      end if;
    end if;
  end process;

end architecture behavioral;