---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Tick Timer
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid (Author of generation script)
--  Created :  11.01.2018 11:01:22
--
--  Description :  Generic Tick Timer.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

Library UNISIM;
use UNISIM.vcomponents.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

entity util_tick_timer is
  generic (
    CGN_TICK_TIME_NS  : real :=  1.0e9; -- ns
    CGN_CLK_PERIOD_NS : real := 12.5;   -- ns
    CGN_TIMER_WIDTH   : integer := 8
  );
  port (
    COUNT_O  : out std_logic_vector(CGN_TIMER_WIDTH-1 downto 0);
    TICK_O   : out std_logic;
    LD_I     : in  std_logic;
    LD_VAL_I : in std_logic_vector(CGN_TIMER_WIDTH-1 downto 0) := (others=>'0');
    CLK_I    : in  std_logic
  );
end util_tick_timer;

architecture behavioral of util_tick_timer is

  constant C_TICK_TIMER_LOAD     : integer := integer(CGN_TICK_TIME_NS/CGN_CLK_PERIOD_NS) - 1;
  constant C_TICK_TIMER_WIDTH    : integer := log2ceil(C_TICK_TIMER_LOAD);
  constant C_TICK_TIMER_LOAD_SLV : std_logic_vector(C_TICK_TIMER_WIDTH-1 downto 0) := CONV_STD_LOGIC_VECTOR(C_TICK_TIMER_LOAD, C_TICK_TIMER_WIDTH);

  signal tick_count_msbs : std_logic_vector(C_TICK_TIMER_WIDTH-1 downto 2) := (others=>'0');
  signal tick_count_lsbs : std_logic_vector(1 downto 0) := (others=>'0');
  signal tick_count      : std_logic_vector(C_TICK_TIMER_WIDTH-1 downto 0) := (others=>'0');
  signal tick            : std_logic := '0';
  signal count           : std_logic_vector(CGN_TIMER_WIDTH-1 downto 0) := (others=>'0');

begin

  COUNT_O <= count;
  TICK_O  <= tick;

  tick_count <= tick_count_msbs & tick_count_lsbs;

  tick_counter : process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if LD_I = '1' or tick_count = 0 then
        tick_count_msbs <= C_TICK_TIMER_LOAD_SLV(C_TICK_TIMER_WIDTH-1 downto 2);
        tick_count_lsbs <= C_TICK_TIMER_LOAD_SLV(1 downto 0);
      else
        tick_count_lsbs <= tick_count_lsbs - 1;
        if tick_count_lsbs = 0 then
          tick_count_msbs <= tick_count_msbs - 1;
        end if;
      end if;
    end if;
  end process;

  tick_generator : process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if tick_count = 0 then
        tick <= '1';
      else
        tick <= '0';
      end if;
    end if;
  end process;

  counter : process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if LD_I = '1' then
        count <= LD_VAL_I;
      else
        if tick = '1' then
          count <= count + 1;
        end if;
      end if;
    end if;
  end process;

end architecture behavioral;
