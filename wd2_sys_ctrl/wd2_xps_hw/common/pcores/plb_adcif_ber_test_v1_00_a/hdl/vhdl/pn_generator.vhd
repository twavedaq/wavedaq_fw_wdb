--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : pn_generator.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  se32
--  Created :  28.01.2016 13:49:43
--
--  Description :  generates pseudo random pattern
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

entity pn_generator is
  port
  (
    DATA_I       : in  std_logic_vector(11 downto 0);
    DATA_O       : out std_logic_vector(11 downto 0);
    LOAD_LO_I    : in  std_logic;
    LOAD_HI_I    : in  std_logic;
    ENABLE_I     : in  std_logic;
    PN_SELECT_I  : in  std_logic; -- 0 = PN9, 1 = PN23
    CLK_I        : in  std_logic
  );
end pn_generator;


architecture behavioral of pn_generator is

  signal pn9_lookahead  : std_logic_vector(11 downto 0) := (others=>'0');
  signal pn23_lookahead : std_logic_vector(11 downto 0) := (others=>'0');
  signal pn_shift_reg   : std_logic_vector(23 downto 0) := (others=>'0');

begin

  DATA_O <= pn_shift_reg(11 downto 0);

  pn9_lookahead_logic : process(pn_shift_reg)
  begin
    pn9_lookahead(11) <= pn_shift_reg(8) xor pn_shift_reg(4);
    pn9_lookahead(10) <= pn_shift_reg(7) xor pn_shift_reg(3);
    pn9_lookahead(9)  <= pn_shift_reg(6) xor pn_shift_reg(2);
    pn9_lookahead(8)  <= pn_shift_reg(5) xor pn_shift_reg(1);
    pn9_lookahead(7)  <= pn_shift_reg(4) xor pn_shift_reg(0);
    pn9_lookahead(6)  <= pn_shift_reg(3) xor (pn_shift_reg(8) xor pn_shift_reg(4));
    pn9_lookahead(5)  <= pn_shift_reg(2) xor (pn_shift_reg(7) xor pn_shift_reg(3));
    pn9_lookahead(4)  <= pn_shift_reg(1) xor (pn_shift_reg(6) xor pn_shift_reg(2));
    pn9_lookahead(3)  <= pn_shift_reg(0) xor (pn_shift_reg(5) xor pn_shift_reg(1));
    pn9_lookahead(2)  <= (pn_shift_reg(4) xor pn_shift_reg(0)) xor (pn_shift_reg(8) xor pn_shift_reg(4));
    pn9_lookahead(1)  <= (pn_shift_reg(7) xor pn_shift_reg(3)) xor ((pn_shift_reg(8) xor pn_shift_reg(4)) xor pn_shift_reg(3));
    pn9_lookahead(0)  <= (pn_shift_reg(6) xor pn_shift_reg(2)) xor ((pn_shift_reg(7) xor pn_shift_reg(3)) xor pn_shift_reg(2));
  end process pn9_lookahead_logic;

  pn23_lookahead_logic : process(pn_shift_reg)
  begin
    for i in 11 downto 0 loop
      pn23_lookahead(i) <= pn_shift_reg(i+11) xor pn_shift_reg(i+6);
    end loop;
  end process pn23_lookahead_logic;

  pn_shift_register : process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if LOAD_LO_I = '1' then
        pn_shift_reg(11 downto  0) <= DATA_I;
      elsif LOAD_HI_I = '1' then
        pn_shift_reg(23 downto 12) <= DATA_I;
      elsif ENABLE_I = '1' then
        if PN_SELECT_I = '0' then
          pn_shift_reg(11 downto  0) <= pn9_lookahead;
        else
          pn_shift_reg(11 downto  0) <= pn23_lookahead;
          pn_shift_reg(23 downto 12) <= pn_shift_reg(11 downto  0);
        end if;
      end if;
    end if;
  end process pn_shift_register;

end Behavioral;
