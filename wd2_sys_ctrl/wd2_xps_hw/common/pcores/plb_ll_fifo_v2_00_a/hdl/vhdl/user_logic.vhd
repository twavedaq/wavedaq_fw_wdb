--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : user_logic.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  tg32
--  Created :  2014.07.30
--
--  Description :  PLB acces to local link receive and transmit fifos
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library unisim;
use unisim.vcomponents.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.all;


--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------

entity user_logic is
  generic
  (
    -- ADD USER GENERICS BELOW THIS LINE ---------------
    C_FAMILY               : string  := "spartan6";  -- "spartan6" -- "virtex5";
    CGN_ISE_12_4           : boolean := true;       -- true
    CGN_ISE_14_7           : boolean := false;      -- false
    CGN_FIFO_DEPTH         : integer := 2**11;
    CGN_LL_I_DATA_WIDTH    : integer := 32;
    CGN_LL_I_REM_WIDTH     : integer := 2;
    CGN_LL_O_DATA_WIDTH    : integer := 32;
    CGN_LL_O_REM_WIDTH     : integer := 2;
    CGN_LL_I_BUFF_CONV     : integer := 1;
    CGN_LL_O_BUFF_CONV     : integer := 1;
    CGN_LL_I_BUFF          : integer := 1;
    CGN_LL_O_BUFF          : integer := 1;
    -- ADD USER GENERICS ABOVE THIS LINE ---------------

    -- DO NOT EDIT BELOW THIS LINE ---------------------
    -- Bus protocol parameters, do not add to or delete
    C_SLV_DWIDTH           : integer              := 32;
    C_NUM_REG              : integer              := 3
    -- DO NOT EDIT ABOVE THIS LINE ---------------------
  );
  port
  (
    -- ADD USER PORTS BELOW THIS LINE ------------------
    RESET_GT_PLB_CLK_O        : out std_logic;
    RESET_LINK_I_CLK_O        : out std_logic;
    RESET_LINK_O_CLK_O        : out std_logic;
    RESET_USER_I_CLK_O        : out std_logic;
    RESET_USER_O_CLK_O        : out std_logic;
    RESET_STATUS_O_CLK_O      : out std_logic;
    CONFIG_VECTOR_O           : out std_logic_vector(19 downto 0);
    STATUS_VECTOR_I           : in  std_logic_vector(19 downto 0);

    LL_I_CLK_I                : in  std_logic;
    LL_I_DATA_I               : in  std_logic_vector(CGN_LL_I_DATA_WIDTH-1 downto 0);
    LL_I_SOF_N_I              : in  std_logic;
    LL_I_EOF_N_I              : in  std_logic;
    LL_I_REM_I                : in  std_logic_vector(CGN_LL_I_REM_WIDTH-1 downto 0);
    LL_I_SRC_RDY_N_I          : in  std_logic;
    LL_I_DST_RDY_N_O          : out std_logic;

    LL_O_CLK_I                : in  std_logic;
    LL_O_DATA_O               : out std_logic_vector(CGN_LL_O_DATA_WIDTH-1 downto 0);
    LL_O_SOF_N_O              : out std_logic;
    LL_O_EOF_N_O              : out std_logic;
    LL_O_REM_O                : out std_logic_vector(CGN_LL_O_REM_WIDTH-1 downto 0);
    LL_O_SRC_RDY_N_O          : out std_logic;
    LL_O_DST_RDY_N_I          : in  std_logic;
    LL_O_PKT_OK_O             : OUT std_logic;
    LL_O_PKT_ERR_O            : OUT std_logic;
        
    -- ADD USER PORTS ABOVE THIS LINE ------------------

    -- DO NOT EDIT BELOW THIS LINE ---------------------
    -- Bus protocol ports, do not add to or delete
    Bus2IP_Clk                 : in  std_logic;
    Bus2IP_Reset               : in  std_logic;
    Bus2IP_Data                : in  std_logic_vector(0 to C_SLV_DWIDTH-1);
    Bus2IP_BE                  : in  std_logic_vector(0 to C_SLV_DWIDTH/8-1);
    Bus2IP_RdCE                : in  std_logic_vector(0 to C_NUM_REG-1);
    Bus2IP_WrCE                : in  std_logic_vector(0 to C_NUM_REG-1);
    IP2Bus_Data                : out std_logic_vector(0 to C_SLV_DWIDTH-1);
    IP2Bus_RdAck               : out std_logic;
    IP2Bus_WrAck               : out std_logic;
    IP2Bus_Error               : out std_logic
    -- DO NOT EDIT ABOVE THIS LINE ---------------------
  );

  attribute SIGIS : string;
  attribute SIGIS of Bus2IP_Clk    : signal is "CLK";
  attribute SIGIS of Bus2IP_Reset  : signal is "RST";

end entity user_logic;

--------------------------------------------------------------------------------
-- Architecture section
--------------------------------------------------------------------------------

architecture IMP of user_logic is

  ------------------------------------------
  -- Signals for user logic slave model s/w accessible register example
  ------------------------------------------
  signal slv_reg0              : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg1_r            : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg2_r            : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg_write_sel     : std_logic_vector(0 to 2);
  signal slv_reg_read_sel      : std_logic_vector(0 to 2);
  signal slv_ip2bus_data       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_read_ack          : std_logic;
  signal slv_write_ack         : std_logic;

  ------------------------------------------
  -- user regs in little endian
  ------------------------------------------

  signal ctrl_reg              : std_logic_vector(31 downto 0);
  signal fifo_status_reg       : std_logic_vector(31 downto 0);

  signal reset_gt_plb_clk      : std_logic;

  signal reset_link_plb_clk    : std_logic;
  signal reset_link_i_clk      : std_logic_vector(4 downto 0);
  signal reset_link_o_clk      : std_logic_vector(4 downto 0);

  signal reset_user_plb_clk    : std_logic;
  signal reset_user_i_clk      : std_logic_vector(4 downto 0);
  signal reset_user_o_clk      : std_logic_vector(4 downto 0);

  signal reset_status_plb_clk  : std_logic;
  signal reset_status_o_clk    : std_logic_vector(4 downto 0);

  ------------------------------------------
  -- TxFifo signals
  ------------------------------------------
  signal tx_ll32_rem           : std_logic_vector(1 downto 0);
  signal tx_ll32_eof_n         : std_logic;
  signal tx_ll32_sof_n         : std_logic;
  signal tx_ll32_data          : std_logic_vector(31 downto 0);
  signal tx_ll32_src_rdy_n     : std_logic;
  signal tx_ll32_dst_rdy_n     : std_logic;

  signal tx_fifo_almostempty   : std_logic;
  signal tx_fifo_almostfull    : std_logic;
  signal tx_fifo_rst           : std_logic;
  signal tx_fifo_rderr         : std_logic;
  signal tx_fifo_wrerr         : std_logic;

  ------------------------------------------
  -- RxFifo signals
  ------------------------------------------

  signal rx_ll32_data          : std_logic_vector(31 downto 0);
  signal rx_ll32_rem           : std_logic_vector(1 downto 0);
  signal rx_ll32_sof_n         : std_logic;
  signal rx_ll32_eof_n         : std_logic;
  signal rx_ll32_src_rdy_n     : std_logic;
  signal rx_ll32_dst_rdy_n     : std_logic;

  signal rx_fifo_almostempty   : std_logic;
  signal rx_fifo_almostfull    : std_logic;
  signal rx_fifo_rst           : std_logic;
  signal rx_fifo_rderr         : std_logic;
  signal rx_fifo_wrerr         : std_logic;

  ------------------------------------------
  -- Local Link signals
  ------------------------------------------

  signal ll32_us_data          : std_logic_vector(0 to 31);
  signal ll32_us_rem           : std_logic_vector(1 downto 0);
  signal ll32_us_sof_n         : std_logic;
  signal ll32_us_eof_n         : std_logic;
  signal ll32_us_src_rdy_n     : std_logic;
  signal ll32_us_dst_rdy_n     : std_logic;

  signal ll32_ds_data          : std_logic_vector(0 to 31);
  signal ll32_ds_rem           : std_logic_vector(1 downto 0);
  signal ll32_ds_sof_n         : std_logic;
  signal ll32_ds_eof_n         : std_logic;
  signal ll32_ds_src_rdy_n     : std_logic;
  signal ll32_ds_dst_rdy_n     : std_logic;
    
  signal ll_o_eof_n            : std_logic;
  signal ll_o_src_rdy_n        : std_logic;


begin

  ------------------------------------------------------------------------------
  -- Register Usage
  ------------------------------------------------------------------------------

  -- reg0 (C_BASEADDR + 0x00) : rw  ctrl_reg

  ctrl_reg    <= slv_reg0;

  tx_ll32_rem                   <=     ctrl_reg(31 downto 30) when (ctrl_reg(29) ='1') else "11" ; -- REM
  tx_ll32_eof_n                 <= not ctrl_reg(29); -- EOF
  tx_ll32_sof_n                 <= not ctrl_reg(28); -- SOF

  tx_fifo_rst                   <=     ctrl_reg(27) or Bus2IP_Reset;
  rx_fifo_rst                   <=     ctrl_reg(26) or Bus2IP_Reset;

  reset_status_plb_clk          <=     ctrl_reg(23) or Bus2IP_Reset;
  reset_user_plb_clk            <=     ctrl_reg(22) or Bus2IP_Reset;
  reset_link_plb_clk            <=     ctrl_reg(21) or Bus2IP_Reset;
  reset_gt_plb_clk              <=     ctrl_reg(20) or Bus2IP_Reset;

  CONFIG_VECTOR_O               <=     ctrl_reg(19 downto 0);

  -- reg1 (C_BASEADDR + 0x04):
  slv_reg1_r                    <= fifo_status_reg;


  fifo_status_reg(31 downto 28)  <= "0000" when rx_ll32_src_rdy_n='1' else (rx_ll32_rem & (not rx_ll32_eof_n) & (not rx_ll32_sof_n) ); -- for simulation! sim fails if rx_fifo_out_parity is undefined
  fifo_status_reg(27)            <= rx_ll32_src_rdy_n;    -- rx empty
  fifo_status_reg(26)            <= rx_fifo_almostempty;  -- rx almostempty
  fifo_status_reg(25)            <= ll32_us_dst_rdy_n;    -- rx full 
  fifo_status_reg(24)            <= rx_fifo_almostfull;   -- rx almostfull

  fifo_status_reg(23)            <= ll32_ds_src_rdy_n;    -- tx empty
  fifo_status_reg(22)            <= tx_fifo_almostempty;  -- tx almostempty
  fifo_status_reg(21)            <= tx_ll32_dst_rdy_n;    -- tx full 
  fifo_status_reg(20)            <= tx_fifo_almostfull;   -- tx almostfull

  fifo_status_reg(19 downto  0)  <= STATUS_VECTOR_I;

  -- reg2 (C_BASEADDR + 0x08):  r   read data from RxFifo
  --                            w   write data to TxFifo

  tx_ll32_src_rdy_n  <= '0'  when slv_reg_write_sel = "001" else '1';
  tx_ll32_data       <= Bus2IP_Data;

  rx_ll32_dst_rdy_n  <= '0'  when slv_reg_read_sel  = "001" else '1';
  slv_reg2_r         <= rx_ll32_data;

   --USER logic implementation added here

  ------------------------------------------
  -- Example code to read/write user logic slave model s/w accessible registers
  --
  -- Note:
  -- The example code presented here is to show you one way of reading/writing
  -- software accessible registers implemented in the user logic slave model.
  -- Each bit of the Bus2IP_WrCE/Bus2IP_RdCE signals is configured to correspond
  -- to one software accessible register by the top level template. For example,
  -- if you have four 32 bit software accessible registers in the user logic,
  -- you are basically operating on the following memory mapped registers:
  --
  --    Bus2IP_WrCE/Bus2IP_RdCE   Memory Mapped Register
  --                     "1000"   C_BASEADDR + 0x0
  --                     "0100"   C_BASEADDR + 0x4
  --                     "0010"   C_BASEADDR + 0x8
  --                     "0001"   C_BASEADDR + 0xC
  --
  ------------------------------------------
  slv_reg_write_sel <= Bus2IP_WrCE(0 to 2);
  slv_reg_read_sel  <= Bus2IP_RdCE(0 to 2);
  slv_write_ack     <= Bus2IP_WrCE(0) or Bus2IP_WrCE(1) or Bus2IP_WrCE(2);
  slv_read_ack      <= Bus2IP_RdCE(0) or Bus2IP_RdCE(1) or Bus2IP_RdCE(2);

  -- implement slave model software accessible register(s)
  SLAVE_REG_WRITE_PROC : process( Bus2IP_Clk ) is
  begin

    if Bus2IP_Clk'event and Bus2IP_Clk = '1' then
      if Bus2IP_Reset = '1' then
        slv_reg0 <= (others => '0');
--        slv_reg1 <= (others => '0');
--        slv_reg2 <= (others => '0');
      else
        case slv_reg_write_sel is
          when "100" =>
            for byte_index in 0 to (C_SLV_DWIDTH/8)-1 loop
              if ( Bus2IP_BE(byte_index) = '1' ) then
                slv_reg0(byte_index*8 to byte_index*8+7) <= Bus2IP_Data(byte_index*8 to byte_index*8+7);
              end if;
            end loop;
--          when "010" =>
--            for byte_index in 0 to (C_SLV_DWIDTH/8)-1 loop
--              if ( Bus2IP_BE(byte_index) = '1' ) then
--                slv_reg1(byte_index*8 to byte_index*8+7) <= Bus2IP_Data(byte_index*8 to byte_index*8+7);
--              end if;
--            end loop;
--          when "001" =>
--            for byte_index in 0 to (C_SLV_DWIDTH/8)-1 loop
--              if ( Bus2IP_BE(byte_index) = '1' ) then
--                slv_reg2(byte_index*8 to byte_index*8+7) <= Bus2IP_Data(byte_index*8 to byte_index*8+7);
--              end if;
--            end loop;
          when others => null;
        end case;
      end if;
    end if;

  end process SLAVE_REG_WRITE_PROC;

  -- implement slave model software accessible register(s) read mux
  SLAVE_REG_READ_PROC : process( slv_reg_read_sel, slv_reg0, slv_reg1_r, slv_reg2_r ) is
  begin

    case slv_reg_read_sel is
      when "100" => slv_ip2bus_data <= slv_reg0;
      when "010" => slv_ip2bus_data <= slv_reg1_r;
      when "001" => slv_ip2bus_data <= slv_reg2_r;
      when others => slv_ip2bus_data <= (others => '0');
    end case;

  end process SLAVE_REG_READ_PROC;

  ------------------------------------------
  -- Example code to drive IP to Bus signals
  ------------------------------------------
  IP2Bus_Data  <= slv_ip2bus_data when slv_read_ack = '1' else
                  (others => '0');

  IP2Bus_WrAck <= slv_write_ack;
  IP2Bus_RdAck <= slv_read_ack;
  IP2Bus_Error <= '0';

  ------------------------------------------------------------
  -- US mapping to externel LL8
  ------------------------------------------------------------

  generate_ll8_us: if CGN_LL_I_DATA_WIDTH = 8 generate
  begin


  ll8_to_ll32_inst :  entity psi_3205_v1_00_a.ll8_to_ll32
  port map
  (
    LL_CLK_I            => LL_I_CLK_I,
    RESET_I             => rx_fifo_rst,

    LL8_I_DATA_I        => LL_I_DATA_I,
    LL8_I_SOF_N_I       => LL_I_SOF_N_I,
    LL8_I_EOF_N_I       => LL_I_EOF_N_I,
    LL8_I_SRC_RDY_N_I   => LL_I_SRC_RDY_N_I,
    LL8_I_DST_RDY_N_O   => LL_I_DST_RDY_N_O,

    LL32_O_DATA_O       => ll32_us_data,
    LL32_O_SOF_N_O      => ll32_us_sof_n,
    LL32_O_EOF_N_O      => ll32_us_eof_n,
    LL32_O_REM_O        => ll32_us_rem,
    LL32_O_SRC_RDY_N_O  => ll32_us_src_rdy_n,
    LL32_O_DST_RDY_N_I  => ll32_us_dst_rdy_n
  );

  -- unused LL_I_REM_I

  end generate;


  ------------------------------------------------------------
  -- US mapping to externel LL16
  ------------------------------------------------------------

  generate_ll16_us: if CGN_LL_I_DATA_WIDTH = 16 generate
  begin

    ll16_to_ll32_inst :  entity psi_3205_v1_00_a.ll16_to_ll32
    generic map
    (
      CGN_LL_I_BUFF => CGN_LL_I_BUFF,
      CGN_LL_O_BUFF => CGN_LL_I_BUFF_CONV
    )
    port map
    (
      LL_CLK_I             => LL_I_CLK_I,
      RESET_I              => rx_fifo_rst,

      LL16_I_DATA_I       => LL_I_DATA_I,
      LL16_I_SOF_N_I      => LL_I_SOF_N_I,
      LL16_I_EOF_N_I      => LL_I_EOF_N_I,
      LL16_I_REM_I        => LL_I_REM_I,
      LL16_I_SRC_RDY_N_I  => LL_I_SRC_RDY_N_I,
      LL16_I_DST_RDY_N_O  => LL_I_DST_RDY_N_O,

      LL32_O_DATA_O       => ll32_us_data,
      LL32_O_SOF_N_O      => ll32_us_sof_n,
      LL32_O_EOF_N_O      => ll32_us_eof_n,
      LL32_O_REM_O        => ll32_us_rem,
      LL32_O_SRC_RDY_N_O  => ll32_us_src_rdy_n,
      LL32_O_DST_RDY_N_I  => ll32_us_dst_rdy_n
    );
  end generate;


  ------------------------------------------------------------
  -- US mapping to externel LL32
  ------------------------------------------------------------

  generate_ll32_us: if CGN_LL_I_DATA_WIDTH = 32 generate
  begin

    use_buff_us: if CGN_LL_I_BUFF > 0 generate
    begin
      dbuff_us : entity psi_3205_v1_00_a.ll_double_buff
      generic map
      (
       CGN_DATA_WIDTH => 32,
       CGN_REM_WIDTH  => 2
      )
      port map
      (
        CLK_I            => LL_I_CLK_I,
        RESET_I          => rx_fifo_rst,

        LL_I_DATA_I      => LL_I_DATA_I,
        LL_I_SOF_N_I     => LL_I_SOF_N_I,
        LL_I_EOF_N_I     => LL_I_EOF_N_I,
        LL_I_REM_I       => LL_I_REM_I,
        LL_I_SRC_RDY_N_I => LL_I_SRC_RDY_N_I,
        LL_I_DST_RDY_N_O => LL_I_DST_RDY_N_O,

        LL_O_DATA_O      => ll32_us_data,
        LL_O_SOF_N_O     => ll32_us_sof_n,
        LL_O_EOF_N_O     => ll32_us_eof_n,
        LL_O_REM_O       => ll32_us_rem,
        LL_O_SRC_RDY_N_O => ll32_us_src_rdy_n,
        LL_O_DST_RDY_N_I => ll32_us_dst_rdy_n
      );
    end generate use_buff_us;

    no_buff_us: if CGN_LL_I_BUFF = 0 generate
    begin
      ll32_us_data         <= LL_I_DATA_I;
      ll32_us_sof_n        <= LL_I_SOF_N_I;
      ll32_us_eof_n        <= LL_I_EOF_N_I;
      ll32_us_rem          <= LL_I_REM_I;
      ll32_us_src_rdy_n    <= LL_I_SRC_RDY_N_I;
      LL_I_DST_RDY_N_O    <= ll32_us_dst_rdy_n;
    end generate no_buff_us;

  end generate;

  ------------------------------------------------------------
  -- US mapping to externel LL64
  ------------------------------------------------------------

  generate_ll64_us: if CGN_LL_I_DATA_WIDTH = 64 generate
  begin

    ll64_to_ll32_inst :  entity psi_3205_v1_00_a.ll64_to_ll32
    generic map
    (
      CGN_LL_I_BUFF => CGN_LL_I_BUFF,
      CGN_LL_O_BUFF => CGN_LL_I_BUFF_CONV
    )
    port map
    (
      LL_CLK_I             => LL_I_CLK_I,
      RESET_I              => rx_fifo_rst,

      LL64_I_DATA_I       => LL_I_DATA_I,
      LL64_I_SOF_N_I      => LL_I_SOF_N_I,
      LL64_I_EOF_N_I      => LL_I_EOF_N_I,
      LL64_I_REM_I        => LL_I_REM_I,
      LL64_I_SRC_RDY_N_I  => LL_I_SRC_RDY_N_I,
      LL64_I_DST_RDY_N_O  => LL_I_DST_RDY_N_O,

      LL32_O_DATA_O       => ll32_us_data,
      LL32_O_SOF_N_O      => ll32_us_sof_n,
      LL32_O_EOF_N_O      => ll32_us_eof_n,
      LL32_O_REM_O        => ll32_us_rem,
      LL32_O_SRC_RDY_N_O  => ll32_us_src_rdy_n,
      LL32_O_DST_RDY_N_I  => ll32_us_dst_rdy_n
    );

  end generate;


  ------------------------------------------------------------
  --  RxFifo : 1k * 36 FIFO
  ------------------------------------------------------------

    rx_fifo_inst : entity psi_3205_v1_00_a.ll_fifo
    generic map
    (
      C_FAMILY                 => C_FAMILY,
      CGN_ISE_12_4             => CGN_ISE_12_4,
      CGN_ISE_14_7             => CGN_ISE_14_7,
      CGN_DATA_WIDTH           => 32,
      CGN_REM_WIDTH            => 2,
      CGN_ALMOST_EMPTY_OFFSET  => 128,            -- Sets the almost empty threshold
      CGN_ALMOST_FULL_OFFSET   => 128,            -- Sets almost full threshold
      CGN_FIFO_DEPTH           => CGN_FIFO_DEPTH,
      CGN_EOF_FIFO_DEPTH       => 0               -- remove empty flag after eof is written to fifo
    )
    port map
    (
      RESET_I              =>  rx_fifo_rst,

      LL_I_CLK_I           =>  LL_I_CLK_I,
      LL_I_DATA_I          =>  ll32_us_data,
      LL_I_REM_I           =>  ll32_us_rem,
      LL_I_SOF_N_I         =>  ll32_us_sof_n,
      LL_I_EOF_N_I         =>  ll32_us_eof_n,
      LL_I_SRC_RDY_N_I     =>  ll32_us_src_rdy_n,
      LL_I_DST_RDY_N_O     =>  ll32_us_dst_rdy_n,
      LL_I_ALMOST_FULL_O   =>  rx_fifo_almostfull,
      LL_I_ERR_O           =>  rx_fifo_wrerr,

      LL_O_CLK_I           =>  Bus2IP_Clk,
      LL_O_DATA_O          =>  rx_ll32_data,
      LL_O_REM_O           =>  rx_ll32_rem,
      LL_O_SOF_N_O         =>  rx_ll32_sof_n,
      LL_O_EOF_N_O         =>  rx_ll32_eof_n,
      LL_O_SRC_RDY_N_O     =>  rx_ll32_src_rdy_n,
      LL_O_ALMOST_EMPTY_O  =>  rx_fifo_almostempty,
      LL_O_ERR_O           =>  rx_fifo_rderr,
      LL_O_DST_RDY_N_I     =>  rx_ll32_dst_rdy_n
    );

  ------------------------------------------------------------
  --  TxFifo
  ------------------------------------------------------------


  tx_fifo_inst : entity psi_3205_v1_00_a.ll_fifo
    generic map
    (
      C_FAMILY                 => C_FAMILY,
      CGN_ISE_12_4             => CGN_ISE_12_4,
      CGN_ISE_14_7             => CGN_ISE_14_7,
      CGN_DATA_WIDTH           => 32,
      CGN_REM_WIDTH            => 2,
      CGN_ALMOST_EMPTY_OFFSET  => 128,            -- Sets the almost empty threshold
      CGN_ALMOST_FULL_OFFSET   => 128,            -- Sets almost full threshold
      CGN_FIFO_DEPTH           => CGN_FIFO_DEPTH,
      CGN_EOF_FIFO_DEPTH       => 8               -- remove empty flag after eof is written to fifo
    )
    port map
    (
      RESET_I              =>  tx_fifo_rst,

      LL_I_CLK_I           =>  Bus2IP_Clk,
      LL_I_DATA_I          =>  tx_ll32_data,
      LL_I_REM_I           =>  tx_ll32_rem,
      LL_I_SOF_N_I         =>  tx_ll32_sof_n,
      LL_I_EOF_N_I         =>  tx_ll32_eof_n,
      LL_I_SRC_RDY_N_I     =>  tx_ll32_src_rdy_n,
      LL_I_DST_RDY_N_O     =>  tx_ll32_dst_rdy_n,
      LL_I_ALMOST_FULL_O   =>  tx_fifo_almostfull,
      LL_I_ERR_O           =>  tx_fifo_wrerr,

      LL_O_CLK_I           =>  LL_O_CLK_I,
      LL_O_DATA_O          =>  ll32_ds_data,
      LL_O_REM_O           =>  ll32_ds_rem,
      LL_O_SOF_N_O         =>  ll32_ds_sof_n,
      LL_O_EOF_N_O         =>  ll32_ds_eof_n,
      LL_O_SRC_RDY_N_O     =>  ll32_ds_src_rdy_n,
      LL_O_ALMOST_EMPTY_O  =>  tx_fifo_almostempty,
      LL_O_ERR_O           =>  tx_fifo_rderr,
      LL_O_DST_RDY_N_I     =>  ll32_ds_dst_rdy_n
    );


  ------------------------------------------------------------
  -- DS mapping to externel LL8
  ------------------------------------------------------------

  generate_ll8_ds: if CGN_LL_O_DATA_WIDTH = 8 generate
  begin

    ll32_to_ll8_inst:  entity psi_3205_v1_00_a.ll32_to_ll8
    port map
    (
      LL_CLK_I         => LL_O_CLK_I,
      RESET_I          => tx_fifo_rst,

      LL32_I_DATA_I      => ll32_ds_data,
      LL32_I_SOF_N_I     => ll32_ds_sof_n,
      LL32_I_EOF_N_I     => ll32_ds_eof_n,
      LL32_I_REM_I       => ll32_ds_rem,
      LL32_I_SRC_RDY_N_I => ll32_ds_src_rdy_n,
      LL32_I_DST_RDY_N_O => ll32_ds_dst_rdy_n,

      LL8_O_DATA_O       => LL_O_DATA_O,
      LL8_O_SOF_N_O      => LL_O_SOF_N_O,
      LL8_O_EOF_N_O      => ll_o_eof_n,
      LL8_O_SRC_RDY_N_O  => ll_o_src_rdy_n,
      LL8_O_DST_RDY_N_I  => LL_O_DST_RDY_N_I
    );

    LL_O_REM_O <= (others => '0');

  end generate;


  ------------------------------------------------------------
  -- DS mapping to externel LL16
  ------------------------------------------------------------

  generate_ll16_ds: if CGN_LL_O_DATA_WIDTH = 16 generate
  begin

    ll32_to_ll16_inst :  entity psi_3205_v1_00_a.ll32_to_ll16
    generic map
    (
      CGN_LL_I_BUFF => CGN_LL_O_BUFF_CONV,
      CGN_LL_O_BUFF => CGN_LL_O_BUFF
    )
    port map
    (
      LL_CLK_I             => LL_O_CLK_I,
      RESET_I              => tx_fifo_rst,

      LL32_I_DATA_I       => ll32_ds_data,
      LL32_I_SOF_N_I      => ll32_ds_sof_n,
      LL32_I_EOF_N_I      => ll32_ds_eof_n,
      LL32_I_REM_I        => ll32_ds_rem,
      LL32_I_SRC_RDY_N_I  => ll32_ds_src_rdy_n,
      LL32_I_DST_RDY_N_O  => ll32_ds_dst_rdy_n,

      LL16_O_DATA_O       => LL_O_DATA_O,
      LL16_O_SOF_N_O      => LL_O_SOF_N_O,
      LL16_O_EOF_N_O      => ll_o_eof_n,
      LL16_O_REM_O        => LL_O_REM_O,
      LL16_O_SRC_RDY_N_O  => ll_o_src_rdy_n,
      LL16_O_DST_RDY_N_I  => LL_O_DST_RDY_N_I
    );

  end generate;


  ------------------------------------------------------------
  -- DS mapping to externel LL32
  ------------------------------------------------------------

  generate_ll32_ds: if CGN_LL_O_DATA_WIDTH = 32 generate
  begin

    use_buff_ds: if CGN_LL_O_BUFF > 0 generate
    begin
      dbuff_ds : entity  psi_3205_v1_00_a.ll_double_buff
      generic map
      (
       CGN_DATA_WIDTH => 32,
       CGN_REM_WIDTH  => 2
      )
      port map
      (
        CLK_I            => LL_O_CLK_I,
        RESET_I          => tx_fifo_rst,

        LL_I_DATA_I      => ll32_ds_data,
        LL_I_SOF_N_I     => ll32_ds_sof_n,
        LL_I_EOF_N_I     => ll32_ds_eof_n,
        LL_I_REM_I       => ll32_ds_rem,
        LL_I_SRC_RDY_N_I => ll32_ds_src_rdy_n,
        LL_I_DST_RDY_N_O => ll32_ds_dst_rdy_n,

        LL_O_DATA_O      => LL_O_DATA_O,
        LL_O_SOF_N_O     => LL_O_SOF_N_O,
        LL_O_EOF_N_O     => ll_o_eof_n,
        LL_O_REM_O       => LL_O_REM_O,
        LL_O_SRC_RDY_N_O => ll_o_src_rdy_n,
        LL_O_DST_RDY_N_I => LL_O_DST_RDY_N_I
      );
    end generate use_buff_ds;

    no_buff_ds: if CGN_LL_O_BUFF = 0 generate
    begin
      LL_O_DATA_O       <= ll32_ds_data;
      LL_O_REM_O        <= ll32_ds_rem;
      LL_O_EOF_N_O      <= ll32_ds_eof_n;
      LL_O_SOF_N_O      <= ll32_ds_sof_n;
      LL_O_SRC_RDY_N_O  <= ll32_ds_src_rdy_n;
      ll32_ds_dst_rdy_n  <= LL_O_DST_RDY_N_I;
    end generate no_buff_ds;

  end generate;


  ------------------------------------------------------------
  -- DS mapping to externel LL64
  ------------------------------------------------------------

  generate_ll64_ds: if CGN_LL_O_DATA_WIDTH = 64 generate
  begin

    ll32_to_ll64_inst :  entity psi_3205_v1_00_a.ll32_to_ll64
    generic map
    (
      CGN_LL_I_BUFF => CGN_LL_O_BUFF_CONV,
      CGN_LL_O_BUFF => CGN_LL_O_BUFF
    )
    port map
    (
      LL_CLK_I             => LL_O_CLK_I,
      RESET_I              => tx_fifo_rst,

      LL32_I_DATA_I       => ll32_ds_data,
      LL32_I_SOF_N_I      => ll32_ds_sof_n,
      LL32_I_EOF_N_I      => ll32_ds_eof_n,
      LL32_I_REM_I        => ll32_ds_rem,
      LL32_I_SRC_RDY_N_I  => ll32_ds_src_rdy_n,
      LL32_I_DST_RDY_N_O  => ll32_ds_dst_rdy_n,

      LL64_O_DATA_O       => LL_O_DATA_O,
      LL64_O_SOF_N_O      => LL_O_SOF_N_O,
      LL64_O_EOF_N_O      => ll_o_eof_n,
      LL64_O_REM_O        => LL_O_REM_O,
      LL64_O_SRC_RDY_N_O  => ll_o_src_rdy_n,
      LL64_O_DST_RDY_N_I  => LL_O_DST_RDY_N_I
    );

  end generate;


  --------------------------------------------------------------------------------
  -- reset
  --------------------------------------------------------------------------------

  process(LL_I_CLK_I) is
  begin
    if rising_edge(LL_I_CLK_I) then
      reset_link_i_clk(4)   <= reset_link_plb_clk;
      reset_link_i_clk(3)   <= reset_link_i_clk(4);
      reset_link_i_clk(2)   <= reset_link_i_clk(3);
      reset_link_i_clk(1)   <= reset_link_i_clk(2);
      reset_link_i_clk(0)   <= reset_link_i_clk(1) or reset_link_i_clk(2) or reset_link_i_clk(3);

      reset_user_i_clk(4)   <= reset_user_plb_clk;
      reset_user_i_clk(3)   <= reset_user_i_clk(4);
      reset_user_i_clk(2)   <= reset_user_i_clk(3);
      reset_user_i_clk(1)   <= reset_user_i_clk(2);
      reset_user_i_clk(0)   <= reset_user_i_clk(1) or reset_user_i_clk(2) or reset_user_i_clk(3);
    end if;
  end process;


  process(LL_O_CLK_I) is
  begin
    if rising_edge(LL_O_CLK_I) then
      reset_link_o_clk(4)   <= reset_link_plb_clk;
      reset_link_o_clk(3)   <= reset_link_o_clk(4);
      reset_link_o_clk(2)   <= reset_link_o_clk(3);
      reset_link_o_clk(1)   <= reset_link_o_clk(2);
      reset_link_o_clk(0)   <= reset_link_o_clk(1) or reset_link_o_clk(2) or reset_link_o_clk(3);

      reset_user_o_clk(4)   <= reset_user_plb_clk;
      reset_user_o_clk(3)   <= reset_user_o_clk(4);
      reset_user_o_clk(2)   <= reset_user_o_clk(3);
      reset_user_o_clk(1)   <= reset_user_o_clk(2);
      reset_user_o_clk(0)   <= reset_user_o_clk(1) or reset_user_o_clk(2) or reset_user_o_clk(3);
      
      reset_status_o_clk(4) <= reset_status_plb_clk;
      reset_status_o_clk(3) <= reset_status_o_clk(4);
      reset_status_o_clk(2) <= reset_status_o_clk(3);
      reset_status_o_clk(1) <= reset_status_o_clk(2);
      reset_status_o_clk(0) <= reset_status_o_clk(1) or reset_status_o_clk(2) or reset_status_o_clk(3);
    end if;
  end process;
  

  RESET_GT_PLB_CLK_O    <= reset_gt_plb_clk;

  RESET_LINK_I_CLK_O    <= reset_link_i_clk(0);
  RESET_LINK_O_CLK_O    <= reset_link_o_clk(0);

  RESET_USER_I_CLK_O    <= reset_user_i_clk(0);
  RESET_USER_O_CLK_O    <= reset_user_o_clk(0);

  RESET_STATUS_O_CLK_O  <= reset_status_o_clk(0);
  
    
   LL_O_EOF_N_O      <=  ll_o_eof_n;
   LL_O_SRC_RDY_N_O  <=  ll_o_src_rdy_n;
  
   LL_O_PKT_OK_O     <=  '1' when ((ll_o_eof_n = '0') and  (ll_o_src_rdy_n = '0') and (LL_O_DST_RDY_N_I = '0')) else '0';
   LL_O_PKT_ERR_O    <=  '0';

end IMP;
