---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Debug Signal MUX
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid (Author of generation script)
--  Created :  08.05.2017 13:03:48
--
--  Description :  Debug signal multiplexer for WD2.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.ALL;
Library UNISIM;
use UNISIM.vcomponents.all;

entity wd2_dbg_mux is
--  generic (
--    CGN_NR_OR_SEL_BITS : integer := 4
--  );
  port (
--    DBG_SIG_I     : in  std_logic_vector(2**CGN_NR_OR_SEL_BITS-1 downto 0);
    DBG_SIG_I     : in  std_logic_vector(15 downto 0);
    DBG_SIG_0_O   : out std_logic;
    DBG_SIG_F_O   : out std_logic;
    DBG_PIN_I     : in  std_logic;
    DBG_PIN_O     : out std_logic;
    DBG_PIN_T     : out std_logic;
    DBG_PIN_T_N_O : out std_logic;
--    DBG_SIG_SEL_I : in  std_logic_vector(CGN_NR_OR_SEL_BITS-1 downto 0)
    DBG_SIG_SEL_I : in  std_logic_vector(3 downto 0)
  );
end wd2_dbg_mux;

architecture behavioral of wd2_dbg_mux is

begin

  DBG_PIN_O <= DBG_SIG_I(CONV_INTEGER(DBG_SIG_SEL_I));

  DBG_SIG_0_O <= DBG_PIN_I when DBG_SIG_SEL_I = x"0" else '0';
  DBG_SIG_F_O <= DBG_PIN_I when DBG_SIG_SEL_I = x"F" else '0';

  DBG_PIN_T     <= '1' when (DBG_SIG_SEL_I = x"0" or DBG_SIG_SEL_I = x"F") else '0';
  DBG_PIN_T_N_O <= '0' when (DBG_SIG_SEL_I = x"0" or DBG_SIG_SEL_I = x"F") else '1';

end architecture behavioral;
