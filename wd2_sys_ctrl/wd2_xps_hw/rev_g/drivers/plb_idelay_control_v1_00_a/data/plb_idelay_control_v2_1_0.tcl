##############################################################################
## Filename:          /afs/psi.ch/project/genie1414/work/wavedream/firmware/wd2_sys_ctrl/wd2_xps_hw/drivers/plb_idelay_control_v1_00_a/data/plb_idelay_control_v2_1_0.tcl
## Description:       Microprocess Driver Command (tcl)
## Date:              Thu Sep 11 15:53:35 2014 (by Create and Import Peripheral Wizard)
##############################################################################

#uses "xillib.tcl"

proc generate {drv_handle} {
  xdefine_include_file $drv_handle "xparameters.h" "plb_idelay_control" "NUM_INSTANCES" "DEVICE_ID" "C_BASEADDR" "C_HIGHADDR" 
}
