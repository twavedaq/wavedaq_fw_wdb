---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  WD2 Busy Control
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid (Author of generation script)
--  Created :  22.05.2017 08:51:24
--
--  Description :  WD2 busy signal generator.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

entity wd2_busy_ctrl is
  port (
    -- internal busy signals
    DRS_CTRL_BUSY_DAQ_I    : in  std_logic;
    DRS_CTRL_BUSY_UC_O     : out std_logic;
    WD2_PKG_GEN_BUSY_DAQ_I : in  std_logic;
    WD2_PKG_GEN_BUSY_UC_O  : out std_logic;
    ADV_TRIG_BUSY_DAQ_I    : in  std_logic;
    ADV_TRIG_BUSY_UC_O     : out std_logic;
    -- wavedream system busy
    WD2_BUSY_DAQ_O         : out std_logic;
    WD2_BUSY_UC_O          : out std_logic;
    -- crate system busy
    SYS_BUSY_DAQ_O         : out std_logic;
    SYS_BUSY_UC_O          : out std_logic;
    SYS_BUSY_O             : out std_logic;
    -- system busy on backplane (wired or)
    SYS_BUSY_N_I           : in  std_logic;
    SYS_BUSY_N_O           : out std_logic;
    SYS_BUSY_N_T           : out std_logic;
    -- clocks
    DAQ_CLK_I              : in  std_logic;
    UC_CLK_I               : in  std_logic
  );
end wd2_busy_ctrl;

architecture behavioral of wd2_busy_ctrl is

  signal wd2_busy_daq : std_logic := '0';
  signal sys_busy     : std_logic := '0';

begin

  wd2_busy_daq <= DRS_CTRL_BUSY_DAQ_I or WD2_PKG_GEN_BUSY_DAQ_I or ADV_TRIG_BUSY_DAQ_I;
  sys_busy     <= not SYS_BUSY_N_I;

  WD2_BUSY_DAQ_O <= wd2_busy_daq;
  SYS_BUSY_N_O   <= '0';
  SYS_BUSY_N_T   <= not wd2_busy_daq;
  SYS_BUSY_O     <= sys_busy;

  sync_to_daq_clk_inst : entity psi_3205_v1_00_a.cdc_sync
  generic map
  (
    CGN_DATA_WIDTH          => 1,
    CGN_USE_INPUT_REG_A     => 0,
    CGN_USE_OUTPUT_REG_A    => 0,
    CGN_USE_GRAY_CONVERSION => 0,
    CGN_NUM_SYNC_REGS_B     => 2,
    CGN_NUM_OUTPUT_REGS_B   => 0,
    CGN_GRAY_TO_BIN_STYLE   => 0
  )
  port map
  (
    CLK_A_I    => UC_CLK_I,
    CLK_B_I    => DAQ_CLK_I,
    PORT_A_I(0) => sys_busy,
    PORT_B_O(0) => SYS_BUSY_DAQ_O
  );

  sync_to_uc_clk_inst : entity psi_3205_v1_00_a.cdc_sync
  generic map
  (
    CGN_DATA_WIDTH          => 5,
    CGN_USE_INPUT_REG_A     => 0,
    CGN_USE_OUTPUT_REG_A    => 0,
    CGN_USE_GRAY_CONVERSION => 0,
    CGN_NUM_SYNC_REGS_B     => 2,
    CGN_NUM_OUTPUT_REGS_B   => 0,
    CGN_GRAY_TO_BIN_STYLE   => 0
  )
  port map
  (
    CLK_A_I    => DAQ_CLK_I,
    CLK_B_I    => UC_CLK_I,
    PORT_A_I(0) => sys_busy,
    PORT_A_I(1) => wd2_busy_daq,
    PORT_A_I(2) => DRS_CTRL_BUSY_DAQ_I,
    PORT_A_I(3) => WD2_PKG_GEN_BUSY_DAQ_I,
    PORT_A_I(4) => ADV_TRIG_BUSY_DAQ_I,
    PORT_B_O(0) => SYS_BUSY_UC_O,
    PORT_B_O(1) => WD2_BUSY_UC_O,
    PORT_B_O(2) => DRS_CTRL_BUSY_UC_O,
    PORT_B_O(3) => WD2_PKG_GEN_BUSY_UC_O,
    PORT_B_O(4) => ADV_TRIG_BUSY_UC_O
  );

end architecture behavioral;
