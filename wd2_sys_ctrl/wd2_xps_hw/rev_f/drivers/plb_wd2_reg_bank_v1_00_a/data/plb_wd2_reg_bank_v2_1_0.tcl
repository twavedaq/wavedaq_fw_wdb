##############################################################################
## Filename:          /afs/psi.ch/project/genie1414/work/wavedream/firmware/wd2_sys_ctrl/wd2_xps_hw/drivers/plb_wd2_reg_bank_v1_00_a/data/plb_wd2_reg_bank_v2_1_0.tcl
## Description:       Microprocess Driver Command (tcl)
## Date:              Thu Aug 14 12:00:46 2014 (by Create and Import Peripheral Wizard)
##############################################################################

#uses "xillib.tcl"

proc generate {drv_handle} {
  xdefine_include_file $drv_handle "xparameters.h" "plb_wd2_reg_bank" "NUM_INSTANCES" "DEVICE_ID" "C_MEM0_BASEADDR" "C_MEM0_HIGHADDR" "C_MEM1_BASEADDR" "C_MEM1_HIGHADDR" 
}
