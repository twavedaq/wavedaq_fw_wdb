##############################################################################
## Filename:          /afs/psi.ch/project/genie1414/work/wd2_repository/firmware/WD2/wd2_sys_ctrl/wd2_xps_hw/drivers/plb_test_frame_ctrl_v1_00_a/data/plb_test_frame_ctrl_v2_1_0.tcl
## Description:       Microprocess Driver Command (tcl)
## Date:              Tue Apr 14 12:36:14 2015 (by Create and Import Peripheral Wizard)
##############################################################################

#uses "xillib.tcl"

proc generate {drv_handle} {
  xdefine_include_file $drv_handle "xparameters.h" "plb_test_frame_ctrl" "NUM_INSTANCES" "DEVICE_ID" "C_BASEADDR" "C_HIGHADDR" 
}
