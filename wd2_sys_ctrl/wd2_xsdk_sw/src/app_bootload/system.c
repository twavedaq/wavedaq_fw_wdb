/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e, theidel
 *  Created :  12.02.2018 13:24:35
 *
 *  Description :  Central control for hardware access.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

/******************************************************************************/
/* include files                                                              */
/******************************************************************************/

#include "system.h"
#include "xparameters.h"
#include "gpio_slow_control.h"
#include "wd2_flash_memory_map.h"
#include "fw_env.h"
#include "utilities.h"
#include "xfs_printf.h"
#include "tftp_config.h"
#include "register_map_wd2.h"
#include "dbg.h"
#include "git-revision.h"
/*#include "drv_soft_spi_lowlevel_lmk.h"*/
/*
#include "dhcp.h"
*/

/******************************************************************************/
/* global vars                                                                */
/******************************************************************************/

/* global hardware system variable */
system_type system_hw;

const char system_sw_build_date[] = __DATE__;
const char system_sw_build_time[] = __TIME__;
const char *system_month_str[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

fw_env_trigger_entry_type fw_env_trigger_table[];
unsigned char gmac_com_buff[GMAC_COM_BUFF_LEN];

/******************************************************************************/
/******************************************************************************/

int fw_env_storage_handler_spi(fw_env_storage_desc_type *storage_desc, unsigned int cmd, char* mem_addr, unsigned int size)
{
  if (cmd == FW_ENV_STORAGE_READ)
  {
    if( DBG_INF0 ) xfs_local_printf("reading SPI flash: %d bytes at 0x%08x\r\n", size, storage_desc->addr);
    spi_flash_read( (spi_flash_w25q64*)(storage_desc->storage_ptr), (unsigned char*) mem_addr, storage_desc->addr, size);
    return FW_ENV_STORAGE_OK;
  /* tbd: check errors /  return FW_ENV_STORAGE_ERROR; */

  }
  else if (cmd == FW_ENV_STORAGE_WRITE)
  {
    if( DBG_INF0 ) xfs_local_printf("writing SPI flash: %d bytes at 0x%08x\r\n", size, storage_desc->addr);

    spi_flash_sect_start_range_erase((spi_flash_w25q64*)storage_desc->storage_ptr, storage_desc->addr, size);
    spi_flash_write((spi_flash_w25q64*)storage_desc->storage_ptr, (unsigned char*) mem_addr, storage_desc->addr, size);

    return FW_ENV_STORAGE_OK;
  /* tbd: check errors /  return FW_ENV_STORAGE_ERROR; */

  }

  return FW_ENV_STORAGE_ERROR;
}

/******************************************************************************/

fw_env_storage_desc_type env_storage_description[] =
{
   { SYSPTR(spi_flash), SPI_FLASH_ENVIRONMENT_ADDR,   fw_env_storage_handler_spi}
/*   { SYSPTR(spi_flash), CB_SPI_FLASH_ENV0_ADDR,   fw_env_storage_handler_spi},  */
/*   { SYSPTR(spi_flash), CB_SPI_FLASH_ENV1_ADDR,   fw_env_storage_handler_spi}   */
};

#define ENV_STORAGE_ENTRIES (sizeof(env_storage_description)/sizeof(fw_env_storage_desc_type))

/******************************************************************************/

char env_mem[SPI_FLASH_ENVIRONMENT_SIZE];

char default_env[] = "sn=0\0"
                     "hostname=WD2new\0"
                     "ethaddr=00:50:c2:46:d9:04\0"
                     "ipaddr=10.1.0.1\0"
                     "ethdhcp=1\0"
                     "\0";

#define DEFAULT_ENV_SIZE  sizeof(default_env)

/******************************************************************************/

xfs_u32 system_env_init(void)
{
  /* SPI Flash */
  /* Initialized here because the            */
  /* envirenment variables are stored in the */
  /* SPI flash memory.                       */
/*  spi_flash_init(spi_flash_ptr, XPAR_PLB_SPI_MASTER_CONFIG_MEMORY_BASEADDR); */

  fw_env_init(SYSPTR(env), env_mem, SPI_FLASH_ENVIRONMENT_SIZE, default_env, DEFAULT_ENV_SIZE, env_storage_description, ENV_STORAGE_ENTRIES, fw_env_trigger_table);

  fw_env_load(SYSPTR(env));

  return 0;
}

/******************************************************************************/

xfs_u32 system_init(void)
{
  unsigned int an_adv_config_vector = 0x21;
  unsigned int configuration_vector = 0x10;
  unsigned int link_timer_value0    = 0x13d;
  unsigned int cfg;

  const char initial_hostname[] = "WD2new";
  const unsigned char initial_eth_mac_addr[6] = { 0x00, 0x50, 0xc2, 0x46, 0xd9, 0x04 } ;
  const unsigned char initial_eth_ip_addr[4]  = { 10, 1, 0, 1} ;
  const unsigned int  initial_dhcp = 1;

  /* init register bank */
  reg_bank_init(XPAR_PLB_WD2_REG_BANK_0_MEM0_BASEADDR, XPAR_PLB_WD2_REG_BANK_0_MEM1_BASEADDR);

  /*
  reg_val = 0x00026464;
  reg_bank_write(WD2_REG_CLK_CTRL, &reg_val, 1);

  lmk03000_init();
  */

  plb_gpio_init(SYSPTR(gpio_sys), XPAR_PLB_GPIO_0_BASEADDR);
  gpio_sc_init();
  gpio_sc_set_sw_status(GPIO_LED_SW_STATUS_BL_LOAD);

  cfg =  (configuration_vector << 25)  |   (link_timer_value0 << 16)  | an_adv_config_vector;

  xfs_out32(XPAR_GMAC_LITE_0_BASEADDR + 4 * 1 , 0x1);  /* reset */
  xfs_out32(XPAR_GMAC_LITE_0_BASEADDR + 4 * 4 , cfg);
  xfs_out32(XPAR_GMAC_LITE_0_BASEADDR + 4 * 2 , 0x1);  /* clr reset */

  plb_ll_fifo_init(SYSPTR(plb_fifo_gmac_0), XPAR_PLB_LL_FIFO_GMAC_LITE_0_BASEADDR);


  gmac_lite_construct(SYSPTR(gmac_lite_0), XPAR_GMAC_LITE_0_BASEADDR);
  nw_if_construct(SYSPTR(nw_if_gmac_0), "", SYSPTR(plb_fifo_gmac_0), SYSPTR(dhcp_gmac_0), SYSPTR(gmac_lite_0), gmac_com_buff, GMAC_COM_BUFF_LEN);


  nw_if_init(SYSPTR(nw_if_gmac_0), initial_eth_mac_addr, initial_eth_ip_addr, initial_dhcp);
  ncpy(system_hw.cfg.hostname, initial_hostname, NAME_BUF_LEN);

  return 0;
}

/******************************************************************************/

void set_hostname_func(const char *name, const char* val)
{
  ncpy(SYSPTR(cfg)->hostname, val, MAX_HOSTNAME_LENGTH);
  SYSPTR(cfg)->hostname[MAX_HOSTNAME_LENGTH-1]=0;
};

/******************************************************************************/

void set_sn_func(const char *name, const char* val)
{
  SYSPTR(cfg)->serial_no = xfs_atoi(val);
};

/******************************************************************************/

void set_dhcp_func(const char *name, const char* val)
{
  nw_if_set_dhcp(SYSPTR(nw_if_gmac_0), xfs_atoi(val));

  if (!SYSPTR(nw_if_gmac_0)->do_dhcp)
  {
    int status;
    unsigned char ip_addr[4];
    status = parse_ip(fw_getenv(SYSPTR(env), "ipaddr"), ip_addr);
    if (status == XFS_OK)
    {
      nw_if_set_ip_address(SYSPTR(nw_if_gmac_0), ip_addr);
    }
  }
  else
  {
    /* xfs_local_printf("dhcp enabled\r\n"); */
  }
};

/************************************************************/

void set_mac_func(const char *name, const char* val)
{
  int status;
  unsigned char mac_addr[6];

  status = parse_mac(val, mac_addr);
  if (status == XFS_OK)
  {
    /* xfs_local_printf("MAC from env : %02X:%02X:%02X:%02X:%02X:%02X\r\n", mac_addr[0], mac_addr[1],mac_addr[2],mac_addr[3],mac_addr[4],mac_addr[5] ); */
    nw_if_set_mac_address(SYSPTR(nw_if_gmac_0), mac_addr);
  }
};

/************************************************************/

void set_ip_func(const char *name, const char* val)
{
  if (!SYSPTR(nw_if_gmac_0)->do_dhcp)
  {
    int status;
    unsigned char ip_addr[4];

    status = parse_ip(val, ip_addr);
    if (status == XFS_OK)
    {
      /* xfs_local_printf("Set IP from env : %d.%d.%d.%d\r\n",  ip_addr[0], ip_addr[1], ip_addr[2], ip_addr[3]); */
      nw_if_set_ip_address(SYSPTR(nw_if_gmac_0), ip_addr);
    }
  }
};


/******************************************************************************/

void set_dbglvl_func(const char *name, const char* val)
{
  unsigned int d;

  d = get_dbg_level();
  xfs_local_printf("Old dbglvl: %2d  (%s)\r\n", d, dbg_level_str[d]);

  d = parse_dbglvl(val);
  set_dbg_level(d);

  d = get_dbg_level();
  xfs_local_printf("New dbglvl: %2d  (%s)\r\n", d, dbg_level_str[d]);
}

/******************************************************************************/

/*
void trg_func(const char *name, const char* val)
{
  xfs_local_printf("tbd:  trg_func %s %S\r\n", name, val);
};
*/

/******************************************************************************/

fw_env_trigger_entry_type fw_env_trigger_table[] =
{
  {"hostname",   set_hostname_func},
  {"sn",         set_sn_func},
  {"ipaddr",     set_ip_func},
  {"ethaddr",    set_mac_func},
  {"ethdhcp",    set_dhcp_func},
/*  {"dbglvl",     set_dbglvl_func}, */
  {NULL, NULL}
};



/******************************************************************************/

void init_settings(int snr)
{
  int cmd_buf_size = 10;
  int name_buf_size = 10;
  int val_buf_size = 30;
  char command[cmd_buf_size];
  char name[name_buf_size];
  char value[val_buf_size];
  char *buffer_ptr[3] = {command, name, value};
  unsigned int board_variant;

  board_variant = (reg_bank_get(WD2_BOARD_VARIANT_REG) & WD2_BOARD_VARIANT_MASK) >> WD2_BOARD_VARIANT_OFS;

  xfs_snprintf(command, cmd_buf_size, "setenv\0");

  /*** Set Environment ******************************************/
  /* set envirnment serial number */
  xfs_snprintf(name,  name_buf_size, "sn\0");
  xfs_snprintf(value, val_buf_size,  "%d\0", (unsigned int)snr);
  xfs_printf("%s %s %s\r\n", command, name, value);
  fw_setenv(SYSPTR(env), 3, buffer_ptr);
  /* set envirnment hostname */
  xfs_snprintf(name,  name_buf_size, "hostname\0");
  xfs_snprintf(value, val_buf_size,  "wd%03d\0", (unsigned int)snr);
  xfs_printf("%s %s %s\r\n", command, name, value);
  fw_setenv(SYSPTR(env), 3, buffer_ptr);
  /* set envirnment mac address */
  if ( (board_variant & BOARD_VARIANT_ETHERNET_MASK) == 0)
  {
    xfs_snprintf(name,  name_buf_size, "ethaddr\0");
    xfs_snprintf(value, val_buf_size,  "00:50:C2:46:D4:%02X\0", (unsigned int)snr);
    xfs_printf("%s %s %s\r\n", command, name, value);
    fw_setenv(SYSPTR(env), 3, buffer_ptr);
  }

  /* set envirnment dhcp */
  xfs_snprintf(name,  name_buf_size, "ethdhcp\0");
  if ( (board_variant & BOARD_VARIANT_ETHERNET_MASK) == 0)
  {
    xfs_snprintf(value, val_buf_size,  "1\0");
  }
  else
  {
    xfs_snprintf(value, val_buf_size,  "0\0");
  }
  xfs_printf("%s %s %s\r\n", command, name, value);
  fw_setenv(SYSPTR(env), 3, buffer_ptr);
  /* set envirnment debug level */
  xfs_snprintf(name,  name_buf_size, "dbglvl\0");
  xfs_snprintf(value, val_buf_size,  "none\0");
  xfs_printf("%s %s %s\r\n", command, name, value);
  fw_setenv(SYSPTR(env), 3, buffer_ptr);
  /* set envirnment static ip address */
  xfs_snprintf(name,  name_buf_size, "ipaddr\0");
  xfs_snprintf(value, val_buf_size,  "10.1.0.1\0");
  xfs_printf("%s %s %s\r\n", command, name, value);
  fw_setenv(SYSPTR(env), 3, buffer_ptr);
  /* set envirnment udp source port */
  xfs_snprintf(name,  name_buf_size, "srcport\0");
  xfs_snprintf(value, val_buf_size,  "3000\0");
  xfs_printf("%s %s %s\r\n", command, name, value);
  fw_setenv(SYSPTR(env), 3, buffer_ptr);
  /* set envirnment side bank definition for scalers */
  /*
  xfs_snprintf(name,  name_buf_size, "sd\0");
  xfs_snprintf(value, val_buf_size,  "scal,0x%08X,34,t,10\0", XPAR_PLB_WD2_REG_BANK_0_MEM1_BASEADDR+REG_SCALER_0_LSB_OFFSET);
  xfs_printf("%s %s %s\r\n", command, name, value);
  fw_setenv(3, buffer_ptr);
  */

  /* store values in SPI flash */
  fw_env_save(SYSPTR(env));
  /**************************************************************/

  /**************************************************************/
  xfs_printf("\r\n*** Environment Initialization Complete ***\r\n\r\n");
}

/******************************************************************************/

void print_sys_info(void)
{
  /*unsigned int version; */
  unsigned int build_date;
  unsigned int build_time;
  unsigned int git_rev_fw;
  unsigned int hw_version;

  unsigned short year;
  unsigned char month;
  unsigned char day;

  unsigned char hour;
  unsigned char minute;
  unsigned char second;
  /*unsigned char compat_level;*/

  unsigned char board_variant;
  unsigned char board_type;
  unsigned char board_revision;

  /* version = xfs_in32(baseaddr); */
  build_date = reg_bank_get(WD2_REG_FW_BUILD_DATE);
  build_time = reg_bank_get(WD2_REG_FW_BUILD_TIME);
  git_rev_fw = reg_bank_get(WD2_REG_FW_GIT_HASH_TAG);
  hw_version = reg_bank_get(WD2_REG_HW_VER);

  /* read and convert from BCD to binary */
  year         = (build_date & WD2_FW_BUILD_YEAR_MASK)  >> WD2_FW_BUILD_YEAR_OFS;
  year         = ((year >> 12) & 0xF) * 1000 + ((year >> 8) & 0xF) * 100 + ((year >> 4) & 0xF) * 10 + (year & 0xF);
  month        = (build_date & WD2_FW_BUILD_MONTH_MASK) >> WD2_FW_BUILD_MONTH_OFS;
  month        = ((month >> 4) & 0xF) * 10 + (month & 0xF);
  day          = (build_date & WD2_FW_BUILD_DAY_MASK)   >> WD2_FW_BUILD_DAY_OFS;
  day          = ((day >> 4) & 0xF) * 10 + (day & 0xF);

  /*compat_level = (build_time & WD2_FW_COMPAT_LEVEL_MASK) >> WD2_FW_COMPAT_LEVEL_OFS;*/
  /* read and convert from BCD to binary */
  hour         = (build_time & WD2_FW_BUILD_HOUR_MASK)   >> WD2_FW_BUILD_HOUR_OFS;
  hour         = ((hour >> 4) & 0xF) * 10 + (hour & 0xF);
  minute       = (build_time & WD2_FW_BUILD_MINUTE_MASK) >> WD2_FW_BUILD_MINUTE_OFS;
  minute       = ((minute >> 4) & 0xF) * 10 + (minute & 0xF);
  second       = (build_time & WD2_FW_BUILD_SECOND_MASK) >> WD2_FW_BUILD_SECOND_OFS;
  second       = ((second >> 4) & 0xF) * 10 + (second & 0xF);

  board_variant  = (hw_version & WD2_BOARD_VARIANT_MASK)  >> WD2_BOARD_VARIANT_OFS;
  board_type     = (hw_version & WD2_BOARD_TYPE_MASK)     >> WD2_BOARD_TYPE_OFS;
  board_revision = (hw_version & WD2_BOARD_REVISION_MASK) >> WD2_BOARD_REVISION_OFS;

  /*xfs_local_printf("-- Compatibility Level: %d\r\n\r\n", compat_level);*/
  xfs_local_printf("-- FW GIT Revision:     0x%07X\r\n", git_rev_fw);
  xfs_local_printf("-- SW GIT Revision:     0x%X (Bootloader)\r\n\r\n", GIT_REV_HASH);
  xfs_local_printf("-- FW Build:            %s %2d %04d  %02d:%02d:%02d\r\n", system_month_str[(month-1)%12], day, year, hour, minute, second);
  xfs_local_printf("-- SW Build:            %s  %s (Bootloader)\r\n\r\n",system_sw_build_date,system_sw_build_time);
  if(board_type == 0x02) xfs_local_printf("-- Board Type:          WaveDREAM2\r\n");
  else                   xfs_local_printf("-- Board Type:          0x%02X -> error\r\n", board_type);
  xfs_local_printf("-- Board Revision:      %c\r\n", 0x41 + board_revision); /* 0x41 = ASCII "A" */
  xfs_local_printf("-- Board Variant:       0x%02X\r\n", board_variant);
}

/******************************************************************************/
