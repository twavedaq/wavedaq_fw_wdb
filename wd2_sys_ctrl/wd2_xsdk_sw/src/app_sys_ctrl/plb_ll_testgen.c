/******************************************************************************/
/*                                                                            */
/*  file: plb_ll_testgen.h                                                    */
/*                                                                            */
/*  (c) 2011 PSI tg32                                                         */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/* Include Files                                                              */
/******************************************************************************/

#include "plb_ll_testgen.h"
#include "utilities.h"

/******************************************************************************/

void plb_ll_testgen_init(plb_ll_testgen_type *self, unsigned int base_addr)
{
  self->base_addr = base_addr;
  plb_ll_testgen_reset(self);
};

/******************************************************************************/

void plb_ll_testgen_reset(plb_ll_testgen_type *self)
{
  xfs_out32(self->base_addr + 4*PLB_LL64_TESTGEN_REG_CTRL, PLB_LL64_TESTGEN_CTRL_RESET );
  xfs_out32(self->base_addr + 4*PLB_LL64_TESTGEN_REG_CTRL, PLB_LL64_TESTGEN_CTRL_RESET );
  xfs_out32(self->base_addr + 4*PLB_LL64_TESTGEN_REG_CTRL, 0 );
}

/******************************************************************************/


void plb_ll_testgen_stop(plb_ll_testgen_type *self)
{
  xfs_out32(self->base_addr + 4*PLB_LL64_TESTGEN_REG_CTRL, PLB_LL64_TESTGEN_CTRL_STOP );
}

/******************************************************************************/


int plb_ll_testgen_running(plb_ll_testgen_type *self)
{
  return (xfs_in32(self->base_addr + 4*PLB_LL64_TESTGEN_REG_STATUS) & PLB_LL64_TESTGEN_STAUS_RUNNING);
}

/******************************************************************************/

int plb_ll_testgen_start(  plb_ll_testgen_type *self,
                           unsigned int header,
                           unsigned int data_len,
                           unsigned int total_frames,
                           unsigned int server_num,
                           unsigned int frames_per_server,
                           unsigned int delay,
                           unsigned int dtype
                           )
{
  unsigned int sel_12;
  /* stop */
  xfs_out32(self->base_addr + 4*PLB_LL64_TESTGEN_REG_CTRL, PLB_LL64_TESTGEN_CTRL_STOP);
  
  if (data_len < 1) return -1;
  if (total_frames < 1) return -1;
  if (server_num < 1) return -1;
  if (frames_per_server < 1) return -1;
  if (delay < 1) delay = 1;

  if (dtype == 12)
  {
    sel_12 = PLB_LL64_TESTGEN_CTRL_SEL_12;
  }
  else
  {
    sel_12 = 0;
  }

  xfs_out32(self->base_addr + 4*PLB_LL64_TESTGEN_REG_HEADER, header);
  xfs_out32(self->base_addr + 4*PLB_LL64_TESTGEN_REG_DATA_LOOP, data_len-1);
  xfs_out32(self->base_addr + 4*PLB_LL64_TESTGEN_REG_COUNT_TOTAL, total_frames-1);
  xfs_out32(self->base_addr + 4*PLB_LL64_TESTGEN_REG_SERVER_NUM, server_num-1);
  xfs_out32(self->base_addr + 4*PLB_LL64_TESTGEN_REG_LOOP_SERV_NUM, frames_per_server-1);
  xfs_out32(self->base_addr + 4*PLB_LL64_TESTGEN_REG_COUNT_IFG, delay-1);

  /* wait until stopped */
  while(plb_ll_testgen_running(self));

  /* start */
  xfs_out32(self->base_addr + 4*PLB_LL64_TESTGEN_REG_CTRL, (PLB_LL64_TESTGEN_CTRL_START | sel_12));
  
  return 0;
}

/******************************************************************************/
/******************************************************************************/


