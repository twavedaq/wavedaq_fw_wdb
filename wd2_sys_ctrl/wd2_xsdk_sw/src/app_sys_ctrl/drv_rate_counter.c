/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  26.03.2018 10:39:17
 *
 *  Description :  Driver for trigger rate counter(scaler).
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#include "drv_rate_counter.h"
#include "system.h"
#include "register_map_wd2.h"
#include "xfs_printf.h"

/******************************************************************************/
/* Local Defines                                                              */
/******************************************************************************/

#define RCNT_STAT_REG_OFS       0x00
#define RCNT_CTRL_REG_OFS       0x04
#define RCNT_CTRL_REG_SET_OFS   0x08
#define RCNT_CTRL_REG_CLR_OFS   0x0C

#define RCNT_CTRL_LOCK_BUF_MASK              0x00000001
#define RCNT_STAT_CURRENT_BUF_MASK           0x00000003

#define RCNT_BUF_SEL_MASK                    0x000000C0
#define RCNT_BUF_SEL_OFS                          (6+2) /* 2 -> 32bit access */

#define RCNT_NR_OF_COUNTERS                          19
#define RCNT_TIME_STAMP_OFS   (4*2*RCNT_NR_OF_COUNTERS)

/******************************************************************************/
/* Global Variables                                                           */
/******************************************************************************/
unsigned int reg_base_addr;
unsigned int mem_base_addr;
unsigned long long counter_values[2][RCNT_NR_OF_COUNTERS];
unsigned long long daq_sys_time[2];
unsigned int past_idx = 1;
unsigned int current_idx = 0;

/******************************************************************************/

void rate_counter_init(unsigned int reg_base_address, unsigned int mem_base_address)
{
  reg_base_addr = reg_base_address;
  mem_base_addr = mem_base_address;
}

/******************************************************************************/

void rate_counter_lock_buffer()
{
  xfs_out32(reg_base_addr+RCNT_CTRL_REG_SET_OFS, RCNT_CTRL_LOCK_BUF_MASK);
}

/******************************************************************************/

void rate_counter_unlock_buffer()
{
  xfs_out32(reg_base_addr+RCNT_CTRL_REG_CLR_OFS, RCNT_CTRL_LOCK_BUF_MASK);
}

/******************************************************************************/

unsigned int rate_counter_get_buff_sel()
{
  return (xfs_in32(reg_base_addr+RCNT_STAT_REG_OFS) & RCNT_STAT_CURRENT_BUF_MASK);
}

/******************************************************************************/

void rate_counter_get_buffer()
{
  unsigned int buff_addr;
  unsigned long long data;
  int i;

  /* update indices */
  past_idx    = (past_idx + 1   ) & 0x01;
  current_idx = (current_idx + 1) & 0x01;

  rate_counter_lock_buffer();
  buff_addr = mem_base_addr + (rate_counter_get_buff_sel() << RCNT_BUF_SEL_OFS);

  /* copy counter values */
  for(i=0;i<RCNT_NR_OF_COUNTERS;i++)
  {
    data = (unsigned long long)xfs_in32(buff_addr+4*(i*2+1));
    data = data << 32;
    data = data | (unsigned long long)(xfs_in32(buff_addr+4*(i*2)));
    counter_values[current_idx][i] = data;
  }
  /* copy daq system time */
  data = (unsigned long long)xfs_in32(buff_addr+RCNT_TIME_STAMP_OFS+4);
  data = data << 32;
  data = data | (unsigned long long)(xfs_in32(buff_addr+RCNT_TIME_STAMP_OFS));
  daq_sys_time[current_idx] = data;

  rate_counter_unlock_buffer();
}

/******************************************************************************/

void rate_counter_update_rates()
{
  unsigned long long dt;
  unsigned int reg_ofs;
  unsigned int data;
  int i;

  reg_ofs = WD2_REG_SCALER_0;

  rate_counter_get_buffer();

  dt = daq_sys_time[current_idx] - daq_sys_time[past_idx];

  /* write rate values to registers */
  for(i=0;i<RCNT_NR_OF_COUNTERS;i++)
  {
    data = (unsigned int)( ( counter_values[current_idx][i] - counter_values[past_idx][i] ) * WD2_DAQ_FREQ / dt );
    reg_bank_write(reg_ofs, &data, 1);
    reg_ofs += 4;
  }

  /* write time stamp to registers */
  data = (unsigned int)daq_sys_time[current_idx];
  reg_bank_write(WD2_REG_SCALER_TIME_STAMP_LSB, &data, 1);
  data = (unsigned int)(daq_sys_time[current_idx] >> 32);
  reg_bank_write(WD2_REG_SCALER_TIME_STAMP_MSB, &data, 1);
}

/******************************************************************************/

void print_counter_values()
{
  unsigned long half_word;
  int i;

  xfs_printf("\r\nCurrent Values:\r\n");
  for(i=0;i<RCNT_NR_OF_COUNTERS;i++)
  {
    half_word = (unsigned long)(counter_values[current_idx][i] >> 32);
    xfs_printf("Counter %d: 0x%08X ", i, half_word);
    half_word = (unsigned long)(counter_values[current_idx][i]);
    xfs_printf("%08X\r\n", half_word);
  }
  half_word = (unsigned long)(daq_sys_time[current_idx] >> 32);
  xfs_printf("Current Time: 0x%08X ", half_word);
  half_word = (unsigned long)(daq_sys_time[current_idx]);
  xfs_printf("%08X\r\n", half_word);

  xfs_printf("\r\nPast Values:\r\n");
  for(i=0;i<RCNT_NR_OF_COUNTERS;i++)
  {
    half_word = (unsigned long)(counter_values[past_idx][i] >> 32);
    xfs_printf("Counter %d: 0x%08X ", i, half_word);
    half_word = (unsigned long)(counter_values[past_idx][i]);
    xfs_printf("%08X\r\n", half_word);
  }
  half_word = (unsigned long)(daq_sys_time[past_idx] >> 32);
  xfs_printf("Past Time: 0x%08X ", half_word);
  half_word = (unsigned long)(daq_sys_time[past_idx]);
  xfs_printf("%08X\r\n", half_word);

  xfs_printf("\r\n");
}

/******************************************************************************/
/******************************************************************************/
