//-------------------------------------------------------------------------------------
//  Paul Scherrer Institute
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDREAM2
//
//  Author  :  rs32
//  Created :  11.09.2014
//
//  Description :  Command interpreter for MSCB communication
//
//-------------------------------------------------------------------------------------

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "xtime.h"
#include "wd2_flash_memory_map.h"
#include "system.h"
#include "mscb.h"
#include "dbg.h"
#include "xtime.h"
#include "git-revision.h"
#include "gpio_slow_control.h"
#include "register_map_wd2.h"

/*------------------------------------------------------------------*/

/* GET_INFO attributes */
#define GET_INFO_GENERAL  0
#define GET_INFO_VARIABLE 1

/* Variable attributes */
#define SIZE_8BIT         1
#define SIZE_16BIT        2
#define SIZE_24BIT        3
#define SIZE_32BIT        4

/* Address modes */
#define ADDR_NONE         0
#define ADDR_NODE         1
#define ADDR_GROUP        2
#define ADDR_ALL          3

/* local variables */
int addressed = 0;
int addr_mode = 0;

unsigned char g_n_sub_addr;
int g_cur_sub_addr = 0;

SYS_INFO sys_info;
extern MSCB_INFO_VAR *variables;
unsigned char g_n_variables, g_var_size;

/*------------------------------------------------------------------*/

void led_blink()
{
   gpio_sc_flash_sw_status(GPIO_LED_SW_STATUS_WDB_ACCESS);
}

/*------------------------------------------------------------------*/

int mscb_flash_read(void)
{
   unsigned int i, sa, address, magic;

   address = SPI_FLASH_MSCB_CONFIG_ADDR;

   // read system info (node address etc.)
   spi_flash_read(SYSPTR(spi_flash), (unsigned char *)&sys_info, address, sizeof(SYS_INFO));
   address += sizeof(SYS_INFO);

   // read MSCB variables
   for (i = 0; i<g_n_variables ; i++) {
      if (!(variables[i].flags & MSCBF_DATALESS)) {
         // do it for each sub-address
         for (sa = 0 ; sa < g_n_sub_addr ; sa++) {
            spi_flash_read(SYSPTR(spi_flash), (unsigned char *)variables[i].ud + g_var_size*sa, address, variables[i].width);
            address += variables[i].width;
         }
      }
   }

   spi_flash_read(SYSPTR(spi_flash), (unsigned char *)&magic, address, sizeof(magic));

   return (magic == 0xDEADC0DE);
}

/*------------------------------------------------------------------*/

void mscb_flash_write(void)
{
   unsigned int i, sa, address, magic;

   address = SPI_FLASH_MSCB_CONFIG_ADDR;

   // erase MSCB sector
   spi_flash_sector_erase(SYSPTR(spi_flash), address);

   // write system info (node address etc.)
   spi_flash_write(SYSPTR(spi_flash), (unsigned char*)&sys_info, address, sizeof(SYS_INFO));
   address += sizeof(SYS_INFO);

   // write MSCB variables
   for (i = 0; i<g_n_variables ; i++) {
      if (!(variables[i].flags & MSCBF_DATALESS)) {
         // do it for each sub-address
         for (sa = 0 ; sa < g_n_sub_addr ; sa++) {
            spi_flash_write(SYSPTR(spi_flash), (unsigned char *)variables[i].ud + g_var_size*sa, address, variables[i].width);
            address += variables[i].width;
         }
      }
   }

   magic = 0xDEADC0DE;
   spi_flash_write(SYSPTR(spi_flash), (unsigned char *)&magic, address, sizeof(magic));
}

/*------------------------------------------------------------------*/

void mscb_init()
{
  int i, adr, flag;

  // obtain number of sub addresses
  mscb_vars(&g_n_sub_addr, &g_var_size);

  // count variables
  for (g_n_variables = 0; variables[g_n_variables].width > 0 ; g_n_variables++);

  // obtain settings and variables from flash
  flag = mscb_flash_read();

  if (!flag) {
     // default addresses if flash info is invalid
     sys_info.group_addr = 0;
     sys_info.node_addr  = 0;
     strcpy(sys_info.node_name, "WD001");

     // default variables
     for (i = 0; i < g_n_variables ; i++)
        if (!(variables[i].flags & MSCBF_DATALESS)) {
           /* do it for each sub-address */
           for (adr = 0 ; adr < g_n_sub_addr ; adr++) {
              memset((char*)variables[i].ud + g_var_size*adr, 0, variables[i].width);
           }
        }
  }

  // get GIT revision from git-revision.h in wavedaq directory and extract hash
  sys_info.revision = GIT_REV_HASH;

  mscb_hv_init(!flag);

  if (DBG_INIT)
    printf("MSCB address   : 0x%04X\r\n", sys_info.node_addr);
}

/*------------------------------------------------------------------*/

unsigned char cur_sub_addr()
{
   return g_cur_sub_addr;
}

/*------------------------------------------------------------------*/

unsigned char crc8_data[] = {
   0x00, 0x5e, 0xbc, 0xe2, 0x61, 0x3f, 0xdd, 0x83,
   0xc2, 0x9c, 0x7e, 0x20, 0xa3, 0xfd, 0x1f, 0x41,
   0x9d, 0xc3, 0x21, 0x7f, 0xfc, 0xa2, 0x40, 0x1e,
   0x5f, 0x01, 0xe3, 0xbd, 0x3e, 0x60, 0x82, 0xdc,
   0x23, 0x7d, 0x9f, 0xc1, 0x42, 0x1c, 0xfe, 0xa0,
   0xe1, 0xbf, 0x5d, 0x03, 0x80, 0xde, 0x3c, 0x62,
   0xbe, 0xe0, 0x02, 0x5c, 0xdf, 0x81, 0x63, 0x3d,
   0x7c, 0x22, 0xc0, 0x9e, 0x1d, 0x43, 0xa1, 0xff,
   0x46, 0x18, 0xfa, 0xa4, 0x27, 0x79, 0x9b, 0xc5,
   0x84, 0xda, 0x38, 0x66, 0xe5, 0xbb, 0x59, 0x07,
   0xdb, 0x85, 0x67, 0x39, 0xba, 0xe4, 0x06, 0x58,
   0x19, 0x47, 0xa5, 0xfb, 0x78, 0x26, 0xc4, 0x9a,
   0x65, 0x3b, 0xd9, 0x87, 0x04, 0x5a, 0xb8, 0xe6,
   0xa7, 0xf9, 0x1b, 0x45, 0xc6, 0x98, 0x7a, 0x24,
   0xf8, 0xa6, 0x44, 0x1a, 0x99, 0xc7, 0x25, 0x7b,
   0x3a, 0x64, 0x86, 0xd8, 0x5b, 0x05, 0xe7, 0xb9,
   0x8c, 0xd2, 0x30, 0x6e, 0xed, 0xb3, 0x51, 0x0f,
   0x4e, 0x10, 0xf2, 0xac, 0x2f, 0x71, 0x93, 0xcd,
   0x11, 0x4f, 0xad, 0xf3, 0x70, 0x2e, 0xcc, 0x92,
   0xd3, 0x8d, 0x6f, 0x31, 0xb2, 0xec, 0x0e, 0x50,
   0xaf, 0xf1, 0x13, 0x4d, 0xce, 0x90, 0x72, 0x2c,
   0x6d, 0x33, 0xd1, 0x8f, 0x0c, 0x52, 0xb0, 0xee,
   0x32, 0x6c, 0x8e, 0xd0, 0x53, 0x0d, 0xef, 0xb1,
   0xf0, 0xae, 0x4c, 0x12, 0x91, 0xcf, 0x2d, 0x73,
   0xca, 0x94, 0x76, 0x28, 0xab, 0xf5, 0x17, 0x49,
   0x08, 0x56, 0xb4, 0xea, 0x69, 0x37, 0xd5, 0x8b,
   0x57, 0x09, 0xeb, 0xb5, 0x36, 0x68, 0x8a, 0xd4,
   0x95, 0xcb, 0x29, 0x77, 0xf4, 0xaa, 0x48, 0x16,
   0xe9, 0xb7, 0x55, 0x0b, 0x88, 0xd6, 0x34, 0x6a,
   0x2b, 0x75, 0x97, 0xc9, 0x4a, 0x14, 0xf6, 0xa8,
   0x74, 0x2a, 0xc8, 0x96, 0x15, 0x4b, 0xa9, 0xf7,
   0xb6, 0xe8, 0x0a, 0x54, 0xd7, 0x89, 0x6b, 0x35,
};

unsigned char crc8(unsigned char *buffer, int len)
/********************************************************************\

  Routine: crc8

  Purpose: Calculate 8-bit cyclic redundancy checksum for a full
           buffer

  Input:
    unsigned char *data     data buffer
    int len                 data length in bytes


  Function value:
    unsighend char          CRC-8 code

\********************************************************************/
{
   int i;
   unsigned char crc8_code, index;

   crc8_code = 0;
   for (i = 0; i < len; i++) {
      index = buffer[i] ^ crc8_code;
      crc8_code = crc8_data[index];
   }

   return crc8_code;
}

/*------------------------------------------------------------------*/

void mscb_loop(void)
{
   mscb_uart_handler();
   mscb_hv_loop();
}

/*------------------------------------------------------------------*/

unsigned char in_buf[256], out_buf[256];
unsigned int i_in = 0;
unsigned int last_received = 0;

void mscb_uart_handler(void)
{
   unsigned char data;
   unsigned int cmd_len, n;

   while (plb_uart_receive(SYSPTR(MSCB_uart), &data, 1)) {
      last_received = time();

      // check for padding character
      if (data == 0 && i_in == 0)
         return;

      in_buf[i_in++] = data;

      if (i_in == sizeof(in_buf)) {
         i_in = 0;                      // don't interpret command on buffer overflow
         return;
      }

      // initialize command length first byte
      cmd_len = (in_buf[0] & 0x07) + 2; // + cmd + crc

      if (i_in > 1 && cmd_len == 9) {
         // variable length command
         if (in_buf[1] & 0x80)
            cmd_len = (((in_buf[1] & 0x7F) << 8) | in_buf[2]) + 4;
         else
            cmd_len = in_buf[1] + 3;    // + cmd + N + crc
      }

      // ignore ping acknowledge from other node
      if (i_in == 1 && in_buf[0] == 0x78) {
         i_in = 0;
         return;
      }

      if (i_in < cmd_len)
         return;                        // return if command not yet complete

      usleep(20);                       // wait inter-char delay

      n = mscb_interpret(0, in_buf, out_buf);

      if (n > 0)
         plb_uart_send(SYSPTR(MSCB_uart), out_buf, n);

      i_in = 0;
   }

   // drop partial buffer if no char received for 100 ms
   unsigned int now = time();
   if (i_in > 0 && time_diff(now, last_received) > 0.1) {
      i_in = 0;
   }
}

/*------------------------------------------------------------------*/

unsigned int time(void)
{
  return time_tic_read();
}

double time_diff(unsigned int t1, unsigned int t2)
{
   return (double) (t1 - t2) / TIME_FREQUENCY;
}

/*------------------------------------------------------------------*/

void addr_node16(unsigned char mode, unsigned int adr, unsigned int node_addr)
{
   if (node_addr == 0xFFFF) {
      if (adr == node_addr) {
         addressed = 1;
         g_cur_sub_addr = 0;
         addr_mode = mode;
      } else {
         addressed = 0;
         addr_mode = ADDR_NONE;
      }
   } else {
      if (mode == ADDR_NODE) {
         if (adr >= node_addr &&
             adr <  node_addr + g_n_sub_addr) {

            addressed = 1;
            g_cur_sub_addr = adr - node_addr;
            addr_mode = ADDR_NODE;
         } else {
            addressed = 0;
            addr_mode = ADDR_NONE;
         }
      } else if (mode == ADDR_GROUP) {
         if (adr == node_addr) {
            addressed = 1;
            g_cur_sub_addr = 0;
            addr_mode = ADDR_GROUP;
         } else {
            addressed = 0;
            addr_mode = ADDR_NONE;
         }
      }
   }
}

/*------------------------------------------------------------------*/

unsigned int mscb_interpret(int submaster, unsigned char *buf, unsigned char *rb)
{
  unsigned char ch, a1, a2;
  unsigned int size, u, adr, i, j, buflen, n = 0;
  unsigned int data = WD2_RECONFIGURE_FPGA_MASK;
  static int firmware_update = 0;

  // determine length of MSCB command
  buflen = (buf[0] & 0x07);
  if (buflen == 7) {
    if (buf[1] & 0x80)
      buflen = (((buf[1] & 0x7F) << 8) | buf[2]) + 4;
    else
      buflen = (buf[1] & 0x7F) + 3;  // add command, length and CRC
  } else
    buflen += 2; // add command and CRC

  if (DBG_INFO) {
    xil_printf("MSCB interpret: %d bytes\r\n", buflen);
    for (j=0 ; j<buflen ; j++) {
      xil_printf("%02X ", buf[j]);
      if ((j+1) % 16 == 0)
        xil_printf("\r\n");
    }
    xil_printf("\r\n");
  }

  // check CRC
  if (crc8(buf, buflen-1) != buf[buflen-1]) {
    xil_printf("MSCB interpret: Invalid CRC %02X vs %02X\r\n", crc8(buf, buflen-1), buf[buflen-1]);
    return 0;
  }

  if (!addressed &&
        buf[0] != MCMD_ADDR_NODE16 &&
        buf[0] != MCMD_ADDR_GRP16 &&
        buf[0] != MCMD_ADDR_BC &&
        buf[0] != MCMD_PING16)
     return 0;

  switch (buf[0]) {
    case MCMD_ADDR_NODE16:
      addr_node16(ADDR_NODE, (buf[1] << 8) | buf[2], sys_info.node_addr);
      break;

    case MCMD_ADDR_GRP16:
      addr_node16(ADDR_GROUP, (buf[1] << 8) | buf[2], sys_info.group_addr);
      break;

    case MCMD_ADDR_BC:
      addressed = 1;
      addr_mode = ADDR_ALL;
      break;

    case MCMD_PING16:
      // turn off blinking LED if update is in progress
      if (firmware_update) {
         firmware_update = 0;
         gpio_sc_clr_sw_status(GPIO_LED_SW_STATUS_SW_UPDATE);
      }

      addr_node16(ADDR_NODE, (buf[1] << 8) | buf[2], sys_info.node_addr);
      if (addressed) {
         led_blink();
         rb[0] = MCMD_ACK;
         n = 1;
      } else {
         if (submaster) {
            rb[0] = 0xFF;
            n = 1;
         }
      }
      break;

    case MCMD_INIT:
      xil_printf("\r\nRebooting...\r\n");
      reg_bank_set(WD2_REG_RST, &data, 1);
      break;

    case MCMD_GET_INFO:
       /* general info */

      n = 0;
      rb[n++] = MCMD_ACK + 7;               // send acknowledge, variable data length
      rb[n++] = 32;                         // data length
      rb[n++] = PROTOCOL_VERSION;           // send protocol version
      rb[n++] = g_n_variables;              // send number of variables
      rb[n++] = sys_info.node_addr >> 8;    // send node address
      rb[n++] = sys_info.node_addr & 0xFF;
      rb[n++] = sys_info.group_addr >> 8;   // send group address
      rb[n++] = sys_info.group_addr & 0xFF;
      rb[n++] = sys_info.revision >> 8;     // send revision
      rb[n++] = sys_info.revision & 0xFF;

      for (i = 0; i < 16; i++)  // send node name
        rb[n++] = sys_info.node_name[i];

      for (i = 0; i < 6 ; i++)  // no RTC
        rb[n++] = 0;

      rb[n++] = 1024 >> 8;      // max. buffer size 1024 bytes
      rb[n++] = 1024 & 0xFF;

      rb[n] = crc8(rb, n);
      n++;
      break;

    case MCMD_GET_INFO + 1:
       /* send variable info */
       if (buf[1] < g_n_variables) {
         MSCB_INFO_VAR *pvar;
         pvar = variables + buf[1];

         n = 0;
         rb[n++] = MCMD_ACK + 7;            // send acknowledge, variable data length
         rb[n++] = 13;                      // data length
         rb[n++] = pvar->width;
         rb[n++] = pvar->unit;
         rb[n++] = pvar->prefix;
         rb[n++] = pvar->status;
         rb[n++] = pvar->flags;

         for (i = 0; i < 8; i++)            // send variable name
           rb[n++] = pvar->name[i];

         rb[n] = crc8(rb, n);
         n++;
       } else {
         /* just send dummy ack */
         rb[0] = MCMD_ACK;
         rb[1] = 0;
         n = 2;
       }
       break;

    case MCMD_GET_UPTIME:
       /* send uptime */

       u = time_get_sec();

       rb[0] = MCMD_ACK + 4;
       rb[1] = *(((unsigned char *)&u) + 0);
       rb[2] = *(((unsigned char *)&u) + 1);
       rb[3] = *(((unsigned char *)&u) + 2);
       rb[4] = *(((unsigned char *)&u) + 3);
       rb[5] = crc8(rb, 5);
       n = 6;
       break;

    case MCMD_SET_ADDR:
       if (buf[1] == ADDR_SET_NODE)
          /* complete node address */
          sys_info.node_addr = (buf[2] << 8) | buf[3];
       else if (buf[1] == ADDR_SET_HIGH)
          /* only high byte node address */
          *((unsigned char *)(&sys_info.node_addr)) = (buf[2] << 8) | buf[3];
       else if (buf[1] == ADDR_SET_GROUP)
          /* group address */
          sys_info.group_addr = (buf[2] << 8) | buf[3];

       /* copy address to flash */
       mscb_flash_write();
       break;

    case MCMD_SET_NAME:
       /* set node name in RAM */
       for (i = 0; i < 16 && i < buf[1]; i++)
          sys_info.node_name[i] = buf[2 + i];
       sys_info.node_name[15] = 0;

       /* copy address to flash */
       mscb_flash_write();
       break;

    case MCMD_FLASH:
       mscb_flash_write();
       break;

    case MCMD_ECHO:
       led_blink();
       rb[0] = MCMD_ACK + 1;
       rb[1] = buf[1];
       rb[2] = crc8(rb, 2);
       n = 3;
       break;

    case MCMD_WRITE_MEM:
       if (!firmware_update) {
    	   firmware_update = 1;
    	   gpio_sc_set_sw_status(GPIO_LED_SW_STATUS_SW_UPDATE);
       }

       size = buflen - 9; // minus cmd, len, subadr, adr and CRC
       // subadr = buf[3]; ignored
       adr = buf[4];
       adr = (adr << 8) | buf[5];
       adr = (adr << 8) | buf[6];
       adr = (adr << 8) | buf[7];

       /* only flash supported right now */
       if ((adr & MSCB_BASE_FLASH) != MSCB_BASE_FLASH)
          break;
       adr = adr & ~MSCB_BASE_FLASH;

       /* if address is on start of block, erase it */
       if ((adr & 0xFFFF) == 0)
         spi_flash_block64k_erase(SYSPTR(spi_flash), adr);

       /* write flash */
       spi_flash_write(SYSPTR(spi_flash), (unsigned char*)buf+8, adr, size);

       /* send ACK with CRC code */
       rb[0] = MCMD_ACK;
       rb[1] = crc8(buf+8, size);
       n = 2;

       buf[0] = 0; // do not re-interpret command below

       break;

    case MCMD_READ_MEM:
       size = (buf[2] << 8) | buf[3];
       // subadr = buf[4]; ignored
       adr = buf[5];
       adr = (adr << 8) | buf[6];
       adr = (adr << 8) | buf[7];
       adr = (adr << 8) | buf[8];

       /* only flash supported right now */
       if ((adr & MSCB_BASE_FLASH) != MSCB_BASE_FLASH)
          break;
       adr = adr & ~MSCB_BASE_FLASH;

       memset(buf, 0, size);
       spi_flash_read(SYSPTR(spi_flash), (unsigned char*)buf, adr, size);

       rb[0] = MCMD_ACK + 7;
       rb[1] = 0x80 | ((size >> 8) & 0x7F);
       rb[2] = size & 0xFF;
       memcpy(rb+3, buf, size);
       rb[3+size] = crc8(rb, size+3);
       n = size+4;

       buf[0] = 0; // do not re-interpret command below

       break;
  }

  if ((buf[0] & 0xF8) == MCMD_READ) {
    if (buf[0] == MCMD_READ + 1) {       // single variable
      if (buf[1] < g_n_variables) {
        n = variables[buf[1]].width;     // number of bytes to return

        if (variables[buf[1]].flags & MSCBF_DATALESS) {
          n = mscb_read_param(buf[1]);         // for data less variables, user routine returns bytes
          rb[0] = MCMD_ACK + 7;          // and places data directly in out_buf
          rb[1] = n;
          n += 2;
          rb[n] = crc8(rb, n);           // generate CRC code
          n += 1;
        } else {

          mscb_read_param(buf[1]);

          if (n > 6) {
            /* variable length buffer */
            rb[0] = MCMD_ACK + 7;        // send acknowledge, variable data length
            rb[1] = n;                   // send data length

            for (i = 0; i < n; i++)      // copy user data
              rb[2+i] = ((char *) variables[buf[1]].ud)[i+g_var_size*g_cur_sub_addr];
            n += 2;
          } else {

            rb[0] = MCMD_ACK + n;

            for (i = 0; i < n; i++)      // copy user data
              rb[1+i] = ((char *) variables[buf[1]].ud)[i+g_var_size*g_cur_sub_addr];
            n += 1;
          }

          rb[n] = crc8(rb, n);           // generate CRC code
          n++;
        }
      } else {
        /* just send dummy ack to indicate error */
        rb[0] = MCMD_ACK;
        n = 1;
      }

    } else if (buf[0] == MCMD_READ + 2) {   // variable range

      if (buf[1] < g_n_variables && buf[2] < g_n_variables && buf[1] <= buf[2]) {
        /* calculate number of bytes to return */
        for (i = buf[1], size = 0; i <= buf[2]; i++) {
          mscb_read_param(i);
          size += variables[i].width;
        }

        n = 0;
        rb[n++] = MCMD_ACK + 7;             // send acknowledge, variable data length
        if (size < 0x80)
          rb[n++] = size;                   // send data length one byte
        else {
          rb[n++] = 0x80 | size / 0x100;    // send data length two bytes
          rb[n++] = size & 0xFF;
        }

        /* loop over all variables */
        for (i = buf[1]; i <= buf[2]; i++) {
          for (j = 0; j < variables[i].width; j++)    // send user data
            rb[n++] = ((char *) variables[i].ud)[j+g_var_size * g_cur_sub_addr];
        }

        rb[n] = crc8(rb, n);
        n++;
      } else {
        /* just send dummy ack to indicate error */
        rb[0] = MCMD_ACK;
        n = 1;
      }
    }
  }

  if ((buf[0] & 0xF8) == MCMD_WRITE_NA || (buf[0] & 0xF8) == MCMD_WRITE_ACK) {

    led_blink();

    n = buf[0] & 0x07;

    if (n == 0x07) {  // variable length
      j = 1;
      n = buf[1];
      ch = buf[2];
    } else {
      j = 0;
      ch = buf[1];
    }

    n--; // data size (minus channel)

    if (ch < g_n_variables) {

      /* don't exceed variable width */
      if (n > variables[ch].width)
        n = variables[ch].width;

      if (addr_mode == ADDR_NODE)
        a1 = a2 = g_cur_sub_addr;
      else {
        a1 = 0;
        a2 = g_n_sub_addr-1;
      }

      for (g_cur_sub_addr = a1 ; g_cur_sub_addr <= a2 ; g_cur_sub_addr++) {
        for (i = 0; i < n; i++)
          if (!(variables[ch].flags & MSCBF_DATALESS)) {
            if (variables[ch].unit == UNIT_STRING) {
              if (n > 4)
                /* copy bytes in normal order */
                ((char *) variables[ch].ud)[i + g_var_size*g_cur_sub_addr] =
                    buf[2 + j + i];
              else
                /* copy bytes in reverse order (got swapped on host) */
                ((char *) variables[ch].ud)[i + g_var_size*g_cur_sub_addr] =
                    buf[buflen - 2 - i];
            } else
              /* copy LSB bytes, needed for BYTE if DWORD is sent */
              ((char *) variables[ch].ud)[i + g_var_size*g_cur_sub_addr] =
                  buf[buflen - 1 - variables[ch].width + i + j];
          }

        mscb_write_param(ch, 0);
      }
      g_cur_sub_addr = a1; // restore previous value

      if ((buf[0] & 0xF8) == MCMD_WRITE_ACK) {
        rb[0] = MCMD_ACK;
        rb[1] = buf[buflen - 1];
        n = 2;
      }
    }
  }

  if (DBG_INFO && n>0) {
    xil_printf("MSCB return: %d bytes\r\n", n);
    for (j=0 ; j<n ; j++) {
      xil_printf("%02X ", rb[j]);
      if ((j+1) % 16 == 0)
        xil_printf("\r\n");
    }
    xil_printf("\r\n");
  }

  return n;
}

/*------------------------------------------------------------------*/
