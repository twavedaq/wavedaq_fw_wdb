/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e, theidel
 *  Created :  16.04.2015 11:36:43
 *
 *  Description :  Test frame generator for  ADC-SERDES-WD2_PACKAGER-ETHERNET chain.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#ifndef __PLB_SERDES_VIA_ETH_TESTGEN_H__
#define __PLB_SERDES_VIA_ETH_TESTGEN_H__

/******************************************************************************/
/* Include Files                                                              */
/******************************************************************************/

#include "xparameters.h"

/******************************************************************************/
/* plb_serdes_via_eth_testgen                                                 */
/******************************************************************************/

#define SRDS_ETH_TESTGEN_REG_CTRL             0
#define SRDS_ETH_TESTGEN_REG_NR_OF_SAMPLES    1
#define SRDS_ETH_TESTGEN_REG_NR_OF_FRAMES     2
#define SRDS_ETH_TESTGEN_REG_CURRENT_FRAME    3
#define SRDS_ETH_TESTGEN_REG_HEADER           4

#define SRDS_ETH_TESTGEN_CTRL_START           0x00000001
#define SRDS_ETH_TESTGEN_CTRL_RESET           0x00000002

typedef struct
{
    unsigned int base_addr;
} serdes_eth_testgen_type;

void serdes_eth_testgen_init(serdes_eth_testgen_type *self, unsigned int base_addr);
void serdes_eth_testgen_reset(serdes_eth_testgen_type *self);
void serdes_eth_testgen_stop(serdes_eth_testgen_type *self);
int  serdes_eth_testgen_start(serdes_eth_testgen_type *self, unsigned int header, unsigned int total_frames);

/******************************************************************************/
#endif /* __PLB_SERDES_VIA_ETH_TESTGEN_H__ */
/******************************************************************************/
