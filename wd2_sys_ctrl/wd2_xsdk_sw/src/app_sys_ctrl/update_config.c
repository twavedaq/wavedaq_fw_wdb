/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  19.09.2014 13:45:08
 *
 *  Description :  Updating external devices via serial protocols if corresponding
 *                 register bank settings have changed.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

/***************************** Include Files *******************************/

#include "update_config.h"
#include "system.h"
#include "xfs_printf.h"
#include "dbg.h"
#include "mscb.h"
#include "drv_drp_pll.h"
#include "register_map_wd2.h"
#include "drv_soft_spi_temp_dac_lmk.h"
#include "gpio_shift_register.h"
#include "gpio_slow_control.h"
#ifdef WD2_REV_F
#include "drv_soft_spi_adc_ad9637.h"
#else
#include "drv_soft_spi_adc_ltm9009.h"
#endif
#include "drv_rate_counter.h"

/************************** Defines ****************************************/

#define PLL_LOCK_TIMEOUT_STEPS   10000
#define PLL_LOCK_TIME_STEP_US      100

/************************** Variable Definitions ***************************/

unsigned int ones = 0xFFFFFFFF;
unsigned int zero = 0;

/************************** Function Definitions ***************************/

int auto_update_configurations()
{
  unsigned int modified[2];

  /* Check for modifications in LMK configuration registers and update configuration if necessary*/
  modified[0] = reg_bank_get(WD2_REG_LMK_0_7_MOD_FLAG);
  modified[1] = reg_bank_get(WD2_REG_LMK_8_15_MOD_FLAG);
  if(modified[0] && (modified[1] == 0) )
  {
    if(DBG_SPAM) xfs_local_printf("Auto-Update Configuration Value LMK Channel:  0x%08X (LMK2: 0x%08X)\r\n", modified[0], modified[1]);
    /*if(DBG_SPAM) xfs_local_printf("Auto-Update Configuration Value LMK2: 0x%08X\r\n", modified[1]); */
    update_lmk_ch(modified[0]);
  }

  /* Check for modifications in maximum payload size register and update configuration if necessary*/
  modified[0] = reg_bank_get(WD2_REG_COM_PLD_SIZE_MOD_FLAG);
  if(modified[0])
  {
    if(DBG_SPAM) xfs_local_printf("Auto-Update Configuration Value Control: 0x%08X\r\n", modified[0]);
    update_max_pkg_samples();
  }

  /* Check for modifications in global control configuration registers and update configuration if necessary*/
  modified[0] = reg_bank_get(WD2_REG_CLK_CTRL_MOD_FLAG);
  if( modified[0] & WD2_ADC_RST_MOD_MASK)
  {
    update_adc_rst();
  }
  if( modified[0] & WD2_TRIGGER_DAQ_CLK_CAL_MOD_MASK)
  {
      update_daq_clk_cal();
  }
  if( modified[0] & (WD2_CLK_SEL_AND_DRS_CLK_DIV_MOD_MASK | WD2_EXT_CLK_FREQ_MOD_MASK | WD2_LOCAL_CLK_FREQ_MOD_MASK))
  {
    update_clocks(modified[0]);
  }

  /* Check for modifications in DRS configuration registers and update configuration if necessary*/
  modified[0] = reg_bank_get(WD2_REG_DRS_MOD_FLAG);
  if(modified[0])
  {
    if(DBG_SPAM) xfs_local_printf("Auto-Update Configuration Value DRS: 0x%08X\r\n", modified[0]);
    update_drs(modified[0]);
  }

  /* Check for modifications in ADC sample div register and update configuration if necessary*/
  modified[0] = reg_bank_get(WD2_REG_ADC_SAMPLE_DIV_MOD_FLAG);
  if(modified[0])
  {
    if(DBG_SPAM) xfs_local_printf("Auto-Update Configuration Value Control: 0x%08X\r\n", modified[0]);
    update_adc_sampling_freq();
  }

  /* Check for modifications in DAC configuration registers and update configuration if necessary*/
  modified[0] = reg_bank_get(WD2_REG_DAC_0_1_MOD_FLAG);
  modified[1] = reg_bank_get(WD2_REG_DAC_2_MOD_FLAG);
  if(modified[0]|| modified[1])
  {
    if(DBG_SPAM) xfs_local_printf("Auto-Update Configuration Value DAC 0 and 1: 0x%08X\r\n", modified[0]);
    if(DBG_SPAM) xfs_local_printf("Auto-Update Configuration Value DAC 2:       0x%08X\r\n", modified[1]);
    update_dac(modified[0], modified[1]);
  }

  /* Check for modifications in Frontend configuration registers and update configuration if necessary*/
  modified[0] = reg_bank_get(WD2_REG_FE_0_15_MOD_FLAG);
  if(modified[0])
  {
    if(DBG_SPAM) xfs_local_printf("Auto-Update Frontend Settings: 0x%08X\r\n", modified[0]);
    update_frontend();
  }

  /* Check for modifications in HV configuration registers and update configuration if necessary*/
  modified[0] = reg_bank_get(WD2_REG_HV_U_TARGET_0_7_MOD_FLAG);
  modified[1] = reg_bank_get(WD2_REG_HV_U_TARGET_8_15_MOD_FLAG);
  /*modified[2] = reg_bank_get(WD2_REG_HV_MOD_FLAG); */
  /*if(modified[0] || modified[1] || modified[2]) */
  if(modified[0] || modified[1])
  {
    if(DBG_SPAM)
    {
      xfs_local_printf("Auto-Update Configuration Value HV U_TARGET_0_7:  0x%08X\r\n", modified[0]);
      xfs_local_printf("Auto-Update Configuration Value HV U_TARGET_8_15: 0x%08X\r\n", modified[1]);
    }
    /*if(DBG_SPAM) xfs_local_printf("Auto-Update Configuration Value HV:               0x%08X\r\n", modified[2]); */
    update_hv(modified[0], modified[1]);
  }

  return 1;
}

/******************************************************************************/

int trigger_update_configurations()
{
  unsigned int apply_settings;

  apply_settings = reg_bank_get(WD2_REG_APLY_CFG);
  reg_bank_write(WD2_REG_APLY_CFG, &zero, 1);

  if(!apply_settings) return 0;

  /* Check for trigger of LMK configuration registers and configure if necessary*/
  if(apply_settings & WD2_APPLY_SETTINGS_LMK_MASK)
  {
    if(DBG_INFO) xfs_local_printf("Triggered Update Configuration LMK\r\n");
    update_lmk();
  }

  /* Check for trigger of Control registers and configure if necessary*/
  if(apply_settings & WD2_APPLY_SETTINGS_CTRL_MASK)
  {
    if(DBG_INFO) xfs_local_printf("Triggered Update Configuration Control Bits\r\n");
#ifdef WD2_REV_F
    update_adc_rst();
    update_clocks(WD2_CLK_SEL_AND_DRS_CLK_DIV_MOD_MASK |
                WD2_EXT_CLK_FREQ_MOD_MASK |
                WD2_LOCAL_CLK_FREQ_MOD_MASK);
#else /* WD2_REV_F */
    update_clocks(WD2_CLK_SEL_AND_DRS_CLK_DIV_MOD_MASK |
                  WD2_EXT_CLK_FREQ_MOD_MASK |
                  WD2_LOCAL_CLK_FREQ_MOD_MASK);
#endif /* WD2_REV_F */
  }

  /* Check for trigger of DRS configuration and configure if necessary*/
  if(apply_settings & WD2_APPLY_SETTINGS_DRS_MASK)
  {
    if(DBG_INFO) xfs_local_printf("Triggered Update Configuration DRS\r\n");
    update_drs(0xFFFFFFFF);
  }

  /* Check for trigger of DAC configuration registers and configure if necessary*/
  if(apply_settings & WD2_APPLY_SETTINGS_DAC_MASK)
  {
    if(DBG_INFO) xfs_local_printf("Triggered Update Configuration DAC\r\n");
    update_dac(0xFFFFFFFF, 0xFFFFFFFF);
  }

  /* Check for trigger of Frontend configuration registers and configure if necessary*/
  if(apply_settings & WD2_APPLY_SETTINGS_FRONTEND_MASK)
  {
    if(DBG_INFO) xfs_local_printf("Triggered Update Frontend Settings\r\n");
    update_frontend();
  }

  /* Check for trigger of HV configuration registers and configure if necessary*/
  if(apply_settings & WD2_APPLY_SETTINGS_HV_MASK)
  {
    if(DBG_INFO) xfs_local_printf("Triggered Update HV Settings\r\n");
    update_hv(0xFFFFFFFF, 0xFFFFFFFF);
  }

  /* Check for trigger of ADC sample divieder register and configure if necessary*/
  if(apply_settings & WD2_APPLY_SETTINGS_ADC_SAMPLE_DIV_MASK)
  {
    if(DBG_INFO) xfs_local_printf("Triggered Update ADC Sampling Frequency Setting\r\n");
    update_adc_sampling_freq();
  }

  /* Check for trigger of maximum payload size register and configure if necessary*/
  if(apply_settings & WD2_APPLY_SETTINGS_MAX_PLD_SIZE_MASK)
  {
    if(DBG_INFO) xfs_local_printf("Triggered Update Max. Payload Size Setting\r\n");
    update_max_pkg_samples();
  }

  return 1;
}

/******************************************************************************/

int update_adc_rst()
{
  unsigned int clk_ctrl_reg;

  if(DBG_INFO) xfs_local_printf("Resetting ADCs\r\n");

  /* Clear modified flags !!! */
  clk_ctrl_reg = WD2_ADC_RST_MOD_MASK;
  reg_bank_write(WD2_REG_CLK_CTRL_MOD_FLAG, &clk_ctrl_reg, 1);

#ifdef WD2_REV_F
    ad9637_reset();
#else /* WD2_REV_F */
    ltm9009_reset();
#endif /* WD2_REV_F */

  return 0;
}

/******************************************************************************/

int update_daq_clk_cal()
{
  unsigned int clk_ctrl_reg;

  if(DBG_INFO) xfs_local_printf("Checking DAQ clock phase\r\n");

  /* Clear modified flags !!! */
  clk_ctrl_reg = WD2_TRIGGER_DAQ_CLK_CAL_MOD_MASK;
  reg_bank_write(WD2_REG_CLK_CTRL_MOD_FLAG, &clk_ctrl_reg, 1);

  ads_timing_cal(SYSPTR(adc_cal_info), ADC_CAL_MODE_CHECK, 0);

  return 0;
}

/******************************************************************************/

int update_clocks(unsigned int mod_reg)
{
  unsigned int reg_val;
  unsigned int clear_flags;
  unsigned int f_old, f_new;
  unsigned int div_old, div_new;
  unsigned int mask = 0;
  unsigned int offset = 0;
  int activate_ext_clk;
  int timeout;
  int i;

  if(DBG_INFO) xfs_local_printf("Updating clock configuration\r\n");

  /* Clear modified flags !!! */
  clear_flags = WD2_CLK_SEL_AND_DRS_CLK_DIV_MOD_MASK | WD2_EXT_CLK_FREQ_MOD_MASK | WD2_LOCAL_CLK_FREQ_MOD_MASK;
  reg_bank_write(WD2_REG_CLK_CTRL_MOD_FLAG, &clear_flags, 1);

  /* Read register */
  reg_val = reg_bank_get(WD2_REG_CLK_CTRL);

  activate_ext_clk = 0;
  if( (mod_reg & (WD2_CLK_SEL_AND_DRS_CLK_DIV_MOD_MASK | WD2_LOCAL_CLK_FREQ_MOD_MASK)) && (reg_val & WD2_DAQ_CLK_SRC_SEL_MASK) )
  {
    mask   = WD2_LOCAL_CLK_FREQ_MASK;
    offset = WD2_LOCAL_CLK_FREQ_OFS;
  }
  else if( (mod_reg & (WD2_CLK_SEL_AND_DRS_CLK_DIV_MOD_MASK | WD2_EXT_CLK_FREQ_MOD_MASK)) && (~reg_val & WD2_DAQ_CLK_SRC_SEL_MASK) )
  {
    activate_ext_clk = 1;
    mask   = WD2_EXT_CLK_FREQ_MASK;
    offset = WD2_EXT_CLK_FREQ_OFS;
  }
  else
  {
    /* return because mask and offset are not defined */
    return 0;
  }

  f_new = (reg_val & mask) >> offset;

  /* get value from LMK register */
  f_old = (reg_bank_get(WD2_LMK13_OSCIN_FREQ_REG) & WD2_LMK13_OSCIN_FREQ_MASK) >> WD2_LMK13_OSCIN_FREQ_OFS;

  if (f_new != f_old)
  {
    if(DBG_INF0) xfs_printf("Updating input clock parameters\r\n");
    div_old = (reg_bank_get(WD2_LMK14_PLL_R_REG) & WD2_LMK14_PLL_R_MASK) >> WD2_LMK14_PLL_R_OFS;
    div_new = (f_new * div_old) / f_old;

    if ( ((float)f_old)/((float)div_old) != 0.0 )
    {
      if(DBG_WARN) xfs_local_printf("Warning: LMK divided frequencies do not precisely match\r\n\r\n");
    }

    /* write new values to LMK register */
    reg_val  = reg_bank_get(WD2_LMK13_OSCIN_FREQ_REG) & ~WD2_LMK13_OSCIN_FREQ_MASK;
    reg_val |= (f_new << WD2_LMK13_OSCIN_FREQ_OFS);
    reg_bank_write(WD2_LMK13_OSCIN_FREQ_REG, &reg_val, 1);

    reg_val  = reg_bank_get(WD2_LMK14_PLL_R_REG) & ~WD2_LMK14_PLL_R_MASK;
    reg_val |= ((div_new << WD2_LMK14_PLL_R_OFS) & WD2_LMK14_PLL_R_MASK);
    reg_bank_write(WD2_LMK14_PLL_R_REG, &reg_val, 1);
  }

  /* Update LMK in any case */
  reg_val = WD2_DAQ_PLL_RST_MASK;
  reg_bank_set(WD2_DAQ_PLL_RST_REG, &reg_val, 1); /* activate PLL reset */

  update_lmk();

  timeout = 0;
  i = 0;
  do /* wait for LMK lock with timeout */
  {
    usleep(PLL_LOCK_TIME_STEP_US);
    reg_val = reg_bank_get(WD2_LMK_PLL_LOCK_REG);
    i++;
  }
  while(((reg_val & WD2_LMK_PLL_LOCK_MASK) == 0) && (i < PLL_LOCK_TIMEOUT_STEPS));
  if (i == PLL_LOCK_TIMEOUT_STEPS)
  {
    timeout++;
  }

  lmk03000_sync();

  reg_val = WD2_DAQ_PLL_RST_MASK;
  reg_bank_clr(WD2_DAQ_PLL_RST_REG, &reg_val, 1); /* release PLL reset */

  i = 0;
  mask = WD2_DAQ_PLL_LOCK_MASK |
         WD2_OSERDES_PLL_LOCK_DCB_MASK |
         WD2_OSERDES_PLL_LOCK_TCB_MASK |
         WD2_LMK_PLL_LOCK_MASK;
  do /* wait for DAQ PLL lock with timeout */
  {
    usleep(PLL_LOCK_TIME_STEP_US);
    reg_val = reg_bank_get(WD2_DAQ_PLL_LOCK_REG);
    i++;
  }
  while(((reg_val & mask) != mask) && (i < PLL_LOCK_TIMEOUT_STEPS));
  if (i == PLL_LOCK_TIMEOUT_STEPS)
  {
    timeout++;
  }

  if (timeout == 0)
  {
    /* confirm clock switch */
    reg_val = WD2_EXT_CLK_ACTIVE_MASK;
    if (activate_ext_clk)
    {
      reg_bank_set(WD2_EXT_CLK_ACTIVE_REG, &reg_val, 1);
    }
    else
    {
      reg_bank_clr(WD2_EXT_CLK_ACTIVE_REG, &reg_val, 1);
    }
  }

  return 0;
}

/******************************************************************************/

int update_drs(unsigned int mod_reg)
{
  unsigned int data;
  unsigned int timer = 100;

  if(DBG_INFO) xfs_local_printf("Updating DRS configuration\r\n\r\n");

  /* Clear modified flags !!! */
  reg_bank_write(WD2_REG_DRS_MOD_FLAG, &ones, 1);

  if(mod_reg & 0x00000007)
  {
    data = WD2_DRS_CONFIGURE_MASK;
    reg_bank_set(WD2_DRS_CONFIGURE_REG, &data, 1);

    while( timer && !( reg_bank_get(WD2_DRS_CONFIG_DONE_REG) & WD2_DRS_CONFIG_DONE_MASK ) )
    {
      usleep(10);
      timer--;
    }
    reg_bank_clr(WD2_DRS_CONFIGURE_REG, &data, 1);

    if( timer == 0 )
    {
      if(DBG_ERR) xfs_printf("Error: DRS configuration timeout\r\n\r\n");
      return 0;
    }
  }

  return 1;
}
/******************************************************************************/

int update_max_pkg_samples()
{
  unsigned int max_payload_bytes;
  unsigned int max_payload_samples;

  if(DBG_INFO) xfs_local_printf("Updating maximum samples per packet\r\n\r\n");

  /* Clear modified flags !!! */
  reg_bank_write(WD2_REG_COM_PLD_SIZE_MOD_FLAG, &ones, 1);

  max_payload_bytes = reg_bank_get(WD2_REG_COM_PLD_SIZE) & WD2_COM_PLD_SIZE_MASK;
  max_payload_bytes = max_payload_bytes - WD2_HEADER_SIZE;

  max_payload_samples = max_payload_bytes/1.5;
  reg_bank_write(WD2_REG_MAX_DRS_ADC_PKT_SAMPLES, &max_payload_samples, 1);

  max_payload_samples = max_payload_bytes*8;
  reg_bank_write(WD2_REG_MAX_TDC_PKT_SAMPLES, &max_payload_samples, 1);

  max_payload_samples = max_payload_bytes/8;
  reg_bank_write(WD2_REG_MAX_TRG_PKT_SAMPLES, &max_payload_samples, 1);

  max_payload_samples = max_payload_bytes/8;
  reg_bank_write(WD2_REG_MAX_SCL_PKT_SAMPLES, &max_payload_samples, 1);

  return 1;
}

/******************************************************************************/

int update_dac(unsigned int mod_reg1, unsigned int mod_reg2)
{
  unsigned int mask;
  unsigned int value;
  unsigned int mod_reg[2] = {mod_reg1, mod_reg2};

  if(DBG_INFO) xfs_printf("Updating DAC configuration\r\n\r\n");
  for(unsigned int j=0;j<2;j++)
  {
    mask = 0xC0000000;
    for(unsigned int i=0;i<16;i++)
    {
      if(mod_reg[j] & mask)
      {
        reg_bank_write(WD2_REG_DAC_0_1_MOD_FLAG+j*4, &mask, 1); /* clears modified flag !!! */
        value = reg_bank_get(WD2_REG_DAC0_A_B+(((j*16+i)&0xFFFFFFFE)*2));
        if(!(i%2))
        {
          value >>= 16;
        }
        value = value & 0x0000FFFF;
        /*if(DBG_INFO) xfs_printf("Writing to DAC channel %d: 0x%04X\r\n", i, value); */
        ltc2600_write(j*16+i, LTC2600_CMD_WR_PU_N, value);
      }
      mask >>= 2;
    }
  }
  return 1;
}

/******************************************************************************/

int update_frontend()
{
  if (DBG_SPAM) xfs_local_printf("Updating Frontend configuration\r\n\r\n");
  frontend_setting_apply();
  return 1;
}

/******************************************************************************/

int update_hv(unsigned int mod_reg_hv_u_target_0_7, unsigned int mod_reg_hv_u_target_8_15)
{
  unsigned int mask;

  if(DBG_SPAM) xfs_local_printf("Updating HV configuration\r\n");

  /* Read register */
  mask = WD2_HV_U_TARGET_0_MOD_MASK; /* = WD2_HV_U_TARGET_8_MOD_MASK */
  for(int i=0;i<8;i++)
  {
    /* Update HV */
    if( mod_reg_hv_u_target_0_7 & mask )
    {
      /* Clear modified flags !!! */
      reg_bank_write(WD2_REG_HV_U_TARGET_0_7_MOD_FLAG, &mask, 1);
      /* Update HV via MSCB */
      mscb_write_param(i, 1);
    }
    /* Update HV */
    if( mod_reg_hv_u_target_8_15 & mask )
    {
      /* Clear modified flags !!! */
      reg_bank_write(WD2_REG_HV_U_TARGET_8_15_MOD_FLAG, &mask, 1);
      /* Update HV via MSCB */
      mscb_write_param(i+8, 1);
    }
    mask >>= 4;
  }
  return 1;
}
/******************************************************************************/

int update_drs_sampling_freq()
{
  unsigned int mask;
  unsigned int lmk_div0;
  unsigned int lmk_oscin;
  unsigned int lmk_r;
  unsigned int lmk_n;
  unsigned int fsample;
  double flt_fsamle;

  mask     = WD2_LMK0_CLKOUT_DIV_MASK;
  lmk_div0 = mask & reg_bank_get(WD2_REG_LMK_0);
  lmk_div0 >>= WD2_LMK0_CLKOUT_DIV_OFS;

  mask      = WD2_LMK13_OSCIN_FREQ_MASK;
  lmk_oscin = mask & reg_bank_get(WD2_REG_LMK_13);
  lmk_oscin >>= WD2_LMK13_OSCIN_FREQ_OFS;

  mask  = WD2_LMK14_PLL_R_MASK;
  lmk_r = mask & reg_bank_get(WD2_REG_LMK_14);
  lmk_r >>= WD2_LMK14_PLL_R_OFS;

  mask  = WD2_LMK15_PLL_N_MASK;
  lmk_n = mask & reg_bank_get(WD2_REG_LMK_15);
  lmk_n >>= WD2_LMK15_PLL_N_OFS;

  flt_fsamle = 1024.0d * (1000.0d * lmk_oscin * lmk_n / (lmk_r * lmk_div0)); /* = 1000 * 2 * 1024 * lmk_oscin * lmk_n / (lmk_r * lmk_div0 * 2); [kHz] */
  fsample = (unsigned int)(flt_fsamle+0.5);

  reg_bank_write(WD2_REG_DRS_SAMPLE_FREQ, &fsample, 1);

  return 1;
}

/******************************************************************************/

int update_adc_sampling_freq()
{
  unsigned int adc_sampling_div;
  unsigned int adc_sampling_freq;

  if(DBG_INFO) xfs_local_printf("Updating ADC sampling frequency\r\n\r\n");

  /* Clear modified flags !!! */
  reg_bank_write(WD2_REG_ADC_SAMPLE_DIV_MOD_FLAG, &ones, 1);

  adc_sampling_div = reg_bank_get(WD2_REG_ADC_SAMPLE_DIV) & WD2_ADC_SAMPLE_DIV_MASK;
  if( adc_sampling_div == 0 ) adc_sampling_div = 1;

  adc_sampling_freq = (WD2_DAQ_FREQ/1000)/adc_sampling_div;
  reg_bank_write(WD2_REG_ADC_SAMPLE_FREQ, &adc_sampling_freq, 1);

  return 1;
}

/******************************************************************************/

int update_lmk_ch(unsigned int mod_reg)
{
  unsigned int mask;
  unsigned int value;

  if(DBG_INFO) xfs_local_printf("Updating LMK channel configuration\r\n\r\n");

  mask = WD2_LMK_0_MOD_MASK;
  for(int i=0;i<8;i++)
  {
    if( mod_reg & mask )
    {
      /* Clear modified flags !!! */
      reg_bank_write(WD2_REG_LMK_0_7_MOD_FLAG, &mask, 1);
      value = reg_bank_get(WD2_REG_LMK_0+(i*4));
      /* validate register content by channel number */
      if( (value & 0x0F) == i)
      {
        lmk03000_set_channel(value);
      }
      else
      {
        return -1;
      }
    }
    mask >>= 4;
  }
  update_drs_sampling_freq();

  return 1;
}

/******************************************************************************/

int update_lmk()
{
  unsigned int drs_fsm_rst;

  if(DBG_INFO) xfs_local_printf("Updating LMK configuration\r\n\r\n");

  /* reset drs fsm while changing LMK PLL parameters */
  drs_fsm_rst = gpio_sc_get_drs_fsm_reset();
  gpio_sc_set_drs_fsm_reset(1);

  lmk03000_upload_configuration();
  update_drs_sampling_freq();

  gpio_sc_set_drs_fsm_reset(drs_fsm_rst);

  return 1;
}

/******************************************************************************/

int update_temperature()
{
  unsigned int value;
  int temperature;

  temperature = 0x0000FFFF & max31723_get_temp();

  value = ~WD2_TEMPERATURE_MASK & reg_bank_get(WD2_REG_STATUS);
  value = value | (temperature << WD2_TEMPERATURE_OFS);
  reg_bank_write(WD2_REG_STATUS, &value, 1);
  return 1;
}

/******************************************************************************/

int update_count_rate()
{
  rate_counter_update_rates();
  return 1;
}

/***************************************************************************/
/***************************************************************************/
