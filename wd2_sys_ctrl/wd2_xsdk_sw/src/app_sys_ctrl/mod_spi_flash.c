/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  18.02.2015 15:38:55
 *
 *  Description :  Module for SPI access to flash memory.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#include "drv_spi_flash_w25q64.h"
#include "xfs_printf.h"
#include "system.h"
#include "utilities.h"
#include <stdlib.h>
#include <ctype.h>
#include "dbg.h"
#include "cmd_processor.h"

/************************************************************/

  int spi_flash_write_enable(int argc, char **argv)
  {
    CMD_HELP("<state>",
             "set the WEL",
             "Sets the WEL in the status register to 1.\r\n" 
             "  <state> : enable or disable (1,0).\r\n"
            );

    /* Check for minimum number of arguments */
    if(argc < 2)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }

    if(fstrcmp(argv[1],"0"))
    {
      spi_flash_write_enable(SYSPTR(spi_flash), 0);
      xfs_printf("\r\nWEL reset (0)\r\n\r\n");
    }
    else
    {
      spi_flash_write_enable(SYSPTR(spi_flash), 1);
      xfs_printf("\r\nWEL set (1)\r\n\r\n");
    }

    return 0;
  }

/************************************************************/

  int spi_flash_get_jedec_id(int argc, char **argv)
  {
    unsigned char jedec_data[3];

    CMD_HELP("",
             "print Jedec id register content",
             "prints the contet of the Jedec id register."  
            );


    spi_flash_get_jedec_id(SYSPTR(spi_flash), jedec_data);
    xfs_printf("\r\nJedec Manufacturer ID 0x%02X (nominal 0xEF)\r\n", jedec_data[0]);
    xfs_printf("Jedec Memory Type     0x%02X (nominal 0x40)\r\n", jedec_data[1]);
    xfs_printf("Jedec Capacity        0x%02X (nominal 0x17)\r\n\r\n", jedec_data[2]);

    return 1;
  }

/************************************************************/

  int spi_flash_get_status_reg(int argc, char **argv)
  {
    unsigned char status_reg1;
    unsigned char status_reg2;

    CMD_HELP("",
             "print status register contents",
             "prints the contets of the spi flash status registers."  
            );


    status_reg1 = spi_flash_get_status_reg1(SYSPTR(spi_flash));
    status_reg2 = spi_flash_get_status_reg2(SYSPTR(spi_flash));

    xfs_printf("\r\nStatus Register Contents:\r\n");
    xfs_printf("Status Register 1:   0x%02X (SRP0, SEC, TB, BP2:BP0, WEL, BUSY)\r\n", status_reg1);
    xfs_printf("Status Register 2:   0x%02X (SUS, CMP, LB3:LB1, (R), QE, SRP1)\r\n\r\n", status_reg2);

    return 1;
  }

/************************************************************/

  int spi_flash_read(int argc, char **argv)
  {
    unsigned int first, last, address;
    unsigned int nr_of_bytes;
    unsigned char rx_buffer[1024];

    CMD_HELP("<address> <bytes>",
             "read from spi flash",
             "reads the specified number of bytes starting at <address>.\r\n"  
             "  <address> : address to start reading from.\r\n"
             "  <bytes>   : number of bytes to read (max. 1024).\r\n"
            );

    if(argc < 3)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }
    first = (unsigned int)strtol(argv[1], NULL, 0);

    /* round starting address down to multiple of 16 */
    address = (first / 16) * 16;

    nr_of_bytes = (unsigned int)strtol(argv[2], NULL, 0);

    /* limit to 1024 bytes */
    if(nr_of_bytes>1024) nr_of_bytes = 1024;
    last = first + nr_of_bytes;

    spi_flash_read(SYSPTR(spi_flash), rx_buffer, address, nr_of_bytes);

    xfs_printf("\r\nSPI flash data:\r\n\r\n");
    for(unsigned int row=0;row<(nr_of_bytes-1)/16+1;row++)
    {
      xfs_printf("[0x%08X]: ", address+row*16);
      for(unsigned int col=0; col<16 ; col++) {
        if (address+row*16+col >= first && address+row*16+col < last)
          xfs_printf("%02X ", rx_buffer[row*16+col]);
        else
          xfs_printf("   ");
      }
      for(unsigned int col=0; col<16 ; col++) {
        if (address+row*16+col >= first && address+row*16+col < last)
          xfs_printf("%c", isprint(rx_buffer[row*16+col]) ? rx_buffer[row*16+col] : '.');
      }
      xfs_printf("\r\n");
    }

    return 1;
  }

/************************************************************/

  int spi_flash_write(int argc, char **argv)
  {
    unsigned int address;
    int nr_of_bytes;
    unsigned char tx_buffer[256];

    CMD_HELP("<address> <data>",
             "write to spi flash",
             "Writes the specified number of bytes (max 256) to\r\n"  
             "the SPI flash, starting at the address specified.\r\n"  
             "  <address> : offset to start from.\r\n"
             "  <data>    : data represented by one big hex string\r\n"
            );


    /* Check for minimum number of arguments */
    if(argc < 3)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }

    address = strtol(argv[1], NULL, 0);
    nr_of_bytes = strlen(argv[2])/2;
    /* Validate argument range */
    if(nr_of_bytes > 256)
    {
      xfs_printf("Note: max. number of bytes to write is 256.\r\n");
      nr_of_bytes = 256;
    }

    for(int i=nr_of_bytes-1;i>=0;i--)
    {
      tx_buffer[i] = (unsigned char)(hatoi((const char*)(&argv[2][2*i])));
      argv[2][2*i] = '\0';
    }

    spi_flash_write(SYSPTR(spi_flash), tx_buffer, address, nr_of_bytes);
    xfs_printf("\r\n%d bytes written to SPI flash starting at address 0x%02X\r\n\r\n", nr_of_bytes, address);

    return 0;
  }

/************************************************************/

  int spi_flash_write_test(int argc, char **argv)
  {
    unsigned int address;
    int nr_of_bytes;
    unsigned char tx_buffer[1024];

    CMD_HELP("<address> <bytes>",
             "write to 1kB to spi flash as a test",
             "Writes the number of bytes to the SPI flash,\r\n"  
             "starting at the address specified and\r\n"
             "repeating the ascii characters 59(;) to 122(z).\r\n"
             "  <address> : offset to start from.\r\n"
             "  <bytes>   : number of bytes to read (max. 1024).\r\n"
            );


    /* Check for minimum number of arguments */
    if(argc < 3)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }

    address = strtol(argv[1], NULL, 0);
    nr_of_bytes = (unsigned int)strtol(argv[2], NULL, 0);
    /* limit to 1024 bytes */
    if(nr_of_bytes>1024) nr_of_bytes = 1024;

    for(int i=0;i<nr_of_bytes;i++)
    {
      /* use the 64 ascii characters 59 to 122 (; to z) */
      tx_buffer[i] = 59 + i%64;
    }

    spi_flash_write(SYSPTR(spi_flash), tx_buffer, address, nr_of_bytes);
    xfs_printf("\r\n%d bytes written to SPI flash starting at address 0x%02X\r\n\r\n", nr_of_bytes, address);

    return 0;
  }

/************************************************************/

  int spi_flash_erase(int argc, char **argv)
  {
    unsigned int address;

    CMD_HELP("<erase_type> <address>",
             "erases a part of the spi flash",
             "erases the specified part of the spi flash.\r\n"
             "  <erase_type> : unit to be erased (chip, sect, bl32, bl64).\r\n"
             "  <address>    : address of unit to be erased.\r\n"
            );


    /* Check for minimum number of arguments */
    if(argc < 2)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }
    if(fstrcmp(argv[1],"chip"))
    {
      spi_flash_chip_erase(SYSPTR(spi_flash));
    }
    else
    {
      if(argc < 3)
      {
        xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
        return 0;
      }
      address = (unsigned int)strtol(argv[2], NULL, 0);
      if(fstrcmp(argv[1],"sect"))
      {
        spi_flash_sector_erase(SYSPTR(spi_flash), address);
      }
      else if(fstrcmp(argv[1],"bl32"))
      {
        spi_flash_block32k_erase(SYSPTR(spi_flash), address);
      }
      else if(fstrcmp(argv[1],"bl64"))
      {
        spi_flash_block64k_erase(SYSPTR(spi_flash), address);
      }
    }


    return 1;
  }

/************************************************************/

  int spi_flash_reset(int argc, char **argv)
  {
    CMD_HELP("",
             "reset the spi flash"
            );


    if( DBG_INFO )  xfs_printf("Resetting SPI flash ...\r\n");
    spi_flash_reset(SYSPTR(spi_flash));
    if( DBG_INFO )  xfs_printf("done\r\n");

    return 1;
  }

/************************************************************/

  int module_sf_help(int argc, char **argv)
  {
    CMD_HELP("",
             "SPI Flash Module",
             "Allows to access the SPI configuration flash"  
            );

    return 0;
  }

/************************************************************/
/* COMMAND TABLE                                            */
/************************************************************/

  cmd_table_entry_type spi_flash_cmd_table[] =
  {
    {0, "sf", module_sf_help},
    {0, "sfjid", spi_flash_get_jedec_id},
    {0, "sfstat", spi_flash_get_status_reg},
    {0, "sfrd", spi_flash_read},
    {0, "sfwr", spi_flash_write},
    {0, "sfwrtst", spi_flash_write_test},
    {0, "sfers", spi_flash_erase},
    {0, "sfrst", spi_flash_reset},
    {0, "sfwen", spi_flash_write_enable},
    {0, NULL, NULL}
  };

/************************************************************/
