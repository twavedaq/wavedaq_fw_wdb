/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e, theidel
 *  Created :  22.09.2014 16:31:44
 *
 *  Description :  ADCs iserdes calibration functions.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

/***************************** Include Files *******************************/

#include "adc_serdes_calibration.h"
#include "system.h"
#include "update_config.h"
#include "drv_drp_pll.h"
/*#include "plb_idelay_control.h" */
#include "plb_serdes_cal_if.h"
#include "xfs_printf.h"
#include "dbg.h"
#include "register_map_wd2.h"

/************************** Function Definitions ***************************/

int ads_find_eye(unsigned int *pattern, unsigned int *adc_ph_step, unsigned int *adc_width, unsigned int max_phase_steps, unsigned int current_step, unsigned char mode, unsigned char print_details)
{
  unsigned int ph_step = 0;
  unsigned int eye_start, best_eye_start = 0;
  unsigned int eye_end, best_eye_end = 0;
  unsigned int eye_width = 0, best_eye_width = 0;
  unsigned int eye_center, best_eye_center = 0;
  unsigned int step_margin;
  unsigned int current_phase_ok = 0;

  while( ph_step<2*max_phase_steps )
  {
    while( (ph_step<2*max_phase_steps) && (pattern[ph_step%max_phase_steps] != VALUES_PER_PHASE_STEP*NO_ADC_CH)) ph_step++;
    eye_start = ph_step;

    while( (ph_step<2*max_phase_steps) && (pattern[ph_step%max_phase_steps] == VALUES_PER_PHASE_STEP*NO_ADC_CH)) ph_step++;
    eye_end = ph_step-1;

    /* get width */
    eye_width = eye_end + 1 - eye_start;
    /* get center (round up) */
    eye_center = (eye_start + eye_end + 1)/2;

    step_margin = eye_width/4;

    if(mode == ADC_CAL_MODE_CHECK)
    {
      if (print_details)
      {
        xfs_printf("        eye_start=%2d     eye_end=%2d     eye_width=%2d        center=%2d\r\n", eye_start%max_phase_steps, eye_end%max_phase_steps, eye_width, eye_center%max_phase_steps);
        xfs_printf("vs   margin_start=%2d  margin_end=%2d  margin_width=%2d  current_step=%2d\r\n", (eye_center-step_margin)%max_phase_steps, (eye_center+step_margin)%max_phase_steps, 2*step_margin, current_step);
      }
      if( ((                current_step > eye_center-step_margin) && (                current_step < eye_center+step_margin)) ||
          ((max_phase_steps+current_step > eye_center-step_margin) && (max_phase_steps+current_step < eye_center+step_margin)) )
      {
        if (print_details) xfs_printf("=> OK\r\n\r\n");
        current_phase_ok = 1;
        best_eye_start  = eye_start;
        best_eye_end    = eye_end;
        best_eye_width  = eye_width;
        best_eye_center = current_step;
      }
      else
      {
        if (print_details) xfs_printf("=> FAIL\r\n\r\n");
      }
    }
    else
    {
      if( eye_width > best_eye_width )
      {
        best_eye_start  = eye_start;
        best_eye_end    = eye_end;
        best_eye_width  = eye_width;
        best_eye_center = eye_center;
        if (print_details) xfs_printf("preliminary: eye_start=%2d  eye_end=%2d  eye_width=%2d  center=%2d\r\n", best_eye_start%max_phase_steps, best_eye_end%max_phase_steps, best_eye_width, best_eye_center%max_phase_steps);
      }
    }
  }

  if(mode == ADC_CAL_MODE_CHECK)
  {
    if (DBG_INFO) xfs_printf("final: eye_start=%2d  eye_end=%2d  eye_width=%2d  current_step=%2d\r\n", best_eye_start%max_phase_steps, best_eye_end%max_phase_steps, best_eye_width, best_eye_center%max_phase_steps);
    if (!current_phase_ok)
    {
      if (DBG_ERR) xfs_printf("ERROR: current step not within an EYE !!!\r\n");
      best_eye_center = 0;
      best_eye_width  = 0;
      return 0;
    }
    else
    {
      if (DBG_INFO)
      {
        if( (best_eye_start == 0) && (best_eye_end == 2*max_phase_steps-1) ) xfs_printf("=> All patterns match!!! Infinite Eye!!!\r\n");
        xfs_printf("=> Current PLL Phase OK !!!\r\n");
      }
    }
  }
  else
  {
    if (DBG_INFO) xfs_printf("final: eye_start=%2d  eye_end=%2d  eye_width=%2d  center=%2d\r\n", best_eye_start%max_phase_steps, best_eye_end%max_phase_steps, best_eye_width, best_eye_center%max_phase_steps);
    if ( (best_eye_start==(2*max_phase_steps-1)) || (best_eye_end==(2*max_phase_steps-1)) || (best_eye_width<MIN_EYE_WIDTH))
    {
      if (DBG_ERR) xfs_printf("ERROR: no valid EYE found !!!\r\n");
      best_eye_center = 0;
      best_eye_width  = 0;
      return 0;
    }
  }
  *adc_ph_step = best_eye_center%max_phase_steps;
  *adc_width   = best_eye_width;

  return 1;
}

/******************************************************************************/

int ads_find_eye_all_adcs(unsigned int *pattern, unsigned int *adc_ph_step, unsigned int *adc_width, unsigned int max_phase_steps, unsigned int current_step, unsigned char mode, unsigned char print_details)
{
  unsigned int ph_step = 0;
  unsigned int eye_start, best_eye_start = 0;
  unsigned int eye_end, best_eye_end = 0;
  unsigned int eye_width = 0, best_eye_width = 0;
  unsigned int eye_center, best_eye_center = 0;
  unsigned int step_margin;
  unsigned int current_phase_ok = 0;

  while( ph_step<2*max_phase_steps )
  {
    while( (ph_step<2*max_phase_steps) && (pattern[ph_step%max_phase_steps] != VALUES_PER_PHASE_STEP*NO_ADCS*NO_ADC_CH)) ph_step++;
    eye_start = ph_step;

    while( (ph_step<2*max_phase_steps) && (pattern[ph_step%max_phase_steps] == VALUES_PER_PHASE_STEP*NO_ADCS*NO_ADC_CH)) ph_step++;
    eye_end = ph_step-1;

    /* get width */
    eye_width  = eye_end + 1 - eye_start;
    /* get center (round up) */
    eye_center = (eye_start + eye_end + 1)/2;

    step_margin = eye_width/4;

    if(mode == ADC_CAL_MODE_CHECK)
    {
      if (print_details)
      {
        xfs_printf("        eye_start=%2d     eye_end=%2d     eye_width=%2d        center=%2d\r\n", eye_start%max_phase_steps, eye_end%max_phase_steps, eye_width, eye_center%max_phase_steps);
        xfs_printf("vs   margin_start=%2d  margin_end=%2d  margin_width=%2d  current_step=%2d\r\n", (eye_center-step_margin)%max_phase_steps, (eye_center+step_margin)%max_phase_steps, 2*step_margin, current_step);
      }
      if( ((                current_step > eye_center-step_margin) && (                current_step < eye_center+step_margin)) ||
          ((max_phase_steps+current_step > eye_center-step_margin) && (max_phase_steps+current_step < eye_center+step_margin)) )
      {
        if (print_details) xfs_printf("=> OK\r\n\r\n");
        current_phase_ok = 1;
        best_eye_start  = eye_start;
        best_eye_end    = eye_end;
        best_eye_width  = eye_width;
        best_eye_center = current_step;
      }
      else
      {
        if (print_details) xfs_printf("=> FAIL\r\n\r\n");
      }
    }
    else
    {
      if( eye_width > best_eye_width )
      {
        best_eye_start  = eye_start;
        best_eye_end    = eye_end;
        best_eye_width  = eye_width;
        best_eye_center = eye_center;
        if (print_details) xfs_printf("preliminary: eye_start=%2d  eye_end=%2d  eye_width=%2d  center=%2d\r\n", best_eye_start%max_phase_steps, best_eye_end%max_phase_steps, best_eye_width, best_eye_center%max_phase_steps);
      }
    }
  }

  if(mode == ADC_CAL_MODE_CHECK)
  {
    if (DBG_INFO) xfs_printf("final: eye_start=%2d  eye_end=%2d  eye_width=%2d  current_step=%2d\r\n", best_eye_start%max_phase_steps, best_eye_end%max_phase_steps, best_eye_width, best_eye_center%max_phase_steps);
    if (!current_phase_ok)
    {
      if (DBG_ERR) xfs_printf("ERROR: current step not within an EYE !!!\r\n");
      best_eye_center = 0;
      best_eye_width  = 0;
      return 0;
    }
    else
    {
      if (DBG_INFO)
      {
        if( (best_eye_start == 0) && (best_eye_end == 2*max_phase_steps-1) ) xfs_printf("=> All patterns match!!! Infinite Eye!!!\r\n");
        xfs_printf("=> Current PLL Phase OK !!!\r\n");
      }
    }
  }
  else
  {
    if (DBG_INFO) xfs_printf("final: eye_start=%2d  eye_end=%2d  eye_width=%2d  center=%2d\r\n", best_eye_start%max_phase_steps, best_eye_end%max_phase_steps, best_eye_width, best_eye_center%max_phase_steps);
    if ( (best_eye_start==(2*max_phase_steps)-1) || (best_eye_end==(2*max_phase_steps-1)) || (best_eye_width<MIN_EYE_WIDTH))
    {
      if (DBG_ERR) xfs_printf("ERROR: no valid EYE found !!!\r\n");
      best_eye_center = 0;
      best_eye_width  = 0;
      return 0;
    }
  }
  *adc_ph_step = best_eye_center%max_phase_steps;
  *adc_width   = best_eye_width;

  return 1;
}

/******************************************************************************/

void ads_timing_cal(adc_eye_cal_type *adc_cal, unsigned char mode, unsigned char print_details)
{
  unsigned int adc_data;
  unsigned int ph_step,loop,adc,adc_ch,word_nr;
  unsigned int pattern_even[NO_ADC_CH], pattern_odd[NO_ADC_CH];
  unsigned int pattern[NO_ADCS][NO_STEPS];
  unsigned int pattern_total[NO_STEPS];
  unsigned int max_phase_steps;
  unsigned int current_step;

  unsigned int adc_ph_step[NO_ADCS];
  unsigned int adc_width[NO_ADCS];
  unsigned int adc_ph_step_total;
  unsigned int adc_width_total;
  unsigned int timecount;
  unsigned int data;
#ifdef WD2_REV_F
  unsigned int adc_mode;
#endif

  int success = 0;

  unsigned int pattern_ch[NO_ADCS][NO_ADC_CH][NO_STEPS];

  if(mode == ADC_CAL_MODE_CHECK)
  {
    data = WD2_DAQ_CLK_DEF_PHASE_OK_MASK | WD2_DAQ_CLK_DEF_PHASE_CHKD_MASK;
    reg_bank_clr(WD2_DAQ_CLK_DEF_PHASE_CHKD_REG, &data, 1);
  }

  if(DBG_INFO) xfs_printf("\r\nInternal Data Clock Phase Calibration\r\n");

  if( !drp_pll_is_locked(SYSPTR(adc_pll_control)) )
  {
    if(DBG_ERR) xil_printf("Error: DAQ PLL not locked! Exiting test\r\n\r\n");
    /* Calibration or Check not possible => set default phase and exit */
    drp_pll_set_phase_step(SYSPTR(adc_pll_control), PLL_DIVCLK_CHANNEL, current_step & 0x07);
    drp_pll_set_phase_step(SYSPTR(adc_pll_control), PLL_DIVCLK_CHANNEL, current_step);
    return;
  }

#ifdef WD2_REV_F
  /* set ADC mode to alternating checkerboard */
  adc_mode = 4; /* alternating checkerboard mode (0x5.../0xA...) */
  /* shift mode bits in correct position */
  adc_mode <<= WD2_ADC_TM_OUT_0_A_OFS;

  /* write the mode configuration to the registers */
  for(adc_ch=0;adc_ch<8;adc_ch++)
  {
    reg_bank_read(WD2_REG_ADC_0_CH_A_CFG+adc_ch*4, &data, 1);
    data &= ~WD2_ADC_TM_OUT_0_A_MASK; /* clear current mode */
    data |= adc_mode;                 /* set new adc_mode */
    reg_bank_write(WD2_REG_ADC_0_CH_A_CFG+adc_ch*4, &data, 1);
    reg_bank_read(WD2_REG_ADC_1_CH_A_CFG+adc_ch*4, &data, 1);
    data &= ~WD2_ADC_TM_OUT_1_A_MASK; /* clear current adc_mode */
    data |= adc_mode;                 /* set new adc_mode */
    reg_bank_write(WD2_REG_ADC_1_CH_A_CFG+adc_ch*4, &data, 1);
  }
#else /* WD2_REV_F */
  /* activate test mode */
  data = WD2_ADC_CAL_PATTERN_EN_MASK;                 /* set new adc_mode */
  reg_bank_set(WD2_ADC_CAL_PATTERN_EN_REG, &data, 1);
#endif /* WD2_REV_F */

  /* get current phase step setting */
  reg_bank_read(WD2_REG_CTRL, &current_step, 1);
  /* shift current_step bits in correct position */
  current_step = (current_step & WD2_DAQ_DATA_PHASE_MASK) >> WD2_DAQ_DATA_PHASE_OFS;

  /* find maximum number of steps */
  max_phase_steps = drp_pll_get_phase_steps(SYSPTR(adc_pll_control), PLL_DIVCLK_CHANNEL);

  /* init pattern arrays */
  for(ph_step=0;ph_step<max_phase_steps;ph_step++)
  {
    for(adc=0; adc<NO_ADCS; adc++)
    {
      pattern[adc][ph_step] = 0;
      for(adc_ch=0;adc_ch<8;adc_ch++)
      {
        pattern_ch[adc][adc_ch][ph_step] = 0;
      }
    }
    pattern_total[ph_step] = 0;
  }

  if(DBG_INFO) xfs_printf("Reading data while changing delays...\r\n");
  for(ph_step=0;ph_step<max_phase_steps;ph_step++)
  {
    /* configure phase step */
    drp_pll_set_phase_step(SYSPTR(adc_pll_control), PLL_DIVCLK_CHANNEL, ph_step);
    usleep(1000);

    for(loop=0; loop<VALUES_PER_PHASE_STEP; loop++)
    {
      plb_serdes_cal_trigger_read(SYSPTR(serdes_cal_ctrl[0]), NO_WORDS_PER_RUN);
      plb_serdes_cal_trigger_read(SYSPTR(serdes_cal_ctrl[1]), NO_WORDS_PER_RUN);
      for(adc=0; adc<NO_ADCS; adc++)
      {
        timecount = 1000000;
        while( !plb_serdes_cal_data_is_available(SYSPTR(serdes_cal_ctrl[adc])) && timecount)
        {
          timecount--;
        }
        if(timecount)
        {
          /* reset the score values for each channel */
          for(adc_ch=0; adc_ch<NO_ADC_CH; adc_ch++)
          {
            pattern_odd[adc_ch]  = 0;
            pattern_even[adc_ch] = 0;
          }

          /* superimpose even and odd patterns channel by channel */
          for(word_nr=0; word_nr<NO_WORDS_PER_RUN; word_nr++)
          {
            for(adc_ch=0; adc_ch<NO_ADC_CH; adc_ch++)
            {
              adc_data = plb_serdes_cal_get(SYSPTR(serdes_cal_ctrl[adc]), adc_ch);
              if(word_nr%2) pattern_odd[adc_ch]  |= adc_data;
              else          pattern_even[adc_ch] |= adc_data;
            }
            if(!plb_serdes_cal_update(SYSPTR(serdes_cal_ctrl[adc])))
            {
              if (DBG_ERR) xfs_printf("Error: ADC %d update -> not enough data available. Exiting test\r\n", adc);
              /* Calibration or Check not possible => set default phase and exit */
              drp_pll_set_phase_step(SYSPTR(adc_pll_control), PLL_DIVCLK_CHANNEL, current_step & 0x07);
              drp_pll_set_phase_step(SYSPTR(adc_pll_control), PLL_DIVCLK_CHANNEL, current_step);
              return;
            }
          }

          /* generate score value for each channel */
          for(adc_ch=0; adc_ch<NO_ADC_CH; adc_ch++)
          {
            /* if (DBG_ALL) xfs_printf("channel %d   even_pattern 0x%04X   odd_pattern 0x%04X\r\n", adc_ch, pattern_even[adc_ch], pattern_odd[adc_ch]); */
            if ( ((pattern_odd[adc_ch] == 0x00000555) && (pattern_even[adc_ch] == 0x00000AAA)) ||
                 ((pattern_odd[adc_ch] == 0x00000AAA) && (pattern_even[adc_ch] == 0x00000555)) )
            {
              pattern_ch[adc][adc_ch][ph_step] ++;
              pattern[adc][ph_step] ++;
              pattern_total[ph_step] ++;
            }
          }
        }
        else
        {
          if (DBG_WARN) xfs_printf("Error: timeout in serdes cal update\r\n");
          /* Calibration or Check not possible => set default phase and exit */
          drp_pll_set_phase_step(SYSPTR(adc_pll_control), PLL_DIVCLK_CHANNEL, current_step & 0x07);
          drp_pll_set_phase_step(SYSPTR(adc_pll_control), PLL_DIVCLK_CHANNEL, current_step);
          return;
        }
      }
    }
  }

  if(DBG_INFO) xfs_printf("\r\nResults:\r\n");
  if (print_details)
  {
    xfs_printf("\r\nPhase-Step  ADC0  ADC1  Both ");
    for(adc=0; adc<NO_ADCS; adc++)
    {
      for(adc_ch=0; adc_ch<NO_ADC_CH; adc_ch++)
      {
        xfs_printf(" A%dC%d", adc, adc_ch);
      }
    }
    xfs_printf("\r\n");
    for(ph_step=0; ph_step<max_phase_steps; ph_step++)
    {
      xfs_printf("    %4d :  %4d  %4d  %4d ", ph_step, pattern[0][ph_step], pattern[1][ph_step], pattern_total[ph_step]);
      for(adc=0; adc<NO_ADCS; adc++)
      {
        for(adc_ch=0; adc_ch<NO_ADC_CH; adc_ch++)
        {
          xfs_printf(" %4d", pattern_ch[adc][adc_ch][ph_step]);
        }
      }
      xfs_printf("\r\n");
    }
  }

  if(print_details) xfs_printf("\r\nmax_phase_steps = %d\r\n", max_phase_steps);
  if(DBG_INFO || print_details) xfs_printf("\r\nFind eye...\r\n");
  /* find mid of eye pattern */
  for(adc=0; adc<NO_ADCS; adc++)
  {
    if (DBG_INFO) xfs_printf("ADC %d\r\n", adc);
    ads_find_eye(pattern[adc], &adc_ph_step[adc], &adc_width[adc], max_phase_steps, current_step, mode, print_details);
  }
  if (DBG_INFO) xfs_printf("Both ADCs\r\n");
  success = ads_find_eye_all_adcs(pattern_total, &adc_ph_step_total, &adc_width_total, max_phase_steps, current_step, mode, print_details);
  if (adc_cal)
  {
    adc_cal->eye_center = adc_ph_step_total;
    adc_cal->eye_width  = adc_width_total;
  }

  if(DBG_INFO) xfs_printf("\r\n");

  if(mode == ADC_CAL_MODE_CHECK)
  {
    data = WD2_DAQ_CLK_DEF_PHASE_CHKD_MASK;
    reg_bank_set(WD2_DAQ_CLK_DEF_PHASE_CHKD_REG, &data, 1);
    if(success)
    {
      data = WD2_DAQ_CLK_DEF_PHASE_OK_MASK;
      reg_bank_set(WD2_DAQ_CLK_DEF_PHASE_OK_REG, &data, 1);
    }
    drp_pll_set_phase_step(SYSPTR(adc_pll_control), PLL_DIVCLK_CHANNEL, current_step & 0x07);
    drp_pll_set_phase_step(SYSPTR(adc_pll_control), PLL_DIVCLK_CHANNEL, current_step);
  }
  else
  {
    drp_pll_set_phase_step(SYSPTR(adc_pll_control), PLL_BITCLK_CHANNEL, adc_ph_step_total & 0x07);
    drp_pll_set_phase_step(SYSPTR(adc_pll_control), PLL_DIVCLK_CHANNEL, adc_ph_step_total);
  }

#ifdef WD2_REV_F
  /* reset ADC test adc_mode configuration to normal operation */
  for(adc_ch=0;adc_ch<8;adc_ch++)
  {
    reg_bank_read(WD2_REG_ADC_0_CH_A_CFG+adc_ch*4, &data, 1);
    data &= ~WD2_ADC_TM_OUT_0_A_MASK; /* clear current adc_mode */
    reg_bank_write(WD2_REG_ADC_0_CH_A_CFG+adc_ch*4, &data, 1);
    reg_bank_read(WD2_REG_ADC_1_CH_A_CFG+adc_ch*4, &data, 1);
    data &= ~WD2_ADC_TM_OUT_1_A_MASK; /* clear current adc_mode */
    reg_bank_write(WD2_REG_ADC_1_CH_A_CFG+adc_ch*4, &data, 1);
  }
#else /* WD2_REV_F */
  /* deactivate test mode */
  data = WD2_ADC_CAL_PATTERN_EN_MASK;                 /* set new adc_mode */
  reg_bank_clr(WD2_ADC_CAL_PATTERN_EN_REG, &data, 1);
#endif /* WD2_REV_F */

  if(DBG_INFO) xfs_printf("Done\r\n");
}

/******************************************************************************/
/******************************************************************************/
