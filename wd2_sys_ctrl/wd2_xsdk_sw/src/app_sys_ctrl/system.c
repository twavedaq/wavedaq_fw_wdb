/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e, theidel
 *  Created :  02.05.2014 13:24:35
 *
 *  Description :  Central control for hardware access.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

/******************************************************************************/
/* include files                                                              */
/******************************************************************************/

#include "system.h"
#include "xparameters.h"
#include "fw_env.h"
#include "dhcp.h"
#include "wd2_flash_memory_map.h"
#include "utilities.h"
#include "drv_soft_spi_temp_dac_lmk.h"
#include "drv_plb_adcif_ber_test.h"
#include "drv_hard_spi_hv.h"
#include "drv_gpio_1wire_hv.h"
#ifdef WD2_REV_F
#include "drv_soft_spi_adc_ad9637.h"
#else
#include "drv_soft_spi_adc_ltm9009.h"
#endif
#include "gpio_shift_register.h"
#include "gpio_slow_control.h"
#include "plb_idelay_control.h"
#include "plb_serdes_cal_if.h"
#include "drv_drs4_control.h"
#include "update_config.h"
#include "xfs_printf.h"
#include "side_data.h"
#include "dbg.h"
#include "tftp_config.h"
#include "drv_rate_counter.h"
#include <stdlib.h>
#include "git-revision.h"

/******************************************************************************/
/* global vars                                                                */
/******************************************************************************/
system_type system_hw;

const char system_sw_build_date[] = __DATE__;
const char system_sw_build_time[] = __TIME__;
const char *system_month_str[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

unsigned int nw_if_ready;

unsigned int do_mscb = 1;

unsigned char gmac_com_buff[GMAC_COM_BUFF_LEN];

/******************************************************************************/

int fw_env_storage_handler_spi(fw_env_storage_desc_type *storage_desc, unsigned int cmd, char* mem_addr, unsigned int size)
{
  if (cmd == FW_ENV_STORAGE_READ)
  {
    xfs_printf("reading SPI flash: %d bytes at 0x%08x\r\n", size, storage_desc->addr);
    spi_flash_read( (spi_flash_w25q64*)(storage_desc->storage_ptr), (unsigned char*) mem_addr, storage_desc->addr, size);
    return FW_ENV_STORAGE_OK;
    /* tbd: check errors /  return FW_ENV_STORAGE_ERROR; */

  }
  else if (cmd == FW_ENV_STORAGE_WRITE)
  {
    xfs_printf("writing SPI flash: %d bytes at 0x%08x\r\n", size, storage_desc->addr);

    spi_flash_sect_start_range_erase((spi_flash_w25q64*)storage_desc->storage_ptr, storage_desc->addr, size);
    spi_flash_write((spi_flash_w25q64*)storage_desc->storage_ptr, (unsigned char*) mem_addr, storage_desc->addr, size);

    return FW_ENV_STORAGE_OK;
    /* tbd: check errors /  return FW_ENV_STORAGE_ERROR; */

  }

  return FW_ENV_STORAGE_ERROR;
}

/******************************************************************************/
/*
fw_env_storage_desc_type env_storage_description[] =
{
   { SYSPTR(spi_flash), SPI_FLASH_ENVIRONMENT_ADDR,   fw_env_storage_handler_spi}
//   { SYSPTR(spi_flash), CB_SPI_FLASH_ENV0_ADDR,   fw_env_storage_handler_spi},
//   { SYSPTR(spi_flash), CB_SPI_FLASH_ENV1_ADDR,   fw_env_storage_handler_spi}
};

#define ENV_STORAGE_ENTRIES (sizeof(env_storage_description)/sizeof(fw_env_storage_desc_type))
*/
/******************************************************************************/

/************************************************************/

void set_hostname_func(const char *name, const char* val)
{
  ncpy(SYSPTR(cfg)->hostname, val, MAX_HOSTNAME_LENGTH);
  /*strncpy(cfg.hostname, cp, MAX_HOSTNAME_LENGTH); */
  SYSPTR(cfg)->hostname[MAX_HOSTNAME_LENGTH-1]=0;
};

/************************************************************/

void set_sn_func(const char *name, const char* val)
{
  SYSPTR(cfg)->serial_no = xfs_atoi(val);
};

/************************************************************/

void set_dhcp_func(const char *name, const char* val)
{
  nw_if_set_dhcp(SYSPTR(nw_if_gmac_0), xfs_atoi(val));

  if (!SYSPTR(nw_if_gmac_0)->do_dhcp)
  {
    int status;
    unsigned char ip_addr[4];
    status = parse_ip(fw_getenv(SYSPTR(env), "ipaddr"), ip_addr);
    if (status == XFS_OK)
    {
      nw_if_set_ip_address(SYSPTR(nw_if_gmac_0), ip_addr);
    }
  }
};

/************************************************************/

void set_mac_func(const char *name, const char* val)
{
  int status;
  unsigned char mac_addr[6];

  status = parse_mac(val, mac_addr);
  if (status == XFS_OK)
  {
    nw_if_set_mac_address(SYSPTR(nw_if_gmac_0), mac_addr);
  }
};

/************************************************************/

void set_ip_func(const char *name, const char* val)
{
  if (!SYSPTR(nw_if_gmac_0)->do_dhcp)
  {
    int status;
    unsigned char ip_addr[4];

    status = parse_ip(val, ip_addr);
    if (status == XFS_OK)
    {
      nw_if_set_ip_address(SYSPTR(nw_if_gmac_0), ip_addr);
    }
  }
};

/************************************************************/

void set_srcport_func(const char *name, const char* val)
{
  parse_port(val, &(SYSPTR(cfg)->nw_data_src_port));
}

/************************************************************/

void set_dbglvl_func(const char *name, const char* val)
{
  unsigned int d;

  d = get_dbg_level();
  xfs_printf("Old dbglvl: %2d  (%s)\r\n", d, dbg_level_str[d]);

  d = parse_dbglvl(val);
  set_dbg_level(d);

  d = get_dbg_level();
  xfs_printf("New dbglvl: %2d  (%s)\r\n", d, dbg_level_str[d]);
}

/************************************************************/

void set_sidedata_func(const char *name, const char* val)
{
  parse_side_data(val, SYSPTR(cfg)->side_data);
}

/************************************************************/

void set_mscb_func(const char *name, const char* val)
{
  do_mscb = xfs_atoi(val);
}

/************************************************************/

/*
void trg_func(const char *name, const char* val)
{
  xfs_printf("tbd:  trg_func %s %S\r\n", name, val);
};
*/

/************************************************************/

fw_env_trigger_entry_type fw_env_trigger_table[] =
{
  {"hostname",   set_hostname_func},
  {"sn",         set_sn_func},
  {"ipaddr",     set_ip_func},
  {"ethaddr",    set_mac_func},
  {"ethdhcp",    set_dhcp_func},
  {"srcport",    set_srcport_func},
  {"dbglvl",     set_dbglvl_func},
  {"sd",         set_sidedata_func},
  {"mscb",       set_mscb_func},
  {NULL, NULL}
};

/************************************************************/

char env_mem[SPI_FLASH_ENVIRONMENT_SIZE];

char default_env[] = "sn=0\0"
                     "hostname=WD2new\0"
                     "ethaddr=00:50:c2:46:d9:04\0"
                     "ipaddr=10.1.0.1\0"
                     "ethdhcp=1\0"
                     "srcport=3000\0"
                     "\0";

#define DEFAULT_ENV_SIZE  sizeof(default_env)

/******************************************************************************/

void system_construct(void)
{
  const char initial_hostname[] = "WD2new";
  const unsigned char initial_eth_mac_addr[6] = { 0x00, 0x50, 0xc2, 0x46, 0xd9, 0x04 } ;
  const unsigned char initial_eth_ip_addr[4]  = { 10, 1, 0, 1} ;
  const unsigned int  initial_dhcp = 1;
  const unsigned int  initial_src_port = 3000;

  plb_gpio_init(SYSPTR(gpio_sys), XPAR_PLB_GPIO_0_BASEADDR);
  gpio_sc_init();
  gpio_sc_set_sw_status(GPIO_LED_SW_STATUS_BL_LOAD);

  nw_if_ready = 0;
  set_cmd_sender(XFS_PRINTF_TARGET_LOCAL);
  set_cmd_reply_mode(XFS_PRINTF_REPLY_MODE_SENDER);

  /*plb_adcif_ber_test_init(SYSPTR(adcif_pn_test_a), XPAR_PLB_ADCIF_BER_TEST_A_BASEADDR); */
  /*plb_adcif_ber_test_init(SYSPTR(adcif_pn_test_b), XPAR_PLB_ADCIF_BER_TEST_B_BASEADDR); */

  drp_pll_init(SYSPTR(adc_pll_control), XPAR_PLB_DRP_IF_DAQ_PLL_0_BASEADDR, 100, CLKFB_SRC_CLKFBOUT, PLL_BANDWIDTH_LOW);

  plb_uart_init(SYSPTR(MSCB_uart), XPAR_RS232_MSCB_BASEADDR);

  plb_ll_fifo_init(SYSPTR(plb_fifo_gmac_0), XPAR_PLB_LL_FIFO_GMAC_LITE_0_BASEADDR);

  gmac_lite_construct(SYSPTR(gmac_lite_0), XPAR_GMAC_LITE_0_BASEADDR);

  dhcp_construct(SYSPTR(dhcp_gmac_0), SYSPTR(nw_if_gmac_0));

  nw_if_construct(SYSPTR(nw_if_gmac_0), "", SYSPTR(plb_fifo_gmac_0), SYSPTR(dhcp_gmac_0), SYSPTR(gmac_lite_0), gmac_com_buff, GMAC_COM_BUFF_LEN);

  nw_if_init(SYSPTR(nw_if_gmac_0), initial_eth_mac_addr, initial_eth_ip_addr, initial_dhcp);

  SYSPTR(cfg)->nw_data_src_port = initial_src_port;
  ncpy(&(SYSPTR(cfg)->hostname), initial_hostname, NAME_BUF_LEN);

  /*serdes_eth_testgen_init(SYSPTR(serdes_eth_testgen), XPAR_PLB_TEST_FRAME_CTRL_0_BASEADDR); */
  udp_pack_gen_construct(SYSPTR(udp_pack_gen), XPAR_PLB_BRAM_0_MEM0_BASEADDR);

  /*env_storage_description[0] = { &spi_flash, SPI_FLASH_ENVIRONMENT_ADDR,   fw_env_storage_handler_spi }; */
  system_hw.env_storage_description[0].storage_ptr     = SYSPTR(spi_flash);
  system_hw.env_storage_description[0].addr            = SPI_FLASH_ENVIRONMENT_ADDR;
  system_hw.env_storage_description[0].storage_handler = fw_env_storage_handler_spi;
}

/******************************************************************************/

xfs_u32 system_init(void)
{
  unsigned int ones = 0xFFFFFFFF;
  unsigned int reg_val;

  xfs_u32 init_status = XFS_SUCCESS;

  soft_spi_temp_dac_lmk_init();
  spi_hv_init(XPAR_PLB_SPI_MASTER_HV_BOARD_BASEADDR);
#ifdef WD2_REV_F
  ad9637_soft_spi_adc_init();
#else
  ltm9009_soft_spi_adc_init();
#endif
  gpio_sreg_init();
  rate_counter_init(XPAR_PLB_SCALER_0_BASEADDR, XPAR_PLB_SCALER_0_MEM0_BASEADDR);
  /*gpio_drs4_cfg_init(); */ /* Replace by new init Function (with init rsr) */
  /*gpio_looptest_init(); */

  /* init spi slave */
  sspi_init(SYSPTR(spi_syslink_slave), XPAR_PLB_WD2_SPI_SLAVE_IF_0_BASEADDR);
  /*plb_idelay_init(adc_idelay_ctrl_ptr, XPAR_PLB_IDELAY_CONTROL_0_BASEADDR, IDELAY_BUSY_TIMEOUT); */
  plb_serdes_cal_init(SYSPTR(serdes_cal_ctrl[0]), XPAR_PLB_SERDES_CAL_IF_0_BASEADDR, SERDES_CAL_TIMEOUT);
  plb_serdes_cal_init(SYSPTR(serdes_cal_ctrl[1]), XPAR_PLB_SERDES_CAL_IF_1_BASEADDR, SERDES_CAL_TIMEOUT);

  /* init register bank */
  reg_bank_init(XPAR_PLB_WD2_REG_BANK_0_MEM0_BASEADDR, XPAR_PLB_WD2_REG_BANK_0_MEM1_BASEADDR);
  /* load initial values to control register */
  reg_bank_load();
  /* write debug MUX register to make sure RS232 is enabled per default */
  reg_val = 0x00000000;
  reg_bank_write(WD2_REG_DBG_SIG_SEL, &reg_val, 1);
  /* write serial number to status register */
  reg_val = SYSPTR(cfg)->serial_no;
  reg_bank_write(WD2_REG_SN, &reg_val, 1);
  /* write protocol version to status register */
  /* reg_val = 5;                                                     */
  /* reg_bank_write(WD2_REG_PROT_VER, &reg_val, 1); */
  /* write software build date to status register */
  reg_val = reg_sw_build_date();
  reg_bank_write(WD2_REG_SW_BUILD_DATE, &reg_val, 1);
  /* write software build time to status register */
  reg_val = reg_sw_build_time();
  reg_bank_write(WD2_REG_SW_BUILD_TIME, &reg_val, 1);
  /* write software GIT hashtag to status register */
  reg_val = GIT_REV_HASH;
  reg_bank_write(WD2_REG_SW_GIT_HASH_TAG, &reg_val, 1);
  /* clear daq clock calibration status */
  reg_val = WD2_DAQ_CLK_DEF_PHASE_OK_MASK | WD2_DAQ_CLK_DEF_PHASE_CHKD_MASK;
  reg_bank_clr(WD2_DAQ_CLK_DEF_PHASE_CHKD_REG, &reg_val, 1);

  /* Update the system configuration according to the registers set */
  reg_bank_write(WD2_REG_APLY_CFG, &ones, 1);
  trigger_update_configurations();

  tftp_config_init();

  tftp_server_construct(SYSPTR(tftp_gmac_0), SYSPTR(nw_if_gmac_0), tftp_targets);

  nw_if_ready = 1;

  if (DBG_INIT) xfs_printf("Host name      : %s\r\n", SYSPTR(cfg)->hostname);

  /* tftp_config_init(); */

  lmk03000_sync();
  usleep(100);

#ifdef WD2_REV_F
  ad9637_reset();
#endif

  /* Reset specific parts of the system */
  reg_val = WD2_SCALER_RST_MASK |
            WD2_TRB_PARITY_ERROR_COUNT_RST_MASK |
            WD2_ADC_IF_RST_MASK |
            WD2_WD_PKGR_RST_MASK |
            WD2_EVENT_COUNTER_RST_MASK;
  reg_bank_write(WD2_REG_RST, &reg_val, 1);
  reg_val = 0;
  reg_bank_write(WD2_REG_RST, &reg_val, 1);

  ads_timing_cal(SYSPTR(adc_cal_info), ADC_CAL_MODE_CHECK, 0);

  gpio_sc_set_drs_fsm_reset(0);

  /* configure DRS chip */
  reg_val = WD2_APPLY_SETTINGS_DRS_MASK;
  reg_bank_write(WD2_REG_APLY_CFG, &reg_val, 1);
  trigger_update_configurations();

  drs4_init();

#ifdef WD2_REV_F
  ad9637_reset();
#endif

  if(init_status != XFS_SUCCESS)
  {
    /* LED red */
    return XFS_FAILURE;
  }
  else
  {
    /* LED green */
    return XFS_SUCCESS;
  }
  return XFS_SUCCESS;
}

/******************************************************************************/

unsigned int reg_sw_build_date()
{
 /* STATUS REGISTERS COMB_SW_BUILD_DATE            Read only Register */
 /* */
/*  31:16 comb_sw_build_year  unsigned int16, Year of the ComBoard software build Encoded as packed BCD */
/*  15:8  comb_sw_build_month unsigned int8, Month of the ComBoard software build Encoded as packed BCD */
/*  7:0   comb_sw_build_day   unsigned int8, Day of the ComBoard software build   Encoded as packed BCD */

  const char *s = system_sw_build_date;
  int m;

  for(m=1; m<=12; m++) if (fstrpcmp(system_month_str[m-1], s)) break;

  return ( ((s[7]-'0')<<28) | ((s[8]-'0')<<24) | ((s[9]-'0')<<20) | ((s[10]-'0')<<16) /* Year  */
         | ((m/10)<<12) | ((m%10)<<8)                     /* Month */
         | ((s[4]==' ')?0:(s[4]-'0')<<4) | (s[5]-'0') );  /* Day   */
}


/******************************************************************************/

unsigned int reg_sw_build_time()
{
 /* STATUS REGISTERS SW_BUILD_TIME            Read only Register */
 /* */
/*  31:24 reserved                                                                   */
/*  23:16 comb_sw_build_hour   unsigned int8, Hour of the ComBoard software build   Encoded as packed BCD */
/*  15:8  comb_sw_build_minute unsigned int8, Minute of the ComBoard software build Encoded as packed BCD */
/*  7:0   comb_sw_build_second unsigned int8, Second of the ComBoard software build Encoded as packed BCD */

  const char *s = system_sw_build_time;

  return ((s[0]-'0')<<20)|((s[1]-'0')<<16)|((s[3]-'0')<<12)|((s[4]-'0')<<8)|((s[6]-'0')<<4)|(s[7]-'0');
}

/******************************************************************************/

xfs_u32 init_env(void)
{
  /* SPI Flash */
  /* Initialized here because the            */
  /* envirenment variables are stored in the */
  /* SPI flash memory.                       */
  mspi_init(SYSPTR(cfg_spi_master), XPAR_PLB_SPI_MASTER_CONFIG_MEMORY_BASEADDR, 1);
  mspi_slv_init(SYSPTR(cfg_spi_master), SYSPTR(cfg_spi_slave_settings), 0xFFFFFFFE, W25Q64_CPOL, W25Q64_CPHA, W25Q64_LSB_FIRST);
  spi_flash_init(SYSPTR(spi_flash), SYSPTR(cfg_spi_slave_settings));

  fw_env_init(SYSPTR(env), env_mem, SPI_FLASH_ENVIRONMENT_SIZE, default_env, DEFAULT_ENV_SIZE, system_hw.env_storage_description, ENV_STORAGE_ENTRIES, fw_env_trigger_table);

  fw_env_load(SYSPTR(env));

  return XFS_SUCCESS;
}

/************************************************************/

void init_settings(int snr)
{
  /**************************************************************/

  init_env_settings(snr);
  xfs_printf("\n\rStoring environment variables in SPI flash\n\r");
  fw_env_save(SYSPTR(env));

  /**************************************************************/

  init_reg_settings();
  xfs_printf("\r\nStoring register bank contents in SPI flash\r\n");
  reg_bank_store();
  reg_bank_write(WD2_REG_SN, (unsigned int*)(&snr), 1);

  /**************************************************************/
  xfs_printf("\n\r*** System Initialization Complete ***\r\n\n\r");
}

/******************************************************************************/

void init_env_settings(int snr)
{
  int cmd_buf_size = 10;
  int name_buf_size = 10;
  int val_buf_size = 30;
  char command[cmd_buf_size];
  char name[name_buf_size];
  char value[val_buf_size];
  char *buffer_ptr[3] = {command, name, value};
  unsigned int board_variant;

  board_variant = (reg_bank_get(WD2_BOARD_VARIANT_REG) & WD2_BOARD_VARIANT_MASK) >> WD2_BOARD_VARIANT_OFS;

  xfs_snprintf(command, cmd_buf_size, "setenv\0");

  /* set envirnment serial number */
  xfs_snprintf(name,  name_buf_size, "sn\0");
  xfs_snprintf(value, val_buf_size,  "%d\0", (unsigned int)snr);
  if(DBG_INFO) xfs_printf("%s %s %s\r\n", command, name, value);
  fw_setenv(SYSPTR(env), 3, buffer_ptr);
  /* set envirnment hostname */
  xfs_snprintf(name,  name_buf_size, "hostname\0");
  xfs_snprintf(value, val_buf_size,  "wd%03d\0", (unsigned int)snr);
  if(DBG_INFO) xfs_printf("%s %s %s\r\n", command, name, value);
  fw_setenv(SYSPTR(env), 3, buffer_ptr);
  /* set envirnment mac address */
  if ( (board_variant & BOARD_VARIANT_ETHERNET_MASK) == 0)
  {
    xfs_snprintf(name,  name_buf_size, "ethaddr\0");
    xfs_snprintf(value, val_buf_size,  "00:50:C2:46:D4:%02X\0", (unsigned int)snr);
    if(DBG_INFO) xfs_printf("%s %s %s\r\n", command, name, value);
    fw_setenv(SYSPTR(env), 3, buffer_ptr);
  }

  /* set envirnment dhcp */
  xfs_snprintf(name,  name_buf_size, "ethdhcp\0");
  if ( (board_variant & BOARD_VARIANT_ETHERNET_MASK) == 0)
  {
    xfs_snprintf(value, val_buf_size,  "1\0");
  }
  else
  {
    xfs_snprintf(value, val_buf_size,  "0\0");
  }
  if(DBG_INFO) xfs_printf("%s %s %s\r\n", command, name, value);
  fw_setenv(SYSPTR(env), 3, buffer_ptr);
  /* set envirnment debug level */
  xfs_snprintf(name,  name_buf_size, "dbglvl\0");
  xfs_snprintf(value, val_buf_size,  "none\0");
  if(DBG_INFO) xfs_printf("%s %s %s\r\n", command, name, value);
  fw_setenv(SYSPTR(env), 3, buffer_ptr);
  /* set envirnment static ip address */
  xfs_snprintf(name,  name_buf_size, "ipaddr\0");
  xfs_snprintf(value, val_buf_size,  "10.1.0.1\0");
  if(DBG_INFO) xfs_printf("%s %s %s\r\n", command, name, value);
  fw_setenv(SYSPTR(env), 3, buffer_ptr);
  /* set envirnment udp source port */
  xfs_snprintf(name,  name_buf_size, "srcport\0");
  xfs_snprintf(value, val_buf_size,  "3000\0");
  if(DBG_INFO) xfs_printf("%s %s %s\r\n", command, name, value);
  fw_setenv(SYSPTR(env), 3, buffer_ptr);
  /* set envirnment side bank definition for scalers */
  /*
  xfs_snprintf(name,  name_buf_size, "sd\0");
  xfs_snprintf(value, val_buf_size,  "scal,0x%08X,34,t,10\0", XPAR_PLB_WD2_REG_BANK_0_MEM1_BASEADDR+REG_SCALER_0_LSB_OFFSET);
  if(DBG_INFO) xfs_printf("%s %s %s\r\n", command, name, value);
  fw_setenv(3, buffer_ptr);
  */
}

/******************************************************************************/

void init_reg_settings()
{
  unsigned int reg_base_addr = SYSPTR(reg_bank)->control_reg_base_address;

  if(DBG_INFO) xfs_printf("\r\nInitializing Control Register:\r\n");
  for(unsigned int i=0;i<REG_NR_OF_CTRL_REGS-1;i++) /* Last register is the checksum */
  {
    if(DBG_INFO) xfs_printf("[0x%08X]: 0x%08X\r\n", reg_base_addr+i*4, ctrl_reg_default[i]);
    reg_bank_write(REG_CTRL_START+i*4, (unsigned int*)(&ctrl_reg_default[i]), 1);
  }
}

/******************************************************************************/

void print_sys_info(void)
{
  /*unsigned int version; */
  unsigned int build_date;
  unsigned int build_time;
  unsigned int git_rev_fw;
  unsigned int hw_version;

  unsigned short year;
  unsigned char month;
  unsigned char day;

  unsigned char hour;
  unsigned char minute;
  unsigned char second;
  unsigned char compat_level;

  unsigned char board_variant;
  unsigned char board_type;
  unsigned char board_revision;

  /* version = xfs_in32(baseaddr); */
  build_date = reg_bank_get(WD2_REG_FW_BUILD_DATE);
  build_time = reg_bank_get(WD2_REG_FW_BUILD_TIME);
  git_rev_fw = reg_bank_get(WD2_REG_FW_GIT_HASH_TAG);
  hw_version = reg_bank_get(WD2_REG_HW_VER);

  /* read and convert from BCD to binary */
  year         = (build_date & WD2_FW_BUILD_YEAR_MASK)  >> WD2_FW_BUILD_YEAR_OFS;
  year         = ((year >> 12) & 0xF) * 1000 + ((year >> 8) & 0xF) * 100 + ((year >> 4) & 0xF) * 10 + (year & 0xF);
  month        = (build_date & WD2_FW_BUILD_MONTH_MASK) >> WD2_FW_BUILD_MONTH_OFS;
  month        = ((month >> 4) & 0xF) * 10 + (month & 0xF);
  day          = (build_date & WD2_FW_BUILD_DAY_MASK)   >> WD2_FW_BUILD_DAY_OFS;
  day          = ((day >> 4) & 0xF) * 10 + (day & 0xF);

  compat_level = (build_time & WD2_FW_COMPAT_LEVEL_MASK) >> WD2_FW_COMPAT_LEVEL_OFS;
  /* read and convert from BCD to binary */
  hour         = (build_time & WD2_FW_BUILD_HOUR_MASK)   >> WD2_FW_BUILD_HOUR_OFS;
  hour         = ((hour >> 4) & 0xF) * 10 + (hour & 0xF);
  minute       = (build_time & WD2_FW_BUILD_MINUTE_MASK) >> WD2_FW_BUILD_MINUTE_OFS;
  minute       = ((minute >> 4) & 0xF) * 10 + (minute & 0xF);
  second       = (build_time & WD2_FW_BUILD_SECOND_MASK) >> WD2_FW_BUILD_SECOND_OFS;
  second       = ((second >> 4) & 0xF) * 10 + (second & 0xF);

  board_variant  = (hw_version & WD2_BOARD_VARIANT_MASK)  >> WD2_BOARD_VARIANT_OFS;
  board_type     = (hw_version & WD2_BOARD_TYPE_MASK)     >> WD2_BOARD_TYPE_OFS;
  board_revision = (hw_version & WD2_BOARD_REVISION_MASK) >> WD2_BOARD_REVISION_OFS;

  xfs_printf("-- Compatibility Level: %d\r\n\r\n", compat_level);
  xfs_printf("-- FW GIT Revision:     0x%07X\r\n", git_rev_fw);
  xfs_printf("-- SW GIT Revision:     0x%X\r\n\r\n", GIT_REV_HASH);
  xfs_printf("-- FW Build:            %s %2d %04d  %02d:%02d:%02d\r\n", system_month_str[(month-1)%12], day, year, hour, minute, second);
  xfs_printf("-- SW Build:            %s  %s\r\n\r\n",system_sw_build_date,system_sw_build_time);
  if(board_type == 0x02) xfs_printf("-- Board Type:          WaveDREAM2\r\n");
  else                   xfs_printf("-- Board Type:          0x%02X -> error\r\n", board_type);
  xfs_printf("-- Board Revision:      %c\r\n", 0x41 + board_revision); /* 0x41 = ASCII "A" */
  xfs_printf("-- Board Variant:       0x%02X\r\n", board_variant);
}

/******************************************************************************/
