/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created : 02.02.2016 12:42:29
 *
 *  Description :  BER test for ADC interface on WaveDream2.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

/***************************** Include Files *******************************/

#include "drv_plb_adcif_ber_test.h"
#include "utilities.h"

/******************************************************************************/

void plb_adcif_ber_test_init(plb_adcif_ber_test_type *self, xfs_u32 b_address)
{
  self->base_address = io_remap(b_address);
}

/******************************************************************************/

void plb_adcif_ber_test_reset(plb_adcif_ber_test_type *self)
{
  xfs_u32 reg_val;
  
  reg_val = xfs_in32(self->base_address+ADCIF_BER_TEST_REG_OFS_CONTROL);
  xfs_out32(self->base_address+ADCIF_BER_TEST_REG_OFS_CONTROL, reg_val | ADCIF_BER_TEST_BIT_CTRL_RESET);
  usleep(1);
  xfs_out32(self->base_address+ADCIF_BER_TEST_REG_OFS_CONTROL, reg_val);
}

/******************************************************************************/

void plb_adcif_ber_test_start(plb_adcif_ber_test_type *self)
{
  xfs_u32 reg_val;
  
  reg_val = xfs_in32(self->base_address+ADCIF_BER_TEST_REG_OFS_CONTROL);
  xfs_out32(self->base_address+ADCIF_BER_TEST_REG_OFS_CONTROL, reg_val | ADCIF_BER_TEST_BIT_CTRL_ENABLE);
}

/******************************************************************************/

void plb_adcif_ber_test_stop(plb_adcif_ber_test_type *self)
{
  xfs_u32 reg_val;
  
  reg_val = xfs_in32(self->base_address+ADCIF_BER_TEST_REG_OFS_CONTROL);
  xfs_out32(self->base_address+ADCIF_BER_TEST_REG_OFS_CONTROL, reg_val & ~ADCIF_BER_TEST_BIT_CTRL_ENABLE);
}

/******************************************************************************/

xfs_u32 plb_adcif_ber_test_is_running(plb_adcif_ber_test_type *self)
{
  return (ADCIF_BER_TEST_BIT_STAT_RUNNING & xfs_in32(self->base_address+ADCIF_BER_TEST_REG_OFS_STATUS));
}

/******************************************************************************/

void plb_adcif_ber_test_inject_error(plb_adcif_ber_test_type *self)
{
  xfs_u32 reg_val;
  
  reg_val = xfs_in32(self->base_address+ADCIF_BER_TEST_REG_OFS_CONTROL);
  xfs_out32(self->base_address+ADCIF_BER_TEST_REG_OFS_CONTROL, reg_val | ADCIF_BER_TEST_BIT_CTRL_INJECT_ERROR);
  usleep(1);
  xfs_out32(self->base_address+ADCIF_BER_TEST_REG_OFS_CONTROL, reg_val);
}

/******************************************************************************/

void plb_adcif_ber_test_set_pn_pattern(plb_adcif_ber_test_type *self, xfs_u32 pn_pattern)
{
  xfs_u32 reg_val;
  
  reg_val = xfs_in32(self->base_address+ADCIF_BER_TEST_REG_OFS_CONTROL);
  if(pn_pattern)
  {
    /* select PN23 pattern by setting PN pattern bit */
    xfs_out32(self->base_address+ADCIF_BER_TEST_REG_OFS_CONTROL, reg_val | ADCIF_BER_TEST_BIT_CTRL_PN_PATTERN);
  }
  else
  {
    /* select PN9 pattern by clearing PN pattern bit */
    xfs_out32(self->base_address+ADCIF_BER_TEST_REG_OFS_CONTROL, reg_val & ~ADCIF_BER_TEST_BIT_CTRL_PN_PATTERN);
  }
}

/******************************************************************************/

void plb_adcif_ber_test_get_errors(plb_adcif_ber_test_type *self, xfs_u32 channel, xfs_u32 *errors)
{
  /* error counter is 64 bit */
  errors[0] = xfs_in32(self->base_address+ADCIF_BER_TEST_REG_BASE_OFS_ERROR+(2*channel*4));
  errors[1] = xfs_in32(self->base_address+ADCIF_BER_TEST_REG_BASE_OFS_ERROR+((2*channel+1)*4));
}

/******************************************************************************/

void plb_adcif_ber_test_get_samples(plb_adcif_ber_test_type *self, xfs_u32 channel, xfs_u32 *samples)
{
  /* samples counter is 64 bit */
  samples[0] = xfs_in32(self->base_address+ADCIF_BER_TEST_REG_BASE_OFS_SAMPLE+(2*channel*4));
  samples[1] = xfs_in32(self->base_address+ADCIF_BER_TEST_REG_BASE_OFS_SAMPLE+((2*channel+1)*4));
}

/******************************************************************************/
/******************************************************************************/

