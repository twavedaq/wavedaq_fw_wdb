/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e (Author of generation script)
 *  Created :  11.08.2016 12:36:32
 *
 *  Description :  Register map definitions.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#ifndef __DRP_PLL_PARAM_H__
#define __DRP_PLL_PARAM_H__

#define FILTER_TABLE_HIGH_BW   0
#define FILTER_TABLE_LOW_BW    1

typedef struct { unsigned char addr;  unsigned char pos;} drp_description_type;

typedef struct
{
  unsigned char lock_ref_dly;
  unsigned char lock_fb_dly;
  unsigned int  lock_cnt;
  unsigned int  lock_sat_high;
  unsigned int  unlock_cnt;
} lock_table_entry_type;

typedef struct
{
  unsigned char res;
  unsigned char lfhf;
  unsigned char cp;
} filter_table_entry_type;

extern lock_table_entry_type   spartan6_lock_table[64];
extern filter_table_entry_type spartan6_filter_table[64][2];

extern drp_description_type CLK0_Delay_Time[];
extern drp_description_type CLK0_Edge[];
extern drp_description_type CLK0_High_Time[];
extern drp_description_type CLK0_Low_Time[];
extern drp_description_type CLK0_No_Count[];
extern drp_description_type CLK0_Phase_MUX[];
extern drp_description_type CLK1_Delay_Time[];
extern drp_description_type CLK1_Edge[];
extern drp_description_type CLK1_High_Time[];
extern drp_description_type CLK1_Low_Time[];
extern drp_description_type CLK1_No_Count[];
extern drp_description_type CLK1_Phase_MUX[];
extern drp_description_type CLK2_Delay_Time[];
extern drp_description_type CLK2_Edge[];
extern drp_description_type CLK2_High_Time[];
extern drp_description_type CLK2_Low_Time[];
extern drp_description_type CLK2_No_Count[];
extern drp_description_type CLK2_Phase_MUX[];
extern drp_description_type CLK3_Delay_Time[];
extern drp_description_type CLK3_Edge[];
extern drp_description_type CLK3_High_Time[];
extern drp_description_type CLK3_Low_Time[];
extern drp_description_type CLK3_No_Count[];
extern drp_description_type CLK3_Phase_MUX[];
extern drp_description_type CLK4_Delay_Time[];
extern drp_description_type CLK4_Edge[];
extern drp_description_type CLK4_High_Time[];
extern drp_description_type CLK4_Low_Time[];
extern drp_description_type CLK4_No_Count[];
extern drp_description_type CLK4_Phase_MUX[];
extern drp_description_type CLK5_Delay_Time[];
extern drp_description_type CLK5_Edge[];
extern drp_description_type CLK5_High_Time[];
extern drp_description_type CLK5_Low_Time[];
extern drp_description_type CLK5_No_Count[];
extern drp_description_type CLK5_Phase_MUX[];
extern drp_description_type CLKFB_Delay_Time[];
extern drp_description_type CLKFB_Edge[];
extern drp_description_type CLKFB_High_Time[];
extern drp_description_type CLKFB_Low_Time[];
extern drp_description_type CLKFB_No_Count[];
extern drp_description_type CLKFB_Phase_MUX[];
extern drp_description_type DIVCLK_Edge[];
extern drp_description_type DIVCLK_High_Time[];
extern drp_description_type DIVCLK_Low_Time[];
extern drp_description_type DIVCLK_No_Count[];
/* extern drp_description_type FILTER_TABLE[]; */
/* extern drp_description_type LKTABLE[]; */
extern drp_description_type FILTER_TABLE_RES[];
extern drp_description_type FILTER_TABLE_LFHF[];
extern drp_description_type FILTER_TABLE_CP[];
extern drp_description_type LKTABLE_LOCK_REF_DLY[];
extern drp_description_type LKTABLE_LOCK_FB_DLY[];
extern drp_description_type LKTABLE_LOCK_CNT[];
extern drp_description_type LKTABLE_LOCK_SAT_HIGH[];
extern drp_description_type LKTABLE_UNLOCK_CNT[];

/******************************************************************************/

#endif /* __DRP_PLL_PARAM_H__ */

/******************************************************************************/