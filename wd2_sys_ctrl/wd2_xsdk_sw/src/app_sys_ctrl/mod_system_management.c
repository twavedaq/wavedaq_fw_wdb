/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  22.05.2014 13:30:33
 *
 *  Description :  Module for configuring doing system management and housekeeping.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#include "system.h"
#include "gpio_slow_control.h"
#include "udp_terminal.h"
#include "xfs_printf.h"
#include "fw_env.h"
#include "utilities.h"
#include "register_map_wd2.h"
#include "dbg.h"
#include "cmd_processor.h"
#include <stdlib.h>



/************************************************************/

  int sm_reset(int argc, char **argv)
  {
    unsigned int data = WD2_RECONFIGURE_FPGA_MASK;
    unsigned char c;

    CMD_HELP("",
             "starts an fpga reconfiguration",
             "Reconfigures the FPGA from the flash memory.\r\n"
            );

    /* Acknowledge SPI command */
    if(get_cmd_sender() == XFS_PRINTF_TARGET_SPI)
    {
      c = 0x03;
      sspi_transmit(SYSPTR(spi_syslink_slave), &c, 1);
    }

    msleep(100);
    reg_bank_set(WD2_REG_RST, &data, 1);

    return 0;
  }

/************************************************************/

  int sm_mark_board(int argc, char **argv)
  {
    CMD_HELP("",
             "mark board by letting led blink magenta"
            );

    gpio_sc_set_sw_status(GPIO_LED_SW_STATUS_MARKER);

    return 0;
  }

/************************************************************/

  int sm_unmark_board(int argc, char **argv)
  {
    CMD_HELP("",
             "remove board marking board resetting the led state"
            );

    gpio_sc_clr_sw_status(GPIO_LED_SW_STATUS_MARKER);

    return 0;
  }

/************************************************************/

  int sm_init(int argc, char **argv)
  {
    CMD_HELP("<snr>",
             "initializes environment variables",
             "Initializes board environment variables (hostname, mac address, ...)\r\n"
             "with the serial number.\r\n"
             "  <snr> : Serial number (digits only)\r\n"
            );

    /* Check for minimum number of arguments */
    if(argc < 2)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }

    /* set envirnment variables and control registers */
    init_settings(xfs_atoi(argv[1]));

    return 0;
  }

/************************************************************/

  int sm_cfg_data_dst(int argc, char **argv)
  {
    unsigned char mac[6];
    unsigned char ip[6];
    unsigned int header_num = 0;
    udp_gen_header_entry_type udp_gen_entry;
    int target_num, current_target = -1;

    CMD_HELP("<port> [<ip_addr> <mac_addr>]",
             "configure data destination settings",
             "Configurest the destination port, ip and address\r\n"
             "for event data.\r\n"
             "If the ip and mac addresse are not specified they\r\n"
             "are obtained from the latest command packet sent via UDP.\r\n"
             "  <port>     : destination port\r\n"
             "  <ip_addr>  : destination ip address\r\n"
             "  <mac_addr> : destination mac address\r\n"
            );

    if(argc < 2)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }
    else
    {
      if(argc == 2)
      {
        /* search for latest communication target */
        for (target_num=0; target_num < NUM_UDP_TERMINAL_CMD_PORTS; target_num++)
        {
          if ( udp_terminal_targets[target_num].valid && udp_terminal_targets[target_num].current_target )
          {
            current_target = target_num;
          }
        }

        if(current_target >= 0)
        {
          SYSPTR(nw_data_target)->dst_port = strtol(argv[1], NULL, 0);
          ncpy(SYSPTR(nw_data_target)->dst_mac_addr, udp_terminal_targets[current_target].dst_mac, 6);
          ncpy(SYSPTR(nw_data_target)->dst_ip_addr,  udp_terminal_targets[current_target].dst_ip,  4);
        }
        else
        {
          xfs_printf("E%02X: Invalid arguments. Type \"cfgdst help\" for help.\r\n", ERR_INVALID_ARGS);
          return 0;
        }
      }
      else if(argc == 3)
      {
        xfs_printf("E%02X: Invalid arguments. Type \"cfgdst help\" for help.\r\n", ERR_INVALID_ARGS);
        return 0;
      }
      else if(argc > 3)
      {
        if( !parse_ip((const char*)argv[2], ip) && !parse_mac((const char*)argv[3], mac) )
        {
          SYSPTR(nw_data_target)->dst_port = strtol(argv[1], NULL, 0);
          ncpy(SYSPTR(nw_data_target)->dst_mac_addr, mac, 6);
          ncpy(SYSPTR(nw_data_target)->dst_ip_addr,  ip,  4);
        }
        else
        {
          xfs_printf("E%02X: Invalid arguments. Type \"cfgdst help\" for help.\r\n", ERR_INVALID_ARGS);
          return 0;
        }
      }

      if(DBG_INFO)
      {
        xfs_printf("\r\nNew data destination:\r\n");
        xfs_printf("PORT: %d\r\n", SYSPTR(nw_data_target)->dst_port);
        xfs_printf("MAC:  %02X:%02X:%02X:%02X:%02X:%02X\r\n", SYSPTR(nw_data_target)->dst_mac_addr[0], SYSPTR(nw_data_target)->dst_mac_addr[1], SYSPTR(nw_data_target)->dst_mac_addr[2], SYSPTR(nw_data_target)->dst_mac_addr[3], SYSPTR(nw_data_target)->dst_mac_addr[4], SYSPTR(nw_data_target)->dst_mac_addr[5]);
        xfs_printf("IP:   %d.%d.%d.%d\r\n", SYSPTR(nw_data_target)->dst_ip_addr[0], SYSPTR(nw_data_target)->dst_ip_addr[1], SYSPTR(nw_data_target)->dst_ip_addr[2], SYSPTR(nw_data_target)->dst_ip_addr[3]);
      }
      /* configure UDP default header for packager */
      if(DBG_INFO) xfs_printf("Updating UDP default header...\r\n");
      udpgen_create_header_entry(SYSPTR(udp_pack_gen), &udp_gen_entry,
                                 SYSPTR(nw_if_gmac_0)->mac_addr,         SYSPTR(nw_if_gmac_0)->ip_addr,         SYSPTR(cfg)->nw_data_src_port,
                                 SYSPTR(nw_data_target)->dst_mac_addr, SYSPTR(nw_data_target)->dst_ip_addr, SYSPTR(nw_data_target)->dst_port);

      udpgen_header_to_plb_bram(SYSPTR(udp_pack_gen), &udp_gen_entry, header_num);
        /* enable data transmission */
      if(DBG_INFO) xfs_printf("Enabling data transmission...\r\n");
      gpio_sc_set_eth_dst_valid();
      if(DBG_INFO) xfs_printf("Done\r\n\r\n");
    }

    return 1;
  }

/************************************************************/

  int sm_get_data_dst(int argc, char **argv)
  {
    CMD_HELP("",
             "Report current data destination parameters",
             "Reports the WDB destination port, ip and address\r\n"
             "for event data.\r\n"
            );

    xfs_printf("\r\nCurrent data destination:\r\n");
    xfs_printf("PORT: %d\r\n", SYSPTR(nw_data_target)->dst_port);
    xfs_printf("MAC:  %02X:%02X:%02X:%02X:%02X:%02X\r\n", SYSPTR(nw_data_target)->dst_mac_addr[0], SYSPTR(nw_data_target)->dst_mac_addr[1], SYSPTR(nw_data_target)->dst_mac_addr[2], SYSPTR(nw_data_target)->dst_mac_addr[3], SYSPTR(nw_data_target)->dst_mac_addr[4], SYSPTR(nw_data_target)->dst_mac_addr[5]);
    xfs_printf("IP:   %d.%d.%d.%d\r\n", SYSPTR(nw_data_target)->dst_ip_addr[0], SYSPTR(nw_data_target)->dst_ip_addr[1], SYSPTR(nw_data_target)->dst_ip_addr[2], SYSPTR(nw_data_target)->dst_ip_addr[3]);

    return 1;
  }

/************************************************************/

  int sm_data_tx_on_off(int argc, char **argv)
  {
    CMD_HELP("on | off",
             "enable or disable event data transmission",
             "Description: Enables or disables event data transmission.\r\n"
             "  on | off:   state state of event data transmission.\r\n"
            );

    /* Check for minimum number of arguments */
    if(argc < 2)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }

    if(fstrcmp(argv[1],"on"))
    {
      gpio_sc_set_eth_dst_valid();
    }
    else if(fstrcmp(argv[1],"off"))
    {
      gpio_sc_disable_event_data_transmission();
    }
    else
    {
      xfs_printf("E%02X: Invalid arguments. Type \"calbuf help\" for help.\r\n", ERR_INVALID_ARGS);
      return 0;
    }

    return 1;
  }

/************************************************************/

  int sm_set_cal_buf(int argc, char **argv)
  {
    unsigned int data = WD2_CALIB_BUFFER_EN_MASK;

    CMD_HELP("on | off",
             "enables or disables the calibration buffers",
             "Enables or disables the calibration buffers.\r\n"
             " on | off:   state to set calibration buffers to.\r\n"
            );


    /* Check for minimum number of arguments */
    if(argc < 2)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }

    if(fstrcmp(argv[1],"on"))
    {
      reg_bank_set(WD2_REG_CAL_CTRL, &data, 1);
    }
    else if(fstrcmp(argv[1],"off"))
    {
      reg_bank_clr(WD2_REG_CAL_CTRL, &data, 1);
    }
    else
    {
      xfs_printf("E%02X: Invalid arguments. Type \"calbuf help\" for help.\r\n", ERR_INVALID_ARGS);
      return 0;
    }

    return 0;
  }

/************************************************************/

  int sm_set_cal_clk(int argc, char **argv)
  {
    unsigned int destination;

    CMD_HELP("<clock_source> <drs_channel>",
             "sets the calibration clock source",
             "Applies one of two LMK clocks to the specified DRS chip\r\n"
             "             calibration port.\r\n"
             "  <clock_source> : source of the calibration clock (tca or lmk).\r\n"
             "  <drs_channel>  : DRS chip (a or b) the clock is applied to.\r\n"
            );

    /* Check for minimum number of arguments */
    if(argc < 3)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }

    /* Set destination DRS chip */
    if(fstrcmp(argv[2],"a"))
    {
      destination = WD2_DRS_0_TIMING_REF_SEL_MASK;
    }
    else if(fstrcmp(argv[2],"b"))
    {
      destination = WD2_DRS_1_TIMING_REF_SEL_MASK;
    }
    else
    {
      xfs_printf("E%02X: Invalid arguments. Type \"calbuf help\" for help.\r\n", ERR_INVALID_ARGS);
      return 0;
    }

    /* Set calibration clock source */
    if(fstrcmp(argv[1],"tca"))
    {
      reg_bank_clr(WD2_REG_CAL_CTRL, &destination, 1);
    }
    else if(fstrcmp(argv[1],"lmk"))
    {
      reg_bank_set(WD2_REG_CAL_CTRL, &destination, 1);
    }
    else
    {
      xfs_printf("E%02X: Invalid arguments. Type \"calbuf help\" for help.\r\n", ERR_INVALID_ARGS);
      return 0;
    }

    return 0;
  }

/************************************************************/

  int sm_set_cal_osc(int argc, char **argv)
  {
    unsigned int data = WD2_TIMING_CALIB_SIGNAL_EN_MASK;

    CMD_HELP("on | off",
             "enables or disables the calibration oscillator",
             "Enables or disables the calibration oscillator and\r\n"
             "the corresponding buffer.\r\n"
             " on | off:  state to set to the calibration oscillator\r\n"
             "            and its buffer.\r\n"
            );

    /* Check for minimum number of arguments */
    if(argc < 2)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }

    if(fstrcmp(argv[1],"on"))
    {
      reg_bank_set(WD2_REG_CAL_CTRL, &data, 1);
    }
    else if(fstrcmp(argv[1],"off"))
    {
      reg_bank_clr(WD2_REG_CAL_CTRL, &data, 1);
    }
    else
    {
      xfs_printf("E%02X: Invalid arguments. Type \"calosc help\" for help.\r\n", ERR_INVALID_ARGS);
      return 0;
    }

    return 0;
  }

/************************************************************/

  int sm_set_pwr_cmp(int argc, char **argv)
  {
    unsigned int data = WD2_COMP_POWER_EN_MASK;

    CMD_HELP("on | off",
             "enables or disables the power of the trigger comparators",
             "Enables or disables the power of the drs trigger comparator.\r\n"
             "  on | off:   state of the drs trigger comparator power\r\n"
            );

    /* Check for minimum number of arguments */
    if(argc < 2)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }

    if(fstrcmp(argv[1],"on"))
    {
      reg_bank_set(WD2_REG_CTRL, &data, 1);
    }
    else if(fstrcmp(argv[1],"off"))
    {
      reg_bank_clr(WD2_REG_CTRL, &data, 1);
    }
    else
    {
      xfs_printf("E%02X: Invalid arguments. Type \"pwrcmp help\" for help.\r\n", ERR_INVALID_ARGS);
      return 0;
    }

    return 0;
  }

/************************************************************/

  int sm_set_clk_ext(int argc, char **argv)
  {
    unsigned int reg_val;

    CMD_HELP("on | off",
             "enables or disables the differential TR_CLK",
             "Enables or disables the differential TR_CLK.\r\n"
             " on | off:   state of the clock enable signal.\r\n"
            );

    /* Check for minimum number of arguments */
    if(argc < 2)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }

    reg_val = WD2_EXT_CLK_IN_SEL_MASK;
    if(fstrcmp(argv[1],"on"))
    {
      reg_bank_set(WD2_REG_CLK_CTRL, &reg_val, 1);
    }
    else if(fstrcmp(argv[1],"off"))
    {
      reg_bank_clr(WD2_REG_CLK_CTRL, &reg_val, 1);
    }
    else
    {
      xfs_printf("E%02X: Invalid arguments. Type \"clkext help\" for help.\r\n", ERR_INVALID_ARGS);
      return 0;
    }

    return 0;
  }

/************************************************************/

  int sm_get_version(int argc, char **argv)
  {
    CMD_HELP("",
             "returns the version indicator values",
             "Returns the HW version information (VERS_IND).\r\n"
            );

    xfs_printf("\r\nCurrently not available.\r\nPins used for UART\r\n\r\n");

    return 0;
  }

/************************************************************/

  void print_scaler_value(double sc_val, const char *name)
  {
    if (sc_val < 1E3)
    {
      xfs_printf("%3d     Hz ", (int)sc_val);
    }
    else if (sc_val < 1E6)
    {
      sc_val = sc_val/1E3 + 0.0005;
      xfs_printf("%3d.%03d kHz", (int)sc_val, (int)((sc_val-((int)sc_val))*1000));
    }
    else
    {
      sc_val = sc_val/1E6 + 0.0005;
      xfs_printf("%3d.%03d MHz", (int)sc_val, (int)((sc_val-((int)sc_val))*1000));
    }
    xfs_printf(" %s\r\n", name);
  }

/************************************************************/

  int sm_get_scaler(int argc, char **argv)
  {
    unsigned char ch;
    char ch_name[6];
    unsigned int data;

    CMD_HELP("[<channel>]",
             "read scaler values",
             "reads scaler ch specified. Omitting the channel\r\n"
             "parameter is equivalent to channel option \"all\".\r\n"
             "  <channel> : scaler channel to read (0-15, ptrntrg, exttrg, dclk, all)\r\n"
            );

    if(argc == 1)
    {
      ch = 0xFF;
    }
    else
    {
      if( fstrcmp(argv[1],"all") )
      {
        ch = 0xFF;
      }
      else if( fstrcmp(argv[1],"ptrntrg") )
      {
        ch = 16;
      }
      else if( fstrcmp(argv[1],"exttrg") )
      {
        ch = 17;
      }
      else if( fstrcmp(argv[1],"dclk") )
      {
        ch = 18;
      }
      else
      {
        ch = strtol(argv[1], NULL, 0);
        if(ch > 18) ch = 0xFF;
      }
    }

    if(ch < 16)
    {
      reg_bank_read(WD2_REG_SCALER_0+ch*4, &data, 1);
      xfs_snprintf(ch_name, 6, "ch %d", ch);
      print_scaler_value(data, ch_name);
    }
    else if(ch == 16)
    {
      reg_bank_read(WD2_REG_SCALER_PTRN_TRG, &data, 1);
      print_scaler_value(data, "pattern trigger \0");
    }
    else if(ch == 17)
    {
      reg_bank_read(WD2_REG_SCALER_EXT_TRG, &data, 1);
      print_scaler_value(data, "external trigger\0");
    }
    else if(ch == 18)
    {
      reg_bank_read(WD2_REG_SCALER_EXT_CLK, &data, 1);
      print_scaler_value(data, "daq ref clock\0");
    }
    else if(ch == 0xFF)
    {
      for(int i=0;i<16;i++)
      {
        reg_bank_read(WD2_REG_SCALER_0+i*4, &data, 1);
        xfs_snprintf(ch_name, 6, "ch %d", i);
        print_scaler_value(data, ch_name);
      }
      reg_bank_read(WD2_REG_SCALER_PTRN_TRG, &data, 1);
      print_scaler_value(data, "pattern trigger \0");
      reg_bank_read(WD2_REG_SCALER_EXT_TRG, &data, 1);
      print_scaler_value(data, "external trigger\0");
      reg_bank_read(WD2_REG_SCALER_EXT_CLK, &data, 1);
      print_scaler_value(data, "daq ref clock\0");
    }

    xfs_printf("\r\n");

    return 0;
  }

/************************************************************/

  int module_sm_help(int argc, char **argv)
  {
    CMD_HELP("",
             "System Management Module",
             "Commands for system mamangement functions"
            );

    return 0;
  }

/************************************************************/
/* COMMAND TABLE                                            */
/************************************************************/

  cmd_table_entry_type system_management_cmd_table[] =
  {
    {0, "sm", module_sm_help},
    {0, "reset", sm_reset},
    {0, "mark", sm_mark_board},
    {0, "unmark", sm_unmark_board},
    {0, "init", sm_init},
    {0, "cfgdst", sm_cfg_data_dst},
    {0, "getdst", sm_get_data_dst},
    {0, "datatx", sm_data_tx_on_off},
    {0, "scaler", sm_get_scaler},
    {0, "calbuf", sm_set_cal_buf},
    {0, "calclk", sm_set_cal_clk},
    {0, "calosc", sm_set_cal_osc},
    {0, "pwrcmp", sm_set_pwr_cmp},
    {0, "clkext", sm_set_clk_ext},
    {0, "version", sm_get_version},
    {0, NULL, NULL}
  };

/************************************************************/
