/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  23.05.2014 13:14:46
 *
 *  Description :  Credits of the DRS4 board. Maybe incomplete. Just fun.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#include "wd2_config.h"
#include "xfs_printf.h"
#include "utilities.h"
#include "cmd_processor.h"
#include <stdlib.h>

/************************************************************/

int credits_stefan(int argc, char **argv)
{
  xfs_printf("\r\n...did a great job inventing me\r\n   and designing my chips together with roberto.\r\n\r\n");
  return 0;
}

/************************************************************/

int credits_ueli(int argc, char **argv)
{
  xfs_printf("\r\n...did a great job designing me...\r\n...even though suffering from the mentor tools.\r\n\r\n");
  return 0;
}

/************************************************************/

int credits_gerd(int argc, char **argv)
{
  xfs_printf("\r\n...hopefully does a great job\r\n   writing firmware for me.\r\n\r\n");
  return 0;
}

/************************************************************/

int credits_elmar(int argc, char **argv)
{
  xfs_printf("\r\n...hopefully does a great job\r\n   writing firmware for me.\r\n\r\n");
  return 0;
}

/************************************************************/

int module_credits_help(int argc, char **argv)
{
CMD_HELP("","show credits");
return 0;

}


/************************************************************/
/* COMMAND TABLE                                            */
/************************************************************/

  cmd_table_entry_type credits_cmd_table[] =
  {
    {0, "credits", module_credits_help},
    {0, "stefan", credits_stefan},
    {0, "ueli", credits_ueli},
    {0, "gerd", credits_gerd},
    {0, "elmar", credits_elmar},
    {0, 0, 0}
  };

/************************************************************/
