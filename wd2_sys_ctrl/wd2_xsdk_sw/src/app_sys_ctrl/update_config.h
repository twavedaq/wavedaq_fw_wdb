/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  19.09.2014 13:45:08
 *
 *  Description :  Updating external devices via serial protocols if corresponding
 *                 register bank settings have changed.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#ifndef __UPDATE_CONFIG_H__
#define __UPDATE_CONFIG_H__

/***************************** Include Files *******************************/

/************************** Function Prototypes ****************************/

/* Update Configuration functions */
int auto_update_configurations();
int trigger_update_configurations();
int update_adc_rst();
int update_daq_clk_cal();
int update_clocks(unsigned int mod_reg);
int update_drs(unsigned int mod_reg);
int update_max_pkg_samples();
int update_dac(unsigned int mod_reg1, unsigned int mod_reg2);
int update_frontend();
int update_drs_sampling_freq();
int update_adc_sampling_freq();
int update_lmk_ch(unsigned int mod_reg);
int update_lmk();
int update_hv(unsigned int mod_reg_hv_u_target_0_7, unsigned int mod_reg_hv_u_target_8_15);
int update_temperature();
int update_count_rate();

#endif /** __UPDATE_CONFIG_H__ */
