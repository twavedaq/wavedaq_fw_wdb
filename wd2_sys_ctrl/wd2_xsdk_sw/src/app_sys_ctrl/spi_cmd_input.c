/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e, theidel
 *  Created :  17.09.2015 15:01:58
 *
 *  Description :  Capturing commands from backplane SPI link.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

/******************************************************************************/
/* include files                                                              */
/******************************************************************************/

#include "spi_cmd_input.h"
#include "xfs_printf.h"
#include "system.h"
#include "plb_spi_slave.h"
#include "cmd_processor.h"
#include "gpio_slow_control.h"

/* #include "xparameters.h" */
/* #include <stdlib.h>      */
/* #include "utilities.h"   */

/******************************************************************************/

void spi_cmd_input_init(spi_cmd_input_type* self)
{
  /* self->esc = 0; */
  self->prompt = 0;
  self->bptr = 0;
}

/******************************************************************************/

int spi_cmd_input_get_cmd(spi_cmd_input_type* self)
{
  int len = -1;
  unsigned char c;

  if (self->prompt)
  {
    c = 0x03;
    sspi_transmit(SYSPTR(spi_syslink_slave), &c, 1);
    self->prompt=0;
  }


  while (!sspi_rx_fifo_is_empty(SYSPTR(spi_syslink_slave)))
  {
    sspi_receive(SYSPTR(spi_syslink_slave), &c, 1, 10);
    /* xfs_terminal_printf("0x%02X\r\n",c); */
    if ((c==0x0a) || (c==0x0d))
    {
      self->buffer[self->bptr]=0;
      xfs_terminal_printf("\r\n>>spi>> %s\r\n", self->buffer);
      len = self->bptr;
      self->bptr = 0;
      break;
    }
    else if  ((c >= 32) && (c <= 126))
    {
      if (self->bptr < SPI_CMD_RECV_BUFF_LEN)
      {
        /* xfs_terminal_printf("%c",c); */
        self->buffer[self->bptr++]=c;
      }
    }
  }

  if (len >= 0) self->prompt=1;
  return len;
}

/******************************************************************************/

void spi_cmd_input_chk_cmd(spi_cmd_input_type* self)
{
  int len;

  len = spi_cmd_input_get_cmd(self);
  if (len>0)
  {
     sspi_reset_txfifo(SYSPTR(spi_syslink_slave));
     cmd_process(self->buffer, len);
  }
}

/******************************************************************************/
/******************************************************************************/
