/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  04.09.2014 16:06:54
 *
 *  Description :  Module for SPI access to AD5590 and LTC2601 on HV board.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#include "hv_board_com.h"
#include "xfs_printf.h"
#include "utilities.h"
#include "dbg.h"
#include "cmd_processor.h"
#include <stdlib.h>
#include <stdio.h>


/************************************************************/

int mod_hv_channel_switch(int argc, char **argv)
{
  unsigned char channel;
  unsigned char flag;

  CMD_HELP("<channel> <flag>",
           "configure CPC7514 switches on the HV board",
           "Switches the HV on or off for the specified channel\r\n" 
           "  <channel> : channel switch (range 0 to 15).\r\n"
           "  <flag>    : 1 switch on, 0 switch off\r\n"
          );


  /* Check for minimum number of arguments */
  if(argc < 3)
  {
    xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
    return 0;
  }

  channel = (unsigned char)(xfs_atoi(argv[1]));
  flag    = (unsigned char)(xfs_atoi(argv[2]));
  /* Validate argument range */
  hv_channel_switch(channel, flag);
  xfs_printf("\r\nHV on channel %d switched ", channel);
  if(flag)
  {
    xfs_printf("on.\r\n");
  }
  else
  {
    xfs_printf("off.\r\n");
  }

  return 0;
}

/************************************************************/

int mod_hv_dac_set(int argc, char **argv)
{
  unsigned int channel;
  float voltage;

  CMD_HELP("<channel> <voltage>",
           "configure and enable one of the DACs on the HV board",
           "Sets the specified voltage to the corresponding DAC channel\r\n" 
           "and enables (power up) it.\r\n"
           "  <channel> : dac channel to set (range 0 to 15).\r\n"
           "  <voltage> : voltage to set in V\r\n"
          );


  /* Check for minimum number of arguments */
  if(argc < 3)
  {
    xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
    return 0;
  }

  channel = xfs_atoi(argv[1]);
  voltage = atof(argv[2]);
  /* Validate argument range */
  if(!hv_dac_set(channel, voltage))
  {
    xfs_printf("E%02X: Invalid arguments. Type \"hvdacset help\" for help.\r\n", ERR_INVALID_ARGS);
    return 0;
  }


  return 0;
}

/************************************************************/

int mod_hv_current_get(int argc, char **argv)
{
  unsigned char channel;
  int gain_index = 0;
  float current;

  CMD_HELP("<channel> <gain_index>",
           "read hv current from the adc on the HV board",
           "reads the ADC on the HV board and calculates\r\n" 
           "the corresponding hv current.\r\n"
           "  <channel>    : adc channel to set (range 0 to 15).\r\n"
           "  <gain_index> : optional index of the ADC gain (range 0 to 7)\r\n"
           "                 corresponding to gain 1, 4, 8, 16, 32, 64, 128, 256.\r\n"
          );

  /* Check for minimum number of arguments */
  if(argc < 2)
  {
    xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
    return 0;
  }

  channel = (unsigned char)(xfs_atoi(argv[1]));
  if(argc >= 3)
  {
    gain_index = xfs_atoi(argv[2]);
  }
  current = hv_test_current_get(channel, gain_index);

  printf("\r\nCurrent of channel %d: %1.3f uA\r\n\r\n", channel, current * 1E6);

  return 0;
}


/************************************************************/

int mod_hv_power_on(int argc, char **argv)
{
  unsigned char flag;
  CMD_HELP("<flag>",
           "enable the DC/DC converter connecte to the HV board",
           "enables or disables the Cokroft Walton generator.\r\n" 
           "  <flag> : 1 enable CW, 0 disable CW.\r\n"
          );


  /* Check for minimum number of arguments */
  if(argc < 2)
  {
    xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
    return 0;
  }

  flag = (unsigned char)(xfs_atoi(argv[1]));
  hv_power_on(flag);
  if( flag > 0 )
  {
    xfs_printf("\r\nCokroft Walton enabled\r\n\r\n");
  }
  else
  {
    xfs_printf("\r\nCokroft Walton disabled\r\n\r\n");
  }

  return 0;
}

/************************************************************/

int mod_hv_is_plugged(int argc, char **argv)
{
  CMD_HELP("",
           "check if HV  board is plugged into the WD2 board",
           "checks if the HV board is plugged into the WD2 board.\r\n" 
          );

  if( hv_is_plugged() )
  {
    xfs_printf("\r\nHV board plugged\r\n\r\n");
  }
  else
  {
    xfs_printf("\r\nHV board NOT plugged\r\n\r\n");
  }

  return 0;
}

/************************************************************/

int mod_hv_power_version(int argc, char **argv)
{
  CMD_HELP("",
           "check HV power board version",
           "checks the HV power board version\r\n" 
          );


  xfs_printf("\r\nHV power board version = V%d\r\n\r\n", hv_power_version()+1);
  
  return 0;
}

/************************************************************/

int mod_hv_power_is_plugged(int argc, char **argv)
{
  CMD_HELP("",
           "check if HV power board is plugged into the HV board",
           "hecks if the HV power board is plugged into the HV board.\r\n" 
          );

  if( hv_power_is_plugged() )
  {
    xfs_printf("\r\nHV power board plugged\r\n\r\n");
  }
  else
  {
    xfs_printf("\r\nHV power board NOT plugged\r\n\r\n");
  }

  return 0;
}

/************************************************************/

int mod_hv_base_set(int argc, char **argv)
{
  float voltage;

  CMD_HELP("<voltage>",
           "configure the DAC on the DC/DC converter board",
           "Sets the specified (scaled) voltage to the HV base DAC\r\n" 
           " <voltage> : voltage to set in V\r\n"
          );


  /* Check for minimum number of arguments */
  if(argc < 2)
  {
    xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
    return 0;
  }

  voltage = atof(argv[1]);
  /* Validate argument range */
  hv_base_set(voltage);
  printf("\r\nHV base voltage set to %1.3f V.\r\n", voltage);

  return 0;
}

/************************************************************/

int mod_hv_base_get(int argc, char **argv)
{
  float voltage;
  CMD_HELP("",
           "read the high voltage of the HV DC/DC converter",
           "eads and scales the base voltage at the ADC on the HV board.\r\n" 
           "Note: Two reads have to be issued to get the current value.\r\n" 
          );

  while (!hv_base_get(&voltage));
  printf("\r\nHV base voltage read %1.3f V.\r\n", voltage);

  return 0;
}


/************************************************************/

int mod_hv_eeprom_read(int argc, char **argv)
{
  unsigned int address;
  int no_bytes;
  unsigned char buffer[256];

  CMD_HELP("<address> <no_bytes>",
           "read from HV board eeprom",
           "Reads the specified number of bytes from the eeprom\r\n"
           "on the HV board, starting at the address specified.\r\n"
           "  <address>  : offset to start from.\r\n"
           "  <no_bytes> : number of bytes to read\r\n"
          );


  /* Check for minimum number of arguments */
  if(argc < 3)
  {
    xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
    return 0;
  }

  address  = xfs_atoi(argv[1]);
  no_bytes = xfs_atoi(argv[2]);
  /* Validate argument range */
  if(no_bytes > 256)
  {
    xfs_printf("Note: max. number of bytes to read is 256.\r\n");
    no_bytes = 256;
  }
  hv_eeprom_read(address, buffer, no_bytes);
  xfs_printf("\r\n[address]: value\r\n");
  for(int i=0;i<no_bytes;i++)
  {
    xfs_printf("[0x%02X]: 0x%02X\r\n", (0xFF&(address+i)), buffer[i]);
  }
  xfs_printf("\r\n");

  return 0;
}

/************************************************************/

int mod_hv_eeprom_write(int argc, char **argv)
{
  unsigned int address;
  int no_bytes;
  unsigned char buffer[256];

  CMD_HELP("<address> <no_bytes> <data>",
           "write to HV board eeprom",
           "Writes the specified number of bytes (max 256) to the eeprom \r\n"
           "on the HV board, starting at the address specified.\r\n"
           "  <address>  : offset to start from.\r\n"
           "  <no_bytes> : number of bytes to write\r\n"
           "  <data>     : data represented by one big hex string\r\n"
          );

  /* Check for minimum number of arguments */
  if(argc < 4)
  {
    xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
    return 0;
  }

  address  = xfs_atoi(argv[1]);
  no_bytes = xfs_atoi(argv[2]);
  /* Validate argument range */
  if(no_bytes > 256)
  {
    xfs_printf("Note: max. number of bytes to read is 256.\r\n");
    no_bytes = 256;
  }
  if((int)strlen(argv[3]) < 2*no_bytes)
  {
    xfs_printf("Number of bytes does not match the data specified.\r\n");
    return 0;
  }

  for(int i=no_bytes-1;i>=0;i--)
  {
    buffer[i] = (unsigned char)(hatoi((const char*)(&argv[3][2*i])));
    argv[3][2*i] = '\0';
  }

  hv_eeprom_write(address, buffer, no_bytes);
  xfs_printf("\r\n%d bytes written to eeprom starting at address 0x%02X\r\n\r\n", no_bytes, address);

  return 0;
}



/************************************************************/

int mod_hv_led(int argc, char **argv)
{
  unsigned char flag;
  CMD_HELP("<flag>",
           "enable or disable HV board led",
           "enables or disables the HV board LED." 
           "  <flag> :  1 enable LED, 0 disable LED.\r\n" 
          );

    
  /* Check for minimum number of arguments */
  if(argc < 2)
  {
    xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
    return 0;
  }

  flag = (unsigned char)(xfs_atoi(argv[1]));
  hv_led(flag);
  if( flag > 0 )
  {
    xfs_printf("\r\nHV LED enabled\r\n\r\n");
  }
  else
  {
    xfs_printf("\r\nHV LED disabled\r\n\r\n");
  }

  return 0;
}

/************************************************************/

int mod_hv_digi_therm_set(int argc, char **argv)
{
  unsigned char value;
  CMD_HELP("<value>",
           "sets output value of digi therm GPIO",
           "sets the output value of the digi therm GPIO" 
           "  <value> : output value (0 or 1).\r\n" 
          );

  /*Check for minimum number of arguments */
  if(argc < 2)
  {
    xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
    return 0;
  }

  value = (unsigned char)(xfs_atoi(argv[1]));
  hv_digi_therm_set(value);
  xfs_printf("\r\nDigi Therm output set to ");
  if( value )
  {
    xfs_printf("1\r\n\r\n");
  }
  else
  {
    xfs_printf("0\r\n\r\n");
  }

  return 0;
}

/************************************************************/

int mod_hv_digi_therm_io(int argc, char **argv)
{
  unsigned char direction;
  CMD_HELP("<direction>",
           "sets io direction of digi therm GPIO",
           "sets the io direction of the digi therm GPIO.\r\n" 
           "  <direction> : 0 = output, 1 = input.\r\n"
          );


  /* Check for minimum number of arguments */
  if(argc < 2)
  {
    xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
    return 0;
  }

  direction = (unsigned char)(xfs_atoi(argv[1]));
  hv_digi_therm_setio(direction);
  xfs_printf("\r\nDigi Therm io direction set to ");
  if( direction )
  {
    xfs_printf("input\r\n\r\n");
  }
  else
  {
    xfs_printf("output\r\n\r\n");
  }

  return 0;
}

/************************************************************/

int mod_hv_digi_therm_get(int argc, char **argv)
{
  CMD_HELP("",
           "reads input value of digi therm GPIO",
           "read the input value of the digi therm GPIO.\r\n" 
          );

  xfs_printf("\r\nDigi Therm input is %d\r\n\r\n", hv_digi_therm_get());

  return 0;
}

/************************************************************/

int module_hv_help(int argc, char **argv)
{
  CMD_HELP("",
           "HV board Module",
           "Configuration of all devices on the HV board."
          );

  return 0;
}

/************************************************************/
/* COMMAND TABLE                                            */
/************************************************************/

cmd_table_entry_type hv_dac_adc_cmd_table[] =
{
  {0, "hv",         module_hv_help},
  {0, "hvchsw",     mod_hv_channel_switch},
  {0, "hvdacset",   mod_hv_dac_set},
  {0, "hvcget",     mod_hv_current_get},
  {0, "hvpwron",    mod_hv_power_on},
  {0, "hvplgd",     mod_hv_is_plugged},
  {0, "hvpwrvers",  mod_hv_power_version},
  {0, "hvpwrplgd",  mod_hv_power_is_plugged},
  {0, "hvbsset",    mod_hv_base_set},
  {0, "hvbsget",    mod_hv_base_get},
  {0, "hveepromrd", mod_hv_eeprom_read},
  {0, "hveepromwr", mod_hv_eeprom_write},
  {0, "hvled",      mod_hv_led},
  {0, "hvdtset",    mod_hv_digi_therm_set},
  {0, "hvdtio",     mod_hv_digi_therm_io},
  {0, "hvdtget",    mod_hv_digi_therm_get},
  {0, NULL, NULL}
};

/************************************************************/
