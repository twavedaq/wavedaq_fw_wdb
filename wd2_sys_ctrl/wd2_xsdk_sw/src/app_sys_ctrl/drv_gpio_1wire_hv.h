/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  27.10.2015 13:44:44
 *
 *  Description :  GPIO software interface to control 1Wire interface on HV board.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#ifndef __DRV_GPIO_1WIRE_HV_H__
#define __DRV_GPIO_1WIRE_HV_H__


#define  DATA_1WIRE_DIR_OUT   0
#define  DATA_1WIRE_DIR_IN    1

/* port 7 IOs: HV_1WIRE_IO, HV_1WIRE_DIR_O */

/* GPIO 1WIRE IOs */
#define  HV_1WIRE_DIR_O      0x0001
#define  HV_1WIRE_IO         0x0002


/************************************************************/

/* DS28EA00 functions */
unsigned int hv_ds28ea00_get_input();
void hv_ds28ea00_set_output(unsigned char value);
void hv_ds28ea00_set_tristate(unsigned char value);
void hv_ds28ea00_init();

#endif /* __DRV_GPIO_1WIRE_HV_H__ */
