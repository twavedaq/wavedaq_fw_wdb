/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  07.05.2014 08:55:59
 *
 *  Description :  Module for SPI access to LTC2600 temperature sensor.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#include "system.h"
#include "drv_soft_spi_temp_dac_lmk.h"
#include "register_map_wd2.h"
#include "xfs_printf.h"
#include "utilities.h"
#include "dbg.h"
#include "cmd_processor.h"
#include <stdlib.h>


/************************************************************/

  unsigned int get_reg_info(char *adr, unsigned int &reg_offset, unsigned int &bit_mask, unsigned int &bit_offset)
  {
    if( (fstrcmp(adr,"0a")) || (fstrcmp(adr,"rofs")) )           {reg_offset = WD2_REG_DAC0_A_B; bit_mask = WD2_DAC0_CH_A_MASK; bit_offset = WD2_DAC0_CH_A_OFS;}
    else if( (fstrcmp(adr,"0b")) || (fstrcmp(adr,"ofs")) )       {reg_offset = WD2_REG_DAC0_A_B; bit_mask = WD2_DAC0_CH_B_MASK; bit_offset = WD2_DAC0_CH_B_OFS;}
    else if( (fstrcmp(adr,"0c")) || (fstrcmp(adr,"caldc")) )     {reg_offset = WD2_REG_DAC0_C_D; bit_mask = WD2_DAC0_CH_C_MASK; bit_offset = WD2_DAC0_CH_C_OFS;}
    else if( (fstrcmp(adr,"0d")) || (fstrcmp(adr,"pulseamp")) )  {reg_offset = WD2_REG_DAC0_C_D; bit_mask = WD2_DAC0_CH_D_MASK; bit_offset = WD2_DAC0_CH_D_OFS;}
    else if( (fstrcmp(adr,"0e")) || (fstrcmp(adr,"pzclevel")) )  {reg_offset = WD2_REG_DAC0_E_F; bit_mask = WD2_DAC0_CH_E_MASK; bit_offset = WD2_DAC0_CH_E_OFS;}
    else if( (fstrcmp(adr,"0f")) || (fstrcmp(adr,"reserved0")) ) {reg_offset = WD2_REG_DAC0_E_F; bit_mask = WD2_DAC0_CH_F_MASK; bit_offset = WD2_DAC0_CH_F_OFS;}
    else if( (fstrcmp(adr,"0g")) || (fstrcmp(adr,"reserved1")) ) {reg_offset = WD2_REG_DAC0_G_H; bit_mask = WD2_DAC0_CH_G_MASK; bit_offset = WD2_DAC0_CH_G_OFS;}
    else if( (fstrcmp(adr,"0h")) || (fstrcmp(adr,"bias")) )      {reg_offset = WD2_REG_DAC0_G_H; bit_mask = WD2_DAC0_CH_H_MASK; bit_offset = WD2_DAC0_CH_H_OFS;}
    else if( (fstrcmp(adr,"1a")) || (fstrcmp(adr,"tlevel0")) )   {reg_offset = WD2_REG_DAC1_A_B; bit_mask = WD2_DAC1_CH_A_MASK; bit_offset = WD2_DAC1_CH_A_OFS;}
    else if( (fstrcmp(adr,"1b")) || (fstrcmp(adr,"tlevel1")) )   {reg_offset = WD2_REG_DAC1_A_B; bit_mask = WD2_DAC1_CH_B_MASK; bit_offset = WD2_DAC1_CH_B_OFS;}
    else if( (fstrcmp(adr,"1c")) || (fstrcmp(adr,"tlevel2")) )   {reg_offset = WD2_REG_DAC1_C_D; bit_mask = WD2_DAC1_CH_C_MASK; bit_offset = WD2_DAC1_CH_C_OFS;}
    else if( (fstrcmp(adr,"1d")) || (fstrcmp(adr,"tlevel3")) )   {reg_offset = WD2_REG_DAC1_C_D; bit_mask = WD2_DAC1_CH_D_MASK; bit_offset = WD2_DAC1_CH_D_OFS;}
    else if( (fstrcmp(adr,"1e")) || (fstrcmp(adr,"tlevel4")) )   {reg_offset = WD2_REG_DAC1_E_F; bit_mask = WD2_DAC1_CH_E_MASK; bit_offset = WD2_DAC1_CH_E_OFS;}
    else if( (fstrcmp(adr,"1f")) || (fstrcmp(adr,"tlevel5")) )   {reg_offset = WD2_REG_DAC1_E_F; bit_mask = WD2_DAC1_CH_F_MASK; bit_offset = WD2_DAC1_CH_F_OFS;}
    else if( (fstrcmp(adr,"1g")) || (fstrcmp(adr,"tlevel6")) )   {reg_offset = WD2_REG_DAC1_G_H; bit_mask = WD2_DAC1_CH_G_MASK; bit_offset = WD2_DAC1_CH_G_OFS;}
    else if( (fstrcmp(adr,"1h")) || (fstrcmp(adr,"tlevel7")) )   {reg_offset = WD2_REG_DAC1_G_H; bit_mask = WD2_DAC1_CH_H_MASK; bit_offset = WD2_DAC1_CH_H_OFS;}
    else if( (fstrcmp(adr,"2a")) || (fstrcmp(adr,"tlevel8")) )   {reg_offset = WD2_REG_DAC2_A_B; bit_mask = WD2_DAC2_CH_A_MASK; bit_offset = WD2_DAC2_CH_A_OFS;}
    else if( (fstrcmp(adr,"2b")) || (fstrcmp(adr,"tlevel9")) )   {reg_offset = WD2_REG_DAC2_A_B; bit_mask = WD2_DAC2_CH_B_MASK; bit_offset = WD2_DAC2_CH_B_OFS;}
    else if( (fstrcmp(adr,"2c")) || (fstrcmp(adr,"tlevel10")) )  {reg_offset = WD2_REG_DAC2_C_D; bit_mask = WD2_DAC2_CH_C_MASK; bit_offset = WD2_DAC2_CH_C_OFS;}
    else if( (fstrcmp(adr,"2d")) || (fstrcmp(adr,"tlevel11")) )  {reg_offset = WD2_REG_DAC2_C_D; bit_mask = WD2_DAC2_CH_D_MASK; bit_offset = WD2_DAC2_CH_D_OFS;}
    else if( (fstrcmp(adr,"2e")) || (fstrcmp(adr,"tlevel12")) )  {reg_offset = WD2_REG_DAC2_E_F; bit_mask = WD2_DAC2_CH_E_MASK; bit_offset = WD2_DAC2_CH_E_OFS;}
    else if( (fstrcmp(adr,"2f")) || (fstrcmp(adr,"tlevel13")) )  {reg_offset = WD2_REG_DAC2_E_F; bit_mask = WD2_DAC2_CH_F_MASK; bit_offset = WD2_DAC2_CH_F_OFS;}
    else if( (fstrcmp(adr,"2g")) || (fstrcmp(adr,"tlevel14")) )  {reg_offset = WD2_REG_DAC2_G_H; bit_mask = WD2_DAC2_CH_G_MASK; bit_offset = WD2_DAC2_CH_G_OFS;}
    else if( (fstrcmp(adr,"2h")) || (fstrcmp(adr,"tlevel15")) )  {reg_offset = WD2_REG_DAC2_G_H; bit_mask = WD2_DAC2_CH_H_MASK; bit_offset = WD2_DAC2_CH_H_OFS;}
    else return 0;
      
    return 1;
  }


/************************************************************/

  int dac_configure_and_enable(int argc, char **argv)
  {
    unsigned int reg_offset;
    unsigned int bit_offset;
    unsigned int bit_mask;
    unsigned int voltage;
    unsigned int reg_content;
    char channel_str[10];
    char *channel_ptr;
    int nr_of_channels;

    CMD_HELP("<dac_channel> <voltage>",
             "set voltage to a DAC channel and enable it",
             "Sets the specified voltage to the corresponding DAC channel\r\n"  
             "and enables (power up) it.\r\n"  
             "  <dac_channel> : dac channel to set 0a-0h,1a-1h,2a-2h.\r\n"
             "                  Alternatively rofs, ofs, caldc, pulseamp, pzclevel,\r\n"
             "                  reserved0, reserved1, tlevel0-tlevel15, tlevelall\r\n"
             "                  or bias can be used to specify the channel.\r\n"
             "  <voltage>     : voltage to set in mV\r\n"
            );
    /* Check for minimum number of arguments */
    if(argc < 3)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }

    if(fstrcmp(argv[1],"tlevelall"))
    {
      channel_ptr = channel_str;
      nr_of_channels = 16;
    }
    else
    {
      channel_ptr = argv[1];
      nr_of_channels = 1;
    }
    
    for(int i=0;i<nr_of_channels;i++)
    {
      xfs_snprintf(channel_str, 10, "tlevel%d", i);
      get_reg_info(channel_ptr, reg_offset, bit_mask, bit_offset);
      voltage = xfs_atoi(argv[2]);

      if(reg_offset == 0xFFFFFFFF)
      {
        xfs_printf("E%02X: Invalid arguments. Type \"daccfg help\" for help.\r\n", ERR_INVALID_ARGS);
        return 0;
      }
      else
      {
        if( voltage > LTC2600_VREF_MV )
        {
          voltage = LTC2600_VREF_MV;
        }
  
        voltage = ltc2600_bin_voltage(voltage*1e6);
  
        /* read */
        reg_content = reg_bank_get(reg_offset);
        /* modify */
        reg_content = (reg_content & ~bit_mask) | (voltage << bit_offset);
        /* write */
        reg_bank_write(reg_offset, &reg_content, 1);
      }
    }

    return 0;
  }

/************************************************************/

  int dac_configure(int argc, char **argv)
  {
    unsigned int adr;
    unsigned int bit_mask, bit_offset;
    unsigned int voltage;

    CMD_HELP("<dac_channel> <voltage>",
             "set voltage to a DAC disabled channel",
             "Sets the specified voltage to the corresponding DAC channel\r\n"  
             "leaving it disabled (power down).\r\n"  
             "  <dac_channel> : dac channel to configure a0-h0,a1-h1,a2-h2.\r\n"
             "                  Alternatively rofs, ofs, caldc, pulseamp, pczlevel,\r\n"
             "                  reserved0, reserved1, tlevel0-tlevel5 or bias can be\r\n"
             "                  used to specify the channel.\r\n"
             "  <voltage>     : voltage to set in mV\r\n"
            );


    /* Check for minimum number of arguments */
    if(argc < 3)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }

    get_reg_info(argv[1], adr, bit_mask, bit_offset);
    voltage = (unsigned int)(xfs_atoi(argv[2]));
    /* Validate argument range */
    if( adr > 23 )
    {
      xfs_printf("E%02X: Invalid arguments. Type \"daccfg help\" for help.\r\n", ERR_INVALID_ARGS);
      return 0;
    }
    /* Limit the voltage value to Vref */
    if( voltage > LTC2600_VREF_MV )
    {
      voltage = LTC2600_VREF_MV;
    }

    ltc2600_set(adr, LTC2600_CMD_WR_N, voltage*1e6);

    return 0;
  }

/************************************************************/

  int dac_enable(int argc, char **argv)
  {
    unsigned int adr;
    unsigned int bit_mask, bit_offset;

    CMD_HELP("<dac_channel>",
             "enable a dac channel",
             "Enables the corresponding DAC channel (power up).\r\n"  
             "  <dac_channel> : dac channel to enable a0-h0,a1-h1,a2-h2.\r\n"
             "                  Alternatively rofs, ofs, caldc, pulseamp, pczlevel,\r\n"
             "                  reserved0, reserved1, tlevel0-tlevel5 or bias can be\r\n"
             "                  used to specify the channel.\r\n"
            );

    /* Check for minimum number of arguments */
    if(argc < 2)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }

    get_reg_info(argv[1], adr, bit_mask, bit_offset);
    /* Validate argument range */
    if( adr > 23 )
    {
      xfs_printf("E%02X: Invalid arguments. Type \"daccfg help\" for help.\r\n", ERR_INVALID_ARGS);
      return 0;
    }
    ltc2600_set(adr, LTC2600_CMD_PU_N, 0);

    return 0;
  }

/************************************************************/

  int dac_disable(int argc, char **argv)
  {
    unsigned int adr;
    unsigned int bit_mask, bit_offset;

    CMD_HELP("<dac_channel>",
             "disable a dac channel",
             "Disables the corresponding DAC channel (power down)"  
             "  <dac_channel> : dac channel to disable (a,b,c,d,e,f,g,h,all)\r\n"
             "                  Alternatively rofs,ofs,caldc,tlevel0,tlevel1,tlevel2\r\n"
             "                  tlevel3 or bias can be used to specify the channel.\r\n"
            );

    /* Check for minimum number of arguments */
    if(argc < 2)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }

    get_reg_info(argv[1], adr, bit_mask, bit_offset);
    /* Validate argument range */
    if( (adr > 7) && (adr != 0x0F) )
    {
      xfs_printf("E%02X: Invalid arguments. Type \"daccfg help\" for help.\r\n", ERR_INVALID_ARGS);
      return 0;
    }
    ltc2600_set(adr, LTC2600_CMD_PD_N, 0);

    return 0;
  }


/************************************************************/

  int module_dac_help(int argc, char **argv)
  {
    CMD_HELP("",
             "SPI DAC module",
             "Allows access to LTC2600 DAC via SPI"  
            );

    return 0;
  }

/************************************************************/
/* COMMAND TABLE                                            */
/************************************************************/

  cmd_table_entry_type dac_cmd_table[] =
  {
    {0, "dac", module_dac_help},
    {0, "dacset", dac_configure_and_enable},
    /* Nur dacset macht die Operation via Register */
    /*{0, "daccfg", dac_configure}, */
    /*{0, "dacen", dac_enable}, */
    /*{0, "dacde", dac_disable}, */
    {0, NULL, NULL}
  };

/************************************************************/
