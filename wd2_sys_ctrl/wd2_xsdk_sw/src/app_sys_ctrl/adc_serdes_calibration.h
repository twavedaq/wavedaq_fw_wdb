/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e, theidel
 *  Created :  22.09.2014 16:32:17
 *
 *  Description :  ADCs iserdes calibration functions.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#ifndef __ADC_SERDES_CALIBRATION_H__
#define __ADC_SERDES_CALIBRATION_H__

/***************************** Include Files *******************************/


/************************** Constant Definitions ***************************/
#define NO_ADCS              2   /* ADCs */
#define NO_ADC_CH            8   /* Channels per ADC */
#define NO_STEPS           160   /* Absolute maximum of PLL phase steps */
#define NO_WORDS_PER_RUN    10   /* Words per run */

#define MIN_EYE_WIDTH       10

#define PLL_BITCLK_CHANNEL   (DRP_CHANNEL_CLK0)
#define PLL_DIVCLK_CHANNEL   (DRP_CHANNEL_CLK1)

#define ADC_CAL_MODE_CHECK       0
#define ADC_CAL_MODE_CALIBRATE   1

#define VALUES_PER_PHASE_STEP   50

/**************************** Type Definitions *****************************/
typedef struct
{
  unsigned char eye_center;
  unsigned char eye_width;
} adc_eye_cal_type;

/***************** Macros (Inline Functions) Definitions *******************/


/************************** Function Prototypes ****************************/

int ads_find_eye(unsigned int *pattern, unsigned int *adc_ph_step, unsigned int *adc_width, unsigned int max_phase_steps, unsigned int current_step, unsigned char mode, unsigned char print_details);
int ads_find_eye_all_adcs(unsigned int *pattern, unsigned int *adc_ph_step, unsigned int *adc_width, unsigned int max_phase_steps, unsigned int current_step, unsigned char mode, unsigned char print_details);
void ads_timing_cal(adc_eye_cal_type *adc_cal, unsigned char mode, unsigned char print_details);

#endif /* __ADC_SERDES_CALIBRATION_H__ */
