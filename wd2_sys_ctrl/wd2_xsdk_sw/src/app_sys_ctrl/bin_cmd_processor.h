/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e, theidel
 *  Created :  30.07.2014 13:32:26
 *
 *  Description :  Processing the binary commands.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#ifndef __BIN_CMD_PROCESSOR_H__
#define __BIN_CMD_PROCESSOR_H__

/******************************************************************************/
/* include files                                                              */
/******************************************************************************/

#include "network_if.h"

/******************************************************************************/
/* definitions                                                                */
/******************************************************************************/

#define UDP_BINARY_CMD_PORT   4000

/* word indices */
#define INDEX_CMD           0
#define INDEX_ACK           1
#define INDEX_SEQ_NR        2
#define INDEX_ADR           4
#define INDEX_NR_OF_BYTES   8
#define INDEX_WRITE_DATA    8
#define INDEX_READ_DATA     4

/* bit masks and offsets */
#define BINCMD_CMD_MASK       0xFF000000
#define BINCMD_CMD_OFS                24
#define BINCMD_ACK_MASK       0x00FF0000
#define BINCMD_ACK_OFS                16
#define BINCMD_SEQ_NR_MASK    0x0000FFFF
#define BINCMD_SEQ_NR_OFS              0

/* commands */
#define BINCMD_WRITE      0x14
#define BINCMD_READ       0x24

/* acknowledge */
#define BINCMD_ACK        0x01

/******************************************************************************/
/* global variables                                                           */
/******************************************************************************/


/******************************************************************************/
/* function prototypes                                                        */
/******************************************************************************/

int wd2_udp_bincmd_handle_request(network_if_type *nw_if, unsigned char *frame, int frame_len);
int bin_cmd_process(char *buffer_i, int len, int max_payload_len);

/******************************************************************************/

#endif /* __CMD_PROCESSOR_H__ */
