

#ifndef __UDP_PACK_GEN_H__
#define __UDP_PACK_GEN_H__

/******************************************************************************/
/* Include Files                                                              */
/******************************************************************************/
#include "ether_com.h"
#include "network_if.h"

/******************************************************************************/


#define UPKG_UDP_LISTEN_PORT 3333
#define UPKG_UDP_SEND_PORT   5555
#define UPKG_UDP_SRC_PORT    3000
#define UPKG_UDP_DST_PORT    2000


#define PAR_FRAME_COUNT     0
#define PAR_DATA_LEN        1
#define PAR_DELAY           2
#define PAR_NUMBER_OF_PORTS 3
#define PAR_FRAMES_PER_PORT 4
#define PAR_FIRST_DST_PORT  5
#define PAR_IFG             6
#define PAR_TYPE            7
#define PAR_COUNT           8    /* number of parameters */


#define DEF_FRAME_COUNT     1000
#define DEF_DATA_LEN        1024
#define DEF_DELAY              0
#define DEF_NUMBER_OF_PORTS    1
#define DEF_FRAMES_PER_PORT   64
#define DEF_FIRST_DST_PORT  2000
#define DEF_IFG               16
#define DEF_TYPE               8

/* UDP Header with fill bytes */
typedef struct
{
  /* ethternet frame  (14 byte) */
  unsigned char dst_mac[6];
  unsigned char src_mac[6];
  unsigned char len_type[2];

  /* ip header    (20 byte) */
  unsigned char ver_headerlen[1];
  unsigned char service_type[1];
  unsigned char total_length[2];
  unsigned char identification[2];
  unsigned char flags[1];
  unsigned char frag_offset[1];
  unsigned char time_to_live[1];
  unsigned char protocol[1];
  unsigned char ip_header_checksum[2];
  unsigned char src_ip[4];
  unsigned char dst_ip[4];

  /* udp header   (8 byte) */
  unsigned char src_port[2];
  unsigned char dst_port[2];
  unsigned char udp_message_len[2];
  unsigned char udp_checksum[2];

  /* dummy fill bytes to align to 32 bit entries */
  unsigned char dummy[2];
} udp_gen_header_entry_type;

typedef struct
{
  unsigned char  dummy_alignment[2];
  /* xfs header */
  unsigned char  xfs_id[4];
  unsigned short xfs_prot_ver;
  unsigned int   frame_nr;
  unsigned short xfs_command;
  unsigned short xfs_len;
  unsigned int   xfs_addr;

  /* first data entry */
  unsigned int   data_0;
 /* more xfs data follow */

} xfs_pg_header_type;




typedef struct
{
  unsigned char  dummy_alignment[2];
  /* upkg header */
  unsigned char xfs_id[4];
  unsigned short xfs_len;
  unsigned int frame_nr;
  /*  unsigned int some_info; */
  /* first data entry */
} upkg_header_type;


typedef struct
{
    unsigned int plb_pram_address;
} udp_pack_gen_type;

void udp_pack_gen_construct(udp_pack_gen_type *self, unsigned int plb_pram_address_i);
/* void send_gen(udp_pack_gen_type *self, unsigned int frame_count); */
/* void stop(udp_pack_gen_type *self); */
void udpgen_init_default_header(udp_pack_gen_type *self, network_if_type *nw_if, unsigned int src_port, network_target_type *target);
void udpgen_header_to_plb_bram(udp_pack_gen_type *self, udp_gen_header_entry_type *udp_gen_entry, unsigned int header_num);
void udpgen_adjust_header_entry(udp_pack_gen_type *self, udp_gen_header_entry_type *ip);
void udpgen_create_header_entry(udp_pack_gen_type *self, udp_gen_header_entry_type *udp_gen_entry,
                                const unsigned char *src_mac,
                                const unsigned char *src_ip,
                                unsigned int  src_port,
                                const unsigned char *dst_mac,
                                const unsigned char *dst_ip,
                                unsigned int  dst_port);

#endif /* __UDP_PACK_GEN_H__ */

