/******************************************************************************/
/*                                                                            */
/*  file: ether_com.c                                                         */
/*                                                                            */
/*  (c) 2008 PSI tg14                                                         */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/* Include Files                                                              */
/******************************************************************************/

/*#include "xil_ll_fifo.h" */
#include "ether_com.h"
#include "utilities.h"
#include "xfs_printf.h"



/******************************************************************************/
/* constant definitions                                                       */
/******************************************************************************/

const unsigned char ar_op_request[2] = { 0x00, 0x01 };
const unsigned char ar_op_reply[2]   = { 0x00, 0x02 };

const unsigned char ether_typ_arp[2] = { 0x08, 0x06 };
const unsigned char ether_typ_ip[2]  = { 0x08, 0x00 };




/******************************************************************************/
/* function  definitions                                                      */
/******************************************************************************/

void adjust_icmp_checksum(unsigned char* fp)
{
  unsigned int i,icmp_checksum;
  unsigned char *cptr;
  unsigned int clen;

  icmp_header_type *icmp;

  icmp = (icmp_header_type *)fp;

  /* calc ip checksum */
  cptr = (unsigned char *) icmp->icmp_type;

  clen = ((icmp->total_length[0]<<8) + icmp->total_length[1]);
  if (clen & 0x01)
  {
    fp[clen] = 0;
    clen += 1;
  }
  clen = (clen - 20)/2;

  icmp->icmp_chksum[0] = 0;
  icmp->icmp_chksum[1] = 0;
  icmp_checksum = 0;

  for (i=0; i<clen; i++)
  {
    icmp_checksum += ( (cptr[2*i] << 8)  + (cptr[2*i + 1]) );
    if (icmp_checksum & 0x00010000) icmp_checksum = (icmp_checksum + 1) & 0x0000ffff;
  }
  icmp_checksum = (~icmp_checksum) & 0x0000ffff;
/*  xfs_printf("\r\nIP Checksum: %04x\r\n",ip_checksum); */
  icmp->icmp_chksum[0] = (icmp_checksum >> 8) & 0xff ;
  icmp->icmp_chksum[1] = icmp_checksum & 0xff ;
}


/******************************************************************************/

void adjust_ip_checksum(udp_header_type* ip)
{
  unsigned int i,ip_checksum;
  unsigned char *cptr;

  /* calc ip checksum */
  cptr = (unsigned char *) ip->ver_headerlen;

  ip->ip_header_checksum[0] = 0;
  ip->ip_header_checksum[1] = 0;
  ip_checksum = 0;
  for (i=0; i<10; i++)
  {
    ip_checksum += ( (cptr[2*i] << 8)  + (cptr[2*i + 1]) );
    if (ip_checksum & 0x00010000) ip_checksum = (ip_checksum + 1) & 0x0000ffff;
  }
  ip_checksum = (~ip_checksum) & 0x0000ffff;
/*  xfs_printf("\r\nIP Checksum: %04x\r\n",ip_checksum); */
  ip->ip_header_checksum[0] = (ip_checksum >> 8) & 0xff ;
  ip->ip_header_checksum[1] = ip_checksum & 0xff ;
}


/******************************************************************************/

void adjust_header(udp_header_type *fp, int frame_len)
{
  unsigned int udp_len, ip_packet_length;

  /* ip packet length */
  ip_packet_length = frame_len - ETHERNET_HEADER_LEN; /* without ethernet header */
  fp->total_length[0] = (ip_packet_length >> 8) & 0xff ;
  fp->total_length[1] = ip_packet_length & 0xff ;

  /* calc ip checksum */
  adjust_ip_checksum(fp);

  /* udp packet length */
  udp_len = frame_len - IP_HEADER_LEN;  /* without ethernet and ip header */
  fp->udp_message_len[0] = (udp_len >> 8) & 0xff ;
  fp->udp_message_len[1] = udp_len & 0xff ;

  /* no udp checksum */
  fp->udp_checksum[0] = 0;
  fp->udp_checksum[1] = 0;

}


/******************************************************************************/

void adjust_ip_checksum_bram(udp_header_type *ip)
{
  unsigned int i,ip_checksum;
  unsigned char *cptr;

  cptr = (unsigned char *) ip->ver_headerlen;

  ip->ip_header_checksum[0] = 0;
  ip->ip_header_checksum[1] = 0;
  ip->total_length[0] = 0;
  ip->total_length[1] = 28; /* IP + UDP Header Length */

  /* calc ip checksum */
  ip_checksum = 0;
  for (i=0; i<10; i++)
  {
    ip_checksum += ( (cptr[2*i] << 8)  + (cptr[2*i + 1]) );
    if (ip_checksum & 0x00010000) ip_checksum = (ip_checksum + 1) & 0x0000ffff;
  }

  ip->ip_header_checksum[0] = (ip_checksum >> 8) & 0xff ;
  ip->ip_header_checksum[1] = ip_checksum & 0xff ;
}

/******************************************************************************/
