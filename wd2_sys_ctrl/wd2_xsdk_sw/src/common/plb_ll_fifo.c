/******************************************************************************/
/*                                                                            */
/*  file: plb_ll_fifo.cpp                                                     */
/*                                                                            */
/*  (c) 2010 PSI tg32                                                         */
/*                                                                            */
/******************************************************************************/

#include "plb_ll_fifo.h"
#include "stdio.h"
#include "xfs_printf.h"
#include "xfs_types.h"
/*#define xfs_printf xfs_printf */
#include "utilities.h"
/*#include "crc32.h" */

/* for simulation */
/*#define xfs_printf(...) */



/******************************************************************************/

void plb_ll_fifo_init(plb_ll_fifo_type *self, unsigned int ll_fifo_badr_i)
{
  self->ll_fifo_badr = ll_fifo_badr_i;
  self->buffer_ptr = 0;
  self->send = plb_ll_fifo_send;
  self->receive = plb_ll_fifo_receive;
}

/******************************************************************************/

int plb_ll_fifo_reset_set(plb_ll_fifo_type *self, unsigned int rst_mask)
{
  xfs_u32 ctrl_reg_val;

  ctrl_reg_val = xfs_in32(self->ll_fifo_badr + PLB_LL_FIFO_REG_CTRL);
  ctrl_reg_val |= rst_mask;
  xfs_out32(self->ll_fifo_badr + PLB_LL_FIFO_REG_CTRL, ctrl_reg_val);

  return 0;
}

/******************************************************************************/


int plb_ll_fifo_reset_clr(plb_ll_fifo_type *self, unsigned int rst_mask)
{
  xfs_u32 ctrl_reg_val;

  ctrl_reg_val = xfs_in32(self->ll_fifo_badr + PLB_LL_FIFO_REG_CTRL);
  ctrl_reg_val &= (~rst_mask);
  xfs_out32(self->ll_fifo_badr + PLB_LL_FIFO_REG_CTRL, ctrl_reg_val);

  return 0;
}

/******************************************************************************/

int plb_ll_fifo_reset(plb_ll_fifo_type *self, unsigned int rst_mask)
{
  xfs_u32 ctrl_reg_val;

  ctrl_reg_val = xfs_in32(self->ll_fifo_badr + PLB_LL_FIFO_REG_CTRL);

  ctrl_reg_val |= rst_mask;

  xfs_out32(self->ll_fifo_badr + PLB_LL_FIFO_REG_CTRL, ctrl_reg_val);
  xfs_out32(self->ll_fifo_badr + PLB_LL_FIFO_REG_CTRL, ctrl_reg_val);
  xfs_out32(self->ll_fifo_badr + PLB_LL_FIFO_REG_CTRL, ctrl_reg_val);

  ctrl_reg_val &= (~rst_mask);

  xfs_out32(self->ll_fifo_badr + PLB_LL_FIFO_REG_CTRL, ctrl_reg_val);
  return 0;
}

/******************************************************************************/

int plb_ll_fifo_ctrl_reg_set(plb_ll_fifo_type *self, unsigned int bit_mask)
{
  xfs_u32 ctrl_reg_val;

  ctrl_reg_val = xfs_in32(self->ll_fifo_badr + PLB_LL_FIFO_REG_CTRL);
  ctrl_reg_val |= bit_mask;
  xfs_out32(self->ll_fifo_badr + PLB_LL_FIFO_REG_CTRL, ctrl_reg_val);

  return 0;
}

/******************************************************************************/

int plb_ll_fifo_ctrl_reg_clr(plb_ll_fifo_type *self, unsigned int bit_mask)
{
  xfs_u32 ctrl_reg_val;

  ctrl_reg_val = xfs_in32(self->ll_fifo_badr + PLB_LL_FIFO_REG_CTRL);

  ctrl_reg_val &= (~bit_mask);
  xfs_out32(self->ll_fifo_badr + PLB_LL_FIFO_REG_CTRL, ctrl_reg_val);

  return 0;
}

/******************************************************************************/

unsigned int plb_ll_fifo_ctrl_reg_write_mask(plb_ll_fifo_type *self, unsigned int mask, unsigned int val)
{
  xfs_u32 ctrl_reg_val, ctrl_reg_prev;

  ctrl_reg_val = xfs_in32(self->ll_fifo_badr + PLB_LL_FIFO_REG_CTRL);
  ctrl_reg_prev = ctrl_reg_val;
  ctrl_reg_val &= (~mask);
  ctrl_reg_val |= ( mask & val);
  xfs_out32(self->ll_fifo_badr + PLB_LL_FIFO_REG_CTRL, ctrl_reg_val);

  return ctrl_reg_prev;
}

/******************************************************************************/

unsigned int plb_ll_fifo_get_status_vector(plb_ll_fifo_type *self)
{
  return (xfs_in32(self->ll_fifo_badr + PLB_LL_FIFO_REG_STATUS) & PLB_LL_FIFO_STATUS_VECTOR);
}

/******************************************************************************/

unsigned int plb_ll_fifo_get_status(plb_ll_fifo_type *self)
{
  return (xfs_in32(self->ll_fifo_badr + PLB_LL_FIFO_REG_STATUS));
}

/******************************************************************************/

void plb_ll_fifo_status(plb_ll_fifo_type *self)
{
  xfs_printf("LL_FIFO_STATUS : 0x%08x \r\n",plb_ll_fifo_get_status(self));
}

/******************************************************************************/

void plb_ll_fifo_write_word(plb_ll_fifo_type *self, unsigned int word, unsigned int sof, unsigned int eof, unsigned int reminder)
{
  unsigned int fifo_ctrl;

  fifo_ctrl = sof ? PLB_LL_FIFO_CTRL_LL_SOF : 0;

  if (eof)
  {
	fifo_ctrl |= (PLB_LL_FIFO_CTRL_LL_EOF | ((reminder << PLB_LL_FIFO_CTRL_LL_REM_SHIFT) & PLB_LL_FIFO_CTRL_LL_REM) );
  }
  
  plb_ll_fifo_ctrl_reg_write_mask(self, PLB_LL_FIFO_CTRL_LL_MASK ,fifo_ctrl);

  xfs_out32(self->ll_fifo_badr + PLB_LL_FIFO_REG_FIFO, word);
}

/******************************************************************************/

int plb_ll_fifo_send_part(plb_ll_fifo_type *self, const void *buffer, unsigned int frame_len, unsigned int part_sof, unsigned int part_eof)
{
 /* note: buffer must be word (4 byte) aligned
  * for all parts not including eof the frame_len must be multiple of 4
  * frame_len in byte
  */
  int vacancy;
  int i;
  int words_send = 0;
  int last_word;
  unsigned int *word_ptr;
  unsigned int retries;
  unsigned int sof, eof, reminder;

/*  frame_len = buff_insert_crc32( (unsigned char *)buffer, frame_len); */

  if ( (frame_len < 1) || (frame_len > 3200 ))return -1;

  last_word = (frame_len-1)/4;
  reminder =  (frame_len-1) & 0x03;
  word_ptr = (unsigned int *)buffer;

  while (words_send <= last_word)
  {
    /* wait until almmost_full flag is not set */
    retries = 100;
    do
    {
      vacancy = ((xfs_in32(self->ll_fifo_badr + PLB_LL_FIFO_REG_STATUS) & PLB_LL_FIFO_STATUS_TX_ALMOSTFULL) == 0);
      if (!vacancy)  usleep(5);
    }
    while(!vacancy && --retries);

    if (!vacancy)  return -1;

    for (i=0; ((i<PLB_LL_FIFO_ALMOST_FULL_THRESHOLD_WORDS) && (words_send <= last_word)); i++)
    {
	  sof = part_sof && (words_send == 0);
	  eof = part_eof && (words_send == last_word);
	  
	  plb_ll_fifo_write_word(self, word_ptr[words_send], sof, eof, reminder);
	  words_send++;
    }
  }
  return frame_len;
}


/******************************************************************************/
    
int plb_ll_fifo_send(plb_ll_fifo_type *self, const void *buffer, unsigned int frame_len)
{
  return plb_ll_fifo_send_part(self, buffer, frame_len, 1, 1);	
}


/******************************************************************************/

int plb_ll_fifo_receive(plb_ll_fifo_type *self, void *buffer, unsigned int bufflen)
{
  /* note: buffer must be word (4 byte) aligned */
  /* frame_len in byte                          */
  int len;
  unsigned int *word_ptr;
  unsigned int status;
  volatile unsigned int fifo_val;
  int sof = 0;

  word_ptr = (unsigned int *)buffer;
  do
  {
   status = xfs_in32(self->ll_fifo_badr + PLB_LL_FIFO_REG_STATUS);

   if (!(status & PLB_LL_FIFO_STATUS_RX_EMPTY))
   {
    if (status & PLB_LL_FIFO_STATUS_LL_SOF)
    {
      if (self->buffer_ptr)
      {
        self->buffer_ptr = 0;
/*        xfs_printf(">>>>  buffer overflow: -1\r\n"); */

        return -1; /* buffer overflow */
      }
/*      xfs_printf(">>>>  SOF\r\n"); */
      self->buffer_ptr = 0;
      sof = 1;
    }

     fifo_val = xfs_in32(self->ll_fifo_badr + PLB_LL_FIFO_REG_FIFO);
/*      xfs_printf("Read from FiFo 0x%08x \r\n", fifo_val); */

    if ((self->buffer_ptr > 0) || sof)
    {
      if ( (bufflen >> 2) > self->buffer_ptr)
      {
        word_ptr[self->buffer_ptr++] = fifo_val;

      }
      else
      {
        self->buffer_ptr = 0;
/*        xfs_printf(">>>>  buffer overflow: -2\r\n"); */
        return -2; /* buffer overflow */
      }

      if (status & PLB_LL_FIFO_STATUS_LL_EOF)
      {
        len = (self->buffer_ptr << 2) -3 + ( (status & PLB_LL_FIFO_STATUS_LL_REM)>>PLB_LL_FIFO_STATUS_LL_REM_SHIFT );
/*        xfs_printf(">>>>status=0x%08x  EOF  len = %d \r\n\r\n",status, len); */
           self->buffer_ptr = 0;

          return len;

      }

     }
   }
  }
  while(!(status & PLB_LL_FIFO_STATUS_RX_EMPTY));

  return 0;
}

/******************************************************************************/


#if 0

#define BUFF_LEN 3200
#define BUFF_LEN_WORD ((BUFF_LEN+3)/4)

   unsigned int buffer[BUFF_LEN_WORD];
   unsigned int rec_buffer[BUFF_LEN_WORD];

int plb_ll_fifo_plb_ll_fifo_test (plb_ll_fifo_type *self)
{
  int i;
   unsigned char *char_ptr;
   int len;
   char_ptr = (unsigned char *)buffer;

   reset(PLB_LL_FIFO_CTRL_RESET_STD );

   for(i=0; i < BUFF_LEN; i++) char_ptr[i]=i+1;


   while(1)
   {
     /*  xfs_printf("------------------------------------------\r\n"); */
     for(i=1; i <= BUFF_LEN; i++)
     {
       send(buffer,i);
          usleep(15);
      /*  xfs_printf("PLB_LL_FIFO_REG_STATUS: 0x%08x\r\n", xfs_in32(self->ll_fifo_badr + PLB_LL_FIFO_REG_STATUS)); */
      while(!(xfs_in32(self->ll_fifo_badr + PLB_LL_FIFO_REG_STATUS) & PLB_LL_FIFO_STATUS_EMPTY))
      {
        len = receive(rec_buffer,BUFF_LEN);
       if (len > 0)
       {
         xfs_printf("rec Frame : len=%d\r\n", len);
         /* print_frame((unsigned char*)rec_buffer, len); */
       }
       if (len < 0)
       {
        xfs_printf("rec ERROR : %d\r\n", len);
       }

      }

     }
   }
  return 0;
}

#endif

/******************************************************************************/
/******************************************************************************/
