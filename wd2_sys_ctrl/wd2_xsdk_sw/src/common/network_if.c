/******************************************************************************/
/*                                                                            */
/*  file: network_if.cpp                                                      */
/*                                                                            */
/*  (c) 2008 PSI tg14                                                         */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/* include files                                                              */
/******************************************************************************/

#include "network_if.h"
#include "utilities.h"
#include <string.h>
#include "xfs_printf.h"
#include "ether_com.h"
#include "dhcp.h"

/******************************************************************************/
/* function prototypes                                                        */
/******************************************************************************/


void nw_if_construct(network_if_type *self, const char *if_name, plb_ll_fifo_type *ll_fifo, void *dhcp, gmac_lite_type *gmac_lite, unsigned char *com_buff, unsigned int com_buff_len)
{
  strncpy(self->if_name, if_name, NW_IF_NAME_LENGTH);
  self->ll_fifo = ll_fifo;
  self->ip_address_valid=0;
  self->do_dhcp=0;
  self->dhcp = dhcp;
  self->gmac_lite = gmac_lite;
  self->com_buff = com_buff;
  self->com_buff_len = com_buff_len;
}


/******************************************************************************/

xfs_u32 nw_if_init(network_if_type *self, const unsigned char *mac_addr, const unsigned char *ip_addr, const unsigned int dhcp)
{
  nw_if_set_mac_address(self, mac_addr);

  nw_if_set_ip_address(self, ip_addr);

  return XFS_SUCCESS;
}

/******************************************************************************/

void nw_if_config(network_if_type *self, const unsigned char *mac_addr, const unsigned char *ip_addr, const unsigned int do_dhcp)
{
  nw_if_set_mac_address(self, mac_addr);
  nw_if_set_dhcp(self, do_dhcp);

  if (!do_dhcp)
  {
    nw_if_set_ip_address(self, ip_addr);
  }
}

/******************************************************************************/

unsigned char* nw_if_get_ip_address(network_if_type *self, unsigned char *ip_addr)
{
  ncpy((char *)ip_addr, (char *)self->ip_addr, 4);
  return ip_addr;
};

/******************************************************************************/

void nw_if_set_ip_address(network_if_type *self,const unsigned char *ip_addr)
{
  nw_if_set_dhcp(self, 0);
  ncpy((char *)self->ip_addr, (char *)ip_addr, 4);
  self->ip_address_valid = 1;
};

/******************************************************************************/

unsigned char* nw_if_get_mac_address(network_if_type *self,unsigned char *mac_addr)
{
  ncpy((char *)mac_addr, (char *)self->mac_addr, 6);
  return mac_addr;
};

/******************************************************************************/

void nw_if_set_mac_address(network_if_type *self,const unsigned char *mac_addr)
{
   ncpy((char *)self->mac_addr, (char *)mac_addr, 6);
   gmac_lite_mac_filter_set(self->gmac_lite, mac_addr);
};

/******************************************************************************/
 
void nw_if_set_dhcp(network_if_type *self, int do_dhcp)
{
  dhcp_init((dhcp_type*)self->dhcp);
  if (self->do_dhcp || do_dhcp) self->ip_address_valid = 0;
  self->do_dhcp = do_dhcp;
};

/******************************************************************************/

int nw_if_get_dhcp(network_if_type *self)
{
  return self->do_dhcp;
};


/******************************************************************************/

void nw_if_print_status(network_if_type *self)
{
/*  xfs_printf("Link       : %s\r\n",(xtemac->link_up?"up":"down")); */
/*  xfs_printf("Speed      : %d Mbps\r\n",xtemac->speed);            */
  xfs_printf("\r\nNetwork Interface %s:\r\n", self->if_name);
  xfs_printf("MAC addr   : %02x:%02x:%02x:%02x:%02x:%02x\r\n",self->mac_addr[0],self->mac_addr[1],self->mac_addr[2],self->mac_addr[3],self->mac_addr[4],self->mac_addr[5]);
  xfs_printf("IP addr    : %d.%d.%d.%d  (%svalid)  %s\r\n\r\n",self->ip_addr[0],self->ip_addr[1],self->ip_addr[2],self->ip_addr[3],(self->ip_address_valid?"":"not "),(self->do_dhcp?"DHCP":"static"));
  if(self->do_dhcp) dhcp_print_times((dhcp_type*)self->dhcp);
}

/******************************************************************************/

void nw_if_set_name(network_if_type *self, char *if_name_i)
{
   strncpy(self->if_name, if_name_i, NW_IF_NAME_LENGTH);
};

/**********************************************************************************/

int handle_arp(network_if_type *nw_if, unsigned char* frame, int len)
{
  arp_frame_type *arp_fp;
  arp_fp = (arp_frame_type *)frame;
  if (!fcmp(arp_fp->len_type,ether_typ_arp)) return 0;
  if (!fcmp(arp_fp->ar_pro,  ether_typ_ip))  return 0;
  if (arp_fp->ar_hln[0] != 6) return 0;
  if (arp_fp->ar_pln[0] != 4) return 0;

  if (!fcmp(arp_fp->ar_op,ar_op_request)) return 0;

  if (!fcmp(arp_fp->ar_tpa, nw_if->ip_addr)) return 0;

  /* xfs_printf("------------------- arp --------------\r\n"); */
  /* print_frame(frame, len);                                  */

  ncpy(arp_fp->dst_mac, arp_fp->src_mac, 6);
  ncpy(arp_fp->src_mac, nw_if->mac_addr, 6);

  fcp(arp_fp->ar_op,  ar_op_reply);

  fcp(arp_fp->ar_tha, arp_fp->ar_sha);
  fcp(arp_fp->ar_tpa, arp_fp->ar_spa);
  ncpy(arp_fp->ar_sha, nw_if->mac_addr,6);
  ncpy(arp_fp->ar_spa, nw_if->ip_addr,4);

  nw_if->ll_fifo->send(nw_if->ll_fifo, (unsigned char*)frame, len);

 return 1;
}

/******************************************************************************/

int handle_ping(network_if_type *nw_if, unsigned char* frame, int len)
{
  unsigned char broadcast_mac[] = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
  icmp_header_type *ip_fp;
  unsigned int broadcast_ping = 0;
  unsigned int icmp_checksum;

  ip_fp = (icmp_header_type *)frame;
  
  if ((fcmp(ip_fp->dst_mac, broadcast_mac)) &&  (ip_fp->dst_ip[3] == 0xFF) ) broadcast_ping = 1;

  if ( (!fcmp(ip_fp->dst_mac,nw_if->mac_addr)) && !broadcast_ping ) return 0;
  if ( (!fcmp(ip_fp->dst_ip, nw_if->ip_addr))  && !broadcast_ping )  return 0;
  if (ip_fp->protocol[0] != 0x01) return 0;   /* ICMP protocol */
  if (ip_fp->icmp_type[0] != 0x08) return 0;  /* ping request  */

/*  xfs_printf("------------------- ping --------------\r\n"); */
/*  print_frame(frame, len);                                   */

  fcp(ip_fp->dst_mac ,  ip_fp->src_mac);
  fcp(ip_fp->src_mac ,  nw_if->mac_addr);

  fcp(ip_fp->dst_ip ,  ip_fp->src_ip);
  fcp(ip_fp->src_ip ,  nw_if->ip_addr);

  ip_fp->icmp_type[0] = 0x00;  /* ping reply */

  if (broadcast_ping)
  {
    adjust_ip_checksum((udp_header_type*)frame);
  }

/* adjust_icmp_checksum(frame);             */
/* just add 8, instead of recalculating all */
  icmp_checksum = (ip_fp->icmp_chksum[0] << 8) + (ip_fp->icmp_chksum[1]) + 0x0800;
  if (icmp_checksum & 0x00010000) icmp_checksum = (icmp_checksum + 1) ;
/*  xfs_printf("\r\nIP icmp_checksum: %04x\r\n",icmp_checksum); */

  ip_fp->icmp_chksum[0] = (icmp_checksum >> 8) & 0xff ;
  ip_fp->icmp_chksum[1] = icmp_checksum & 0xff ;

  nw_if->ll_fifo->send(nw_if->ll_fifo, (unsigned char*)frame, len);

 return 1;
}

/******************************************************************************/

int check_udp(network_if_type *nw_if, unsigned char* frame, int len)
{
  udp_header_type *ip_fp;
  int udp_port;
  unsigned int ip_len;
  unsigned int udp_len;
  unsigned int len_type;

  ip_fp = (udp_header_type *)frame;

  if ((unsigned int)len < sizeof(udp_header_type)) return -1;
  
  len_type = (ip_fp->len_type[0] << 8 ) | ip_fp->len_type[1];

  if (len_type != 0x0800) return -1;          /* IPV4 protocol */
  if (ip_fp->protocol[0] != 0x11) return -1;  /* UDP protocol */
  
  udp_len = udp_payload_len_get(ip_fp);
  
  ip_len = (ip_fp->total_length[0] << 8 ) | ip_fp->total_length[1];

   /* xfs_printf("len_type=0x%04x  len=%4d  ip_len=%4d  udp_payload_len=%4d\r\n", len_type, len , ip_len, udp_len); */

  /* length check */
  if ( udp_len != (ip_len - 28)) return -1;
  if ( (ip_len > 50)  && (ip_len != (len - 14))) return -1;

  if (!fcmp(ip_fp->dst_mac, nw_if->mac_addr))  return -1;
  if (!fcmp(ip_fp->dst_ip, nw_if->ip_addr))  return -1;

  udp_port = (ip_fp->dst_port[0] << 8 ) | ip_fp->dst_port[1];

  return udp_port;


}


/******************************************************************************/

void udp_send_frame(network_if_type *nw_if, udp_header_type* frame, int payload_len)
{
  int len;

  len = UDP_HEADER_LEN + payload_len;
  adjust_header(frame, len);

  nw_if->ll_fifo->send(nw_if->ll_fifo, (unsigned char*)frame, len);

}


/******************************************************************************/

void* udp_payload_ptr_get(void* frame)
{
  return (void*) ( ((char*)frame) + sizeof(udp_header_type));
}


/******************************************************************************/

int udp_payload_len_get(void* frame)
{
  udp_header_type *udp_fp = (udp_header_type*)frame;
  return (( (udp_fp->udp_message_len[0] << 8 ) | udp_fp->udp_message_len[1]) - 8 );
}


/******************************************************************************/

void prepare_udp_frame(network_if_type *nw_if, void* frame)
{
  udp_header_type* udp_fp = (udp_header_type*)frame;

  /* ethternet frame  (14 byte) -- do not overwrite */

/*
 *  udp_fp->dst_mac[0] = 0;
 *  udp_fp->dst_mac[1] = 0;
 *  udp_fp->dst_mac[2] = 0;
 *  udp_fp->dst_mac[3] = 0;
 *  udp_fp->dst_mac[4] = 0;
 *  udp_fp->dst_mac[5] = 0;
 */

  fcp(udp_fp->src_mac ,  nw_if->mac_addr);

  udp_fp->len_type[0] = 0x08;
  udp_fp->len_type[1] = 0x00;

  /* ip header    (20 byte) */
  udp_fp->ver_headerlen[0] = 0x45;
  udp_fp->service_type[0] = 0x00;
  udp_fp->total_length[0] = 0;
  udp_fp->total_length[1] = 0;

  udp_fp->identification[0] = 0;
  udp_fp->identification[1] = 0;
  udp_fp->flags[0] = 0x40;
  udp_fp->frag_offset[0] = 0;
  udp_fp->time_to_live[0] = 0xff; /* 0x40; */
  udp_fp->protocol[0] = 0x11;     /* UDP */
  udp_fp->ip_header_checksum[0] = 0;
  udp_fp->ip_header_checksum[1] = 0;

  fcp(udp_fp->src_ip ,  nw_if->ip_addr);
/*
 *  udp_fp->dst_ip[0] = 0;
 *  udp_fp->dst_ip[1] = 0;
 *  udp_fp->dst_ip[2] = 0;
 *  udp_fp->dst_ip[3] = 0;
 */

  /* udp header   (8 byte) */
  udp_fp->src_port[0] = 0;
  udp_fp->src_port[1] = 0;
/*  udp_fp->dst_port[0] = 0; */
/*  udp_fp->dst_port[1] = 0; */
  udp_fp->udp_message_len[0] = 0;
  udp_fp->udp_message_len[1] = 0;
  udp_fp->udp_checksum[0] = 0;
  udp_fp->udp_checksum[1] = 0;

}
