/*******************************************************************************
*
* generic tftp server 
*
*******************************************************************************/

/*
   References:

   RFC 1350    https://tools.ietf.org/html/rfc1350

   optional extension (currently not implemented)
   RFC 2347    https://tools.ietf.org/html/rfc2347
   RFC 2348    https://tools.ietf.org/html/rfc2348



  Overview:


             2 bytes     string    1 byte     string   1 byte
             ------------------------------------------------
  RRQ/WRQ   | Opcode |  Filename  |   0  |    Mode    |   0  |
             ------------------------------------------------
            
            
             2 bytes     2 bytes      n bytes
             ----------------------------------
  DATA      | Opcode |   Block #  |   Data     |
             ----------------------------------
            
            
            2 bytes     2 bytes
             ---------------------
  ACK       | Opcode |   Block #  |
             ---------------------
            
            
             2 bytes     2 bytes      string    1 byte
             -----------------------------------------
  ERROR     | Opcode |  ErrorCode |   ErrMsg   |   0  |
             -----------------------------------------

  
*/

/******************************************************************************/
/* include files                                                              */
/******************************************************************************/

/*#include <stdio.h> */
#include <string.h>
#include "xparameters.h"
#include "ether_com.h"
#include "tftp_server.h"
#include "system.h"
#include "utilities.h"
#include "xfs_printf.h"
#include "dbg.h"

/******************************************************************************/


/* tftp_errorcode error strings */
const char *tftp_errorcode_string[] = 
{
  "not defined",
  "file not found",
  "access violation",
  "disk full",
  "illegal operation",
  "unknown transfer id",
  "file already exists",
  "no such user",
};

/******************************************************************************/
/* function definitions                                                       */
/******************************************************************************/

tftp_opcode tftp_decode_op(unsigned char *ptr)
{
  return (tftp_opcode)(ptr[1]);
}

/******************************************************************************/

unsigned short tftp_extract_block(unsigned char *ptr)
{
  unsigned short *b = (unsigned short *)ptr;
  return ntohs(b[1]);
}

/******************************************************************************/

void tftp_set_opcode(unsigned char *ptr, tftp_opcode op)
{
  /* we assume that op fits in a byte */
  ptr[0] = 0;
  ptr[1] = (unsigned char)op;
}

/******************************************************************************/

void tftp_set_errorcode(unsigned char *ptr, tftp_errorcode err)
{
  /* we assume that err fits in a byte */
  ptr[2] = 0;
  ptr[3] = (unsigned char)err;
}

/******************************************************************************/

void tftp_set_errormsg(unsigned char *ptr, const char *errormsg)
{
  strcpy((char*)ptr + 4, errormsg);
}

/******************************************************************************/

void tftp_set_block(unsigned char *ptr, int block)
{
  unsigned short *p = (unsigned short *)ptr;
  p[1] = htons(block);
}

/******************************************************************************/

int tftp_is_correct_ack(unsigned char *ptr, int block)
{
  /* first make sure this is a data ACK packet */
  if (tftp_decode_op(ptr) != TFTP_ACK) return 0;

  /* then compare block numbers */
  if (block != tftp_extract_block(ptr)) return 0;

  return 1;
}

/******************************************************************************/
/* construct an error message into buf using err as the error code */

int tftp_send_error_str(tftp_server_type *self, tftp_errorcode err, const char* error_string)
{
  if (DBG_INF4) xfs_local_printf("tftp_send_error_str %d %s\r\n", err, error_string);

  /* use rx buffer for answer, so we do not overwrite tx buffer in case of retransmit */
  /* (error could be send to another client while self->busy == 1) */

  /* use source as destination */
  fcp(self->udp_rx_fp->dst_port, self->udp_rx_fp->src_port);
  fcp(self->udp_rx_fp->dst_mac , self->udp_rx_fp->src_mac);
  fcp(self->udp_rx_fp->dst_ip  , self->udp_rx_fp->src_ip);

  prepare_udp_frame(self->nw_if, self->udp_rx_fp);

  tftp_set_opcode(self->udp_rx_data_ptr, TFTP_ERROR);
  tftp_set_errorcode(self->udp_rx_data_ptr, err);
  tftp_set_errormsg(self->udp_rx_data_ptr, error_string);

  udp_send_frame(self->nw_if, self->udp_rx_fp, (4 + strlen(error_string) + 1));

  return 0;
}

/******************************************************************************/
/* construct and send an error message back to client */

int tftp_send_error_message(tftp_server_type *self, tftp_errorcode err)
{
  if (DBG_INF4) xfs_local_printf("tftp_send_error_message %d\r\n", err);
  
  tftp_send_error_str(self, err, tftp_errorcode_string[err]);
  return 0;
}

/******************************************************************************/

void tftp_send_data_packet(tftp_server_type *self)
{
  int len;
  unsigned char *buff;

  if (DBG_INF4) xfs_local_printf("tftp_send_data_packet %d\r\n", self->block);

  buff = self->udp_tx_data_ptr + TFTP_DATA_PACKET_HDR_LEN;
  
  len = self->target->target_handler(self, TFTP_TARGET_HANDLER_CMD_RD, buff, TFTP_DATA_PACKET_MSG_LEN);

  if (len<0)
  {
     tftp_err_cleanup(self, TFTP_TARGET_HANDLER_CMD_RD_CLEANUP);
     return;
  }

  self->fpos += len;
  
  tftp_set_opcode(self->udp_tx_data_ptr, TFTP_DATA);
  tftp_set_block(self->udp_tx_data_ptr, self->block);

  self->retries = TFTP_MAX_RETRIES;
  self->udp_tx_len  = TFTP_DATA_PACKET_HDR_LEN + len;
  self->udp_tx_time = time_tic_read();
  
  udp_send_frame(self->nw_if, self->udp_tx_fp, self->udp_tx_len);

}

/******************************************************************************/

void tftp_send_ack_packet(tftp_server_type *self)
{
  if (DBG_INF4) xfs_local_printf("tftp_send_ack_packet %d\r\n", self->block);

  tftp_set_opcode(self->udp_tx_data_ptr, TFTP_ACK);
  tftp_set_block(self->udp_tx_data_ptr, self->block);

  self->retries = TFTP_MAX_RETRIES;
  self->udp_tx_len  = TFTP_ACK_PACKET_LEN;
  self->udp_tx_time = time_tic_read();
  udp_send_frame(self->nw_if, self->udp_tx_fp, self->udp_tx_len);
}

/******************************************************************************/

int tftp_prepare_read_write(tftp_server_type *self, tftp_opcode op)
{
  char *fname  = (char*) self->udp_rx_data_ptr + 2;
  char *mode   = fname + strlen(fname) + 1;
  int status;
  int i;


  if (DBG_INF4) xfs_local_printf("tftp_prepare_read_write : %s   mode: %s\r\n",fname ,mode);

  for (i=0; self->target_table[i].name; i++)
  {
    if (fstrcmp(fname, self->target_table[i].name))
    {
      self->target = &self->target_table[i];
      break;
    }
  }

  if (!self->target_table[i].name || !self->target_table[i].target_handler)
  {
    if (DBG_WARN) xfs_local_printf("WARNING: tftp_server illegal Location : %s\r\n", fname);
    tftp_send_error_str(self, TFTP_ERR_NOTDEFINED, "Illegal Location");
    return -1;    
  }

  prepare_udp_frame(self->nw_if, self->udp_tx_fp);

  /* set destination */
  fcp(self->udp_tx_fp->dst_port, self->udp_rx_fp->src_port);
  fcp(self->udp_tx_fp->dst_mac , self->udp_rx_fp->src_mac);
  fcp(self->udp_tx_fp->dst_ip  , self->udp_rx_fp->src_ip);

  self->rdy_rsnd_last_ack = 0;
  self->fpos = 0;
  self->busy = 1;


  if (op == TFTP_WRQ )
  {
    if (DBG_INF4) xfs_local_printf("TFTP WRQ (write request): %s\r\n", fname);

    status = self->target->target_handler(self, TFTP_TARGET_HANDLER_CMD_WR_PREP, 0, 0);
    if (status<0)
    {
       tftp_err_cleanup(self, TFTP_TARGET_HANDLER_CMD_WR_CLEANUP);
       return -1;
    }


    self->udp_tx_fp->src_port[0] = (TFTP_WRITE_PORT >> 8) & 0xff;
    self->udp_tx_fp->src_port[1] =  TFTP_WRITE_PORT & 0xff;
    self->block = 0;

    /* initiate the transaction by sending the first ack */
    tftp_send_ack_packet(self);
  }
  else  /* if (op == TFTP_RRQ) */
  {
    if (DBG_INF4) xfs_local_printf("TFTP RRQ (read request): %s\r\n", fname);
    
    status = self->target->target_handler(self, TFTP_TARGET_HANDLER_CMD_RD_PREP, 0, 0);
    if (status<0)
    {
       tftp_err_cleanup(self, TFTP_TARGET_HANDLER_CMD_RD_CLEANUP);
       return -1;
    }

    self->udp_tx_fp->src_port[0] = (TFTP_READ_PORT >> 8) & 0xff;
    self->udp_tx_fp->src_port[1] =  TFTP_READ_PORT & 0xff;
    self->block = 1;

    /* initiate the transaction by sending the first block of data
     * further blocks will be sent when ACKs are received
     */
    tftp_send_data_packet(self);
  }

  return 0;
}

/******************************************************************************/

void tftp_handle_request(tftp_server_type *self)
{
  tftp_opcode op;

  if (self->busy)
  {
     tftp_send_error_str(self, TFTP_ERR_NOTDEFINED, "Server is busy");
     return;
  }

  /* process new session request */
  op = tftp_decode_op(self->udp_rx_data_ptr);

  switch (op)
  {
    case TFTP_RRQ:
    case TFTP_WRQ:
      tftp_prepare_read_write(self, op);
      break;

    default:
      /* send a generic access violation message */
      tftp_send_error_message(self, TFTP_ERR_ACCESS_VIOLATION);
      if (DBG_WARN) xfs_local_printf("TFTP unknown request op: %d\r\n", op);
      break;
  }

}

/******************************************************************************/

int tftp_handle_read(tftp_server_type *self)
{
  int status;
  
  if (DBG_INF4) xfs_local_printf("tftp_handle_read\r\n");

  if (!self->busy) return -1;
  if (!fcmp(self->udp_rx_fp->src_mac,  self->udp_tx_fp->dst_mac )) return -1;
  if (!fcmp(self->udp_rx_fp->src_ip,   self->udp_tx_fp->dst_ip  )) return -1;
  if (!fcmp(self->udp_rx_fp->src_port, self->udp_tx_fp->dst_port)) return -1;

  if (tftp_is_correct_ack(self->udp_rx_data_ptr, self->block))
  {

    if (self->udp_tx_len < TFTP_DATA_PACKET_LEN)
    {
      /* got ack to last packet */
      status = self->target->target_handler(self, TFTP_TARGET_HANDLER_CMD_RD_DONE, 0, 0);
      if (status<0)
      {
         /* tftp_err_cleanup(self, TFTP_TARGET_HANDLER_CMD_RD_CLEANUP); */
         tftp_cleanup(self);
         return -1;
      }

      if (DBG_INF4) xfs_local_printf("tftp read transfer done\r\n");
      tftp_cleanup(self);
      return 0;
    }

    /* increment block # */
    self->block++;
    
    tftp_send_data_packet(self);
  }
  else
  {
    /* we did not receive the expected ACK, so
       do not update block #, thereby resending current block */

    if (self->retries)
    {
      if (DBG_WARN) xfs_local_printf("Warning: tftp_server received incorrect ACK, resending last block\r\n");
      self->retries--;
      self->udp_tx_time = time_tic_read();
      udp_send_frame(self->nw_if, self->udp_tx_fp, self->udp_tx_len);
    }
    else
    {
      tftp_cleanup(self);
      return -1;
    }

  }

  return 0;
}

/******************************************************************************/

int tftp_handle_write(tftp_server_type *self)
{
  int udp_payload_len ;
  int  len;
  unsigned char *buff;
  int status;

  if (DBG_INF4) xfs_local_printf("tftp_handle_write\r\n");
  
  if (!fcmp(self->udp_rx_fp->src_mac,  self->udp_tx_fp->dst_mac )) return -1;
  if (!fcmp(self->udp_rx_fp->src_ip,   self->udp_tx_fp->dst_ip  )) return -1;
  if (!fcmp(self->udp_rx_fp->src_port, self->udp_tx_fp->dst_port)) return -1;

  if (self->rdy_rsnd_last_ack)
  {
    udp_send_frame(self->nw_if, self->udp_tx_fp, self->udp_tx_len);
    return 0;
  }
  if (!self->busy) return -1;
  
  udp_payload_len = udp_payload_len_get(self->udp_rx_fp);

  /* make sure data block is what we expect */
  if ((udp_payload_len >= TFTP_DATA_PACKET_HDR_LEN) && (tftp_extract_block(self->udp_rx_data_ptr) == (self->block + 1)))
  {
    buff = self->udp_rx_data_ptr + TFTP_DATA_PACKET_HDR_LEN;
    len  = udp_payload_len       - TFTP_DATA_PACKET_HDR_LEN;

    /* call handler function to write the data packet */
    status = self->target->target_handler(self, TFTP_TARGET_HANDLER_CMD_WR, buff, len);
    if (status<0)
    {
       tftp_err_cleanup(self, TFTP_TARGET_HANDLER_CMD_WR_CLEANUP);
       return -1;
    }

    self->fpos += len;
    self->block++;

    /* if the data package contains less than the maximum number of data bytes
     * then the whole file is sent
     */

    if (len < TFTP_DATA_PACKET_MSG_LEN)
    {
      if (DBG_INF4) xfs_local_printf("transfer done: total len = %d \r\n",self->fpos);

      status = self->target->target_handler(self, TFTP_TARGET_HANDLER_CMD_WR_LAST, 0, 0);
      if (status<0)
      {
/*         tftp_err_cleanup(self, TFTP_TARGET_HANDLER_CMD_WR_CLEANUP); */
         tftp_cleanup(self);

         return -1;
      }

      if ( !self->target->target_handler(self, TFTP_TARGET_HANDLER_CMD_WR_HDR_CHK, 0, 0) )
      {
        tftp_send_ack_packet(self);
      }
      tftp_cleanup(self);
      self->rdy_rsnd_last_ack = 2 * TFTP_MAX_RETRIES;
      status = self->target->target_handler(self, TFTP_TARGET_HANDLER_CMD_WR_DONE, 0, 0);
    }
    else
    {
      tftp_send_ack_packet(self);
    }
  }
  else  /* send last ack again */
  {
    tftp_send_ack_packet(self);
  }

  return 0;
}

/******************************************************************************/

int handle_tftp(tftp_server_type *self, unsigned char *frame, int frame_len, unsigned int udp_port)
{
  self->udp_rx_fp       = (udp_header_type *) frame;
  self->udp_rx_data_ptr = (unsigned char *) udp_payload_ptr_get(frame);
  self->udp_rx_len      = frame_len;


  if ( udp_port == TFTP_REQUEST_PORT)
  {
    tftp_handle_request(self);
  }
  else if ( udp_port == TFTP_READ_PORT)
  {
    tftp_handle_read(self);
  }
  else if ( udp_port == TFTP_WRITE_PORT)
  {
    tftp_handle_write(self);
  }
  else
  {
    return 0;
  }
  
  return 1;
}

/******************************************************************************/

void tftp_check_timeout(tftp_server_type *self)
{

  if ( msec_since_tic(self->udp_tx_time) > TFTP_TIMEOUT_INTERVAL_MS)
  {
    if (self->busy)
    {
      if (self->retries)
      {
        if (DBG_INF4) xfs_local_printf("tftp timeout, resending package\r\n");
        self->retries--;
        self->udp_tx_time = time_tic_read();
        udp_send_frame(self->nw_if, self->udp_tx_fp, self->udp_tx_len);
      }
      else
      {
        tftp_cleanup(self);
      }
    }
    if (self->rdy_rsnd_last_ack)
    {
       self->rdy_rsnd_last_ack--;
       self->udp_tx_time = time_tic_read();
    }
  }
}

/******************************************************************************/

void tftp_cleanup(tftp_server_type *self)
{
  /* terminate session */  
  if (DBG_INF4) xfs_local_printf("tftp_cleanup \r\n");
  self->busy = 0;
}
/******************************************************************************/

void tftp_err_cleanup(tftp_server_type *self, unsigned int cmd)
{
  /* terminate session */  
  if (DBG_INF4) xfs_local_printf("tftp_err_cleanup \r\n");
  
  self->busy = 0;
}

/******************************************************************************/

void tftp_server_construct(tftp_server_type *self, network_if_type *nw_if_i, tftp_target_type *tftp_targets)
{
  if (DBG_INIT) xfs_local_printf("starting tftp server\r\n");

  self->nw_if = nw_if_i;
  self->udp_tx_fp = (udp_header_type *)self->udp_tx_frame;
  self->udp_tx_data_ptr = (unsigned char *) udp_payload_ptr_get(self->udp_tx_fp);
  self->target_table = tftp_targets;
  self->rdy_rsnd_last_ack = 0;
  self->retries = 0;
  self->block = 0;
  self->busy = 0;
  self->fpos = 0;
  
}

/******************************************************************************/
