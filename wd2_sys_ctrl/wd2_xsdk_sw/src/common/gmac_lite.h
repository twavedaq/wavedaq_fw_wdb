/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  tg32
 *  Created :  08.08.2014
 *
 *  Description :
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#ifndef __GMAC_LITE_H__
#define __GMAC_LITE_H__

/******************************************************************************/
/* include files                                                              */
/******************************************************************************/

/* #include "cmd_table.h" */

/******************************************************************************/
/* constant definitions                                                       */
/******************************************************************************/

#define GMAC_LITE_REG_CTRL             0x00

#define GMAC_LITE_REG_CTRL_SET         0x04  /* reg write */
#define GMAC_LITE_REG_STATUS           0x04  /* reg read  */

#define GMAC_LITE_REG_CTRL_CLR         0x08  /* reg write only */


#define GMAC_LITE_REG_IFG              0x0C 
#define GMAC_LITE_REG_CONFIG           0x10
#define GMAC_LITE_REG_RESERVED         0x14 
#define GMAC_LITE_REG_MAC_FILTER_LOW   0x18 
#define GMAC_LITE_REG_MAC_FILTER_HIGH  0x1C 


#define GMAC_LITE_MAC_FILTER_HIGH_BRDCST_EN   0x80000000
#define GMAC_LITE_MAC_FILTER_HIGH_PROMISC_EN  0x40000000

#define GMAC_LITE_CTRL_RESET             0x00000001
#define GMAC_LITE_CTRL_RESTART_CONFIG    0x00000002

#define GMAC_LITE_STATUS_VECTOR0         0x0000FFFF
#define GMAC_LITE_STATUS_AN_INTERRUPT0   0x00010000
#define GMAC_LITE_STATUS_SIGNAL_DETECT0  0x00020000

#define GMAC_LITE_CONFIG_AN_ADV_VECTOR         0x0000FFFF
#define GMAC_LITE_CONFIG_AN_ADV_VECTOR_SHIFT   0

#define GMAC_LITE_CONFIG_LINK_TIMER            0x01FF0000
#define GMAC_LITE_CONFIG_LINK_TIMER_SHIFT      16

#define GMAC_LITE_CONFIG_VECTOR                0x3E000000
#define GMAC_LITE_CONFIG_VECTOR_SHIFT          25

#define GMAC_LITE_CONFIG_VECTOR_DEFAULT          0x10
#define GMAC_LITE_CONFIG_LINK_TIMER_DEFAULT     0x13d
#define GMAC_LITE_CONFIG_AN_ADV_VECTOR_DEFAULT   0x21

#define GMAC_LITE_CONFIG_DEFAULT ( ((GMAC_LITE_CONFIG_VECTOR_DEFAULT)     << (GMAC_LITE_CONFIG_VECTOR_SHIFT))     \
                                 | ((GMAC_LITE_CONFIG_LINK_TIMER_DEFAULT) << (GMAC_LITE_CONFIG_LINK_TIMER_SHIFT)) \
                                 |  (GMAC_LITE_CONFIG_AN_ADV_VECTOR_DEFAULT))


/* 
 *  reset0                 <= ctrl_reg_w(0);
 *  an_restart_config0     <= ctrl_reg_w(1);
 * 
 *  an_adv_config_vector0 <= "0000000000100001";
 *  an_restart_config0    <= '0';
 *  ------------------------------------------------------------------------------
 *  -- Set the Auto-Negotiation Link Timer duration for the core
 *  ------------------------------------------------------------------------------
 * 
 *  -- The link timer value is here set at 10.39 ms (please refer to the
 *  -- core's User Manual).
 *  link_timer_value0  <= "100111101";
 * 
 *    configuration_vector(1 downto 0) <= (others => '0');   -- Disable Loopback
 *    configuration_vector(2)          <= '0';               -- Disable POWERDOWN
 *    configuration_vector(3)          <= '0';               -- Disable ISOLATE
 *    configuration_vector(4)          <= '0';               -- Enable AN
 * 
 *
 * -- reg 4
 *
 * --  reserved 
 *  an_adv_config_vector0  <= slv_reg3(16 to 31);
 *  link_timer_value0      <= slv_reg3(7  to 15);
 *  configuration_vector0  <= slv_reg3(2  to  6);
 *  
 */

/******************************************************************************/
/* type definitions                                                           */
/******************************************************************************/

typedef struct
{
  unsigned int base_addr;
 /* more to follow... */

} gmac_lite_type;

/******************************************************************************/
/* function prototypes                                                        */
/******************************************************************************/

void gmac_lite_construct(gmac_lite_type *s, unsigned int base_addr);

void gmac_lite_init(gmac_lite_type *s);

void gmac_lite_mac_filter_set(gmac_lite_type *s, const unsigned char *mac_addr);
int gmac_lite_mac_filter_get(gmac_lite_type *s, unsigned char *mac_addr);

void gmac_lite_ifg_set(gmac_lite_type *s, unsigned int ifg);

void gmac_lite_phy_reset(gmac_lite_type *s, unsigned int rst);

unsigned int gmac_lite_status(gmac_lite_type *s);

int gmac_lite_promiscuos_mode(gmac_lite_type *s, int promisc);
int gmac_lite_rcv_broadcast(gmac_lite_type *s, int broadcast);

#endif /* __GMAC_LITE_H__ */
