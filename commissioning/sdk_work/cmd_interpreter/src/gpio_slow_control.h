//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  14.05.2014 13:22:15
//
//  Description :  Software interface for gpio slow control signal inputs and outputs.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#ifndef __GPIO_SLOW_CONTROL_H__
#define __GPIO_SLOW_CONTROL_H__

////// port 3  Inputs: SELECT_I, INIT_I, TRG_BUSY_I, TRG_SYNC_I, TRIGGER_I, trg_fe
//const xfs_u32 GPIO_SLOWCONTROL_INPUTS    = 3;
////// port 4  Outputs: mscb, PWR_CMP_O, TCA_CTRL_O, CAL_CTRL_A_O, CAL_CTRL_B_O,
//////                  BUFFER_CTRL_O, RESET_FPGA_O, LED_R_O, LED_G_O, LED_B_O
//const xfs_u32 GPIO_SLOW_CONTROL_OUTPUTS  = 4;

// GPIO Slow Control Inputs
#define  GPIO_SC_IN_TRG_FE          0x0000FFFF
//#define  GPIO_SC_IN_TRIGGER         0x00010000
//#define  GPIO_SC_IN_TRG_SYNC        0x00020000
//#define  GPIO_SC_IN_TRG_BUSY        0x00040000
#define  GPIO_SC_IN_INIT            0x00010000
#define  GPIO_SC_IN_SELECT          0x00020000
#define  GPIO_SC_VERS_IND           0x00040000

// GPIO Slow Control Outputs
#define  GPIO_SC_OUT_LED_G          0x0001   // Swapped in schematic
#define  GPIO_SC_OUT_LED_B          0x0002   // Swapped in schematic
#define  GPIO_SC_OUT_LED_R          0x0004
#define  GPIO_SC_OUT_RESET_FPGA     0x0008
#define  GPIO_SC_OUT_BUFFER_CTRL    0x0010
#define  GPIO_SC_OUT_CAL_CTRL_B     0x0020
#define  GPIO_SC_OUT_CAL_CTRL_A     0x0040
#define  GPIO_SC_OUT_TCA_CTRL       0x0080
#define  GPIO_SC_OUT_PWR_CMP        0x0100
#define  GPIO_SC_OUT_CLK_SEL_EXT    0x0200
#define  GPIO_SC_OUT_MSCB           0x0400

/************************************************************/

void gpio_sc_init(void);

// Slow control functions
void gpio_sc_set_led(unsigned int val);
void gpio_sc_reset();
void gpio_sc_set_cal_buf(int val);
void gpio_sc_set_cal_clk(int source, int destination);
void gpio_sc_set_cal_osc(int val);
void gpio_sc_set_pwr_cmp(int val);
void gpio_sc_clk_sel_ext(int val);
int gpio_sc_get_vers();

#endif // __GPIO_SLOW_CONTROL_H__
