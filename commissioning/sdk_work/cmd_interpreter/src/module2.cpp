//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  28.04.2014 11:34:20
//
//  Description :  Example module for a command line interpreter test application
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#include "module2.h"
#include "xil_printf.h"

/************************************************************/
/* COMMAND TABLE                                            */
/************************************************************/
  cmd_table_entry module_2_cmd_table[] =
  {
    {"mod2f1", mod2func1},
    {"mod2f2", mod2func2},
#if INCLUDE_MOD2_FUNC_3
    {"mod2f3", mod2func3},
#endif
    {0, general_module2_help_func}
  };

/************************************************************/
  int mod2func1(int arc, char **argv)
  {
    xil_printf("I'm function mod2func1 of module2 called by %s with argument %s\n\r", argv[0], argv[1]);
    return 0;
  }

/************************************************************/
  int mod2func2(int arc, char **argv)
  {
    xil_printf("I'm function mod2func2 of module2 called by %s with arguments %s and %s\n\r", argv[0], argv[1], argv[2]);
    return 0;
  }

/************************************************************/
#if INCLUDE_MOD2_FUNC_3
  int mod2func3(int arc, char **argv)
  {
    xil_printf("I'm function mod2func3 of module2 called by %s with arguments %s, %s and %s\n\r", argv[0], argv[1], argv[2], argv[3]);
    return 0;
  }
#endif

/************************************************************/
  int general_module2_help_func(int arc, char **argv)
  {
    xil_printf("\n\rThis is the help text for module2\n\r");
    return 0;
  }

/************************************************************/
