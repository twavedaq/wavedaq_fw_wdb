//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  28.04.2014 11:33:53
//
//  Description :  Example module for a command line interpreter test application
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#ifndef __MODULE2_H__
#define __MODULE2_H__

/******************************************************************************/
/* include files                                                              */
/******************************************************************************/

#include "cmd_table.h"

/******************************************************************************/
/* definitions                                                                */
/******************************************************************************/

#define INCLUDE_MOD2_FUNC_3   1

  extern cmd_table_entry module_2_cmd_table[];

  int mod2func1(int arc, char **argv);
  int mod2func2(int arc, char **argv);

#if INCLUDE_MOD2_FUNC_3
  int mod2func3(int arc, char **argv);
#endif

  int general_module2_help_func(int arc, char **argv);

#endif // __MODULE2_H__
