//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  07.05.2014 12:23:51
//
//  Description :  Software controlled shift register.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#ifndef __GPIO_SHIFT_REGISTER_H__
#define __GPIO_SHIFT_REGISTER_H__

#define  SREG25   0
#define  SREG33   1

#define  GPIO_SREG_DI25_O    0x01
#define  GPIO_SREG_ST25_O    0x02
#define  GPIO_SREG_CLK25_O   0x04
#define  GPIO_SREG_DI33_O    0x08
#define  GPIO_SREG_ST33_O    0x10
#define  GPIO_SREG_CLK33_O   0x20

/*
*  Format :
*           D7    D6    D5    D4    D3    D2    D1    D0
*           ACDC  COMP2 OP2   COMP1 OP1   RSRVD CAL1  CAL0
*           ^first                                    ^last
*/

#define  FRONTEND_CFG_BIT_ACDC    0x80
#define  FRONTEND_CFG_BIT_COMP2   0x40
#define  FRONTEND_CFG_BIT_OP2     0x20
#define  FRONTEND_CFG_BIT_COMP1   0x10
#define  FRONTEND_CFG_BIT_OP1     0x08
#define  FRONTEND_CFG_BIT_RSRVD   0x04
#define  FRONTEND_CFG_BIT_CAL1    0x02
#define  FRONTEND_CFG_BIT_CAL0    0x01

/************************************************************/

void gpio_sreg_init(void);

// Frontend configuration functions
void frontend_setting_apply();
void frontend_setting_clr(unsigned int channel, unsigned int value);
void frontend_setting_set(unsigned int channel, unsigned int value);
void frontend_setting_write(unsigned int channel, unsigned int value);
#endif // __GPIO_SHIFT_REGISTER_H__
