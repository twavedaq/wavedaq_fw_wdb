//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e, theidel
//  Created :  30.07.2014 13:32:26
//
//  Description :  Processing the binary commands.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#ifndef __BIN_CMD_PROCESSOR_H__
#define __BIN_CMD_PROCESSOR_H__

/******************************************************************************/
/* include files                                                              */
/******************************************************************************/



/******************************************************************************/
/* definitions                                                                */
/******************************************************************************/

/* bit masks */
#define BINCMD_CHKSUM_BIT   0x80000000
#define BINCMD_CMD_MASK     0x0000007F
#define BINCMD_SIZE_MASK    0x0000FFFF
/* commands */
#define BINCMD_WRITE        0x00000001
#define BINCMD_READ         0x00000002
#define BINCMD_SET          0x00000003
#define BINCMD_CLEAR        0x00000004

#define BIN_BUFF_LEN   64

/******************************************************************************/
/* global variables                                                           */
/******************************************************************************/

extern unsigned int checksum_add;
extern unsigned int bin_bptr;
extern unsigned int bin_size;
extern unsigned int bin_buffer[BIN_BUFF_LEN];

/******************************************************************************/
/* function prototypes                                                        */
/******************************************************************************/

void bin_cmd_init(void);
void chk_binary_cmd(void);
int get_binary_cmd(void);
int bin_cmd_process(unsigned int *buffer_i,unsigned int len);

/******************************************************************************/

#endif // __CMD_PROCESSOR_H__
