//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  07.05.2014 08:55:59
//
//  Description :  Module for SPI access to LTC2600 temperature sensor.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#include "mod_dac.h"
#include "drv_soft_spi_temp_dac_lmk.h"
#include "xil_printf.h"
#include "utilities.h"
#include <stdlib.h>

/************************************************************/
/* COMMAND TABLE                                            */
/************************************************************/

  cmd_table_entry dac_cmd_table[] =
  {
    {"dac", dac_help},
    {"dacset", dac_configure_and_enable},
    {"daccfg", dac_configure},
    {"dacen", dac_enable},
    {"dacde", dac_disable},
    {0, general_spi_dac_help_func}
  };

/************************************************************/

  unsigned int get_iadr(char *adr)
  {
    if(fstrcmp(adr,"all"))
    {
      return 0x0F;
    }
    else if( (fstrcmp(adr,"a")) || (fstrcmp(adr,"rofs")) )
    {
      return 0x00;
    }
    else if( (fstrcmp(adr,"b")) || (fstrcmp(adr,"ofs")) )
    {
      return 0x01;
    }
    else if( (fstrcmp(adr,"c")) || (fstrcmp(adr,"caldc")) )
    {
      return 0x02;
    }
    else if( (fstrcmp(adr,"d")) || (fstrcmp(adr,"tlevel1")) )
    {
      return 0x03;
    }
    else if( (fstrcmp(adr,"e")) || (fstrcmp(adr,"tlevel2")) )
    {
      return 0x04;
    }
    else if( (fstrcmp(adr,"f")) || (fstrcmp(adr,"tlevel3")) )
    {
      return 0x05;
    }
    else if( (fstrcmp(adr,"g")) || (fstrcmp(adr,"tlevel4")) )
    {
      return 0x06;
    }
    else if( (fstrcmp(adr,"h")) || (fstrcmp(adr,"bias")) )
    {
      return 0x07;
    }
    else
    {
      return 0x08;
    }
  }

/************************************************************/

  int dac_help(int arc, char **argv)
  {
    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rDAC commands:\n\r");
      xil_printf("dacset:  set voltage to a DAC channel and enable it\n\r");
      xil_printf("daccfg:  set voltage to a DAC disabled channel\n\r");
      xil_printf("dacen:   enable a dac channel\n\r");
      xil_printf("dacde:   disable a dac channel\n\r");
      xil_printf("\n\r");
    }
    else
    {
      xil_printf("Unknown command\n\r");
    }
    return 0;
  }

/************************************************************/

  int dac_configure_and_enable(int arc, char **argv)
  {
    unsigned int adr;
    unsigned int voltage;

    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rUsage: dacset <dac channel> <voltage>\n\r\n\r");
      xil_printf("       dac channel:   dac channel to set a,b,c,d,e,f,g,h or all.\n\r");
      xil_printf("                      Alternatively rofs,ofs,caldc,tlevel1,tlevel2,tlevel3\n\r");
      xil_printf("                      tlevel4 or bias can be used to specify the channel.\n\r");
      xil_printf("       voltage:       voltage to set in mV\n\r");
      xil_printf("\n\rDescription: Sets the specified voltage to the corresponding DAC channel\n\r");
      xil_printf("             and enables (power up) it.\n\r");
      xil_printf("\n\r");
    }
    else
    {
      //Check for minimum number of arguments
      if(arc < 3)
      {
        xil_printf("Too few arguments\n\r");
        return 0;
      }

      adr = get_iadr(argv[1]);
      voltage = atoi(argv[2]);
      //Validate argument range
      if( (adr > 7) && (adr != 0x0F) )
      {
        xil_printf("Invalid arguments. Type \"dacset help\" for help.\n\r");
        return 0;
      }
      if( voltage > LTC2600_VREF_MV )
      {
        voltage = LTC2600_VREF_MV;
      }
      ltc2600_set(adr, LTC2600_CMD_WR_PU_N, voltage*1e6);
    }
    return 0;
  }

/************************************************************/

  int dac_configure(int arc, char **argv)
  {
    unsigned int adr;
    unsigned int voltage;

    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rUsage: daccfg <dac channel> <voltage>\n\r\n\r");
      xil_printf("       dac channel:   dac channel to set (a,b,c,d,e,f,g,h,all)\n\r");
      xil_printf("                      Alternatively rofs,ofs,caldc,tlevel1,tlevel2,tlevel3\n\r");
      xil_printf("                      tlevel4 or bias can be used to specify the channel.\n\r");
      xil_printf("       voltage:       voltage to set in mV\n\r");
      xil_printf("\n\rDescription: Sets the specified voltage to the corresponding DAC channel\n\r");
      xil_printf("             leaving it disabled (power down).\n\r");
      xil_printf("\n\r");
    }
    else
    {
      //Check for minimum number of arguments
      if(arc < 3)
      {
        xil_printf("Too few arguments\n\r");
        return 0;
      }

      adr = get_iadr(argv[1]);
      voltage = (unsigned int)(atoi(argv[2]));
      //Validate argument range
      if( (adr > 7) && (adr != 0x0F) )
      {
        xil_printf("Invalid arguments. Type \"daccfg help\" for help.\n\r");
        return 0;
      }
      //Limit the voltage value to Vref
      if( voltage > LTC2600_VREF_MV )
      {
        voltage = LTC2600_VREF_MV;
      }

      ltc2600_set(adr, LTC2600_CMD_WR_N, voltage*1e6);
    }
    return 0;
  }

/************************************************************/

  int dac_enable(int arc, char **argv)
  {
    unsigned int adr;

    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rUsage: dacen <dac channel>\n\r\n\r");
      xil_printf("       dac channel:   dac channel to enable (a,b,c,d,e,f,g,h,all)\n\r");
      xil_printf("                      Alternatively rofs,ofs,caldc,tlevel1,tlevel2,tlevel3\n\r");
      xil_printf("                      tlevel4 or bias can be used to specify the channel.\n\r");
      xil_printf("\n\rDescription: Enables the corresponding DAC channel (power up).\n\r");
      xil_printf("\n\r");
    }
    else
    {
      //Check for minimum number of arguments
      if(arc < 2)
      {
        xil_printf("Too few arguments\n\r");
        return 0;
      }

      adr = get_iadr(argv[1]);
      //Validate argument range
      if( (adr > 7) && (adr != 0x0F) )
      {
        xil_printf("Invalid arguments. Type \"daccfg help\" for help.\n\r");
        return 0;
      }
      ltc2600_set(adr, LTC2600_CMD_PU_N, 0);
    }
    return 0;
  }

/************************************************************/

  int dac_disable(int arc, char **argv)
  {
    unsigned int adr;

    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rUsage: dacde <dac channel>\n\r\n\r");
      xil_printf("       dac channel:   dac channel to disable (a,b,c,d,e,f,g,h,all)\n\r");
      xil_printf("                      Alternatively rofs,ofs,caldc,tlevel1,tlevel2,tlevel3\n\r");
      xil_printf("                      tlevel4 or bias can be used to specify the channel.\n\r");
      xil_printf("\n\rDescription: Disables the corresponding DAC channel (power down).\n\r");
      xil_printf("\n\r");
    }
    else
    {
      //Check for minimum number of arguments
      if(arc < 2)
      {
        xil_printf("Too few arguments\n\r");
        return 0;
      }

      adr = get_iadr(argv[1]);
      //Validate argument range
      if( (adr > 7) && (adr != 0x0F) )
      {
        xil_printf("Invalid arguments. Type \"daccfg help\" for help.\n\r");
        return 0;
      }
      ltc2600_set(adr, LTC2600_CMD_PD_N, 0);
    }
    return 0;
  }

/************************************************************/

  int general_spi_dac_help_func(int arc, char **argv)
  {
    xil_printf("\n\r* SPI DAC Module:\n\rAllows access to LTC2600 DAC via SPI.\n\r");
    xil_printf("Type \"dac help\" for help on DAC commands.\n\r");
    return 0;
  }

/************************************************************/
