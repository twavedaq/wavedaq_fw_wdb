//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  22.05.2014 13:30:33
//
//  Description :  Module for configuring doing system management and housekeeping.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#include "mod_system_management.h"
#include "gpio_slow_control.h"
#include "xil_printf.h"
#include "utilities.h"
#include <stdlib.h>

/************************************************************/
/* COMMAND TABLE                                            */
/************************************************************/

  cmd_table_entry system_management_cmd_table[] =
  {
    {"sm", sm_help},
    {"reset", sm_reset},
    {"calbuf", sm_set_cal_buf},
    {"calclk", sm_set_cal_clk},
    {"calosc", sm_set_cal_osc},
    {"pwrcmp", sm_set_pwr_cmp},
    {"clkext", sm_set_clk_ext},
    {"version", sm_get_version},
    {0, general_sm_help_func}
  };

/************************************************************/

  int sm_help(int arc, char **argv)
  {
    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rSystem Management commands:\n\r");
      xil_printf("reset:    starts an fpga reconfiguration\n\r");
      xil_printf("calbuf:   enables or disables the calibration buffers\n\r");
      xil_printf("calclk:   sets the calibration clock source\n\r");
      xil_printf("calosc:   enables or disables the calibration oscillator\n\r");
      xil_printf("pwrcmp:   enables or disables the power of the trigger comparators\n\r");
      xil_printf("clkext:   enables or disables the differential TR_CLK\n\r");
      xil_printf("version:  returns the version indicator values\n\r");
      xil_printf("\n\r");
    }
    else
    {
      xil_printf("Unknown command\n\r");
    }
    return 0;
  }

/************************************************************/

  int sm_reset(int arc, char **argv)
  {
    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rUsage: reset\n\r");
      xil_printf("\n\rDescription: Reconfigures the FPGA from the flash memory.\n\r");
      xil_printf("\n\r");
    }
    else
    {
      gpio_sc_reset();
    }

    return 0;
  }

/************************************************************/

  int sm_set_cal_buf(int arc, char **argv)
  {
    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rUsage: calbuf <on/off>\n\r\n\r");
      xil_printf("       on/off:   state to set ac/dc calibration buffers to.\n\r");
      xil_printf("\n\rDescription: Disables or enables the AC/DC calibration buffers.\n\r");
      xil_printf("\n\r");
    }
    else
    {
      //Check for minimum number of arguments
      if(arc < 2)
      {
        xil_printf("Too few arguments\n\r");
        return 0;
      }

      if(fstrcmp(argv[1],"on"))
      {
        gpio_sc_set_cal_buf(1);
      }
      else if(fstrcmp(argv[1],"off"))
      {
        gpio_sc_set_cal_buf(0);
      }
      else
      {
        xil_printf("Invalid arguments. Type \"calbuf help\" for help.\n\r");
        return 0;
      }
    }
    return 0;
  }

/************************************************************/

  int sm_set_cal_clk(int arc, char **argv)
  {
    int source;
    int destination;
    
    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rUsage: calclk <clock source> <drs channel>\n\r\n\r");
      xil_printf("       clock source:  source of the calibration clock (tca or lmk).\n\r");
      xil_printf("       drs channel:   DRS chip (a or b) the clock is applied to.\n\r");
      xil_printf("\n\rDescription: Applies one of two LMK clocks to the specified DRS chip\n\r");
      xil_printf("             calibration port.\n\r");
      xil_printf("\n\r");
    }
    else
    {
      //Check for minimum number of arguments
      if(arc < 3)
      {
        xil_printf("Too few arguments\n\r");
        return 0;
      }

      //Set calibration clock source
      if(fstrcmp(argv[1],"tca"))
      {
        source = 0;
      }
      else if(fstrcmp(argv[1],"lmk"))
      {
        source = 1;
      }
      else
      {
        xil_printf("Invalid arguments. Type \"calbuf help\" for help.\n\r");
        return 0;
      }

      //Set destination DRS chip
      if(fstrcmp(argv[2],"a"))
      {
        destination = GPIO_SC_OUT_CAL_CTRL_A;
      }
      else if(fstrcmp(argv[2],"b"))
      {
        destination = GPIO_SC_OUT_CAL_CTRL_B;
      }
      else
      {
        xil_printf("Invalid arguments. Type \"calbuf help\" for help.\n\r");
        return 0;
      }

      gpio_sc_set_cal_clk(source, destination);
    }
    return 0;
  }

/************************************************************/

  int sm_set_cal_osc(int arc, char **argv)
  {
    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rUsage: calosc <on/off>\n\r\n\r");
      xil_printf("       on/off:   state to set to the calibration oscillator\n\r");
      xil_printf("                 and its buffer.\n\r");
      xil_printf("\n\rDescription: Disables or enables the calibration oscillator and\n\r");
      xil_printf("             the corresponding buffer.\n\r");
      xil_printf("\n\r");
    }
    else
    {
      //Check for minimum number of arguments
      if(arc < 2)
      {
        xil_printf("Too few arguments\n\r");
        return 0;
      }

      if(fstrcmp(argv[1],"on"))
      {
        gpio_sc_set_cal_osc(1);
      }
      else if(fstrcmp(argv[1],"off"))
      {
        gpio_sc_set_cal_osc(0);
      }
      else
      {
        xil_printf("Invalid arguments. Type \"calosc help\" for help.\n\r");
        return 0;
      }
    }
    return 0;
  }

/************************************************************/

  int sm_set_pwr_cmp(int arc, char **argv)
  {
    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rUsage: pwrcmp <on/off>\n\r\n\r");
      xil_printf("       on/off:   state of the drs trigger comparator power\n\r");
      xil_printf("\n\rDescription: Disables or enables the power of the drs trigger\n\r");
      xil_printf("             comparator.\n\r");
      xil_printf("\n\r");
    }
    else
    {
      //Check for minimum number of arguments
      if(arc < 2)
      {
        xil_printf("Too few arguments\n\r");
        return 0;
      }

      if(fstrcmp(argv[1],"on"))
      {
        gpio_sc_set_pwr_cmp(1);
      }
      else if(fstrcmp(argv[1],"off"))
      {
        gpio_sc_set_pwr_cmp(0);
      }
      else
      {
        xil_printf("Invalid arguments. Type \"pwrcmp help\" for help.\n\r");
        return 0;
      }
    }
    return 0;
  }

/************************************************************/

  int sm_set_clk_ext(int arc, char **argv)
  {
    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rUsage: clkext <on/off>\n\r\n\r");
      xil_printf("       on/off:   state of the clock enable signal.\n\r");
      xil_printf("\n\rDescription: Disables or enables the differential TR_CLK.\n\r");
      xil_printf("\n\r");
    }
    else
    {
      //Check for minimum number of arguments
      if(arc < 2)
      {
        xil_printf("Too few arguments\n\r");
        return 0;
      }

      if(fstrcmp(argv[1],"on"))
      {
        gpio_sc_clk_sel_ext(1);
      }
      else if(fstrcmp(argv[1],"off"))
      {
        gpio_sc_clk_sel_ext(0);
      }
      else
      {
        xil_printf("Invalid arguments. Type \"clkext help\" for help.\n\r");
        return 0;
      }
    }
    return 0;
  }

/************************************************************/

  int sm_get_version(int arc, char **argv)
  {
    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rUsage: verions\n\r\n\r");
      xil_printf("\n\rDescription: Returns the HW version information (VERS_IND).\n\r");
      xil_printf("\n\r");
    }
    else
    {
      xil_printf("\n\rVersion (VERS_IND_2:VERS_IND1): 0x%02X\n\r\n\r", gpio_sc_get_vers());
    }
    return 0;
  }

/************************************************************/

  int general_sm_help_func(int arc, char **argv)
  {
    xil_printf("\n\r* System Management Module:\n\r");
    xil_printf("Commands for system mamangement functions\n\r");
    xil_printf("Type \"sm help\" for help on system management commands.\n\r");
    return 0;
  }

/************************************************************/
/************************************************************/
