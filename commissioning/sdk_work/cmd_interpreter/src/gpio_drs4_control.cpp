//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  07.05.2014 12:23:47
//
//  Description :  Software interface for the DRS4 control signals.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#include "xparameters.h"
#include "system.h"
#include "xil_printf.h"
#include "xtime.h"
#include "gpio_drs4_control.h"

/******************************************************************************/

void gpio_drs4_cfg_init(void)
{
  SYSTEM->gpio_sys->write(GPIO_DRS4_CFG_IF, 0x0000);
  SYSTEM->gpio_sys->tristate_write(GPIO_DRS4_CFG_IF, GPIO_DRS4_SROUT_A_I | GPIO_DRS4_SROUT_B_I);
}

/******************************************************************************/

void gpio_drs4_cfg_srclk(unsigned int val)
{
  if (val)
  {
    SYSTEM->gpio_sys->set(GPIO_DRS4_CFG_IF, GPIO_DRS4_SRCLK_O );
  }
  else
  {
    SYSTEM->gpio_sys->clr(GPIO_DRS4_CFG_IF, GPIO_DRS4_SRCLK_O );
  }
}

/******************************************************************************/

void gpio_drs4_cfg_srin(unsigned int val)
{
  if (val)
  {
    SYSTEM->gpio_sys->set(GPIO_DRS4_CFG_IF, GPIO_DRS4_SRIN_O );
  }
  else
  {
    SYSTEM->gpio_sys->clr(GPIO_DRS4_CFG_IF, GPIO_DRS4_SRIN_O );
  }
}


/******************************************************************************/

void gpio_drs4_cfg_a(unsigned int val)
{
  SYSTEM->gpio_sys->clr(GPIO_DRS4_CFG_IF, GPIO_DRS4_A_O );
  SYSTEM->gpio_sys->set(GPIO_DRS4_CFG_IF, (val & 0x0F) );
}

/******************************************************************************/

void gpio_drs4_cfg_rsload(unsigned int val)
{
  if (val)
  {
    SYSTEM->gpio_sys->set(GPIO_DRS4_CFG_IF, GPIO_DRS4_RSRLOAD_O );
  }
  else
  {
    SYSTEM->gpio_sys->clr(GPIO_DRS4_CFG_IF, GPIO_DRS4_RSRLOAD_O );
  }
}

/******************************************************************************/

int gpio_drs4_cfg_srout(unsigned int drs4_ch)
{
  if(SYSTEM->gpio_sys->get(GPIO_DRS4_CFG_IF) & drs4_ch)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/******************************************************************************/

void gpio_drs4_cfg_wait()
{
  usleep(10);
}

/******************************************************************************/

void gpio_drs4_cfg_transmit(unsigned int adr, unsigned char tx_data)
{
  int i;

  gpio_drs4_cfg_a(adr);

/*
*  Format :
*           D7 D6 D5 D4 D3 D2 D1 D0
*           ^first               ^last
*/

  for (i=0; i<8; i++)
  {
    gpio_drs4_cfg_srclk(0);
    gpio_drs4_cfg_srin(tx_data & 0x80);
    gpio_drs4_cfg_wait();
    gpio_drs4_cfg_srclk(1);
    gpio_drs4_cfg_wait();
    tx_data <<= 1;
  }
  gpio_drs4_cfg_srclk(0);
  gpio_drs4_cfg_wait();
}

/******************************************************************************/

void gpio_drs4_cfg_set_read_sreg(int position)
{
  int i;

  gpio_drs4_cfg_a(DRS4_ADR_EN_READ_SREG);

  for (i=0; i<DRS4_SAMPLING_CELLS_PER_CHANNEL; i++)
  {
    gpio_drs4_cfg_srclk(0);
    gpio_drs4_cfg_wait();
    gpio_drs4_cfg_srclk(1);
    if(i == position)
    {
      gpio_drs4_cfg_srin(1);
    }
    else
    {
      gpio_drs4_cfg_srin(0);
    }
    gpio_drs4_cfg_wait();
  }
  gpio_drs4_cfg_srclk(0);
}

/******************************************************************************/

int gpio_drs4_read_pll_lock(int channel)
{
  if(SYSTEM->gpio_sys->get(GPIO_DRS4_CFG_IF) & channel)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/******************************************************************************/
/******************************************************************************/
