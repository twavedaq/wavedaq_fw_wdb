//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  18.06.2014 13:51:04
//
//  Description :  Module for testing SERDES routed to the backplane.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#include "mod_serdes.h"
#include "gpio_looptest.h"
//#include "gpio_slow_control.h"
#include "xil_printf.h"
#include "utilities.h"
#include <stdlib.h>

/************************************************************/
/* COMMAND TABLE                                            */
/************************************************************/
  cmd_table_entry serdes_cmd_table[] =
  {
    {"srds", serdes_help},
    {"srdsloop", serdes_looptest},
    {0, general_serdes_help_func}
  };

/************************************************************/
  int serdes_help(int arc, char **argv)
  {
    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rSERDES commands:\n\r");
      xil_printf("srdsloop: performs a loopback test at the backplane serdes\n\r");
      xil_printf("\n\r");
    }
    else
    {
      xil_printf("Unknown command\n\r");
    }

    return 0;
  }

/************************************************************/

  int serdes_looptest(int arc, char **argv)
  {
    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rUsage: serdeslooptest <enable debugging>\n\r");
      xil_printf("       enable debugging: if set to \"dbg\", a detailed error report is shown.\n\r");
      xil_printf("                         If omitted, only a summary is shown at the end of the test.\n\r");
      xil_printf("\n\rDescription: Performs a looptest of the serdes lines to the backplane connector.\n\r");
      xil_printf("\n\r         The test drives a counter value to the serdes lines while reading the\n\r");
      xil_printf("\n\r         serdes_dcb lines and comparing the received values to the sent values.\n\r");
      xil_printf("\n\r         I.e. a loop cable has to connected to the test board.\n\r");
      xil_printf("\n\r");
    }
    else
    {
      xil_printf("\n\rONE HOT Loop Test:\n\r");
      serdes_looptest_oh_oc(7, ONE_HOT, 1);
      xil_printf("\n\r\n\rONE COLD Loop Test:\n\r");
      serdes_looptest_oh_oc(7, ONE_COLD, 1);
    }

    return 0;
  }

/************************************************************/

  int general_serdes_help_func(int arc, char **argv)
  {
    xil_printf("\n\r* SERDES Module:\n\rControls tests of the serdes ios to the backplane.\n\r");
    xil_printf("Type \"srds help\" for help on serdes commands.\n\r");
    return 0;
  }

/************************************************************/
