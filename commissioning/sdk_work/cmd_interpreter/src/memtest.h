

#ifndef __MEMTEST_H__
#define __MEMTEST_H__



#include "xparameters.h"
//#include "xcache_l.h"
#include "stdio.h"
//#include "xutil.h"
#include "xil_printf.h"

#if 0
  #define xil_printf(...)
  #define print(...)
#endif


/*****************************************************************************/

int memtest       (unsigned int do_print, unsigned int start_adr, unsigned size);
int memtest_8bit  (unsigned int do_print, unsigned int start_adr, unsigned size);
int memtest_16bit (unsigned int do_print, unsigned int start_adr, unsigned size);
int memtest_32bit (unsigned int do_print, unsigned int start_adr, unsigned size);

#endif // __MEMTEST_H__
