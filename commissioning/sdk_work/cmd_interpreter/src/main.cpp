//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  24.04.2014 11:50:06
//
//  Description :  Test application for a command line interpreter
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#include "xil_printf.h"

#include "term_cmd_input.h"
#include "bin_cmd_processor.h"
#include "cmd_processor.h"
#include "system.h"
#include "wd2_dbg.h"

/******************************************************************************/
/* global var definitions                                                     */
/******************************************************************************/
term_cmd_input_class *term_cmd_input;

int main()
{
  system_ptr = new system_class();
  SYSTEM->init();

  term_cmd_input = new term_cmd_input_class();
  if (!term_cmd_input)
  {
      if (DBG_ERR) xil_printf("\n\rERROR: term_cmd_input_class failed\n\r");
  }
  bin_cmd_init();

  if (DBG_INIT)
  {
    xil_printf("\r\n\r\n");
    xil_printf("**********************************\r\n");
    xil_printf("Test Firmware for WaveDream2 Board\n\r");
    xil_printf("%s               %s\r\n",__DATE__, __TIME__);
    xil_printf("**********************************\r\n\r\n");
  }

  while(1)   // main loop
  {
    term_cmd_input->chk_cmd();
    chk_binary_cmd();
  }

  return 0;
}
