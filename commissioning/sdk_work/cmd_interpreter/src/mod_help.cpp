//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  28.04.2014 13:02:30
//
//  Description :  Help module for a command line interpreter.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#include "mod_help.h"
#include "wd2_config.h"
#include "xil_printf.h"
#include <stdlib.h>

/************************************************************/
/* COMMAND TABLE                                            */
/************************************************************/
  cmd_table_entry help_cmd_table[] =
  {
    {"help", main_help},
    {0, 0}
  };

/************************************************************/
  int main_help(int arc, char **argv)
  {
    int cmd_table_index;
    int cmd_list_index;
    cmd_table_entry *cmd_table_ptr;

    xil_printf("\n\rModule Help:\n\r============\n\r");

    cmd_list_index = 0;
    while(cmd_list[cmd_list_index] != NULL)
    {
      cmd_table_ptr = cmd_list[cmd_list_index];
      cmd_table_index = 0;
      while(cmd_table_ptr[cmd_table_index].cmd_name != NULL)
      {
        cmd_table_index++;
      }
      if( cmd_table_ptr[cmd_table_index].cmd_func_ptr != NULL )
      {
        cmd_table_ptr[cmd_table_index].cmd_func_ptr(arc, argv);
      }
      cmd_list_index++;
    }

    xil_printf("\n\r");

    return 0;
  }

/************************************************************/
