//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  12.05.2014 08:53:40
//
//  Description :  Software interface for the DRS4 control signals.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#ifndef __GPIO_DRS4_CONTROL_H__
#define __GPIO_DRS4_CONTROL_H__

#define  DRS4_SAMPLING_CELLS_PER_CHANNEL   1024

#define  GPIO_DRS4_A_O          0x000F
#define  GPIO_DRS4_SRIN_O       0x0010
#define  GPIO_DRS4_SROUT_A_I    0x0020
#define  GPIO_DRS4_SROUT_B_I    0x0040
#define  GPIO_DRS4_SRCLK_O      0x0080
#define  GPIO_DRS4_RSRLOAD_O    0x0100
#define  GPIO_DRS4_PLLLCK_B_I   0x0200
#define  GPIO_DRS4_PLLLCK_A_I   0x0400
//#define  GPIO_DRS4_WSROUT_A_I   0x0800
//#define  GPIO_DRS4_WSROUT_B_I   0x1000
//#define  GPIO_DRS4_DENABLE_O    0x2000
//#define  GPIO_DRS4_DWRITE_O     0x4000

#define  DRS4_ADR_MUXSEL_CH0          0x00
#define  DRS4_ADR_MUXSEL_CH1          0x01
#define  DRS4_ADR_MUXSEL_CH2          0x02
#define  DRS4_ADR_MUXSEL_CH3          0x03
#define  DRS4_ADR_MUXSEL_CH4          0x04
#define  DRS4_ADR_MUXSEL_CH5          0x05
#define  DRS4_ADR_MUXSEL_CH6          0x06
#define  DRS4_ADR_MUXSEL_CH7          0x07
#define  DRS4_ADR_MUXSEL_CH8          0x08
#define  DRS4_ADR_EN_OUT_0TO8         0x09
#define  DRS4_ADR_EN_TRANSP_MODE      0x0A
#define  DRS4_ADR_EN_READ_SREG        0x0B
#define  DRS4_ADR_EN_CFG_SREG         0x0C
#define  DRS4_ADR_EN_WRITE_SREG       0x0D
#define  DRS4_ADR_EN_WRITE_CFG_SREG   0x0E
#define  DRS4_ADR_DISABLE_ALL_OUTS    0x0F

/************************************************************/

void gpio_drs4_cfg_init(void);

// DRS4 configuration functions
void gpio_drs4_cfg_a(unsigned int val);
void gpio_drs4_cfg_rsload(unsigned int val);
int gpio_drs4_cfg_srout(unsigned int drs4_ch);
void gpio_drs4_cfg_wait();
void gpio_drs4_cfg_transmit(unsigned int adr, unsigned char tx_data);
void gpio_drs4_cfg_set_read_sreg(int position);
int gpio_drs4_read_pll_lock(int channel);

#endif // __GPIO_DRS4_CONTROL_H__
