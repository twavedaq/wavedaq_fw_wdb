//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e, theidel
//  Created :  02.05.2014 13:24:35
//
//  Description :  Processing the commands entered via the terminal command line
//                 using the modular command implementation.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

/******************************************************************************/
/* include files                                                              */
/******************************************************************************/

#include <new>

#include "bin_cmd_processor.h"
#include "xil_printf.h"
#include <stdlib.h>
#include "utilities.h"
#include "system.h"
#include "plb_spi_slave.h"
#include "wd2_dbg.h"

//#include "wd2_config.h"

/******************************************************************************/
/* global vars                                                                */
/******************************************************************************/

unsigned int checksum_add;
unsigned int bin_bptr;
unsigned int bin_size;
unsigned int bin_buffer[BIN_BUFF_LEN];

/******************************************************************************/
/* function definitions                                                       */
/******************************************************************************/

void bin_cmd_init(void)
{
  bin_bptr = 0;
}

/******************************************************************************/

void chk_binary_cmd(void)
{
  int len;

  len = get_binary_cmd();
  if (len>0)
  {
     bin_cmd_process(bin_buffer, len);
  }
}

/******************************************************************************/

int get_binary_cmd(void)
{
  int len = -1;
  unsigned int rx_word;

  if (!sspi_rx_fifo_is_empty(system_ptr->spi_loop_slave_ptr))
  {
    sspi_receive(system_ptr->spi_loop_slave_ptr, &rx_word, 1, 1);
    if(bin_bptr == 0)
    {
      bin_size = BINCMD_SIZE_MASK & rx_word;
      if(BINCMD_CHKSUM_BIT & rx_word) // checksum included
      {
        checksum_add = 1;
      }
      else
      {
        checksum_add = 0;
      }
    }

    if (bin_bptr < BIN_BUFF_LEN)
    {
      // if the buffer is not full, store the word
      if(DBG_ALL)  xil_printf("bin_bptr = %d, rx_word = 0x%08X\n\r", bin_bptr, rx_word);
      bin_buffer[bin_bptr++] = rx_word;
    }
    else
    {
      // if there is a buffer overflow, reset
      len = -2;
      bin_bptr = 0;
    }

    if(bin_bptr == bin_size+2+checksum_add)
    {
      // if the last word is reached,
      // prepare for passing the buffer and reset
      len = bin_bptr;
      bin_bptr = 0;
    }
    // add overflow exception
  }
  return len;
}

/******************************************************************************/

int bin_cmd_process(unsigned int *buffer_i,unsigned int len)
{
  unsigned int cmd;
  unsigned int size;
  //unsigned int address;
  unsigned int checksum;

  cmd  = BINCMD_CMD_MASK  & (buffer_i[0] >> 24);
  size = BINCMD_SIZE_MASK &  buffer_i[0];
  //address = buffer_i[1];

  switch ( cmd )
  {
    case BINCMD_WRITE:
      // cmd_write_reg(&buffer_i[1], size);
      xil_printf("\n\r\n\rWriting to address 0x%08X:\n\r", buffer_i[1]);
      for(unsigned int i=0;i<size;i++)
      {
        xil_printf("word %d: 0x%08X\n\r", i, buffer_i[i+2]);
      }
      break;
    case BINCMD_SET:
      // cmd_read_reg(&buffer_i[1], size);
      // modify reg
      // cmd_write_reg(&buffer_i[1], size);
      xil_printf("\n\r\n\rSetting bits on address 0x%08X:\n\r", buffer_i[1]);
      for(unsigned int i=0;i<size;i++)
      {
        xil_printf("word %d: 0x%08X\n\r", i, buffer_i[i+2]);
      }
      break;
    case BINCMD_CLEAR:
      // cmd_read_reg(&buffer_i[1], size);
      // modify reg
      // cmd_write_reg(&buffer_i[1], size);
      xil_printf("\n\r\n\rClearing bits on address 0x%08X:\n\r", buffer_i[1]);
      for(unsigned int i=0;i<size;i++)
      {
        xil_printf("word %d: 0x%08X\n\r", i, buffer_i[i+2]);
      }
      break;
    case BINCMD_READ:
      // cmd_read_reg(&buffer_i[1], size);
      xil_printf("\n\r\n\rReading from address 0x%08X\n\r", buffer_i[1]);
      for(unsigned int i=0;i<size;i++)
      {
        xil_printf("word %d: 0x%08X\n\r", i, buffer_i[i+2]);
      }
      break;
    default:
      if(DBG_WARN)  xil_printf("\n\rWarning: unknown binary command\n\r\n\r");
      break;
  }

  if( BINCMD_CHKSUM_BIT & buffer_i[0] )
  {
    // checksum = calc_crc(buffer_i, size);
    xil_printf("\n\rTransmitted checksum: 0x%08X\n\r\n\r", buffer_i[size+2]);
  }

  return 0;
}

/******************************************************************************/
/******************************************************************************/
