//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  28.04.2014 13:02:30
//
//  Description :  Help module for a command line interpreter.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#ifndef __MOD_HELP_H__
#define __MOD_HELP_H__

/******************************************************************************/
/* include files                                                              */
/******************************************************************************/

#include "cmd_table.h"

/******************************************************************************/
/* definitions                                                                */
/******************************************************************************/

//#define INCLUDE_MOD1_FUNC_3   0

  extern cmd_table_entry help_cmd_table[];

  int main_help(int arc, char **argv);
//  int mod1func2(int arc, char **argv);

//#if INCLUDE_MOD1_FUNC_3
//  int mod1func3(int arc, char **argv);
//#endif

//  int general_module1_help_func(int arc, char **argv);

#endif // __MOD_HELP_H__
