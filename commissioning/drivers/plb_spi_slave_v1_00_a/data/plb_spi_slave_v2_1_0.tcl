##############################################################################
## Filename:          /afs/psi.ch/project/genie1414/work/wavedream/firmware/commissioning/drivers/plb_spi_slave_v1_00_a/data/plb_spi_slave_v2_1_0.tcl
## Description:       Microprocess Driver Command (tcl)
## Date:              Tue Jul 22 15:48:04 2014 (by Create and Import Peripheral Wizard)
##############################################################################

#uses "xillib.tcl"

proc generate {drv_handle} {
  xdefine_include_file $drv_handle "xparameters.h" "plb_spi_slave" "NUM_INSTANCES" "DEVICE_ID" "C_BASEADDR" "C_HIGHADDR" 
}
