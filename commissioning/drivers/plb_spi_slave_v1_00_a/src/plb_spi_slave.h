/*****************************************************************************
* Filename:          /afs/psi.ch/project/genie1414/work/wavedream/firmware/commissioning/drivers/plb_spi_slave_v1_00_a/src/plb_spi_slave.h
* Version:           1.00.a
* Description:       plb_spi_slave Driver Header File
* Date:              Tue Jul 22 15:48:04 2014 (by Create and Import Peripheral Wizard)
*****************************************************************************/

#ifndef PLB_SPI_SLAVE_H
#define PLB_SPI_SLAVE_H

/***************************** Include Files *******************************/

#include "xbasic_types.h"
#include "xstatus.h"
#include "xil_io.h"

/************************** Constant Definitions ***************************/


/**
 * User Logic Slave Space Offsets
 * -- SLV_REG0 : user logic slave module register 0
 * -- SLV_REG1 : user logic slave module register 1
 * -- SLV_REG2 : user logic slave module register 2
 * -- SLV_REG3 : user logic slave module register 3
 * -- SLV_REG4 : user logic slave module register 4
 * -- SLV_REG5 : user logic slave module register 5
 */
#define PLB_SPI_SLAVE_USER_SLV_SPACE_OFFSET (0x00000000)
#define PLB_SPI_SLAVE_SLV_REG0_OFFSET (PLB_SPI_SLAVE_USER_SLV_SPACE_OFFSET + 0x00000000)
#define PLB_SPI_SLAVE_SLV_REG1_OFFSET (PLB_SPI_SLAVE_USER_SLV_SPACE_OFFSET + 0x00000004)
#define PLB_SPI_SLAVE_SLV_REG2_OFFSET (PLB_SPI_SLAVE_USER_SLV_SPACE_OFFSET + 0x00000008)
#define PLB_SPI_SLAVE_SLV_REG3_OFFSET (PLB_SPI_SLAVE_USER_SLV_SPACE_OFFSET + 0x0000000C)
#define PLB_SPI_SLAVE_SLV_REG4_OFFSET (PLB_SPI_SLAVE_USER_SLV_SPACE_OFFSET + 0x00000010)
#define PLB_SPI_SLAVE_SLV_REG5_OFFSET (PLB_SPI_SLAVE_USER_SLV_SPACE_OFFSET + 0x00000014)

/**************************** Type Definitions *****************************/


/***************** Macros (Inline Functions) Definitions *******************/

/**
 *
 * Write a value to a PLB_SPI_SLAVE register. A 32 bit write is performed.
 * If the component is implemented in a smaller width, only the least
 * significant data is written.
 *
 * @param   BaseAddress is the base address of the PLB_SPI_SLAVE device.
 * @param   RegOffset is the register offset from the base to write to.
 * @param   Data is the data written to the register.
 *
 * @return  None.
 *
 * @note
 * C-style signature:
 * 	void PLB_SPI_SLAVE_mWriteReg(Xuint32 BaseAddress, unsigned RegOffset, Xuint32 Data)
 *
 */
#define PLB_SPI_SLAVE_mWriteReg(BaseAddress, RegOffset, Data) \
 	Xil_Out32((BaseAddress) + (RegOffset), (Xuint32)(Data))

/**
 *
 * Read a value from a PLB_SPI_SLAVE register. A 32 bit read is performed.
 * If the component is implemented in a smaller width, only the least
 * significant data is read from the register. The most significant data
 * will be read as 0.
 *
 * @param   BaseAddress is the base address of the PLB_SPI_SLAVE device.
 * @param   RegOffset is the register offset from the base to write to.
 *
 * @return  Data is the data from the register.
 *
 * @note
 * C-style signature:
 * 	Xuint32 PLB_SPI_SLAVE_mReadReg(Xuint32 BaseAddress, unsigned RegOffset)
 *
 */
#define PLB_SPI_SLAVE_mReadReg(BaseAddress, RegOffset) \
 	Xil_In32((BaseAddress) + (RegOffset))


/**
 *
 * Write/Read 32 bit value to/from PLB_SPI_SLAVE user logic slave registers.
 *
 * @param   BaseAddress is the base address of the PLB_SPI_SLAVE device.
 * @param   RegOffset is the offset from the slave register to write to or read from.
 * @param   Value is the data written to the register.
 *
 * @return  Data is the data from the user logic slave register.
 *
 * @note
 * C-style signature:
 * 	void PLB_SPI_SLAVE_mWriteSlaveRegn(Xuint32 BaseAddress, unsigned RegOffset, Xuint32 Value)
 * 	Xuint32 PLB_SPI_SLAVE_mReadSlaveRegn(Xuint32 BaseAddress, unsigned RegOffset)
 *
 */
#define PLB_SPI_SLAVE_mWriteSlaveReg0(BaseAddress, RegOffset, Value) \
 	Xil_Out32((BaseAddress) + (PLB_SPI_SLAVE_SLV_REG0_OFFSET) + (RegOffset), (Xuint32)(Value))
#define PLB_SPI_SLAVE_mWriteSlaveReg1(BaseAddress, RegOffset, Value) \
 	Xil_Out32((BaseAddress) + (PLB_SPI_SLAVE_SLV_REG1_OFFSET) + (RegOffset), (Xuint32)(Value))
#define PLB_SPI_SLAVE_mWriteSlaveReg2(BaseAddress, RegOffset, Value) \
 	Xil_Out32((BaseAddress) + (PLB_SPI_SLAVE_SLV_REG2_OFFSET) + (RegOffset), (Xuint32)(Value))
#define PLB_SPI_SLAVE_mWriteSlaveReg3(BaseAddress, RegOffset, Value) \
 	Xil_Out32((BaseAddress) + (PLB_SPI_SLAVE_SLV_REG3_OFFSET) + (RegOffset), (Xuint32)(Value))
#define PLB_SPI_SLAVE_mWriteSlaveReg4(BaseAddress, RegOffset, Value) \
 	Xil_Out32((BaseAddress) + (PLB_SPI_SLAVE_SLV_REG4_OFFSET) + (RegOffset), (Xuint32)(Value))
#define PLB_SPI_SLAVE_mWriteSlaveReg5(BaseAddress, RegOffset, Value) \
 	Xil_Out32((BaseAddress) + (PLB_SPI_SLAVE_SLV_REG5_OFFSET) + (RegOffset), (Xuint32)(Value))

#define PLB_SPI_SLAVE_mReadSlaveReg0(BaseAddress, RegOffset) \
 	Xil_In32((BaseAddress) + (PLB_SPI_SLAVE_SLV_REG0_OFFSET) + (RegOffset))
#define PLB_SPI_SLAVE_mReadSlaveReg1(BaseAddress, RegOffset) \
 	Xil_In32((BaseAddress) + (PLB_SPI_SLAVE_SLV_REG1_OFFSET) + (RegOffset))
#define PLB_SPI_SLAVE_mReadSlaveReg2(BaseAddress, RegOffset) \
 	Xil_In32((BaseAddress) + (PLB_SPI_SLAVE_SLV_REG2_OFFSET) + (RegOffset))
#define PLB_SPI_SLAVE_mReadSlaveReg3(BaseAddress, RegOffset) \
 	Xil_In32((BaseAddress) + (PLB_SPI_SLAVE_SLV_REG3_OFFSET) + (RegOffset))
#define PLB_SPI_SLAVE_mReadSlaveReg4(BaseAddress, RegOffset) \
 	Xil_In32((BaseAddress) + (PLB_SPI_SLAVE_SLV_REG4_OFFSET) + (RegOffset))
#define PLB_SPI_SLAVE_mReadSlaveReg5(BaseAddress, RegOffset) \
 	Xil_In32((BaseAddress) + (PLB_SPI_SLAVE_SLV_REG5_OFFSET) + (RegOffset))

/************************** Function Prototypes ****************************/


/**
 *
 * Run a self-test on the driver/device. Note this may be a destructive test if
 * resets of the device are performed.
 *
 * If the hardware system is not built correctly, this function may never
 * return to the caller.
 *
 * @param   baseaddr_p is the base address of the PLB_SPI_SLAVE instance to be worked on.
 *
 * @return
 *
 *    - XST_SUCCESS   if all self-test code passed
 *    - XST_FAILURE   if any self-test code failed
 *
 * @note    Caching must be turned off for this function to work.
 * @note    Self test may fail if data memory and device are not on the same bus.
 *
 */
XStatus PLB_SPI_SLAVE_SelfTest(void * baseaddr_p);

#endif /** PLB_SPI_SLAVE_H */
