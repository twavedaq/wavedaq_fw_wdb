##############################################################################
## Filename:          /afs/psi.ch/project/genie1414/work/wavedream/firmware/commissioning/drivers/plb_spi_master_v1_00_a/data/plb_spi_master_v2_1_0.tcl
## Description:       Microprocess Driver Command (tcl)
## Date:              Mon Jul 21 12:53:12 2014 (by Create and Import Peripheral Wizard)
##############################################################################

#uses "xillib.tcl"

proc generate {drv_handle} {
  xdefine_include_file $drv_handle "xparameters.h" "plb_spi_master" "NUM_INSTANCES" "DEVICE_ID" "C_BASEADDR" "C_HIGHADDR" 
}
