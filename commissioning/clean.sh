#!/bin/sh
if [ -n "$1" ]; then
  cd $1
else
  cd `dirname $0`
fi
echo cleaning EDK project in `pwd`

rm -f *.log
rm -f *.make
rm -f *.opt
rm -f system.xml
rm -rf hdl
rm -rf simulation
rm -rf blkdiagram
rm -rf blockdiagram
rm -rf implementation
rm -rf synthesis
rm -rf __xps
rm -rf microblaze_0
rm -rf ppc440_cpu
find . -name "*.*~" -exec rm -f {} \;
find . -name "*.bak" -exec rm -f {} \;
find . -name "dum*.txt*" -exec rm -f {} \;

