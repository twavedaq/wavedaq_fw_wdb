#! /usr/bin/env python
if __name__ == "__main__":
  import sys, os, csv
  #import re
  from datetime import datetime
  import openpyxl
  #import openpyxl.cell
  from openpyxl.utils import get_column_letter, column_index_from_string
#  from openpyxl.cell import get_column_letter, column_index_from_string
  from shutil import copyfile

  ### Usage:
  ###
  ### csv2regconst.py <xlsx path>
  ###
  ### <xlsx path> is the path to the register map Excel file to be processed.
  ###

  ###############################################################
  ###
  ### This script generates a VHDL and software files
  ### containing constans with register indices
  ### for a register bank based on an Excel.sheet.
  ###
  ###############################################################

  ## Win 10: Python 2.7.12 | Anaconda 4.2.0 | Openpyxl 2.6.4
  ## RH7:    Python 2.7.5  | Openpyxl 2.6.4

###################################################################################################
# File and Path Checks/Settings
###################################################################################################

  ### Checking input file parameters
  if os.path.isabs(sys.argv[1]):
    path = sys.argv[1]
  else:
    path = os.path.abspath('.')+os.sep+sys.argv[1]

  indir = os.path.dirname(path)
  infile = os.path.basename(path)

###################################################################################################
# Helper Functions
###################################################################################################

  def underscore_to_camelcase(name):
    return ''.join(w.capitalize() for w in name.split('_'))

  max_reg_name_len  = 0
  max_port_name_len = 0
  max_func_name_len = 0

  def reg_designator(reg):
    return "WD2_REG_%s"%reg[i_reg_name]
  def calc_reg_des_max_len():
    return max_reg_name_len + 8

  def bit_reg_designator(port):
    return "WD2_%s_REG"%port[i_port_name]
  def calc_bit_reg_des_max_len():
    return max_port_name_len + 8

  def bit_msk_designator(port):
    return "WD2_%s_MASK"%port[i_port_name]
  def calc_bit_msk_des_max_len():
    return max_port_name_len + 9

  def bit_ofs_designator(port):
    return "WD2_%s_OFS"%port[i_port_name]
  def calc_bit_ofs_des_max_len():
    return max_port_name_len + 8

  def bit_const_designator(port):
    return "WD2_%s_CONST"%port[i_port_name]
  def calc_bit_const_des_max_len():
    return max_port_name_len + 10

  def find_vcs_root(test):
      import subprocess
      import os
      cwd = os.getcwd()
      os.chdir(test)
      result = subprocess.check_output(["git", "rev-parse", "--show-toplevel"])
      os.chdir(cwd)
      result = result.rsplit('\n',1)[0]
      if os.path.isdir(result):
        return result
      else:
        return None

  def copy_file_to_target(src, dst):
    src_dir  = os.path.dirname(src)
    src_file = os.path.basename(src)
    dst_dir  = os.path.dirname(dst)
    dst_file = os.path.basename(dst)
    if not os.path.exists(dst_dir):
      create_folder = raw_input("Destination path does not exist. Shall I creat it? ([y]/n): ")
      if (not create_folder) or (create_folder == 'y') or (create_folder == "yes"):
        os.makedirs(dst_dir)
      else:
        return
    copyfile(src, dst)
    print "From %s\nto   %s"%(src, dst)

###################################################################################################
# Headers, Comments and Constant Code
###################################################################################################

  vhd_header_start = """---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Register Map
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid (Author of generation script)\n"""

  vhd_header_end = """--  Description :  Mapping of the register content of WaveDream2.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------\n"""

  c_h_header_start = """/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e (Author of generation script)\n"""

  c_h_header_end = """ *  Description :  Register map definitions.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */\n\n"""

  c_restore = """\n/******************************************************************************/
/* Register Restore                                                          */
/******************************************************************************/\n\n"""

  c_default = """\n/******************************************************************************/
/* Control Register Defaults                                                  */
/******************************************************************************/\n\n"""

  c_reg_mapping = """\n/******************************************************************************/
/* Register Mapping                                                           */
/******************************************************************************/\n\n"""

  c_bit_mapping = """\n/******************************************************************************/
/* Bit Mapping                                                                */
/******************************************************************************/\n\n"""

  py_header_start = """# #####################################################################################
#  Paul Scherrer Institut
# #####################################################################################
#
#  Project :  WaveDream2
#
#  Author  :  schmid_e (Author of generation script)\n"""

  py_header_end = """#  Description :  Register map definitions.
#
# #####################################################################################
# #####################################################################################

# ###############################################################################
# # definitions                                                                ##
# ###############################################################################\n\n"""

  py_end = """\n# #############################################################################
# #############################################################################\n"""

  mpd_header_start = """#######################################################################################
##  Paul Scherrer Institut
#######################################################################################
##
##  Unit :  Register Map
##
##  Project :  WaveDream2
##
##  PCB  :  -
##  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
##
##  Tool Version :  14.7 (Version the code was testet with)
##
##  Author  :  Elmar Schmid (Author of generation script)\n"""
  mpd_header_end = """##  Description :  .mpd file for pcore of register bank.
##
#######################################################################################
#######################################################################################

BEGIN util_register_map

## Peripheral Options
OPTION IPTYPE = PERIPHERAL
OPTION IMP_NETLIST = TRUE
OPTION HDL = VHDL
OPTION IP_GROUP = USER


## Bus Interfaces\n"""

  mpd_end = "\nEND\n"

  class_header_start = """//
//  WDBReg.h
//
//  WaveDAQ Register Access Class
//
//  This file is generated automatically, please do not edit!
//\n"""

  class_header_end = """//--------------------------------------------------------------------

class WDBREG {

private:
   // virtual bit functions, must be implemented in derived class
   virtual unsigned int BitExtractStatus(unsigned int reg, unsigned int mask, unsigned int ofs) = 0;
   virtual unsigned int BitExtractControl(unsigned int reg, unsigned int mask, unsigned int ofs) = 0;
   virtual void SetRegMask(unsigned int reg, unsigned int mask, unsigned int ofs, unsigned int v) = 0;

public:

   // constructor
   WDBREG(){};\n\n"""

  class_end = """\n};\n\n//--------------------------------------------------------------------

#endif /* defined(__wdbreg_h__) */\n"""

  html_end = """
</body>

</html>
"""

  css_content = """/* Created :   %s */
html *
{
   font-family: Arial, Helvetica, sans-serif !important;
}

table {
  border-collapse: collapse;
  border: none;
}

th, td {
  padding-top: 3px;
  padding-bottom: 3px;
  padding-left: 10px;
  padding-right: 10px;
  border: 1px solid black;
}

tr.reg_header {
  font-weight: bold;
  background: rgb(150,180,215);
  border: 3px solid black;
}

tr.reg_name {
  font-weight: bold;
  background: rgb(150,180,215);
  border: 3px solid black;
}

td.reg_table_title, tr.reg_table_title {
  background: rgb(255,255,255);
  padding: 0px;
  border: none;
}

td.back_to_overview, tr.back_to_overview {
  background: rgb(255,255,255);
  padding: 0px;
  border: none;
  border-top: 3px solid black;
}

tbody tr.reg_list:nth-child(even) {
  background: rgb(185,205,230);
  border-left:  3px solid black;
  border-right: 3px solid black;
}

tbody tr.reg_list:nth-child(odd) {
  background: rgb(150,180,215);
  border-left:  3px solid black;
  border-right: 3px solid black;
}

tbody tr.reg_content:nth-child(even) {
  background: rgb(170,255,255);
  border-left:  3px solid black;
  border-right: 3px solid black;
}

tbody tr.reg_content:nth-child(odd) {
  background: rgb(220,255,255);
  border-left:  3px solid black;
  border-right: 3px solid black;
}

a:link {
  color: rgb(0,0,0);
  text-decoration: none;
}

a:visited {
  color: rgb(20,20,20);
}

a:hover {
  color: rgb(235,235,235);
}

a:active {
  color: rgb(255,255,0);
}
"""%datetime.now().strftime('%d.%m.%Y %H:%M:%S')

###################################################################################################
# File Processing
###################################################################################################

  #generic constants
  bits_per_reg  = 32

  extra_name_len = 12

  # Indices
  i_reg_num     = 0
  i_reg_name    = 1
  i_reg_offset  = 2
  i_reg_type    = 3
  i_reg_hw_sw   = 4
  i_reg_default = 5
  i_reg_comment = 6
  i_reg_func    = 7
  i_reg_param   = 8
  i_reg_group   = 9
  i_port_count         =  0
  i_port_name          =  1
  i_port_name_pfx      =  2
  i_port_dir           =  3
  i_port_type          =  4
  i_port_vec           =  5
  i_port_range_hi      =  6
  i_port_range_lo      =  7
  i_port_initial       =  8
  i_port_initial_valid =  9
  i_port_comment       = 10

  ### Reading Register Information
  print "\nReading %s\n"%(indir + os.sep + infile)

  portlist   = []
  reglist    = []

  reg_names  = []
  port_names = []

  #in_file = file(indir+os.sep+infile,'r')
  excel_wb = openpyxl.load_workbook(indir+os.sep+infile, data_only=True)
  # data_only means reading the results of formulas rather than the formula itself
  ctrl_sheet = excel_wb.get_sheet_by_name('Control')
  stat_sheet = excel_wb.get_sheet_by_name('Status')
  cfg_sheet  = excel_wb.get_sheet_by_name('Reg Bank Config')

  print "\nReading configuration\n"
  ctrl_base_addr = -1
  stat_base_addr = -1
  for row in cfg_sheet['A3':'C10']:
    if row[0].value == 'Control Register Base Offset':
      ctrl_base_addr = row[1].value
    elif row[0].value == 'Status Register Base Offset':
      stat_base_addr = row[1].value

  if ctrl_base_addr == -1 or stat_base_addr == -1:
    print "\n!!! ERROR !!!   Failed to read register base offsets\n"
    print "\nQUITTING SCRIPT\n\n"
    quit()

  print "Generating register list\n"
  for excel_sheet in [ctrl_sheet,stat_sheet]:
    if excel_sheet == ctrl_sheet:
      print "Sheet: " + ctrl_sheet.title + "..."
    else:
      print "Sheet: " + stat_sheet.title + "...\n"

    reg_count = 0
    header_found = 0
    hw_regs = ""

    for row in excel_sheet['A2':'K2']:
      for current_cell in row:
        if current_cell.value == '#':
          header_found = 1
          i_csv_reg_num = current_cell.column-1
        elif current_cell.value == 'Offset_dec':
          i_csv_offset  = current_cell.column-1
        elif current_cell.value == 'Name':
          i_csv_name    = current_cell.column-1
        elif current_cell.value == 'Bit':
          i_csv_range   = current_cell.column-1
        elif current_cell.value == 'Initial':
          i_csv_initial = current_cell.column-1
        elif current_cell.value == 'Default':
          i_csv_default = current_cell.column-1
        elif current_cell.value == 'R/W':
          i_csv_rw = current_cell.column-1
        elif current_cell.value == 'HW/SW':
          i_csv_hw_sw = current_cell.column-1
        elif current_cell.value == 'Comment':
          i_csv_comment = current_cell.column-1
        elif current_cell.value == 'SW Function':
          i_csv_func = current_cell.column-1
        elif current_cell.value == 'SW Parameter':
          i_csv_param = current_cell.column-1
        elif current_cell.value == 'Group':
          i_csv_group = current_cell.column-1
      if header_found == 1:
        break;

    for row in excel_sheet['A3':'K2000']:
      port_item  = []
      reg_item   = []
      if row[i_csv_reg_num].value != None:
        reg_count += 1
        reg_item.append(row[i_csv_reg_num].value)
        reg_item.append(row[i_csv_name].value)
        reg_item.append("0x%04X" % row[i_csv_offset].value)
        if excel_sheet == ctrl_sheet:
          reg_item.append("ctrl")
        else:
          reg_item.append("stat")
        reg_item.append(row[i_csv_hw_sw].value)
        if "HW" in row[i_csv_hw_sw].value:
          hw_regs += "1"
        else:
          hw_regs += "0"
        reg_item.append(row[i_csv_default].value)
        reg_item.append(str(row[i_csv_comment].value))
        reg_item.append(str(row[i_csv_func].value))
        if max_func_name_len < (len(str(row[i_csv_func].value))):
          max_func_name_len = len(row[i_csv_func].value)
        reg_item.append(str(row[i_csv_param].value))
        reg_item.append(str(row[i_csv_group].value))
        reglist.append(reg_item)
        reg_names.append(row[i_csv_name].value)
        if max_reg_name_len < (len(row[i_csv_name].value)):
          max_reg_name_len = len(row[i_csv_name].value)
      elif row[i_csv_range].value != None:
        port_item.append(reg_count-1)
        bit_range = str(row[i_csv_range].value).split(':',1)
        range_hi = int(bit_range[0])
        if len(bit_range)>1:
          range_lo = int(bit_range[1])
        else:
          range_lo = range_hi
        if excel_sheet == ctrl_sheet:
          port_item.append(row[i_csv_name].value.upper())
          port_item.append("_O")
          port_item.append("out")
          port_item.append("ctrl")
        else:
          port_item.append(row[i_csv_name].value.upper())
          port_item.append("_I")
          port_item.append("in ")
          port_item.append("stat")
        if max_port_name_len < (len(row[i_csv_name].value)):
          max_port_name_len = len(row[i_csv_name].value)
        if len(bit_range)>1:
          port_item.append("std_logic_vector")
        else:
          port_item.append("std_logic")
        port_item.append(range_hi)
        port_item.append(range_lo)
        if excel_sheet == ctrl_sheet:
          if row[i_csv_default].value != None:
            port_item.append(int(row[i_csv_default].value,0))
            port_item.append(True)
          else:
            port_item.append(0)
            port_item.append(False)
        else:
          if row[i_csv_initial].value != None:
            port_item.append(int(row[i_csv_initial].value,0))
            port_item.append(True)
          else:
            port_item.append(0)
            port_item.append(False)
        port_item.append(str(row[i_csv_comment].value))
        portlist.append(port_item)
        if (row[i_csv_name].value).lower() != "reserved":
          port_names.append(row[i_csv_name].value)
        if excel_sheet == stat_sheet:
          stat_port_item = port_item[:]
          stat_port_item[i_port_name_pfx] = "_O"
          stat_port_item[i_port_dir]      = "out"
          portlist.append(stat_port_item)

    if excel_sheet == ctrl_sheet:
      num_ctrl_regs = reg_count
      hw_ctrl_regs  = hw_regs
    else:
      num_stat_regs = reg_count
      hw_stat_regs  = hw_regs

  # check for duplicate register names
  dupl_set = set()
  duplicate_names = set(x for x in reg_names if x in dupl_set or dupl_set.add(x))
  if duplicate_names:
    print "\n!!! ERROR !!!   Duplicate register names found:\n"
    print "\n".join(str(i) for i in duplicate_names)
    print "\n"
    print "\nQUITTING SCRIPT\n\n"
    quit()
  # check for duplicate bit/port names
  dupl_set = set()
  duplicate_names = set(x for x in port_names if x in dupl_set or dupl_set.add(x))
  if duplicate_names:
    print "\n!!! ERROR !!!   Duplicate bit/port names found:\n"
    print "\n".join(str(i) for i in duplicate_names)
    print "\n"
    print "\nQUITTING SCRIPT\n\n"
    quit()
  # extract register layout version
  reg_layout_version = -1
  for port in portlist:
    if ("stat" in port[i_port_type]) and ("REG_LAYOUT_VERSION" in port[i_port_name]):
      reg_layout_version = port[i_port_initial]

  if reg_layout_version == -1:
    sys.exit('Error: no valid register layout version found')
  else:
    print "Register Layout Version: %d\n"%(reg_layout_version)

  # calculate maximum lengths
  reg_des_max_len       = calc_reg_des_max_len()
  bit_reg_des_max_len   = calc_bit_reg_des_max_len()
  bit_msk_des_max_len   = calc_bit_msk_des_max_len()
  bit_ofs_des_max_len   = calc_bit_ofs_des_max_len()
  bit_const_des_max_len = calc_bit_const_des_max_len()

  ### create pcore directory structure
  def gen_pcore_dirs():
    print "Creating pcore_dir structure..."
    if not os.path.exists(pcore_dir):
      os.makedirs(pcore_dir)
    if not os.path.exists(pcore_vhdl_dir):
      os.makedirs(pcore_vhdl_dir)
    if not os.path.exists(pcore_data_dir):
      os.makedirs(pcore_data_dir)




  # ###### vhd file functions ###############################################################################################

  # vhd main register io ports

  def write_vhd_main_io(out_file):
    outline = "\n  -- Register Bank Status Contents (Ext to uBlaze)\n"
    out_file.write(outline)
    port_name = '{0:<{max_port_name_len}}'.format("STATUS_REG_O", max_port_name_len=max_port_name_len+extra_name_len)
    outline = "    %s : out std_logic_vector(CGN_NUM_STAT_REG*CGN_BITS_PER_REG-1 downto 0);\n"%(port_name)
    out_file.write(outline)

    outline = "\n  -- Register Bank Status Contents (uBlaze to Ext)\n"
    out_file.write(outline)
    port_name = '{0:<{max_port_name_len}}'.format("STATUS_REG_I", max_port_name_len=max_port_name_len+extra_name_len)
    outline = "    %s : in  std_logic_vector(CGN_NUM_STAT_REG*CGN_BITS_PER_REG-1 downto 0);\n"%(port_name)
    out_file.write(outline)

    outline = "\n  -- Register Bank Read Bits\n"
    out_file.write(outline)
    port_name = '{0:<{max_port_name_len}}'.format("STAT_REG_READ_I", max_port_name_len=max_port_name_len+extra_name_len)
    outline = "    %s : in  std_logic_vector(CGN_NUM_STAT_REG-1 downto 0);\n"%(port_name)
    out_file.write(outline)

    outline = "\n  -- Register Bank Write Bits\n"
    out_file.write(outline)
    port_name = '{0:<{max_port_name_len}}'.format("STAT_REG_WRITE_I", max_port_name_len=max_port_name_len+extra_name_len)
    outline = "    %s : in  std_logic_vector(CGN_NUM_STAT_REG-1 downto 0);\n"%(port_name)
    out_file.write(outline)

    outline = "\n  -- Register Bank Bit Triggers\n"
    out_file.write(outline)
    port_name = '{0:<{max_port_name_len}}'.format("STAT_BIT_TRIG_I", max_port_name_len=max_port_name_len+extra_name_len)
    outline = "    %s : in  std_logic_vector(CGN_NUM_STAT_REG*CGN_BITS_PER_REG-1 downto 0);\n"%(port_name)
    out_file.write(outline)

    outline = "\n  -- Register Bank Byte Triggers\n"
    out_file.write(outline)
    port_name = '{0:<{max_port_name_len}}'.format("STAT_BYTE_TRIG_I", max_port_name_len=max_port_name_len+extra_name_len)
    outline = "    %s : in  std_logic_vector((CGN_NUM_STAT_REG*CGN_BITS_PER_REG/8)-1 downto 0);\n"%(port_name)
    out_file.write(outline)

    outline = "\n  -- Register Bank Control Contents\n"
    out_file.write(outline)
    port_name = '{0:<{max_port_name_len}}'.format("CONTROL_REG_I", max_port_name_len=max_port_name_len+extra_name_len)
    outline = "    %s : in  std_logic_vector(CGN_NUM_CTRL_REG*CGN_BITS_PER_REG-1 downto 0);\n"%(port_name)
    out_file.write(outline)

    outline = "\n  -- Register Bank Read Bits\n"
    out_file.write(outline)
    port_name = '{0:<{max_port_name_len}}'.format("CTRL_REG_READ_I", max_port_name_len=max_port_name_len+extra_name_len)
    outline = "    %s : in  std_logic_vector(CGN_NUM_CTRL_REG-1 downto 0);\n"%(port_name)
    out_file.write(outline)

    outline = "\n  -- Register Bank Write Bits\n"
    out_file.write(outline)
    port_name = '{0:<{max_port_name_len}}'.format("CTRL_REG_WRITE_I", max_port_name_len=max_port_name_len+extra_name_len)
    outline = "    %s : in  std_logic_vector(CGN_NUM_CTRL_REG-1 downto 0);\n"%(port_name)
    out_file.write(outline)

    outline = "\n  -- Register Bank Bit Triggers\n"
    out_file.write(outline)
    port_name = '{0:<{max_port_name_len}}'.format("CTRL_BIT_TRIG_I", max_port_name_len=max_port_name_len+extra_name_len)
    outline = "    %s : in  std_logic_vector(CGN_NUM_CTRL_REG*CGN_BITS_PER_REG-1 downto 0);\n"%(port_name)
    out_file.write(outline)

    outline = "\n  -- Register Bank Byte Triggers\n"
    out_file.write(outline)
    port_name = '{0:<{max_port_name_len}}'.format("CTRL_BYTE_TRIG_I", max_port_name_len=max_port_name_len+extra_name_len)
    outline = "    %s : in  std_logic_vector((CGN_NUM_CTRL_REG*CGN_BITS_PER_REG/8)-1 downto 0)\n"%(port_name)
    out_file.write(outline)

  # vhd entity
  def write_vhd_entity(out_file):
    outline = "entity util_register_map is\n  generic (\n"
    out_file.write(outline)
    outline = "    CGN_BITS_PER_REG : integer := %s;\n"%(bits_per_reg)
    out_file.write(outline)
    outline = "    CGN_NUM_CTRL_REG : integer := %s;\n"%(num_ctrl_regs)
    out_file.write(outline)
    outline = "    CGN_NUM_STAT_REG : integer := %s;\n"%(num_stat_regs)
    out_file.write(outline)
    outline = "    CGN_HW_CTRL_REG  : std_logic_vector(0 to %d) := \"%s\";\n"%(num_ctrl_regs-1, hw_ctrl_regs)
    out_file.write(outline)
    outline = "    CGN_HW_STAT_REG  : std_logic_vector(0 to %d) := \"%s\"\n"%(num_stat_regs-1, hw_stat_regs)
    out_file.write(outline)
    outline = "  );\n  port (\n"
    out_file.write(outline)
    # write all ports with comments per register
    current_reg = -1
    reg_index = -1
    for port in portlist:
      if current_reg != port[i_port_count]:
        current_reg = port[i_port_count]
        reg_index  += 1
        if "ctrl" in port[i_port_type]:
          outline = "\n    -- Control"
        else:
          outline = "\n    -- Status"
        #outline = outline + " Register %s [%s]: %s - %s (Default: %s)\n"%(reglist[reg_index][i_reg_num], reglist[reg_index][i_reg_offset], reglist[reg_index][i_reg_name], reglist[reg_index][i_reg_comment], reglist[reg_index][i_reg_default])
        outline = outline + " Register %s [%s]: %s\n"%(reglist[reg_index][i_reg_num], reglist[reg_index][i_reg_offset], reglist[reg_index][i_reg_name])
        out_file.write(outline)
        if "HW" in reglist[reg_index][i_reg_hw_sw]:
          reg_name = '{0:<{max_port_name_len}}'.format(reglist[reg_index][i_reg_name] + "_BYTE_TRIG_O", max_port_name_len=max_port_name_len+extra_name_len)
          outline = "    %s : out std_logic_vector(%s downto 0);\n"%(reg_name, bits_per_reg/8-1)
          out_file.write(outline)
          reg_name = '{0:<{max_port_name_len}}'.format(reglist[reg_index][i_reg_name] + "_REG_READ_O", max_port_name_len=max_port_name_len+extra_name_len)
          outline = "    %s : out std_logic;\n"%(reg_name)
          out_file.write(outline)
          reg_name = '{0:<{max_port_name_len}}'.format(reglist[reg_index][i_reg_name] + "_REG_WRITE_O", max_port_name_len=max_port_name_len+extra_name_len)
          outline = "    %s : out std_logic;\n"%(reg_name)
          out_file.write(outline)
        else:
          outline = "    -- '--> Pure software register => no ports available\n"
          out_file.write(outline)
      if "RESERVED" not in port[i_port_name]:
        if "HW" in reglist[reg_index][i_reg_hw_sw]:
          port_name = '{0:<{max_port_name_len}}'.format(port[i_port_name]+port[i_port_name_pfx], max_port_name_len=max_port_name_len+extra_name_len)
          outline = "    %s : %s "%(port_name, port[i_port_dir])
          port_vec = "%s"%(port[i_port_vec])
          if port[i_port_range_hi] != port[i_port_range_lo]:
            port_vec = port_vec + "(%s downto 0)"%(port[i_port_range_hi]-port[i_port_range_lo])
          port_vec = '{0:<34}'.format(port_vec + ";")
          #outline = outline + port_vec + "-- %s\n"%(port[i_port_comment])
          outline = outline + port_vec + "\n"
          out_file.write(outline)
          # Trigger Signals for Control and Status Registers
          if "out" in port[i_port_dir]:
            port_name = '{0:<{max_port_name_len}}'.format(port[i_port_name]+"_BIT_TRIG"+port[i_port_name_pfx], max_port_name_len=max_port_name_len+extra_name_len)
            outline = "    %s : %s %s"%(port_name, port[i_port_dir], port[i_port_vec])
            if port[i_port_range_hi] != port[i_port_range_lo]:
              outline = outline + "(%s downto 0)"%(port[i_port_range_hi]-port[i_port_range_lo])
            outline = outline + ";\n"
            out_file.write(outline)
    write_vhd_main_io(out_file)
    outline = "  );\nend util_register_map;\n\n"
    out_file.write(outline)

  # vhd constants
  def write_vhd_constants(out_file):
    for i in range(0,num_ctrl_regs):
      outline = "  constant C_CTRL_REG%s_OFFSET : integer := %s*CGN_BITS_PER_REG;\n"%(i,i)
      out_file.write(outline)
    out_file.write("\n")
    for i in range(0,num_stat_regs):
      outline = "  constant C_STAT_REG%s_OFFSET : integer := %s*CGN_BITS_PER_REG;\n"%(i,i)
      out_file.write(outline)

  # vhd internal signal definition
  def write_vhd_int_signals(out_file):
    outline = "\n  -- Register Bank Status Contents (Defaults)\n"
    out_file.write(outline)
    for reg in reversed(reglist):
      if "stat" in reg[i_reg_type]:
        reg_default = str(reg[i_reg_default]).split('x',1)
        if reg[i_reg_num] == num_stat_regs-1:
          outline = "  signal status_reg : std_logic_vector(CGN_NUM_STAT_REG*CGN_BITS_PER_REG-1 downto 0) := X\"%s\""%(reg_default[1])
        else:
          outline = "                                                                                        X\"%s\""%(reg_default[1])
        out_file.write(outline)
        if reg[i_reg_num] == 0:
          outline = ";  -- %s %s\n"%(reg[i_reg_offset], reg[i_reg_name])
        else:
          outline = " & -- %s %s\n"%(reg[i_reg_offset], reg[i_reg_name])
        out_file.write(outline)
    outline = "\n"
    out_file.write(outline)

  # vhd signal mappings
  def write_vhd_signal_mappings(out_file):
    outline = "\n  STATUS_REG_O <= status_reg;\n"
    out_file.write(outline)
    current_reg = -1
    reg_index = -1
    for port in portlist:
      if current_reg != port[i_port_count]:
        current_reg = port[i_port_count]
        reg_index += 1
        if "ctrl" in port[i_port_type]:
          #outline = "\n  -- Control Register %s [%s]: %s - %s (Default: %s)\n"%(reglist[reg_index][i_reg_num], reglist[reg_index][i_reg_offset], reglist[reg_index][i_reg_name], reglist[reg_index][i_reg_comment], reglist[reg_index][i_reg_default])
          outline = "\n  -- Control Register %s [%s]: %s\n"%(reglist[reg_index][i_reg_num], reglist[reg_index][i_reg_offset], reglist[reg_index][i_reg_name])
          out_file.write(outline)
          if "HW" in reglist[reg_index][i_reg_hw_sw]:
            port_name = '{0:<{max_port_name_len}}'.format(reglist[reg_index][i_reg_name]+"_BYTE_TRIG_O", max_port_name_len=max_port_name_len+extra_name_len)
            outline = "  %s <= CTRL_BYTE_TRIG_I((C_CTRL_REG%s_OFFSET/8)+3 downto C_CTRL_REG%s_OFFSET/8);\n"%(port_name, port[i_port_count], port[i_port_count])
            out_file.write(outline)
            port_name = '{0:<{max_port_name_len}}'.format(reglist[reg_index][i_reg_name]+"_REG_READ_O", max_port_name_len=max_port_name_len+extra_name_len)
            outline = "  %s <= CTRL_REG_READ_I(C_CTRL_REG%s_OFFSET/CGN_BITS_PER_REG);\n"%(port_name, port[i_port_count])
            out_file.write(outline)
            port_name = '{0:<{max_port_name_len}}'.format(reglist[reg_index][i_reg_name]+"_REG_WRITE_O", max_port_name_len=max_port_name_len+extra_name_len)
            outline = "  %s <= CTRL_REG_WRITE_I(C_CTRL_REG%s_OFFSET/CGN_BITS_PER_REG);\n"%(port_name, port[i_port_count])
            out_file.write(outline)
          else:
            outline = "  -- '--> Pure software register => no ports assigned\n"
            out_file.write(outline)
        else:
          #outline = "\n  -- Status Register %s [%s]: %s - %s\n"%(reglist[reg_index][i_reg_num], reglist[reg_index][i_reg_offset], reglist[reg_index][i_reg_name], reglist[reg_index][i_reg_comment])
          outline = "\n  -- Status Register %s [%s]: %s\n"%(reglist[reg_index][i_reg_num], reglist[reg_index][i_reg_offset], reglist[reg_index][i_reg_name])
          out_file.write(outline)
          if "HW" in reglist[reg_index][i_reg_hw_sw]:
            port_name = '{0:<{max_port_name_len}}'.format(reglist[reg_index][i_reg_name]+"_BYTE_TRIG_O", max_port_name_len=69)
            outline = "  %s <= STAT_BYTE_TRIG_I((C_STAT_REG%s_OFFSET/8)+3 downto C_STAT_REG%s_OFFSET/8);\n"%(port_name, port[i_port_count], port[i_port_count])
            out_file.write(outline)
            port_name = '{0:<{max_port_name_len}}'.format(reglist[reg_index][i_reg_name]+"_REG_READ_O", max_port_name_len=69)
            outline = "  %s <= STAT_REG_READ_I(C_STAT_REG%s_OFFSET/CGN_BITS_PER_REG);\n"%(port_name, port[i_port_count])
            out_file.write(outline)
            port_name = '{0:<{max_port_name_len}}'.format(reglist[reg_index][i_reg_name]+"_REG_WRITE_O", max_port_name_len=69)
            outline = "  %s <= STAT_REG_WRITE_I(C_STAT_REG%s_OFFSET/CGN_BITS_PER_REG);\n"%(port_name, port[i_port_count])
            out_file.write(outline)
          else:
            outline = "  -- '--> Pure software register => no ports assigned\n"
            out_file.write(outline)
      if "RESERVED" in port[i_port_name]:
        if ("stat" in port[i_port_type]) and ("in" in port[i_port_dir]) and ("HW" in reglist[reg_index][i_reg_hw_sw]):
          outline = "  status_reg"
          if port[i_port_range_hi] != port[i_port_range_lo]:
            port_range = "(C_STAT_REG%s_OFFSET+%s downto C_STAT_REG%s_OFFSET+%s)"%(port[i_port_count], port[i_port_range_hi], port[i_port_count], port[i_port_range_lo])
            zeros = "(others=>\'0\')"
          else:
            port_range = "(C_STAT_REG%s_OFFSET+%s)"%(port[i_port_count], port[i_port_range_hi])
            zeros = "\'0\'"
          port_range = '{0:<59}'.format(port_range)
          outline = outline + port_range + " <= %s; -- reserved\n"%(zeros)
          out_file.write(outline)
      else:
        if "HW" in reglist[reg_index][i_reg_hw_sw]:
          if "ctrl" in port[i_port_type]:
            if port[i_port_range_hi] != port[i_port_range_lo]:
              port_range = "(C_CTRL_REG%s_OFFSET+%s downto C_CTRL_REG%s_OFFSET+%s)"%(port[i_port_count], port[i_port_range_hi], port[i_port_count], port[i_port_range_lo])
            else:
              port_range = "(C_CTRL_REG%s_OFFSET+%s)"%(port[i_port_count], port[i_port_range_hi])
            port_name = '{0:<{max_port_name_len}}'.format(port[i_port_name]+"_BIT_TRIG_O", max_port_name_len=max_port_name_len+extra_name_len)
            outline = "  %s <= CTRL_BIT_TRIG_I%s;\n"%(port_name, port_range)
            out_file.write(outline)
            port_name = '{0:<{max_port_name_len}}'.format(port[i_port_name]+port[i_port_name_pfx], max_port_name_len=max_port_name_len+extra_name_len)
            outline = "  %s <= CONTROL_REG_I%s;\n"%(port_name, port_range)
            out_file.write(outline)
          else:
            # generate constants
            port_initial = port[i_port_initial]
            nr_of_bits = port[i_port_range_hi]-port[i_port_range_lo]+1
            if port[i_port_range_hi] == port[i_port_range_lo]:
              port_initial = "\'%d\'" % port_initial
            else:
              if nr_of_bits % 4 != 0:
                port_initial = '\"{0:0{bitlen}b}\"'.format(port_initial, bitlen = nr_of_bits)
              else:
                port_initial = 'x\"{0:0{hexlen}X}\"'.format(port_initial, hexlen = nr_of_bits/4)
            if "in" in port[i_port_dir]:
              outline = "  status_reg"
              if port[i_port_range_hi] != port[i_port_range_lo]:
                port_range = "(C_STAT_REG%s_OFFSET+%s downto C_STAT_REG%s_OFFSET+%s)"%(port[i_port_count], port[i_port_range_hi], port[i_port_count], port[i_port_range_lo])
              else:
                port_range = "(C_STAT_REG%s_OFFSET+%s)"%(port[i_port_count], port[i_port_range_hi])
              port_range_left = '{0:<59}'.format(port_range)
              if port[i_port_initial_valid]:
                # write constant
                outline = outline + port_range_left + " <= %s;\n"%port_initial
              else:
                # assign port
                outline = outline + port_range_left + " <= %s;\n"%(port[i_port_name]+port[i_port_name_pfx])
                out_file.write(outline)
                port_name = '{0:<{max_port_name_len}}'.format(port[i_port_name]+"_BIT_TRIG_O", max_port_name_len=69)
                outline = "  %s <= STAT_BIT_TRIG_I%s;\n"%(port_name, port_range)
              out_file.write(outline)
            else:
              if port[i_port_range_hi] != port[i_port_range_lo]:
                port_range = "(C_STAT_REG%s_OFFSET+%s downto C_STAT_REG%s_OFFSET+%s)"%(port[i_port_count], port[i_port_range_hi], port[i_port_count], port[i_port_range_lo])
              else:
                port_range = "(C_STAT_REG%s_OFFSET+%s)"%(port[i_port_count], port[i_port_range_hi])
              port_name = '{0:<69}'.format(port[i_port_name]+port[i_port_name_pfx])
              if port[i_port_initial_valid]:
                # write constant
                outline = "  %s <= %s;\n"%(port_name, port_initial)
              else:
                # assign port
                outline = "  %s <= STATUS_REG_I%s;\n"%(port_name, port_range)
              out_file.write(outline)

  # #########################################################################################################################

  # ###### mpd file functions ###############################################################################################

  def write_mpd_generics(out_file):
    outline = "\n### Generics:\n"
    out_file.write(outline)
    outline = "PARAMETER CGN_BITS_PER_REG = %s, DT = INTEGER\n"%(bits_per_reg)
    out_file.write(outline)
    outline = "PARAMETER CGN_NUM_CTRL_REG = %s, DT = INTEGER\n"%(num_ctrl_regs)
    out_file.write(outline)
    outline = "PARAMETER CGN_NUM_STAT_REG = %s, DT = INTEGER\n"%(num_stat_regs)
    out_file.write(outline)


  def write_mpd_main_ports(out_file):
    # add the main register io ports
    port_name = '{0:<{max_port_name_len}}'.format("STATUS_REG_O", max_port_name_len=max_port_name_len+extra_name_len)
    outline = "PORT %s = \"\", DIR = O, VEC = [CGN_NUM_STAT_REG*CGN_BITS_PER_REG-1:0]\n"%(port_name)
    out_file.write(outline)
    port_name = '{0:<{max_port_name_len}}'.format("STATUS_REG_I", max_port_name_len=max_port_name_len+extra_name_len)
    outline = "PORT %s = \"\", DIR = I, VEC = [CGN_NUM_STAT_REG*CGN_BITS_PER_REG-1:0]\n"%(port_name)
    out_file.write(outline)
    port_name = '{0:<{max_port_name_len}}'.format("STAT_REG_READ_I", max_port_name_len=max_port_name_len+extra_name_len)
    outline = "PORT %s = \"\", DIR = I, VEC = [CGN_NUM_STAT_REG-1:0]\n"%(port_name)
    out_file.write(outline)
    port_name = '{0:<{max_port_name_len}}'.format("STAT_REG_WRITE_I", max_port_name_len=max_port_name_len+extra_name_len)
    outline = "PORT %s = \"\", DIR = I, VEC = [CGN_NUM_STAT_REG-1:0]\n"%(port_name)
    out_file.write(outline)
    port_name = '{0:<{max_port_name_len}}'.format("STAT_BIT_TRIG_I", max_port_name_len=max_port_name_len+extra_name_len)
    outline = "PORT %s = \"\", DIR = I, VEC = [CGN_NUM_STAT_REG*CGN_BITS_PER_REG-1:0]\n"%(port_name)
    out_file.write(outline)
    port_name = '{0:<{max_port_name_len}}'.format("STAT_BYTE_TRIG_I", max_port_name_len=max_port_name_len+extra_name_len)
    outline = "PORT %s = \"\", DIR = I, VEC = [(CGN_NUM_STAT_REG*CGN_BITS_PER_REG/8)-1:0]\n"%(port_name)
    out_file.write(outline)
    port_name = '{0:<{max_port_name_len}}'.format("CONTROL_REG_I", max_port_name_len=max_port_name_len+extra_name_len)
    outline = "PORT %s = \"\", DIR = I, VEC = [CGN_NUM_CTRL_REG*CGN_BITS_PER_REG-1:0]\n"%(port_name)
    out_file.write(outline)
    port_name = '{0:<{max_port_name_len}}'.format("CTRL_REG_READ_I", max_port_name_len=max_port_name_len+extra_name_len)
    outline = "PORT %s = \"\", DIR = I, VEC = [CGN_NUM_CTRL_REG-1:0]\n"%(port_name)
    out_file.write(outline)
    port_name = '{0:<{max_port_name_len}}'.format("CTRL_REG_WRITE_I", max_port_name_len=max_port_name_len+extra_name_len)
    outline = "PORT %s = \"\", DIR = I, VEC = [CGN_NUM_CTRL_REG-1:0]\n"%(port_name)
    out_file.write(outline)
    port_name = '{0:<{max_port_name_len}}'.format("CTRL_BIT_TRIG_I", max_port_name_len=max_port_name_len+extra_name_len)
    outline = "PORT %s = \"\", DIR = I, VEC = [CGN_NUM_CTRL_REG*CGN_BITS_PER_REG-1:0]\n"%(port_name)
    out_file.write(outline)
    port_name = '{0:<{max_port_name_len}}'.format("CTRL_BYTE_TRIG_I", max_port_name_len=max_port_name_len+extra_name_len)
    outline = "PORT %s = \"\", DIR = I, VEC = [(CGN_NUM_CTRL_REG*CGN_BITS_PER_REG/8)-1:0]\n"%(port_name)
    out_file.write(outline)

  def write_mpd_ports(out_file):
    outline = "\n### Ports:\n"
    out_file.write(outline)

    # add all ports
    for register in reglist:
      if "HW" in register[i_reg_hw_sw]:
        if "RESERVED" not in register[i_reg_name]:
          port_name = '{0:<{max_port_name_len}}'.format(register[i_reg_name]+"_BYTE_TRIG_O", max_port_name_len=max_port_name_len+extra_name_len)
          outline = "PORT %s = \"\", DIR = O, VEC = [(CGN_BITS_PER_REG/8)-1:0]\n"%(port_name)
          out_file.write(outline)
        port_name = '{0:<{max_port_name_len}}'.format(register[i_reg_name]+"_REG_READ_O", max_port_name_len=max_port_name_len+extra_name_len)
        outline = "PORT %s = \"\", DIR = O\n"%(port_name)
        out_file.write(outline)
        port_name = '{0:<{max_port_name_len}}'.format(register[i_reg_name]+"_REG_WRITE_O", max_port_name_len=max_port_name_len+extra_name_len)
        outline = "PORT %s = \"\", DIR = O\n"%(port_name)
        out_file.write(outline)

    current_reg = -1
    reg_index = -1
    for port in portlist:
      if current_reg != port[i_port_count]:
        current_reg = port[i_port_count]
        reg_index += 1
      if ("RESERVED" not in port[i_port_name]) and ("HW" in reglist[reg_index][i_reg_hw_sw]):
        port_name = '{0:<{max_port_name_len}}'.format(port[i_port_name]+port[i_port_name_pfx], max_port_name_len=max_port_name_len+extra_name_len)
        if "out" in port[i_port_dir]:
          port_dir = "O"
        else:
          port_dir = "I"
        if port[i_port_range_hi] != port[i_port_range_lo]:
          port_range = ", VEC = [%s:0]"%(port[i_port_range_hi]-port[i_port_range_lo])
        else:
          port_range = ""
        outline = "PORT %s = \"\", DIR = %s%s\n"%(port_name, port_dir, port_range)
        out_file.write(outline)
        if "out" in port[i_port_dir]:
          port_name = '{0:<{max_port_name_len}}'.format(port[i_port_name]+"_BIT_TRIG_O", max_port_name_len=max_port_name_len+extra_name_len)
          outline = "PORT %s = \"\", DIR = %s%s\n"%(port_name, port_dir, port_range)
          out_file.write(outline)

    write_mpd_main_ports(out_file)

  # #########################################################################################################################

  # ###### h file functions #################################################################################################

  def bit_mask(port):
    bit_mask_pattern = ""
    for i in range(31,-1,-1):
      if i in range(port[i_port_range_lo],port[i_port_range_hi]+1):
        bit_mask_pattern = bit_mask_pattern + '1'
      else:
        bit_mask_pattern = bit_mask_pattern + '0'
    bit_mask_pattern = "0x" + "{0:0>8X}".format(int(bit_mask_pattern, 2))
    return bit_mask_pattern

  def write_h_reg_offsets(out_file):
    outline =  "/******************************************************************************/\n"
    outline += "/* definitions                                                                */\n"
    outline += "/******************************************************************************/\n\n"
    out_file.write(outline)
    outline = "/*\n * Register Offsets\n */\n\n"
    out_file.write(outline)
    outline = "#define WD2_CTRL_REG_BASE_OFS   0x%04X\n"%ctrl_base_addr
    out_file.write(outline)
    outline = "#define WD2_STAT_REG_BASE_OFS   0x%04X\n\n"%stat_base_addr
    out_file.write(outline)
    outline = "/* *** Control Registers *** */\n"
    out_file.write(outline)
    last_reg = 0
    for register in reglist:
      if last_reg > int(register[i_reg_num]):
        outline = "\n/* *** Status Registers *** */\n"
        out_file.write(outline)
      last_reg = int(register[i_reg_num])
      reg_name = '{0:<{max_reg_name_len}}'.format(reg_designator(register), max_reg_name_len=reg_des_max_len+extra_name_len)
      outline = "#define %s   %s\n"%(reg_name, register[i_reg_offset])
      out_file.write(outline)

  def write_h_bit_parameters(out_file):
    outline = "\n/*\n * Bit Positions\n */"
    out_file.write(outline)
    current_reg = -1
    reg_index = -1
    for port in portlist:
      if current_reg != port[i_port_count]:
        current_reg = port[i_port_count]
        reg_index += 1
        if "ctrl" in port[i_port_type]:
          outline = "\n\n/* ****** Control"
        else:
          outline = "\n\n/* ****** Status"
        outline = outline + " Register %s [%s]: %s - %s (Default: %s) ****** */\n\n"%(reglist[reg_index][i_reg_num], reglist[reg_index][i_reg_offset], reglist[reg_index][i_reg_name], reglist[reg_index][i_reg_comment], reglist[reg_index][i_reg_default])
        out_file.write(outline)
      if ("RESERVED" not in port[i_port_name]) and not ( ("in" in port[i_port_dir]) and ("stat" in port[i_port_type]) ):
        outline = "/* %s - %s */\n"%(port[i_port_name], port[i_port_comment])
        out_file.write(outline)
        port_name = '{0:<{max_port_name_len}}'.format(bit_reg_designator(port), max_port_name_len=bit_reg_des_max_len+1)
        port_reg  = '{0:>{max_port_name_len}}'.format(reg_designator(reglist[reg_index]), max_port_name_len=reg_des_max_len)
        outline = "#define %s   %s\n"%(port_name, port_reg)
        out_file.write(outline)
        port_name = '{0:<{max_port_name_len}}'.format(bit_msk_designator(port), max_port_name_len=bit_msk_des_max_len)
        bit_pattern = bit_mask(port)
        bit_pattern = '{0:>{max_port_name_len}}'.format(bit_pattern, max_port_name_len=reg_des_max_len)
        outline = "#define %s   %s\n"%(port_name, bit_pattern)
        out_file.write(outline)
        port_name = '{0:<{max_port_name_len}}'.format(bit_ofs_designator(port), max_port_name_len=bit_ofs_des_max_len+1)
        port_ofs  = '{0:>{max_port_name_len}}'.format(port[i_port_range_lo], max_port_name_len=reg_des_max_len)
        outline = "#define %s   %s\n"%(port_name, port_ofs)
        out_file.write(outline)
        if ( port[i_port_initial_valid] and ("stat" in port[i_port_type]) ):
          hex_len = (port[i_port_range_hi]-port[i_port_range_lo]+3)/4
          port_name = '{0:<{max_port_name_len}}'.format(bit_const_designator(port), max_port_name_len=bit_const_des_max_len-1)
          port_const = "0x{0:0{hex_len}X}".format(port[i_port_initial], hex_len=hex_len)
          port_const = '{0:>{max_port_name_len}}'.format(port_const, max_port_name_len=reg_des_max_len)
          outline = "#define %s   %s\n"%(port_name, port_const)
          out_file.write(outline)
        outline = "\n"
        out_file.write(outline)

  def write_h_total_nr_of_regs(out_file):
    outline = "\n/*\n * Number of Registers\n */\n\n"
    out_file.write(outline)
    outline = "#define REG_NR_OF_CTRL_REGS          %d\n"%num_ctrl_regs
    out_file.write(outline)
    outline = "#define REG_NR_OF_STAT_REGS          %d\n\n"%num_stat_regs
    out_file.write(outline)

  def write_h_register_access_declarations(out_file):
    outline = "typedef unsigned int (*wd2_reg_func)(unsigned int, unsigned int, unsigned int, unsigned int);\n\n"
    out_file.write(outline)
    outline = "typedef struct\n{\n  wd2_reg_func  func;\n  unsigned int par;\n} wd2_reg_func_type;\n\n"
    out_file.write(outline)
    outline =  "extern const wd2_reg_func_type  wd2_ctrl_reg_func_list[];\n"
    outline += "extern const wd2_reg_func_type  wd2_stat_reg_func_list[];\n\n"
    out_file.write(outline)

  def write_h_register_mapping_declarations(out_file):
    outline =  "typedef struct {\n  const char * name;\n  const unsigned int reg;\n} wd2_reg_name_entry_type;\n\n"
    outline += "extern const wd2_reg_name_entry_type  wd2_reg_list[];\n\n"
    out_file.write(outline)
    outline =  "typedef struct {\n  const char * name;\n  const unsigned int reg;\n  const unsigned int mask;\n  const unsigned int ofs;\n} wd2_bit_group_entry_type;\n\n"
    outline += "extern const wd2_bit_group_entry_type  wd2_bit_group_list[];\n\n"
    out_file.write(outline)
    outline = "extern const unsigned int ctrl_reg_default[];\n\n"
    out_file.write(outline)

  def write_c_register_access_functions(out_file):
    for rtype in ["ctrl", "stat"]:
      outline = "\nconst wd2_reg_func_type  wd2_%s_reg_func_list[] = {\n"%rtype
      out_file.write(outline)
      prev_reg_ind = -1
      for register in reglist:
        if rtype in register[i_reg_type]:
          if prev_reg_ind != -1:
            outline = ",   /* %s register %d: %s */\n"%(reglist[prev_reg_ind][i_reg_type], reglist[prev_reg_ind][i_reg_num], reglist[prev_reg_ind][i_reg_name])
            out_file.write(outline)
          prev_reg_ind += 1
          outline = '{0:<{max_func_name_len}}'.format("  { %s"%(register[i_reg_func]), max_func_name_len=max_func_name_len+4)
          outline = outline + " , %s }"%(register[i_reg_param])
          out_file.write(outline)
      outline = "    /* %s register %d: %s */\n};\n"%(register[i_reg_type], register[i_reg_num], register[i_reg_name])
      out_file.write(outline)
    outline = "\n"
    out_file.write(outline)

  def write_c_mapping_lists(out_file):
    # list with register mappings
    out_file.write(c_reg_mapping)
    outline = "const wd2_reg_name_entry_type  wd2_reg_list[] = {\n"
    out_file.write(outline)
    for register in reglist:
      outline = '{0:<{max_reg_name_len}}'.format("  { \"%s\""%(register[i_reg_name]), max_reg_name_len=max_reg_name_len+6)
      outline = outline + '{0:<{max_reg_name_len}}'.format(" , %s"%(reg_designator(register)), max_reg_name_len=reg_des_max_len+3)
      outline = outline + " },\n"
      out_file.write(outline)
    outline = '{0:<{max_reg_name_len}}'.format("  { (const char*)0", max_reg_name_len=max_reg_name_len+6)
    outline = outline + '{0:<{max_reg_name_len}}'.format(" , 0", max_reg_name_len=reg_des_max_len+3)
    outline = outline + " }\n};\n"
    out_file.write(outline)

    # list with bit mappings
    out_file.write(c_bit_mapping)
    outline = "const wd2_bit_group_entry_type  wd2_bit_group_list[] = {\n"
    out_file.write(outline)
    for port in portlist:
      if ("RESERVED" not in port[i_port_name]) and not (("stat" in port[i_port_type]) and ("out" in port[i_port_dir])):
        outline = '{0:<{max_port_name_len}}'.format("  { \"%s\""%(port[i_port_name]), max_port_name_len=max_port_name_len+6)
        outline = outline + '{0:<{max_port_name_len}}'.format(" , %s"%(bit_reg_designator(port)), max_port_name_len=bit_reg_des_max_len+3)
        outline = outline + '{0:<{max_port_name_len}}'.format(" , %s"%(bit_msk_designator(port)), max_port_name_len=bit_msk_des_max_len+3)
        outline = outline + '{0:<{max_port_name_len}}'.format(" , %s"%(bit_ofs_designator(port)), max_port_name_len=bit_ofs_des_max_len+3)
        outline = outline + " },\n"
        out_file.write(outline)
    outline = '{0:<{max_port_name_len}}'.format("  { (const char*)0", max_port_name_len=max_port_name_len+6)
    outline = outline + '{0:<{max_port_name_len}}'.format(" , 0", max_port_name_len=bit_reg_des_max_len+3)
    outline = outline + '{0:<{max_port_name_len}}'.format(" , 0", max_port_name_len=bit_msk_des_max_len+3)
    outline = outline + '{0:<{max_port_name_len}}'.format(" , 0", max_port_name_len=bit_ofs_des_max_len+3)
    outline = outline + " }\n};\n"
    out_file.write(outline)

  def write_c_register_defaults(out_file):
    out_file.write(c_default)
    for register in reglist:
      if ("ctrl" in register[i_reg_type]) and ("CRC32" not in register[i_reg_name]):
        if reglist.index(register) == 0:
          outline = "const unsigned int ctrl_reg_default[] = {\n"
        else:
          outline = ",   /* Offset %s */\n"%reg_offset
        reg_offset = register[i_reg_offset]
        out_file.write(outline)
        outline = "  %s"%(register[i_reg_default])
        out_file.write(outline)
    outline = "    /* Offset %s */\n};\n\n"%reg_offset
    out_file.write(outline)

  # #########################################################################################################################

  # ###### py file functions ################################################################################################

  def write_py_reg_offsets(out_file):
    outline = "#\n# Register Offsets\n#\n\n"
    out_file.write(outline)
    outline = "WD2_CTRL_REG_BASE_OFS = 0x%04X\n"%ctrl_base_addr
    out_file.write(outline)
    outline = "WD2_STAT_REG_BASE_OFS = 0x%04X\n\n"%stat_base_addr
    out_file.write(outline)
    outline = "# Control Registers ***\n"
    out_file.write(outline)

    last_reg = 0
    for register in reglist:
      if last_reg > int(register[i_reg_num]):
        outline = "\n# Status Registers \n"
        out_file.write(outline)
      last_reg = int(register[i_reg_num])
      reg_name = '{0:<{max_reg_name_len}}'.format(reg_designator(register), max_reg_name_len=reg_des_max_len+extra_name_len)
      outline = "%s   = %s\n"%(reg_name, register[i_reg_offset])
      out_file.write(outline)

  def write_py_bit_parameters(out_file):
    outline = "\n#\n# Bit Positions\n#"
    out_file.write(outline)

    current_reg = -1
    reg_index = -1
    for port in portlist:
      if current_reg != port[i_port_count]:
        current_reg = port[i_port_count]
        reg_index += 1
        if "ctrl" in port[i_port_type]:
          outline = "\n\n# ****** Control"
        else:
          outline = "\n\n# ****** Status"
        outline = outline + " Register %s [%s]: %s - %s (Default: %s) ******\n\n"%(reglist[reg_index][i_reg_num], reglist[reg_index][i_reg_offset], reglist[reg_index][i_reg_name], reglist[reg_index][i_reg_comment], reglist[reg_index][i_reg_default])
        out_file.write(outline)
      if ("RESERVED" not in port[i_port_name]) and not (("stat" in port[i_port_type]) and ("out" in port[i_port_dir])):
        outline = "# %s - %s\n"%(port[i_port_name], port[i_port_comment].replace("\n","\n# "))
        out_file.write(outline)
        port_name = '{0:<{max_port_name_len}}'.format(bit_reg_designator(port), max_port_name_len=bit_reg_des_max_len+1)
        port_reg  = '{0:>{max_port_name_len}}'.format(reg_designator(reglist[reg_index]), max_port_name_len=reg_des_max_len)
        outline = "%s   = %s\n"%(port_name, port_reg)
        out_file.write(outline)
        port_name = '{0:<{max_port_name_len}}'.format(bit_msk_designator(port), max_port_name_len=bit_msk_des_max_len)
        bit_pattern = bit_mask(port)
        bit_pattern = '{0:>{max_port_name_len}}'.format(bit_pattern, max_port_name_len=max_port_name_len+4)
        outline = "%s   = %s\n"%(port_name, bit_pattern)
        out_file.write(outline)
        port_name = '{0:<{max_port_name_len}}'.format(bit_ofs_designator(port), max_port_name_len=bit_ofs_des_max_len+1)
        port_ofs  = '{0:>{max_port_name_len}}'.format(port[i_port_range_lo], max_port_name_len=max_port_name_len+4)
        outline = "%s   = %s\n"%(port_name, port_ofs)
        out_file.write(outline)
        if ( port[i_port_initial_valid] and ("stat" in port[i_port_type]) ):
          hex_len = (port[i_port_range_hi]-port[i_port_range_lo]+4)/4
          port_name = '{0:<{max_port_name_len}}'.format(bit_const_designator(port), max_port_name_len=bit_const_des_max_len+1)
          port_const = "0x{0:0{hex_len}X}".format(port[i_port_initial], hex_len=hex_len)
          port_const = '{0:>{max_port_name_len}}'.format(port_const, max_port_name_len=reg_des_max_len)
          outline = "%s = %s\n"%(port_name, port_const)
          out_file.write(outline)
        outline = "\n"
        out_file.write(outline)

  def write_py_total_nr_of_regs(out_file):
    outline = "\n\n# Number of Registers\n \n\n"
    out_file.write(outline)
    outline = "REG_NR_OF_CTRL_REGS          = %d\n"%num_ctrl_regs
    out_file.write(outline)
    outline = "REG_NR_OF_STAT_REGS          = %d\n\n"%num_stat_regs
    out_file.write(outline)

  def optimize_comment_string(in_str):
    out_str = in_str.replace("\n"," / ")
    out_str = out_str.replace('"','\\"')
    out_str = out_str.replace("'","\\'")
    return out_str

  def write_py_mapping_lists(out_file):
    # write list with register name-offset mapping
    outline = "\n\n# List of register names and offsets\n \n\n"
    out_file.write(outline)
    outline = "\nwd2_reg_list = [\n"
    out_file.write(outline)
    for register in reglist:
      outline = '{0:<{max_reg_name_len}}'.format("  [ \"%s\""%(register[i_reg_name]), max_reg_name_len=max_reg_name_len+6)
      outline = outline + '{0:<{max_reg_name_len}}'.format(" , %s"%(reg_designator(register)), max_reg_name_len=reg_des_max_len+3)
      outline = outline + '{0:<{max_reg_name_len}}'.format(" , \"%s\""%(register[i_reg_group]), max_reg_name_len=reg_des_max_len+3)
      outline = outline + ", \"%s\""%(optimize_comment_string(register[i_reg_comment]))
      outline = outline + " ],\n"
      out_file.write(outline)
    outline = "]\n"
    out_file.write(outline)
    # write list with bit mappings
    outline = "\n\n# List of bit names and parameters\n \n\n"
    out_file.write(outline)
    outline = "\nwd2_bit_group_list = [\n"
    out_file.write(outline)
    for port in portlist:
      if ("RESERVED" not in port[i_port_name]) and not (("stat" in port[i_port_type]) and ("out" in port[i_port_dir])):
        outline = '{0:<{max_port_name_len}}'.format("  [ \"%s\""%(port[i_port_name]), max_port_name_len=max_port_name_len+6)
        outline = outline + '{0:<{max_port_name_len}}'.format(" , %s"%(bit_reg_designator(port)), max_port_name_len=bit_reg_des_max_len+3)
        outline = outline + '{0:<{max_port_name_len}}'.format(" , %s"%(bit_msk_designator(port)), max_port_name_len=bit_msk_des_max_len+3)
        outline = outline + '{0:<{max_port_name_len}}'.format(" , %s"%(bit_ofs_designator(port)), max_port_name_len=bit_ofs_des_max_len+3)
        outline = outline + ", \"%s\""%(optimize_comment_string(port[i_port_comment]))
        outline = outline + " ],\n"
        out_file.write(outline)
    outline = "]\n"
    out_file.write(outline)

  def write_py_register_defaults(out_file):
    outline = "\n#\n# Control Register Defaults\n#\n\n"
    out_file.write(outline)
    for register in reglist:
      if ("ctrl" in register[i_reg_type]) and ("CRC32" not in register[i_reg_name]):
        if reglist.index(register) == 0:
          outline =    "ctrl_reg_default = ["
        else:
          outline = ",   # Offset %s \n                    "%reg_offset
        reg_offset = register[i_reg_offset]
        out_file.write(outline)
        outline = "%s"%(register[i_reg_default])
        out_file.write(outline)
    outline = "]   # Offset %s \n\n"%reg_offset
    out_file.write(outline)

  # #########################################################################################################################

  # ###### html file ########################################################################################################

  def write_html_reg_list(out_file, reg_type):
    if reg_type == "ctrl":
      outline = "  <a id=\"" + reg_type + "_register_overview\"><h2>Control Registers</h2></a>\n"
    else:
      outline = "  <a id=\"" + reg_type + "_register_overview\"><h2>Status Registers</h2></a>\n"
    out_file.write(outline)
    outline = "  <table>\n    <tbody>\n"
    out_file.write(outline)
    outline = "      <tr class=\"reg_header\">\n        <th>#</th>\n        <th>Offset</th>\n        <th>HW/SW</th>\n        <th>Initial</th>\n        <th>Name</th>\n        <th>Comment</th>\n        <th>Group</th>\n      </tr>\n"
    out_file.write(outline)
    last_reg = 0
    for register in reglist:
      if reg_type == register[i_reg_type]:
        outline = "      <tr class=\"reg_list\">\n"
        out_file.write(outline)
        outline = "        <td style=\"text-align:right\" ><a href=\"#%s_register_%d\" id=\"%s_reg_list_%d\">%d</a></td>\n"%(register[i_reg_type], register[i_reg_num], register[i_reg_type], register[i_reg_num], register[i_reg_num])
        out_file.write(outline)
        outline = "        <td style=\"text-align:right\" ><a href=\"#%s_register_%d\">%s</a></td>\n"%(register[i_reg_type], register[i_reg_num], register[i_reg_offset])
        out_file.write(outline)
        outline = "        <td style=\"text-align:center\"><a href=\"#%s_register_%d\">%s</a></td>\n"%(register[i_reg_type], register[i_reg_num], register[i_reg_hw_sw])
        out_file.write(outline)
        outline = "        <td style=\"text-align:right\" ><a href=\"#%s_register_%d\">%s</a></td>\n"%(register[i_reg_type], register[i_reg_num], register[i_reg_default])
        out_file.write(outline)
        outline = "        <td style=\"text-align:left\"  ><a href=\"#%s_register_%d\">%s</a></td>\n"%(register[i_reg_type], register[i_reg_num], register[i_reg_name])
        out_file.write(outline)
        outline = "        <td style=\"text-align:left\"  ><a href=\"#%s_register_%d\">%s</a></td>\n"%(register[i_reg_type], register[i_reg_num], register[i_reg_comment])
        out_file.write(outline)
        outline = "        <td style=\"text-align:left\"  ><a href=\"#%s_register_%d\">%s</a></td>\n"%(register[i_reg_type], register[i_reg_num], register[i_reg_group])
        out_file.write(outline)
        outline = "      </tr>\n"
        out_file.write(outline)
    outline = "      <tr class=\"back_to_overview\">"
    out_file.write(outline)
    if reg_type == "ctrl":
      outline = "        <td class=\"back_to_overview\" colspan=\"7\"><a href=\"#%s_register_overview\">Control Registers Top</a></a></td>"%(reg_type)
    else:
      outline = "        <td class=\"back_to_overview\" colspan=\"7\"><a href=\"#%s_register_overview\">Status Registers Top</a></a></td>"%(reg_type)
    out_file.write(outline)
    outline = "      </tr>\n    </tbody>\n  </table>"
    out_file.write(outline)

  def write_html_reg_contents(out_file):
    outline = "  <table>\n    <tbody>\n"
    out_file.write(outline)
    current_reg = -1
    reg_index = -1
    for port in portlist:
      if current_reg != port[i_port_count]:
        if current_reg != -1:
          outline = "      <tr class=\"back_to_overview\">\n"
          out_file.write(outline)
          outline = "        <td class=\"back_to_overview\" colspan=\"7\"><a href=\"#%s_reg_list_%d\">Register Overview</a></td>\n      </tr>\n"%(reglist[reg_index][i_reg_type], reglist[reg_index][i_reg_num])
          out_file.write(outline)
        current_reg = port[i_port_count]
        reg_index += 1
        outline = "      <tr class=\"reg_table_title\">\n"
        out_file.write(outline)
        outline = "        <td class=\"reg_table_title\" colspan=\"7\">"
        out_file.write(outline)
        if "ctrl" in port[i_port_type]:
          outline = "<a id=\"ctrl_register_%s\">"%(reglist[reg_index][i_reg_num])
          out_file.write(outline)
          outline = "<h3>Control Register %s</h3>"%(reglist[reg_index][i_reg_name])
          out_file.write(outline)
        else:
          outline = "<a id=\"stat_register_%s\">"%(reglist[reg_index][i_reg_num])
          out_file.write(outline)
          outline = "<h3>Status Register %s</h3>"%(reglist[reg_index][i_reg_name])
          out_file.write(outline)
        outline = "</a></td>\n      </tr>\n"
        out_file.write(outline)
        outline = "      <tr class=\"reg_header\">\n        <th>#</th>\n        <th>Offset</th>\n        <th>HW/SW</th>\n        <th>Bit</th>\n        <th>Initial</th>\n        <th>Name</th>\n        <th>Comment</th>\n      </tr>\n"
        out_file.write(outline)
        outline = "      <tr class=\"reg_name\">\n"
        out_file.write(outline)
        outline = "        <td style=\"text-align:center\">%s</td>\n"%(reglist[reg_index][i_reg_num])
        out_file.write(outline)
        outline = "        <td style=\"text-align:right\" >%s</td>\n"%(reglist[reg_index][i_reg_offset])
        out_file.write(outline)
        outline = "        <td style=\"text-align:center\">%s</td>\n"%(reglist[reg_index][i_reg_hw_sw])
        out_file.write(outline)
        outline = "        <td style=\"text-align:center\"></td>\n"
        out_file.write(outline)
        outline = "        <td style=\"text-align:right\" >%s</td>\n"%(reglist[reg_index][i_reg_default])
        out_file.write(outline)
        outline = "        <td style=\"text-align:left\"  >%s</td>\n"%(reglist[reg_index][i_reg_name])
        out_file.write(outline)
        outline = "        <td style=\"text-align:left\"  >%s</td>\n"%(reglist[reg_index][i_reg_comment])
        out_file.write(outline)
        outline = "      </tr>\n"
        out_file.write(outline)
      if "out" in port[i_port_dir]:
        outline = "      <tr class=\"reg_content\">\n"
        out_file.write(outline)
        outline = "        <td style=\"text-align:center\"></td>\n"
        out_file.write(outline)
        outline = "        <td style=\"text-align:right\" ></td>\n"
        out_file.write(outline)
        outline = "        <td style=\"text-align:center\"></td>\n"
        out_file.write(outline)
        if port[i_port_range_hi] == port[i_port_range_lo]:
          outline = "        <td style=\"text-align:center\">%d</td>\n"%(port[i_port_range_hi])
        else:
          outline = "        <td style=\"text-align:center\">%d:%d</td>\n"%(port[i_port_range_hi], port[i_port_range_lo])
        out_file.write(outline)
        port_initial = port[i_port_initial]
        nr_of_bits = port[i_port_range_hi]-port[i_port_range_lo]+1
        if "RESERVED" in port[i_port_name] and port[i_port_initial] == 0:
          port_initial = "0"
        elif nr_of_bits % 4 != 0:
          port_initial = '0b{0:0{bitlen}b}'.format(port_initial, bitlen = nr_of_bits)
        else:
          port_initial = '0x{0:0{hexlen}X}'.format(port_initial, hexlen = nr_of_bits/4)
        outline = "        <td style=\"text-align:right\" >%s</td>\n"%port_initial
        out_file.write(outline)
        outline = "        <td style=\"text-align:left\"  >%s</td>\n"%(port[i_port_name])
        out_file.write(outline)
        if "RESERVED" in port[i_port_name]:
          outline = "        <td style=\"text-align:left\"  ></td>\n"
        else:
          outline = "        <td style=\"text-align:left\"  >%s</td>\n"%("<br>".join(port[i_port_comment].split("\n")))
        out_file.write(outline)
        outline = "      </tr>\n"
        out_file.write(outline)
    outline = "      <tr class=\"back_to_overview\">\n"
    out_file.write(outline)
    outline = "        <td class=\"back_to_overview\" colspan=\"7\"><a href=\"#%s_reg_list_%d\">Register Overview</a></td>\n      </tr>\n"%(reglist[reg_index][i_reg_type], reglist[reg_index][i_reg_num])
    out_file.write(outline)
    outline = "\n    </tbody>\n"
    out_file.write(outline)
    outline = "\n  </table>\n"
    out_file.write(outline)

###################################################################################################
# File and Path Checks/Settings
###################################################################################################

  outdir = indir + os.sep + "script_output"
  if not os.path.isdir(outdir):
    os.makedirs(outdir)

  vhd_outfile    = "util_register_map.vhd"
  c_outfile      = "register_map_wd2_v%d.c"%(reg_layout_version)
  h_outfile      = "register_map_wd2_v%d.h"%(reg_layout_version)
  py_outfile     = "wd2_regs_v%d.py"%(reg_layout_version)
  mpd_outfile    = "util_register_map_v2_1_0.mpd"
  pao_outfile    = "util_register_map_v2_1_0.pao"
  css_outfile    = "wd2_register_map.css"
  html_outfile   = "wd2_register_map.html"
  html_outfile_v = "wd2_register_map_v%d.html"%(reg_layout_version)

  # Paths relative to GIT root directory
  pcore_name = "util_register_map_v%d_00_a"%(reg_layout_version)
  pcore_dir = "wd2_sys_ctrl/wd2_xps_hw/common/pcores/%s"%(pcore_name)
  pcore_dir = pcore_dir.replace('/', os.sep)
  pcore_vhdl_dir = pcore_dir + os.sep + "hdl" + os.sep + "vhdl"
  pcore_data_dir = pcore_dir + os.sep + "data"
  pcore_doc_dir  = pcore_dir + os.sep + "doc"

  c_h_dir_ub = "wd2_sys_ctrl/wd2_xsdk_sw/src/common"
  c_h_dir_ub = c_h_dir_ub.replace('/', os.sep)

  sw_doc_dir = "sw/doc"
  sw_doc_dir = sw_doc_dir.replace('/', os.sep)

  sw_include_dir = "sw/include"
  sw_include_dir = sw_include_dir.replace('/', os.sep)

  py_dir = "sw/scripts"
  py_dir = py_dir.replace('/', os.sep)

  doc_dir = "doc/fw/wdb"
  doc_dir = doc_dir.replace('/', os.sep)

  # #########################################################################################################################

  def write_vhd():
    outfile = vhd_outfile
    out_file = file(outdir+os.sep+outfile,'w')
    out_file.write(vhd_header_start)
    outline =  "--  Created : %s\n--\n"%(datetime.now().strftime('%d.%m.%Y %H:%M:%S'))
    outline += "--  Register Layout Version : %d\n--\n"%(reg_layout_version)
    out_file.write(outline)
    out_file.write(vhd_header_end)

    outline = "\nlibrary ieee;\nuse ieee.std_logic_1164.all;\n\n"
    out_file.write(outline)

    write_vhd_entity(out_file)

    outline = "architecture structural of util_register_map is\n\n"
    out_file.write(outline)

    write_vhd_constants(out_file)
    write_vhd_int_signals(out_file)

    outline = "\nbegin\n"
    out_file.write(outline)

    write_vhd_signal_mappings(out_file)

    outline = "\nend architecture structural;"
    out_file.write(outline)
    out_file.close()

  def write_mpd():
    outfile = mpd_outfile
    out_file = file(outdir+os.sep+outfile,'w')
    out_file.write(mpd_header_start)
    outline =  "##  Created :  %s\n##\n"%(datetime.now().strftime('%d.%m.%Y %H:%M:%S'))
    outline += "##  Register Layout Version :  %d\n##\n"%(reg_layout_version)
    out_file.write(outline)
    out_file.write(mpd_header_end)

    write_mpd_generics(out_file)

    write_mpd_ports(out_file)

    out_file.write(mpd_end)
    out_file.close()

  def write_pao():
    outfile = pao_outfile
    out_file = file(outdir+os.sep+outfile,'w')
    vhd_outflie_noext = vhd_outfile.split('.')[0]
    outline = "lib %s %s vhdl\n"%(pcore_name, vhd_outflie_noext)
    out_file.write(outline)
    out_file.close()

  def write_c():
    outfile = c_outfile
    out_file = file(outdir+os.sep+outfile,'w')
    out_file.write(c_h_header_start)
    outline =  " *  Created :  %s\n *\n"%(datetime.now().strftime('%d.%m.%Y %H:%M:%S'))
    outline += " *  Register Layout Version :  %d\n *\n"%(reg_layout_version)
    out_file.write(outline)
    out_file.write(c_h_header_end)
    outline =  "#include \"register_map_wd2_v%d.h\"\n\n"%(reg_layout_version)
    outline += "#ifdef WD2_REGISTER_MAP_V%d\n\n"%(reg_layout_version)
    out_file.write(outline)

    outline = "#ifndef WD2_DONT_INCLUDE_REG_ACCESS_VARS\n"
    out_file.write(outline)

    write_c_register_access_functions(out_file)

    outline = "#endif /* WD2_DONT_INCLUDE_REG_ACCESS_VARS */\n\n"
    out_file.write(outline)

    outline = "#ifndef WD2_DONT_INCLUDE_VARS\n"
    out_file.write(outline)

    write_c_mapping_lists(out_file)
    write_c_register_defaults(out_file)

    outline =  "#endif /* WD2_DONT_INCLUDE_VARS */\n\n"
    outline += "#endif /* WD2_REGISTER_MAP_V%d */\n\n"%(reg_layout_version)
    outline += "/******************************************************************************/\n"
    out_file.write(outline)

    out_file.close()

  def write_h():
    outfile = h_outfile
    out_file = file(outdir+os.sep+outfile,'w')
    out_file.write(c_h_header_start)
    outline =  " *  Created :  %s\n *\n"%(datetime.now().strftime('%d.%m.%Y %H:%M:%S'))
    outline += " *  Register Layout Version :  %d\n *\n"%(reg_layout_version)
    out_file.write(outline)
    out_file.write(c_h_header_end)

    # header guards top
    outline =  "#ifndef __REGISTER_MAP_V%d_H__\n#define __REGISTER_MAP_V%d_H__\n\n"%(reg_layout_version, reg_layout_version)
    # include main register map header
    outline += "#include \"register_map_wd2.h\"\n\n"
    # include main register map header
    outline += "#ifdef WD2_REGISTER_MAP_V%d\n\n"%(reg_layout_version)
    # include other files
    outline += "#ifndef WD2_DONT_INCLUDE_REG_ACCESS_VARS\n#include \"drv_plb_wd2_reg_bank.h\"\n#endif\n\n"
    out_file.write(outline)

    write_h_reg_offsets(out_file)
    write_h_bit_parameters(out_file)
    write_h_total_nr_of_regs(out_file)

    outline = "#ifndef WD2_DONT_INCLUDE_REG_ACCESS_VARS\n\n"
    out_file.write(outline)

    write_h_register_access_declarations(out_file)

    outline =  "#endif /* WD2_DONT_INCLUDE_REG_ACCESS_VARS */\n\n"
    outline += "#ifndef WD2_DONT_INCLUDE_VARS\n\n"
    out_file.write(outline)

    write_h_register_mapping_declarations(out_file)

    outline =  "#endif /* WD2_DONT_INCLUDE_VARS */\n\n"
    outline += "#endif /* WD2_REGISTER_MAP_V%d */\n\n"%(reg_layout_version)
    outline += "/******************************************************************************/\n\n"
    outline += "#endif /* __REGISTER_MAP_V%d_H__ */\n\n"%(reg_layout_version)
    outline += "/******************************************************************************/\n"
    out_file.write(outline)

    out_file.close()

  def write_py():
    outfile = py_outfile
    out_file = file(outdir+os.sep+outfile,'w')
    out_file.write(py_header_start)
    outline =  "#  Created :  %s\n#\n"%(datetime.now().strftime('%d.%m.%Y %H:%M:%S'))
    outline += "#  Register Layout Version :  %d\n#\n"%(reg_layout_version)
    out_file.write(outline)
    out_file.write(py_header_end)

    write_py_reg_offsets(out_file)
    write_py_bit_parameters(out_file)
    write_py_total_nr_of_regs(out_file)
    write_py_mapping_lists(out_file)
    write_py_register_defaults(out_file)

    out_file.write(py_end)
    out_file.close()

  def write_html():
    outfile = html_outfile
    out_file = file(outdir+os.sep+outfile,'w')

    outline =  "<!DOCTYPE html>\n<!-- Created :   %s -->\n<html>\n\n"%(datetime.now().strftime('%d.%m.%Y %H:%M:%S'))
    outline += "<head>\n"
    outline += "  <link href=\"wd2_register_map.css\" rel=\"stylesheet\" type=\"text/css\">\n"
    outline += "  <title>WD2 Register Map V%d</title>"%(reg_layout_version)
    outline += "</head>\n\n"
    out_file.write(outline)
    outline =  "<body>\n"
    outline += "  <h1>WD2 Register Map Version %d</h1>"%(reg_layout_version)
    outline += "  Generated : %s"%(datetime.now().strftime('%d.%m.%Y %H:%M:%S'))
    out_file.write(outline)

    write_html_reg_list(out_file, "ctrl")
    outline = "  <br>\n  <hr>\n"
    out_file.write(outline)
    write_html_reg_list(out_file, "stat")

    outline = "  <br>\n  <br>\n  <hr>\n  <hr>\n  <h2>Register Contents</h2>\n"
    out_file.write(outline)

    write_html_reg_contents(out_file)

    outline = "\n  <br>\n  Generated :   %s\n\n</body>\n\n</html>\n"%datetime.now().strftime('%d.%m.%Y %H:%M:%S')
    out_file.write(outline)

    out_file.close()

  def write_css():
    outfile = css_outfile
    out_file = file(outdir+os.sep+outfile,'w')
    out_file.write(css_content)
    out_file.close()

  # #########################################################################################################################

  print "Writing temporary output files:"
  print "Writing " + outdir + os.sep + vhd_outfile +"..."
  write_vhd()
  print "Writing " + outdir + os.sep + mpd_outfile + "..."
  write_mpd()
  print "Writing " + outdir + os.sep + pao_outfile + "..."
  write_pao()
  print "Writing " + outdir + os.sep + css_outfile + "..."
  write_css()
  print "Writing " + outdir + os.sep + html_outfile + "..."
  write_html()
  print "Writing " + outdir + os.sep + c_outfile + "..."
  write_c()
  print "Writing " + outdir + os.sep + h_outfile + "..."
  write_h()
  print "Writing " + outdir + os.sep + py_outfile + "..."
  write_py()

  # Copy files to target directories?
  do_file_copy = raw_input("\nCopy files to target directories? ([y]/n): ")
  if (not do_file_copy) or (do_file_copy == 'y') or (do_file_copy == "yes"):
    do_file_copy = 'y'
  else:
    do_file_copy = 'n'

  # Copy files to target directories!
  if do_file_copy == 'y':
    print "\n"
    #git_root = find_vcs_root(os.getcwd()) # to find git root with current working directory
    git_root = find_vcs_root(indir)
    if git_root == None:
      sys.exit("Error: GIT root not found.\n       Make sure .xlsx source is in a WDB GIT repository directory")
    else:
      print "Performing filecopy to target directories (GIT root = %s):"%(git_root)
      print "Copy pcore files ..."
      print "---\n%s :"%(vhd_outfile)
      copy_file_to_target(outdir + os.sep + vhd_outfile,  git_root + os.sep + pcore_vhdl_dir + os.sep + vhd_outfile)
      print "---\n%s :"%(mpd_outfile)
      copy_file_to_target(outdir + os.sep + mpd_outfile,  git_root + os.sep + pcore_data_dir + os.sep + mpd_outfile)
      print "---\n%s :"%(pao_outfile)
      copy_file_to_target(outdir + os.sep + pao_outfile,  git_root + os.sep + pcore_data_dir + os.sep + pao_outfile)
      print "---\n%s :"%(css_outfile)
      copy_file_to_target(outdir + os.sep + css_outfile,  git_root + os.sep + pcore_doc_dir + os.sep + css_outfile)
      print "---\n%s :"%(html_outfile)
      copy_file_to_target(outdir + os.sep + html_outfile, git_root + os.sep + pcore_doc_dir + os.sep + html_outfile)
      print "\nCopy Microblaze baremetal softwre files ..."
      print "---\n%s :"%(c_outfile)
      copy_file_to_target(outdir + os.sep + c_outfile, git_root + os.sep + c_h_dir_ub + os.sep + c_outfile)
      print "---\n%s :"%(h_outfile)
      copy_file_to_target(outdir + os.sep + h_outfile, git_root + os.sep + c_h_dir_ub + os.sep + h_outfile)
      print "\n--> Remember to update the .mhs with the correct register map revision <--\n"
    parent_git_root = find_vcs_root("%s/.."%git_root)
    if parent_git_root == None:
      sys.exit("Error: Parent GIT root not found.\n       Make sure .xlsx source is in a wavedaq_main GIT repository directory")
    else:
      print "\nCopy python script files ..."
      print "---\n%s :"%(py_outfile)
      copy_file_to_target(outdir + os.sep + py_outfile, parent_git_root + os.sep + py_dir + os.sep + py_outfile)
      print "\nCopy software files ..."
      print "---\n%s :"%(c_outfile)
      copy_file_to_target(outdir + os.sep + c_outfile, parent_git_root + os.sep + sw_include_dir + os.sep + c_outfile)
      print "---\n%s :"%(h_outfile)
      copy_file_to_target(outdir + os.sep + h_outfile, parent_git_root + os.sep + sw_include_dir + os.sep + h_outfile)
      print "\nCopy documentation files to software repository ..."
      print "---\n%s :"%(css_outfile)
      copy_file_to_target(outdir + os.sep + css_outfile,  parent_git_root + os.sep + sw_doc_dir + os.sep + css_outfile)
      print "---\n%s :"%(html_outfile)
      copy_file_to_target(outdir + os.sep + html_outfile, parent_git_root + os.sep + sw_doc_dir + os.sep + html_outfile_v)
      print "\nCopy documentation files to documentation repository ..."
      print "---\n%s :"%(css_outfile)
      copy_file_to_target(outdir + os.sep + css_outfile,  parent_git_root + os.sep + doc_dir + os.sep + css_outfile)
      print "---\n%s :"%(html_outfile)
      copy_file_to_target(outdir + os.sep + html_outfile, parent_git_root + os.sep + doc_dir + os.sep + html_outfile_v)
      print "\nFilecopy completed !!!\n"
  else:
    print "Target files not updated"
