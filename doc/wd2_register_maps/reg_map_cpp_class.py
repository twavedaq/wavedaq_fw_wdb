#! /usr/bin/env python
if __name__ == "__main__":
  import sys, os, importlib
  #import re
  from datetime import datetime
  from shutil import copyfile

  ### Usage:
  ###
  ### reg_map_cpp_class.py <incl versions>
  ###
  ### <incl versions> : register layout versions to include
  ###                   value examples:
  ###                   8 10 15 = versions 8, 10 and 15
  ###                   8:10 = versions 8, 9, and 10
  ###                   8:10 15 = versions 8, 9, 10 and 15
  ###

  ###############################################################
  ###
  ### This script generates a .h  software file containing a
  ### class to be used for WDB register access. The register
  ### parameters are taken from scripts generated for each
  ### class by xlsx2regconst.py. Note that these scripts all
  ### have tobe present when this script is run.
  ###
  ###############################################################

  ## Win 10: Python 2.7.12 | Anaconda 4.2.0
  ## RH7:    Python 2.7.5

  sys.path.append('./script_output')

  bit_name_idx = 0
  reg_addr_idx = 1
  bit_mask_idx = 2
  bit_offs_idx = 3
  bit_txt_idx  = 4

  reg_name_idx = 0
  reg_offs_idx = 1
  reg_grp_idx  = 2
  reg_txt_idx  = 3

  # interprete arguments to generate version list
  versions = []
  for v_in in sys.argv[1:]:
    if v_in.isdigit():
      versions.append(int(v_in))
    else:
      range_in = v_in.split(':')
      if ( len(range_in)==2 and range_in[0].isdigit() and range_in[1].isdigit() ):
        range_in = map(int, range_in)
        versions = versions+range( min(range_in) , max(range_in)+1 )
      else:
        sys.exit("Invalid arguments")
  versions.sort()
  print "\n===========================================\n"
  print "\nCreating class for register layout versions :  %s\n"%(", ".join(map(str,versions)))

  # check if files exist for all versions
  file_list = []
  file_ext = ".py"
  for vers in versions:
    in_file = "wd2_regs_v%d"%(vers)
    file_list.append(in_file)

  path = "script_output"
  path = path.replace('/', os.sep)

  modules = []
  for in_file in file_list:
    if os.path.isfile(path + os.sep + in_file + file_ext):
      infile_module = importlib.import_module(in_file)
      modules.append(infile_module)
    else:
      sys.exit("Error: file %s not found"%(in_file))



  # create class file output (copy and adapt from other script)
  class_header = """//
//  WDBReg.h
//
//  WaveDAQ Register Access Class
//
//  This file is generated automatically, please do not edit!
//\n"""

  class_start = """class WDBREG {

private:

   unsigned int mVersion;
   // virtual bit functions, must be implemented in derived class
   virtual unsigned int BitExtractStatus(unsigned int reg, unsigned int mask, unsigned int ofs) = 0;
   virtual unsigned int BitExtractControl(unsigned int reg, unsigned int mask, unsigned int ofs) = 0;
   virtual void SetRegMask(unsigned int reg, unsigned int mask, unsigned int ofs, unsigned int v) = 0;
   virtual std::vector<unsigned int> ReadUDP(unsigned int ofs, unsigned int nReg) = 0;

public:

   // constructor
   WDBREG() {
     this->mVersion = 0;
   };

   void SetVersion(unsigned int version) {
      mVersion = version;
   }

   unsigned int GetVersion() {
      return this->mVersion;
   }\n\n"""

  class_end = """\n};\n\n//--------------------------------------------------------------------

#endif /* defined(__wdbreg_h__) */\n"""

  # #########################################################################################################################

  indir =  os.path.abspath('.')

  outdir = indir + os.sep + "script_output"
  if not os.path.isdir(outdir):
    os.makedirs(outdir)

  class_outfile = "WDBReg.h"

  class_dir = "sw/include"
  class_dir = class_dir.replace('/', os.sep)

  # #########################################################################################################################

  # ###### class file functions #############################################################################################

  exception = "std::invalid_argument"

  grp_name_idx   = 0
  grp_offset_idx = 1
  grp_len_idx    = 2

  def underscore_to_camelcase(name):
    return ''.join(w.capitalize() for w in name.split('_'))

  def get_nr_of_ctrl_regs(modules):
    return [module.REG_NR_OF_CTRL_REGS for module in modules]

  def get_nr_of_stat_regs(modules):
    return [module.REG_NR_OF_STAT_REGS for module in modules]

  def get_bit_names(module):
    return [item[bit_name_idx] for item in module.wd2_bit_group_list]

  def get_all_bit_names(modules):
    bit_names = []
    for module in modules:
      bit_names += get_bit_names(module)
    bit_names = list(set(bit_names))
    bit_names.sort()
    return bit_names

  def get_group_names(groups):
    return [item[grp_name_idx] for item in groups]

  def get_all_group_names(all_vers_groups):
    grp_names = []
    for groups in all_vers_groups:
      grp_names += get_group_names(groups)
    grp_names = list(set(grp_names))
    grp_names.sort()
    return grp_names

  def get_reg_names(module):
    return [item[bit_name_idx] for item in module.wd2_reg_list]

  def get_all_reg_names(modules):
    reg_names = []
    for module in modules:
      reg_names += get_reg_names(module)
    reg_names = list(set(reg_names))
    reg_names.sort()
    return reg_names

  def get_reg_groups(module):
    prev_group = "None"
    groups = []
    grp_len = 1
    for reg_item in module.wd2_reg_list:
      if reg_item[reg_grp_idx] != prev_group:
        if prev_group != "None":
          groups.append([prev_group, grp_offs, grp_len])
        grp_offs = reg_item[reg_addr_idx]
        grp_len = 1
        prev_group = reg_item[reg_grp_idx]
      else:
        grp_len += 1
    return groups

  def get_all_reg_groups(modules):
    groups = []
    for module in modules:
      groups.append(get_reg_groups(module))
    return groups

  def get_bit_entry(module, bit_name):
    for bit_item in module.wd2_bit_group_list:
      if bit_item[0] == bit_name:
        return bit_item
    return []
    # # Equivalent code:
    # return [s for s in module.wd2_bit_group_list if bit_name in s][0]
    # # Failed with "list index out of range" when bit name was not found

  def get_bit_entries(modules, bit_name):
    return map(lambda x: get_bit_entry(x, bit_name), modules)

  def get_group_entry(groups, grp_name):
    for grp_item in groups:
      if grp_item[0] == grp_name:
        return grp_item
    return []
    # # Equivalent code:
    # return [s for s in groups if grp_name in s][0]
    # # Failed with "list index out of range" when bit name was not found

  def get_group_entries(groups, grp_name):
    return map(lambda x: get_group_entry(x, grp_name), groups)

  def entries_are_equal(in_list):
    return in_list.count(in_list[0]) == len(in_list)

  def is_control_reg(bit_info):
    return bit_info[reg_addr_idx] >= 0x1000
    # This function is programmed statically. However, if status or control register boundaries
    # are changed in certain register layout versions, it has to be made dynamic

  def has_control_regs(bit_infos):
    for idx in range(len(versions)):
      if (bit_infos[idx] != []) and is_control_reg(bit_infos[idx]):
        return True
    return False

  def has_status_regs(bit_infos):
    for idx in range(len(versions)):
      if (bit_infos[idx] != []) and not is_control_reg(bit_infos[idx]):
        return True
    return False

  def gen_bit_access_function_cases(bit_infos, master_function, func_name):
    switch_str = "      switch(this->mVersion)\n      {\n"
    for idx in range(len(versions)):
      if bit_infos[idx] != []:
        switch_str += "         case %d:\n"%(versions[idx])
        if (idx == (len(versions)-1)) or (bit_infos[idx] != bit_infos[idx+1]):
          switch_str += "            // %s\n"%(bit_infos[idx][bit_txt_idx])
          switch_str += "            return %s"%(master_function)
          if master_function == "BitExtract":
            if is_control_reg(bit_infos[idx]): switch_str += "Control"
            else:                              switch_str += "Status"
          switch_str += "("
          switch_str += "0x%04X, "%(bit_infos[idx][reg_addr_idx])
          switch_str += "0x%08X, "%(bit_infos[idx][bit_mask_idx])
          switch_str += "%d"%(bit_infos[idx][bit_offs_idx])
          if master_function == "SetRegMask":
            switch_str += ", value"
          switch_str += ");\n"
    switch_str += "         default:\n"
    switch_str += "            throw %s(\"Function %s() not defined for this board version\");\n"%(exception, func_name)
    switch_str += "      };\n"
    return switch_str

  def gen_location_function_cases(bit_infos, func_name):
    switch_str = "      switch(this->mVersion)\n      {\n"
    for idx in range(len(versions)):
      if bit_infos[idx] != []:
        switch_str += "         case %d:\n"%(versions[idx])
        if (idx == (len(versions)-1)) or (bit_infos[idx] != bit_infos[idx+1]):
          switch_str += "            // %s\n"%(bit_infos[idx][bit_txt_idx])
          switch_str += "            if(BitMask) *BitMask = 0x%08X;\n"%(bit_infos[idx][bit_mask_idx])
          switch_str += "            if(BitOfs) *BitOfs = %d;\n"%(bit_infos[idx][bit_offs_idx])
          switch_str += "            return 0x%04X;\n"%(bit_infos[idx][reg_addr_idx])
    switch_str += "         default:\n"
    switch_str += "            throw %s(\"Function %s() not defined for this board version\");\n"%(exception, func_name)
    switch_str += "      };\n"
    return switch_str

  def gen_bit_function(bit_infos, bit_name):
    name = underscore_to_camelcase(bit_name)
    func_str = "   // Bit(s) %s\n"%(bit_name)
    func_name = "Get%s"%(name)
    func_str += "   unsigned int %s()\n   {\n"%(func_name)
    func_str += gen_bit_access_function_cases(bit_infos, "BitExtract", func_name)
    func_str += "   };\n"
    if has_control_regs(bit_infos):
      func_name = "Set%s"%(name)
      func_str += "   void %s(unsigned int value)\n   {\n"%(func_name)
      func_str += gen_bit_access_function_cases(bit_infos, "SetRegMask", func_name)
      func_str += "   };\n"
    func_name = "Get%sLoc"%(name)
    func_str += "   unsigned int %s(unsigned int *BitMask=0, unsigned int *BitOfs=0)\n   {\n"%(func_name)
    func_str += gen_location_function_cases(bit_infos, func_name)
    func_str += "   };\n"
    func_str += "\n"
    return func_str

  def gen_group_function_cases(grp_infos, func_name):
    switch_str = "      switch(this->mVersion)\n      {\n"
    for idx in range(len(versions)):
      if grp_infos[idx] != []:
        switch_str += "         case %d:\n"%(versions[idx])
        if (idx == (len(versions)-1)) or (grp_infos[idx] != grp_infos[idx+1]):
          switch_str += "            if(GrpLen) *GrpLen = %d;\n"%(grp_infos[idx][grp_len_idx])
          switch_str += "            return 0x%04X;\n"%(grp_infos[idx][grp_offset_idx])
    switch_str += "         default:\n"
    switch_str += "            throw %s(\"Function %s() not defined for this board version\");\n"%(exception, func_name)
    switch_str += "      };\n"
    return switch_str

  def gen_group_function(grp_infos, grp_name):
    name = underscore_to_camelcase(grp_name)
    func_str = "   // Register Group %s\n"%(grp_name)
    func_name = "Get%sGroupParam"%(name)
    func_str += "   unsigned int %s(unsigned int *GrpLen)\n   {\n"%(func_name)
    func_str += gen_group_function_cases(grp_infos, func_name)
    func_str += "   };\n"
    func_str += "\n"
    return func_str

  def gen_nr_of_regs_function_cases(nr_of_regs, func_name):
    switch_str = "      switch(this->mVersion)\n      {\n"
    for idx in range(len(versions)):
      switch_str += "         case %d:\n"%(versions[idx])
      switch_str += "            return %d;\n"%(nr_of_regs[idx])
    switch_str += "         default:\n"
    switch_str += "            throw %s(\"Function %s() not defined for this board version\");\n"%(exception, func_name)
    switch_str += "      };\n"
    return switch_str

  def gen_nr_of_regs_functions():
    func_str = "   // Number of Control Registers\n"
    func_name = "GetNrOfCtrlRegs"
    func_str += "   unsigned int %s()\n   {\n"%(func_name)
    func_str += gen_nr_of_regs_function_cases(get_nr_of_ctrl_regs(modules), func_name)
    func_str += "   };\n"
    func_str += "\n"
    func_str += "   // Number of Status Registers\n"
    func_name = "GetNrOfStatRegs"
    func_str += "   unsigned int %s()\n   {\n"%(func_name)
    func_str += gen_nr_of_regs_function_cases(get_nr_of_stat_regs(modules), func_name)
    func_str += "   };\n"
    func_str += "\n"
    return func_str

  def write_defines(out_file):
    layout_version_regs = get_bit_entries(modules, "REG_LAYOUT_VERSION")
    if entries_are_equal(layout_version_regs):
      outline =  "#define mVersionReg    0x%04X\n"%(layout_version_regs[0][reg_addr_idx])
      outline += "#define mVersionMask   0x%08X\n"%(layout_version_regs[0][bit_mask_idx])
      outline += "#define mVersionOffs   %d\n\n"%(layout_version_regs[0][bit_offs_idx])
      out_file.write(outline)
    else:
      sys.exit("Error: Layout version registers are not equal!\n%s"%(layout_version_regs))

  def write_nr_of_regs(out_file):
    out_file.write(gen_nr_of_regs_functions())

  def write_bit_functions(out_file):
    bit_list = get_all_bit_names(modules)
    for bit_name in bit_list:
#   ////// ------ Control Register 0 [0x1000]: WDB_LOC - Location where the WD2 is plugged in (Default: 0xFFFFFFFF) ------ //////
#   // 0x00FF0000: CRATE_ID - ID of the crate where the current board is plugged in
      all_bit_params = get_bit_entries(modules, bit_name)
      out_file.write(gen_bit_function(all_bit_params, bit_name))

  def write_group_functions(out_file):
    groups = get_all_reg_groups(modules)
    group_list = get_all_group_names(groups)
    for grp_name in group_list:
      all_groups_params = get_group_entries(groups, grp_name)
      out_file.write(gen_group_function(all_groups_params, grp_name))

  def copy_file_to_target(src, dst):
    src_dir  = os.path.dirname(src)
    src_file = os.path.basename(src)
    dst_dir  = os.path.dirname(dst)
    dst_file = os.path.basename(dst)
    if not os.path.exists(dst_dir):
      create_folder = raw_input("Destination path does not exist. Shall I creat it? ([y]/n): ")
      if (not create_folder) or (create_folder == 'y') or (create_folder == "yes"):
        os.makedirs(dst_dir)
      else:
        return
    copyfile(src, dst)
    print "From %s\nto   %s"%(src, dst)

  def find_vcs_root(test):
      import subprocess
      import os
      cwd = os.getcwd()
      os.chdir(test)
      result = subprocess.check_output(["git", "rev-parse", "--show-toplevel"])
      os.chdir(cwd)
      result = result.rsplit('\n',1)[0]
      if os.path.isdir(result):
        return result
      else:
        return None

  # #########################################################################################################################

  def write_class():
    outfile = class_outfile
    out_file = file(outdir+os.sep+outfile,'w')
    out_file.write(class_header)
    outline =  "// Created :  %s\n//\n"%(datetime.now().strftime('%d.%m.%Y %H:%M:%S'))
    outline += "// Register Layout Versions :  %s\n//\n\n"%(", ".join(map(str,versions)))
    out_file.write(outline)
    outline =  "#ifndef __wdbreg_h__\n"
    outline += "#define __wdbreg_h__\n\n"
    outline += "//--------------------------------------------------------------------\n\n"
    out_file.write(outline)
    write_defines(out_file)
    outline = "//--------------------------------------------------------------------\n\n"
    out_file.write(outline)
    out_file.write(class_start)
    write_nr_of_regs(out_file)
    outline  = "//--------------------------------------------------------------------\n"
    outline += "// Bit/Register Access and Location Functions\n"
    outline += "//--------------------------------------------------------------------\n\n"
    out_file.write(outline)
    write_bit_functions(out_file)
    outline  = "//--------------------------------------------------------------------\n"
    outline += "// Register Group Functions\n"
    outline += "//--------------------------------------------------------------------\n\n"
    out_file.write(outline)
    write_group_functions(out_file)
    out_file.write(class_end)
    out_file.close()

  # #########################################################################################################################

  print "Writing temporary output file:"
  print "Writing " + outdir + os.sep + class_outfile + "..."
  write_class()

  # Copy files to target directories?
  do_file_copy = raw_input("\nCopy files to target directories? ([y]/n): ")
  if (not do_file_copy) or (do_file_copy == 'y') or (do_file_copy == "yes"):
    do_file_copy = 'y'
  else:
    do_file_copy = 'n'

  # Copy files to target directories!
  if do_file_copy == 'y':
    print "\n"
    #git_root = find_vcs_root(os.getcwd()) # to find git root with current working directory
    git_root = find_vcs_root(indir)
    parent_git_root = find_vcs_root("%s/.."%git_root)
    if parent_git_root == None:
      sys.exit("Error: Parent GIT root not found.\n       Make sure .xlsx source is in a wavedaq_main GIT repository directory")
    else:
      print "Performing filecopy to target directories (GIT root = %s):"%(parent_git_root)
      print "\nCopy WDS softwre files ..."
      print "---\n%s :"%(class_outfile)
      copy_file_to_target(outdir + os.sep + class_outfile, parent_git_root + os.sep + class_dir + os.sep + class_outfile)
      print "\nFilecopy completed !!!\n"
      print "\n--> Remember to recompile code <--\n"
  else:
    print "Target files not updated"
  print "\n===========================================\n"
